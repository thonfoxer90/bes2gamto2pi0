#!/bin/bash

option=$1

srcPath=/data/work/kphpbb/bgarillo/ee-ee2Pi0/Bes2Gamto2Pi0-00-00-01 
dataPath=/data/work/kphpbb/bgarillo/ee-ee2Pi0

currentDir=$PWD

if [[ $option == *"--Selection "* ]]
then
echo "Selection"
fi

if [[ $option == *"--cpToWorkDir "* ]]
then
echo "Copy files to analysis directory"
${srcPath}/run/HIMSTER/scpMCIHEPtoHIMSTER.sh . 

cd ${dataPath}/Galuga2.0/Logs/
rm pipiList.txt
ls -1 pipi* > pipiList.txt
sed -i 's@pipi@'"$PWD"'/pipi@g' pipiList.txt
/usr/bin/perl -i -pe 'chomp if eof' pipiList.txt
cd ${dataPath}/Galuga2.0/Histos/
${srcPath}/run/IHEP/mergeROOT.sh galuga histo_all &
cd ${dataPath}/Galuga2.0/NTuples/
${srcPath}/run/IHEP/mergeROOT.sh galuga ntuple_all &

cd ${dataPath}/Ekhara2.1/GamGamToEta/Logs/
rm fileList.txt
ls -1 Ekhara* > fileList.txt
sed -i 's@Ekhara@'"$PWD"'/Ekhara@g' fileList.txt
/usr/bin/perl -i -pe 'chomp if eof' fileList.txt
cd ${dataPath}/Ekhara2.1/GamGamToEta/Histos/
${srcPath}/run/IHEP/mergeROOT.sh Ekhara histo_all &
cd ${dataPath}/Ekhara2.1/GamGamToEta/NTuples/
${srcPath}/run/IHEP/mergeROOT.sh Ekhara ntuple_all &

cd ${dataPath}/Ekhara2.1/GamGamToPi0/Logs/
rm fileList.txt
ls -1 Ekhara* > fileList.txt
sed -i 's@Ekhara@'"$PWD"'/Ekhara@g' fileList.txt
/usr/bin/perl -i -pe 'chomp if eof' fileList.txt
cd ${dataPath}/Ekhara2.1/GamGamToPi0/Histos/
${srcPath}/run/IHEP/mergeROOT.sh Ekhara histo_all &
cd ${dataPath}/Ekhara2.1/GamGamToPi0/NTuples/
${srcPath}/run/IHEP/mergeROOT.sh Ekhara ntuple_all &

cd $currentDir
fi

if [[ $option == *"--LumGamGam "* ]]
then
echo "Calculate 2Gamma luminosity function"
cd ${dataPath}/lumGamGam/Logs/
rm logList_Q2pW.txt
ls -1 galuga_LumGamGam_iapprx1_Q2pW* > logList_Q2pW.txt
sed -i 's@galuga@'"$PWD"'/galuga@g' logList_Q2pW.txt
/usr/bin/perl -i -pe 'chomp if eof' logList_Q2pW.txt

rm logList_W.txt
ls -1 galuga_LumGamGam_iapprx1_W* > logList_W.txt
sed -i 's@galuga@'"$PWD"'/galuga@g' logList_W.txt
/usr/bin/perl -i -pe 'chomp if eof' logList_W.txt

#cd ${dataPath}/lumGamGam/GenOut/
#rm GenOutList.txt
#ls -1 galuga* > GenOutList.txt
#sed -i 's@galuga@'"$PWD"'/galuga@g' GenOutList.txt
#/usr/bin/perl -i -pe 'chomp if eof' GenOutList.txt

cd $currentDir

#root -l -b -q ''"$srcPath"'/src/lundToNTuple.c+("lumGamGam/GenOut/GenOutList.txt","GamGam ", "lumGamGam/NTuples/ntuple")'

#root -l -b -q ''"$srcPath"'/src/lumGamGam.c+("Bes2Gamto2Pi0-00-00-01/run/lumGamGam.in","","lumGamGam/lumGamGam")'

root -l -b -q ''"$srcPath"'/src/lumGamGam.c+("'"$srcPath"'/run/lumGamGam.in","","lumGamGam/lumGamGam")'
root -l -b -q ''"$srcPath"'/src/lumGamGam.c+("'"$srcPath"'/run/lumGamGam_W.in","","lumGamGam/lumGamGam_W")'

fi

if [[ $option == *"--YieldTable "* ]]
then
	root -l -b -q ''"$srcPath"'/src/makeYieldTable.c+("'"$srcPath"'/run/makeYieldTable.in","MCTruth","YieldTable/MCTruth")'
	root -l -b -q ''"$srcPath"'/src/makeYieldTable.c+("'"$srcPath"'/run/makeYieldTable.in","Tagged","YieldTable/Tagged")'
	root -l -b -q ''"$srcPath"'/src/makeYieldTable.c+("'"$srcPath"'/run/makeYieldTable.in","eTagged","YieldTable/eTagged")'
	root -l -b -q ''"$srcPath"'/src/makeYieldTable.c+("'"$srcPath"'/run/makeYieldTable.in","pTagged","YieldTable/pTagged")'
fi


if [[ $option == *"--Normalize "* ]]
then
	echo "Normalize histograms"
	root -l -b -q ''"$srcPath"'/src/normalizeToLum.c+("'"$srcPath"'/run/normalizeToLum.NTuples.in")'
	root -l -b -q ''"$srcPath"'/src/normalizeToLum.c+("'"$srcPath"'/run/normalizeToLum.Histos.in")'
	echo 'gROOT->LoadMacro("'"$srcPath"'/src/compDataMCScaled.c+");MultiCompDataMCScaled("'"$srcPath"'/run/compDataMCScaled.in","Histo=Tagged","compDataMCScaled/log");' | root -l -b
	echo 'gROOT->LoadMacro("'"$srcPath"'/src/compDataMCScaled.c+");MultiCompDataMCScaled("'"$srcPath"'/run/compDataMCScaled.in","Histo=Tagged isLogScale=0","compDataMCScaled/linear");' | root -l -b
	root -l -b -q ''"$srcPath"'/src/makeYieldTable.c+("'"$srcPath"'/run/makeYieldTable_Scaled.in","Tagged","YieldTable/scaled/Tagged")'
	root -l -b -q ''"$srcPath"'/src/makeYieldTable.c+("'"$srcPath"'/run/makeYieldTable_Scaled.in","eTagged","YieldTable/scaled/eTagged")'
	root -l -b -q ''"$srcPath"'/src/makeYieldTable.c+("'"$srcPath"'/run/makeYieldTable_Scaled.in","pTagged","YieldTable/scaled/pTagged")'
fi

if [[ $option == *"--Binning "* ]]
then
	if [[ $option == *"Q2AbsCThetaStM2Pi_10x5x45 "* ]]
	then
	root -l -b -q ''"$srcPath"'/src/makeBinning.cxx+("2Gamto2Pi0_Q2AbsCThetaStM2Pi_10x5x45","binning")'
	elif [[ $option == *"Q2AbsCThetaStPhiStM2Pi_10x5x10x45 "* ]]
	then
	root -l -b -q ''"$srcPath"'/src/makeBinning.cxx+("2Gamto2Pi0_Q2AbsCThetaStPhiStM2Pi_10x5x10x45","binning")'
	fi
	root -l -b -q ''"$srcPath"'/src/drawBinning.c+("Data/NTuples/ntuple_all.root","BinningDir=binning DrawE=1 ","Data/drawBinning/drawBinning")'
	root -l -b -q ''"$srcPath"'/src/drawBinning.c+("Galuga2.0/NTuples/ntuple_all.root","BinningDir=binning ","Galuga2.0/drawBinning/drawBinning")'
fi

if [[ $option == *"--ResolutionRECGEN "* ]]
then
root -l -b -q ''"$srcPath"'/src/deltaRECGEN.c+("Galuga2.0/NTuples/ntuple_all.root","BinningDir=binning","deltaRECGEN/deltaRECGEN")'
root -l -b -q ''"$srcPath"'/src/deltaRECGEN.c+("Galuga2.0/NTuples/ntuple_all.root","BinningDir=binning Mode=2 ","deltaRECGEN/deltaRECGEN_2D")'
fi

if [[ $option == *"--FitPi0InvM "* ]]
then
root -l -b -q ''"$srcPath"'/src/fitPi0InvM.c+("Data/NTuples/ntuple_all.root","","Data/fitPi0InvM/fitPi0InvM")'
fi

if [[ $option == *"--BgndSub "* ]]
then
##With include "../Bes2Gamto2Pi0/MyGlobal.h"
	#root -l -b -q << STOP
	#.L ${srcPath}/src/MyGlobal.cxx+
	#.L ${srcPath}/src/makeTHnSparse.c+
	#makeTHnSparse("${srcPath}/run/makeTHnSparse.in","binning") 
#STOP
	#echo '.L '"${srcPath}"'/src/MyGlobal.cxx+;.x '"${srcPath}"'/src/makeTHnSparse.c+("'"${srcPath}"'/run/makeTHnSparse.in","binning");' | root -l -b -q
##With include "MyGlobal.cxx"
	root -l -b -q ''"$srcPath"'/src/makeTHnSparse.c+("'"${srcPath}"'/run/makeTHnSparse.in","binning")' 
	root -l -b -q ''"$srcPath"'/src/bgndSubtract.c+("'"${srcPath}"'/run/bgndSubtract.in","hnD","bgndSubtract/bgndSubtract")'
	root -l -b -q ''"$srcPath"'/src/bgndSubtract.c+("'"${srcPath}"'/run/bgndSubtract.in","hnDeT","bgndSubtract/bgndSubtracteT")'
	root -l -b -q ''"$srcPath"'/src/bgndSubtract.c+("'"${srcPath}"'/run/bgndSubtract.in","hnDpT","bgndSubtract/bgndSubtractpT")'
	hadd -f bgndSubtract/bgndSubtractAll.root bgndSubtract/*.root
fi

if [[ $option == *"--Unfolding "* ]]
then
cd ../Galuga2.0/NTuples/
ls -1 galuga_2Pi0_20000* galuga_2Pi0_20001* > fileList_Data.txt
sed -i 's@galuga@'"$PWD"'/galuga@g' fileList_Data.txt
/usr/bin/perl -i -pe 'chomp if eof' fileList_Data.txt
mv fileList_Data.txt $currentDir/unfolding/.

ls -1 galuga_2Pi0_2001* > fileList_MC.txt
sed -i 's@galuga@'"$PWD"'/galuga@g' fileList_MC.txt
/usr/bin/perl -i -pe 'chomp if eof' fileList_MC.txt
mv fileList_MC.txt $currentDir/unfolding/.

cd $currentDir
root -l -b -q ''"$srcPath"'/src/unfold.c+("'"${srcPath}"'/run/unfold.in","unfolding/unfolding")'
root -l -b -q ''"$srcPath"'/src/unfold.c+("'"${srcPath}"'/run/unfold_TestSample.in","unfolding/unfolding_TestSample")'
fi

if [[ $option == *"--Efficiency "* ]]
then
root -l -b -q ''"$srcPath"'/src/efficiency.c+("Galuga2.0/NTuples/ntuple_all.root","BinningDir=binning","Efficiency/Efficiency")'
root -l -b -q ''"$srcPath"'/src/drawCompEfficiency.c+("Efficiency/Efficiency.root","","Efficiency/CompEfficiency")'
fi

if [[ $option == *"--XSec "* ]]
then
root -l -b -q ''"$srcPath"'/src/crossSection.c+("'"${srcPath}"'/run/crossSection.in","crossSection/crossSection")'
root -l -b -q ''"$srcPath"'/src/drawCrossSection.c("crossSection/crossSection.root","X=W,Y=Q2","crossSection/dsigmaeedQ2CThetaW_XeqWYeqQ2")'
root -l -b -q ''"$srcPath"'/src/drawCrossSection.c("crossSection/crossSection.root","X=Q2,Y=W","crossSection/dsigmaeedQ2CThetaW_XeqQ2YeqW")'
root -l -b -q ''"$srcPath"'/src/drawCrossSection.c("crossSection/crossSection.root","dsigmaeedW","crossSection/dsigmaeedW")'
root -l -b -q ''"$srcPath"'/src/drawCrossSection.c("crossSection/crossSection.root","dsigmaggdW","crossSection/dsigmaggdW")'
fi
