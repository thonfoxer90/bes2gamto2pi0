#!/bin/bash

IHEPpath=/scratchfs/bes/garillon
#chnList=('Data' 'Galuga' 'Bhabha' 'qqbar' 'D0D0bar' 'DpDm' 'GammaJpsi' 'GammaPsip' 'nonDD')

chnList=('D0D0bar')

for i in "${chnList[@]}"
  do
	path=${IHEPpath}/$i/Histos	
	echo $path
	cd $path	
	/home/bes/garillon/boss-6.6.4/Analysis/Bes2Gamto2Pi0/Bes2Gamto2Pi0-00-00-01/run/IHEP/mergeROOT.sh histo_run histo_all
	done