#!/bin/bash

inputname=$1
outname=$2

echo "mergeROOT.sh"
echo "Input : $inputname"
echo "Output : $outname"
ps -p $$ -oargs=
echo "Remove empty files"
find . -type f -size 0 -delete

echo "Move files into separated folders"
rm -rf ${outname}.root
rm -rf part-*
c=1; d=1; mkdir -p part-${d}
#for file in *.root
for file in `ls *.root `;
do
if [ $c -eq 500 ]
then
d=$(( d + 1 )); c=0; mkdir -p part-${d}
fi
mv "$file" part-${d}/
c=$(( c + 1 ))
done

echo "Merge files within each directory"
which hadd 
#for dir in part-*
for dir in `ls -d part-*`;
do
echo "hadd -f $dir/${outname}.root $dir/${inputname}"
hadd -f $dir/${outname}.root $dir/${inputname}*
#/afs/ihep.ac.cn/bes3/offline/ExternalLib/SLC6/ExternalLib/external/ROOT/5.34.09/x86_64-slc6-gcc46-opt/root/bin/hadd
FileList+=("$dir/${outname}.root") 
done

echo "Merge all files"
hadd -f ${outname}.root "${FileList[@]}"

echo "Check file"
size=`du -k "${outname}.root" | cut -f1`
#Remove temporary directories if the final file is heavier than 1kB
if [ $size -ge 1 ]
	then
	echo "Remove temporary directories"
	rm -rf part-* 
fi