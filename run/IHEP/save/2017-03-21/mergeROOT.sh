#!/bin/bash

inputname=$1
outname=$2

echo "Remove empty files"
find . -type f -size 0 -delete

echo "Move files into separated folders"
rm -rf part-*
c=1; d=1; mkdir -p part-${d}
for file in *.root
do
if [ $c -eq 500 ]
then
d=$(( d + 1 )); c=0; mkdir -p part-${d}
fi
mv "$file" part-${d}/
c=$(( c + 1 ))
done

echo "Merge files within each directory"
for dir in part-*
do
hadd -f $dir/${outname}.root $dir/${inputname}*
FileList+=("$dir/${outname}.root") 
done

echo "Merge all files"
hadd -f ${outname}.root "${FileList[@]}"
rm -rf part-*