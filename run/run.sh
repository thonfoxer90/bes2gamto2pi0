#!/bin/bash

option=$1

srcPath=/home/bgarillo/boss-6.6.4/Analysis/Bes2Gamto2Pi0/Bes2Gamto2Pi0-00-00-01 
dataPath=/data/work/kphpbb/bgarillo/ee-ee2Pi0

currentDir=$PWD

if [[ $option == *"--Setup "* ]]
then
echo "Setup symlinks"
ln -fs ${srcPath} Bes2Gamto2Pi0
ln -fs ${srcPath}/analysis/Release/analysis .
#cp ${srcPath}/run/*.in .
fi

if [[ $option == *"--HelloWorld "* ]]
then
Bes2Gamto2Pi0/analysis/Release/analysis HelloWorld test
fi

if [[ $option == *"--Selection "* ]]
then
echo "Selection"
fi

if [[ $option == *"--cpToWorkDir "* ]]
then
echo "Copy files to analysis directory"
Bes2Gamto2Pi0/run/HIMSTER/scpMCIHEPtoHIMSTER.sh . 

cd ${dataPath}/Galuga2.0/Logs/
rm pipiList.txt
ls -1 pipi* > pipiList.txt
sed -i 's@pipi@'"$PWD"'/pipi@g' pipiList.txt
/usr/bin/perl -i -pe 'chomp if eof' pipiList.txt
cd ${dataPath}/Galuga2.0/Histos/
${srcPath}/run/IHEP/mergeROOT.sh galuga_2Pi0 histo_all &
cd ${dataPath}/Galuga2.0/NTuples/
${srcPath}/run/IHEP/mergeROOT.sh galuga_2Pi0 ntuple_all &

cd ${dataPath}/Ekhara2.1/GamGamToEta/Logs/
rm fileList.txt
ls -1 Ekhara* > fileList.txt
sed -i 's@Ekhara@'"$PWD"'/Ekhara@g' fileList.txt
/usr/bin/perl -i -pe 'chomp if eof' fileList.txt
cd ${dataPath}/Ekhara2.1/GamGamToEta/Histos/
${srcPath}/run/IHEP/mergeROOT.sh Ekhara histo_all &
cd ${dataPath}/Ekhara2.1/GamGamToEta/NTuples/
${srcPath}/run/IHEP/mergeROOT.sh Ekhara ntuple_all &

cd ${dataPath}/Ekhara2.1/GamGamToPi0/Logs/
rm fileList.txt
ls -1 Ekhara* > fileList.txt
sed -i 's@Ekhara@'"$PWD"'/Ekhara@g' fileList.txt
/usr/bin/perl -i -pe 'chomp if eof' fileList.txt
cd ${dataPath}/Ekhara2.1/GamGamToPi0/Histos/
${srcPath}/run/IHEP/mergeROOT.sh Ekhara histo_all &
cd ${dataPath}/Ekhara2.1/GamGamToPi0/NTuples/
${srcPath}/run/IHEP/mergeROOT.sh Ekhara ntuple_all &

cd ${dataPath}/Ekhara2.1/GamGamToEtaPrime/Logs/
rm fileList.txt
ls -1 Ekhara* > fileList.txt
sed -i 's@Ekhara@'"$PWD"'/Ekhara@g' fileList.txt
/usr/bin/perl -i -pe 'chomp if eof' fileList.txt
cd ${dataPath}/Ekhara2.1/GamGamToEtaPrime/Histos/
${srcPath}/run/IHEP/mergeROOT.sh Ekhara histo_all &
cd ${dataPath}/Ekhara2.1/GamGamToEtaPrime/NTuples/ Ekhara ntuple_all &

cd $currentDir
fi

if [[ $option == *"--LumGamGam "* ]]
then
echo "Calculate 2Gamma luminosity function"
#cd ${dataPath}/lumGamGam/Logs/
#rm logList_Q2pW.txt
#ls -1 galuga_LumGamGam_iapprx1_Q2pW* > logList_Q2pW.txt
#sed -i 's@galuga@'"$PWD"'/galuga@g' logList_Q2pW.txt
#/usr/bin/perl -i -pe 'chomp if eof' logList_Q2pW.txt

#rm logList_W.txt
#ls -1 galuga_LumGamGam_iapprx1_W* > logList_W.txt
#sed -i 's@galuga@'"$PWD"'/galuga@g' logList_W.txt
#/usr/bin/perl -i -pe 'chomp if eof' logList_W.txt

#cd $currentDir

	Bes2Gamto2Pi0/analysis LumGamGam Bes2Gamto2Pi0/run/LumGamGam.in "" "lumGamGam/lumGamGam"

fi

if [[ $option == *"--YieldTable "* ]]
then
	Bes2Gamto2Pi0/analysis/Release/analysis YieldTable Bes2Gamto2Pi0/run/makeYieldTable.in MCTruth YieldTable/MCTruth
	Bes2Gamto2Pi0/analysis/Release/analysis YieldTable Bes2Gamto2Pi0/run/makeYieldTable.in Tagged YieldTable/Tagged
	Bes2Gamto2Pi0/analysis/Release/analysis YieldTable Bes2Gamto2Pi0/run/makeYieldTable.in eTagged YieldTable/eTagged
	Bes2Gamto2Pi0/analysis/Release/analysis YieldTable Bes2Gamto2Pi0/run/makeYieldTable.in pTagged YieldTable/pTagged
fi


if [[ $option == *"--Normalize "* ]]
then
	echo "Normalize histograms"
	Bes2Gamto2Pi0/analysis/Release/analysis NormalizeToLum Bes2Gamto2Pi0/run/normalizeToLum.NTuples.in
	Bes2Gamto2Pi0/analysis/Release/analysis NormalizeToLum Bes2Gamto2Pi0/run/normalizeToLum.Histos.in
	
	Bes2Gamto2Pi0/analysis/Release/analysis CompDataMCScaledPlotter Bes2Gamto2Pi0/run/compDataMCScaled.in "Histo=Tagged" "compDataMCScaled/log"
	Bes2Gamto2Pi0/analysis/Release/analysis CompDataMCScaledPlotter Bes2Gamto2Pi0/run/compDataMCScaled.in "Histo=Tagged isLogScale=0" "compDataMCScaled/linear"
	
	Bes2Gamto2Pi0/analysis/Release/analysis YieldTable Bes2Gamto2Pi0/run/makeYieldTable_Scaled.in Tagged YieldTable/scaled/Tagged
	Bes2Gamto2Pi0/analysis/Release/analysis YieldTable Bes2Gamto2Pi0/run/makeYieldTable_Scaled.in eTagged YieldTable/scaled/eTagged
	Bes2Gamto2Pi0/analysis/Release/analysis YieldTable Bes2Gamto2Pi0/run/makeYieldTable_Scaled.in pTagged YieldTable/scaled/pTagged
	
fi

if [[ $option == *"--Binning "* ]]
then
	if [[ $option == *"Q2M2PiAbsCThetaSt_10x45x5 "* ]]
	then
	root -l -b -q 'Bes2Gamto2Pi0/src/makeBinning.cxx+("Q2M2PiAbsCThetaSt_10x45x5","binning")'
	elif [[ $option == *"Q2M2PiAbsCThetaStPhiSt_10x45x5x9 "* ]]
	then
	root -l -b -q 'Bes2Gamto2Pi0/src/makeBinning.cxx+("Q2M2PiAbsCThetaStPhiSt_10x45x5x9","binning")'
	fi
	root -l -b -q 'Bes2Gamto2Pi0/src/drawBinning.c+("Data/NTuples/ntuple_all.root","BinningDir=binning DrawE=1 ","Data/drawBinning/drawBinning")'
	root -l -b -q 'Bes2Gamto2Pi0/src/drawBinning.c+("Galuga2.0/NTuples/ntuple_all.root","BinningDir=binning ","Galuga2.0/drawBinning/drawBinning")'
fi

if [[ $option == *"--ResolutionRECGEN "* ]]
then
root -l -b -q 'Bes2Gamto2Pi0/src/deltaRECGEN.c+("Galuga2.0/NTuples/ntuple_all.root","BinningDir=binning","deltaRECGEN/deltaRECGEN")'
root -l -b -q 'Bes2Gamto2Pi0/src/deltaRECGEN.c+("Galuga2.0/NTuples/ntuple_all.root","BinningDir=binning Mode=2 ","deltaRECGEN/deltaRECGEN_2D")'
fi

if [[ $option == *"--FitPi0InvM "* ]]
then
root -l -b -q 'Bes2Gamto2Pi0/src/fitPi0InvM.c+("Data/NTuples/ntuple_all.root","","Data/fitPi0InvM/fitPi0InvM")'
fi

if [[ $option == *"--BgndSub "* ]]
then
	Bes2Gamto2Pi0/analysis/Release/analysis THnSparseMaker Bes2Gamto2Pi0/run/makeTHnSparse.in "binning"
	Bes2Gamto2Pi0/analysis/Release/analysis BgndSubtractor Bes2Gamto2Pi0/run/bgndSubtract.in hnD bgndSubtract/bgndSubtract
	Bes2Gamto2Pi0/analysis/Release/analysis BgndSubtractor Bes2Gamto2Pi0/run/bgndSubtract.in hnDeT bgndSubtract/bgndSubtracteT
	Bes2Gamto2Pi0/analysis/Release/analysis BgndSubtractor Bes2Gamto2Pi0/run/bgndSubtract.in hnDpT bgndSubtract/bgndSubtractpT
	hadd -f bgndSubtract/bgndSubtractAll.root bgndSubtract/*.root
fi

if [[ $option == *"--Unfolding "* ]]
then
cd ../Galuga2.0/NTuples/
ls -1 galuga_2Pi0_20000* galuga_2Pi0_20001* > fileList_Data.txt
sed -i 's@galuga@'"$PWD"'/galuga@g' fileList_Data.txt
/usr/bin/perl -i -pe 'chomp if eof' fileList_Data.txt
mv fileList_Data.txt $currentDir/unfolding/.

ls -1 galuga_2Pi0_2001* > fileList_MC.txt
sed -i 's@galuga@'"$PWD"'/galuga@g' fileList_MC.txt
/usr/bin/perl -i -pe 'chomp if eof' fileList_MC.txt
mv fileList_MC.txt $currentDir/unfolding/.

cd $currentDir
root -l -b -q 'Bes2Gamto2Pi0/src/unfold.c+("Bes2Gamto2Pi0/run/unfold.in","unfolding/unfolding")'
root -l -b -q 'Bes2Gamto2Pi0/src/unfold.c+("Bes2Gamto2Pi0/run/unfold_TestSample.in","unfolding/unfolding_TestSample")'
fi

if [[ $option == *"--Efficiency "* ]]
then
#root -l -b -q 'Bes2Gamto2Pi0/src/efficiency.c+("Galuga2.0/NTuples/ntuple_all.root","BinningDir=binning","Efficiency/Efficiency")'
Bes2Gamto2Pi0/analysis/Release/analysis Efficiency Galuga2.0/NTuples/ntuple_all.root "BinningDir=binning" Efficiency/Efficiency
root -l -b -q 'Bes2Gamto2Pi0/src/drawCompEfficiency.c+("Efficiency/Efficiency.root","","Efficiency/CompEfficiency")'
fi

if [[ $option == *"--HoleFactors "* ]]
then
Bes2Gamto2Pi0/analysis/Release/analysis HoleFactors Efficiency/Efficiency.root "" HoleFactors/HoleFactors
fi

if [[ $option == *"--XSec "* ]]
then
Bes2Gamto2Pi0/analysis/Release/analysis CrossSection Bes2Gamto2Pi0/run/crossSection.in crossSection/crossSection
root -l -b -q 'Bes2Gamto2Pi0/src/drawCrossSection.c("crossSection/crossSection.root","X=W,Y=Q2","crossSection/dsigmaeedQ2CThetaW_XeqWYeqQ2")'
root -l -b -q 'Bes2Gamto2Pi0/src/drawCrossSection.c("crossSection/crossSection.root","X=Q2,Y=W","crossSection/dsigmaeedQ2CThetaW_XeqQ2YeqW")'
root -l -b -q 'Bes2Gamto2Pi0/src/drawCrossSection.c("crossSection/crossSection.root","dsigmaeedW","crossSection/dsigmaeedW")'
root -l -b -q 'Bes2Gamto2Pi0/src/drawCrossSection.c("crossSection/crossSection.root","dsigmaggdW","crossSection/dsigmaggdW")'
fi

if [[ $option == *"--PWA1D "* ]]
then
Bes2Gamto2Pi0/analysis/Release/analysis PartialWaveFitter1D crossSection/crossSection.root "" PWA/fits
fi

if [[ $option == *"--PWA "* ]]
then
Bes2Gamto2Pi0/analysis/Release/analysis PartialWaveFitter crossSection/crossSection.root "" PWA/fits
fi