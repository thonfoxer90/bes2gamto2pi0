#!/bin/sh

start=$1
end=$2
info=$3

#Nevents=100
#Nevents=50000
Nevents=20000
#Nevents=2000

for seed in `seq $start $end`;
do
	X=${info}_$seed
	#Write Galuga (MC event generator) input file
	echo 'pipi.hbk' >>pipi_$X.in
	echo $seed >>pipi_$X.in
	echo '1 1 11 ivegas iwaght imodpi' >>pipi_$X.in #2Pi0
	#echo '1 1 31 ivegas iwaght imodpi' >>pipi_$X.in #Pi+Pi-
	echo "$Nevents" '1000000        1000000   Nev  Nfcall(Vegas)  Npts' >>pipi_$X.in
	echo '-1      1    -1d16  0d0  -1d0 iapprx,  imodel, t2umin, t2umax, t2user'	>>pipi_$X.in
	echo '3.773d0     0.13d0     5d0  1d0      rs, Wmin, Wmax, W' >>pipi_$X.in
	echo '0d0     180.d0   0d0  10d0   theta1min/max(deg)  E1min/max(GeV) ' >>pipi_$X.in
	echo '0d0     180.d0   0d0  10d0   theta2min/max(deg)  E2min/max(GeV)' >>pipi_$X.in
	echo '100d0  2d0  1d12  0.3d0  3d0 100d0 0.1888756028   BFKL: delta,Qmin,Qmax,Lambda,Nf,cQ,cmu' >>pipi_$X.in
	
	
	#Write ManualGenerator (BESIII simulation) option file
	echo '#include "$KKMCROOT/share/jobOptions_KKMC.txt"' >>JobOptions-ManualGenerator_$X.txt
	echo >>JobOptions-ManualGenerator_$X.txt	
	echo '//*************job options for generator (KKMC)***************' >>JobOptions-ManualGenerator_$X.txt
  echo 'KKMC.CMSEnergy = 3.773; //BOSS gives warning in case of inconsistency with .out file above' >>JobOptions-ManualGenerator_$X.txt
  echo 'KKMC.BeamEnergySpread=0.0;' >>JobOptions-ManualGenerator_$X.txt
  echo 'KKMC.GeneratePsi3770=true;' >>JobOptions-ManualGenerator_$X.txt
	echo 'KKMC.ParticleDecayThroughEvtGen = true;' >>JobOptions-ManualGenerator_$X.txt
	echo 'KKMC.ThresholdCut=0.0;' >>JobOptions-ManualGenerator_$X.txt
	echo 'KKMC.RadiationCorrection = false;' >>JobOptions-ManualGenerator_$X.txt
	echo '//*************job options for EvtGen***************' >>JobOptions-ManualGenerator_$X.txt
	echo >>JobOptions-ManualGenerator_$X.txt
	echo '#include "$MANUALGENERATORROOT/share/ManualGenerator.txt"' >>JobOptions-ManualGenerator_$X.txt
#	echo 'ManualGenerator.InputFile ="/data/work/kphpbb/bgarillo/Galuga2.0/GenOut/'"galuga_$X.out"'";' >>JobOptions-ManualGenerator_$X.txt
	echo 'ManualGenerator.InputFile ="$MyLocalWorkDir/'"galuga.out"'";' >>JobOptions-ManualGenerator_$X.txt
	echo >>JobOptions-ManualGenerator_$X.txt
	echo '//**************job options for random number************************' >>JobOptions-ManualGenerator_$X.txt
	echo 'BesRndmGenSvc.RndmSeed = 100;' >>JobOptions-ManualGenerator_$X.txt
	echo >>JobOptions-ManualGenerator_$X.txt
	echo '//**************job options for detector simulation******************' >>JobOptions-ManualGenerator_$X.txt
	echo '#include "$BESSIMROOT/share/G4Svc_BesSim.txt"' >>JobOptions-ManualGenerator_$X.txt
	echo >>JobOptions-ManualGenerator_$X.txt
	echo '//configure for calibration constants' >>JobOptions-ManualGenerator_$X.txt
	echo '#include "$CALIBSVCROOT/share/calibConfig_sim.txt"' >>JobOptions-ManualGenerator_$X.txt
	echo >>JobOptions-ManualGenerator_$X.txt
	echo 'RealizationSvc.RunIdList = {-11414, 0, -13988, -14395, 0, -14604, -20448, 0, -23454};//RUN Number for psipp data' >>JobOptions-ManualGenerator_$X.txt
	echo >>JobOptions-ManualGenerator_$X.txt
	echo '#include "$ROOTIOROOT/share/jobOptions_Digi2Root.txt"' >>JobOptions-ManualGenerator_$X.txt
#	echo 'RootCnvSvc.digiRootOutputFile = "/data/work/kphpbb/bgarillo/Galuga2.0/rtraw/'"galuga_$X.rtraw"'";' >>JobOptions-ManualGenerator_$X.txt
	echo 'RootCnvSvc.digiRootOutputFile = "$MyLocalWorkDir/'"galuga.rtraw"'";' >>JobOptions-ManualGenerator_$X.txt
	echo >>JobOptions-ManualGenerator_$X.txt
	echo 'MessageSvc.OutputLevel  = 5;'>>JobOptions-ManualGenerator_$X.txt
	echo >>JobOptions-ManualGenerator_$X.txt
	echo '// Number of events to be processed (default is 10)'>>JobOptions-ManualGenerator_$X.txt
	echo 'ApplicationMgr.EvtMax =' "$Nevents" '; //Number of events to be simulated'>>JobOptions-ManualGenerator_$X.txt
	
	#Write reconstruction option file
	echo '#include "$ROOTIOROOT/share/jobOptions_ReadRoot.txt"' >>JobOptions-Rec_$X.txt
	echo '#include "$OFFLINEEVENTLOOPMGRROOT/share/OfflineEventLoopMgr_Option.txt"' >>JobOptions-Rec_$X.txt
	echo '#include "$BESEVENTMIXERROOT/share/jobOptions_EventMixer_rec.txt"' >>JobOptions-Rec_$X.txt
	echo 'MixerAlg.ReplaceDataPath = "/data/group/bes3/bes3data/offline/data/fltrandomtrg/";' >>JobOptions-Rec_$X.txt
	echo '#include "$CALIBSVCROOT/share/job-CalibData.txt"' >>JobOptions-Rec_$X.txt
	echo '#include "$MAGNETICFIELDROOT/share/MagneticField.txt"' >>JobOptions-Rec_$X.txt
	echo '#include "$ESTIMEALGROOT/share/job_EsTimeAlg.txt"' >>JobOptions-Rec_$X.txt
	echo '#include "$MDCXRECOROOT/share/jobOptions_MdcPatTsfRec.txt"' >>JobOptions-Rec_$X.txt
	echo '#include "$KALFITALGROOT/share/job_kalfit_numf_data.txt"' >>JobOptions-Rec_$X.txt
	echo '#include "$MDCDEDXALGROOT/share/job_dedx_all.txt"' >>JobOptions-Rec_$X.txt
	echo '#include "$TRKEXTALGROOT/share/TrkExtAlgOption.txt"' >>JobOptions-Rec_$X.txt
	echo '#include "$TOFRECROOT/share/jobOptions_TofRec.txt"' >>JobOptions-Rec_$X.txt
	echo '#include "$TOFENERGYRECROOT/share/TofEnergyRecOptions_MC.txt"' >>JobOptions-Rec_$X.txt
	echo '#include "$EMCRECROOT/share/EmcRecOptions.txt"' >>JobOptions-Rec_$X.txt
	echo '#include "$MUCRECALGROOT/share/jobOptions_MucRec.txt"' >>JobOptions-Rec_$X.txt
	echo '#include "$EVENTASSEMBLYROOT/share/EventAssembly.txt"' >>JobOptions-Rec_$X.txt
	echo '#include "$EVENTASSEMBLYROOT/share/EventAssembly.txt"' >>JobOptions-Rec_$X.txt
	echo '#include "$VEEVERTEXALGROOT/share/jobOptions_veeVertex.txt"' >>JobOptions-Rec_$X.txt
	echo '#include "$HLTMAKERALGROOT/share/jobOptions_HltMakerAlg.txt"' >>JobOptions-Rec_$X.txt
	echo '#include "$EVENTNAVIGATORROOT/share/EventNavigator.txt"' >>JobOptions-Rec_$X.txt
	echo '#include "$ROOTIOROOT/share/jobOptions_Dst2Root.txt"' >>JobOptions-Rec_$X.txt
	echo '#include "$CALIBSVCROOT/share/calibConfig_rec_mc.txt"' >>JobOptions-Rec_$X.txt
	echo  >>JobOptions-Rec_$X.txt
	echo 'BesRndmGenSvc.RndmSeed = 100;' >>JobOptions-Rec_$X.txt
	echo  >>JobOptions-Rec_$X.txt
	echo 'MessageSvc.OutputLevel = 5;' >>JobOptions-Rec_$X.txt
	echo  >>JobOptions-Rec_$X.txt
#	echo 'EventCnvSvc.digiRootInputFile = {"/data/work/kphpbb/bgarillo/Galuga2.0/rtraw/'"galuga_$X.rtraw"'"};' >>JobOptions-Rec_$X.txt
	echo 'EventCnvSvc.digiRootInputFile = {"$MyLocalWorkDir/'"galuga.rtraw"'"};' >>JobOptions-Rec_$X.txt
	echo  >>JobOptions-Rec_$X.txt
#	echo 'EventCnvSvc.digiRootOutputFile ="/data/work/kphpbb/bgarillo/Galuga2.0/dst/'"galuga_$X.dst"'";' >>JobOptions-Rec_$X.txt
	echo 'EventCnvSvc.digiRootOutputFile ="$MyLocalWorkDir/'"galuga.dst"'";' >>JobOptions-Rec_$X.txt
	echo  >>JobOptions-Rec_$X.txt
	echo 'ApplicationMgr.EvtMax =' "$Nevents" ';' >>JobOptions-Rec_$X.txt
	
	
	#Write 
	echo '#include "/cluster/bes3/dist/6.6.4.p01/Event/RootIO/RootIO-00-01-31/share/jobOptions_ReadRec.txt"' >>JobOptions-Bes2Gamto2Pi0_$X.txt
	echo '#include "/cluster/bes3/dist/6.6.4.p01/Analysis/VertexFit/VertexFit-00-02-69/share/jobOptions_VertexDbSvc.txt"' >>JobOptions-Bes2Gamto2Pi0_$X.txt
	echo '#include "/cluster/bes3/dist/6.6.4.p01/MagneticField/MagneticField-00-01-38/share/MagneticField.txt"' >>JobOptions-Bes2Gamto2Pi0_$X.txt
	echo '#include "/cluster/bes3/dist/6.6.4.p01/Analysis/PhotonCor/AbsCor/AbsCor-00-00-28/share/jobOptions_AbsCor.txt"' >>JobOptions-Bes2Gamto2Pi0_$X.txt
	echo >>JobOptions-Bes2Gamto2Pi0_$X.txt
	echo 'ApplicationMgr.DLLs += {"Bes2Gamto2Pi0"};' >>JobOptions-Bes2Gamto2Pi0_$X.txt
	echo 'ApplicationMgr.TopAlg += {"Bes2Gamto2Pi0"};' >>JobOptions-Bes2Gamto2Pi0_$X.txt
	echo 'Bes2Gamto2Pi0.WriteAllMCTruth = true;' >>JobOptions-Bes2Gamto2Pi0_$X.txt
	echo 'Bes2Gamto2Pi0.MCGenerator="Galuga";' >>JobOptions-Bes2Gamto2Pi0_$X.txt
	echo 'MessageSvc.OutputLevel = 5;' >>JobOptions-Bes2Gamto2Pi0_$X.txt
	echo 'ApplicationMgr.EvtMax = -1;' >>JobOptions-Bes2Gamto2Pi0_$X.txt
	echo 'ApplicationMgr.HistogramPersistency = "ROOT";' >>JobOptions-Bes2Gamto2Pi0_$X.txt
#	echo 'NTupleSvc.Output = {"FILE1 DATAFILE='"'/data/work/kphpbb/bgarillo/Galuga2.0/Ntuples/galuga_"$X".root' OPT='NEW' TYP='ROOT'"'"};' >>JobOptions-Bes2Gamto2Pi0_$X.txt
	echo 'NTupleSvc.Output = {"FILE1 DATAFILE='"'\$MyLocalWorkDir/galuga.root' OPT='NEW' TYP='ROOT'"'"};' >>JobOptions-Bes2Gamto2Pi0_$X.txt
#	echo 'EventCnvSvc.digiRootInputFile={"/data/work/kphpbb/bgarillo/Galuga2.0/dst/'"galuga_$X.dst"'"};' >>JobOptions-Bes2Gamto2Pi0_$X.txt
	echo 'HistogramPersistencySvc.OutputFile ="'"\$MyLocalWorkDir/galuga_$X.histo.root"'";' >>JobOptions-Bes2Gamto2Pi0_$X.txt
	echo 'EventCnvSvc.digiRootInputFile={"$MyLocalWorkDir/'"galuga.dst"'"};' >>JobOptions-Bes2Gamto2Pi0_$X.txt
	
	#Write 
	echo "#!/bin/bash" > MCSubScript_$X
  echo "#PBS -N MCAna_$X" >> MCSubScript_$X
  echo "#PBS -q long" >> MCSubScript_$X
  echo "#PBS -l walltime=23:00:00" >> MCSubScript_$X
	echo "#PBS -l vmem=4096mb" >> MCSubScript_$X
  echo "#PBS -j oe" >> MCSubScript_$X
  echo "#PBS -o /data/work/kphpbb/bgarillo/Galuga2.0/Logs/MCAna_$X.log" >> MCSubScript_$X
  echo "#PBS -V" >> MCSubScript_$X
  echo "#PBS -S /bin/bash" >> MCSubScript_$X
  echo >> MCSubScript_$X
	#echo 'echo "truc"' >> MCSubScript_$X
	echo "cd /home/bgarillo/cmthome-6.6.4/" >> MCSubScript_$X
	echo "source setup.sh" >> MCSubScript_$X	
  echo >> MCSubScript_$X
  echo "export MyLocalWorkDir=/tmp/\$PBS_JOBID" >> MCSubScript_$X
  echo "echo \$MyLocalWorkDir" >> MCSubScript_$X
  echo "if [ -d \"\$MyLocalWorkDir\" ];" >> MCSubScript_$X
  echo "then " >> MCSubScript_$X
  echo "rm -rf \$MyLocalWorkDir" >> MCSubScript_$X
  echo "fi" >> MCSubScript_$X
  echo "mkdir \$MyLocalWorkDir" >> MCSubScript_$X
  echo >> MCSubScript_$X
  #echo "cd "`pwd` >> MCSubScript_$X
	#Run the event generator
	echo "cd \$MyLocalWorkDir" >> MCSubScript_$X
	echo "time /home/bgarillo/boss-6.6.4/Generator/Galuga-2pi0/galuga < `pwd`/pipi_$X.in > pipi.log" >> MCSubScript_$X
	
	#echo "time /home/redmer/Generators/Galuga2.0/galuganew < `pwd`/pipi_$X.in > pipi.log" >> MCSubScript_$X
	#echo "mv pipi.hbk.out galuga.out" >> MCSubScript_$X
	#echo "mv pipi.hbk.hbk pipi.hbk" >> MCSubScript_$X 
	
	echo "cp pipi.log /data/work/kphpbb/bgarillo/Galuga2.0/Logs/pipi_$X.log" >> MCSubScript_$X
	echo "cp pipi.hbk /data/work/kphpbb/bgarillo/Galuga2.0/hbk/pipi_$X.hbk" >> MCSubScript_$X
	echo "cp \$MyLocalWorkDir/galuga.out /data/work/kphpbb/bgarillo/Galuga2.0/GenOut/galuga_$X.out" >> MCSubScript_$X
  echo >> MCSubScript_$X
	echo "cd "`pwd` >> MCSubScript_$X
	echo "boss.exe JobOptions-ManualGenerator_$X.txt"  >> MCSubScript_$X
	echo "cp \$MyLocalWorkDir/galuga.rtraw /data/work/kphpbb/bgarillo/Galuga2.0/rtraw/galuga_$X.rtraw" >> MCSubScript_$X
	echo "boss.exe JobOptions-Rec_$X.txt" >> MCSubScript_$X
	echo "cp \$MyLocalWorkDir/galuga.dst /data/work/kphpbb/bgarillo/Galuga2.0/dst/galuga_$X.dst" >> MCSubScript_$X
	echo "boss.exe JobOptions-Bes2Gamto2Pi0_$X.txt" >> MCSubScript_$X
	echo "cp \$MyLocalWorkDir/galuga.root /data/work/kphpbb/bgarillo/Galuga2.0/NTuples/galuga_$X.root" >> MCSubScript_$X
	echo "cp \$MyLocalWorkDir/galuga_$X.histo.root /data/work/kphpbb/bgarillo/Galuga2.0/Histos/galuga_$X.histo.root" >> MCSubScript_$X
	echo "root -l -b -q '/home/bgarillo/boss-6.6.4/Analysis/Bes2Gamto2Pi0/Bes2Gamto2Pi0-00-00-01/src/rmEmptyNtuples.c(\"/data/work/kphpbb/bgarillo/Galuga2.0/NTuples/galuga_$X.root\",\" \")'" >> MCSubScript_$X
#  echo "mv \$MyLocalWorkDir/ntuple_run$X.root /data/work/kphpbb/bgarillo/Galuga2.0/NTuples/." >> MCSubScript_$X
#  echo "ls -ltra \$MyLocalWorkDir" >> MCSubScript_$X
#  echo "rm -rf \$MyLocalWorkDir" >> MCSubScript_$X
#  echo "unset MyLocalWorkDir" >> MCSubScript_$X
#  echo >> MCSubScript_$X
#  echo "rm DataAnaJob_$X" >> MCSubScript_$X
#  echo "rm MCSubScript_$X" >> MCSubScript_$X
#  echo >> MCSubScript_$X
#  echo "exit 0" >> MCSubScript_$X
	
	echo "Submit $X"
  qsub MCSubScript_$X
done
