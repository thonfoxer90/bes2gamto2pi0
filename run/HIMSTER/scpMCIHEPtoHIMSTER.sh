#!/bin/sh

#CHECK BEFORE RUNNING THE SCRIPT THAT SSH KEY TO HOST EXISTS
#OTHERWISE, DO :
#ssh-keygen
#ssh-copy-id user@host
#This allow to perform scp without asking password for every ssh connection


outdir=$1
rundir=$2

chnList=('Data' 'Galuga2.0' 'Ekhara2.1/GamGamToEta' 'Ekhara2.1/GamGamToPi0' 'Ekhara2.1/GamGamToEtaPrime' 'Bhabha' 'qqbar' 'D0D0bar' 'DpDm' 'GammaJpsi' 'GammaPsip' 'nonDD')

#chnList=('Ekhara2.1/GamGamToEtaPrime') 

IHEPpath=/scratchfs/bes/garillon
HIMSTERpath=/data/work/kphpbb/bgarillo

#fullHIMSTERpath=${HIMSTERpath}/ee-ee2Pi0/${outdir}
#mkdir ${fullHIMSTERpath}

for chndir in "${chnList[@]}"
  do
	fullIHEPpath=${IHEPpath}/${rundir}/${chndir}
	fullHIMSTERpath=${HIMSTERpath}/ee-ee2Pi0/${outdir}/${chndir}
	mkdir ${fullHIMSTERpath}
	if [[ $chndir == *'Data'* ]]
		then
			scp -r garillon@lxslc6.ihep.ac.cn:"${fullIHEPpath}/{Histos,NTuples}" ${fullHIMSTERpath}/. &
	elif [[ $chndir == *'Galuga2.0'* ]]
		then
		cp -rv $HIMSTERpath/Galuga2.0/{NTuples,Histos,Logs} ${fullHIMSTERpath}/. &
	elif [[ $chndir == *'Ekhara2.1/GamGamToEta'* ]]
		then
		cp -rv $HIMSTERpath/Ekhara2.1/GamGamToEta/{NTuples,Histos,Logs} ${fullHIMSTERpath}/. &
	elif [[ $chndir == *'Ekhara2.1/GamGamToEtaPrime'* ]]
		then
		cp -rv $HIMSTERpath/Ekhara2.1/GamGamToEtaPrime/{NTuples,Histos,Logs} ${fullHIMSTERpath}/. &
	elif [[ $chndir == *'Ekhara2.1/GamGamToPi0'* ]]
		then
		cp -rv $HIMSTERpath/Ekhara2.1/GamGamToPi0/{NTuples,Histos,Logs} ${fullHIMSTERpath}/. &
	else
			scp -r garillon@lxslc6.ihep.ac.cn:"${fullIHEPpath}/{Histos,NTuples}" ${fullHIMSTERpath}/. &
		fi
	done
	
wait