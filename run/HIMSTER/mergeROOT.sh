#!/bin/bash

outdir=$1

HIMSTERpath=/data/work/kphpbb/bgarillo/ee-ee2Pi0

#chnList=('Data' 'Galuga' 'Ekhara' 'Bhabha' 'qqbar' 'D0D0bar' 'DpDm' 'GammaJpsi' 'GammaPsip' 'nonDD')

chnList=('Galuga' 'Ekhara')

for i in "${chnList[@]}"
  do
	if [[ $i == *'Galuga'* ]]
  	then
		fullpath=${HIMSTERpath}/${outdir}/$i/Histos
		cd $fullpath
		pwd
		hadd -f histo_all.root galuga*
	elif [[ $i == *'Ekhara'* ]]
		then
		fullpath=${HIMSTERpath}/${outdir}/$i/Histos
		cd $fullpath
		pwd
		hadd -f histo_all.root Ekhara*
	else
			fullpath=${HIMSTERpath}/${outdir}/$i/Histos
			cd $fullpath
			pwd
			hadd -f histo_all.root histo_run*
			if [[ $i == *'Data'* ]]
			then
				fullpath=${HIMSTERpath}/${outdir}/$i/NTuples
				cd $fullpath
				pwd
			hadd -f ntuple_all.root ntuple_run*
			fi
	fi
	done