#!/bin/sh

#CHECK BEFORE RUNNING THE SCRIPT THAT SSH KEY TO HOST EXISTS
#OTHERWISE, DO :
#ssh-keygen
#ssh-copy-id user@host
#This allow to perform scp without asking password for every ssh connection


outdir=$1

IHEPpath=/scratchfs/bes/garillon
HIMSTERpath=/data/work/kphpbb/bgarillo/ee-ee2Pi0

fullHIMSTERpath=${HIMSTERpath}/${outdir}
mkdir ${fullHIMSTERpath}

chndir=Data
fullIHEPpath=${IHEPpath}/${chndir}
fullHIMSTERpath=${HIMSTERpath}/${outdir}/${chndir}
mkdir ${fullHIMSTERpath}
scp -r garillon@lxslc5.ihep.ac.cn:"${fullIHEPpath}/{Histos,Logs,NTuples}" ${fullHIMSTERpath}/. &

#chndir=Galuga2.0
#fullIHEPpath=${IHEPpath}/${chndir}
#fullHIMSTERpath=${HIMSTERpath}/${outdir}/${chndir}
#mkdir ${fullHIMSTERpath}
#scp -r garillon@lxslc5.ihep.ac.cn:"${fullIHEPpath}/{Histos,Logs}" ${fullHIMSTERpath}/. &

chndir=qqbar
fullIHEPpath=${IHEPpath}/${chndir}
fullHIMSTERpath=${HIMSTERpath}/${outdir}/${chndir}
mkdir ${fullHIMSTERpath}
scp -r garillon@lxslc5.ihep.ac.cn:"${fullIHEPpath}/{Histos,Logs}" ${fullHIMSTERpath}/. &

chndir=Bhabha
fullIHEPpath=${IHEPpath}/${chndir}
fullHIMSTERpath=${HIMSTERpath}/${outdir}/${chndir}
mkdir ${fullHIMSTERpath}
scp -r garillon@lxslc5.ihep.ac.cn:"${fullIHEPpath}/{Histos,Logs}" ${fullHIMSTERpath}/. &

chndir=D0D0bar
fullIHEPpath=${IHEPpath}/${chndir}
fullHIMSTERpath=${HIMSTERpath}/${outdir}/${chndir}
mkdir ${fullHIMSTERpath}
scp -r garillon@lxslc5.ihep.ac.cn:"${fullIHEPpath}/{Histos,Logs}" ${fullHIMSTERpath}/. &

chndir=DpDm
fullIHEPpath=${IHEPpath}/${chndir}
fullHIMSTERpath=${HIMSTERpath}/${outdir}/${chndir}
mkdir ${fullHIMSTERpath}
scp -r garillon@lxslc5.ihep.ac.cn:"${fullIHEPpath}/{Histos,Logs}" ${fullHIMSTERpath}/. &

chndir=GammaPsip
fullIHEPpath=${IHEPpath}/${chndir}
fullHIMSTERpath=${HIMSTERpath}/${outdir}/${chndir}
mkdir ${fullHIMSTERpath}
scp -r garillon@lxslc5.ihep.ac.cn:"${fullIHEPpath}/{Histos,Logs}" ${fullHIMSTERpath}/. &

chndir=GammaJpsi
fullIHEPpath=${IHEPpath}/${chndir}
fullHIMSTERpath=${HIMSTERpath}/${outdir}/${chndir}
mkdir ${fullHIMSTERpath}
scp -r garillon@lxslc5.ihep.ac.cn:"${fullIHEPpath}/{Histos,Logs}" ${fullHIMSTERpath}/. &

chndir=nonDD
fullIHEPpath=${IHEPpath}/${chndir}
fullHIMSTERpath=${HIMSTERpath}/${outdir}/${chndir}
mkdir ${fullHIMSTERpath}
scp -r garillon@lxslc5.ihep.ac.cn:"${fullIHEPpath}/{Histos,Logs}" ${fullHIMSTERpath}/. &