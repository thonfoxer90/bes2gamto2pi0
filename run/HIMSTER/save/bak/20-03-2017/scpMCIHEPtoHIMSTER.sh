#!/bin/sh

#CHECK BEFORE RUNNING THE SCRIPT THAT SSH KEY TO HOST EXISTS
#OTHERWISE, DO :
#ssh-keygen
#ssh-copy-id user@host
#This allow to perform scp without asking password for every ssh connection


outdir=$1

#chnList=('Data' 'Galuga' 'Ekhara' 'Bhabha' 'qqbar' 'D0D0bar' 'DpDm' 'GammaJpsi' 'GammaPsip' 'nonDD')

chnList=('Data' 'Bhabha' 'qqbar' 'D0D0bar' 'DpDm' 'GammaJpsi' 'GammaPsip' 'nonDD')

#chnList=('Data')

IHEPpath=/scratchfs/bes/garillon
HIMSTERpath=/data/work/kphpbb/bgarillo/ee-ee2Pi0

fullHIMSTERpath=${HIMSTERpath}/${outdir}
mkdir ${fullHIMSTERpath}

for chndir in "${chnList[@]}"
  do
	fullIHEPpath=${IHEPpath}/${chndir}
	fullHIMSTERpath=${HIMSTERpath}/${outdir}/${chndir}
	mkdir ${fullHIMSTERpath}
	if [[ $chndir == *'Data'* ]]
		then
			scp -r garillon@lxslc5.ihep.ac.cn:"${fullIHEPpath}/{Histos,Logs,NTuples}" ${fullHIMSTERpath}/. &
		else
			scp -r garillon@lxslc5.ihep.ac.cn:"${fullIHEPpath}/{Histos,Logs}" ${fullHIMSTERpath}/. &
		fi
	done
	