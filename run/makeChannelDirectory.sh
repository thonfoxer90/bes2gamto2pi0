#!/bin/sh

dirname=$1
option=$2

mkdir ${dirname}

cd ${dirname}
mkdir rtraw dst Histos Logs NTuples
if [[ $option == *"Galuga2.0"* ]]
then
mkdir GenOut
fi
cd ../