#!/bin/sh

#CHECK BEFORE RUNNING THE SCRIPT THAT SSH KEY TO HOST EXISTS
#OTHERWISE, DO :
#ssh-keygen
#ssh-copy-id user@host
#This allow to perform scp without asking password for every ssh connection


outdir=$1

#chnList=('Data' 'Galuga' 'Ekhara' 'Bhabha' 'qqbar' 'D0D0bar' 'DpDm' 'GammaJpsi' 'GammaPsip' 'nonDD')

#chnList=('Galuga' 'Ekhara')

#chnList=('Data')

chnList=('Data' 'Bhabha' 'qqbar' 'D0D0bar' 'DpDm' 'GammaJpsi' 'GammaPsip' 'nonDD') 

IHEPpath=/scratchfs/bes/garillon
HIMSTERpath=/media/garillon/DATA/Brice/BESIII/GammaGamma/Run/save

fullHIMSTERpath=${HIMSTERpath}/${outdir}
mkdir ${fullHIMSTERpath}

for chndir in "${chnList[@]}"
  do
	fullIHEPpath=${IHEPpath}/${chndir}
	fullHIMSTERpath=${HIMSTERpath}/${outdir}/${chndir}
	mkdir ${fullHIMSTERpath}
	if [[ $chndir == *'Data'* ]]
		then
			scp -r garillon@lxslc5.ihep.ac.cn:"${fullIHEPpath}/{Histos,Logs,NTuples}" ${fullHIMSTERpath}/. &
	elif [[ $chndir == *'Galuga'* ]]
		then
		cp -r $HIMSTERpath/Galuga2.0/{Histos,Logs} ${fullHIMSTERpath}/. &
	elif [[ $chndir == *'Ekhara'* ]]
		then
		cp -r $HIMSTERpath/Ekhara2.1/{Histos,Logs} ${fullHIMSTERpath}/. &
	else
			scp -r garillon@lxslc5.ihep.ac.cn:"${fullIHEPpath}/{Histos,Logs}" ${fullHIMSTERpath}/. &
		fi
	done
	
