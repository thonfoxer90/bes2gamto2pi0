#-- start of make_header -----------------

#====================================
#  Library Bes2Gamto2Pi0
#
#   Generated Mon Apr 16 15:10:51 2018  by bgarillo
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_Bes2Gamto2Pi0_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_Bes2Gamto2Pi0_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_Bes2Gamto2Pi0

Bes2Gamto2Pi0_tag = $(tag)

#cmt_local_tagfile_Bes2Gamto2Pi0 = $(Bes2Gamto2Pi0_tag)_Bes2Gamto2Pi0.make
cmt_local_tagfile_Bes2Gamto2Pi0 = $(bin)$(Bes2Gamto2Pi0_tag)_Bes2Gamto2Pi0.make

else

tags      = $(tag),$(CMTEXTRATAGS)

Bes2Gamto2Pi0_tag = $(tag)

#cmt_local_tagfile_Bes2Gamto2Pi0 = $(Bes2Gamto2Pi0_tag).make
cmt_local_tagfile_Bes2Gamto2Pi0 = $(bin)$(Bes2Gamto2Pi0_tag).make

endif

include $(cmt_local_tagfile_Bes2Gamto2Pi0)
#-include $(cmt_local_tagfile_Bes2Gamto2Pi0)

ifdef cmt_Bes2Gamto2Pi0_has_target_tag

cmt_final_setup_Bes2Gamto2Pi0 = $(bin)setup_Bes2Gamto2Pi0.make
#cmt_final_setup_Bes2Gamto2Pi0 = $(bin)Bes2Gamto2Pi0_Bes2Gamto2Pi0setup.make
cmt_local_Bes2Gamto2Pi0_makefile = $(bin)Bes2Gamto2Pi0.make

else

cmt_final_setup_Bes2Gamto2Pi0 = $(bin)setup.make
#cmt_final_setup_Bes2Gamto2Pi0 = $(bin)Bes2Gamto2Pi0setup.make
cmt_local_Bes2Gamto2Pi0_makefile = $(bin)Bes2Gamto2Pi0.make

endif

cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)Bes2Gamto2Pi0setup.make

#Bes2Gamto2Pi0 :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'Bes2Gamto2Pi0'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = Bes2Gamto2Pi0/
#Bes2Gamto2Pi0::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

Bes2Gamto2Pi0libname   = $(bin)$(library_prefix)Bes2Gamto2Pi0$(library_suffix)
Bes2Gamto2Pi0lib       = $(Bes2Gamto2Pi0libname).a
Bes2Gamto2Pi0stamp     = $(bin)Bes2Gamto2Pi0.stamp
Bes2Gamto2Pi0shstamp   = $(bin)Bes2Gamto2Pi0.shstamp

Bes2Gamto2Pi0 :: dirs  Bes2Gamto2Pi0LIB
	$(echo) "Bes2Gamto2Pi0 ok"

#-- end of libary_header ----------------

Bes2Gamto2Pi0LIB :: $(Bes2Gamto2Pi0lib) $(Bes2Gamto2Pi0shstamp)
	@/bin/echo "------> Bes2Gamto2Pi0 : library ok"

$(Bes2Gamto2Pi0lib) :: $(bin)Pi0recHelper.o $(bin)Bes2Gamto2Pi0.o $(bin)ParticleContainer.o $(bin)makeBinning.o $(bin)ChargedTracksList.o $(bin)countNevtDst.o $(bin)MyGlobal.o $(bin)Bes2Gamto2Pi0_entries.o $(bin)Bes2Gamto2Pi0_load.o
	$(lib_echo) library
	$(lib_silent) cd $(bin); \
	  $(ar) $(Bes2Gamto2Pi0lib) $?
	$(lib_silent) $(ranlib) $(Bes2Gamto2Pi0lib)
	$(lib_silent) cat /dev/null >$(Bes2Gamto2Pi0stamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

$(Bes2Gamto2Pi0libname).$(shlibsuffix) :: $(Bes2Gamto2Pi0lib) $(Bes2Gamto2Pi0stamps)
	$(lib_silent) cd $(bin); QUIET=$(QUIET); $(make_shlib) "$(tags)" Bes2Gamto2Pi0 $(Bes2Gamto2Pi0_shlibflags)

$(Bes2Gamto2Pi0shstamp) :: $(Bes2Gamto2Pi0libname).$(shlibsuffix)
	@if test -f $(Bes2Gamto2Pi0libname).$(shlibsuffix) ; then cat /dev/null >$(Bes2Gamto2Pi0shstamp) ; fi

Bes2Gamto2Pi0clean ::
	$(cleanup_echo) objects
	$(cleanup_silent) cd $(bin); /bin/rm -f $(bin)Pi0recHelper.o $(bin)Bes2Gamto2Pi0.o $(bin)ParticleContainer.o $(bin)makeBinning.o $(bin)ChargedTracksList.o $(bin)countNevtDst.o $(bin)MyGlobal.o $(bin)Bes2Gamto2Pi0_entries.o $(bin)Bes2Gamto2Pi0_load.o

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

ifeq ($(INSTALLAREA),)
installarea = $(CMTINSTALLAREA)
else
ifeq ($(findstring `,$(INSTALLAREA)),`)
installarea = $(shell $(subst `,, $(INSTALLAREA)))
else
installarea = $(INSTALLAREA)
endif
endif

install_dir = ${installarea}/${CMTCONFIG}/lib
Bes2Gamto2Pi0installname = $(library_prefix)Bes2Gamto2Pi0$(library_suffix).$(shlibsuffix)

Bes2Gamto2Pi0 :: Bes2Gamto2Pi0install

install :: Bes2Gamto2Pi0install

Bes2Gamto2Pi0install :: $(install_dir)/$(Bes2Gamto2Pi0installname)
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

$(install_dir)/$(Bes2Gamto2Pi0installname) :: $(bin)$(Bes2Gamto2Pi0installname)
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test ! -d "$(install_dir)"; then \
	      mkdir -p $(install_dir); \
	    fi ; \
	    if test -d "$(install_dir)"; then \
	      echo "Installing library $(Bes2Gamto2Pi0installname) into $(install_dir)"; \
	      if test -e $(install_dir)/$(Bes2Gamto2Pi0installname); then \
	        $(cmt_uninstall_area_command) $(install_dir)/$(Bes2Gamto2Pi0installname); \
	        $(cmt_uninstall_area_command) $(install_dir)/$(Bes2Gamto2Pi0installname).cmtref; \
	      fi; \
	      $(cmt_install_area_command) `pwd`/$(Bes2Gamto2Pi0installname) $(install_dir)/$(Bes2Gamto2Pi0installname); \
	      echo `pwd`/$(Bes2Gamto2Pi0installname) >$(install_dir)/$(Bes2Gamto2Pi0installname).cmtref; \
	    fi \
          else \
	    echo "Cannot install library $(Bes2Gamto2Pi0installname), no installation directory specified"; \
	  fi; \
	fi

Bes2Gamto2Pi0clean :: Bes2Gamto2Pi0uninstall

uninstall :: Bes2Gamto2Pi0uninstall

Bes2Gamto2Pi0uninstall ::
	@if test ! "${installarea}" = ""; then \
	  cd $(bin); \
	  if test ! "$(install_dir)" = ""; then \
	    if test -d "$(install_dir)"; then \
	      echo "Removing installed library $(Bes2Gamto2Pi0installname) from $(install_dir)"; \
	      $(cmt_uninstall_area_command) $(install_dir)/$(Bes2Gamto2Pi0installname); \
	      $(cmt_uninstall_area_command) $(install_dir)/$(Bes2Gamto2Pi0installname).cmtref; \
	    fi \
          else \
	    echo "Cannot uninstall library $(Bes2Gamto2Pi0installname), no installation directory specified"; \
	  fi; \
	fi




#-- start of dependency ------------------
ifneq ($(MAKECMDGOALS),Bes2Gamto2Pi0clean)
ifneq ($(MAKECMDGOALS),uninstall)

#$(bin)Bes2Gamto2Pi0_dependencies.make :: dirs

ifndef QUICK
$(bin)Bes2Gamto2Pi0_dependencies.make : $(src)Pi0recHelper.cxx $(src)Bes2Gamto2Pi0.cxx $(src)ParticleContainer.cxx $(src)makeBinning.cxx $(src)ChargedTracksList.cxx $(src)countNevtDst.cxx $(src)MyGlobal.cxx $(src)components/Bes2Gamto2Pi0_entries.cxx $(src)components/Bes2Gamto2Pi0_load.cxx $(use_requirements) $(cmt_final_setup_Bes2Gamto2Pi0)
	$(echo) "(Bes2Gamto2Pi0.make) Rebuilding $@"; \
	  $(build_dependencies) Bes2Gamto2Pi0 -all_sources -out=$@ $(src)Pi0recHelper.cxx $(src)Bes2Gamto2Pi0.cxx $(src)ParticleContainer.cxx $(src)makeBinning.cxx $(src)ChargedTracksList.cxx $(src)countNevtDst.cxx $(src)MyGlobal.cxx $(src)components/Bes2Gamto2Pi0_entries.cxx $(src)components/Bes2Gamto2Pi0_load.cxx
endif

#$(Bes2Gamto2Pi0_dependencies)

-include $(bin)Bes2Gamto2Pi0_dependencies.make

endif
endif
#-- end of dependency -------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),Bes2Gamto2Pi0clean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)Pi0recHelper.d

$(bin)$(binobj)Pi0recHelper.d : $(use_requirements) $(cmt_final_setup_Bes2Gamto2Pi0)

$(bin)$(binobj)Pi0recHelper.d : $(src)Pi0recHelper.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/Pi0recHelper.dep $(use_pp_cppflags) $(Bes2Gamto2Pi0_pp_cppflags) $(lib_Bes2Gamto2Pi0_pp_cppflags) $(Pi0recHelper_pp_cppflags) $(use_cppflags) $(Bes2Gamto2Pi0_cppflags) $(lib_Bes2Gamto2Pi0_cppflags) $(Pi0recHelper_cppflags) $(Pi0recHelper_cxx_cppflags)  $(src)Pi0recHelper.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/Pi0recHelper.o $(src)Pi0recHelper.cxx $(@D)/Pi0recHelper.dep
endif
endif

$(bin)$(binobj)Pi0recHelper.o : $(src)Pi0recHelper.cxx
else
$(bin)Bes2Gamto2Pi0_dependencies.make : $(Pi0recHelper_cxx_dependencies)

$(bin)$(binobj)Pi0recHelper.o : $(Pi0recHelper_cxx_dependencies)
endif
	$(cpp_echo) $(src)Pi0recHelper.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(Bes2Gamto2Pi0_pp_cppflags) $(lib_Bes2Gamto2Pi0_pp_cppflags) $(Pi0recHelper_pp_cppflags) $(use_cppflags) $(Bes2Gamto2Pi0_cppflags) $(lib_Bes2Gamto2Pi0_cppflags) $(Pi0recHelper_cppflags) $(Pi0recHelper_cxx_cppflags)  $(src)Pi0recHelper.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),Bes2Gamto2Pi0clean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)Bes2Gamto2Pi0.d

$(bin)$(binobj)Bes2Gamto2Pi0.d : $(use_requirements) $(cmt_final_setup_Bes2Gamto2Pi0)

$(bin)$(binobj)Bes2Gamto2Pi0.d : $(src)Bes2Gamto2Pi0.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/Bes2Gamto2Pi0.dep $(use_pp_cppflags) $(Bes2Gamto2Pi0_pp_cppflags) $(lib_Bes2Gamto2Pi0_pp_cppflags) $(Bes2Gamto2Pi0_pp_cppflags) $(use_cppflags) $(Bes2Gamto2Pi0_cppflags) $(lib_Bes2Gamto2Pi0_cppflags) $(Bes2Gamto2Pi0_cppflags) $(Bes2Gamto2Pi0_cxx_cppflags)  $(src)Bes2Gamto2Pi0.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/Bes2Gamto2Pi0.o $(src)Bes2Gamto2Pi0.cxx $(@D)/Bes2Gamto2Pi0.dep
endif
endif

$(bin)$(binobj)Bes2Gamto2Pi0.o : $(src)Bes2Gamto2Pi0.cxx
else
$(bin)Bes2Gamto2Pi0_dependencies.make : $(Bes2Gamto2Pi0_cxx_dependencies)

$(bin)$(binobj)Bes2Gamto2Pi0.o : $(Bes2Gamto2Pi0_cxx_dependencies)
endif
	$(cpp_echo) $(src)Bes2Gamto2Pi0.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(Bes2Gamto2Pi0_pp_cppflags) $(lib_Bes2Gamto2Pi0_pp_cppflags) $(Bes2Gamto2Pi0_pp_cppflags) $(use_cppflags) $(Bes2Gamto2Pi0_cppflags) $(lib_Bes2Gamto2Pi0_cppflags) $(Bes2Gamto2Pi0_cppflags) $(Bes2Gamto2Pi0_cxx_cppflags)  $(src)Bes2Gamto2Pi0.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),Bes2Gamto2Pi0clean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)ParticleContainer.d

$(bin)$(binobj)ParticleContainer.d : $(use_requirements) $(cmt_final_setup_Bes2Gamto2Pi0)

$(bin)$(binobj)ParticleContainer.d : $(src)ParticleContainer.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/ParticleContainer.dep $(use_pp_cppflags) $(Bes2Gamto2Pi0_pp_cppflags) $(lib_Bes2Gamto2Pi0_pp_cppflags) $(ParticleContainer_pp_cppflags) $(use_cppflags) $(Bes2Gamto2Pi0_cppflags) $(lib_Bes2Gamto2Pi0_cppflags) $(ParticleContainer_cppflags) $(ParticleContainer_cxx_cppflags)  $(src)ParticleContainer.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/ParticleContainer.o $(src)ParticleContainer.cxx $(@D)/ParticleContainer.dep
endif
endif

$(bin)$(binobj)ParticleContainer.o : $(src)ParticleContainer.cxx
else
$(bin)Bes2Gamto2Pi0_dependencies.make : $(ParticleContainer_cxx_dependencies)

$(bin)$(binobj)ParticleContainer.o : $(ParticleContainer_cxx_dependencies)
endif
	$(cpp_echo) $(src)ParticleContainer.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(Bes2Gamto2Pi0_pp_cppflags) $(lib_Bes2Gamto2Pi0_pp_cppflags) $(ParticleContainer_pp_cppflags) $(use_cppflags) $(Bes2Gamto2Pi0_cppflags) $(lib_Bes2Gamto2Pi0_cppflags) $(ParticleContainer_cppflags) $(ParticleContainer_cxx_cppflags)  $(src)ParticleContainer.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),Bes2Gamto2Pi0clean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)makeBinning.d

$(bin)$(binobj)makeBinning.d : $(use_requirements) $(cmt_final_setup_Bes2Gamto2Pi0)

$(bin)$(binobj)makeBinning.d : $(src)makeBinning.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/makeBinning.dep $(use_pp_cppflags) $(Bes2Gamto2Pi0_pp_cppflags) $(lib_Bes2Gamto2Pi0_pp_cppflags) $(makeBinning_pp_cppflags) $(use_cppflags) $(Bes2Gamto2Pi0_cppflags) $(lib_Bes2Gamto2Pi0_cppflags) $(makeBinning_cppflags) $(makeBinning_cxx_cppflags)  $(src)makeBinning.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/makeBinning.o $(src)makeBinning.cxx $(@D)/makeBinning.dep
endif
endif

$(bin)$(binobj)makeBinning.o : $(src)makeBinning.cxx
else
$(bin)Bes2Gamto2Pi0_dependencies.make : $(makeBinning_cxx_dependencies)

$(bin)$(binobj)makeBinning.o : $(makeBinning_cxx_dependencies)
endif
	$(cpp_echo) $(src)makeBinning.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(Bes2Gamto2Pi0_pp_cppflags) $(lib_Bes2Gamto2Pi0_pp_cppflags) $(makeBinning_pp_cppflags) $(use_cppflags) $(Bes2Gamto2Pi0_cppflags) $(lib_Bes2Gamto2Pi0_cppflags) $(makeBinning_cppflags) $(makeBinning_cxx_cppflags)  $(src)makeBinning.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),Bes2Gamto2Pi0clean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)ChargedTracksList.d

$(bin)$(binobj)ChargedTracksList.d : $(use_requirements) $(cmt_final_setup_Bes2Gamto2Pi0)

$(bin)$(binobj)ChargedTracksList.d : $(src)ChargedTracksList.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/ChargedTracksList.dep $(use_pp_cppflags) $(Bes2Gamto2Pi0_pp_cppflags) $(lib_Bes2Gamto2Pi0_pp_cppflags) $(ChargedTracksList_pp_cppflags) $(use_cppflags) $(Bes2Gamto2Pi0_cppflags) $(lib_Bes2Gamto2Pi0_cppflags) $(ChargedTracksList_cppflags) $(ChargedTracksList_cxx_cppflags)  $(src)ChargedTracksList.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/ChargedTracksList.o $(src)ChargedTracksList.cxx $(@D)/ChargedTracksList.dep
endif
endif

$(bin)$(binobj)ChargedTracksList.o : $(src)ChargedTracksList.cxx
else
$(bin)Bes2Gamto2Pi0_dependencies.make : $(ChargedTracksList_cxx_dependencies)

$(bin)$(binobj)ChargedTracksList.o : $(ChargedTracksList_cxx_dependencies)
endif
	$(cpp_echo) $(src)ChargedTracksList.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(Bes2Gamto2Pi0_pp_cppflags) $(lib_Bes2Gamto2Pi0_pp_cppflags) $(ChargedTracksList_pp_cppflags) $(use_cppflags) $(Bes2Gamto2Pi0_cppflags) $(lib_Bes2Gamto2Pi0_cppflags) $(ChargedTracksList_cppflags) $(ChargedTracksList_cxx_cppflags)  $(src)ChargedTracksList.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),Bes2Gamto2Pi0clean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)countNevtDst.d

$(bin)$(binobj)countNevtDst.d : $(use_requirements) $(cmt_final_setup_Bes2Gamto2Pi0)

$(bin)$(binobj)countNevtDst.d : $(src)countNevtDst.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/countNevtDst.dep $(use_pp_cppflags) $(Bes2Gamto2Pi0_pp_cppflags) $(lib_Bes2Gamto2Pi0_pp_cppflags) $(countNevtDst_pp_cppflags) $(use_cppflags) $(Bes2Gamto2Pi0_cppflags) $(lib_Bes2Gamto2Pi0_cppflags) $(countNevtDst_cppflags) $(countNevtDst_cxx_cppflags)  $(src)countNevtDst.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/countNevtDst.o $(src)countNevtDst.cxx $(@D)/countNevtDst.dep
endif
endif

$(bin)$(binobj)countNevtDst.o : $(src)countNevtDst.cxx
else
$(bin)Bes2Gamto2Pi0_dependencies.make : $(countNevtDst_cxx_dependencies)

$(bin)$(binobj)countNevtDst.o : $(countNevtDst_cxx_dependencies)
endif
	$(cpp_echo) $(src)countNevtDst.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(Bes2Gamto2Pi0_pp_cppflags) $(lib_Bes2Gamto2Pi0_pp_cppflags) $(countNevtDst_pp_cppflags) $(use_cppflags) $(Bes2Gamto2Pi0_cppflags) $(lib_Bes2Gamto2Pi0_cppflags) $(countNevtDst_cppflags) $(countNevtDst_cxx_cppflags)  $(src)countNevtDst.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),Bes2Gamto2Pi0clean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)MyGlobal.d

$(bin)$(binobj)MyGlobal.d : $(use_requirements) $(cmt_final_setup_Bes2Gamto2Pi0)

$(bin)$(binobj)MyGlobal.d : $(src)MyGlobal.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/MyGlobal.dep $(use_pp_cppflags) $(Bes2Gamto2Pi0_pp_cppflags) $(lib_Bes2Gamto2Pi0_pp_cppflags) $(MyGlobal_pp_cppflags) $(use_cppflags) $(Bes2Gamto2Pi0_cppflags) $(lib_Bes2Gamto2Pi0_cppflags) $(MyGlobal_cppflags) $(MyGlobal_cxx_cppflags)  $(src)MyGlobal.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/MyGlobal.o $(src)MyGlobal.cxx $(@D)/MyGlobal.dep
endif
endif

$(bin)$(binobj)MyGlobal.o : $(src)MyGlobal.cxx
else
$(bin)Bes2Gamto2Pi0_dependencies.make : $(MyGlobal_cxx_dependencies)

$(bin)$(binobj)MyGlobal.o : $(MyGlobal_cxx_dependencies)
endif
	$(cpp_echo) $(src)MyGlobal.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(Bes2Gamto2Pi0_pp_cppflags) $(lib_Bes2Gamto2Pi0_pp_cppflags) $(MyGlobal_pp_cppflags) $(use_cppflags) $(Bes2Gamto2Pi0_cppflags) $(lib_Bes2Gamto2Pi0_cppflags) $(MyGlobal_cppflags) $(MyGlobal_cxx_cppflags)  $(src)MyGlobal.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),Bes2Gamto2Pi0clean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)Bes2Gamto2Pi0_entries.d

$(bin)$(binobj)Bes2Gamto2Pi0_entries.d : $(use_requirements) $(cmt_final_setup_Bes2Gamto2Pi0)

$(bin)$(binobj)Bes2Gamto2Pi0_entries.d : $(src)components/Bes2Gamto2Pi0_entries.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/Bes2Gamto2Pi0_entries.dep $(use_pp_cppflags) $(Bes2Gamto2Pi0_pp_cppflags) $(lib_Bes2Gamto2Pi0_pp_cppflags) $(Bes2Gamto2Pi0_entries_pp_cppflags) $(use_cppflags) $(Bes2Gamto2Pi0_cppflags) $(lib_Bes2Gamto2Pi0_cppflags) $(Bes2Gamto2Pi0_entries_cppflags) $(Bes2Gamto2Pi0_entries_cxx_cppflags) -I../src/components $(src)components/Bes2Gamto2Pi0_entries.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/Bes2Gamto2Pi0_entries.o $(src)components/Bes2Gamto2Pi0_entries.cxx $(@D)/Bes2Gamto2Pi0_entries.dep
endif
endif

$(bin)$(binobj)Bes2Gamto2Pi0_entries.o : $(src)components/Bes2Gamto2Pi0_entries.cxx
else
$(bin)Bes2Gamto2Pi0_dependencies.make : $(Bes2Gamto2Pi0_entries_cxx_dependencies)

$(bin)$(binobj)Bes2Gamto2Pi0_entries.o : $(Bes2Gamto2Pi0_entries_cxx_dependencies)
endif
	$(cpp_echo) $(src)components/Bes2Gamto2Pi0_entries.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(Bes2Gamto2Pi0_pp_cppflags) $(lib_Bes2Gamto2Pi0_pp_cppflags) $(Bes2Gamto2Pi0_entries_pp_cppflags) $(use_cppflags) $(Bes2Gamto2Pi0_cppflags) $(lib_Bes2Gamto2Pi0_cppflags) $(Bes2Gamto2Pi0_entries_cppflags) $(Bes2Gamto2Pi0_entries_cxx_cppflags) -I../src/components $(src)components/Bes2Gamto2Pi0_entries.cxx

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq ($(cppdepflags),)

ifneq ($(MAKECMDGOALS),Bes2Gamto2Pi0clean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)Bes2Gamto2Pi0_load.d

$(bin)$(binobj)Bes2Gamto2Pi0_load.d : $(use_requirements) $(cmt_final_setup_Bes2Gamto2Pi0)

$(bin)$(binobj)Bes2Gamto2Pi0_load.d : $(src)components/Bes2Gamto2Pi0_load.cxx
	$(dep_echo) $@
	$(cpp_silent) $(cppcomp) $(cppdepflags) -o $(@D)/Bes2Gamto2Pi0_load.dep $(use_pp_cppflags) $(Bes2Gamto2Pi0_pp_cppflags) $(lib_Bes2Gamto2Pi0_pp_cppflags) $(Bes2Gamto2Pi0_load_pp_cppflags) $(use_cppflags) $(Bes2Gamto2Pi0_cppflags) $(lib_Bes2Gamto2Pi0_cppflags) $(Bes2Gamto2Pi0_load_cppflags) $(Bes2Gamto2Pi0_load_cxx_cppflags) -I../src/components $(src)components/Bes2Gamto2Pi0_load.cxx
	$(cpp_silent) $(format_dependencies) $@ $(@D)/Bes2Gamto2Pi0_load.o $(src)components/Bes2Gamto2Pi0_load.cxx $(@D)/Bes2Gamto2Pi0_load.dep
endif
endif

$(bin)$(binobj)Bes2Gamto2Pi0_load.o : $(src)components/Bes2Gamto2Pi0_load.cxx
else
$(bin)Bes2Gamto2Pi0_dependencies.make : $(Bes2Gamto2Pi0_load_cxx_dependencies)

$(bin)$(binobj)Bes2Gamto2Pi0_load.o : $(Bes2Gamto2Pi0_load_cxx_dependencies)
endif
	$(cpp_echo) $(src)components/Bes2Gamto2Pi0_load.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(Bes2Gamto2Pi0_pp_cppflags) $(lib_Bes2Gamto2Pi0_pp_cppflags) $(Bes2Gamto2Pi0_load_pp_cppflags) $(use_cppflags) $(Bes2Gamto2Pi0_cppflags) $(lib_Bes2Gamto2Pi0_cppflags) $(Bes2Gamto2Pi0_load_cppflags) $(Bes2Gamto2Pi0_load_cxx_cppflags) -I../src/components $(src)components/Bes2Gamto2Pi0_load.cxx

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: Bes2Gamto2Pi0clean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(Bes2Gamto2Pi0.make) $@: No rule for such target" >&2
#	@echo "#CMT> Warning: $@: No rule for such target" >&2; exit
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(Bes2Gamto2Pi0.make): $@: File no longer generated" >&2; exit 0; fi
else
.DEFAULT::
	$(echo) "(Bes2Gamto2Pi0.make) PEDANTIC: $@: No rule for such target" >&2
	if echo $@ | grep '$(package)setup\.make$$' >/dev/null; then\
	 echo "$(CMTMSGPREFIX)" "(Bes2Gamto2Pi0.make): $@: File no longer generated" >&2; exit 0;\
	 elif test $@ = "$(cmt_final_setup)" -o\
	 $@ = "$(cmt_final_setup_Bes2Gamto2Pi0)" ; then\
	 found=n; for s in 1 2 3 4 5; do\
	 sleep $$s; test ! -f $@ || { found=y; break; }\
	 done; if test $$found = n; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(Bes2Gamto2Pi0.make) PEDANTIC: $@: Seems to be missing. Ignore it for now" >&2; exit 0 ; fi;\
	 elif test `expr $@ : '.*/'` -ne 0 ; then\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(Bes2Gamto2Pi0.make) PEDANTIC: $@: Seems to be a missing file. Please check" >&2; exit 2 ; \
	 else\
	 test -z "$(cmtmsg)" ||\
	 echo "$(CMTMSGPREFIX)" "(Bes2Gamto2Pi0.make) PEDANTIC: $@: Seems to be a fake target due to some pattern. Just ignore it" >&2 ; exit 0; fi
endif

Bes2Gamto2Pi0clean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library Bes2Gamto2Pi0
	-$(cleanup_silent) cd $(bin); /bin/rm -f $(library_prefix)Bes2Gamto2Pi0$(library_suffix).a $(library_prefix)Bes2Gamto2Pi0$(library_suffix).s? Bes2Gamto2Pi0.stamp Bes2Gamto2Pi0.shstamp
#-- end of cleanup_library ---------------
