#include <TFile.h>
#include <TDirectory.h>
#include <TTree.h>
#include <TChain.h>
#include <stdio.h>
#include <string.h>
#include <iostream> //std::cout
#include <sstream>
#include <fstream> //std::ifstream,ofstream
#include <vector>
#include <algorithm> //std::sort
#include <TKey.h>
#include <TList.h>
#include <TROOT.h>

#define SSTR( x ) static_cast< std::ostringstream & >( \
        ( std::ostringstream() << std::dec << x ) ).str()
				
using namespace std;

/*
void countNGenEvtDst(string fileList, string option){
int NTot=0;

ifstream readList(fileList.c_str());

if(readList.is_open()){
	while(readList.good()){
	string strline;
	readList >> strline;
	string rootFileName="file.root";
	char cmd[256];
	sprintf(cmd,"ln -fs %s %s",strline.c_str(), rootFileName.c_str());
	cout << cmd << endl;
	system(cmd);
	
	TFile *file = TFile::Open(rootFileName.c_str());
		if(file==NULL){
		cout << "****** UNABLE TO OPEN FILE ******" << endl;
		continue;
		}
	TTree* tree = (TTree*) file->Get("Event");
	TBranch* MCEvent = (TBranch*) tree->GetBranch("TMcEvent");
	int NEntries = MCEvent->GetEntries();
	cout << "File " << strline << " NGen = " << NEntries <<endl;
	NTot=NTot+NEntries;

	sprintf("rm %s",rootFileName.c_str());
	system(cmd);
	} 
}

cout << "Total number of events = " <<  NTot << endl;
}
*/

void countNGenEvtDst(string fileList, string option){
int NTot=0;

ifstream readList(fileList.c_str());

if(readList.is_open()){
	while(readList.good()){
	string strline;
	readList >> strline;
	
	TFile *file = TFile::Open(strline.c_str());
		if(file==NULL){
		cout << "****** UNABLE TO OPEN FILE ******" << endl;
		continue;
		}
	TTree* tree = (TTree*) file->Get("Event");
	TBranch* MCEvent = (TBranch*) tree->GetBranch("TMcEvent");
	int NEntries = MCEvent->GetEntries();
	cout << "File " << strline << " NGen = " << NEntries <<endl;
	NTot=NTot+NEntries;
	
	delete tree;
	delete file;
	} 
}

cout << "Total number of events = " <<  NTot << endl;
}

void makeDstFileList(string dirpath, string outfile){
char cmd[256];
sprintf(cmd,"ls -d -1 %s/*.dst > %s", dirpath.c_str(), outfile.c_str());
cout << cmd << endl;
system(cmd);
}

void makeSymLimkList(string inputfile, string outpath){
ifstream readList(inputfile.c_str());

if(readList.is_open()){
	while(readList.good()){
	string strline;
	readList >> strline;
	string outname;
	//outname= strline.substr(strline.begin(),strline.end(),);
	//outname.copy();
	outname=strline;
	std::size_t pos;
	//Change the path
	pos = outname.find_last_of("/"); 
	outname.replace(0,pos,outpath);
	//Change the file extension from .dst to .root
	pos = outname.find(".dst"); 
	outname.replace(pos,string::npos,string(".root"));
	//cout << outname << endl;
	char cmd[256];
	sprintf(cmd,"ln -fs %s %s",strline.c_str(),outname.c_str());
	cout << cmd << endl;
	system(cmd);
	}
}

char cmd[256];
system(cmd);
sprintf(cmd,"rm %s/rootList",outpath.c_str());	
cout << cmd << endl;
sprintf(cmd,"ls -1 *.root > %s/rootList",outpath.c_str());	
cout << cmd << endl;
system(cmd);

}