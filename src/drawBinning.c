#include <TROOT.h>
#include <TChain.h>
#include <TRint.h>
#include <TTree.h>
#include <TFile.h>
#include <TStyle.h>
#include <TString.h>
#include <TSelector.h>
#include <TF1.h>
#include <TH2.h>
#include <TH1.h>
#include <THnSparse.h>
#include <TVirtualIndex.h>
#include <iostream>             // std::cout, std::endl
#include <fstream>              // std::ifstream
#include <sstream>  
#include <TPad.h>
#include <TCanvas.h>
#include <TGaxis.h>
#include <TPave.h>
#include <TPaveText.h>
#include <TLatex.h>
#include <TVector3.h>
#include <TLorentzVector.h>
#include <TSystem.h>
#include <cstdlib>
#include <map>
#include <time.h>

#include "MyGlobal.cxx"

vector<vector<double> > m_vec_limbins;
string m_binningdir;

int m_drawE=0;

void drawLimBins(vector <double> limbin, TH1D* histo){
vector <TLine*> lines; 
lines.resize(limbin.size());
for(int k=0;k<limbin.size();k++){
	lines[k] = new TLine(limbin[k],histo->GetMinimum(),limbin[k],histo->GetMaximum());
	lines[k] ->SetLineColor(2);
	lines[k]->Draw();
}

}

void drawBinning(string inputname, string option, string outpath){
TFile* file;
file = TFile::Open(inputname.c_str());
TTree* tree = (TTree*) file->Get("Tagged");

vector<string> optionSplit = MyGlobal::tokenize(option,' ');
cout << optionSplit.size() << endl;
for(int k=0;k<optionSplit.size();k++){
	vector<string> strsplit = MyGlobal::tokenize(optionSplit[k],'=');
	string arg=strsplit[0];
	string val=strsplit[1];
	if(arg=="BinningDir"){m_binningdir=val;}
	if(arg=="DrawE"){m_drawE=atoi(val.c_str());}
}

m_vec_limbins.resize(3);
for(int k=0;k<3;k++){
		string limbinFullPath;
		string limbinFileName;
		if(k==0) limbinFileName="limbin_Q2.dat";
		if(k==1) limbinFileName="limbin_AbsCThetaSt.dat";
		if(k==2) limbinFileName="limbin_M2Pions.dat";
		limbinFullPath=m_binningdir+"/"+limbinFileName;
		m_vec_limbins[k]=MyGlobal::getLimBin(limbinFullPath);
}

string outps=outpath+".ps";
string outpdf=outpath+".pdf";

tree->Draw("Q2>>hQ2(100,0.,6.)");
tree->Draw("TMath::Abs(cosThetaStarPi01)>>hAbsCosThetaStar(100,0.,1.)");
tree->Draw("2Pi0InvM>>hM2Pi(100,0.26,3.)");


TH1D* hQ2 = (TH1D*) gROOT->FindObject("hQ2");
hQ2->SetTitle("");
hQ2->GetXaxis()->SetTitle("Q^{2} (GeV^{2})");
//hQ2->GetYaxis()->SetTitle(Form("Events/%f GeV^{2}",MyGlobal::getDeltaX(hQ2)));
TH1D* hAbsCosThetaStar = (TH1D*) gROOT->FindObject("hAbsCosThetaStar");
hAbsCosThetaStar->SetTitle("");
hAbsCosThetaStar->GetXaxis()->SetTitle("|cos #theta*|");
//hAbsCosThetaStar->GetYaxis()->SetTitle(Form("Events/%f ",MyGlobal::getDeltaX(hAbsCosThetaStar)));
TH1D* hM2Pi = (TH1D*) gROOT->FindObject("hM2Pi");
hM2Pi->SetTitle("");
hM2Pi->GetXaxis()->SetTitle("W (GeV/c^{2})");
//hM2Pi->GetYaxis()->SetTitle(Form("Events/%f GeV/c^{2}",MyGlobal::getDeltaX(hM2Pi)));

TCanvas *c = new TCanvas();
gStyle->SetOptStat(0);
c->Divide(2,2);
c->cd(1);
if(m_drawE==1)hQ2->Draw("E");
else hQ2->Draw();
drawLimBins(m_vec_limbins[0], hQ2);
c->cd(2);
if(m_drawE==1) hAbsCosThetaStar->Draw("E");
else hAbsCosThetaStar->Draw();
drawLimBins(m_vec_limbins[1], hAbsCosThetaStar);
c->cd(3);
if(m_drawE==1) hM2Pi->Draw("E");
else hM2Pi->Draw();
drawLimBins(m_vec_limbins[2], hM2Pi);

c->Print(Form("%s",outps.c_str()));

char systcmd[256];
sprintf(systcmd,"ps2pdf %s %s",outps.c_str(), outpdf.c_str());
system(systcmd);
sprintf(systcmd,"rm %s",outps.c_str());
system(systcmd);

}