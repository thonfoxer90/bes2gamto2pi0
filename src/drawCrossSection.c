using namespace std;

void drawCrossSection(string inputfile, string option, string outpath){
TFile* file = TFile::Open(inputfile.c_str());

string outps=outpath+".ps";
string outpdf=outpath+".pdf";

TCanvas *c = new TCanvas();
gStyle->SetOptStat(0);
if(option.find("X=Q2,Y=W")!=string::npos){
	c->Divide(3,2);
	for(int k=0;k<6;k++){
		c->cd(k+1);
		char hname[256];
		if(k==0) sprintf(hname,"dsigmaeedAbsCThetaStAll_Q2M2Pi_0-11");
		if(k==1) sprintf(hname,"dsigmaeedAbsCThetaStAll_Q2M2Pi_1-11");
		if(k==2) sprintf(hname,"dsigmaeedAbsCThetaStAll_Q2M2Pi_2-11");
		if(k==3) sprintf(hname,"dsigmaeedAbsCThetaStAll_Q2M2Pi_0-16");
		if(k==4) sprintf(hname,"dsigmaeedAbsCThetaStAll_Q2M2Pi_1-16");
		if(k==5) sprintf(hname,"dsigmaeedAbsCThetaStAll_Q2M2Pi_2-16");
		TH1D* histo = (TH1D*) file->Get(hname);
		if(histo==NULL) continue;
		histo->Draw("");
	}
}
else if(option.find("X=W,Y=Q2")!=string::npos){
	c->Divide(3,2);
	for(int k=0;k<6;k++){
		c->cd(k+1);
		char hname[256];
		if(k==0) sprintf(hname,"dsigmaeedAbsCThetaStAll_Q2M2Pi_0-11");
		if(k==1) sprintf(hname,"dsigmaeedAbsCThetaStAll_Q2M2Pi_0-16");
		if(k==2) sprintf(hname,"dsigmaeedAbsCThetaStAll_Q2M2Pi_0-18");
		if(k==3) sprintf(hname,"dsigmaeedAbsCThetaStAll_Q2M2Pi_2-11");
		if(k==4) sprintf(hname,"dsigmaeedAbsCThetaStAll_Q2M2Pi_2-16");
		if(k==5) sprintf(hname,"dsigmaeedAbsCThetaStAll_Q2M2Pi_2-18");
		TH1D* histo = (TH1D*) file->Get(hname);
		histo->SetMarkerStyle(8);
		if(histo==NULL) continue;
		histo->Draw("");
	}
}

else if(option.find("dsigmaeedW")!=string::npos){
	TH1D* histo = (TH1D*) file->Get("dsigmaeedM2PiAll");
	histo->SetMarkerStyle(8);
	histo->GetYaxis()->SetRangeUser(0.,50.);
	histo->Draw("E1");
}

else if(option.find("dsigmaggdW")!=string::npos){
	TH1D* histo = (TH1D*) file->Get("dsigmaggdM2PiAll");
	histo->SetMarkerStyle(8);
	histo->Draw("E1");
}

c->Print(outpdf.c_str());

}