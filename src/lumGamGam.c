#include <TROOT.h>
#include <TChain.h>
#include <TRint.h>
#include <TTree.h>
#include <TFile.h>
#include <TStyle.h>
#include <TString.h>
#include <TSelector.h>
#include <TF1.h>
#include <TH2.h>
#include <TH3.h>
#include <TH1.h>
#include <THnSparse.h>
#include <TGraphErrors.h>
#include <TGraph2D.h>
#include <TVirtualIndex.h>
#include <iostream>             // std::cout, std::endl
#include <fstream>              // std::ifstream
#include <sstream>  
#include <TPad.h>
#include <TCanvas.h>
#include <TGaxis.h>
#include <TPave.h>
#include <TPaveText.h>
#include <TLatex.h>
#include <TVector3.h>
#include <TLorentzVector.h>
#include <TSystem.h>
#include <cstdlib>
#include <map>
#include <time.h>

#include "MyGlobal.cxx"

#define SSTR( x ) static_cast< std::ostringstream & >( \
        ( std::ostringstream() << std::dec << x ) ).str()
				
using namespace std;

double Q2eGEN,Q2pGEN;
double WGEN;

string m_NTuplefile;
string m_Intgfile;
string m_binningdir;
int m_dim=2;

TGraph2D *m_gr2D;
TGraphErrors *m_gr1D;

map<string,double> m_integral;
map<string,double> m_integralError;
map<string,double> m_Wmin;
map<string,double> m_Wmax;
map<string,double> m_W;
map<string,double> m_ECMS;
map<string,double> m_t2min;
map<string,double> m_t2max;
map<string,double> m_t2;

double GetIntegral(string inputfile){
double val;
ifstream readFile(inputfile.c_str());

if(readFile.is_open()){
	while(readFile.good()){
		string strline;
		getline(readFile,strline);
		{
				if(strline.find("Total e+e- cross section [nb]") != string::npos)
					{
					stringstream ss;
					ss.str(strline);					
					string strtmp;
					vector <string> strsplit;
					while (getline(ss, strtmp, '=')) {
 						strsplit.push_back(strtmp);
						}
					
					val=atof((strsplit.back()).c_str());
					printf("val = %e nb \n", val);
					break;
					}
				}
	}
}

return val;
}

void ParseLogFile(string inputfile){

ifstream readFile(inputfile.c_str());
cout << inputfile << endl;

vector<string> inputfilesplit = MyGlobal::tokenize(inputfile,'_');
vector<string> lastsplit = MyGlobal::tokenize(inputfilesplit.back(),'.');
string key = lastsplit.front();
cout << key << endl;

if(m_integral.count(key)>0){cout << "KEY ALREADY FILLED" << endl;sleep(2);}
/*
for(std::map<string,double>::iterator it = m_integral.begin(); it!=m_integral.end();++it){
 if(m_integral.count(key)==0){cout << "KEY ALREADY FILLED" << endl;sleep(2);}
}
*/
if(readFile.is_open()){	
	while(readFile.good()){
		string strline;		
		getline(readFile,strline);	
		
		if(strline.find("cm energy sqrt(s)") != string::npos){
			vector <string> strsplit=MyGlobal::tokenize(strline,'=');
			m_ECMS[key]=atof((strsplit.back()).c_str());
		}
		
		if(strline.find("hadronic energy W") != string::npos){
			vector <string> strsplit=MyGlobal::tokenize(strline,'=');
			m_W[key]=atof((strsplit.back()).c_str());
		}
		
		if(strline.find("user-fixed t2") != string::npos){
			vector <string> strsplit=MyGlobal::tokenize(strline,'=');
			m_t2[key]=atof((strsplit.back()).c_str());
		}
			
		if(strline.find("Total e+e- cross section [nb]") != string::npos){
			vector <string> strsplit=MyGlobal::tokenize(strline,'=');
			m_integral[key]=atof((strsplit.back()).c_str());
			cout << m_integral[key] << endl;
		}
		
		if(strline.find("Differential e+e- cross section dsigma/dtau dt2 [nb/GeV^2]") != string::npos){
			vector <string> strsplit=MyGlobal::tokenize(strline,'=');
			m_integral[key]=atof((strsplit.back()).c_str());
			cout << m_integral[key] << endl;
		}
		
		if(strline.find("Two-photon luminosity L(tau)") != string::npos){
			vector <string> strsplit=MyGlobal::tokenize(strline,'=');
			m_integral[key]=atof((strsplit.back()).c_str());
			cout << m_integral[key] << endl;
		}
		
		if(strline.find("Estimated error") != string::npos){
			vector <string> strsplit=MyGlobal::tokenize(strline,'=');
			m_integralError[key]=atof((strsplit.back()).c_str());
			cout << m_integralError[key] << endl;
			break;
		}		
				/*
				if(strline.find("minimum, maximum t2") != string::npos)
					{
					vector <string> strsplit=MyGlobal::tokenize(strline,'=');
					for(int k=0;k<strsplit.size();k++) cout << strsplit[k] << endl;
					cout <<endl;
					break;
					}
				*/				
				
		
	}
}

}

void lumGamGam(string inputcard,string option, string outpath){


ifstream readInput(inputcard.c_str());
if(readInput.is_open()){
	while(readInput.good()){
		string strline;
		getline(readInput,strline);
		
		vector<string> strsplit = MyGlobal::tokenize(strline,' ');
		for(int k=0;k<strsplit.size();k++){
			vector<string> vecstr = MyGlobal::tokenize(strsplit[k],'=');	
			string arg = vecstr[0];
			string val = vecstr[1];
			if(arg=="NTuple") m_NTuplefile=val;
			if(arg=="Integral") m_Intgfile=val;
			if(arg=="Binning") m_binningdir=val;
			if(arg=="Dim") m_dim=atof(val.c_str());
		}
	}
	
}

if(m_Intgfile.find(".txt")!=string::npos){
	ifstream readList(m_Intgfile.c_str());
	int i=0;
	if(readList.is_open()){
		while(readList.good()){
			string strline;
			getline(readList,strline);
			ParseLogFile(strline);
		}
	}
}
else {
	m_integral["0"]=GetIntegral(m_Intgfile);
}

if(option.find("Mode 2 ")!=string::npos){

cout << "m_integral " << m_integral["0"] << endl;

TFile* file= TFile::Open(m_NTuplefile.c_str());
TTree* tree = (TTree*) file->Get("AllMCTruth");

string outname=outpath+".root";
TFile* outROOT = new TFile(outname.c_str(),"RECREATE");

tree->SetBranchAddress("Q2eGEN",&Q2eGEN);
tree->SetBranchAddress("Q2pGEN",&Q2pGEN);
tree->SetBranchAddress("WGEN",&WGEN);

const int nDim=3; //Q2e,Q2p,W
Int_t nBins[nDim];
Double_t min[nDim];
Double_t max[nDim];

for(int k=0;k<nDim;k++) nBins[k]=100;
min[0]=0.;min[1]=0.;min[2]=0.26;
max[0]=4.;max[1]=4.;max[2]=4.;
	
THnSparseD* hnD= new THnSparseD("hnD","hnD",nDim,nBins,min, max);

for(int entry=0;entry<tree->GetEntries();entry++){
	tree->GetEntry(entry);
	if (entry % 100000 == 0) cout << "Event counter = " << entry << endl;
	vector<double> binval; binval.resize(nDim);
	binval[0]=Q2eGEN;binval[1]=Q2pGEN;binval[2]=WGEN;
	//cout << Q2eGEN << " " << Q2pGEN <<  " " << WGEN << endl;
	hnD->Fill(&(binval[0]));
}

TH3D* h3D = (TH3D*) hnD->Projection(0,1,2);
double integralHist = h3D->Integral("width");
cout << integralHist << " " << m_integral["0"] << endl;
h3D->Scale(1/integralHist);
h3D->Scale(m_integral["0"]);

TH1D* hW = (TH1D*) h3D->ProjectionZ();

hnD->Write();
h3D->Write();
hW->Write();
}

else{
	if(m_dim==1){
		
		string outname=outpath+".root";
		string outps=outpath+".ps";
		string outpdf=outpath+".pdf";
		TFile* outROOT = new TFile(outname.c_str(),"RECREATE");

		m_gr1D = new TGraphErrors();

		int Npt=0;
		for(std::map<string,double>::iterator it = m_integral.begin(); it!=m_integral.end();++it){
					string key=it->first;
					cout << key << endl;
					double W=m_W[key];
					double tau=W*W;
					double dLdtau=m_integral[key];
					double ECMS=m_ECMS[key];
					double s=ECMS*ECMS;
					double dLdW=(2*W/s)*dLdtau;
					double val=dLdW;
					//double err=m_integralError[key];
					double err=0;					
					if(val!=val) continue;
					if(err!=err) continue;
					printf("Npt=%d W=%f ECMS=%f dLdtau=%f dLdW=%f \n",Npt,W,ECMS,dLdtau,dLdW);
					m_gr1D->SetPoint(Npt,W,val);
					//m_gr1D->SetPointError(0,err);
					Npt++;
		}
		
		m_gr1D->Sort();
		
		TCanvas *c = new TCanvas();
		m_gr1D->Draw("AL*");
		gPad->SetLogy();
		m_gr1D->SetName("gr1D");
		m_gr1D->SetTitle("");
		m_gr1D->GetXaxis()->SetTitle("W (GeV/c^{2})");
		m_gr1D->GetXaxis()->SetName("W");
		m_gr1D->GetYaxis()->SetTitle("dL_{#gamma #gamma}/dW(Q^{2},W)");
		m_gr1D->GetYaxis()->SetName("LGamGam");
		m_gr1D->Write();
		
		c->Print(Form("%s",outps.c_str()));

		char systcmd[256];
		sprintf(systcmd,"ps2pdf %s %s",outps.c_str(), outpdf.c_str());
		system(systcmd);
		sprintf(systcmd,"rm %s",outps.c_str());
		system(systcmd);
		
	}
	else{
		string outname=outpath+".root";
		string outps=outpath+".ps";
		string outpdf=outpath+".pdf";
		TFile* outROOT = new TFile(outname.c_str(),"RECREATE");

		m_gr2D = new TGraph2D();
		/*
		int Npt=0;
		for(int i=0;i<vec_limbins[0].size()-1;i++){
				for(int j=0;j<vec_limbins[1].size()-1;j++){
					string key=SSTR(i)+"-"+SSTR(j);
					double Q2=(vec_limbins[0][i]+vec_limbins[0][i+1])/2;
					double W=(vec_limbins[1][j]+vec_limbins[1][j+1])/2;
					double val=m_integral[key];
					double err=m_integralError[key];
					if(val!=val) continue;
					if(err!=err) continue;
					m_gr2D->SetPoint(Npt,Q2,W,val);
					Npt++;
				}
		}
		*/

		int Npt=0;
		for(std::map<string,double>::iterator it = m_integral.begin(); it!=m_integral.end();++it){
					string key=it->first;
					cout << key << endl;
					double Q2=-m_t2[key];
					double W=m_W[key];
					double val=m_integral[key];
					double err=m_integralError[key];
					if(val!=val) continue;
					if(err!=err) continue;
					m_gr2D->SetPoint(Npt,Q2,W,val);
					Npt++;
		}

		m_gr2D->SetName("gr2D");
		m_gr2D->SetTitle("");
		m_gr2D->GetXaxis()->SetTitle("Q^{2} (Gev^{2})");
		m_gr2D->GetXaxis()->SetName("Q2");
		m_gr2D->GetYaxis()->SetTitle("W (GeV/c^{2})");
		m_gr2D->GetYaxis()->SetName("W");
		m_gr2D->GetZaxis()->SetTitle("d^{2}L_{#gamma #gamma}/dQ^{2}dW (Q^{2},W)");
		m_gr2D->GetZaxis()->SetName("LGamGam");

		m_gr2D->Write();

		TH2D* h2D_gr2D=(TH2D*) m_gr2D->GetHistogram();	
		h2D_gr2D->SetName("h2D_gr2D");
		h2D_gr2D->GetXaxis()->SetTitle("Q^{2} (Gev^{2})");
		h2D_gr2D->GetXaxis()->SetName("Q2");
		h2D_gr2D->GetYaxis()->SetTitle("W (GeV/c^{2})");
		h2D_gr2D->GetYaxis()->SetName("W");
		h2D_gr2D->GetZaxis()->SetTitle("d^{2}L_{#gamma #gamma}/dQ^{2}dW (Q^{2},W)");
		h2D_gr2D->GetZaxis()->SetName("LGamGam");
		h2D_gr2D->Write();

		TCanvas *c = new TCanvas();
		m_gr2D->Draw("surf1");
		m_gr2D->GetXaxis()->SetTitleOffset(1.4);
		m_gr2D->GetYaxis()->SetTitleOffset(1.4);
		m_gr2D->GetZaxis()->SetTitleOffset(1.2);
		TGraph2D* grpt = (TGraph2D*) m_gr2D->Clone();
		grpt->Draw("same p0");
		c->SetLogz();
		gPad->SetPhi(-120);
		c->Print(Form("%s",outps.c_str()));

		char systcmd[256];
		sprintf(systcmd,"ps2pdf %s %s",outps.c_str(), outpdf.c_str());
		system(systcmd);
		sprintf(systcmd,"rm %s",outps.c_str());
		system(systcmd);
	}
}

}