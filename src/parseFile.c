#include <string>
#include <vector>
#include <iostream>
#include <fstream>

#include "MyGlobal.cxx"

void parseFile(string fileName){
	ifstream readFile(fileName.c_str());

	if(readFile.is_open()){
		while(readFile.good()){
			string str;
			getline(readFile,str);
			vector<string> vstr=MyGlobal::tokenizeArgs(str);
			for(int k=0;k<vstr.size();k++) cout << vstr[k] << endl;
		}
	}	

}