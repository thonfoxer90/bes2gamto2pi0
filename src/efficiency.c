#include <TROOT.h>
#include <TChain.h>
#include <TRint.h>
#include <TTree.h>
#include <TFile.h>
#include <TStyle.h>
#include <TString.h>
#include <TSelector.h>
#include <TF1.h>
#include <TH2.h>
#include <TH1.h>
#include <THnSparse.h>
#include <TVirtualIndex.h>
#include <iostream>             // std::cout, std::endl
#include <fstream>              // std::ifstream
#include <sstream>  
#include <TPad.h>
#include <TCanvas.h>
#include <TGaxis.h>
#include <TPave.h>
#include <TPaveText.h>
#include <TLatex.h>
#include <TVector3.h>
#include <TLorentzVector.h>
#include <TSystem.h>
#include <cstdlib>
#include <map>
#include <time.h>

#include "MyGlobal.cxx"

int m_UseFittedKinematics=1;

void efficiency(string inputname, string option, string outpath){
TFile* file;
file = TFile::Open(inputname.c_str());

TTree* treeGEN;
TTree* treeREC;

string binningdir;

vector<string> optionSplit = MyGlobal::tokenize(option,' ');
cout << optionSplit.size() << endl;
for(int k=0;k<optionSplit.size();k++){
	vector<string> strsplit = MyGlobal::tokenize(optionSplit[k],'=');
	string arg=strsplit[0];
	string val=strsplit[1];
	if(arg=="BinningDir"){binningdir=val;}
}

string outname=outpath+".root";
TFile* outROOT = new TFile(outname.c_str(),"RECREATE");

THnSparseD* hnDGEN[2]; //0: e-Tagged 1: e+Tagged
THnSparseD* hnDREC[2]; //0: e-Tagged 1: e+Tagged
THnSparseD* hnDEff[2]; //0: e-Tagged 1: e+Tagged


const Int_t nDim = 3;
vector<vector<double> > vec_limbins;
vec_limbins.resize(nDim);
for(int k=0;k<nDim;k++){
		string limbinFullPath;
		string limbinFileName;
		if(k==0) limbinFileName="limbin_Q2.dat";
		if(k==1) limbinFileName="limbin_AbsCThetaSt.dat";
		if(k==2) limbinFileName="limbin_M2Pions.dat";
		limbinFullPath=binningdir+"/"+limbinFileName;
		vec_limbins[k]=MyGlobal::getLimBin(limbinFullPath);
		//for(int i=0;i<vec_limbins[k].size();i++) cout << vec_limbins[k][i] << endl;
		//cout << endl;
}
Int_t nBins[nDim];
Double_t min[nDim];
Double_t max[nDim];
for(int k=0;k<nDim;k++) {
		nBins[k]=vec_limbins[k].size()-1;
		min[k]=vec_limbins[k][0];
		max[k]=vec_limbins[k][vec_limbins[k].size()-1];
	}

for(int i=0;i<2;i++){
		string prefix;
		if(i==0) prefix="hnDeT";
		else if(i==1) prefix="hnDpT";
		string histoname;
		histoname=prefix+"GEN";
		hnDGEN[i]= new THnSparseD(histoname.c_str(),"",nDim,nBins,min, max);
		histoname=prefix+"REC";
		hnDREC[i]= new THnSparseD(histoname.c_str(),"",nDim,nBins,min, max);
		histoname=prefix+"Eff";
		hnDEff[i]= new THnSparseD(histoname.c_str(),"",nDim,nBins,min, max);
		//Define the binning explicitly for variable bin size case
		for(int k=0;k<nDim;k++){
			vector<double> limbin=vec_limbins[k];
			hnDGEN[i]->GetAxis(k)->Set(nBins[k],&limbin[0]);
			hnDREC[i]->GetAxis(k)->Set(nBins[k],&limbin[0]);
			hnDEff[i]->GetAxis(k)->Set(nBins[k],&limbin[0]);
		}	
		hnDGEN[i]->Sumw2();
		hnDREC[i]->Sumw2();
		hnDEff[i]->Sumw2();
		
		hnDGEN[i]->GetAxis(0)->SetTitle("Q^{2} (Gev^{2})");
		hnDGEN[i]->GetAxis(0)->SetName("Q2");
		hnDGEN[i]->GetAxis(1)->SetTitle("|cos #theta^{*}|");
		hnDGEN[i]->GetAxis(1)->SetName("AbsCThetaSt");
		hnDGEN[i]->GetAxis(2)->SetTitle("W (GeV/c^{2})");
		hnDGEN[i]->GetAxis(2)->SetName("M2Pi");
		
		//hnDREC[i]->SetTitle(";Q^{2} (Gev^{2});cos(#theta^{*});M_{#pi#pi} (GeV);");
		hnDREC[i]->GetAxis(0)->SetTitle("Q^{2} (Gev^{2})");
		hnDREC[i]->GetAxis(0)->SetName("Q2");
		hnDREC[i]->GetAxis(1)->SetTitle("|cos #theta^{*}|");
		hnDREC[i]->GetAxis(1)->SetName("AbsCThetaSt");
		hnDREC[i]->GetAxis(2)->SetTitle("W (GeV/c^{2})");
		hnDREC[i]->GetAxis(2)->SetName("M2Pi");
				
		hnDEff[i]->GetAxis(0)->SetTitle("Q^{2} (Gev^{2})");
		hnDEff[i]->GetAxis(0)->SetName("Q2");
		hnDEff[i]->GetAxis(1)->SetTitle("|cos #theta^{*}|");
		hnDEff[i]->GetAxis(1)->SetName("AbsCThetaSt");
		hnDEff[i]->GetAxis(2)->SetTitle("W (GeV/c^{2})");
		hnDEff[i]->GetAxis(2)->SetName("M2Pi");
	}
		
double Q2GEN[2];
double InvM2Pi0GEN;
double CThetaStGEN[2][2];
double PhiStGEN[2][2];

double Q2REC,InvM2Pi0REC,CThetaStREC,PhiStREC;
int QTag;
double Q2,InvM2Pi0,CThetaSt,absCThetaSt;
double Q2GENTagged;

treeREC=(TTree*) file->Get("Tagged");
if(treeREC==NULL) return;
if(m_UseFittedKinematics==0){
	treeREC->SetBranchAddress("Q2",&Q2REC);
	treeREC->SetBranchAddress("2Pi0InvM", &InvM2Pi0REC);
	treeREC->SetBranchAddress("cosThetaStarPi01", &CThetaStREC);
	treeREC->SetBranchAddress("PhiStarPi01", &PhiStREC);
}
else{
	treeREC->SetBranchAddress("Q2Fit",&Q2REC);
	treeREC->SetBranchAddress("2Pi0InvMFit", &InvM2Pi0REC);
	treeREC->SetBranchAddress("cosThetaStarPi0Fit1", &CThetaStREC);
	treeREC->SetBranchAddress("PhiStarPi0Fit1", &PhiStREC);

}	
treeREC->SetBranchAddress("QTag",&QTag);
treeREC->SetBranchAddress("Q2GEN",&Q2GENTagged);

treeGEN=(TTree*) file->Get("AllMCTruth");
treeGEN->SetBranchAddress("2Pi0InvMGEN",&InvM2Pi0GEN);
for(int i=0;i<2;i++){
		string lept;
		if(i==0) lept="e";
		else if(i==1) lept="p";
		string varname;
		varname="Q2"+lept+"GEN";
		treeGEN->SetBranchAddress(varname.c_str(),&Q2GEN[i]);		
		for(int j=0;j<2;j++){
			string pi0;
			if(j==0) pi0="Pi01";
			if(j==1) pi0="Pi02";
			varname=lept+"TcosThetaStar"+pi0+"GEN";
			treeGEN->SetBranchAddress(varname.c_str(),&CThetaStGEN[i][j]);
			varname=lept+"TPhiStar"+pi0+"GEN";
			treeGEN->SetBranchAddress(varname.c_str(),&PhiStGEN[i][j]);
		}
	}

//Fill generated tree
for(int entry=0;entry<treeGEN->GetEntries();entry++){
		treeGEN->GetEntry(entry);
		if (entry % 10000 == 0) cout << "Event counter = " << entry << endl;
		vector<double> binval; binval.resize(nDim);
		for(int i=0;i<2;i++){
			if(i==0) {binval[0]=Q2GEN[0];binval[1]=TMath::Abs(CThetaStGEN[0][0]);}
			else if(i==1) {binval[0]=Q2GEN[1];binval[1]=TMath::Abs(CThetaStGEN[1][0]);}
			binval[2]=InvM2Pi0GEN;			
			hnDGEN[i]->Fill(&(binval[0]));
			}
	}

//Fill Reconstructed tree
for(int entry=0;entry<treeREC->GetEntries();entry++){
		treeREC->GetEntry(entry);
		if (entry % 10000 == 0) cout << "Event counter = " << entry << endl;
		vector<double> binval; binval.resize(nDim);
		binval[0]=Q2REC; binval[1]=TMath::Abs(CThetaStREC);binval[2]=InvM2Pi0REC;
		if(QTag==-1) hnDREC[0]->Fill(&(binval[0]));
		else if (QTag==1) hnDREC[1]->Fill(&(binval[0]));
	}

//Calculation of efficiency
for(int i=0;i<2;i++) {
	hnDEff[i]->Divide(hnDREC[i],hnDGEN[i],1.,1.,"B");
}

//1D efficiencies
vector<vector <TH1D*> > h1DEffs;
vector<TH1D*> h1DEffeTOverEffpT;
h1DEffs.resize(2); //1: e-Tag 2: e+Tag
h1DEffeTOverEffpT.resize(hnDGEN[0]->GetNdimensions());

for(int i=0;i<h1DEffs.size();i++) {h1DEffs[i].resize(hnDGEN[0]->GetNdimensions());}

for(int i=0;i<h1DEffs.size();i++){
	string leptstr;
	string lepttitle;
	if(i==0) {leptstr="eT";lepttitle="e-Tag";}
	else if(i==1) {leptstr="pT";lepttitle="p-Tag";}
	for(int j=0;j<h1DEffs[i].size();j++){
		cout << h1DEffs.size() << " " << h1DEffs[i].size() << endl;
		cout << i << " " << j << endl;
		TH1D* hprojGEN = (TH1D*) hnDGEN[i]->Projection(j);
		TH1D* hprojREC = (TH1D*) hnDREC[i]->Projection(j);		
		if(hprojREC==NULL || hprojGEN==NULL) continue;
		h1DEffs[i][j] = (TH1D*) hprojGEN->Clone();
		h1DEffs[i][j]->Divide(hprojREC,hprojGEN,1,1,"B");
		string hname;
		string Ytitle;
			
		hname=leptstr+"Eff";		
		hname=hname+string(hnDREC[i]->GetAxis(j)->GetName());	
		
		h1DEffs[i][j]->SetName(hname.c_str());
		h1DEffs[i][j]->SetTitle("");
		h1DEffs[i][j]->GetXaxis()->SetTitle(hnDREC[i]->GetAxis(j)->GetTitle());
		Ytitle="#varepsilon_{"+lepttitle+"}";
		h1DEffs[i][j]->GetYaxis()->SetTitle(Ytitle.c_str());
		h1DEffs[i][j]->Write();	
	}
}

for(int k=0;k<h1DEffeTOverEffpT.size();k++){
	h1DEffeTOverEffpT[k]= (TH1D*) h1DEffs[0][k];
	h1DEffeTOverEffpT[k]->Divide(h1DEffs[1][k]);
	
	string hname;
	string Ytitle;
	hname="EffeTOverEffpT"+string(hnDREC[0]->GetAxis(k)->GetName());
	Ytitle=	string(h1DEffs[0][k]->GetYaxis()->GetTitle())+"/"+string(h1DEffs[1][k]->GetYaxis()->GetTitle());	
	h1DEffeTOverEffpT[k]->SetName(hname.c_str());
	h1DEffeTOverEffpT[k]->GetYaxis()->SetTitle(Ytitle.c_str());;
	h1DEffeTOverEffpT[k]->Write();
}

map<vector<int>,TH2D*> map2DGENCThetaM2Pi_Q2;
map<vector<int>,TH2D*> map2DRECCThetaM2Pi_Q2;
map<vector<int>,TH2D*> map2DEffsCThetaM2Pi_Q2;


int binQ2Min = hnDGEN[0]->GetAxis(0)->GetFirst();
int binQ2Max = hnDGEN[0]->GetAxis(0)->GetLast();
	
for(int i=0;i<2;i++){
	string leptstr;
	string leptstrTitle;
	if(i==0) {leptstr="eT";leptstrTitle="e-Tag";}
	else if(i==1) {leptstr="pT";leptstrTitle="p-Tag";}
	
	//(cos theta, M2Pi) distributions in Q2 bins
	for(int binQ2=binQ2Min;binQ2<=binQ2Max;binQ2++){
		string XName; XName=string(hnDREC[i]->GetAxis(1)->GetName());
		string YName; YName=string(hnDREC[i]->GetAxis(2)->GetName());
		
		string XTitle; XTitle=string(hnDREC[i]->GetAxis(1)->GetTitle());
		string YTitle; YTitle=string(hnDREC[i]->GetAxis(2)->GetTitle());
		
		THnSparseD* hnDGEN_Restrict= (THnSparseD*) hnDGEN[i]->Clone();
		hnDGEN_Restrict->GetAxis(0)->SetRange(binQ2,binQ2);
		
		THnSparseD* hnDREC_Restrict= (THnSparseD*) hnDREC[i]->Clone();
		hnDREC_Restrict->GetAxis(0)->SetRange(binQ2,binQ2);
		
		TH2D* hprojCThetaM2PiGEN = (TH2D*) hnDGEN_Restrict->Projection(2,1);
		TH2D* hprojCThetaM2PiREC = (TH2D*) hnDREC_Restrict->Projection(2,1);
		
		double Q2Min=hnDGEN_Restrict->GetAxis(0)->GetBinLowEdge(binQ2);
		double Q2Max=hnDGEN_Restrict->GetAxis(0)->GetBinUpEdge(binQ2);
				
		vector<int> key;key.resize(2);
		key[0]=i;
		key[1]=binQ2-1;
		map2DGENCThetaM2Pi_Q2[key] = (TH2D*) hprojCThetaM2PiGEN->Clone();
		map2DRECCThetaM2Pi_Q2[key] = (TH2D*) hprojCThetaM2PiREC->Clone();
		map2DEffsCThetaM2Pi_Q2[key]= (TH2D*) hprojCThetaM2PiREC->Clone();
		map2DEffsCThetaM2Pi_Q2[key]->Divide(hprojCThetaM2PiREC,hprojCThetaM2PiGEN,1.,1.,"B");
		
		string hname;
		/*
		hname=leptstr+"Eff";
		hname=hname
					+XName+YName
					+"_"+string(hnDREC[i]->GetAxis(0)->GetName())+"_"+MyGlobal::intToStr(binQ2-1);
		*/
		hname="Eff"
					+XName+YName+leptstr
					+"_"+string(hnDREC[i]->GetAxis(0)->GetName())+"_"+MyGlobal::intToStr(binQ2-1);			
		cout << hname << endl;			
		map2DEffsCThetaM2Pi_Q2[key]->SetName(hname.c_str());
		string htitle;
		htitle=MyGlobal::doubleToStr(Q2Min)+"#leq "+ string(hnDGEN_Restrict->GetAxis(0)->GetTitle())+"< "+MyGlobal::doubleToStr(Q2Max)
		+", "+leptstrTitle;
		map2DEffsCThetaM2Pi_Q2[key]->SetTitle(htitle.c_str());
		map2DEffsCThetaM2Pi_Q2[key]->GetXaxis()->SetTitle(XTitle.c_str());
		map2DEffsCThetaM2Pi_Q2[key]->GetYaxis()->SetTitle(YTitle.c_str());
		map2DEffsCThetaM2Pi_Q2[key]->GetZaxis()->SetTitle("#varepsilon");
		map2DEffsCThetaM2Pi_Q2[key]->Write();	
		
		hname=XName+YName+leptstr+"GEN"
					+"_"+string(hnDREC[i]->GetAxis(0)->GetName())+"_"+MyGlobal::intToStr(binQ2-1);
		map2DGENCThetaM2Pi_Q2[key]->SetName(hname.c_str());	
		map2DGENCThetaM2Pi_Q2[key]->SetTitle(htitle.c_str());
		map2DGENCThetaM2Pi_Q2[key]->GetXaxis()->SetTitle(XTitle.c_str());
		map2DGENCThetaM2Pi_Q2[key]->GetYaxis()->SetTitle(YTitle.c_str());
		map2DGENCThetaM2Pi_Q2[key]->Write();		
		
		hname=XName+YName+leptstr+"REC"
					+"_"+string(hnDREC[i]->GetAxis(0)->GetName())+"_"+MyGlobal::intToStr(binQ2-1);
		map2DRECCThetaM2Pi_Q2[key]->SetName(hname.c_str());	
		map2DRECCThetaM2Pi_Q2[key]->SetTitle(htitle.c_str());
		map2DRECCThetaM2Pi_Q2[key]->GetXaxis()->SetTitle(XTitle.c_str());
		map2DRECCThetaM2Pi_Q2[key]->GetYaxis()->SetTitle(YTitle.c_str());
		map2DRECCThetaM2Pi_Q2[key]->Write();					
	}
	
	
}

//Delta eff over eff histograms
vector<TH1D*> hDeltaEffOvrEff;
hDeltaEffOvrEff.resize(2);
for(int i=0;i<hDeltaEffOvrEff.size();i++){
		string prefix;
		if(i==0) prefix="eT";
		else if(i==1) prefix="pT";
		string histoname;
		histoname=prefix+"DeltaEffOverEff";
		hDeltaEffOvrEff[i] = new TH1D(histoname.c_str(),"",512,0.,0.);
		hDeltaEffOvrEff[i]->GetXaxis()->SetTitle("#Delta #varepsilon/#varepsilon");
		
		for(int bin=0;bin<hnDEff[i]->GetNbins();bin++){
			double val = hnDEff[i]->GetBinContent(bin);
			double err = hnDEff[i]->GetBinError(bin);
			hDeltaEffOvrEff[i]->Fill(err/val);
		}
		hDeltaEffOvrEff[i]->Write();
}
	
for(int i=0;i<2;i++) {hnDGEN[i]->Write();hnDREC[i]->Write();hnDEff[i]->Write();}

//outROOT->Write(); 
//outROOT->Close();	

string outps=outpath+".ps";
string outpdf=outpath+".pdf";

gStyle->SetNumberContours(99);
gStyle->SetOptStat(0);
gStyle->SetTitleOffset(1.5, "x");
gStyle->SetTitleOffset(1.5, "y");
gStyle->SetTitleOffset(1.5, "z");
	 
TCanvas *c = new TCanvas();
c->Print(Form("%s[",outps.c_str()));
for (std::map<vector<int>,TH2D*>::iterator it=map2DEffsCThetaM2Pi_Q2.begin(); it!=map2DEffsCThetaM2Pi_Q2.end(); ++it){
	(it->second)->Draw("LEGO2Z");
	(it->second)->GetXaxis()->SetTitleOffset(1.5);
	(it->second)->GetYaxis()->SetTitleOffset(1.5);
	//(it->second)->Draw("SURF3Z");
	//(it->second)->Draw("SURF1");
	c->Print(Form("%s",outps.c_str()));
}
c->Print(Form("%s]",outps.c_str()));

char systcmd[256];
sprintf(systcmd,"ps2pdf %s %s",outps.c_str(), outpdf.c_str());
system(systcmd);
sprintf(systcmd,"rm %s",outps.c_str());
system(systcmd);

c->Clear();

outps=outpath+"_GEN.ps";
outpdf=outpath+"_GEN.pdf";
c->Print(Form("%s[",outps.c_str()));
for (std::map<vector<int>,TH2D*>::iterator it=map2DGENCThetaM2Pi_Q2.begin(); it!=map2DGENCThetaM2Pi_Q2.end(); ++it){
	(it->second)->Draw("LEGO2Z");
	c->Print(Form("%s",outps.c_str()));
}
c->Print(Form("%s]",outps.c_str()));
sprintf(systcmd,"ps2pdf %s %s",outps.c_str(), outpdf.c_str());
system(systcmd);
sprintf(systcmd,"rm %s",outps.c_str());
system(systcmd);

outps=outpath+"_REC.ps";
outpdf=outpath+"_REC.pdf";
c->Print(Form("%s[",outps.c_str()));
for (std::map<vector<int>,TH2D*>::iterator it=map2DRECCThetaM2Pi_Q2.begin(); it!=map2DRECCThetaM2Pi_Q2.end(); ++it){
	(it->second)->Draw("LEGO2Z");
	c->Print(Form("%s",outps.c_str()));
}
c->Print(Form("%s]",outps.c_str()));
sprintf(systcmd,"ps2pdf %s %s",outps.c_str(), outpdf.c_str());
system(systcmd);
sprintf(systcmd,"rm %s",outps.c_str());
system(systcmd);
		
}