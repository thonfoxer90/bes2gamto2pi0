#include <TROOT.h>
#include <TChain.h>
#include <TRint.h>
#include <TTree.h>
#include <TFile.h>
#include <TStyle.h>
#include <TString.h>
#include <TSelector.h>
#include <TF1.h>
#include <TH2.h>
#include <TH1.h>
#include <TF1.h>
#include <THnSparse.h>
#include <TVirtualIndex.h>
#include <iostream>             // std::cout, std::endl
#include <fstream>              // std::ifstream
#include <sstream>  
#include <TPad.h>
#include <TCanvas.h>
#include <TGaxis.h>
#include <TPave.h>
#include <TPaveText.h>
#include <TLatex.h>
#include <TVector3.h>
#include <TLorentzVector.h>
#include <TSystem.h>
#include <cstdlib>
#include <map>
#include <time.h>

#include "MyGlobal.cxx"

void fitPi0InvM(string inputname, string option,string outpath){
TFile* file;
file=TFile::Open(inputname.c_str());
TTree* tree = (TTree*) file->Get("Tagged");

string outps=outpath+".ps";
string outpdf=outpath+".pdf";

TH1D* hPi0InvM[2];
TF1* funcFitPi0[2];
gStyle->SetOptTitle(0);
TCanvas *c = new TCanvas();
c->Divide(2,1);
c->cd(1);
tree->Draw("Pi01InvM>>hPi01InvM(60,0.1,0.16)","","E");
hPi0InvM[0] = (TH1D*) gROOT->FindObject("hPi01InvM");
hPi0InvM[0]->GetXaxis()->SetTitle("M_{#pi^{0}_{1}} (GeV/c^{2})");
hPi0InvM[0]->GetYaxis()->SetTitle(Form("N.evts/%.3f (GeV/c^{2})",MyGlobal::getDeltaX(hPi0InvM[0])));
funcFitPi0[0] = new TF1("funcFitPi01","gaus(0)+pol0(3)",0.1,0.16);
funcFitPi0[0]->SetParameter(1,0.134);
funcFitPi0[0]->SetParameter(2,0.005);
hPi0InvM[0]->Fit(funcFitPi0[0],"","",0.1,0.16);
c->cd(2);
tree->Draw("Pi02InvM>>hPi02InvM(60,0.1,0.16)","","E");
hPi0InvM[1] = (TH1D*) gROOT->FindObject("hPi02InvM");
hPi0InvM[1]->GetXaxis()->SetTitle("M_{#pi^{0}_{2}} (GeV/c^{2})");
hPi0InvM[1]->GetYaxis()->SetTitle(Form("N.evts/%.3f (GeV/c^{2})",MyGlobal::getDeltaX(hPi0InvM[1])));
funcFitPi0[1] = new TF1("funcFitPi02","gaus(0)+pol0(3)",0.1,0.16);
funcFitPi0[1]->SetParameter(1,0.134);
funcFitPi0[1]->SetParameter(2,0.005);
hPi0InvM[1]->Fit(funcFitPi0[1],"","",0.1,0.16);

c->Print(Form("%s",outps.c_str()));

char systcmd[256];
sprintf(systcmd,"ps2pdf %s %s",outps.c_str(), outpdf.c_str());
system(systcmd);
sprintf(systcmd,"rm %s",outps.c_str());
system(systcmd);
}