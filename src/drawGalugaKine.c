void drawGalugaKine(string input, string option, string outpath){

TFile *file = TFile::Open(input.c_str());
TTree* tree = (TTree*) file->Get("AllMCTruth");

string outname=outpath+".root";
string outps=outpath+".ps";
string outpdf=outpath+".pdf";
TFile* outROOT = new TFile(outname.c_str(),"RECREATE");

TCanvas* c = new TCanvas();
tree->Draw("2Pi0InvMGEN>>h2Pi0InvMGEN(200,0.,4.)");

TH1D* h2Pi0InvMGEN = (TH1D*) gROOT->FindObject("h2Pi0InvMGEN");
int nBins= h2Pi0InvMGEN->GetNbinsX();
double xmin=h2Pi0InvMGEN->GetXaxis()->GetXmin();
double xmax=h2Pi0InvMGEN->GetXaxis()->GetXmax();
double delta = (xmax-xmin)/nBins;
char title[256];
if(option=="PipPim"){sprintf(title,";M_{#pi^{+}#pi^{-}} (GeV/c^{2});Events/%.2f GeV/c^{2}",delta);}
else sprintf(title,";W (GeV/c^{2});Events/%.2f GeV/c^{2}",delta);
h2Pi0InvMGEN->SetTitle(title);
c->Print(Form("%s",outps.c_str()));

h2Pi0InvMGEN->Write();
outROOT->Write();

char systcmd[256];
sprintf(systcmd,"ps2pdf %s %s",outps.c_str(), outpdf.c_str());
system(systcmd);
sprintf(systcmd,"rm %s",outps.c_str());
system(systcmd);

}

/*
void MultiDraw(string file1,string file2,string file3, string outpath){

string outname=outpath+".root";
string outps=outpath+".ps";
string outpdf=outpath+".pdf";
TFile* outROOT = new TFile(outname.c_str(),"RECREATE");

vector<TFile*>
}
*/