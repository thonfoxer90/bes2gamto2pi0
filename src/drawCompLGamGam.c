#include <TROOT.h>
#include <TChain.h>
#include <TRint.h>
#include <TTree.h>
#include <TFile.h>
#include <TStyle.h>
#include <TString.h>
#include <TSelector.h>
#include <TF1.h>
#include <TH2.h>
#include <TH1.h>
#include <THnSparse.h>
#include <TGraphErrors.h>
#include <TGraph2D.h>
#include <TVirtualIndex.h>
#include <iostream>             // std::cout, std::endl
#include <fstream>              // std::ifstream
#include <sstream>  
#include <TPad.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TLegendEntry.h>
#include <TGaxis.h>
#include <TPave.h>
#include <TPaveText.h>
#include <TLatex.h>
#include <TVector3.h>
#include <TLorentzVector.h>
#include <TSystem.h>
#include "TRandom.h"
#include <cstdlib>
#include <map>
#include <time.h>

#include "MyGlobal.cxx"

vector<TFile*> m_Files;
vector<TGraphErrors*> m_Graphs;
vector<int> m_ColorIR;
vector<int> m_LineStyle;
vector<int> m_Description;

using namespace std;

void drawCompLGamGam(string inputcard, string outpath){
ifstream readInput(inputcard.c_str());
if(readInput.is_open()){
	while(readInput.good()){
		string strline;
		getline(readInput,strline);

		vector<string> strsplit = MyGlobal::tokenize(strline,':');
		string arg = strsplit[0];
		string val = strsplit[1];

		if(arg=="File") m_Files.push_back(TFile::Open(val.c_str()));
		if(arg=="Graph") m_Graphs.push_back((TGraphErrors*)m_Files.back()->Get(val.c_str()));
		if(arg=="ColorID") m_Graphs.back()->SetLineColor(atoi(val.c_str()));
		if(arg=="LineStyle") m_Graphs.back()->SetLineStyle(atoi(val.c_str()));
		if(arg=="Description") m_Graphs.back()->SetTitle(val.c_str());

	}
}

string outps=outpath+".ps";
string outpdf=outpath+".pdf";

gStyle->SetOptTitle(0);
gStyle->SetOptStat(0);

TCanvas* c = new TCanvas("c","",500,785);

for(int k=0;k<m_Graphs.size();k++){
	if(k==0){m_Graphs[k]->Draw("AL*");
	m_Graphs[k]->SetMinimum(1e-4);
	m_Graphs[k]->SetMaximum(1.);
	m_Graphs[k]->GetXaxis()->SetRangeUser(0.,24.);
	m_Graphs[k]->GetYaxis()->SetTitle("dL_{#gamma#gamma}/dW");
	m_Graphs[k]->GetXaxis()->SetTitle("W(GeV/c^{2})");}
	else m_Graphs[k]->Draw("same L*");
}

gPad->SetLogy();

TLegend* leg = (TLegend*) c->BuildLegend();
leg->SetFillColor(0);

c->Print(outpdf.c_str());
}