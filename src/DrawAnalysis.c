#include <TFile.h>
#include <TDirectory.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TH1.h>
#include <TH2.h>
#include <TLine.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <TKey.h>
#include <TList.h>
#include <TStyle.h>
#include <TROOT.h>

using namespace std;


void DrawAnalysis(string inputlist, string option, string outputname)
{
gROOT->SetBatch();
//gStyle->SetOptStat(0);

ifstream ReadList(inputlist.c_str());
vector<string> filenames;
vector<string> drawopts;
vector<string> descriptions;

string outps=outputname+".ps";
string outpdf=outputname+".pdf";

if(ReadList.is_open())
	{
	while(ReadList.good())
		{
		string filename,drawopt,description;
		getline(ReadList,filename);
		getline(ReadList,drawopt);
		getline(ReadList,description);
		
		filenames.push_back(filename);
		drawopts.push_back(drawopt);
		descriptions.push_back(description);
		}
	}

/*
for(int k=0;k<filenames.size();k++)
	{
	printf("%s %s\n",filenames[k].c_str(),drawopts[k].c_str());
	}
*/


vector <TFile*> files;
for(int k=0;k<filenames.size();k++)
	{
	TFile* file = TFile::Open(filenames[k].c_str());
	if(file==NULL) continue;
	files.push_back(file);
	}


if(option.find("Histo") != string::npos)
	{
		//files[0]->cd("Untagged");
		//files[0]->ls();
		/*
	 TH1D* histo = (TH1D*) files[0]->Get("Untagged/Stage5/1");
	 histo->Draw();
	 */
	 string dirname;
	 if(option.find("Untagged") != string::npos) dirname="Untagged";
	 if(option.find("eTagged") != string::npos) dirname="eTagged";
	 
TDirectory* directory= files[0]->GetDirectory(dirname.c_str());
TIter nextkey(directory->GetListOfKeys());
TKey *key;

TCanvas *c = new TCanvas();
c->Print(Form("%s[",outps.c_str()));
	
		while ((key = (TKey*)nextkey())) {
      TObject *obj = (TObject*)key->ReadObj();
      cout << "Reading: " << obj->GetName() << " " << obj->ClassName()<< endl;				
			if (obj->InheritsFrom("TH1") || obj->InheritsFrom("TH2")) {
				obj->Draw();
       }
			if (obj->InheritsFrom("TDirectory")) {
				TDirectory* subdir = (TDirectory*) obj;
				cout << subdir->GetPath() << endl;
				//Extract path to the histogram							
				const char* fullpath = subdir->GetPath();
				TString* TStrfullpath = new TString(fullpath);
				//Extract directory path only
				TObjArray* strSplit = TStrfullpath->Tokenize(":");
				TString strDirPath=((TObjString *)(strSplit->At(1)))->String();
				cout << strDirPath.Data() << endl;
				TObjArray* strDirPathSplit = strDirPath.Tokenize("/");
				TString strStage=((TObjString *)(strDirPathSplit->Last()))->String();
				
					TIter subnextkey(subdir->GetListOfKeys());
					TKey *subkey;
						while ((subkey = (TKey*)subnextkey())) {
						 TObject *subobj = (TObject*)subkey->ReadObj();
						 if (subobj->InheritsFrom("TH1") || subobj->InheritsFrom("TH2")) {
						 	const char* hname = subobj->GetName();
							const char* htitle = subobj->GetTitle();
							cout << hname << endl;
							if(subobj->InheritsFrom("TH1"))
								{
								TH1D* histo = (TH1D*)subobj;
								if(histo->GetEntries()==0) continue;
								char XTitle[256];
								sprintf(XTitle,"");
								/*
								if(strcmp(htitle,"SumEemc")==0) sprintf(XTitle,"E_{EMC} (GeV)");
								if(strcmp(htitle,"Valid2Combs")==0) sprintf(XTitle,"N_{Valid 2x(2 #gamma)}");
								if(strcmp(htitle,"KinFitChi2")==0) sprintf(XTitle,"#Chi^{2}");
								if(strcmp(htitle,"Pi01InvM")==0) sprintf(XTitle,"M_{#pi^{0}_{1}} without fit (GeV)");
								if(strcmp(htitle,"Pi02InvM")==0) sprintf(XTitle,"M_{#pi^{0}_{2}} without fit (GeV)");
								histo->GetXaxis()->SetTitle(XTitle);
								*/
								histo->SetTitle(strStage.Data());
								histo->Draw();
								}
							if(subobj->InheritsFrom("TH2"))
								{
								TH2D* histo = (TH2D*)subobj;
								if(histo->GetEntries()==0) continue;
								char XTitle[256];
								sprintf(XTitle,"");
								char YTitle[256];
								sprintf(YTitle,"");
								/*
								if(strcmp(htitle,"NumGamvsEnGam")==0) {sprintf(XTitle,"E_{#gamma}"); sprintf(YTitle,"N_{#gamma}");}
								
								histo->GetXaxis()->SetTitle(XTitle);
								histo->GetYaxis()->SetTitle(YTitle);
								*/
								histo->SetTitle(strStage.Data());
								histo->Draw("colz");
								}
							
							//Find the corresponding histograms in other files
							for(int k=1;k<files.size();k++)
								{
								char hpath[256];
								sprintf(hpath,"%s/%s",strDirPath.Data(),hname);
								cout << hpath << endl;
								TObject* objtoadd= files[k]->Get(hpath);
								if(objtoadd==NULL) continue;
								if(objtoadd->InheritsFrom("TH1"))
									{TH1D* histtoadd=(TH1D*)objtoadd;
									histtoadd->SetLineColor(k+1);
									histtoadd->Draw("same");
									}
								/*
								if(objtoadd->InheritsFrom("TH2")) 
									{TH2D* histtoadd=(TH2D*)objtoadd;
									histtoadd->Draw("colz");
									}
			 					else if(objtoadd->InheritsFrom("TH1"))
									{TH1D* histtoadd=(TH1D*)objtoadd;
									histtoadd->Draw("same");
									}
									*/
								}
							c->Print(Form("%s",outps.c_str()));
       				}
						}
       }	 
   }
	} 
	
if(option.find("NTuple") != string::npos)
	{
	gStyle->SetOptTitle(0);
	string treename;
	
	if(option.find("Untagged") != string::npos) treename="Untagged";
	if(option.find("eTagged") != string::npos) treename="eTagged";
	
	vector<TTree*> trees;
	for(int k=0;k<files.size();k++)
		{
		TTree* tree = (TTree*) files[k]->Get(treename.c_str());
		trees.push_back(tree);
		}
	TCanvas *c = new TCanvas();
	c->Print(Form("%s[",outps.c_str()));
	char hname[64];
	char drawcmd[512];
	char selection[512];
	char XTitle[128];
	char YTitle[128];
	
	for(int i=0;i<3;i++)
		{
			//sprintf(selection,"iIseT2pi0==1");
			sprintf(selection,"");

			for(int j=0;j<trees.size();j++)
				{
				sprintf(hname,"h%d",j);
				if(i==0) 
					{
					//sprintf(drawcmd,"deTFit2pi0InvM>>%s(100,0.26,3.)",hname);
					sprintf(drawcmd,"deT2pi0InvM>>%s(100,0.26,3.)",hname);
					sprintf(XTitle,"M_{#pi^{0}#pi^{0}} (GeV)");
					}
				if(i==1) 
					{
					sprintf(drawcmd,"deTFitSumPtCM>>%s(50,0.,2.)",hname);
							sprintf(XTitle,"|#vec{P^{*}_{e-}}+#vec{P^{*}_{#pi^{O}1}}+#vec{P^{*}_{#pi^{O}2}}| (GeV/c)");
					}
				if(i==2) 
					{
					sprintf(drawcmd,"deTFitcosThetaPi01CM2Gam>>%s(50,-1.,1.)",hname);
					sprintf(XTitle,"cos #theta*");
					}
						
				if(j==0) 
					{trees[j]->Draw(drawcmd,selection,drawopts[j].c_str());
					
					}
				else trees[j]->Draw(drawcmd,selection,"same");
				TH1D* histo = (TH1D*) gROOT->FindObject(hname);
				histo->GetXaxis()->SetTitle(XTitle);
				histo->GetXaxis()->SetTitleSize(0.04);
				histo->SetLineColor(j+1);
				}
			c->Print(Form("%s",outps.c_str()));
		}
	
	
	c->Print(Form("%s]",outps.c_str()));
	}

	
//Convert ps file into pdf with ps2pdf (can be properly view by evince)
//On Himster environment, pdf file created by ROOT is corrupted when opened with evince
//To view the pdf : evince filename.pdf
char systcmd[256];
sprintf(systcmd,"ps2pdf %s %s",outps.c_str(), outpdf.c_str());
system(systcmd);
sprintf(systcmd,"rm %s",outps.c_str());
system(systcmd);


return;
}
