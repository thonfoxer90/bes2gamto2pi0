#if !defined(__CINT__) || defined(__MAKECINT__)
#include "/home/bgarillo/boss-6.6.4/Analysis/Bes2Gamto2Pi0/Bes2Gamto2Pi0-00-00-01/RooUnfold-1.1.1/src/RooUnfoldResponse.h"
#include "/home/bgarillo/boss-6.6.4/Analysis/Bes2Gamto2Pi0/Bes2Gamto2Pi0-00-00-01/RooUnfold-1.1.1/src/RooUnfoldBayes.h"
#include "/home/bgarillo/boss-6.6.4/Analysis/Bes2Gamto2Pi0/Bes2Gamto2Pi0-00-00-01/RooUnfold-1.1.1/src/RooUnfoldSvd.h"
#include "/home/bgarillo/boss-6.6.4/Analysis/Bes2Gamto2Pi0/Bes2Gamto2Pi0-00-00-01/RooUnfold-1.1.1/src/RooUnfoldTUnfold.h"
#include "/home/bgarillo/boss-6.6.4/Analysis/Bes2Gamto2Pi0/Bes2Gamto2Pi0-00-00-01/RooUnfold-1.1.1/src/RooUnfoldInvert.h"
#endif
#include <TROOT.h>
#include <TChain.h>
#include <TRint.h>
#include <TTree.h>
#include <TFile.h>
#include <TStyle.h>
#include <TString.h>
#include <TSelector.h>
#include <TF1.h>
#include <TH2.h>
#include <TH1.h>
#include <THnSparse.h>
#include <TGraph2D.h>
#include <TVirtualIndex.h>
#include <iostream>             // std::cout, std::endl
#include <fstream>              // std::ifstream
#include <sstream>  
#include <TPad.h>
#include <TCanvas.h>
#include <TGaxis.h>
#include <TPave.h>
#include <TPaveText.h>
#include <TLatex.h>
#include <TVector3.h>
#include <TLorentzVector.h>
#include <TSystem.h>
#include "TRandom.h"
#include <cstdlib>
#include <map>
#include <time.h>

#include "MyGlobal.cxx"

using namespace std;

string m_FileName("");
string m_NTupleName("");
string m_LimBinPath("");
string m_Algorithm("");
double m_RegPar;
int m_UseFittedKinematics=1;

const Double_t cutdummy= -99999.0;

//==============================================================================
// Gaussian smearing, systematic translation, and variable inefficiency
//==============================================================================

Double_t smear (Double_t xt)
{
  Double_t xeff= 0.3 + (1.0-0.3)/20*(xt+10.0);  // efficiency
  Double_t x= gRandom->Rndm();
  if (x>xeff) return cutdummy;
  Double_t xsmear= gRandom->Gaus(-2.5,0.2);     // bias and smear
  return xt+xsmear;
}

void unfold(string inputcard, string outpath){
//#ifdef __CINT__
//  gSystem->Load("libRooUnfold");
//#endif
//gSystem->Load("/home/bgarillo/boss-6.6.4/Analysis/Bes2Gamto2Pi0/Bes2Gamto2Pi0-00-00-01/libRooUnfold");

ifstream readInput(inputcard.c_str());
if(readInput.is_open()){
	while(readInput.good()){
		string strline;
		getline(readInput,strline);
		
		vector<string> strsplit = MyGlobal::Tokenize(strline,' ');
		for(int k=0;k<strsplit.size();k++){
			vector<string> vecstr = MyGlobal::Tokenize(strsplit[k],'=');	
			string arg = vecstr[0];
			string val = vecstr[1];
			if(arg=="File") m_FileName=val;
			if(arg=="NTuple") m_NTupleName=val;
			if(arg=="Binning") m_LimBinPath=val;
			if(arg=="Algorithm") m_Algorithm=val;
			if(arg=="RegPar") m_RegPar=atof(val.c_str());
			if(arg=="UseFittedKinematics") m_UseFittedKinematics=atoi(val.c_str());
		}
	}
	
}

const Int_t nDim = 1;
	vector<vector<double> > vec_limbins;
	vec_limbins.resize(nDim);
	for(int k=0;k<nDim;k++){
		string limbinFullPath;
		string limbinFileName;
		if(k==0) limbinFileName="limbin_M2Pions.dat";
		limbinFullPath=m_LimBinPath+"/"+limbinFileName;
		vec_limbins[k]=MyGlobal::getLimBin(limbinFullPath);
		//for(int i=0;i<vec_limbins[k].size();i++){
		//	cout << vec_limbins[k][i] << endl;
		//}
	}
	Int_t nBins[nDim];
	Double_t min[nDim];
	Double_t max[nDim];
	
	for(int k=0;k<nDim;k++) {
		nBins[k]=vec_limbins[k].size()-1;
		min[k]=vec_limbins[k][0];
		max[k]=vec_limbins[k][vec_limbins[k].size()-1];
	}

TFile* file = TFile::Open(m_FileName.c_str());
TTree* tree = (TTree*) file->Get(m_NTupleName.c_str());
TTree* treeGEN = (TTree*) file->Get("AllMCTruth");


double WMeas,WTrue;
tree->SetBranchAddress("2Pi0InvMGEN", &WTrue);
if(m_UseFittedKinematics=0) tree->SetBranchAddress("2Pi0InvM", &WMeas);
else 	tree->SetBranchAddress("2Pi0InvMFit", &WMeas);

double WGEN;
treeGEN->SetBranchAddress("2Pi0InvMGEN", &WGEN);
treeGEN->AddFriend(tree); //Connect reconstructed event tree to the generated one

string outROOTname=outpath+".root";
TFile* outROOT = new TFile(outROOTname.c_str(),"RECREATE");

TH1D* hTrue= new TH1D ("true", "Test Truth",    vec_limbins[0].size()-1,&vec_limbins[0][0]);
TH1D* hMeas= new TH1D ("meas", "Test Measured", vec_limbins[0].size()-1,&vec_limbins[0][0]);
TH2D* hTruthvsMeas = new TH2D("hTruthvsMeas",";Measured;Truth",
vec_limbins[0].size()-1,&vec_limbins[0][0],
vec_limbins[0].size()-1,&vec_limbins[0][0]);

TH2D* hMeasvsDeltaMeasTrue = new TH2D("hMeasvsDeltaTrueMeas",";meas-true;meas",50,-0.2,0.2,vec_limbins[0].size()-1,vec_limbins[0].front(),vec_limbins[0].back());
//TH1D* hTrue= new TH1D ("true", "Test Truth",    50,0.26,3.);
//TH1D* hMeas= new TH1D ("meas", "Test Measured", 50,0.26,3.);

//Fill histograms
for(int entry=0;entry<tree->GetEntries();entry++){
	tree->GetEntry(entry);
	hTrue->Fill(WTrue);
	hMeas->Fill(WMeas);
	hTruthvsMeas->Fill(WMeas,WTrue);
	hMeasvsDeltaMeasTrue->Fill(WMeas-WTrue,WMeas);
}

double DeltaMeasTrueMean = hMeasvsDeltaMeasTrue->GetMean();
double DeltaMeasTrueRMS = hMeasvsDeltaMeasTrue->GetRMS();

TCanvas* c = new TCanvas();
hMeasvsDeltaMeasTrue->Draw("colz");	
	
RooUnfoldResponse response (hMeas,hTrue,hTruthvsMeas); //Define the response matrix using hMeas and hTrue to specify the binning
//The values for response matric elements are filled by hTruthvsMeas.

/*
RooUnfoldResponse response (hMeas,hTrue); //Define the response matrix using hMeas and hTrue to specify the binning
//The values for response matric elements are not defined at this point.

cout << "Computation of response matrix from MC sample" << endl;

//for(int entry=0;entry<treeGEN->GetEntries();entry++){
//	if(entry%1000==0) cout << "Generated event #" << entry << endl;
//	treeGEN->GetEntry(entry);
//	if(WGEN==WTrue){
//		response.Fill(WMeas, WGEN);
//		cout << "Fill" << endl;sleep(1);
//	}
//	else{
//		response.Miss(WGEN);
//		cout << "MISS" << endl;
//	}
//}


int entryMatch=0;
for(int entry=0;entry<treeGEN->GetEntries();entry++){
	if(entry%1000==0) cout << "Generated event #" << entry << endl;
	treeGEN->GetEntry(entry);
	bool match=false;
	for(int entryREC=entryMatch;entryREC<tree->GetEntries();entryREC++){
		tree->GetEntry(entryREC);
		//cout << "	Reconstructed event #" << entryREC << endl;
		if(WGEN==WTrue){
			entryMatch=entryREC;
			match=true;
			break;
		}
	}
	if(match==true) {
		response.Fill(WMeas, WGEN); 
		//cout << "FILL" << endl;
	}
	else {
		response.Miss(WGEN); 
		//cout << "MISS" << endl;
	}
}

*/	
cout << "Unfolding" << endl;

TH1D* hReco;
TH2D* hReponse;	
hReponse=(TH2D*)response.Hmeasured();
if(m_Algorithm=="SVD"){
	RooUnfoldSvd     unfold (&response, hMeas, m_RegPar);
	hReco= (TH1D*) unfold.Hreco();
	
}

if(m_Algorithm=="TUnfold"){
	RooUnfoldTUnfold     unfold (&response, hMeas);	
	hReco= (TH1D*) unfold.Hreco();
	//hReponse=(TH2D*)(unfold.response())->Hmeasured();
}

if(m_Algorithm=="RooUnfoldInvert"){
	RooUnfoldInvert     unfold (&response, hMeas);	
	hReco= (TH1D*) unfold.Hreco();
}


TCanvas *c2 = new TCanvas();
hReco->SetLineColor(3);
hReco->Draw("E1");
hTrue->Draw("E1 same");
hMeas->SetLineColor(2);
hMeas->Draw("E1 same");

TCanvas *c3 = new TCanvas();
hReponse->Draw("colz");

}

