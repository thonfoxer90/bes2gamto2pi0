

#include "Bes2Gamto2Pi0/Bes2Gamto2Pi0.h"
#include <sstream>

#define SSTR( x ) dynamic_cast< std::ostringstream & >( \
            ( std::ostringstream() << std::dec << x ) ).str()

using namespace Event;

///////////////////////////////////////////////////////////////////////////
//
//Analysis for e+ e- -> e+ e- pi0 pi0 at BES-III
//Author: Brice Garillon, Institut fuer Kernphysik, Uni Mainz, Germany 
//
///////////////////////////////////////////////////////////////////////////

//----------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------
// class Bes2Gamto2Pi0
//----------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------
Bes2Gamto2Pi0::Bes2Gamto2Pi0(const std::string& name, ISvcLocator* pSvcLocator) :
   Algorithm(name, pSvcLocator)
//----------------------------------------------------------------------------------------------------------
{
  //Declare the properties
	//declareProperty("UntaggedAnalysis", m_bDoUntagged = true);
	//declareProperty("TaggedAnalysis", m_bDoTagged = true);
  //declareProperty("MonteCarloAnalysis", m_MC_Analysis = false);
  //m_dCMSEnergy         = 3.773;       // psi(3770)
	m_dMasselec					 = 511e-6;
  m_dMassPi0           = 0.134976;    // PDG 2010
  m_dMassPiCharged     = 0.139570;    // PDG 2010
	m_dCrossingAngle		 = 0.011;
  m_dEoPcut            = 1.2;//0.8;
  m_dPi0GcutHigh       = 2.5;//1.4;
  m_dPhEnergyCutBarrel = 0.025;
  m_dPhEnergyCutEndCap = 0.05;
  m_dMaxPi0Combs       = 20;
  //m_dMaxGoodPh         = 15;
	m_dMaxGoodPh         = 9;
	//m_dMaxGoodPh         = 4;
  m_dCylRad            = 1;//2.5;  // for point of closest approach
  m_dCylHeight         = 10;//15.0; // for point of closest approach
  m_dPi0MassCutLow     = 0.10;
  m_dPi0MassCutHigh    = 0.16;
  m_dCosThetaMax       = 0.92;
  m_dCosThetaBarrel    = 0.83;
  m_dMaxTime           = 14;  // *50ns
  //m_lvBoost            = HepLorentzVector(0.0415,0,0,m_dCMSEnergy);
  m_lvBoost            = HepLorentzVector(m_dCMSEnergy*sin(m_dCrossingAngle),0,0,m_dCMSEnergy);
	m_bKalFit						 = true;
	//m_bKalFit						 = false;
  //m_dChisqMaxValue     = 400;
	m_dChisqMaxValue     = 30;
	//m_dUSumPtMax				 = 0.05;
	m_dUSumPtMax				 = 0.1;
	m_dUSumEemcMin			 = 0.5;
	//m_dUSumEemcMax			 = 4.5;
	m_dUSumEemcMax			 = 3.5;
  m_bMCrecorded        = false;
	
	//m_bDoUntagged				 = true;
	m_bDoUntagged				 = false;
	m_bDoTagged				 = true;
	//m_bWriteNt2G				 = true;
	m_bWriteNt2G				 = false;
	m_bWriteNt2x2G			 = false;
	//m_bWriteNt2x2G			 = false;
	
	
}
//----------------------------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------------------------
StatusCode Bes2Gamto2Pi0::initialize()
//----------------------------------------------------------------------------------------------------------
{
    cout << "initialize begins" << endl;
    MsgStream log(msgSvc(), name());
    log << MSG::INFO << "in initialize()" << endreq;

		m_iAll        = 0;
    m_iUPreSkip    = 0;
    m_iUPhotonSkip = 0;
    m_iUPi0Skip    = 0;
    m_iUFitSkip   = 0;
		m_iUPreKept		=	0;
		m_iUPhotonKept	=	0;
		m_iUPi0kept		=	0;
		m_iUFitKept		=	0;
    m_iAcceptedU  = 0;
		
    m_KalKinFit  = KalmanKinematicFit::instance();
    m_Vtxfit     = VertexFit::instance();
    m_PID        = ParticleID::instance();
		
		
		
    //Initialize ROOT trees and declare Ntuples
		//All MC Truth
		NTuplePtr allmctruth(ntupleSvc(), "FILE1/AllMCTruth");
		
		if(allmctruth){m_NAmcTruth=allmctruth;}
		else{m_NAmcTruth = ntupleSvc()->book("FILE1/AllMCTruth",CLID_ColumnWiseTuple,"All Monte Carlo-Truth");}
		
		if(m_NAmcTruth){
			m_NAmcTruth->addItem("AmcNpart",m_iAmcNpart,0,1024);			
			m_NAmcTruth->addItem("AmcID",m_iAmcNpart,m_adAmcID);	
			m_NAmcTruth->addItem("AmcPrimary",m_iAmcNpart,m_iAmcPrimary);
			m_NAmcTruth->addItem("AmcDecayGenerator",m_iAmcNpart,m_iAmcDecayGenerator);
			m_NAmcTruth->addItem("AmcMother",m_iAmcNpart,m_iAmcMother);
			m_NAmcTruth->addItem("AmcP",m_iAmcNpart,m_adAmcP);	
			m_NAmcTruth->addItem("AmcTheta",m_iAmcNpart,m_adAmcTheta);	
			m_NAmcTruth->addItem("AmcPhi",m_iAmcNpart,m_adAmcPhi);
			m_NAmcTruth->addItem("AmcE",m_iAmcNpart,m_adAmcE);
			
			m_NAmcTruth->addItem("Amc2Pi0InvM",m_dAmc2Pi0InvM);	
			m_NAmcTruth->addItem("AmcUSumPt2Pi0",m_dAmcUSumPt2Pi0);
			m_NAmcTruth->addItem("AmceTSumPt2Pi0",m_dAmceTSumPt2Pi0);
			m_NAmcTruth->addItem("AmcpTSumPt2Pi0",m_dAmcpTSumPt2Pi0);
			m_NAmcTruth->addItem("AmceTRGam",m_dAmceTRGam);
						
			}
		else {
      log << MSG::ERROR << "  Cannot book N-tuple1 MC truth: " << long(m_NAmcTruth) << endreq;
      return StatusCode::FAILURE;
   	}
		
		int hnum;
		double xmin,xmax,ymin,ymax;
		double deltax,deltay;
		int nbinsx,nbinsy;
		//All MC-truth
		m_haMCNumEvt = histoSvc()-> book("MCTruth/","NumEvt","Number of events ; ;",1,0.,1.);
		
		//Untagged
		m_hU2CombswValidFit=	histoSvc()->book("Untagged/","2CombswValidFit","Number of (2#gamma) pairs with valid kinematic fit",30,0.,30.);
		//m_maphU2CombswSharedGammas =	histoSvc()->book("Untagged/","2CombswSharedGammas","Number of (2#gamma) pairs with shared photons",30,0.,30.);
		
		m_h2G1vs2G2InvM =	histoSvc()->book("Untagged/","m_h2G1vs2G2InvM","M_(2#gamma)1 vs M_(2#gamma)2",100,0.,0.5,100,0.,0.5);
		
		for(int i=0;i<8;i++){
		
		m_maphUNumEvt[i]	=	histoSvc()->book(Form("Untagged/Stage%i",i),"NumEvt","Number of events ; ;",1,0.,1.);	
		m_maphUNNeutral[i]	=	histoSvc()->book(Form("Untagged/Stage%i",i),"NNeutral",";N_{Neutral}",100,0.,100.);	
		m_maphUSumEemc[i]	=	MakeIHistogram1D(Form("Untagged/Stage%i",i),"SumEemc","Total Energy sum in EMC ; E_{EMC} (GeV);",0.04,0.,6.);
		m_maphUNGoodGamma[i] = histoSvc()->book(Form("Untagged/Stage%i",i),"NGoodGamma","; N_{#gamma} ;",100,0.,100.);
		m_maphUEGam[i] = histoSvc()->book(Form("Untagged/Stage%i",i),"EGam","; E_{#gamma} (GeV)",256,0.,5.);
		m_maphUPtGam[i] = histoSvc()->book(Form("Untagged/Stage%i",i),"PtGam","; P_{t,#gamma} (GeV)",256,0.,5.);
		
		if(i<=2){			
			m_maphUNumGamvsEnGam[i] = histoSvc()->book(Form("Untagged/Stage%i",i),"NumGamvsEnGam","; E_{#gamma} (GeV); #gamma number",256,0.,5.,50,0,50);			
		}
		/*
		if(i==1){
			m_maphU2GammasInvM[i] = histoSvc()->book(Form("Untagged/Stage%i",i),"2GammasInvM","Total Energy sum in EMC ; E_{EMC} (GeV);",500,0.,5.);
		}
		*/
		if(i>=1){
		m_maphU2GamvsEn2Gam[i] = histoSvc()->book(Form("Untagged/Stage%i",i),"2GamvsEn2Gam","; E_{2#gamma} (GeV); #gamma_{1} index",256,0.,5.,50,0,50);
		m_maphU2GEAsym[i]=	histoSvc()->book(Form("Untagged/Stage%i",i),"EAsym2G","; E_{Asym (2#gamma)1}",100,0.,2.);		
		m_maphU2GCThetaHelicity[i]=	histoSvc()->book(Form("Untagged/Stage%i",i),"2GCThetaHelicity","; cos #theta_{H}",100,-1.,1.);
		m_maphU2GCThetaHelicityvsInvM[i] = histoSvc()->book(Form("Untagged/Stage%i",i),"2GCThetaHelicityvsInvM","; M_{2#gamma} (GeV);cos #theta_{H}",100.,0.,1.,100,-1.,1.);
		m_maphU2GOpeningAngle[i]=	MakeIHistogram1D(Form("Untagged/Stage%i",i),"2GOpeningAngle","; #alpha_{#gamma #gamma}",0.01,0.,TMath::Pi());
		m_maphU2GDeltaTheta[i]=	MakeIHistogram1D(Form("Untagged/Stage%i",i),"2GDeltaTheta","; #Delta#theta_{#gamma #gamma}",0.01,-TMath::Pi(),TMath::Pi());
		m_maphU2GDeltaPhi[i]=	MakeIHistogram1D(Form("Untagged/Stage%i",i),"2GDeltaPhi","; #Delta#phi_{#gamma #gamma}",0.01,-TMath::Pi(),TMath::Pi());
		m_maphU2GDeltaThetavs2GOpeningAngle[i]= MakeIHistogram2D(
																								Form("Untagged/Stage%i",i),
																								"2GDeltaThetavs2GOpeningAngle",
																								";#alpha_{#gamma #gamma};#Delta#theta_{#gamma #gamma}",
																								0.01,0.,TMath::Pi(),0.01,-TMath::Pi(),TMath::Pi()
																								)
																								;
		m_maphU2GDeltaPhivs2GOpeningAngle[i]= MakeIHistogram2D(
																								Form("Untagged/Stage%i",i),
																								"2GDeltaPhivs2GOpeningAngle",
																								";#alpha_{#gamma #gamma};#Delta#phi_{#gamma #gamma}",
																								0.01,0.,TMath::Pi(),0.01,-TMath::Pi(),TMath::Pi()
																								)
																								;
		m_maphU2GDeltaPhivs2GDeltaTheta[i]= MakeIHistogram2D(
																								Form("Untagged/Stage%i",i),
																								"2GDeltaPhivs2GDeltaTheta",
																								";#Delta#theta_{#gamma #gamma};#Delta#phi_{#gamma #gamma}",
																								0.01,-TMath::Pi(),TMath::Pi(),0.01,-TMath::Pi(),TMath::Pi()
																								)
																								;																						
			m_maphU2GammasInvM[i] =	histoSvc()->book(Form("Untagged/Stage%i",i),"2GammasInvM"," ;M_{2#gamma} (GeV)",500,0.,5.);	
		}
		
		if(i>=2){
			m_maphU2CombswSharedGammas[i] =	histoSvc()->book(Form("Untagged/Stage%i",i),"2CombswSharedGammas","Number of (2#gamma) pairs with shared photons; N_{2#gamma with shared #gamma}",30,0.,30.);
			m_maphUValid2Combs[i]	=	histoSvc()->book(Form("Untagged/Stage%i",i),"Valid2Combs"," ; N_{Valid 2x(2 #gamma)};",30,0.,30.);
		}
		
		if(i>=1 && i<=3){
		m_maphU2G2vs2G1InvM[i] =	MakeIHistogram2D(Form("Untagged/Stage%i",i),"2G1vs2G2InvM","M_{2#gamma}1 vs M_{2#gamma}2; M_{(2#gamma)1} (GeV); M_{(2#gamma)2} (GeV)",0.005,0.,1.,0.005,0.,1.);
		}
		
		if(i==3){
			m_maphU2G1InvM[i] = MakeIHistogram1D(Form("Untagged/Stage%i",i),"2G1InvM",";M_{(2#gamma)1} (GeV)",0.0005,0.1,0.16);
			m_maphU2G2InvM[i] = MakeIHistogram1D(Form("Untagged/Stage%i",i),"2G2InvM",";M_{(2#gamma)2} (GeV)",0.0005,0.1,0.16);
			m_maphUEAsym2G1[i] = histoSvc()->book(Form("Untagged/Stage%i",i),"EAsym2G1","; E_{Asym (2#gamma)1}",100,0.,2.);
			m_maphUEAsym2G2[i] = histoSvc()->book(Form("Untagged/Stage%i",i),"EAsym2G2","; E_{Asym (2#gamma)2}",100,0.,2.);
			
			m_maphU2x2GMissM[i]=	MakeIHistogram1D(Form("Untagged/Stage%i",i),"2x2GMissM","; Mm[(2#gamma)_{1}(2#gamma)_{2}X] (GeV)",0.02,0.,4.);
			m_maphU2x2GMissE[i]=	MakeIHistogram1D(Form("Untagged/Stage%i",i),"2x2GMissE","; E[(2#gamma)_{1}(2#gamma)_{2}2X] (GeV)",0.02,0.,10.);
			m_maphU2x2GDeltaEemcE4Gam[i]	=	MakeIHistogram1D(Form("Untagged/Stage%i",i),"2x2GDeltaEemcE4Gam","; E_{EMC}-E_{4#gamma} (GeV);",0.04,-1.,6.);
			m_maphU2x2GCTheta2G1HelicityG1[i]= histoSvc()->book(Form("Untagged/Stage%i",i),"2x2GCTheta2G1HelicityG1","; cos #theta_{H (2#gamma)1}",100,-1.,1.);
			m_maphU2x2GCTheta2G2HelicityG1[i]= histoSvc()->book(Form("Untagged/Stage%i",i),"2x2GCTheta2G2HelicityG1","; cos #theta_{H (2#gamma)1}",100,-1.,1.);
			m_maphU2x2GInvM[i] = MakeIHistogram1D(Form("Untagged/Stage%i",i),"2x2GInvM","; M_{2x(2#gamma)} (GeV)",0.02,0.,4.);		
			m_maphU2x2GSumPt[i]	=	MakeIHistogram1D(Form("Untagged/Stage%i",i),"2x2GSumPt",";|#vec{P^{*}_{(2#gamma)1}}+#vec{P^{*}_{(2#gamma)2}}| (GeV/c)",0.01,0.,5.);
			m_maphU2x2GcosTheta2G1CM2Gam[i]	=	histoSvc()->book(Form("Untagged/Stage%i",i),"2x2GcosTheta2G1CM2Gam",";cos #theta*_{(2#gamma)1}",100,-1.,1.);
		}
		
		if(i>=3 && i<=4){
			m_maphUKinFitChi2[i] = histoSvc()->book(Form("Untagged/Stage%i",i),"KinFitChi2","Total fit; #Chi^{2}",2000,0.,2000.);
		}			
		
		if(i>=4){	
			m_maphUPi01InvM[i] = MakeIHistogram1D(Form("Untagged/Stage%i",i),"Pi01InvM","Unfitted ;M_{#pi^{0}1}^{Unfitted} (GeV)",0.0005,0.1,0.16);
			m_maphUPi02InvM[i] = MakeIHistogram1D(Form("Untagged/Stage%i",i),"Pi02InvM","Unfitted ;M_{#pi^{0}2}^{Unfitted} (GeV)",0.0005,0.1,0.16);
			m_maphUFitPi01InvM[i] = MakeIHistogram1D(Form("Untagged/Stage%i",i),"FitPi01InvM","Fitted ;M_{#pi^{0}1} (GeV)",0.0005,0.1,0.16);
			m_maphUFitPi02InvM[i] = MakeIHistogram1D(Form("Untagged/Stage%i",i),"FitPi02InvM","Fitted ;M_{#pi^{0}2} (GeV)",0.0005,0.1,0.16);
			m_maphU2Pi0InvM[i] = MakeIHistogram1D(Form("Untagged/Stage%i",i),"2Pi0InvM","Unfitted ; M_{#pi^{0}#pi^{0}}^{Unfitted} (GeV)",0.02,0.,4.);
			m_maphUFit2Pi0InvM[i] = MakeIHistogram1D(Form("Untagged/Stage%i",i),"2FitPi0InvM","Fit ; M_{#pi^{0}#pi^{0}} (GeV)",0.02,0.,4.);
			m_maphUFit2Pi0InvMvs2Pi0InvM[i] = histoSvc()->book(Form("Untagged/Stage%i",i),"Fit2Pi0InvMvs2Pi0InvM",";M_{#pi^{0}#pi^{0}}^{Unfitted} (GeV);M_{#pi^{0}#pi^{0}}^{Fit} (GeV)",400,0.,4.,400,0.,4.);
			m_maphUFitEAsymPi01[i] = histoSvc()->book(Form("Untagged/Stage%i",i),"FitEAsymPi01","; E_{Asym #pi^{0}1}",100,0.,2.);
			m_maphUFitEAsymPi02[i] = histoSvc()->book(Form("Untagged/Stage%i",i),"FitEAsymPi02","; E_{Asym #pi^{0}2}",100,0.,2.);
			m_maphUSumPt[i]	=	MakeIHistogram1D(Form("Untagged/Stage%i",i),"SumPt",";|#vec{P^{*}_{#pi^{O}1}}+#vec{P^{*}_{#pi^{O}2}}|^{Unfitted} (GeV/c)",0.01,0.,5.);
			m_maphUFitSumPt[i]	=	MakeIHistogram1D(Form("Untagged/Stage%i",i),"FitSumPt",";|#vec{P^{*}_{#pi^{O}1}}+#vec{P^{*}_{#pi^{O}2}}| (GeV/c)",0.01,0.,5.);	
			m_maphUcosThetaPi01CM2Gam[i]	=	histoSvc()->book(Form("Untagged/Stage%i",i),"cosThetaPi01CM2Gam",";cos #theta*",100,-1.,1.);	
			m_maphUFitcosThetaPi01CM2Gam[i]	=	histoSvc()->book(Form("Untagged/Stage%i",i),"FitcosThetaPi01CM2Gam",";cos #theta*",100,-1.,1.);
			
			m_maphUMissM[i]	=	MakeIHistogram1D(Form("Untagged/Stage%i",i),"MissM","; Mm[#pi^{0}#pi^{0}X] (GeV)",0.02,0.,4.);
			m_maphUMissE[i]	=	MakeIHistogram1D(Form("Untagged/Stage%i",i),"MissE","; E[#pi^{0}#pi^{0}X] (GeV)",0.02,0.,10.);
			m_maphUDeltaEemcE4Gam[i]	=	MakeIHistogram1D(Form("Untagged/Stage%i",i),"DeltaEemcE4Gam","; E_{EMC}-E_{4#gamma} (GeV);",0.04,-1.,6.);
			m_maphUCThetaHelicity[i]=	histoSvc()->book(Form("Untagged/Stage%i",i),"CThetaHelicity","; cos #theta_{H}",100,-1.,1.);
			m_maphUFitCThetaHelicity[i]=	histoSvc()->book(Form("Untagged/Stage%i",i),"FitCThetaHelicity","; cos #theta_{H}",100,-1.,1.);
		}	
			
		}
		
		NTuplePtr ut(ntupleSvc(), "FILE1/Untagged");
		
		if   ( ut ) {m_NUntagged = ut;}
    else {m_NUntagged = ntupleSvc()->book("FILE1/Untagged", CLID_ColumnWiseTuple, "untagged");}
		
		if ( m_NUntagged ) {
     m_NUntagged->addItem("URunNumber"    ,m_dURunNumber);
     m_NUntagged->addItem("UEvtNumber"    ,m_dUEvtNumber);
     m_NUntagged->addItem("UCMSEnergy"    ,m_dUCMSEnergy);
     m_NUntagged->addItem("iUAnzGoodP"    ,m_iUAnzGoodP);
     m_NUntagged->addItem("iUAnzGoodM"    ,m_iUAnzGoodM);
     m_NUntagged->addItem("iUAnzGoodGamma",m_iUAnzGoodGamma);
     m_NUntagged->addItem("iUAnzPi0Tracks",m_iUAnzPi0Tracks);
     m_NUntagged->addItem("iUAnz2Combs"  ,m_iUAnz2Combs);
     m_NUntagged->addItem("iUAnz3Combs"  ,m_iUAnz3Combs);
     m_NUntagged->addItem("UTotalChi2"   ,m_dUTotalChi2);
     m_NUntagged->addItem("UPi01Chi2"    ,m_dUPi01Chi2);
     m_NUntagged->addItem("UPi02Chi2"    ,m_dUPi02Chi2);
     m_NUntagged->addItem("iUmcTruePi0"  ,m_iUmcTruePi0);
     m_NUntagged->addItem("iUmcTrueFSR"  ,m_iUmcTrueFSR);
     m_NUntagged->addItem("iUmcTrueGammaFromJpsi"  ,m_iUmcTrueGammaFromJpsi);
     m_NUntagged->addItem("iUmcTrueOtherGamma"  ,m_iUmcTrueOtherGamma);
     m_NUntagged->addItem("iUmcTrueOther" ,m_iUmcTrueOther);
     m_UPi01    .AttachToNtuple(m_NUntagged,"UPi01");
     m_UPi02    .AttachToNtuple(m_NUntagged,"UPi02");
		 m_UPi01CM    .AttachToNtuple(m_NUntagged,"UPi01CM");
     m_UPi02CM    .AttachToNtuple(m_NUntagged,"UPi02CM");
     m_UFitPi01    .AttachToNtuple(m_NUntagged,"UFitPi01");
     m_UFitPi02    .AttachToNtuple(m_NUntagged,"UFitPi02");
     //for (int a=0;a<GAMMAS;a++) {m_UGammas   [a].AttachToNtuple(m_NUntagged,"UG"   +SSTR(a)+"emc",true); m_NUntagged->addItem("ClosestTr"+SSTR(a),m_dUClosestTrack[a]);}
		 for (int a=0;a<GAMMAS;a++) {m_UGammas   [a].AttachToNtuple(m_NUntagged,"UG"   +SSTR(a)+"emc",true);}
     for (int a=0;a<5;a++)        {m_UFitGammas[a].AttachToNtuple(m_NUntagged,"UFitG"+SSTR(a),true);}
     m_NUntagged->addItem("UVtxChi2"  ,m_dUVtxChi2);
     m_NUntagged->addItem("VtxPosX"   ,m_dUVtxPosX);
     m_NUntagged->addItem("VtxPosY"   ,m_dUVtxPosY);
     m_NUntagged->addItem("VtxPosZ"   ,m_dUVtxPosZ);
     m_NUntagged->addItem("IPposX"    ,m_dUIPposX);
     m_NUntagged->addItem("IPposY"    ,m_dUIPposY);
     m_NUntagged->addItem("IPposZ"    ,m_dUIPposZ);
		 m_NUntagged->addItem("dUPi01InvM"    ,m_dUPi01InvM);
		 m_NUntagged->addItem("dUFitPi01InvM"    ,m_dUFitPi01InvM);
		 m_NUntagged->addItem("dUPi02InvM"    ,m_dUPi02InvM);
		 m_NUntagged->addItem("dUFitPi02InvM"    ,m_dUFitPi02InvM);
		 m_NUntagged->addItem("dUMissMass"    ,m_dUMissMass);
		 m_NUntagged->addItem("dUMissE"    ,m_dUMissE);
		 m_NUntagged->addItem("dU2Pi0InvM"    ,m_dU2Pi0InvM);
		 m_NUntagged->addItem("dUFit2Pi0InvM"    ,m_dUFit2Pi0InvM);		 
		 m_NUntagged->addItem("dUEAsymPi01"    ,m_dUEAsymPi01);
		 m_NUntagged->addItem("dUFitEAsymPi01"    ,m_dUFitEAsymPi01);
		 m_NUntagged->addItem("dUEAsymPi02"    ,m_dUEAsymPi02);
		 m_NUntagged->addItem("dUFitEAsymPi02"    ,m_dUFitEAsymPi02);			 
		 m_NUntagged->addItem("dUSumPtCM"    ,m_dUSumPtCM);
		 m_NUntagged->addItem("dUFitSumPtCM"    ,m_dUFitSumPtCM);
		 m_NUntagged->addItem("dUPCMbeamelecpositron"    ,m_dUPCMbeamelecpositron);
		 m_NUntagged->addItem("dUPbeamelecpositron"    ,m_dUPbeamelecpositron);
		 m_NUntagged->addItem("dUPi01CMPx"    ,m_dUPi01CMPx);
		 m_NUntagged->addItem("dUPi01CMPy"    ,m_dUPi01CMPy);
		 m_NUntagged->addItem("dUPi01CMPz"    ,m_dUPi01CMPz);
		 m_NUntagged->addItem("dUPi01CMPt"    ,m_dUPi01CMPt);
		 m_NUntagged->addItem("dUPi02CMPx"    ,m_dUPi02CMPx);
		 m_NUntagged->addItem("dUPi02CMPy"    ,m_dUPi02CMPy);
		 m_NUntagged->addItem("dUPi02CMPz"    ,m_dUPi02CMPz);
		 m_NUntagged->addItem("dUPi02CMPt"    ,m_dUPi02CMPt);
		 m_NUntagged->addItem("dUSumEemc"    ,m_dUSumEemc);
		 m_NUntagged->addItem("dUDeltaEemcE4Gam"    ,m_dUDeltaEemcE4Gam);
		 m_NUntagged->addItem("dUcosThetaPi01CM2Gam"    ,m_dUcosThetaPi01CM2Gam);
		 m_NUntagged->addItem("dUFitcosThetaPi01CM2Gam"    ,m_dUFitcosThetaPi01CM2Gam);
		 m_NUntagged->addItem("dUCThetaHelicity"    ,m_dUCThetaHelicity);
		 m_NUntagged->addItem("dUFitCThetaHelicity"    ,m_dUFitCThetaHelicity);
		 m_NUntagged->addItem("iIsU2Pi0"    ,m_iIsU2Pi0);
		 m_NUntagged->addItem("iUNPi0",m_iUNPi0,0,2);	
		 m_NUntagged->addItem("dU2GOpeningAngle",m_iUNPi0,m_dU2GOpeningAngle);	
		 m_NUntagged->addItem("dU2GDeltaTheta",m_iUNPi0,m_dU2GDeltaTheta);	
		 m_NUntagged->addItem("dU2GDeltaPhi",m_iUNPi0,m_dU2GDeltaPhi);	
  }
   else {
      log << MSG::ERROR << "  Cannot book N-tuple1 untagged: " << long(m_NUntagged) << endreq;
      return StatusCode::FAILURE;
   }
	 	 
	 //Electron Tagged
	 m_hTDist	=	histoSvc()->book("Tagged","Dist","Distance to IP - Charged Tracks",128,0.,50.);
		m_hTCylRad	=	histoSvc()->book("Tagged","CylRad","Radial distance of closest approach to IP - Charged Tracks; |dr| (cm)",128,0.,10.);		
		m_hTCylHeight	=	histoSvc()->book("Tagged","CylHeight","Axial distance of closest approach to IP - Charged Tracks; |dz| (cm) ",128,0.,50.);
		m_hTPChargedTracks	=	histoSvc()->book("Tagged","PChargedTracks","Momentum - Charged Tracks; P (GeV/c)",128,0.,6.);
		m_hTEtotOvrPvsP	=	histoSvc()->book("Tagged","EtotOvrPvsP","Momentum - Charged Tracks and valid EMC shower; P (GeV/c)",128,0.,6.,128,0.,1.5);
	
	for(int k=0;k<3;k++)
		{
		string tagType;
		if(k==0) tagType="Tagged";
		if(k==1) tagType="eTagged";
		if(k==2) tagType="pTagged";
		
		for(int i=0;i<12;i++)
			{
			vector<int> key; key.resize(2);
			key[0]=k; key[1]=i;
		
			m_maphTNumEvt[key]	=	histoSvc()->book(Form("%s/Stage%i",tagType.c_str(),i),"NumEvt","Number of events ; ;",1,0.,1.);
			m_maphTNNeutral[key]	=	histoSvc()->book(Form("%s/Stage%i",tagType.c_str(),i),"NNeutral",";N_{Neutral}",100,0.,100.);
			m_maphTNCharged[key]	=	histoSvc()->book(Form("%s/Stage%i",tagType.c_str(),i),"NCharged",";N_{Charged}",100,0.,100.);
			
			if(i<=1){
			m_maphTEtotOvrPvsP[key]	=	histoSvc()->book(Form("%s/Stage%i",tagType.c_str(),i),"EtotOvrPvsP",";P (GeV/c); E_{tot}/P",128,0.,6.,128,0.,1.5);			
			}
			
			if(i>=2)
				{
				m_maphTSumEemc[key]	=	MakeIHistogram1D(Form("%s/Stage%i",tagType.c_str(),i),"SumEemc","Total Energy sum in EMC ; E_{EMC} (GeV);",0.04,0.,6.);
				m_maphTNGoodGamma[key] = histoSvc()->book(Form("%s/Stage%i",tagType.c_str(),i),"NGoodGamma","; N_{#gamma} ;",100,0.,100.);
				
				m_maphT2GCThetaMiss[key]=	histoSvc()->book(Form("%s/Stage%i",tagType.c_str(),i),"2GCThetaMiss","; cos #theta_{Miss}",100,-1.,1.);
		m_maphT2GEAsym[key]=	histoSvc()->book(Form("%s/Stage%i",tagType.c_str(),i),"EAsym2G","; E_{Asym (2#gamma)1}",100,0.,2.);		
		m_maphT2GCThetaHelicity[key]=	histoSvc()->book(Form("%s/Stage%i",tagType.c_str(),i),"2GCThetaHelicity","; cos #theta_{H}",100,-1.,1.);
		m_maphT2GCThetaHelicityvsInvM[key] = histoSvc()->book(Form("%s/Stage%i",tagType.c_str(),i),"2GCThetaHelicityvsInvM","; M_{2#gamma} (GeV);cos #theta_{H}",100.,0.,1.,100,-1.,1.);
		m_maphT2GOpeningAngle[key]=	MakeIHistogram1D(Form("%s/Stage%i",tagType.c_str(),i),"2GOpeningAngle","; #alpha_{#gamma #gamma}",0.01,0.,TMath::Pi());
		m_maphT2GDeltaTheta[key]=	MakeIHistogram1D(Form("%s/Stage%i",tagType.c_str(),i),"2GDeltaTheta","; #Delta#theta_{#gamma #gamma}",0.01,-TMath::Pi(),TMath::Pi());
		m_maphT2GDeltaPhi[key]=	MakeIHistogram1D(Form("%s/Stage%i",tagType.c_str(),i),"2GDeltaPhi","; #Delta#phi_{#gamma #gamma}",0.01,-TMath::Pi(),TMath::Pi());
		m_maphT2GDeltaThetavs2GOpeningAngle[key]= MakeIHistogram2D(
																								Form("%s/Stage%i",tagType.c_str(),i),
																								"2GDeltaThetavs2GOpeningAngle",
																								";#alpha_{#gamma #gamma};#Delta#theta_{#gamma #gamma}",
																								0.01,0.,TMath::Pi(),0.01,-TMath::Pi(),TMath::Pi()
																								)
																								;
		m_maphT2GDeltaPhivs2GOpeningAngle[key]= MakeIHistogram2D(
																								Form("%s/Stage%i",tagType.c_str(),i),
																								"2GDeltaPhivs2GOpeningAngle",
																								";#alpha_{#gamma #gamma};#Delta#phi_{#gamma #gamma}",
																								0.01,0.,TMath::Pi(),0.01,-TMath::Pi(),TMath::Pi()
																								)
																								;
		m_maphT2GDeltaPhivs2GDeltaTheta[key]= MakeIHistogram2D(
																								Form("%s/Stage%i",tagType.c_str(),i),
																								"2GDeltaPhivs2GDeltaTheta",
																								";#Delta#theta_{#gamma #gamma};#Delta#phi_{#gamma #gamma}",
																								0.01,-TMath::Pi(),TMath::Pi(),0.01,-TMath::Pi(),TMath::Pi()
																								)
																								;	
			m_maphT2GamvsEn2Gam[key] = histoSvc()->book(Form("%s/Stage%i",tagType.c_str(),i),"2GamvsEn2Gam","; E_{2#gamma} (GeV); #gamma_{1} index",256,0.,5.,50,0,50);
			m_maphT2GammasInvM[key] =	histoSvc()->book(Form("%s/Stage%i",tagType.c_str(),i),"2GammasInvM"," ;M_{2#gamma} (GeV)",500,0.,5.);	
			m_maphT2G2vs2G1InvM[key] =	MakeIHistogram2D(Form("%s/Stage%i",tagType.c_str(),i),"2G1vs2G2InvM","M_{2#gamma}1 vs M_{2#gamma}2; M_{(2#gamma)1} (GeV); M_{(2#gamma)2} (GeV)",0.005,0.,1.,0.005,0.,1.);
			
				}
			
			if(i>=1 && i<=2){
		m_maphTEGam[key] = histoSvc()->book(Form("%s/Stage%i",tagType.c_str(),i),"EGam","; E_{#gamma} (GeV)",256,0.,5.);
		m_maphTPtGam[key] = histoSvc()->book(Form("%s/Stage%i",tagType.c_str(),i),"PtGam","; P_{t,#gamma} (GeV)",256,0.,5.);
			}
			
			if(i>=2 && i<=3){					
		
			}
			
			if(i>=3 && i<=5){
			m_maphTValid2Combs[key]	=	histoSvc()->book(Form("%s/Stage%i",tagType.c_str(),i),"Valid2Combs"," ; N_{Valid 2x(2 #gamma)};",30,0.,30.);
			m_maphT2CombswSharedGammas[key] =	histoSvc()->book(Form("%s/Stage%i",tagType.c_str(),i),"2CombswSharedGammas","Number of (2#gamma) pairs with shared photons ; ; N_{2#gamma with shared #gamma}",30,0.,30.);
			}
			
			if(i==4){
			m_maphT2G1InvM[key] = MakeIHistogram1D(Form("%s/Stage%i",tagType.c_str(),i),"2G1InvM",";M_{(2#gamma)1} (GeV)",0.0005,0.1,0.16);
			m_maphT2G2InvM[key] = MakeIHistogram1D(Form("%s/Stage%i",tagType.c_str(),i),"2G2InvM",";M_{(2#gamma)2} (GeV)",0.0005,0.1,0.16);
			m_maphTEAsym2G1[key] = histoSvc()->book(Form("%s/Stage%i",tagType.c_str(),i),"EAsym2G1","; E_{Asym (2#gamma)1}",100,0.,2.);
			m_maphTEAsym2G2[key] = histoSvc()->book(Form("%s/Stage%i",tagType.c_str(),i),"EAsym2G2","; E_{Asym (2#gamma)2}",100,0.,2.);
			m_maphT2x2GMissM[key]=	MakeIHistogram1D(Form("%s/Stage%i",tagType.c_str(),i),"2x2GMissM","; Mm[e(2#gamma)_{1}(2#gamma)_{2}X] (GeV)",0.02,0.,4.);
			m_maphT2x2GMissE[key]=	MakeIHistogram1D(Form("%s/Stage%i",tagType.c_str(),i),"2x2GMissE","; E[e(2#gamma)_{1}(2#gamma)_{2}2X] (GeV)",0.02,0.,10.);
			m_maphT2x2GCThetaMiss[key]=	histoSvc()->book(Form("%s/Stage%i",tagType.c_str(),i),"2x2GCThetaMiss","; cos #theta_{Miss}",100,-1.,1.);
			m_maphT2x2GCTheta2G1HelicityG1[key]= histoSvc()->book(Form("%s/Stage%i",tagType.c_str(),i),"2x2GCTheta2G1HelicityG1","; cos #theta_{H (2#gamma)1}",100,-1.,1.);
			m_maphT2x2GCTheta2G2HelicityG1[key]= histoSvc()->book(Form("%s/Stage%i",tagType.c_str(),i),"2x2GCTheta2G2HelicityG1","; cos #theta_{H (2#gamma)1}",100,-1.,1.);
			m_maphT2x2GInvM[key] = MakeIHistogram1D(Form("%s/Stage%i",tagType.c_str(),i),"2x2GInvM","; M_{2x(2#gamma)} (GeV)",0.02,0.,4.);		
			m_maphT2x2GSumPt[key]	=	MakeIHistogram1D(Form("%s/Stage%i",tagType.c_str(),i),"2x2GSumPt",";|#vec{P^{*}_{e-}}+#vec{P^{*}_{(2#gamma)1}}+#vec{P^{*}_{(2#gamma)2}}| (GeV/c)",0.01,0.,5.);	
			m_maphT2x2GcosTheta2G1CM2Gam[key]	=	histoSvc()->book(Form("%s/Stage%i",tagType.c_str(),i),"2x2GcosTheta2G1CM2Gam",";cos #theta*_{(2#gamma)1}",100,-1.,1.);
			m_maphT2x2GRGam[key] = histoSvc()->book(Form("%s/Stage%i",tagType.c_str(),i),"2x2GRGam",";R_{#gamma}",100,-1.,1.);
		}		

		if(i>=4){
		m_maphTKinFitChi2[key] = histoSvc()->book(Form("%s/Stage%i",tagType.c_str(),i),"KinFitChi2","Total fit; #Chi^{2}",2000,0.,2000.);	
		}
		
			if(i>=5)
				{
				m_maphTPi01InvM[key] = MakeIHistogram1D(Form("%s/Stage%i",tagType.c_str(),i),"Pi01InvM","Unfitted ;M_{#pi^{0}1}^{Unfitted} (GeV)",0.0005,0.1,0.16);
				m_maphTPi02InvM[key] = MakeIHistogram1D(Form("%s/Stage%i",tagType.c_str(),i),"Pi02InvM","Unfitted ;M_{#pi^{0}2}^{Unfitted} (GeV)",0.0005,0.1,0.16);
				m_maphTFitPi01InvM[key] = MakeIHistogram1D(Form("%s/Stage%i",tagType.c_str(),i),"FitPi01InvM","Fitted ;M_{#pi^{0}1} (GeV)",0.0005,0.1,0.16);
				m_maphTFitPi02InvM[key] = MakeIHistogram1D(Form("%s/Stage%i",tagType.c_str(),i),"FitPi02InvM","Fitted ;M_{#pi^{0}2} (GeV)",0.0005,0.1,0.16);
				m_maphTMissM[key]=	MakeIHistogram1D(Form("%s/Stage%i",tagType.c_str(),i),"MissM","; Mm[#pi^{0}#pi^{0}X] (GeV)",0.02,0.,4.);
				m_maphTMissE[key]=	MakeIHistogram1D(Form("%s/Stage%i",tagType.c_str(),i),"MissE","; E[#pi^{0}#pi^{0}2X] (GeV)",0.02,0.,10.);				
				m_maphTCThetaHelicity[key] = histoSvc()->book(Form("%s/Stage%i",tagType.c_str(),i),"CThetaHelicity","; cos #theta_{H (2#gamma)1}",100,-1.,1.);				
				m_maphT2Pi0InvM[key] = MakeIHistogram1D(Form("%s/Stage%i",tagType.c_str(),i),"2Pi0InvM","Unfitted ; M_{#pi^{0}#pi^{0}}^{Unfitted} (GeV)",0.02,0.,4.);
				m_maphTFit2Pi0InvM[key] = MakeIHistogram1D(Form("%s/Stage%i",tagType.c_str(),i),"2FitPi0InvM","Fit ; M_{#pi^{0}#pi^{0}} (GeV)",0.02,0.,4.);
				m_maphTFit2Pi0InvMvs2Pi0InvM[key] = histoSvc()->book(Form("%s/Stage%i",tagType.c_str(),i),"Fit2Pi0InvMvs2Pi0InvM",";M_{#pi^{0}#pi^{0}}^{Unfitted} (GeV);M_{#pi^{0}#pi^{0}}^{Fit} (GeV)",200,0.,4.,200,0.,4.);	
				m_maphTFitEAsymPi01[key] = histoSvc()->book(Form("%s/Stage%i",tagType.c_str(),i),"FitEAsymPi01","; E_{Asym #pi^{0}1}",100,0.,2.);
				m_maphTFitEAsymPi02[key] = histoSvc()->book(Form("%s/Stage%i",tagType.c_str(),i),"FitEAsymPi02","; E_{Asym #pi^{0}2}",100,0.,2.);				
				m_maphTSumPt[key]	=	MakeIHistogram1D(Form("%s/Stage%i",tagType.c_str(),i),"TSumPt",";|#vec{P^{*}_{e-}}+#vec{P^{*}_{#pi^{O}1}}+#vec{P^{*}_{#pi^{O}2}}|^{Unfitted} (GeV/c)",0.01,0.,5.);
				m_maphTFitSumPt[key]	=	MakeIHistogram1D(Form("%s/Stage%i",tagType.c_str(),i),"TFitSumPt",";|#vec{P^{*}_{e-}}+#vec{P^{*}_{#pi^{O}1}}+#vec{P^{*}_{#pi^{O}2}}| (GeV/c)",0.01,0.,5.);		
				m_maphTFitcosThetaPi01CM2Gam[key]	=	histoSvc()->book(Form("%s/Stage%i",tagType.c_str(),i),"FitcosThetaPi01CM2Gam",";cos #theta*",100,-1.,1.);
				m_maphTRGam[key] = histoSvc()->book(Form("%s/Stage%i",tagType.c_str(),i),"RGam",";R^{Unfitted}_{#gamma}",100,-1.,1.);
				m_maphTFitRGam[key] = histoSvc()->book(Form("%s/Stage%i",tagType.c_str(),i),"FitRGam",";R_{#gamma}",100,-1.,1.);
				m_maphTQ2[key] = histoSvc()->book(Form("%s/Stage%i",tagType.c_str(),i),"Q2",";Q^{2} (GeV)",20,0.,3.);
				m_maphTcosThetaPi01CM2Gam[key] =  MakeIHistogram1D(Form("%s/Stage%i",tagType.c_str(),i),"cosThetaPi01CM2Gam",";|cos #theta^{*}|",0.025,0.,1.);
				m_maphTPhiPi01CM2Gam[key] =  histoSvc()->book(Form("%s/Stage%i",tagType.c_str(),i),"PhiPi01CM2Gam",";#phi^{*}",32,-TMath::Pi(),TMath::Pi());
			}

			}
		}
	
			
	 NTuplePtr et2G(ntupleSvc(), "FILE1/Tagged2G");
	 if   ( et2G ) {m_NT2G = et2G;}
    else {m_NT2G = ntupleSvc()->book("FILE1/Tagged2G", CLID_ColumnWiseTuple, "Tagged");}
		
		if ( m_NT2G) {
		m_NT2G->addItem("T2GRunNumber"    ,m_dT2GRunNumber);
		m_NT2G->addItem("T2GEvtNumber"    ,m_dT2GEvtNumber);
    m_NT2G->addItem("T2GCMSEnergy"    ,m_dT2GCMSEnergy);
		m_NT2G->addItem("iT2GN2Gs"    ,m_iT2GN2Gs,0,1024);
		m_NT2G->addIndexedItem("dT2GEG1",m_iT2GN2Gs,m_dT2GEG1);
		m_NT2G->addIndexedItem("dT2GEG2",m_iT2GN2Gs,m_dT2GEG2);
		m_NT2G->addIndexedItem("dT2GEAsymmetry",m_iT2GN2Gs,m_dT2GEAsymmetry);		
		m_NT2G->addIndexedItem("dT2GMass2G",m_iT2GN2Gs,m_dT2GMass2G);
		m_NT2G->addIndexedItem("dT2GCThetaMiss",m_iT2GN2Gs,m_dT2GCThetaMiss);
		m_NT2G->addIndexedItem("dT2GCThetaHelicity",m_iT2GN2Gs,m_dT2GCThetaHelicity);
		m_NT2G->addIndexedItem("dT2GOpeningAngle",m_iT2GN2Gs,m_dT2GOpeningAngle);
		m_NT2G->addIndexedItem("dT2GDeltaTheta",m_iT2GN2Gs,m_dT2GDeltaTheta);
		m_NT2G->addIndexedItem("dT2GDeltaPhi",m_iT2GN2Gs,m_dT2GDeltaPhi);
		
		}
	 
	 NTuplePtr et2x2G(ntupleSvc(), "FILE1/Tagged2x2G");
	 if   ( et2x2G ) {m_NT2x2G = et2x2G;}
    else {m_NT2x2G = ntupleSvc()->book("FILE1/Tagged2x2G", CLID_ColumnWiseTuple, "Tagged");}
		
		if ( m_NT2x2G) {
		m_NT2x2G->addItem("T2x2GRunNumber"    ,m_dT2x2GRunNumber);
		m_NT2x2G->addItem("T2x2GEvtNumber"    ,m_dT2x2GEvtNumber);
    m_NT2x2G->addItem("T2x2GCMSEnergy"    ,m_dT2x2GCMSEnergy);
		m_NT2x2G->addItem("iT2x2GNSizePi0TrackTable"    ,m_iT2x2GNSizePi0TrackTable);
		//m_NT2x2G->addItem("iT2x2GN2x2G"    ,m_iT2x2GN2x2G,0,50);
		m_NT2x2G->addItem("iT2x2GN2x2G"    ,m_iT2x2GN2x2G,0,1024);
		m_NT2x2G->addIndexedItem("dT2x2GE2G1",m_iT2x2GN2x2G,m_dT2x2GE2G1);
		m_NT2x2G->addIndexedItem("dT2x2GE2G2",m_iT2x2GN2x2G,m_dT2x2GE2G2);
		m_NT2x2G->addIndexedItem("dT2x2GInvM2G1",m_iT2x2GN2x2G,m_dT2x2GInvM2G1);
		m_NT2x2G->addIndexedItem("dT2x2GInvM2G2",m_iT2x2GN2x2G,m_dT2x2GInvM2G2);
		m_NT2x2G->addIndexedItem("dT2x2GEAsym2G1",m_iT2x2GN2x2G,m_dT2x2GEAsym2G1);
		m_NT2x2G->addIndexedItem("dT2x2GEAsym2G2",m_iT2x2GN2x2G,m_dT2x2GEAsym2G2);
		m_NT2x2G->addIndexedItem("dT2x2GCThetaMiss",m_iT2x2GN2x2G,m_dT2x2GCThetaMiss);
		m_NT2x2G->addIndexedItem("dT2x2GInvM2x2G",m_iT2x2GN2x2G,m_dT2x2GInvM2x2G);
		m_NT2x2G->addIndexedItem("dT2x2GSumPtCM",m_iT2x2GN2x2G,m_dT2x2GSumPtCM);
		m_NT2x2G->addIndexedItem("dT2x2GCTheta2G1HelicityG1",m_iT2x2GN2x2G,m_dT2x2GCTheta2G1HelicityG1);
		m_NT2x2G->addIndexedItem("dT2x2GCTheta2G2HelicityG1",m_iT2x2GN2x2G,m_dT2x2GCTheta2G2HelicityG1);
		m_NT2x2G->addIndexedItem("dT2x2GRGam",m_iT2x2GN2x2G,m_dT2x2GRGam);
		}
		
			
	 NTuplePtr et(ntupleSvc(), "FILE1/Tagged");
	 if   ( et ) {m_NTagged = et;}
    else {m_NTagged = ntupleSvc()->book("FILE1/Tagged", CLID_ColumnWiseTuple, "Tagged");}
		
		if ( m_NTagged ) {
     m_NTagged->addItem("TRunNumber"    ,m_dTRunNumber);
		 m_NTagged->addItem("TEvtNumber"    ,m_dTEvtNumber);
     m_NTagged->addItem("TCMSEnergy"    ,m_dTCMSEnergy);
		 m_NTagged->addItem("iTQTag"    ,m_iTQTag);
		 m_NTagged->addItem("dTSumEemc"    ,m_dTSumEemc);
     m_NTagged->addItem("iTAnzGoodP"    ,m_iTAnzGoodP);
     m_NTagged->addItem("iTAnzGoodM"    ,m_iTAnzGoodM);
     m_NTagged->addItem("iTAnzGoodGamma",m_iTAnzGoodGamma);
     m_NTagged->addItem("iTAnzPi0Tracks",m_iTAnzPi0Tracks);
     m_NTagged->addItem("iTAnz2Combs"  ,m_iTAnz2Combs);
     m_NTagged->addItem("iTAnz3Combs"  ,m_iTAnz3Combs);
     m_NTagged->addItem("TTotalChi2"   ,m_dTTotalChi2);
     m_NTagged->addItem("TPi01Chi2"    ,m_dTPi01Chi2);
     m_NTagged->addItem("TPi02Chi2"    ,m_dTPi02Chi2);
     m_NTagged->addItem("iTmcTruePi0"  ,m_iTmcTruePi0);
     m_NTagged->addItem("iTmcTrueFSR"  ,m_iTmcTrueFSR);
     m_NTagged->addItem("iTmcTrueGammaFromJpsi"  ,m_iTmcTrueGammaFromJpsi);
     m_NTagged->addItem("iTmcTrueOtherGamma"  ,m_iTmcTrueOtherGamma);
     m_NTagged->addItem("iTmcTrueOther" ,m_iTmcTrueOther);
		 m_TLepton    .AttachToNtuple(m_NTagged,"TLepton");
     m_TPi01    .AttachToNtuple(m_NTagged,"TPi01");
     m_TPi02    .AttachToNtuple(m_NTagged,"TPi02");
		 m_TFitLepton    .AttachToNtuple(m_NTagged,"TFitLepton");
     m_TFitPi01    .AttachToNtuple(m_NTagged,"TFitPi01");
     m_TFitPi02    .AttachToNtuple(m_NTagged,"TFitPi02");
     //for (int a=0;a<GAMMAS;a++) {m_TGammas   [a].AttachToNtuple(m_NTagged,"TG"   +SSTR(a)+"emc",true); m_NTagged->addItem("ClosestTr"+SSTR(a),m_dTClosestTrack[a]);}
		 for (int a=0;a<GAMMAS;a++) {m_TGammas   [a].AttachToNtuple(m_NTagged,"TG"   +SSTR(a)+"emc",true);}
     for (int a=0;a<5;a++)        {m_TFitGammas[a].AttachToNtuple(m_NTagged,"TFitG"+SSTR(a),true);}
     m_NTagged->addItem("TVtxChi2"  ,m_dTVtxChi2);
     m_NTagged->addItem("etVtxPosX"   ,m_dTVtxPosX);
     m_NTagged->addItem("etVtxPosY"   ,m_dTVtxPosY);
     m_NTagged->addItem("etVtxPosZ"   ,m_dTVtxPosZ);
     m_NTagged->addItem("etIPposX"    ,m_dTIPposX);
     m_NTagged->addItem("etIPposY"    ,m_dTIPposY);
		 m_NTagged->addItem("etIPposZ"    ,m_dTIPposZ);	 
		 m_NTagged->addItem("dTPi01InvM"    ,m_dTPi01InvM);
		 m_NTagged->addItem("dTFitPi01InvM"    ,m_dTFitPi01InvM);
		 m_NTagged->addItem("dTPi02InvM"    ,m_dTPi02InvM);
		 m_NTagged->addItem("dTFitPi02InvM"    ,m_dTFitPi02InvM);
		 m_NTagged->addItem("dTMissMass"    ,m_dTMissMass);
		 m_NTagged->addItem("dTMissE"    ,m_dTMissE);
		 m_NTagged->addItem("dTDeltaEemcE4GamLept"    ,m_dTDeltaEemcE4GamLept);
		 m_NTagged->addItem("dT2Pi0InvM"    ,m_dT2Pi0InvM);
		 m_NTagged->addItem("dTFit2Pi0InvM"    ,m_dTFit2Pi0InvM);
		 m_NTagged->addItem("dTQ2"    ,m_dTQ2);
		 m_NTagged->addItem("dTFitQ2"    ,m_dTFitQ2);
		 m_NTagged->addItem("dTEAsymPi01"    ,m_dTEAsymPi01);
		 m_NTagged->addItem("dTFitEAsymPi01"    ,m_dTFitEAsymPi01);
		 m_NTagged->addItem("dTEAsymPi02"    ,m_dTEAsymPi02);
		 m_NTagged->addItem("dTFitEAsymPi02"    ,m_dTFitEAsymPi02);
		 m_NTagged->addItem("dTSumPtCM"    ,m_dTSumPtCM);
		 m_NTagged->addItem("dTFitSumPtCM"    ,m_dTFitSumPtCM);
		 m_NTagged->addItem("dTcosThetaPi01CM2Gam"    ,m_dTcosThetaPi01CM2Gam);
		 m_NTagged->addItem("dTFitcosThetaPi01CM2Gam"    ,m_dTFitcosThetaPi01CM2Gam);
		 m_NTagged->addItem("dTPhiPi01CM2Gam"    ,m_dTPhiPi01CM2Gam);
		 m_NTagged->addItem("dTFitPhiPi01CM2Gam"    ,m_dTFitPhiPi01CM2Gam);
		 m_NTagged->addItem("dTCThetaHelicity"    ,m_dTCThetaHelicity);
		 m_NTagged->addItem("dTFitCThetaHelicity"    ,m_dTFitCThetaHelicity);
		 m_NTagged->addItem("dTRGam"    ,m_dTRGam);
		 m_NTagged->addItem("dTFitRGam"    ,m_dTFitRGam);
		 m_NTagged->addItem("iIsT2Pi0"    ,m_iIsT2Pi0);

		 //m_NTagged->addItem("dTMCQ2",m_dTMCQ2);
		 m_NTagged->addItem("dTMC2Pi0InvM",m_dTMC2Pi0InvM);
		 m_NTagged->addItem("dTMCSumPtCM",m_dTMCSumPtCM);
  }
   else {
      log << MSG::ERROR << "  Cannot book N-tuple1 e- tagged: " << long(m_NTagged) << endreq;
      return StatusCode::FAILURE;
   }
	 
	 
	 StatusCode sc = Gaudi::svcLocator()->service( "VertexDbSvc", m_vtxSvc,true);
   if(sc.isFailure())
     {
     //assert(false); //terminate program
     log << MSG::ERROR << "Cannot book VertexDbSvc!!" <<endmsg;
     return StatusCode::FAILURE;
   }
   //m_particleTable = m_partPropSvc->PDT();
	 
   return StatusCode::SUCCESS;
}
//----------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------
StatusCode Bes2Gamto2Pi0::execute()
//----------------------------------------------------------------------------------------------------------
{

//cout<<"begin execute"<<endl;
    MsgStream log(msgSvc(), name());
    log << MSG::INFO << "in execute()" << endreq;

    SmartDataPtr<Event::EventHeader> eventHeader(eventSvc(),"/Event/EventHeader");
    

		//log << MSG::WARNING <<  m_iAll << endreq;
    
    m_iAll ++;

		if(m_iAll%10000==0) log << MSG::WARNING << "Event # " << m_iAll<< " " << endreq;
		//log << MSG::WARNING << "Event # " << m_iAll<< " " << endreq;
		
		//printf("\n");
		//printf("Event #%d \n",m_iAll);
				
		m_dRunNumber   = eventHeader->runNumber();
		m_dEvtNumber  = eventHeader->eventNumber(); 
		m_dCMSEnergy = beam_energy(abs(eventHeader->runNumber()));
    if (m_dCMSEnergy == -1) {log<<MSG::FATAL<<"unknown runNr, can not determine CMS Energy!"<<endreq; return StatusCode::FAILURE;}  
		
		//------ initial state kinematics ------//
		/*
		double TotEngBeam = m_dCMSEnergy/2 + m_dMasselec;
		double Abs3MomBeam = sqrt(TotEngBeam*TotEngBeam - m_dMasselec*m_dMasselec);
  	*/
		
		double Abs3MomBeam = sqrt((pow(m_dCMSEnergy,2)/2-pow(m_dMasselec,2))/(1-cos(TMath::Pi()-m_dCrossingAngle)));
		double TotEngBeam = sqrt(pow(m_dMasselec,2)+pow(Abs3MomBeam,2));
		
		
		m_lvBeamElectron.setPx(Abs3MomBeam*sin(m_dCrossingAngle));
		m_lvBeamElectron.setPy(0);
		m_lvBeamElectron.setPz(-Abs3MomBeam*cos(m_dCrossingAngle));
		m_lvBeamElectron.setE(TotEngBeam);
  
		m_lvBeamPositron.setPx(Abs3MomBeam*sin(m_dCrossingAngle));
		m_lvBeamPositron.setPy(0);
		m_lvBeamPositron.setPz(Abs3MomBeam*cos(m_dCrossingAngle));
		m_lvBeamPositron.setE(TotEngBeam);
		
		m_lvBoost        = m_lvBeamElectron+m_lvBeamPositron;
		//m_lvBoost            = HepLorentzVector(m_dCMSEnergy*sin(m_dCrossingAngle),0,0,m_dCMSEnergy);		

		/*
		m_vectboostCM=m_lvBoost.boostVector();		
		m_lvBeamElectronCM=m_lvBeamElectron;
		m_lvBeamElectronCM.boost(-m_vectboostCM);
		m_lvBeamPositronCM=m_lvBeamPositron;
		m_lvBeamPositronCM.boost(-m_vectboostCM);
		*/
		
		m_vectboostCM=m_lvBoost.boostVector();
		vector<HepLorentzVector> veclv;
		veclv.push_back(m_lvBeamElectron);veclv.push_back(m_lvBeamPositron);
		vector <HepLorentzVector> veclvCM = MakeBoostedVecLV(veclv,m_vectboostCM);
		m_lvBeamElectronCM=veclvCM[0];
		m_lvBeamPositronCM=veclvCM[1];
		
		m_dPi0GcutHigh   = m_dCMSEnergy*0.6;
		
		//------ Get montecarlo truth event ------//
		SmartDataPtr<McParticleCol> mcParticles(eventSvc(),"/Event/MC/McParticleCol");
		
  	if(!mcParticles) {
		m_iMCel						 = -2;
		m_iMCelprimary		 = -2;
		m_iMCpositron			 = -2;
		m_iMCpositronprimary=-2;
    m_iMCpi0           = -2;
    m_iMCfsr           = -2;
    m_iMCGammaFromJpsi = -2;
    m_iMCOtherGamma    = -2;
    m_iMCOther         = -2;
    m_bMCrecorded      = false;
    m_dmTotFinalMCtruth = -2;
  	} 
		else { 
  	DoMCTruth();				
		}
		
		//------ Get reconstructed event ------//
		SmartDataPtr<EvtRecEvent> evtRecEvent(eventSvc(), "/Event/EvtRec/EvtRecEvent");
    if ( ! evtRecEvent ) {
        log << MSG::FATAL << "Could not find EvtRecEvent" << endreq;
        return StatusCode::FAILURE;
    }
    SmartDataPtr<EvtRecTrackCol> evtRecTrackCol(eventSvc(), "/Event/EvtRec/EvtRecTrackCol");
    if ( ! evtRecTrackCol ) {
        log << MSG::FATAL << "Could not find EvtRecTrackCol" << endreq;
        return StatusCode::FAILURE;
    }
    Gaudi::svcLocator()->service("VertexDbSvc", m_vtxSvc);
		
		
		//------ initialize virtual box ------//
    m_hp3IP.setX(0);
    m_hp3IP.setY(0);
    m_hp3IP.setZ(0);
    if (m_vtxSvc->isVertexValid()) {
      double* dbv =  m_vtxSvc->PrimaryVertex();
      m_hp3IP.setX(dbv[0]);
      m_hp3IP.setY(dbv[1]);
      m_hp3IP.setZ(dbv[2]);
    }
    else{std::cout<<"no IP"<<std::endl;}
	
		//------ initialize iterators ------//
    m_itBegin      = evtRecTrackCol->begin();
    m_itEndCharged = m_itBegin + evtRecEvent->totalCharged();
    m_itEndNeutral = m_itBegin + evtRecEvent->totalTracks();
    
    
		m_iNCharged= evtRecEvent->totalCharged();
		m_iNNeutral= evtRecEvent->totalNeutral();
		
		
		//----- Event selection ------//		
		if(m_bDoUntagged==true) DoUntagged();
		if(m_bDoTagged==true) DoTagged();
		
		//DoUntagged();
		//DoTagged();
		
		return StatusCode::SUCCESS;
}
//----------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------- 
StatusCode Bes2Gamto2Pi0::finalize()
//----------------------------------------------------------------------------------------------------------
{
  MsgStream log(msgSvc(), name());
  log << MSG::INFO << "in finalize()" << endreq;
	log << MSG::WARNING << endreq;
	log << MSG::WARNING << "****** Total number of events " << m_iAll << " ******" << endreq;
	log << MSG::WARNING << "****** UNTAGGED : Number of kept events at  neutral particles preselection " << m_iUPreKept << " ******" << endreq;
	log << MSG::WARNING << "****** UNTAGGED :Number of kept events at preselect.+ good photons requirement " << m_iUPhotonKept << " ******" << endreq;
	log << MSG::WARNING << "****** UNTAGGED : Number of kept events at npreselect.+ good photons requirement + 2 pi0 candidates requirement " << m_iUPi0kept << " ******" << endreq;
  log << MSG::WARNING << "****** UNTAGGED : Number of kept events at npreselect.+ good photons requirement + 2 pi0 candidates + kalmanfit requirement " << m_iUFitKept << " ******" << endreq;
	log << MSG::WARNING << "****** UNTAGGED : Number of untagged e+e-2Pi0 events " << m_iAcceptedU << " ******" << endreq;
	log << MSG::WARNING << endreq;
	log << MSG::WARNING << "****** TAGGED : Number of valid MDC charged tracks " << m_iTValidMDCKept << " ******" << endreq;
	log << MSG::WARNING << "****** TAGGED : Number of valid MDC + close to IP charged tracks " << m_iTValidMDCIPKept << " ******" << endreq;
	log << MSG::WARNING << "****** TAGGED : Number of good negative tracks " << m_iTGoodNegKept << " ******" << endreq;
	log << MSG::WARNING << "****** TAGGED : Number of events with only one lepton " << m_iT1Lepton << " ******" << endreq;
	log << MSG::WARNING << "****** TAGGED : Number of kept events at 1e- + good photons requirement " << m_iTPhotonKept << " ******" << endreq;
  log << MSG::WARNING << "****** TAGGED : Number of kept events at npreselect.+ good photons + 2 pi0 candidates " << m_iTPi0kept << " ******" << endreq;
	log << MSG::WARNING << "****** TAGGED : Number of kept events at npreselect.+ good photons + 2 pi0 candidates + kinematic fit " << m_iTFitKept << " ******" << endreq;
	log << MSG::WARNING << "****** TAGGED : Number of untagged e+e-2Pi0 events " << m_iAcceptedT << " ******" << endreq;
	return StatusCode::SUCCESS;
}
//----------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------
double Bes2Gamto2Pi0::beam_energy(int runNo)
//----------------------------------------------------------------------------------------------------------
{
    double ecms = -1;

    if( runNo>=23463 && runNo<=23505 ){//psi(4040) dataset-I
      ecms = 4.0104;
    }
    else if( runNo>=23509 && runNo<24141 ){//psi(4040) dataset-II
      ecms = 4.0093;
    }
    else if( runNo>=-24141 && runNo<=-23463 ){// psi(4009)
      ecms = 4.009;
    }
    else if( runNo>=24151 && runNo<=24177 ){//psi(2S) data
      ecms = 3.686;
    }
    else if( runNo>=29677 && runNo<=30367 ){//Y(4260) data-I
      ecms = 4.26;
    }
    else if( runNo>=31561 && runNo<=31981 ){//Y(4260) data-II
      ecms = 4.26;
    }
    else if( runNo>=31327 && runNo<=31390 ){//4420 data
      ecms = 4.42;
    }
    else if( runNo>=31281 && runNo<=31325 ){//4390 data
      ecms = 4.39;
    }
    else if( runNo>=30616 && runNo<=31279 ){//Y(4360) data
      ecms = 4.36;
    }
    else if( runNo>=30492 && runNo<=30557 ){// 4310 data
      ecms = 4.31;
    }
    else if( runNo>=30438 && runNo<=30491 ){// 4230 data
      ecms = 4.23;
    }
    else if( runNo>=30372 && runNo<=30437 ){// 4190 data
      ecms = 4.19;
    }
    else if( runNo>=31983 && runNo<=32045 ){// 4210 data
      ecms = 4.21;
    }
    else if( runNo>=32046 && runNo<=32140 ){// 4220 data
      ecms = 4.22;
    }
    else if( runNo>=32141 && runNo<=32226 ){// 4245 data
      ecms = 4.245;
    }
    else if( runNo>=32239 && runNo<=32850 ){// new 4230 data-I
      ecms = 4.23;
    }
    else if( runNo>=32851 && runNo<=33484 ){// new 4230 data-II
      ecms = 4.228;
    }
    else if( runNo>=33490 && runNo<=33556 ){// 3810 data
      ecms = 3.81;
    }
    else if( runNo>=33571 && runNo<=33657 ){// 3900 data
      ecms = 3.90;
    }
    else if( runNo>=33659 && runNo<=33719 ){// 4090 data
      ecms = 4.09;
    }
    else if( runNo>=11397 && runNo<=23454 ){// psi(3770) data
      ecms = 3.773;
    }

    return ecms;
}

//---------------------------------------------------------------------------------------------------------- 
double Bes2Gamto2Pi0::GetSumPt(std::vector<HepLorentzVector> vectlv)
{
double SumPt;
HepLorentzVector Sumlv(0,0,0,0);
Hep3Vector Sum3v(0,0,0);
Hep3Vector Sum3vperp(0,0,0);

for(int k=0;k<vectlv.size();k++){
	Sumlv+=vectlv[k];
	Sum3v+=vectlv[k].vect();
	Sum3vperp+=(vectlv[k].vect()).perpPart();
	}
//SumPt=(Sum3v.perpPart()).r();	
//SumPt=Sum3vperp.r();
SumPt=Sumlv.perp();
/*
SumPt=sqrt(pow(Sumlv.x(),2)
					+pow(Sumlv.y(),2)
					);
*/

//m_dUSumPtCM=((m_lvPi01CM.vect()).perpPart()+(m_lvPi02CM.vect()).perpPart()).r();
//m_dUSumPtCM=((m_lvPi01CM.vect()+m_lvPi02CM.vect()).perpPart()).r();
//m_dUSumPtCM=(m_lvPi01CM+m_lvPi02CM).perp();

//Hep3Vector vZAxis(0,0,1);
//m_dUSumPtCM=((m_lvPi01CM.vect()+m_lvPi02CM.vect()).perpPart(vZAxis)).r();
//m_dUSumPtCM=sqrt(pow((m_lvPi01CM.vect()+m_lvPi02CM.vect()).x(),2)
//								+pow((m_lvPi01CM.vect()+m_lvPi02CM.vect()).y(),2)
//								);

return SumPt;
}
//---------------------------------------------------------------------------------------------------------- 

//---------------------------------------------------------------------------------------------------------- 
double Bes2Gamto2Pi0::GetPt(HepLorentzVector lv)
{
double Pt;
Pt=lv.perp();
return Pt;
}
//---------------------------------------------------------------------------------------------------------- 

//---------------------------------------------------------------------------------------------------------- 
bool Bes2Gamto2Pi0::IsPhoton(EvtRecTrack* pTrack)
{
if (!pTrack->isEmcShowerValid()) {return (false);}
  if (   (abs(cos(pTrack->emcShower()->theta())) <= m_dCosThetaBarrel  && pTrack->emcShower()->energy() < m_dPhEnergyCutBarrel)
      || (abs(cos(pTrack->emcShower()->theta())) >  m_dCosThetaBarrel  && pTrack->emcShower()->energy() < m_dPhEnergyCutEndCap)
      || abs(pTrack->emcShower()->time()) >  m_dMaxTime 
     )
     {return (false);}
	if (abs(RejectEMCtrack(pTrack)) < 0.349) {return (false);}
  return (true);

}
//---------------------------------------------------------------------------------------------------------- 

//---------------------------------------------------------------------------------------------------------- 
double Bes2Gamto2Pi0::RejectEMCtrack (EvtRecTrack* pCur)
//---------------------------------------------------------------------------------------------------------- 
{

  EvtRecTrackIterator  itERT;
  double dAngMin = 200.0, dAng;
  Hep3Vector extpos;
  Hep3Vector emcpos(pCur->emcShower()->x(), pCur->emcShower()->y(), pCur->emcShower()->z());
  for (itERT  = m_itBegin;itERT != m_itEndCharged;itERT ++) {
    if(!(*itERT)->isExtTrackValid()) {continue;}
    if((*itERT)->extTrack()->emcVolumeNumber() == -1) {continue;}
    extpos = (*itERT)->extTrack()->emcPosition();
    double dAng = extpos.angle(emcpos);
    if(dAng < dAngMin){
      dAngMin = dAng;
    }
  }
  //if(dAngMin >= 200) {dAngMin = -4.0;}
  //if(dAngMin >= 200) {return (true);}
  //dAngMin *= 180.0/(CLHEP::pi);
  //if(fabs(dAngMin) < m_dDeltaAngleCut) {return (true);}

  return dAngMin;
}	      
//---------------------------------------------------------------------------------------------------------- 
void Bes2Gamto2Pi0::DoMCTruth()
{
SmartDataPtr<McParticleCol> mcParticles(eventSvc(),"/Event/MC/McParticleCol");

int  iPin  = 0;
			m_iMCel						 = 0;
			m_iMCelprimary		 = 0;
			m_iMCpositron			 = 0;
			m_iMCpositronprimary=0;
  		m_iMCpi0           = 0;
  		m_iMCfsr           = 0;
  		m_iMCGammaFromJpsi = 0;
  		m_iMCOtherGamma    = 0;
  		m_iMCOther         = 0;
  		m_bMCrecorded      = true;
  		m_dmTotFinalMCtruth  = -1;
			
			m_iAmcNpart=0;
			
			m_haMCNumEvt->fill(0,1.);
					
			//FOR GALUGA+MANUAL GENERATOR ONLY!
			//PARTICLES ADDED BY KKMC (CHARM PRODUCTION) INTO MANUAL GENERATOR
			//GALUGA OUTPUT FILES CONTAINS PID AND 4 MOMENTUM COORDINATES ONLY.
			//KKMC ADD FALSE VERTICES AND FALSE MC FLAG (PRIMARY)
			//POSITRON IS COUNTED AS SECONDARY INSTEAD OF PRIMARY PARTICLE!
  		for( McParticleCol::iterator itMC = mcParticles->begin();itMC != mcParticles->end();itMC ++) {
    		
			
			int PID = (*itMC)->particleProperty();
      HepLorentzVector Particle = (*itMC)->initialFourMomentum();
      
      bool primary = (*itMC)->primaryParticle();
      bool leaf = (*itMC)->leafParticle();
      bool generator = (*itMC)->decayFromGenerator();
      bool flight = (*itMC)->decayInFlight();
      
      Event::McParticle Mother = (*itMC)->mother();
      int MPID = Mother.particleProperty();
			
			if(m_iAmcNpart <= m_iAmcNpart->range().distance()) {
				m_iAmcPrimary[m_iAmcNpart]=primary;
				m_iAmcDecayGenerator[m_iAmcNpart]=generator;
				m_adAmcID[m_iAmcNpart]=PID;
				m_iAmcMother[m_iAmcNpart]=MPID;
				m_adAmcP[m_iAmcNpart]=((*itMC)->initialFourMomentum()).mag();
				m_adAmcTheta[m_iAmcNpart]=((*itMC)->initialFourMomentum()).theta();
				m_adAmcPhi[m_iAmcNpart]=((*itMC)->initialFourMomentum()).phi();
				m_adAmcE[m_iAmcNpart]=((*itMC)->initialFourMomentum()).e();
				m_iAmcNpart++;
      	}
				
			switch(PID) {
				case 11: //electron
					m_iMCel++; 
					//With KKMC Generator fed into ManualGenerator,
					//Final state particles (e+e-pi0pi0) are produced from Psipp(PID:30443)
					//These particles are selected by requiring they come from Psipp decay.
					// 2 electrons are contained in MC files:
					// 1-incoming electron beam (Mother ID=11)
					// 2- final state electron (Mother ID=30443)
					if(MPID==30443) 
						{m_iMCelprimary++;
						m_lvmcel = (*itMC)->initialFourMomentum(); 
						}
					break;
				case -11: //positron
					m_iMCpositron++; 
					if(MPID==30443) 
						{m_iMCpositronprimary++;
						m_lvmcpositron = (*itMC)->initialFourMomentum(); 
						}
					break;
    		case 111:// pi0
					m_iMCpi0 ++;      
      		switch(iPin) {
      		case 0: iPin ++;// bFirstP0G = true;
          	m_lvmcPi01       = (*itMC)->initialFourMomentum();
        		break;
      		case 1: iPin ++;// bFirstP0G = true;
          	m_lvmcPi02       = (*itMC)->initialFourMomentum();
        		break;
      		case 2: 
						m_lvmcPi03 = (*itMC)->initialFourMomentum(); 
						break;
      		}
				case   22: // gamma 
				if ((*itMC)->mother().particleProperty() != 111) {m_iMCOtherGamma ++;}    // all gammas except from pi0 decay
      	break;
				
				default:
					// count only final state particles except from pi0 decays
						if ((*itMC)->daughterList().size()                     == 0   // does not decay -> final state particle
								&& (*itMC)->mother().particleProperty()               != 111 // from pi0  decay
         		) {
          		m_iMCOther ++;
      		}
      		break;
					
    		}
				      
  		}
			
			bool Isee2Pi0=(m_iMCpi0 == 2 && m_iMCelprimary==1 && m_iMCpositronprimary ==1);
  		if (Isee2Pi0) {
    	//m_dmTotFinalMCtruth  = (m_lvmcel+m_lvmcpositron+m_lvmcPi01+m_lvmcPi02).mag();
			//m_dAmcUSumPt2Pi0=(m_lvmcPi01+m_lvmcPi02).perp();
			m_dMC2Pi0InvM=(m_lvmcPi01+m_lvmcPi02).m();
			
			m_dAmc2Pi0InvM=m_dMC2Pi0InvM;
			
			//Kinematics in e+e- CM frame	
			vector <HepLorentzVector> veclv;
			veclv.push_back(m_lvBeamElectron);
			veclv.push_back(m_lvBeamPositron);
			veclv.push_back(m_lvmcel);
			veclv.push_back(m_lvmcpositron);
			veclv.push_back(m_lvmcPi01);
			veclv.push_back(m_lvmcPi02);
					
			vector <HepLorentzVector> veclvCM = MakeBoostedVecLV(veclv,m_vectboostCM);
			HepLorentzVector m_lvmcelCM=veclvCM[2];
			HepLorentzVector m_lvmcpositronCM=veclvCM[3];
			HepLorentzVector m_lvmcPi01CM=veclvCM[4];
			HepLorentzVector m_lvmcPi02CM=veclvCM[5];
			
			m_dMCUSumPtCM=GetPt(m_lvmcPi01CM+m_lvmcPi02CM);
			m_dMCeTSumPtCM=GetPt(m_lvmcelCM+m_lvmcPi01CM+m_lvmcPi02CM);
			m_dMCpTSumPtCM=GetPt(m_lvmcpositronCM+m_lvmcPi01CM+m_lvmcPi02CM);		
			
			m_dAmcUSumPt2Pi0=m_dMCUSumPtCM;			
			m_dAmceTSumPt2Pi0=m_dMCeTSumPtCM;
			m_dAmcpTSumPt2Pi0=m_dMCpTSumPtCM;
			
			//RGamma
			double ELept2x2GCM;
			double AbsMomLept2x2GCM;

			ELept2x2GCM=(m_lvmcelCM+m_lvmcPi01CM+m_lvmcPi02CM).e();
			AbsMomLept2x2GCM=((m_lvmcelCM+m_lvmcPi01CM+m_lvmcPi02CM).vect()).mag();
			m_dAmceTRGam=(m_lvBoost.m()-ELept2x2GCM-AbsMomLept2x2GCM)/m_lvBoost.m();
			
  		}
			
			
			/*
			for( McParticleCol::iterator itMC = mcParticles->begin();itMC != mcParticles->end();itMC ++) {
    		
			int PID = (*itMC)->particleProperty();
      HepLorentzVector Particle = (*itMC)->initialFourMomentum();
      
      bool primary = (*itMC)->primaryParticle();
      bool leaf = (*itMC)->leafParticle();
      bool generator = (*itMC)->decayFromGenerator();
      bool flight = (*itMC)->decayInFlight();
      
      Event::McParticle Mother = (*itMC)->mother();
      int MPID = Mother.particleProperty();
			
			//if(m_iAmcNpart <= m_iAmcNpart->range().distance()) {
				printf("%d %d %d %d %f %f %f %f \n",primary,generator,PID,MPID,
				((*itMC)->initialFourMomentum()).mag(),
				((*itMC)->initialFourMomentum()).theta(),
				((*itMC)->initialFourMomentum()).phi(),
				((*itMC)->initialFourMomentum()).e()
				);				
				
				m_iAmcPrimary[m_iAmcNpart]=primary;
				m_iAmcDecayGenerator[m_iAmcNpart]=generator;
				m_adAmcID[m_iAmcNpart]=PID;
				m_iAmcMother[m_iAmcNpart]=MPID;
				m_adAmcP[m_iAmcNpart]=((*itMC)->initialFourMomentum()).mag();
				m_adAmcTheta[m_iAmcNpart]=((*itMC)->initialFourMomentum()).theta();
				m_adAmcPhi[m_iAmcNpart]=((*itMC)->initialFourMomentum()).phi();
				m_adAmcE[m_iAmcNpart]=((*itMC)->initialFourMomentum()).e();
				m_iAmcNpart++;
				
      	//}
				
			}
			
			cout<< m_iAmcNpart<< endl;
			printf("\n");
			*/
			
			/*
			for( McParticleCol::iterator itMC = mcParticles->begin();itMC != mcParticles->end();itMC ++) {
    		
			
			int PID = (*itMC)->particleProperty();
      HepLorentzVector Particle = (*itMC)->initialFourMomentum();
      
      bool primary = (*itMC)->primaryParticle();
      bool leaf = (*itMC)->leafParticle();
      bool generator = (*itMC)->decayFromGenerator();
      bool flight = (*itMC)->decayInFlight();
      
      Event::McParticle Mother = (*itMC)->mother();
      int MPID = Mother.particleProperty();
			
			if(m_iAmcNpart <= m_iAmcNpart->range().distance()) {
				m_adAmcID[m_iAmcNpart]=PID;
				m_adAmcP[m_iAmcNpart]=Particle.mag();
				m_iAmcNpart++;
      	}
				
			switch(PID) {
				case 11: //electron
					m_iMCel++; 
					if(primary) m_iMCelprimary++;
					m_lvmcel = (*itMC)->initialFourMomentum(); 
					break;
				case -11: //positron
					m_iMCpositron++; 
					if(primary) m_iMCpositronprimary++;
					m_lvmcpositron = (*itMC)->initialFourMomentum(); 
					break;
    		case 111:// pi0
					m_iMCpi0 ++;      
      		switch(iPin) {
      		case 0: iPin ++;// bFirstP0G = true;
          	m_lvmcPi01       = (*itMC)->initialFourMomentum();
        		break;
      		case 1: iPin ++;// bFirstP0G = true;
          	m_lvmcPi02       = (*itMC)->initialFourMomentum();
        		break;
      		case 2: 
						m_lvmcPi03 = (*itMC)->initialFourMomentum(); 
						break;
      		}
				case   22: // gamma 
				if ((*itMC)->mother().particleProperty() != 111) {m_iMCOtherGamma ++;}    // all gammas except from pi0 decay
      	break;
				
				default:
					// count only final state particles except from pi0 decays
						if ((*itMC)->daughterList().size()                     == 0   // does not decay -> final state particle
								&& (*itMC)->mother().particleProperty()               != 111 // from pi0  decay
         		) {
          		m_iMCOther ++;
      		}
      		break;
					
    		}
				      
  		}
			
			bool Isee2Pi0=(m_iMCpi0 == 2 && m_iMCelprimary==1 && m_iMCpositronprimary ==1 && m_iMCOther == 0 && m_iMCOtherGamma == 0);
  		if (Isee2Pi0) {
    	m_dmTotFinalMCtruth  = (m_lvmcel+m_lvmcpositron+m_lvmcPi01+m_lvmcPi02).mag();
			m_dAmcUSumPt2Pi0=(m_lvmcPi01+m_lvmcPi02).perp();
  		}
			*/
			
			m_NAmcTruth->write();
			
/*
  	else { 
  		int  iPin  = 0;
			m_iMCel						 = 0;
			m_iMCelprimary		 = 0;
			m_iMCpositron			 = 0;
			m_iMCpositronprimary=0;
  		m_iMCpi0           = 0;
  		m_iMCfsr           = 0;
  		m_iMCGammaFromJpsi = 0;
  		m_iMCOtherGamma    = 0;
  		m_iMCOther         = 0;
  		m_bMCrecorded      = true;
  		m_dmTotFinalMCtruth  = -1;
	
			//Count types of MC particles
			for( McParticleCol::iterator itMC = mcParticles->begin();itMC != mcParticles->end();itMC ++) {
    		if ((*itMC)->particleProperty() == 23 || (*itMC)->particleProperty() == 91) {continue;} // Generator internal particle
    		if (abs((*itMC)->particleProperty()) < 11) {continue;} // Generator internal particle
    		switch((*itMC)->particleProperty()) {
				case   11: //electron 
					m_iMCel++; 
					if((*itMC)->primaryParticle()) m_iMCelprimary++;
					break; 
				case  -11: 
					m_iMCpositron++; 
					if((*itMC)->primaryParticle()) m_iMCpositronprimary++;
					break;
    		case   22: // gamma 
					if ((*itMC)->mother().particleProperty() == 443) {m_iMCGammaFromJpsi ++;} // 443 = J/psi -> gamma + anything
      		else if ((*itMC)->mother().particleProperty() != 111) {m_iMCOtherGamma ++;}    // all other except from pi0 decay
      		break;
    		case  -22: m_iMCfsr ++;   break; // FSR gamma
    		case  111: m_iMCpi0 ++;   break; // pi0
    		default:
					// count only final state particles except from pi0 decays
						if ((*itMC)->daughterList().size()                     == 0   // does not decay -> final state particle
								&& (*itMC)->mother().particleProperty()               != 111 // from pi0  decay
         		) {
          		m_iMCOther ++;
      		}
      		break;
    		}
  		}
	
  		for( McParticleCol::iterator itMC = mcParticles->begin();itMC != mcParticles->end();itMC ++) {
    		if (!(*itMC)->primaryParticle () && (*itMC)->particleProperty() == 22) {continue;}
    		switch((*itMC)->particleProperty()) {
				case 11: //electron
					m_lvmcel = (*itMC)->initialFourMomentum(); break;
				case -11: //positron
					m_lvmcpositron = (*itMC)->initialFourMomentum(); break;
    		case 111:// pi0      
      		switch(iPin) {
      		case 0: iPin ++;// bFirstP0G = true;
          	m_lvmcPi01       = (*itMC)->initialFourMomentum();
        		break;
      		case 1: iPin ++;// bFirstP0G = true;
          	m_lvmcPi02       = (*itMC)->initialFourMomentum();
        		break;
      		case 2: m_lvmcPi03 = (*itMC)->initialFourMomentum(); break;
      		}
    		break;
    		}
				      
  		}

  		if (m_iMCpi0 == 2 && m_iMCelprimary==1 && m_iMCpositronprimary ==1 && m_iMCOther == 0 && m_iMCOtherGamma == 0 && m_iMCGammaFromJpsi == 0) {
    	m_dmTotFinalMCtruth  = (m_lvmcel+m_lvmcpositron+m_lvmcPi01+m_lvmcPi02).mag();
  		}
		}
		*/
}

//---------------------------------------------------------------------------------------------------------- 
void Bes2Gamto2Pi0::DoUntagged()
{

MsgStream log(msgSvc(), name());
    log << MSG::INFO << "in execute()" << endreq;
		
SmartDataPtr<Event::EventHeader> eventHeader(eventSvc(),"/Event/EventHeader");
SmartDataPtr<EvtRecEvent> evtRecEvent(eventSvc(), "/Event/EvtRec/EvtRecEvent");
    if ( ! evtRecEvent ) {
        log << MSG::FATAL << "Could not find EvtRecEvent" << endreq;
        return;
    }
    SmartDataPtr<EvtRecTrackCol> evtRecTrackCol(eventSvc(), "/Event/EvtRec/EvtRecTrackCol");
    if ( ! evtRecTrackCol ) {
        log << MSG::FATAL << "Could not find EvtRecTrackCol" << endreq;
        return;
    }
    Gaudi::svcLocator()->service("VertexDbSvc", m_vtxSvc);
				
//------ prepare ntuples ------//		
		//ResetNTuples();
		m_dURunNumber   = m_dRunNumber;
		m_dUEvtNumber  = m_dEvtNumber;
    m_dUCMSEnergy = m_dCMSEnergy;	

		//IP pos
    m_dUIPposX  = m_hp3IP.x();
    m_dUIPposY  = m_hp3IP.y();
    m_dUIPposZ  = m_hp3IP.z();
    
    m_iUmcTrueFSR           = m_iMCfsr;
    m_iUmcTruePi0           = m_iMCpi0;
    m_iUmcTrueGammaFromJpsi = m_iMCGammaFromJpsi;
    m_iUmcTrueOtherGamma    = m_iMCOtherGamma;
    m_iUmcTrueOther         = m_iMCOther;

		
    // signal untagged
    m_iUAnzGoodP        = -1;
    m_iUAnzGoodM        = -1;
    m_iUAnzGoodGamma    = -1;
    m_iUAnzPi0Tracks    = -1;
    m_iUAnz2Combs       = -1;
    m_iUAnz3Combs       = -1;
		m_dUTotalChi2       = m_dChisqMaxValue;
    m_dUPi01Chi2        = m_dChisqMaxValue;
    m_dUPi02Chi2        = m_dChisqMaxValue;
		m_dUSumEemc					=0;
		m_iIsU2Pi0 					=0;
		m_NsharedGam				=0;
		
    m_UPi01.Clear();
    m_UPi02.Clear();
		
		for (int a=0;a<GAMMAS;a++) {m_UGammas[a].Clear();}
			
    m_UFitPi01.Clear();
    m_UFitPi02.Clear();
    for (int a=0;a<5;a++) {m_UFitGammas[a].Clear();}
    m_dUVtxChi2 = -1;
    m_dUVtxPosX = -1;
    m_dUVtxPosY = -1;
    m_dUVtxPosZ = -20;
		
		
		InitGammaContainers("Untagged");
		Init2GammaContainers("Untagged");
		
		//m_lvBoost            = HepLorentzVector(m_dCMSEnergy*sin(m_dCrossingAngle),0,0,m_dCMSEnergy);
		
		//------ preselector: skip, if there are insufficient amounts of tracks ------//
    //if (m_iNCharged > 0 || m_iNNeutral > 50) {
		if (m_iNCharged > 0) {
        //cout<<"# insuficinet tracks: ch= "<<evtRecEvent->totalCharged()<<" , neut= "<<evtRecEvent->totalNeutral()<<endl;
        m_iUPreSkip ++;
        return;
    }
		m_iUPreKept++;
    //cout<<"passed preselection: charged= "<<evtRecEvent->totalCharged()<<" , neutral= "<<evtRecEvent->totalNeutral()<<endl;
		

		//------ Find 2 pi0 candidates ------//
		
		EvtRecTrackIterator    itERT;
		
		//------ Loop over neutral tracks ------//
		
		for (itERT = m_itEndCharged;itERT != m_itEndNeutral;itERT ++) {
				int index = std::distance(m_itEndCharged,itERT);
				m_maphUNumGamvsEnGam[0]->fill((*itERT)->emcShower()->energy(), index,1);
				if ((*itERT)->isEmcShowerValid()) m_dUSumEemc+=(*itERT)->emcShower()->energy();			
    }
		
		FillGammaContainer("Untagged",0);
		m_iUAnzGoodGamma=(m_mapUGList[0].GetGamTable()).size();	
		
		FillHistograms("Untagged",0);
		
    if (m_iUAnzGoodGamma < 4 || m_iUAnzGoodGamma > m_dMaxGoodPh) {m_iUPhotonSkip ++; return;}		
		m_iUPhotonKept++;
		FillGammaContainer("Untagged",1);
		
		//Add good photons to the pi0 candidates list
		
		//Fill 2 gamma containers
	
		
		Fill2GammaContainer("Untagged",1);
		Fill2x2GammaContainer("Untagged",1);
		
		FillHistograms("Untagged",1);
			
		//------ Loops over pions candidates ------//
		
		//Find good 2 gamma combinations		
		
		
		if ((m_mapU2GList[1].GetPi0Tracks()).size() > m_dMaxPi0Combs) {m_iUPi0Skip ++; return;}
		FillGammaContainer("Untagged",2);
		Fill2GammaContainer("Untagged",2);
		Fill2x2GammaContainer("Untagged",2);
		
	 	//------ End of loops over pions candidates ------//
		FillHistograms("Untagged",2);
		
		FillGammaContainer("Untagged",3);
		Fill2GammaContainer("Untagged",3);
		Fill2x2GammaContainer("Untagged",3);
    if (m_mapU2GList[3].m_aValidCombs2.size() < 1) {
		m_dUTotalChi2 = -5; 
		m_dUPi01Chi2 = -5; 
		m_dUPi02Chi2 = -5;
		m_iUPi0Skip ++; 
		return;} // insufficient number of 2Pi0 candidates
		m_iUPi0kept++;
			
    // record combinatorics
    m_iUAnzPi0Tracks  = m_mapU2GList[3].m_aPi0TracksTable.size();
    m_iUAnz2Combs     = m_mapU2GList[3].m_aValidCombs2.size();
		
		FillHistograms("Untagged",3);
			
		//------ END of Find 2 pi0 candidates------//
		
		//------ Untagged 2Pi0 selection ------//
		//Find good 2 photons combinations
		//bool Iselpos2Pi0Untagged = Findelpos2Pi0Untagged();
		
		vector<cPi0Tracks> bestpt2G;
		bestpt2G.resize(2);
			 
		if(m_bKalFit){	 
    bool bDone = false;
		//------ With Kinematic FIT ------//
		// try untagged fit
		int Nfits=0;
		
    for(int k=0;k<m_mapU2GList[3].m_aValidCombs2.size();k++) {	
									
					cPi0Tracks* pt2G1 = (m_mapU2GList[3].m_aValidCombs2[k]).m_pTrack1;
					HepLorentzVector lv2G1G1= m_mapU2GList[3].Get4VecFromEMC(pt2G1->m_pTrack1,0.0);
					HepLorentzVector lv2G1G2= m_mapU2GList[3].Get4VecFromEMC(pt2G1->m_pTrack2,0.0);
					HepLorentzVector lv2G1 = lv2G1G1+lv2G1G2;
					cPi0Tracks* pt2G2 = (m_mapU2GList[3].m_aValidCombs2[k]).m_pTrack2;
					HepLorentzVector lv2G2G1= m_mapU2GList[3].Get4VecFromEMC(pt2G2->m_pTrack1,0.0);
					HepLorentzVector lv2G2G2= m_mapU2GList[3].Get4VecFromEMC(pt2G2->m_pTrack2,0.0);
					HepLorentzVector lv2G2 = lv2G2G1+lv2G2G2;
					
					//Missing Mass, Energy
					HepLorentzVector misslv = m_lvBeamElectron+m_lvBeamPositron-(lv2G1+lv2G2);
					m_dMissMass= misslv.m();
					m_dMissE= misslv.e();
					
					m_dDeltaEemcE4Gam=(m_dUSumEemc-(lv2G1G1+lv2G1G2+lv2G2G1+lv2G2G2).e());
					//Kinematics in e+e- CM frame	
					vector <HepLorentzVector> veclv;
					veclv.push_back(m_lvBeamElectron);
					veclv.push_back(m_lvBeamPositron);
					veclv.push_back(lv2G1);
					veclv.push_back(lv2G1G1);
					veclv.push_back(lv2G1G2);
					veclv.push_back(lv2G2);
					veclv.push_back(lv2G2G1);
					veclv.push_back(lv2G2G2);
					
					vector <HepLorentzVector> veclvCM = MakeBoostedVecLV(veclv,m_vectboostCM);
					HepLorentzVector lv2G1CM=veclvCM[2];
					HepLorentzVector lv2G2CM=veclvCM[5];
					
					m_dUSumPtCM=GetPt(lv2G1CM+lv2G2CM);
					
					//Helicity Angle
					HepLorentzVector lv2G1G12GCM=lv2G1G1;
					lv2G1G12GCM.boost(-(lv2G1.boostVector()));
					m_dCosTheta2G1HelicityG1=(lv2G1G12GCM.vect()).cosTheta(lv2G1.vect());
					
					HepLorentzVector lv2G2G12GCM=lv2G2G1;
					lv2G2G12GCM.boost(-(lv2G2.boostVector()));
					m_dCosTheta2G2HelicityG1=(lv2G2G12GCM.vect()).cosTheta(lv2G2.vect());
					
					m_maphU2G1InvM[3]->fill(lv2G1.m(),1);
					m_maphU2G2InvM[3]->fill(lv2G2.m(),1);
					m_maphUEAsym2G1[3]->fill(GetEAsym2Gam(lv2G1G1,lv2G1G2),1);
					m_maphUEAsym2G2[3]->fill(GetEAsym2Gam(lv2G2G1,lv2G2G2),1);
					m_maphU2x2GMissM[3]->fill(m_dMissMass);
					m_maphU2x2GMissE[3]->fill(m_dMissE);
					m_maphU2x2GDeltaEemcE4Gam[3]->fill(m_dDeltaEemcE4Gam);
					m_maphU2x2GCTheta2G1HelicityG1[3]->fill(m_dCosTheta2G1HelicityG1,1);
					m_maphU2x2GCTheta2G2HelicityG1[3]->fill(m_dCosTheta2G2HelicityG1,1);
					m_maphU2x2GInvM[3]->fill((lv2G1+lv2G2).m(),1);
					m_maphU2x2GSumPt[3]->fill(m_dUSumPtCM,1);
					
				
			//Pi0 mass contrained fit of the decay photons
			m_KalKinFit->init();
    	//m_KalKinFit->setChisqCut(m_dChisqMaxValue);
			cPi0Tracks* ptPi01 = m_mapU2GList[3].m_aValidCombs2[k].m_pTrack1;
				cPi0Tracks* ptPi02 = m_mapU2GList[3].m_aValidCombs2[k].m_pTrack2;
			m_KalKinFit->AddTrack(0,0.0,ptPi01->m_pTrack1->emcShower());
    	m_KalKinFit->AddTrack(1,0.0,ptPi01->m_pTrack2->emcShower());
      m_KalKinFit->AddTrack(2,0.0,ptPi02->m_pTrack1->emcShower());
      m_KalKinFit->AddTrack(3,0.0,ptPi02->m_pTrack2->emcShower());
      
			//Add pi0 mass constraint
			m_KalKinFit->AddResonance(0,m_dMassPi0,0,1);
			m_KalKinFit->AddResonance(1,m_dMassPi0,2,3);
			
			/*
			m_KalKinFit->AddMissTrack(4,0.0,m_lvBoost -m_mapU2GList[3].Get4VecFromEMC(ptPi01->m_pTrack1,0.0)
                                               -m_mapU2GList[3].Get4VecFromEMC(ptPi01->m_pTrack2,0.0)
                                               -m_mapU2GList[3].Get4VecFromEMC(ptPi02->m_pTrack1,0.0)
                                               -m_mapU2GList[3].Get4VecFromEMC(ptPi02->m_pTrack2,0.0));
			
			//Add 4-momentum constraint
			m_KalKinFit->AddFourMomentum(0,m_lvBoost);
			
			//Add pi0 mass constraint
			m_KalKinFit->AddResonance(1,m_dMassPi0,0,1);
			m_KalKinFit->AddResonance(2,m_dMassPi0,2,3);
			*/
																									 
			//Perform the fit
			if (!m_KalKinFit->Fit()) {continue;}
			Nfits++;
			m_maphUKinFitChi2[3]->fill((double)m_KalKinFit->chisq(),1);
			//Keep information for the 2Pi0 combination that has the best chi2 fit
      if (m_KalKinFit->chisq() > 0 && m_KalKinFit->chisq() < m_dUTotalChi2) {
				//if (m_KalKinFit->chisq() > 0 && m_KalKinFit->chisq() < m_dUTotalChi2 && m_KalKinFit->chisq()<m_dChisqMaxValue ) {    
				//Prepare to write 
				m_dUTotalChi2 = m_KalKinFit->chisq();
				// detector
    		m_lvPi01   = m_mapU2GList[3].Get4VecFromEMC(ptPi01->m_pTrack1,0.0) + m_mapU2GList[3].Get4VecFromEMC(ptPi01->m_pTrack2,0.0);
    		m_lvPi01G1 = m_mapU2GList[3].Get4VecFromEMC(ptPi01->m_pTrack1,0.0);
    		m_lvPi01G2 = m_mapU2GList[3].Get4VecFromEMC(ptPi01->m_pTrack2,0.0);
    		m_lvPi02   = m_mapU2GList[3].Get4VecFromEMC(ptPi02->m_pTrack1,0.0) + m_mapU2GList[3].Get4VecFromEMC(ptPi02->m_pTrack2,0.0);
    		m_lvPi02G1 = m_mapU2GList[3].Get4VecFromEMC(ptPi02->m_pTrack1,0.0);
    		m_lvPi02G2 = m_mapU2GList[3].Get4VecFromEMC(ptPi02->m_pTrack2,0.0);
    		m_aPhIndexList.clear();
   		  m_aPhIndexList.push_back (ptPi01->m_pTrack1);
    		m_aPhIndexList.push_back (ptPi01->m_pTrack2);
    		m_aPhIndexList.push_back (ptPi02->m_pTrack1);
    		m_aPhIndexList.push_back (ptPi02->m_pTrack2);
    		// fit
    		m_lvFitPi01G1 = m_KalKinFit->pfit(0);
    		m_lvFitPi01G2 = m_KalKinFit->pfit(1);
    		m_lvFitPi02G1 = m_KalKinFit->pfit(2);
    		m_lvFitPi02G2 = m_KalKinFit->pfit(3);
    		m_lvFitPi01   = m_KalKinFit->pfit(0) + m_KalKinFit->pfit(1);
    		m_lvFitPi02   = m_KalKinFit->pfit(2) + m_KalKinFit->pfit(3);
    		
    		// the lines below MUSST stay below the settings of the 4vectors!
				if (m_KalKinFit->Fit(0)) {m_dUPi01Chi2 = m_KalKinFit->chisq(0);} else {m_dUPi01Chi2 = -5;}
    		if (m_KalKinFit->Fit(1)) {m_dUPi02Chi2 = m_KalKinFit->chisq(1);} else {m_dUPi02Chi2 = -5;}
				
				bestpt2G[0]= *ptPi01;
				bestpt2G[1]= *ptPi02;
				
				m_maphUKinFitChi2[4]->fill((double)m_KalKinFit->chisq(),1);
        bDone = true;
      }
    }
		
		m_hU2CombswValidFit->fill(Nfits,1);

if (!bDone) {m_dUTotalChi2 = -5;m_dUPi01Chi2 = -5; m_dUPi02Chi2 = -5; return;}
m_iUFitKept++;
		
		}	
		else{
			//------ Without Kinematic FIT ------//
			if (m_mapU2GList[3].m_aValidCombs2.size() != 1) {return;}
			for(int k=0;k<m_mapU2GList[3].m_aValidCombs2.size();k++) {
			cPi0Tracks* ptPi01 = m_mapU2GList[3].m_aValidCombs2[k].m_pTrack1;
			cPi0Tracks* ptPi02 = m_mapU2GList[3].m_aValidCombs2[k].m_pTrack2;
			
    		// detector
    		m_lvPi01   = m_mapU2GList[3].Get4VecFromEMC(ptPi01->m_pTrack1,0.0) + m_mapU2GList[3].Get4VecFromEMC(ptPi01->m_pTrack2,0.0);
    		m_lvPi01G1 = m_mapU2GList[3].Get4VecFromEMC(ptPi01->m_pTrack1,0.0);
    		m_lvPi01G2 = m_mapU2GList[3].Get4VecFromEMC(ptPi01->m_pTrack2,0.0);
    		m_lvPi02   = m_mapU2GList[3].Get4VecFromEMC(ptPi02->m_pTrack1,0.0) + m_mapU2GList[3].Get4VecFromEMC(ptPi02->m_pTrack2,0.0);
    		m_lvPi02G1 = m_mapU2GList[3].Get4VecFromEMC(ptPi02->m_pTrack1,0.0);
    		m_lvPi02G2 = m_mapU2GList[3].Get4VecFromEMC(ptPi02->m_pTrack2,0.0);
    		m_aPhIndexList.clear();
   		  m_aPhIndexList.push_back (ptPi01->m_pTrack1);
    		m_aPhIndexList.push_back (ptPi01->m_pTrack2);
    		m_aPhIndexList.push_back (ptPi02->m_pTrack1);
    		m_aPhIndexList.push_back (ptPi02->m_pTrack2);	
				
				bestpt2G[0]= *ptPi01;
				bestpt2G[1]= *ptPi02;	
    }
		}

//Fill the selected Pi0 candidates into a new Pi0TrackList
m_mapU2GList[4].Add(bestpt2G[0]);
m_mapU2GList[4].Add(bestpt2G[1]);	
Fill2x2GammaContainer("Untagged",4);
					
//Fill particle containers
m_UPi01.Fill(&m_lvPi01);
m_UPi02.Fill(&m_lvPi02);
m_UGammas[0].Fill(&m_lvPi01G1);
m_UGammas[1].Fill(&m_lvPi01G2);
m_UGammas[2].Fill(&m_lvPi02G1);
m_UGammas[3].Fill(&m_lvPi02G2);
m_UFitPi01.Fill(&m_lvFitPi01);
m_UFitPi02.Fill(&m_lvFitPi02);
m_UFitGammas[0].Fill(&m_lvFitPi01G1);
m_UFitGammas[1].Fill(&m_lvFitPi01G2);
m_UFitGammas[2].Fill(&m_lvFitPi02G1);
m_UFitGammas[3].Fill(&m_lvFitPi02G2);

m_iUNPi0=0;
for(int k=0;k<2;k++)
	{
	m_dU2GOpeningAngle[m_iUNPi0] = bestpt2G[k].Get2GOpeningAngle();
	m_dU2GDeltaTheta[m_iUNPi0] = bestpt2G[k].Get2GDeltaTheta();
	m_dU2GDeltaPhi[m_iUNPi0] = bestpt2G[k].Get2GDeltaPhi();
	m_iUNPi0++;
	}
//Missing Mass,Energy
HepLorentzVector misslv = m_lvBeamElectron+m_lvBeamPositron-(m_lvPi01+m_lvPi02);
m_dUMissMass=misslv.m();
m_dUMissE=misslv.e();

m_dDeltaEemcE4Gam=(m_dUSumEemc-(m_lvPi01G1+m_lvPi01G2+m_lvPi02G1+m_lvPi02G2).e());
m_dUDeltaEemcE4Gam=m_dDeltaEemcE4Gam;

//Invariant Mass
m_dUPi01InvM=m_lvPi01.m();
m_dUPi02InvM=m_lvPi02.m();
m_dU2Pi0InvM=(m_lvPi01+m_lvPi02).m();

m_dUFitPi01InvM=m_lvFitPi01.m();
m_dUFitPi02InvM=m_lvFitPi02.m();
m_dUFit2Pi0InvM=(m_lvFitPi01+m_lvFitPi02).m();

//Energy Asymmetry
m_dUEAsymPi01=GetEAsym2Gam(m_lvPi01G1,m_lvPi01G2);
m_dUEAsymPi02=GetEAsym2Gam(m_lvPi02G1,m_lvPi02G2);
m_dUFitEAsymPi01=GetEAsym2Gam(m_lvFitPi01G1,m_lvFitPi01G2);
m_dUFitEAsymPi02=GetEAsym2Gam(m_lvFitPi02G1,m_lvFitPi02G2);

//Kinematics in e+e- CM frame	
vector <HepLorentzVector> veclv;
veclv.push_back(m_lvBeamElectron);
veclv.push_back(m_lvBeamPositron);
veclv.push_back(m_lvPi01);
veclv.push_back(m_lvPi01G1);
veclv.push_back(m_lvPi01G2);
veclv.push_back(m_lvPi02);
veclv.push_back(m_lvPi02G1);
veclv.push_back(m_lvPi02G2);

vector <HepLorentzVector> veclvFit;
veclvFit.push_back(m_lvBeamElectron);
veclvFit.push_back(m_lvBeamPositron);
veclvFit.push_back(m_lvFitPi01);
veclvFit.push_back(m_lvFitPi01G1);
veclvFit.push_back(m_lvFitPi01G2);
veclvFit.push_back(m_lvFitPi02);
veclvFit.push_back(m_lvFitPi02G1);
veclvFit.push_back(m_lvFitPi02G2);
veclvFit.push_back(m_lvFitGammaStar);

vector <HepLorentzVector> veclvCM = MakeBoostedVecLV(veclv,m_vectboostCM);
m_lvPi01CM=veclvCM[2];
m_lvPi02CM=veclvCM[5];

vector <HepLorentzVector> veclvFitCM = MakeBoostedVecLV(veclvFit,m_vectboostCM);
m_lvFitPi01CM=veclvFitCM[2];
m_lvFitPi02CM=veclvFitCM[5];

//Transverse Momentum
m_dUSumPtCM=GetPt(m_lvPi01CM+m_lvPi02CM);
m_dUFitSumPtCM=GetPt(m_lvFitPi01CM+m_lvFitPi02CM);

//Energy momentum conservation test
m_dUPbeamelecpositron=(m_lvBeamElectron+m_lvBeamPositron).mag();
m_dUPCMbeamelecpositron=(m_lvBeamElectronCM+m_lvBeamPositronCM).mag();

m_dUPi01CMPx=(m_lvPi01CM.vect()).x();
m_dUPi01CMPy=(m_lvPi01CM.vect()).y();
m_dUPi01CMPz=(m_lvPi01CM.vect()).z();
//m_dUPi01CMPt=(m_lvPi01CM.vect()).perp();
m_dUPi01CMPt=GetPt(m_lvPi01CM);
m_dUPi02CMPx=(m_lvPi02CM.vect()).x();
m_dUPi02CMPy=(m_lvPi02CM.vect()).y();
m_dUPi02CMPz=(m_lvPi02CM.vect()).z();
//m_dUPi02CMPt=(m_lvPi02CM.vect()).perp();
m_dUPi02CMPt=GetPt(m_lvPi02CM);

m_UPi01CM.Fill(&m_lvPi01CM);
m_UPi02CM.Fill(&m_lvPi02CM);

//Kinematics in 2 Gammas CM frame	
m_boost2Pi0=(m_lvFitPi01+m_lvPi02).boostVector();
vector <HepLorentzVector> veclvCM2Gam = MakeBoostedVecLV(veclv,m_boost2Pi0);
m_lvPi01CM2Gam=veclvCM2Gam[2];

m_boostFit2Pi0=(m_lvFitPi01+m_lvFitPi02).boostVector();
vector <HepLorentzVector> veclvFitCM2Gam = MakeBoostedVecLV(veclvFit,m_boostFit2Pi0);
m_lvFitPi01CM2Gam=veclvFitCM2Gam[2];

//Cos theta
//Z axis for Gamma-Gamma CM frame : Z direction from e+ beam in e+e- rest frame
Hep3Vector ZAxisGam2Pi0CM = (m_lvBeamPositronCM.vect()).unit();
double ThetaPi01CM2Gam;
ThetaPi01CM2Gam=m_lvFitPi01CM2Gam.angle(ZAxisGam2Pi0CM);
m_dUFitcosThetaPi01CM2Gam=cos(ThetaPi01CM2Gam);

ThetaPi01CM2Gam=m_lvPi01CM2Gam.angle(ZAxisGam2Pi0CM);
m_dUcosThetaPi01CM2Gam=cos(ThetaPi01CM2Gam);

//Helicity angle
HepLorentzVector lv2G1=m_lvPi01; 
HepLorentzVector lvG12GCM=m_lvPi01G1;
m_dUCThetaHelicity=GetCThetaHelicity(lv2G1,lvG12GCM);

HepLorentzVector lvFit2G1=m_lvFitPi01; 
HepLorentzVector lvFitG12GCM=m_lvFitPi01G1;
m_dUFitCThetaHelicity=GetCThetaHelicity(lvFit2G1,lvFitG12GCM);
		
FillHistograms("Untagged",4);

bool bStage5OK=m_dUTotalChi2<m_dChisqMaxValue;
bool bStage6OK=m_dUSumEemc<m_dUSumEemcMax;
bool bStage7OK=m_dUSumPtCM<= m_dUSumPtMax;

if(bStage5OK && bStage6OK && bStage7OK) m_iIsU2Pi0=1;

if(!bStage5OK){return;}
m_mapU2GList[5].Add(bestpt2G[0]);
m_mapU2GList[5].Add(bestpt2G[1]);	
Fill2x2GammaContainer("Untagged",5);

FillHistograms("Untagged",5);

if(!bStage6OK){return;}
m_mapU2GList[6].Add(bestpt2G[0]);
m_mapU2GList[6].Add(bestpt2G[1]);	
Fill2x2GammaContainer("Untagged",6);

FillHistograms("Untagged",6);
		
if(!bStage7OK){return;}
m_mapU2GList[7].Add(bestpt2G[0]);
m_mapU2GList[7].Add(bestpt2G[1]);	
Fill2x2GammaContainer("Untagged",7);

FillHistograms("Untagged",7);
		
m_NUntagged->write();
m_iAcceptedU ++;

//------ END of Untagged 2Pi0 selection ------//


return;
}
//---------------------------------------------------------------------------------------------------------- 

//---------------------------------------------------------------------------------------------------------- 
void Bes2Gamto2Pi0::DoTagged()
{

MsgStream log(msgSvc(), name());
    log << MSG::INFO << "in execute()" << endreq;


		
SmartDataPtr<Event::EventHeader> eventHeader(eventSvc(),"/Event/EventHeader");
SmartDataPtr<EvtRecEvent> evtRecEvent(eventSvc(), "/Event/EvtRec/EvtRecEvent");
    if ( ! evtRecEvent ) {
        log << MSG::FATAL << "Could not find EvtRecEvent" << endreq;
        return;
    }
    SmartDataPtr<EvtRecTrackCol> evtRecTrackCol(eventSvc(), "/Event/EvtRec/EvtRecTrackCol");
    if ( ! evtRecTrackCol ) {
        log << MSG::FATAL << "Could not find EvtRecTrackCol" << endreq;
        return;
    }
    Gaudi::svcLocator()->service("VertexDbSvc", m_vtxSvc);


m_dT2GRunNumber   = m_dRunNumber;
m_dT2GEvtNumber  = m_dEvtNumber;
m_dT2GCMSEnergy = m_dCMSEnergy;
m_iT2GN2Gs=0;

m_dT2x2GRunNumber   = m_dRunNumber;
m_dT2x2GEvtNumber  = m_dEvtNumber;
m_dT2x2GCMSEnergy = m_dCMSEnergy;
m_iT2x2GN2x2G=0;


m_dTRunNumber   = m_dRunNumber;
m_dTEvtNumber  = m_dEvtNumber;
m_dTCMSEnergy = m_dCMSEnergy;	

//IP pos
m_dTIPposX  = m_hp3IP.x();
m_dTIPposY  = m_hp3IP.y();
m_dTIPposZ  = m_hp3IP.z();


   
m_iTmcTrueFSR           = m_iMCfsr;
m_iTmcTruePi0           = m_iMCpi0;
m_iTmcTrueGammaFromJpsi = m_iMCGammaFromJpsi;
m_iTmcTrueOtherGamma    = m_iMCOtherGamma;
m_iTmcTrueOther         = m_iMCOther;

		
// signal untagged
//int NLeptons=0;
int idxElectron=-1;
m_bHas1Lepton=false;
m_iTAnzGoodP        = -1;
m_iTAnzGoodM        = -1;
m_iTAnzGoodGamma    = -1;
m_iTAnzPi0Tracks    = -1;
m_iTAnz2Combs       = -1;
m_iTAnz3Combs       = -1;
m_dTTotalChi2       = m_dChisqMaxValue;
m_dTPi01Chi2        = m_dChisqMaxValue;
m_dTPi02Chi2        = m_dChisqMaxValue;
m_iNLeptons					= 0;
m_iTQTag						= -99;
m_dTSumEemc				=0;
m_iIsT2Pi0					= 0;
m_NsharedGam				=0;

m_TLepton.Clear();		
m_TPi01.Clear();
m_TPi02.Clear();	
//for (int a=0;a<GAMMAS;a++) {m_UGammas[a].Clear(); m_dUClosestTrack[a] = -4.0;}
for (int a=0;a<GAMMAS;a++) {m_TGammas[a].Clear();}

m_TFitLepton.Clear();			
m_TFitPi01.Clear();
m_TFitPi02.Clear();
for (int a=0;a<5;a++) {m_TFitGammas[a].Clear();}

m_dTVtxChi2 = -1;
m_dTVtxPosX = -1;
m_dTVtxPosY = -1;
m_dTVtxPosZ = -20;


m_aT2GList.SetIterators (m_itBegin,m_itEndCharged,m_itEndNeutral);
m_aT2GList.Init(m_dPi0MassCutLow,m_dPi0MassCutHigh,m_dPhEnergyCutBarrel,m_dPhEnergyCutEndCap,m_dCosThetaBarrel,m_dMaxTime,m_dCMSEnergy/2.0);

m_aTPi0List.SetIterators (m_itBegin,m_itEndCharged,m_itEndNeutral);		
m_aTPi0List.Init(m_dPi0MassCutLow,m_dPi0MassCutHigh,m_dPhEnergyCutBarrel,m_dPhEnergyCutEndCap,m_dCosThetaBarrel,m_dMaxTime,m_dCMSEnergy/2.0);

InitGammaContainers("Tagged");
Init2GammaContainers("Tagged");
InitChargedTracksContainer("Tagged");
 
EvtRecTrackIterator  itERT;
    double       dDist, dMinDistP = 100, dMinDistN = 100;
    double       dEoP,dEoPP,dEoPN;

vector<int> key; key.resize(2);

FillChargedTracksContainer("Tagged",0);
FillHistograms("Tagged",0);
			 
if(m_bHas1Lepton==false) return;
m_iT1Lepton++;

for (itERT = m_itEndCharged;itERT != m_itEndNeutral;itERT ++) {
				int index = std::distance(m_itEndCharged,itERT);
				if ((*itERT)->isEmcShowerValid()) {m_dTSumEemc+=(*itERT)->emcShower()->energy();}	
    }

FillChargedTracksContainer("Tagged",1);
FillGammaContainer("Tagged",1);
FillHistograms("Tagged",1);
FillHistograms("eTagged",1);
FillHistograms("pTagged",1);

		m_iTAnzGoodGamma=(m_mapTGList[1].GetGamTable()).size();	
		
    if (m_iTAnzGoodGamma < 4 || m_iTAnzGoodGamma > m_dMaxGoodPh) {return;}
		m_iTPhotonKept++;
		
		FillGammaContainer("Tagged",2);
		Fill2GammaContainer("Tagged",2);
		Fill2x2GammaContainer("Tagged",2);
		
		FillHistograms("Tagged",2);
		FillHistograms("eTagged",2);
		FillHistograms("pTagged",2);
		
		

    if (m_mapT2GList[2].m_aPi0TracksTable.size() > m_dMaxPi0Combs) {return;}
		
		FillGammaContainer("Tagged",3);
		Fill2GammaContainer("Tagged",3);
		Fill2x2GammaContainer("Tagged",3);
		
		FillHistograms("Tagged",3);
		FillHistograms("eTagged",3);
		FillHistograms("pTagged",3);
		
//m_maphTValid2Combs[3]->fill(m_mapT2GList[4].m_aValidCombs2.size(),1.);
//m_maphT2CombswSharedGammas[3]->fill(m_NsharedGam,1);

//printf("%d %d \n",m_mapT2GList[4].m_aPi0TracksTable.size(),m_mapT2GList[4].m_aValidCombs2.size());


if (m_mapT2GList[3].m_aValidCombs2.size() < 1) {
		m_dTTotalChi2 = -5; 
		m_dTPi01Chi2 = -5; 
		m_dTPi02Chi2 = -5;
		return;} // insufficient number of 2Pi0 candidates
		m_iTPi0kept++;
		
		FillGammaContainer("Tagged",4);
		Fill2GammaContainer("Tagged",4);
		Fill2x2GammaContainer("Tagged",4);
		
    // record combinatorics
    m_iTAnzPi0Tracks  = m_mapT2GList[4].m_aPi0TracksTable.size();
    m_iTAnz2Combs     = m_mapT2GList[4].m_aValidCombs2.size();
		
		FillHistograms("Tagged",4);
		FillHistograms("eTagged",4);
		FillHistograms("pTagged",4);
		
		//m_maphTValid2Combs[4]->fill(m_mapT2GList[4].m_aValidCombs2.size(),1.);
		//m_maphT2CombswSharedGammas[4]->fill(m_NsharedGam,1);
		//------ END of Find 2 pi0 candidates------//
		
		//------ e-Tagged 2Pi0 selection ------//		
		m_iT2x2GNSizePi0TrackTable=m_mapT2GList[4].m_aPi0TracksTable.size();
		//printf("%d %d \n",m_mapT2GList[4].m_aPi0TracksTable.size(),m_mapT2GList[4].m_aValidCombs2.size());
		
		vector<cPi0Tracks> bestpt2G;
		bestpt2G.resize(2);
		
		if(m_bKalFit){	 
    bool bDone = false;
		//------ With Kinematic FIT ------//
		int Nfits=0;
    for(int k=0;k<m_mapT2GList[4].m_aValidCombs2.size();k++) {
					HepVector w_zHel(5,0);
					w_zHel = m_pERTLepton->mdcKalTrack()->getZHelix();
    			WTrackParameter wtpElec (m_dMasselec,w_zHel,m_pERTLepton->mdcKalTrack()->getZError());
					m_lvLepton= wtpElec.p();
					cPi0Tracks* pt2G1 = (m_mapT2GList[4].m_aValidCombs2[k]).m_pTrack1;
					HepLorentzVector lv2G1G1= m_mapT2GList[4].Get4VecFromEMC(pt2G1->m_pTrack1,0.0);
					HepLorentzVector lv2G1G2= m_mapT2GList[4].Get4VecFromEMC(pt2G1->m_pTrack2,0.0);
					HepLorentzVector lv2G1 = lv2G1G1+lv2G1G2;
					cPi0Tracks* pt2G2 = (m_mapT2GList[4].m_aValidCombs2[k]).m_pTrack2;
					HepLorentzVector lv2G2G1= m_mapT2GList[4].Get4VecFromEMC(pt2G2->m_pTrack1,0.0);
					HepLorentzVector lv2G2G2= m_mapT2GList[4].Get4VecFromEMC(pt2G2->m_pTrack2,0.0);
					HepLorentzVector lv2G2 = lv2G2G1+lv2G2G2;
					
					//Missing Mass	
					HepLorentzVector lvMissLept=m_lvBeamElectron+m_lvBeamPositron-(m_lvLepton+lv2G1+lv2G2);
					m_dMissMass= lvMissLept.m();
					m_dMissE= lvMissLept.e();
					m_dCosThetaMissLepton	= lvMissLept.cosTheta();
					
					//Kinematics in e+e- CM frame	
					vector <HepLorentzVector> veclv;
					veclv.push_back(m_lvBeamElectron);
					veclv.push_back(m_lvBeamPositron);
					veclv.push_back(m_lvLepton);
					veclv.push_back(lv2G1);
					veclv.push_back(lv2G1G1);
					veclv.push_back(lv2G1G2);
					veclv.push_back(lv2G2);
					veclv.push_back(lv2G2G1);
					veclv.push_back(lv2G2G2);
					
					vector <HepLorentzVector> veclvCM = MakeBoostedVecLV(veclv,m_vectboostCM);
					HepLorentzVector lvLeptonCM=veclvCM[2];
					HepLorentzVector lv2G1CM=veclvCM[3];
					HepLorentzVector lv2G2CM=veclvCM[6];

					m_dTSumPtCM=GetPt(lvLeptonCM+lv2G1CM+lv2G2CM);
					
					//Helicity Angle				
					HepLorentzVector lv2G1G12GCM=lv2G1G1;
					lv2G1G12GCM.boost(-lv2G1.boostVector());
					m_dCosTheta2G1HelicityG1=(lv2G1G12GCM.vect()).cosTheta(lv2G1.vect());
					
					HepLorentzVector lv2G2G12GCM=lv2G2G1;
					lv2G2G12GCM.boost(-lv2G2.boostVector());
					m_dCosTheta2G2HelicityG1=(lv2G2G12GCM.vect()).cosTheta(lv2G2.vect());
					
					//RGamma
					double ELept2x2GCM;
					double AbsMomLept2x2GCM;

					ELept2x2GCM=(lvLeptonCM+lv2G1CM+lv2G2CM).e();
					AbsMomLept2x2GCM=((lvLeptonCM+lv2G1CM+lv2G2CM).vect()).mag();
					m_dRGam=(m_lvBoost.m()-ELept2x2GCM-AbsMomLept2x2GCM)/m_lvBoost.m();
					m_dT2x2GE2G1[m_iT2x2GN2x2G]=lv2G1.e();
					m_dT2x2GE2G2[m_iT2x2GN2x2G]=lv2G2.e();	
					m_dT2x2GInvM2G1[m_iT2x2GN2x2G]=lv2G1.m();					
					m_dT2x2GInvM2G2[m_iT2x2GN2x2G]=lv2G2.m();	
					m_dT2x2GEAsym2G1[m_iT2x2GN2x2G] =	GetEAsym2Gam(lv2G1G1,lv2G1G2);
					m_dT2x2GEAsym2G2[m_iT2x2GN2x2G] =	GetEAsym2Gam(lv2G2G1,lv2G2G2);
					m_dT2x2GCThetaMiss[m_iT2x2GN2x2G]=m_dCosThetaMissLepton;
					m_dT2x2GInvM2x2G[m_iT2x2GN2x2G]=(lv2G1+lv2G2).m();
					m_dT2x2GSumPtCM[m_iT2x2GN2x2G]=m_dTSumPtCM;
					m_dT2x2GCTheta2G1HelicityG1[m_iT2x2GN2x2G]=m_dCosTheta2G1HelicityG1;
					m_dT2x2GCTheta2G2HelicityG1[m_iT2x2GN2x2G]=m_dCosTheta2G2HelicityG1;
					m_dT2x2GRGam[m_iT2x2GN2x2G]=m_dRGam;
					
					m_iT2x2GN2x2G++;	
					
					for(int iTagType=0;iTagType<3;iTagType++){	
					key[0]=iTagType; key[1]=4;
					if(iTagType==1)
						{
						if(m_iTQTag>=0) continue;
						}
					if(iTagType==2)
						{
						if(m_iTQTag<=0) continue;
						}	
							
					m_maphT2G1InvM[key]->fill(lv2G1.m(),1);
					m_maphT2G2InvM[key]->fill(lv2G2.m(),1);
					m_maphTEAsym2G1[key]->fill(GetEAsym2Gam(lv2G1G1,lv2G1G2),1);
					m_maphTEAsym2G2[key]->fill(GetEAsym2Gam(lv2G2G1,lv2G2G2),1);
					m_maphT2x2GMissM[key]->fill(m_dMissMass,1);
					m_maphT2x2GMissE[key]->fill(m_dMissE,1);
					m_maphT2x2GCThetaMiss[key]->fill(m_dCosThetaMissLepton,1);
					m_maphT2x2GCTheta2G1HelicityG1[key]->fill(m_dCosTheta2G1HelicityG1,1);
					m_maphT2x2GCTheta2G2HelicityG1[key]->fill(m_dCosTheta2G2HelicityG1,1);
					m_maphT2x2GInvM[key]->fill((lv2G1+lv2G2).m(),1);
					m_maphT2x2GSumPt[key]->fill(m_dTSumPtCM,1);
					//m_maphT2x2GcosTheta2G1CM2Gam[key]->fill(,1);
					m_maphT2x2GRGam[key]->fill(m_dRGam);		
					}
					
				/*	
			//6C Kinematic fit
			
			//Pi0 mass contrained fit of the decay photons
			m_KalKinFit->init();
    	//m_KalKinFit->setChisqCut(m_dChisqMaxValue);;
			cPi0Tracks* ptPi01 = m_mapT2GList[4].m_aValidCombs2[k].m_pTrack1;
			cPi0Tracks* ptPi02 = m_mapT2GList[4].m_aValidCombs2[k].m_pTrack2;	
			m_KalKinFit->AddTrack(0,wtpElec);	
			m_KalKinFit->AddTrack(1,0.0,ptPi01->m_pTrack1->emcShower());
    	m_KalKinFit->AddTrack(2,0.0,ptPi01->m_pTrack2->emcShower());
      m_KalKinFit->AddTrack(3,0.0,ptPi02->m_pTrack1->emcShower());
      m_KalKinFit->AddTrack(4,0.0,ptPi02->m_pTrack2->emcShower());
      m_KalKinFit->AddMissTrack(5,0.0, m_lvBoost-wtpElec.p()
                                               -m_mapT2GList[4].Get4VecFromEMC(ptPi01->m_pTrack1,0.0)
                                               -m_mapT2GList[4].Get4VecFromEMC(ptPi01->m_pTrack2,0.0)
																							 -m_mapT2GList[4].Get4VecFromEMC(ptPi02->m_pTrack1,0.0)
                                               -m_mapT2GList[4].Get4VecFromEMC(ptPi02->m_pTrack2,0.0));
			
			//Add pi0 mass constraint
			m_KalKinFit->AddFourMomentum(0,m_lvBoost);
			m_KalKinFit->AddResonance(1,m_dMassPi0,1,2);
			m_KalKinFit->AddResonance(2,m_dMassPi0,3,4);
			
																							 
			//Perform the fit
			if (!m_KalKinFit->Fit()) {continue;}
			Nfits++;
				if (m_KalKinFit->chisq() > 0 && m_KalKinFit->chisq() < m_dTTotalChi2 ) {
				//if (m_KalKinFit->chisq() > 0 && m_KalKinFit->chisq() < m_dTTotalChi2 && m_KalKinFit->chisq()<m_dChisqMaxValue ) {    
				//Prepare to write 
				m_dTTotalChi2 = m_KalKinFit->chisq();
				// detector
				m_lvLepton = wtpElec.p();
				m_lvGammaStar = m_lvBeamElectron-m_lvLepton;
    		m_lvPi01   = m_mapT2GList[4].Get4VecFromEMC(ptPi01->m_pTrack1,0.0) + m_mapT2GList[4].Get4VecFromEMC(ptPi01->m_pTrack2,0.0);
    		m_lvPi01G1 = m_mapT2GList[4].Get4VecFromEMC(ptPi01->m_pTrack1,0.0);
    		m_lvPi01G2 = m_mapT2GList[4].Get4VecFromEMC(ptPi01->m_pTrack2,0.0);
    		m_lvPi02   = m_mapT2GList[4].Get4VecFromEMC(ptPi02->m_pTrack1,0.0) + m_mapT2GList[4].Get4VecFromEMC(ptPi02->m_pTrack2,0.0);
    		m_lvPi02G1 = m_mapT2GList[4].Get4VecFromEMC(ptPi02->m_pTrack1,0.0);
    		m_lvPi02G2 = m_mapT2GList[4].Get4VecFromEMC(ptPi02->m_pTrack2,0.0);
    		m_aPhIndexList.clear();
   		  m_aPhIndexList.push_back (ptPi01->m_pTrack1);
    		m_aPhIndexList.push_back (ptPi01->m_pTrack2);
    		m_aPhIndexList.push_back (ptPi02->m_pTrack1);
    		m_aPhIndexList.push_back (ptPi02->m_pTrack2);
    		// fit
				m_lvFitLepton = m_KalKinFit->pfit(0);
				m_lvFitGammaStar = m_lvBeamElectron-m_lvFitLepton;
    		m_lvFitPi01G1 = m_KalKinFit->pfit(1);
    		m_lvFitPi01G2 = m_KalKinFit->pfit(2);
    		m_lvFitPi02G1 = m_KalKinFit->pfit(3);
    		m_lvFitPi02G2 = m_KalKinFit->pfit(4);
    		m_lvFitPi01   = m_KalKinFit->pfit(1) + m_KalKinFit->pfit(2);
    		m_lvFitPi02   = m_KalKinFit->pfit(3) + m_KalKinFit->pfit(4);
    		
    		// the lines below MUSST stay below the settings of the 4vectors!
    		if (m_KalKinFit->Fit(1)) {m_dTPi01Chi2 = m_KalKinFit->chisq(1);} else {m_dTPi01Chi2 = -5;}
    		if (m_KalKinFit->Fit(2)) {m_dTPi02Chi2 = m_KalKinFit->chisq(2);} else {m_dTPi02Chi2 = -5;}			
				
        bDone = true;
				
				*/
				
				
				//2 C Kinematic Fit
				//Pi0 mass contrained fit of the decay photons
			m_KalKinFit->init();
    	//m_KalKinFit->setChisqCut(m_dChisqMaxValue);
			cPi0Tracks* ptPi01 = m_mapT2GList[4].m_aValidCombs2[k].m_pTrack1;
			cPi0Tracks* ptPi02 = m_mapT2GList[4].m_aValidCombs2[k].m_pTrack2;	
			m_KalKinFit->AddTrack(0,wtpElec);	
			m_KalKinFit->AddTrack(1,0.0,ptPi01->m_pTrack1->emcShower());
    	m_KalKinFit->AddTrack(2,0.0,ptPi01->m_pTrack2->emcShower());
      m_KalKinFit->AddTrack(3,0.0,ptPi02->m_pTrack1->emcShower());
      m_KalKinFit->AddTrack(4,0.0,ptPi02->m_pTrack2->emcShower());
			
			//Add pi0 mass constraint
			m_KalKinFit->AddResonance(0,m_dMassPi0,1,2);
			m_KalKinFit->AddResonance(1,m_dMassPi0,3,4);
			
																							 
			//Perform the fit
			if (!m_KalKinFit->Fit()) {continue;}
			Nfits++;
			//m_maphTKinFitChi2[4]->fill(m_KalKinFit->chisq(),1);
			for(int iTagType=0;iTagType<3;iTagType++){	
			key[0]=iTagType; key[1]=4;
					if(iTagType==1)
						{
						if(m_iTQTag>=0) continue;
						}
					if(iTagType==2)
						{
						if(m_iTQTag<=0) continue;
						}	
			m_maphTKinFitChi2[key]->fill(m_KalKinFit->chisq(),1);	
			}
			
				if (m_KalKinFit->chisq() > 0 && m_KalKinFit->chisq() < m_dTTotalChi2 && m_KalKinFit->chisq()<m_dChisqMaxValue ) {
				//cout<<  m_KalKinFit->chisq() << endl;   
				//Prepare to write 
				m_dTTotalChi2 = m_KalKinFit->chisq();
				// detector
				m_lvLepton = wtpElec.p();
				if(m_iTQTag==-1) m_lvGammaStar = m_lvBeamElectron-m_lvLepton;
				if(m_iTQTag==1) m_lvGammaStar = m_lvBeamPositron-m_lvLepton;
    		m_lvPi01   = m_mapT2GList[4].Get4VecFromEMC(ptPi01->m_pTrack1,0.0) + m_mapT2GList[4].Get4VecFromEMC(ptPi01->m_pTrack2,0.0);
    		m_lvPi01G1 = m_mapT2GList[4].Get4VecFromEMC(ptPi01->m_pTrack1,0.0);
    		m_lvPi01G2 = m_mapT2GList[4].Get4VecFromEMC(ptPi01->m_pTrack2,0.0);
    		m_lvPi02   = m_mapT2GList[4].Get4VecFromEMC(ptPi02->m_pTrack1,0.0) + m_mapT2GList[4].Get4VecFromEMC(ptPi02->m_pTrack2,0.0);
    		m_lvPi02G1 = m_mapT2GList[4].Get4VecFromEMC(ptPi02->m_pTrack1,0.0);
    		m_lvPi02G2 = m_mapT2GList[4].Get4VecFromEMC(ptPi02->m_pTrack2,0.0);
    		m_aPhIndexList.clear();
   		  m_aPhIndexList.push_back (ptPi01->m_pTrack1);
    		m_aPhIndexList.push_back (ptPi01->m_pTrack2);
    		m_aPhIndexList.push_back (ptPi02->m_pTrack1);
    		m_aPhIndexList.push_back (ptPi02->m_pTrack2);
    		// fit
				m_lvFitLepton = m_KalKinFit->pfit(0);
				if(m_iTQTag==-1) m_lvFitGammaStar = m_lvBeamElectron-m_lvFitLepton;
				if(m_iTQTag==1) m_lvFitGammaStar = m_lvBeamPositron-m_lvFitLepton;
    		m_lvFitPi01G1 = m_KalKinFit->pfit(1);
    		m_lvFitPi01G2 = m_KalKinFit->pfit(2);
    		m_lvFitPi02G1 = m_KalKinFit->pfit(3);
    		m_lvFitPi02G2 = m_KalKinFit->pfit(4);
    		m_lvFitPi01   = m_KalKinFit->pfit(1) + m_KalKinFit->pfit(2);
    		m_lvFitPi02   = m_KalKinFit->pfit(3) + m_KalKinFit->pfit(4);
    		
    		// the lines below MUSST stay below the settings of the 4vectors!
    		if (m_KalKinFit->Fit(0)) {m_dTPi01Chi2 = m_KalKinFit->chisq(0);} else {m_dTPi01Chi2 = -5;}
    		if (m_KalKinFit->Fit(1)) {m_dTPi02Chi2 = m_KalKinFit->chisq(1);} else {m_dTPi02Chi2 = -5;}			
				
				bestpt2G[0]= *ptPi01;
				bestpt2G[1]= *ptPi02;
				
        bDone = true;
				
      }
			
    }
//cout << "FINAL CHI2 " << m_dTTotalChi2 <<endl;
//cout << endl;
if(m_bWriteNt2x2G==true) m_NT2x2G->write();
if (!bDone) {m_dTTotalChi2 = -5;m_dTPi01Chi2 = -5; m_dTPi02Chi2 = -5; return;}
m_iTFitKept++;
	}
	else{
	//------ Without Kinematic FIT ------//
			if (m_mapT2GList[4].m_aValidCombs2.size() != 1) {return;}
			for(int k=0;k<m_mapT2GList[4].m_aValidCombs2.size();k++) {
			HepVector w_zHel(5,0);
					w_zHel = m_pERTLepton->mdcKalTrack()->getZHelix();
    			WTrackParameter wtpElec (m_dMasselec,w_zHel,m_pERTLepton->mdcKalTrack()->getZError());
					m_lvLepton= wtpElec.p();
					cPi0Tracks* pt2G1 = (m_mapT2GList[4].m_aValidCombs2[k]).m_pTrack1;
					HepLorentzVector lv2G1G1= m_mapT2GList[4].Get4VecFromEMC(pt2G1->m_pTrack1,0.0);
					HepLorentzVector lv2G1G2= m_mapT2GList[4].Get4VecFromEMC(pt2G1->m_pTrack2,0.0);
					HepLorentzVector lv2G1 = lv2G1G1+lv2G1G2;
					cPi0Tracks* pt2G2 = (m_mapT2GList[4].m_aValidCombs2[k]).m_pTrack2;
					HepLorentzVector lv2G2G1= m_mapT2GList[4].Get4VecFromEMC(pt2G2->m_pTrack1,0.0);
					HepLorentzVector lv2G2G2= m_mapT2GList[4].Get4VecFromEMC(pt2G2->m_pTrack2,0.0);
					HepLorentzVector lv2G2 = lv2G2G1+lv2G2G2;
					
					HepLorentzVector lvMissLept=m_lvBeamElectron+m_lvBeamPositron-(m_lvLepton+lv2G1+lv2G2);
					m_dCosThetaMissLepton	= lvMissLept.cosTheta();
					
					//Kinematics in e+e- CM frame						
					vector <HepLorentzVector> veclv;
					veclv.push_back(m_lvBeamElectron);
					veclv.push_back(m_lvBeamPositron);
					veclv.push_back(m_lvLepton);
					veclv.push_back(lv2G1);
					veclv.push_back(lv2G1G1);
					veclv.push_back(lv2G1G2);
					veclv.push_back(lv2G2);
					veclv.push_back(lv2G2G1);
					veclv.push_back(lv2G2G2);
					
					vector <HepLorentzVector> veclvCM = MakeBoostedVecLV(veclv,m_vectboostCM);
					HepLorentzVector lvLeptonCM=veclvCM[2];
					HepLorentzVector lv2G1CM=veclvCM[3];
					HepLorentzVector lv2G2CM=veclvCM[6];

					m_dTSumPtCM=GetPt(lvLeptonCM+lv2G1CM+lv2G2CM);
					
					//Helicity Angle
					Hep3Vector boost2x2G=(lv2G1+lv2G2).boostVector();
					HepLorentzVector lv2G1G12GCM=lv2G1G1;
					lv2G1G12GCM.boost(-(lv2G1.boostVector()));
					m_dCosTheta2G1HelicityG1=(lv2G1G12GCM.vect()).cosTheta(lv2G1.vect());
					
					HepLorentzVector lv2G2G12GCM=lv2G2G1;
					lv2G2G12GCM.boost(-(lv2G2.boostVector()));
					m_dCosTheta2G2HelicityG1=(lv2G2G12GCM.vect()).cosTheta(lv2G2.vect());
					
					//RGamma
					double ELept2x2GCM;
					double AbsMomLept2x2GCM;

					ELept2x2GCM=(lvLeptonCM+lv2G1CM+lv2G2CM).e();
					AbsMomLept2x2GCM=((lvLeptonCM+lv2G1CM+lv2G2CM).vect()).mag();
					m_dRGam=(m_lvBoost.m()-ELept2x2GCM-AbsMomLept2x2GCM)/m_lvBoost.m();
					m_dT2x2GE2G1[m_iT2x2GN2x2G]=lv2G1.e();
					m_dT2x2GE2G2[m_iT2x2GN2x2G]=lv2G2.e();	
					m_dT2x2GInvM2G1[m_iT2x2GN2x2G]=lv2G1.m();					
					m_dT2x2GInvM2G2[m_iT2x2GN2x2G]=lv2G2.m();	
					m_dT2x2GEAsym2G1[m_iT2x2GN2x2G] =	GetEAsym2Gam(lv2G1G1,lv2G1G2);
					m_dT2x2GEAsym2G2[m_iT2x2GN2x2G] =	GetEAsym2Gam(lv2G2G1,lv2G2G2);
					m_dT2x2GCThetaMiss[m_iT2x2GN2x2G]=m_dCosThetaMissLepton;
					m_dT2x2GInvM2x2G[m_iT2x2GN2x2G]=(lv2G1+lv2G2).m();
					m_dT2x2GSumPtCM[m_iT2x2GN2x2G]=m_dTSumPtCM;
					m_dT2x2GCTheta2G1HelicityG1[m_iT2x2GN2x2G]=m_dCosTheta2G1HelicityG1;
					m_dT2x2GCTheta2G2HelicityG1[m_iT2x2GN2x2G]=m_dCosTheta2G2HelicityG1;
					m_dT2x2GRGam[m_iT2x2GN2x2G]=m_dRGam;
					
					m_iT2x2GN2x2G++;	
							
					for(int iTagType=0;iTagType<3;iTagType++){	
					key[0]=iTagType; key[1]=4;
					if(iTagType==1)
						{
						if(m_iTQTag>=0) continue;
						}
					if(iTagType==2)
						{
						if(m_iTQTag<=0) continue;
						}	
							
					m_maphT2G1InvM[key]->fill(lv2G1.m(),1);
					m_maphT2G2InvM[key]->fill(lv2G2.m(),1);
					m_maphTEAsym2G1[key]->fill(GetEAsym2Gam(lv2G1G1,lv2G1G2),1);
					m_maphTEAsym2G2[key]->fill(GetEAsym2Gam(lv2G2G1,lv2G2G2),1);
					m_maphT2x2GMissM[key]->fill(m_dMissMass,1);
					m_maphT2x2GMissE[key]->fill(m_dMissE,1);
					m_maphT2x2GCThetaMiss[key]->fill(m_dCosThetaMissLepton,1);
					m_maphT2x2GCTheta2G1HelicityG1[key]->fill(m_dCosTheta2G1HelicityG1,1);
					m_maphT2x2GCTheta2G2HelicityG1[key]->fill(m_dCosTheta2G2HelicityG1,1);
					m_maphT2x2GInvM[key]->fill((lv2G1+lv2G2).m(),1);
					m_maphT2x2GSumPt[key]->fill(m_dTSumPtCM,1);
					//m_maphT2x2GcosTheta2G1CM2Gam[key]->fill(,1);
					m_maphT2x2GRGam[key]->fill(m_dRGam);		
					}
			
			cPi0Tracks* ptPi01 = m_mapT2GList[4].m_aValidCombs2[k].m_pTrack1;
			cPi0Tracks* ptPi02 = m_mapT2GList[4].m_aValidCombs2[k].m_pTrack2;
			
    		// detector
				m_lvLepton = wtpElec.p();
				if(m_iTQTag==-1) m_lvGammaStar = m_lvBeamElectron-m_lvLepton;
				if(m_iTQTag==1) m_lvGammaStar = m_lvBeamPositron-m_lvLepton;
    		m_lvPi01   = m_mapT2GList[4].Get4VecFromEMC(ptPi01->m_pTrack1,0.0) + m_mapT2GList[4].Get4VecFromEMC(ptPi01->m_pTrack2,0.0);
    		m_lvPi01G1 = m_mapT2GList[4].Get4VecFromEMC(ptPi01->m_pTrack1,0.0);
    		m_lvPi01G2 = m_mapT2GList[4].Get4VecFromEMC(ptPi01->m_pTrack2,0.0);
    		m_lvPi02   = m_mapT2GList[4].Get4VecFromEMC(ptPi02->m_pTrack1,0.0) + m_mapT2GList[4].Get4VecFromEMC(ptPi02->m_pTrack2,0.0);
    		m_lvPi02G1 = m_mapT2GList[4].Get4VecFromEMC(ptPi02->m_pTrack1,0.0);
    		m_lvPi02G2 = m_mapT2GList[4].Get4VecFromEMC(ptPi02->m_pTrack2,0.0);
    		m_aPhIndexList.clear();
   		  m_aPhIndexList.push_back (ptPi01->m_pTrack1);
    		m_aPhIndexList.push_back (ptPi01->m_pTrack2);
    		m_aPhIndexList.push_back (ptPi02->m_pTrack1);
    		m_aPhIndexList.push_back (ptPi02->m_pTrack2);	
				
				bestpt2G[0]= *ptPi01;
				bestpt2G[1]= *ptPi02;	
    }
		
	if(m_bWriteNt2x2G==true) m_NT2x2G->write();
	}

//Fill the selected Pi0 candidates into a new Pi0TrackList
m_mapT2GList[5].Add(bestpt2G[0]);
m_mapT2GList[5].Add(bestpt2G[1]);	
Fill2x2GammaContainer("Tagged",5);
	
//Fill particle containers
m_TLepton.Fill(&m_lvLepton);
m_TPi01.Fill(&m_lvPi01);
m_TPi02.Fill(&m_lvPi02);
m_TGammas[0].Fill(&m_lvPi01G1);
m_TGammas[1].Fill(&m_lvPi01G2);
m_TGammas[2].Fill(&m_lvPi02G1);
m_TGammas[3].Fill(&m_lvPi02G2);
m_TFitLepton.Fill(&m_lvFitLepton);
m_TFitPi01.Fill(&m_lvFitPi01);
m_TFitPi02.Fill(&m_lvFitPi02);
m_TFitGammas[0].Fill(&m_lvFitPi01G1);
m_TFitGammas[1].Fill(&m_lvFitPi01G2);
m_TFitGammas[2].Fill(&m_lvFitPi02G1);
m_TFitGammas[3].Fill(&m_lvFitPi02G2);

//Missing Mass, Energy	
HepLorentzVector lvMissLept=m_lvBeamElectron+m_lvBeamPositron-(m_lvLepton+m_lvPi01+m_lvPi02);
m_dTMissMass= lvMissLept.m();
m_dTMissE= lvMissLept.e();

m_dDeltaEemcE4GamLept=m_dTSumEemc-((m_lvPi01G1+m_lvPi01G2+m_lvPi02G1+m_lvPi02G2+m_lvLepton).e());
m_dTDeltaEemcE4GamLept	=m_dDeltaEemcE4GamLept;				
//Tagged virtuality
m_dTQ2=-(m_lvGammaStar).m2();
m_dTFitQ2=-(m_lvFitGammaStar).m2();

//Invariant mass
m_dTPi01InvM=m_lvPi01.m();
m_dTPi02InvM=m_lvPi02.m();
m_dT2Pi0InvM=(m_lvPi01+m_lvPi02).m();
	
m_dTFitPi01InvM=m_lvFitPi01.m();
m_dTFitPi02InvM=m_lvFitPi02.m();
m_dTFit2Pi0InvM=(m_lvFitPi01+m_lvFitPi02).m();

m_dTMC2Pi0InvM=m_dMC2Pi0InvM;

//Energy Asymmetry
m_dTEAsymPi01=GetEAsym2Gam(m_lvPi01G1,m_lvPi01G2);
m_dTEAsymPi02=GetEAsym2Gam(m_lvPi02G1,m_lvPi02G2);

m_dTFitEAsymPi01=GetEAsym2Gam(m_lvFitPi01G1,m_lvFitPi01G2);
m_dTFitEAsymPi02=GetEAsym2Gam(m_lvFitPi02G1,m_lvFitPi02G2);

//Kinematics in e+e- CM frame	
vector <HepLorentzVector> veclv;
veclv.push_back(m_lvBeamElectron);
veclv.push_back(m_lvBeamPositron);
veclv.push_back(m_lvLepton);
veclv.push_back(m_lvPi01);
veclv.push_back(m_lvPi01G1);
veclv.push_back(m_lvPi01G2);
veclv.push_back(m_lvPi02);
veclv.push_back(m_lvPi02G1);
veclv.push_back(m_lvPi02G2);
veclv.push_back(m_lvGammaStar);

vector <HepLorentzVector> veclvFit;
veclvFit.push_back(m_lvBeamElectron);
veclvFit.push_back(m_lvBeamPositron);
veclvFit.push_back(m_lvFitLepton);
veclvFit.push_back(m_lvFitPi01);
veclvFit.push_back(m_lvFitPi01G1);
veclvFit.push_back(m_lvFitPi01G2);
veclvFit.push_back(m_lvFitPi02);
veclvFit.push_back(m_lvFitPi02G1);
veclvFit.push_back(m_lvFitPi02G2);
veclvFit.push_back(m_lvFitGammaStar);

vector <HepLorentzVector> veclvCM = MakeBoostedVecLV(veclv,m_vectboostCM);
m_lvLeptonCM=veclvCM[2];
m_lvPi01CM=veclvCM[3];
m_lvPi02CM=veclvCM[6];

vector <HepLorentzVector> veclvFitCM = MakeBoostedVecLV(veclvFit,m_vectboostCM);
m_lvFitLeptonCM=veclvFitCM[2];
m_lvFitPi01CM=veclvFitCM[3];
m_lvFitPi02CM=veclvFitCM[6];

//Tranverse momentum
m_dTSumPtCM=GetPt(m_lvLeptonCM+m_lvPi01CM+m_lvPi02CM);
m_dTFitSumPtCM=GetPt(m_lvFitLeptonCM+m_lvFitPi01CM+m_lvFitPi02CM);

if(m_iTQTag==-1) m_dTMCSumPtCM=m_dMCeTSumPtCM;
if(m_iTQTag==1) m_dTMCSumPtCM=m_dMCpTSumPtCM;

//Alternate transformation into Gamma-Gamma frame
//Z axis for Gamma-Gamma CM frame : Z direction along gamma star
Hep3Vector ZAxisGam2Pi0CM;
Hep3Vector YAxisGam2Pi0CM;
Hep3Vector XAxisGam2Pi0CM;
double ThetaPi01CM2Gam;
double PhiPi01CM2Gam;

//Without fit
m_boost2Pi0=(m_lvPi01+m_lvPi02).boostVector();
vector <HepLorentzVector> veclvCM2Gam = MakeBoostedVecLV(veclv,m_boost2Pi0);
m_lvLeptonCM2Gam=veclvCM2Gam[2];
m_lvPi01CM2Gam=veclvCM2Gam[3];
m_lvGammaStarCM2Gam=veclvCM2Gam[9];

ZAxisGam2Pi0CM = (m_lvGammaStarCM2Gam.vect()).unit();
YAxisGam2Pi0CM = (ZAxisGam2Pi0CM.cross(m_lvLeptonCM2Gam)).unit();
XAxisGam2Pi0CM = (YAxisGam2Pi0CM.cross(ZAxisGam2Pi0CM)).unit();
if((m_lvLeptonCM2Gam.vect()).dot(XAxisGam2Pi0CM)<0) log << MSG::WARNING << "***** Px(e-) <0 IN GG FRAME !!!!!!!! *****" << endreq;
ThetaPi01CM2Gam=m_lvPi01CM2Gam.angle(ZAxisGam2Pi0CM);
if((m_lvPi01CM2Gam.vect()).dot(XAxisGam2Pi0CM)>=0) PhiPi01CM2Gam=m_lvPi01CM2Gam.angle(XAxisGam2Pi0CM);
else PhiPi01CM2Gam=-m_lvPi01CM2Gam.angle(XAxisGam2Pi0CM);
//if((m_lvFitPi01CM2Gam.vect()).dot(XAxisGam2Pi0CM)<0) log << MSG::WARNING << "***** m_lvFitPi01CM2Gam.dot(XAxisGam2Pi0CM)<0 *****" << endreq;

m_dTcosThetaPi01CM2Gam=cos(ThetaPi01CM2Gam);
m_dTPhiPi01CM2Gam=PhiPi01CM2Gam;

//With fit
m_boostFit2Pi0=(m_lvFitPi01+m_lvFitPi02).boostVector();
vector <HepLorentzVector> veclvFitCM2Gam = MakeBoostedVecLV(veclvFit,m_boostFit2Pi0);
m_lvFitLeptonCM2Gam=veclvFitCM2Gam[2];
m_lvFitPi01CM2Gam=veclvFitCM2Gam[3];
//m_lvFitGammaStarCM2Gam=m_lvFitGammaStar;
//m_lvFitGammaStarCM2Gam.boost(-m_boostFit2Pi0);
m_lvFitGammaStarCM2Gam=veclvFitCM2Gam[9];

ZAxisGam2Pi0CM = (m_lvFitGammaStar.vect()).unit();
YAxisGam2Pi0CM = (ZAxisGam2Pi0CM.cross(m_lvFitLeptonCM2Gam)).unit();
XAxisGam2Pi0CM = (YAxisGam2Pi0CM.cross(ZAxisGam2Pi0CM)).unit();
if((m_lvFitLeptonCM2Gam.vect()).dot(XAxisGam2Pi0CM)<0) log << MSG::WARNING << "***** Px(e-) <0 IN GG FRAME !!!!!!!! *****" << endreq;
ThetaPi01CM2Gam=m_lvFitPi01CM2Gam.angle(ZAxisGam2Pi0CM);
if((m_lvFitPi01CM2Gam.vect()).dot(XAxisGam2Pi0CM)>=0) PhiPi01CM2Gam=m_lvFitPi01CM2Gam.angle(XAxisGam2Pi0CM);
else PhiPi01CM2Gam=-m_lvFitPi01CM2Gam.angle(XAxisGam2Pi0CM);
//if((m_lvFitPi01CM2Gam.vect()).dot(XAxisGam2Pi0CM)<0) log << MSG::WARNING << "***** m_lvFitPi01CM2Gam.dot(XAxisGam2Pi0CM)<0 *****" << endreq;

m_dTFitcosThetaPi01CM2Gam=cos(ThetaPi01CM2Gam);
m_dTFitPhiPi01CM2Gam=PhiPi01CM2Gam;


//Helicity Angle
HepLorentzVector lv2G1=m_lvPi01; 
HepLorentzVector lvG12GCM=m_lvPi01G1;
m_dTCThetaHelicity=GetCThetaHelicity(lv2G1,lvG12GCM);

HepLorentzVector lvFit2G1=m_lvFitPi01; 
HepLorentzVector lvFitG12GCM=m_lvFitPi01G1;
m_dTFitCThetaHelicity=GetCThetaHelicity(lvFit2G1,lvFitG12GCM);

//RGamma
double ELept2Pi0CM;
double AbsMomLept2Pi0CM;

ELept2Pi0CM=(m_lvLeptonCM+m_lvPi01CM+m_lvPi02CM).e();
AbsMomLept2Pi0CM=((m_lvLeptonCM+m_lvPi01CM+m_lvPi02CM).vect()).mag();
m_dRGam=(m_lvBoost.m()-ELept2Pi0CM-AbsMomLept2Pi0CM)/m_lvBoost.m();
m_dTRGam=m_dRGam;

ELept2Pi0CM=(m_lvFitLeptonCM+m_lvFitPi01CM+m_lvFitPi02CM).e();
AbsMomLept2Pi0CM=((m_lvFitLeptonCM+m_lvFitPi01CM+m_lvFitPi02CM).vect()).mag();
m_dRGam=(m_lvBoost.m()-ELept2Pi0CM-AbsMomLept2Pi0CM)/m_lvBoost.m();
m_dTFitRGam=m_dRGam;

FillHistograms("Tagged",5);
FillHistograms("eTagged",5);
FillHistograms("pTagged",5);
/*
if(!(m_dTTotalChi2<m_dChisqMaxValue)){return;}
m_mapT2GList[6].Add(bestpt2G[0]);
m_mapT2GList[6].Add(bestpt2G[1]);
Fill2x2GammaContainer("Tagged",6);
FillHistograms("Tagged",6);
FillHistograms("eTagged",6);
FillHistograms("pTagged",6);

if(!(m_dTSumEemc<m_dUSumEemcMax)){return;}
m_mapT2GList[7].Add(bestpt2G[0]);
m_mapT2GList[7].Add(bestpt2G[1]);
Fill2x2GammaContainer("Tagged",7);
FillHistograms("Tagged",7);
FillHistograms("eTagged",7);
FillHistograms("pTagged",7);

if(!(m_dTSumPtCM<= m_dUSumPtMax)){return;}
m_mapT2GList[8].Add(bestpt2G[0]);
m_mapT2GList[8].Add(bestpt2G[1]);
Fill2x2GammaContainer("Tagged",8);
FillHistograms("Tagged",8);
FillHistograms("eTagged",8);
FillHistograms("pTagged",8);
*/

if(!(m_dTSumEemc<m_dUSumEemcMax)){return;}
m_mapT2GList[6].Add(bestpt2G[0]);
m_mapT2GList[6].Add(bestpt2G[1]);
Fill2x2GammaContainer("Tagged",6);
FillHistograms("Tagged",6);
FillHistograms("eTagged",6);
FillHistograms("pTagged",6);

if(!(m_dTMissMass<1.5)){return;}
m_mapT2GList[7].Add(bestpt2G[0]);
m_mapT2GList[7].Add(bestpt2G[1]);
Fill2x2GammaContainer("Tagged",7);
FillHistograms("Tagged",7);
FillHistograms("eTagged",7);
FillHistograms("pTagged",7);

if(!(m_dTRGam<0.05)){return;}
m_mapT2GList[8].Add(bestpt2G[0]);
m_mapT2GList[8].Add(bestpt2G[1]);
Fill2x2GammaContainer("Tagged",8);
FillHistograms("Tagged",8);
FillHistograms("eTagged",8);
FillHistograms("pTagged",8);

if(!(fabs(m_dTCThetaHelicity)<0.8)){return;}
m_mapT2GList[9].Add(bestpt2G[0]);
m_mapT2GList[9].Add(bestpt2G[1]);
Fill2x2GammaContainer("Tagged",9);
FillHistograms("Tagged",9);
FillHistograms("eTagged",9);
FillHistograms("pTagged",9);

if(!(m_dTSumPtCM<= 0.05)){return;}
m_mapT2GList[10].Add(bestpt2G[0]);
m_mapT2GList[10].Add(bestpt2G[1]);
Fill2x2GammaContainer("Tagged",10);
FillHistograms("Tagged",10);
FillHistograms("eTagged",10);
FillHistograms("pTagged",10);

if(!(m_dTTotalChi2<m_dChisqMaxValue)){return;}
m_mapT2GList[11].Add(bestpt2G[0]);
m_mapT2GList[11].Add(bestpt2G[1]);
Fill2x2GammaContainer("Tagged",11);
FillHistograms("Tagged",11);
FillHistograms("eTagged",11);
FillHistograms("pTagged",11);

m_iIsT2Pi0=1;
m_NTagged->write();
m_iAcceptedT++;	

//if(m_dTSumEemc<m_dUSumEemcMax && m_dTTotalChi2<m_dChisqMaxValue && m_dTFitSumPtCM<m_dUSumPtMax) m_iIsT2Pi0=1;
/*
bool bStage5OK=m_dTTotalChi2<m_dChisqMaxValue;
if(!bStage5OK){return;}

bool bStage6OK=m_dTSumEemc<m_dUSumEemcMax;
if(!bStage6OK){return;}

bool bStage7OK=m_dTFitSumPtCM<= m_dUSumPtMax;
if(!bStage7OK){return;}

m_iIsT2Pi0=1;

m_iAcceptedT++;				
*/
		
return;
}
//---------------------------------------------------------------------------------------------------------- 

//---------------------------------------------------------------------------------------------------------- 
double Bes2Gamto2Pi0::IPdist (EvtRecTrack* pCur)
//----------------------------------------------------------------------------------------------------------
{
    // Constructor with pivot, helix parameter a, and its error matrix.
    HepPoint3D   point0(0,0,0);
    VFHelix      helixipP(point0,pCur->mdcKalTrack()->helix(),pCur->mdcKalTrack()->err());
    helixipP.pivot(m_hp3IP);
    return (sqrt(pow(fabs(helixipP.a()[0]),2)+pow(helixipP.a()[3],2)));
}
//---------------------------------------------------------------------------------------------------------- 

//---------------------------------------------------------------------------------------------------------- 
double Bes2Gamto2Pi0::GetEAsym2Gam(HepLorentzVector lvG1,HepLorentzVector lvG2)
//----------------------------------------------------------------------------------------------------------
{
    double EAsym;
		double EG1=lvG1.e();
		double EG2=lvG2.e();
		EAsym=(fabs(EG1-EG2))/(EG1+EG2);
		return EAsym;
}
//---------------------------------------------------------------------------------------------------------- 

//---------------------------------------------------------------------------------------------------------- 
HepLorentzVector Bes2Gamto2Pi0::GetBoostedLV(HepLorentzVector lvIniFrame, Hep3Vector vectboost)
//----------------------------------------------------------------------------------------------------------
{
    HepLorentzVector lvBoostedFrame;
		lvBoostedFrame=lvIniFrame;
		lvBoostedFrame.boost(-vectboost);
		return lvBoostedFrame;
}
//----------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------- 
double Bes2Gamto2Pi0::GetCThetaHelicity(HepLorentzVector lv2G, HepLorentzVector lvG)
//----------------------------------------------------------------------------------------------------------
{
    double CThetaHelicity;
		lvG.boost(-(lv2G.boostVector()));		
		CThetaHelicity=(lvG.vect()).cosTheta(lv2G.vect());

		return CThetaHelicity;
}
//----------------------------------------------------------------------------------------------------------

/*
//---------------------------------------------------------------------------------------------------------- 
std::vector<double> Bes2Gamto2Pi0::GetTagPi0CThetaPhiCM2Gam(std::vector<HepLorentzVector> veclvCM2Gam, HepLorentzVector lvPi0CM2Gam)
//----------------------------------------------------------------------------------------------------------
{
		MsgStream log(msgSvc(), name());
		
    vector<double> vecAngles;
		vecAngles.resize(2);
		
		Hep3Vector ZAxisGam2Pi0CM;
		Hep3Vector YAxisGam2Pi0CM;
		Hep3Vector XAxisGam2Pi0CM;
		double ThetaPi0CM2Gam;
		double PhiPi0CM2Gam;

		HepLorentzVector lvLeptonCM2Gam=veclvCM2Gam[2];
		HepLorentzVector lvGammaStarCM2Gam=veclvCM2Gam[9];

		ZAxisGam2Pi0CM = (lvGammaStarCM2Gam.vect()).unit();
		YAxisGam2Pi0CM = (ZAxisGam2Pi0CM.cross(lvLeptonCM2Gam)).unit();
		XAxisGam2Pi0CM = (YAxisGam2Pi0CM.cross(ZAxisGam2Pi0CM)).unit();
		if((lvLeptonCM2Gam.vect()).dot(XAxisGam2Pi0CM)<0) log << MSG::WARNING << "***** Px(e-) <0 IN GG FRAME !!!!!!!! *****" << endreq;
		ThetaPi0CM2Gam=lvPi0CM2Gam.angle(ZAxisGam2Pi0CM);
		if((lvPi0CM2Gam.vect()).dot(XAxisGam2Pi0CM)>=0) PhiPi0CM2Gam=lvPi0CM2Gam.angle(XAxisGam2Pi0CM);
		else PhiPi0CM2Gam=-lvPi0CM2Gam.angle(XAxisGam2Pi0CM);
		//if((m_lvFitPi0CM2Gam.vect()).dot(XAxisGam2Pi0CM)<0) log << MSG::WARNING << "***** m_lvFitPi01CM2Gam.dot(XAxisGam2Pi0CM)<0 *****" << endreq;

		vecAngles[0]=cos(ThetaPi0CM2Gam);
		vecAngles[1]=PhiPi0CM2Gam;
		
		return vecAngles;
}
//----------------------------------------------------------------------------------------------------------
*/

//---------------------------------------------------------------------------------------------------------- 
int Bes2Gamto2Pi0::GetNBins(double xmin, double xmax, double dx)
//----------------------------------------------------------------------------------------------------------
{
    int NBins=0;
		NBins=fabs(xmax-xmin)/dx;
		return NBins;
}
//----------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------- 
IHistogram1D* Bes2Gamto2Pi0::MakeIHistogram1D(const std::string &dirPath, const std::string &relPath,const std::string &title, double deltaX, double lowX, double highX)
//----------------------------------------------------------------------------------------------------------
{
    IHistogram1D* histo;
		int nbinsX=GetNBins(lowX,highX,deltaX);
		histo=histoSvc()->book(dirPath,relPath,title,nbinsX,lowX,highX);
		return histo;
}
//----------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------- 
IHistogram2D* Bes2Gamto2Pi0::MakeIHistogram2D(const std::string &dirPath, const std::string &relPath,const std::string &title, double deltaX, double lowX, double highX,double deltaY, double lowY, double highY)
//----------------------------------------------------------------------------------------------------------
{
    IHistogram2D* histo;
		int nbinsX=GetNBins(lowX,highX,deltaX);
		int nbinsY=GetNBins(lowY,highY,deltaY);
		histo=histoSvc()->book(dirPath,relPath,title,nbinsX,lowX,highX,nbinsY,lowY,highY);
		return histo;
}
//----------------------------------------------------------------------------------------------------------

/*
//---------------------------------------------------------------------------------------------------------- 
void Bes2Gamto2Pi0::BoostKine(std::vector<HepLorentzVector>& veclv, Hep3Vector boostvec)
//----------------------------------------------------------------------------------------------------------
{
 for(int k=0;k<veclv.size();k++){
	veclv[k].boost(-boostvec);
	}   
}
//----------------------------------------------------------------------------------------------------------
*/


//---------------------------------------------------------------------------------------------------------- 
std::vector<HepLorentzVector> Bes2Gamto2Pi0::MakeBoostedVecLV(std::vector<HepLorentzVector> iniveclv, Hep3Vector boostvec)
//----------------------------------------------------------------------------------------------------------
{
 std::vector<HepLorentzVector> veclv;
 for(int k=0;k<iniveclv.size();k++){
	veclv.push_back(iniveclv[k]);
	(veclv.back()).boost(-boostvec);
	} 
	
return veclv;  
}
//----------------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------------- 
void Bes2Gamto2Pi0::InitGammaContainers(std::string option)
//----------------------------------------------------------------------------------------------------------
{
if(option=="Untagged")
		{
			for(int k=0;k<8;k++){
			m_mapUGList[k].Init(m_dPhEnergyCutBarrel,m_dPhEnergyCutEndCap,m_dCosThetaBarrel,m_dMaxTime,m_dCMSEnergy/2.0);
			m_mapUGList[k].SetIterators (m_itBegin,m_itEndCharged,m_itEndNeutral);
			}  
		}
		
if(option=="Tagged")
		{
			for(int k=0;k<12;k++){
			m_mapTGList[k].Init(m_dPhEnergyCutBarrel,m_dPhEnergyCutEndCap,m_dCosThetaBarrel,m_dMaxTime,m_dCMSEnergy/2.0);
			m_mapTGList[k].SetIterators (m_itBegin,m_itEndCharged,m_itEndNeutral);
			}  
		}
		
}
//----------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------- 
void Bes2Gamto2Pi0::Init2GammaContainers(std::string option)
//----------------------------------------------------------------------------------------------------------
{
if(option=="Untagged")
		{
			for(int k=0;k<8;k++){			
			m_mapU2GList[k].SetIterators (m_itBegin,m_itEndCharged,m_itEndNeutral);
			m_mapU2GList[k].Init(m_dPi0MassCutLow,m_dPi0MassCutHigh,m_dPhEnergyCutBarrel,m_dPhEnergyCutEndCap,m_dCosThetaBarrel,m_dMaxTime,m_dCMSEnergy/2.0); 
			}  
		}

if(option=="Tagged")
	{
		for(int k=0;k<12;k++){			
			m_mapT2GList[k].SetIterators (m_itBegin,m_itEndCharged,m_itEndNeutral);
			m_mapT2GList[k].Init(m_dPi0MassCutLow,m_dPi0MassCutHigh,m_dPhEnergyCutBarrel,m_dPhEnergyCutEndCap,m_dCosThetaBarrel,m_dMaxTime,m_dCMSEnergy/2.0); 
		}
	}
		
}
//----------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------- 
void Bes2Gamto2Pi0::InitChargedTracksContainer(std::string option)
//----------------------------------------------------------------------------------------------------------
{
	
if(option=="Tagged")
		{
			for(int k=0;k<12;k++){			
			m_mapTCTList[k].SetIterators (m_itBegin,m_itEndCharged,m_itEndNeutral);
			}  
		}
		
}
//----------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------- 
void Bes2Gamto2Pi0::FillGammaContainer(std::string option, int stage)
//----------------------------------------------------------------------------------------------------------
{
if(option=="Untagged"){
			EvtRecTrackIterator    itERT;
			for (itERT = m_itEndCharged;itERT != m_itEndNeutral;itERT ++) {	
			int index = std::distance(m_itEndCharged,itERT);			
				// count good photons
        if (m_mapUGList[stage].IsPhoton(*itERT)) {				
				m_mapUGList[stage].Add(*itERT,index);
				}	
    	}	  
		}
		
if(option=="Tagged"){
	double m_dBeamEnergy=m_dCMSEnergy/2.0	;
	EvtRecTrackIterator    itERT;
			for (itERT = m_itEndCharged;itERT != m_itEndNeutral;itERT ++) {	
			int index = std::distance(m_itEndCharged,itERT);			
				// count good photons
        if (m_mapTGList[stage].IsPhoton(*itERT)) {	
				//if(m_mapTGList[stage].IsPhoton(*itERT) && (*itERT)->emcShower()->energy() <= m_dBeamEnergy){			
				m_mapTGList[stage].Add(*itERT,index);
				}	
    	}

	}
return;		
}
//----------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------- 
void Bes2Gamto2Pi0::Fill2GammaContainer(std::string option, int stage)
//----------------------------------------------------------------------------------------------------------
{
if(option=="Untagged")
	{
			double m_dBeamEnergy=m_dCMSEnergy/2.0;
			for (int i=0;i<(m_mapUGList[stage].GetGamTable()).size();i++) {
		EvtRecTrack* ERT1 = (m_mapUGList[stage].GetGamTable())[i];
		int trackIndex1=	(m_mapUGList[stage].GetGamTableTrackIndex())[i];	
		 if(ERT1->emcShower()->energy() > m_dBeamEnergy) continue; 
			for (int j = i+1;j<(m_mapUGList[stage].GetGamTable()).size();j++) { 
				EvtRecTrack* ERT2 = (m_mapUGList[stage].GetGamTable())[j];
				int trackIndex2=	(m_mapUGList[stage].GetGamTableTrackIndex())[j];
				if(ERT2->emcShower()->energy() > m_dBeamEnergy) continue; 
				
				HepLorentzVector lvG1 = m_mapUGList[stage].Get4VecFromEMC(ERT1);
				HepLorentzVector lvG2 = m_mapUGList[stage].Get4VecFromEMC(ERT2);
				HepLorentzVector lv2G=lvG1+lvG2;
				HepLorentzVector lvG12GCM=lvG1;
				lvG12GCM.boost(-(lv2G.boostVector()));
			
				double d2GMass = m_mapU2GList[stage].GetM(ERT1,0.0,ERT2,0.0);
				
				m_dEAsym=GetEAsym2Gam(lvG1,lvG2);
				//m_dCosTheta2GHelicityG1=(lvG12GCM.vect()).cosTheta(lv2G.vect());
				m_dCosTheta2GHelicityG1=GetCThetaHelicity(lv2G,lvG1);
				m_d2GOpeningAngle=(lvG1.vect()).angle(lvG2.vect());
				m_d2GDeltaTheta=(lvG1.theta())-(lvG2.theta());
				m_d2GDeltaPhi=(lvG1.vect()).deltaPhi(lvG2.vect());
				
				bool cond=true;
				if(stage>=3) cond=d2GMass>=m_dPi0MassCutLow && d2GMass<=m_dPi0MassCutHigh;
				
				if(!cond) continue;
				
				cPi0Tracks* TwoGamTracks;
				
				m_mapU2GList[stage].Add(ERT1,ERT2,d2GMass,trackIndex1,trackIndex2);								
				vector<cPi0Tracks>& a2GamTracksTable = m_mapU2GList[stage].GetPi0Tracks();
				TwoGamTracks = &(a2GamTracksTable.back());
				TwoGamTracks->SetEAsym(m_dEAsym);
				TwoGamTracks->SetCosTheta2GHelicityG1(m_dCosTheta2GHelicityG1);
				TwoGamTracks->Set2GOpeningAngle(m_d2GOpeningAngle);
				TwoGamTracks->Set2GDeltaTheta(m_d2GDeltaTheta);
				TwoGamTracks->Set2GDeltaPhi(m_d2GDeltaPhi);
								
     }
  	}  
	}
	
if(option=="Tagged")
	{
			double m_dBeamEnergy=m_dCMSEnergy/2.0;
			for (int i=0;i<(m_mapTGList[stage].GetGamTable()).size();i++) {
		EvtRecTrack* ERT1 = (m_mapTGList[stage].GetGamTable())[i];
		int trackIndex1=	(m_mapTGList[stage].GetGamTableTrackIndex())[i];	
		 if(ERT1->emcShower()->energy() > m_dBeamEnergy) continue; 
			for (int j = i+1;j<(m_mapTGList[stage].GetGamTable()).size();j++) { 
				EvtRecTrack* ERT2 = (m_mapTGList[stage].GetGamTable())[j];
				int trackIndex2=	(m_mapTGList[stage].GetGamTableTrackIndex())[j];
				if(ERT2->emcShower()->energy() > m_dBeamEnergy) continue; 
				
				HepLorentzVector lvG1 = m_mapTGList[stage].Get4VecFromEMC(ERT1);
				HepLorentzVector lvG2 = m_mapTGList[stage].Get4VecFromEMC(ERT2);
				HepLorentzVector lv2G=lvG1+lvG2;
				HepLorentzVector lvG12GCM=lvG1;
				lvG12GCM.boost(-(lv2G.boostVector()));
			
				double d2GMass = m_mapT2GList[stage].GetM(ERT1,0.0,ERT2,0.0);
				
				m_dEAsym=GetEAsym2Gam(lvG1,lvG2);
				//m_dCosTheta2GHelicityG1=(lvG12GCM.vect()).cosTheta(lv2G.vect());
				m_dCosTheta2GHelicityG1=GetCThetaHelicity(lv2G,lvG1);
				m_d2GOpeningAngle=(lvG1.vect()).angle(lvG2.vect());
				m_d2GDeltaTheta=(lvG1.theta())-(lvG2.theta());
				m_d2GDeltaPhi=(lvG1.vect()).deltaPhi(lvG2.vect());
				
				bool cond=true;
				if(stage>=3) cond=d2GMass>=m_dPi0MassCutLow && d2GMass<=m_dPi0MassCutHigh;
				
				if(!cond) continue;
				
				cPi0Tracks* TwoGamTracks;
				
				m_mapT2GList[stage].Add(ERT1,ERT2,d2GMass,trackIndex1,trackIndex2);								
				vector<cPi0Tracks>& a2GamTracksTable = m_mapT2GList[stage].GetPi0Tracks();
				TwoGamTracks = &(a2GamTracksTable.back());
				TwoGamTracks->SetEAsym(m_dEAsym);
				TwoGamTracks->SetCosTheta2GHelicityG1(m_dCosTheta2GHelicityG1);
				TwoGamTracks->Set2GOpeningAngle(m_d2GOpeningAngle);
				TwoGamTracks->Set2GDeltaTheta(m_d2GDeltaTheta);
				TwoGamTracks->Set2GDeltaPhi(m_d2GDeltaPhi);
								
     }
  	}  
	}
	
return;		
}
//----------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------- 
void Bes2Gamto2Pi0::Fill2x2GammaContainer(std::string option, int stage)
//----------------------------------------------------------------------------------------------------------
{
if(option=="Untagged")
	{
		int NSharedGam=0;
		ValidCombs2 aVC2dummy;
		m_mapU2GList[stage].GetValidCombs2().clear();
  	for (int i=0;i<(m_mapU2GList[stage].GetPi0Tracks()).size();i++) {
    	for (int k=i+1;k<(m_mapU2GList[stage].GetPi0Tracks()).size();k++) {
      	// Exclude 2 gammas combinations that shares same photons
      	if ((m_mapU2GList[stage].GetPi0Tracks())[k].GetERT1() == (m_mapU2GList[stage].GetPi0Tracks())[i].GetERT1() ||
	 				  (m_mapU2GList[stage].GetPi0Tracks())[k].GetERT1() == (m_mapU2GList[stage].GetPi0Tracks())[i].GetERT2() ||
	  				(m_mapU2GList[stage].GetPi0Tracks())[k].GetERT2() == (m_mapU2GList[stage].GetPi0Tracks())[i].GetERT1() ||
	  				(m_mapU2GList[stage].GetPi0Tracks())[k].GetERT2() == (m_mapU2GList[stage].GetPi0Tracks())[i].GetERT2()
	  				) 
				{NSharedGam++;
				continue;}
      	aVC2dummy.Set(&((m_mapU2GList[stage].GetPi0Tracks())[i]),&((m_mapU2GList[stage].GetPi0Tracks())[k]));
      	m_mapU2GList[stage].GetValidCombs2().push_back(aVC2dummy);
				
    		}
  	}
	m_mapU2GList[stage].SetNCombs2WSharedGam(NSharedGam);		  
	}
if(option=="Tagged")
	{
		int NSharedGam=0;
		ValidCombs2 aVC2dummy;
		m_mapT2GList[stage].GetValidCombs2().clear();
  	for (int i=0;i<(m_mapT2GList[stage].GetPi0Tracks()).size();i++) {
    	for (int k=i+1;k<(m_mapT2GList[stage].GetPi0Tracks()).size();k++) {
      	// Exclude 2 gammas combinations that shares same photons
      	if ((m_mapT2GList[stage].GetPi0Tracks())[k].GetERT1() == (m_mapT2GList[stage].GetPi0Tracks())[i].GetERT1() ||
	 				  (m_mapT2GList[stage].GetPi0Tracks())[k].GetERT1() == (m_mapT2GList[stage].GetPi0Tracks())[i].GetERT2() ||
	  				(m_mapT2GList[stage].GetPi0Tracks())[k].GetERT2() == (m_mapT2GList[stage].GetPi0Tracks())[i].GetERT1() ||
	  				(m_mapT2GList[stage].GetPi0Tracks())[k].GetERT2() == (m_mapT2GList[stage].GetPi0Tracks())[i].GetERT2()
	  				) 
				{NSharedGam++;
				continue;}
      	aVC2dummy.Set(&((m_mapT2GList[stage].GetPi0Tracks())[i]),&((m_mapT2GList[stage].GetPi0Tracks())[k]));
      	m_mapT2GList[stage].GetValidCombs2().push_back(aVC2dummy);
				
    		}
  	}
		
	m_mapT2GList[stage].SetNCombs2WSharedGam(NSharedGam);
	}
return;		
}
//----------------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------------- 
void Bes2Gamto2Pi0::FillChargedTracksContainer(std::string option, int stage)
//----------------------------------------------------------------------------------------------------------
{
if(option=="Tagged")
	{
	EvtRecTrackIterator    itERT;
	for (itERT  = m_itBegin;itERT != m_itEndCharged;itERT ++) 
		{
		
		if (!(*itERT)->isMdcKalTrackValid()) {continue;}
		double Dist = IPdist (*itERT);
    HepPoint3D   point0(0,0,0);
    VFHelix      helixipP(point0,(*itERT)->mdcKalTrack()->helix(),(*itERT)->mdcKalTrack()->err());
    helixipP.pivot(m_hp3IP);
		double CylRad=fabs(helixipP.a()[0]);
		double CylHeight=fabs(helixipP.a()[3]);
		
		double P=(*itERT)->mdcKalTrack()->p();
		double Q=(*itERT)->mdcKalTrack()->charge();
		
		if(stage==0){
		m_hTDist->fill(Dist,1.);
		m_hTCylRad->fill(CylRad,1.);
		m_hTCylHeight->fill(CylHeight,1.);
		m_hTPChargedTracks->fill((*itERT)->mdcKalTrack()->p());
		}
		

		if(stage==0) m_iTValidMDCKept++;
		//Skip if track origin is not in the virtual box
		bool FromInteractionPoint=fabs(helixipP.a()[0]) < m_dCylRad && fabs(helixipP.a()[3]) < m_dCylHeight;
		if(!FromInteractionPoint) continue;
		
		if(stage==0) {m_iTValidMDCIPKept++;
		//return;
		}
		
		//Lepton identification
		if (!(*itERT)->isEmcShowerValid()) continue;
		double eEMCTot=(*itERT)->emcShower()->energy() ;
				
		bool cond=true;
		bool bStage0OK= FromInteractionPoint && Q!=0;
		bool IsLepton=Q!=0 && eEMCTot/P>0.8;
		if(stage==0) 
			{cond=bStage0OK;
			if(cond)
				{
				m_iTGoodNegKept++;
								
				if(IsLepton)
				{
				m_iTQTag=Q;
				m_pERTLepton=*itERT;
				HepVector w_zHel(5,0);
				w_zHel = m_pERTLepton->mdcKalTrack()->getZHelix();
    		WTrackParameter wtpElec (m_dMasselec,w_zHel,m_pERTLepton->mdcKalTrack()->getZError());
				m_lvLepton = wtpElec.p();
				m_iNLeptons++;
				}		
				m_bHas1Lepton=IsLepton && m_iNLeptons==1;
				}
			
			}
		if(stage==1)
			{
			cond=bStage0OK && IsLepton;
			}
		if(!cond)	continue;
		
		m_mapTCTList[stage].Add(*itERT);
				  
		}
	}	
return;		
}
//----------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------- 
void Bes2Gamto2Pi0::FillHistograms(std::string option, int stage)
//----------------------------------------------------------------------------------------------------------
{
if(option=="Untagged"){
m_maphUNumEvt[stage]->fill(0,1.);
m_maphUNNeutral[stage]->fill(m_iNNeutral,1.);
m_maphUSumEemc[stage]->fill(m_dUSumEemc,1.);
m_maphUNGoodGamma[stage]->fill(m_iUAnzGoodGamma);

for(int k=0;k<(m_mapUGList[stage].GetGamTable()).size();k++){		
		EvtRecTrack* ERT = (m_mapUGList[stage].GetGamTable())[k];		
		int trackIndex = (m_mapUGList[stage].GetGamTableTrackIndex())[k];
		HepLorentzVector lvG = m_mapUGList[stage].Get4VecFromEMC(ERT);
		m_maphUEGam[stage]->fill(lvG.e());
		m_maphUPtGam[stage]->fill(GetPt(lvG));
		//m_maphUNumGamvsEnGam[stage]->fill((ERT)->emcShower()->energy(), trackIndex,1);	
		}
			
			if(stage<=2)
			{
			for(int k=0;k<(m_mapUGList[stage].GetGamTable()).size();k++){		
				EvtRecTrack* ERT = (m_mapUGList[stage].GetGamTable())[k];		
				int trackIndex = (m_mapUGList[stage].GetGamTableTrackIndex())[k];
				HepLorentzVector lvG = m_mapUGList[stage].Get4VecFromEMC(ERT);
				m_maphUNumGamvsEnGam[stage]->fill((ERT)->emcShower()->energy(), trackIndex,1);	
				}		
			}
			
			if(stage>=1)
			{
				for(int k=0;k<(m_mapU2GList[stage].GetPi0Tracks()).size();k++){		
					vector<cPi0Tracks>& a2GamTracksTable = m_mapU2GList[stage].GetPi0Tracks();
					cPi0Tracks* TwoGamTracks = &(a2GamTracksTable[k]);		
					m_maphU2GamvsEn2Gam[stage]->fill((TwoGamTracks->m_pTrack1->emcShower()->energy()+TwoGamTracks->m_pTrack2->emcShower()->energy()),k+1,1);
					m_maphU2GammasInvM[stage]->fill(TwoGamTracks->GetMass(),1);
					m_maphU2GEAsym[stage]->fill(TwoGamTracks->GetEAsym());			
					m_maphU2GCThetaHelicity[stage]->fill(TwoGamTracks->GetCosTheta2GHelicityG1());
					m_maphU2GCThetaHelicityvsInvM[stage]->fill(TwoGamTracks->GetMass(),TwoGamTracks->GetCosTheta2GHelicityG1());
					m_maphU2GOpeningAngle[stage]->fill(TwoGamTracks->Get2GOpeningAngle());
					m_maphU2GDeltaTheta[stage]->fill(TwoGamTracks->Get2GDeltaTheta());
					m_maphU2GDeltaPhi[stage]->fill(TwoGamTracks->Get2GDeltaPhi());
					m_maphU2GDeltaThetavs2GOpeningAngle[stage]->fill(TwoGamTracks->Get2GOpeningAngle(),TwoGamTracks->Get2GDeltaTheta());
					m_maphU2GDeltaPhivs2GOpeningAngle[stage]->fill(TwoGamTracks->Get2GOpeningAngle(),TwoGamTracks->Get2GDeltaPhi());
					m_maphU2GDeltaPhivs2GDeltaTheta[stage]->fill(TwoGamTracks->Get2GDeltaTheta(),TwoGamTracks->Get2GDeltaPhi());		
					}	 
			}
			
			if(stage>=2)
			{	 
			//m_maphU2CombswSharedGammas[stage]->fill(m_NsharedGam,1);
			m_maphU2CombswSharedGammas[stage]->fill(m_mapU2GList[stage].GetNCombs2WSharedGam(),1);			
m_maphUValid2Combs[stage]->fill(m_mapU2GList[stage].m_aValidCombs2.size(),1.);
			}
			
			if(stage>=1 && stage<=3)
			{
				for(int k=0;k<m_mapU2GList[stage].m_aValidCombs2.size();k++)
				{
				cPi0Tracks* pt2G1 = (m_mapU2GList[stage].m_aValidCombs2[k]).m_pTrack1;
				cPi0Tracks* pt2G2 = (m_mapU2GList[stage].m_aValidCombs2[k]).m_pTrack2;
				m_maphU2G2vs2G1InvM[stage]->fill(pt2G1->GetMass(),pt2G2->GetMass(),1);
				}
			}
			
			if(stage==3)
			{
				 //for(int k=0;k<m_mapU2GList[3].m_aValidCombs2.size();k++) {
				 //
				 //}
			}
			
			if(stage>=3 && stage<=4)
			{
			
			}
			
			if(stage>=4)
			{

				m_maphUPi01InvM[stage]->fill(m_dUPi01InvM,1.);
				m_maphUPi02InvM[stage]->fill(m_dUPi02InvM,1.);
				m_maphUFitPi01InvM[stage]->fill(m_dUFitPi01InvM,1.);
				m_maphUFitPi02InvM[stage]->fill(m_dUFitPi02InvM,1.);
				m_maphUMissM[stage]->fill(m_dUMissMass,1.);
				m_maphUMissE[stage]->fill(m_dUMissE,1.);
				m_maphUDeltaEemcE4Gam[stage]->fill(m_dUDeltaEemcE4Gam,1.);
				m_maphU2Pi0InvM[stage]->fill(m_dU2Pi0InvM,1.);
				m_maphUFit2Pi0InvM[stage]->fill(m_dUFit2Pi0InvM,1.);
				m_maphUFit2Pi0InvMvs2Pi0InvM[stage]->fill(m_dU2Pi0InvM,m_dUFit2Pi0InvM,1.);
				m_maphUFitEAsymPi01[stage]->fill(m_dUFitEAsymPi01);
				m_maphUFitEAsymPi02[stage]->fill(m_dUFitEAsymPi02);
				m_maphUSumPt[stage]->fill(m_dUSumPtCM,1.);
				m_maphUFitSumPt[stage]->fill(m_dUFitSumPtCM,1.);
				m_maphUcosThetaPi01CM2Gam[stage]->fill(m_dUcosThetaPi01CM2Gam,1.);
				m_maphUFitcosThetaPi01CM2Gam[stage]->fill(m_dUFitcosThetaPi01CM2Gam,1.);
				m_maphUCThetaHelicity[stage]->fill(m_dUCThetaHelicity,1);
				m_maphUFitCThetaHelicity[stage]->fill(m_dUFitCThetaHelicity,1);			
			}
	}

if(option=="Tagged" || option=="eTagged" || option=="pTagged")
	{
	int iTagType=0;
	if(option=="Tagged") iTagType=0;
	if(option=="eTagged") iTagType=1;
	if(option=="pTagged") iTagType=2;
	
	if(iTagType==1)
		{
		if(m_iTQTag>=0) return;
		}
	if(iTagType==2)
		{
		if(m_iTQTag<=0) return;
		}	
		
	vector<int> key; key.resize(2);
	key[0]=iTagType; key[1]=stage;
	m_maphTNumEvt[key]->fill(0.,1);
	m_maphTNCharged[key]->fill(m_iNCharged,1.);
	m_maphTNNeutral[key]->fill(m_iNNeutral,1.);
	
	if(stage<=1)
		{
		for(int k=0;k<m_mapTCTList[stage].GetERTList().size();k++)
				{
				
				EvtRecTrack*  ERT = (m_mapTCTList[stage].GetERTList())[k];
				double P=ERT->mdcKalTrack()->p();
				double eEMCTot=ERT->emcShower()->energy() ;
				m_maphTEtotOvrPvsP[key]->fill(P,eEMCTot/P);
				
				}
		
		}
	
	if(stage>=1 && stage<=2)
		{
		
		for(int k=0;k<(m_mapTGList[stage].GetGamTable()).size();k++)
			{		
			EvtRecTrack* ERT = (m_mapTGList[stage].GetGamTable())[k];		
			int trackIndex = (m_mapTGList[stage].GetGamTableTrackIndex())[k];
			HepLorentzVector lvG = m_mapTGList[stage].Get4VecFromEMC(ERT);
			m_maphTEGam[key]->fill(lvG.e());
			m_maphTPtGam[key]->fill(GetPt(lvG));
			}
		
		
		}
	
	if(stage>=2)
		{
		m_maphTSumEemc[key]->fill(m_dTSumEemc,1.);
		m_maphTNGoodGamma[key]->fill(m_iTAnzGoodGamma,1);
		
		for(int k=0;k<(m_mapT2GList[stage].GetPi0Tracks()).size();k++){		
					vector<cPi0Tracks>& a2GamTracksTable = m_mapT2GList[stage].GetPi0Tracks();
					cPi0Tracks* TwoGamTracks = &(a2GamTracksTable[k]);		
					m_maphT2GamvsEn2Gam[key]->fill((TwoGamTracks->m_pTrack1->emcShower()->energy()+TwoGamTracks->m_pTrack2->emcShower()->energy()),k+1,1);
					m_maphT2GammasInvM[key]->fill(TwoGamTracks->GetMass(),1);
					m_maphT2GEAsym[key]->fill(TwoGamTracks->GetEAsym());			
					m_maphT2GCThetaHelicity[key]->fill(TwoGamTracks->GetCosTheta2GHelicityG1());
					m_maphT2GCThetaHelicityvsInvM[key]->fill(TwoGamTracks->GetMass(),TwoGamTracks->GetCosTheta2GHelicityG1());
					m_maphT2GOpeningAngle[key]->fill(TwoGamTracks->Get2GOpeningAngle());
					m_maphT2GDeltaTheta[key]->fill(TwoGamTracks->Get2GDeltaTheta());
					m_maphT2GDeltaPhi[key]->fill(TwoGamTracks->Get2GDeltaPhi());
					m_maphT2GDeltaThetavs2GOpeningAngle[key]->fill(TwoGamTracks->Get2GOpeningAngle(),TwoGamTracks->Get2GDeltaTheta());
					m_maphT2GDeltaPhivs2GOpeningAngle[key]->fill(TwoGamTracks->Get2GOpeningAngle(),TwoGamTracks->Get2GDeltaPhi());
					m_maphT2GDeltaPhivs2GDeltaTheta[key]->fill(TwoGamTracks->Get2GDeltaTheta(),TwoGamTracks->Get2GDeltaPhi());		
					}
					
			for(int k=0;k<m_mapT2GList[stage].m_aValidCombs2.size();k++)
				{
				cPi0Tracks* pt2G1 = (m_mapT2GList[stage].m_aValidCombs2[k]).m_pTrack1;
				cPi0Tracks* pt2G2 = (m_mapT2GList[stage].m_aValidCombs2[k]).m_pTrack2;
				m_maphT2G2vs2G1InvM[key]->fill(pt2G1->GetMass(),pt2G2->GetMass(),1);
				}
		}
	if(stage>=2 && stage<=3)
		{
		
				
		}
	
	if(stage>=3 && stage<=5)
		{
		m_maphTValid2Combs[key]->fill(m_mapT2GList[stage].m_aValidCombs2.size(),1.);
		m_maphT2CombswSharedGammas[key]->fill(m_mapT2GList[stage].GetNCombs2WSharedGam(),1);
		}
	
	if(stage==4)
		{
		
		}
	
	if(stage>4)
		{
		m_maphTKinFitChi2[key]->fill(m_dTTotalChi2,1);
		}
	
	if(stage>=5)
		{
		
		m_maphTPi01InvM[key]->fill(m_dTPi01InvM,1.);
		m_maphTPi02InvM[key]->fill(m_dTPi02InvM,1.);
		m_maphTFitPi01InvM[key]->fill(m_dTFitPi01InvM,1.);
		m_maphTFitPi02InvM[key]->fill(m_dTFitPi02InvM,1.);
		m_maphTMissM[key]->fill(m_dTMissMass,1);
		m_maphTMissE[key]->fill(m_dTMissE,1);
		m_maphTCThetaHelicity[key]->fill(m_dTCThetaHelicity,1);
		m_maphT2Pi0InvM[key]->fill(m_dT2Pi0InvM,1.);
		m_maphTFit2Pi0InvM[key]->fill(m_dTFit2Pi0InvM,1.);
		m_maphTFit2Pi0InvMvs2Pi0InvM[key]->fill(m_dT2Pi0InvM,m_dTFit2Pi0InvM,1.);
		m_maphTFitEAsymPi01[key]->fill(m_dTFitEAsymPi01);
		m_maphTFitEAsymPi02[key]->fill(m_dTFitEAsymPi02);
		m_maphTSumPt[key]->fill(m_dTSumPtCM,1.);
		m_maphTFitSumPt[key]->fill(m_dTFitSumPtCM,1.);
		m_maphTFitcosThetaPi01CM2Gam[key]->fill(m_dTFitcosThetaPi01CM2Gam,1.);
		m_maphTRGam[key]->fill(m_dTRGam,1.);
		m_maphTFitRGam[key]->fill(m_dTFitRGam,1.);
		m_maphTQ2[key]->fill(m_dTQ2,1.);
		m_maphTcosThetaPi01CM2Gam[key]->fill(m_dTcosThetaPi01CM2Gam,1);
		m_maphTPhiPi01CM2Gam[key]->fill(m_dTPhiPi01CM2Gam,1);
		}
		
	}	
return;		
}
//----------------------------------------------------------------------------------------------------------
