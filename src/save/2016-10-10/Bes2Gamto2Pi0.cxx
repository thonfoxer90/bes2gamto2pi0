

#include "Bes2Gamto2Pi0/Bes2Gamto2Pi0.h"
#include <sstream>

#define SSTR( x ) dynamic_cast< std::ostringstream & >( \
            ( std::ostringstream() << std::dec << x ) ).str()

using namespace Event;

///////////////////////////////////////////////////////////////////////////
//
//Analysis for e+ e- -> e+ e- pi0 pi0 at BES-III
//Author: Brice Garillon, Institut fuer Kernphysik, Uni Mainz, Germany 
//
///////////////////////////////////////////////////////////////////////////

//----------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------
// class Bes2Gamto2Pi0
//----------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------
Bes2Gamto2Pi0::Bes2Gamto2Pi0(const std::string& name, ISvcLocator* pSvcLocator) :
   Algorithm(name, pSvcLocator)
//----------------------------------------------------------------------------------------------------------
{
  //Declare the properties
	//declareProperty("UntaggedAnalysis", m_bDoUntagged = true);
	//declareProperty("eTaggedAnalysis", m_bDoeTagged = true);
  //declareProperty("MonteCarloAnalysis", m_MC_Analysis = false);
  //m_dCMSEnergy         = 3.773;       // psi(3770)
	m_dMasselec					 = 511e-6;
  m_dMassPi0           = 0.134976;    // PDG 2010
  m_dMassPiCharged     = 0.139570;    // PDG 2010
	m_dCrossingAngle		 = 0.011;
  m_dEoPcut            = 1.2;//0.8;
  m_dPi0GcutHigh       = 2.5;//1.4;
  m_dPhEnergyCutBarrel = 0.025;
  m_dPhEnergyCutEndCap = 0.05;
  m_dMaxPi0Combs       = 20;
  //m_dMaxGoodPh         = 15;
	m_dMaxGoodPh         = 9;
  m_dCylRad            = 1;//2.5;  // for point of closest approach
  m_dCylHeight         = 10;//15.0; // for point of closest approach
  m_dPi0MassCutLow     = 0.10;
  m_dPi0MassCutHigh    = 0.16;
  m_dCosThetaMax       = 0.92;
  m_dCosThetaBarrel    = 0.83;
  m_dMaxTime           = 14;  // *50ns
  //m_lvBoost            = HepLorentzVector(0.0415,0,0,m_dCMSEnergy);
  m_lvBoost            = HepLorentzVector(m_dCMSEnergy*sin(m_dCrossingAngle),0,0,m_dCMSEnergy);
	m_bKalFit						 = true;
	//m_bKalFit						 = false;
  //m_dChisqMaxValue     = 400;
	m_dChisqMaxValue     = 200;
	//m_dUSumPtMax				 = 0.05;
	m_dUSumPtMax				 = 0.2;
	m_dUSumEemcMin			 = 0.5;
	m_dUSumEemcMax			 = 4.5;
  m_bMCrecorded        = false;
	
	m_bDoUntagged				 = true;
	//m_bDoUntagged				 = false;
	m_bDoeTagged				 = true;
	//m_bWriteNt2G				 = true;
	m_bWriteNt2G				 = false;
	m_bWriteNt2x2G			 = true;
	//m_bWriteNt2x2G			 = false;
	
	
}
//----------------------------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------------------------
StatusCode Bes2Gamto2Pi0::initialize()
//----------------------------------------------------------------------------------------------------------
{
    cout << "initialize begins" << endl;
    MsgStream log(msgSvc(), name());
    log << MSG::INFO << "in initialize()" << endreq;

		m_iAll        = 0;
    m_iUPreSkip    = 0;
    m_iUPhotonSkip = 0;
    m_iUPi0Skip    = 0;
    m_iUFitSkip   = 0;
		m_iUPreKept		=	0;
		m_iUPhotonKept	=	0;
		m_iUPi0kept		=	0;
		m_iUFitKept		=	0;
    m_iAcceptedU  = 0;
		
    m_KalKinFit  = KalmanKinematicFit::instance();
    m_Vtxfit     = VertexFit::instance();
    m_PID        = ParticleID::instance();
		
		
		
    //Initialize ROOT trees and declare Ntuples
		//All MC Truth
		NTuplePtr allmctruth(ntupleSvc(), "FILE1/AllMCTruth");
		
		if(allmctruth){m_NAmcTruth=allmctruth;}
		else{m_NAmcTruth = ntupleSvc()->book("FILE1/AllMCTruth",CLID_ColumnWiseTuple,"All Monte Carlo-Truth");}
		
		if(m_NAmcTruth){
			m_NAmcTruth->addItem("AmcNpart",m_iAmcNpart,0,50);			
			m_NAmcTruth->addItem("AmcID",m_iAmcNpart,m_adAmcID);	
			m_NAmcTruth->addItem("AmcPrimary",m_iAmcNpart,m_iAmcPrimary);
			m_NAmcTruth->addItem("AmcDecayGenerator",m_iAmcNpart,m_iAmcDecayGenerator);
			m_NAmcTruth->addItem("AmcMother",m_iAmcNpart,m_iAmcMother);
			m_NAmcTruth->addItem("AmcP",m_iAmcNpart,m_adAmcP);	
			m_NAmcTruth->addItem("AmcTheta",m_iAmcNpart,m_adAmcTheta);	
			m_NAmcTruth->addItem("AmcPhi",m_iAmcNpart,m_adAmcPhi);
			m_NAmcTruth->addItem("AmcE",m_iAmcNpart,m_adAmcE);
			m_NAmcTruth->addItem("Amc2pi0InvM",m_dAmc2pi0InvM);	
			m_NAmcTruth->addItem("AmcUSumPt2Pi0",m_dAmcUSumPt2Pi0);

			m_NAmcTruth->addItem("AmceTSumPt2Pi0",m_dAmceTSumPt2Pi0);
			m_NAmcTruth->addItem("AmcpTSumPt2Pi0",m_dAmcpTSumPt2Pi0);
			
			}
		else {
      log << MSG::ERROR << "  Cannot book N-tuple1 MC truth: " << long(m_NAmcTruth) << endreq;
      return StatusCode::FAILURE;
   	}
		
		int hnum;
		double xmin,xmax,ymin,ymax;
		double deltax,deltay;
		int nbinsx,nbinsy;
		//Untagged
		m_hU2CombswValidFit=	histoSvc()->book("Untagged/","2CombswValidFit","Number of (2#gamma) pairs with valid kinematic fit",30,0.,30.);
		m_hU2CombswSharedGammas =	histoSvc()->book("Untagged/","2CombswSharedGammas","Number of (2#gamma) pairs with shared photons",30,0.,30.);
		
		m_h2G1vs2G2InvM =	histoSvc()->book("Untagged/","m_h2G1vs2G2InvM","M_(2#gamma)1 vs M_(2#gamma)2",100,0.,0.5,100,0.,0.5);
		
		for(int i=0;i<8;i++){
		
		m_maphUNumEvt[i]	=	histoSvc()->book(Form("Untagged/Stage%i",i),"NumEvt","Number of events ; ;",1,0.,1.);		
		m_maphUSumEemc[i]	=	histoSvc()->book(Form("Untagged/Stage%i",i),"SumEemc","Total Energy sum in EMC ; E_{EMC} (GeV);",512,0.,6.);
		
		if(i<=1){
			m_maphUNumGamvsEnGam[i] = histoSvc()->book(Form("Untagged/Stage%i",i),"NumGamvsEnGam","; E_{#gamma} (GeV); #gamma number",256,0.,5.,50,0,50);
		}
		
		if(i==1){
			m_maphU2GammasInvM[i] = histoSvc()->book(Form("Untagged/Stage%i",i),"2GammasInvM","Total Energy sum in EMC ; E_{EMC} (GeV);",500,0.,5.);
		}
		
		if(i>=1 && i<=2){
		m_maphU2GamvsEn2Gam[i] = histoSvc()->book(Form("Untagged/Stage%i",i),"2GamvsEn2Gam","; E_{2#gamma} (GeV); #gamma_{1} index",256,0.,5.,50,0,50);
		}
		
		if(i>=2){
			m_maphUValid2Combs[i]	=	histoSvc()->book(Form("Untagged/Stage%i",i),"Valid2Combs"," ; N_{Valid 2x(2 #gamma)};",30,0.,30.);
		}
		
		if(i==1 || i==3){
		m_maphU2G1vs2G2InvM[i] =	MakeIHistogram2D(Form("Untagged/Stage%i",i),"2G1vs2G2InvM","M_{2#gamma}1 vs M_{2#gamma}2; M_{(2#gamma)1} (GeV); M_{(2#gamma)2} (GeV)",0.002,0.,1.,0.002,0.,1.);
		}
		
		if(i==3){
			m_maphU2G1InvM[i] = MakeIHistogram1D(Form("Untagged/Stage%i",i),"2G1InvM",";M_{(2#gamma)1} (GeV)",0.001,0.1,0.16);
			m_maphU2G2InvM[i] = MakeIHistogram1D(Form("Untagged/Stage%i",i),"2G2InvM",";M_{(2#gamma)2} (GeV)",0.001,0.1,0.16);
			m_maphUEAsym2G1[i] = histoSvc()->book(Form("Untagged/Stage%i",i),"EAsym2G1","; E_{Asym (2#gamma)1}",100,0.,2.);
			m_maphUEAsym2G2[i] = histoSvc()->book(Form("Untagged/Stage%i",i),"EAsym2G2","; E_{Asym (2#gamma)2}",100,0.,2.);
			m_maphU2x2GCThetaMiss[i]=	histoSvc()->book(Form("Untagged/Stage%i",i),"2x2GCThetaMiss","; cos #theta_{Miss}",100,-1.,1.);
			m_maphU2x2GCTheta2G1HelicityG1[i]= histoSvc()->book(Form("Untagged/Stage%i",i),"x2GCTheta2G1HelicityG1","; cos #theta_{H (2#gamma)1}",100,-1.,1.);
			m_maphU2x2GCTheta2G2HelicityG1[i]= histoSvc()->book(Form("Untagged/Stage%i",i),"x2GCTheta2G2HelicityG1","; cos #theta_{H (2#gamma)1}",100,-1.,1.);
			m_maphU2x2GInvM[i] = MakeIHistogram1D(Form("Untagged/Stage%i",i),"2x2GInvM","; M_{2x(2#gamma)} (GeV)",0.02,0.,4.);		
			m_maphU2x2GSumPt[i]	=	MakeIHistogram1D(Form("Untagged/Stage%i",i),"2x2GSumPt",";|#vec{P^{*}_{(2#gamma)1}}+#vec{P^{*}_{(2#gamma)2}}| (GeV/c)",0.01,0.,5.);
			m_maphU2x2GcosTheta2G1CM2Gam[i]	=	histoSvc()->book(Form("Untagged/Stage%i",i),"2x2GcosTheta2G1CM2Gam",";cos #theta*_{(2#gamma)1}",100,-1.,1.);
		}
		
		if(i>=3 && i<=4){
			m_maphUKinFitChi2[i] = histoSvc()->book(Form("Untagged/Stage%i",i),"KinFitChi2","Total fit; #Chi^{2}",2000,0.,2000.);
		}			
		
		if(i>=4){	
			m_maphUPi01InvM[i] = MakeIHistogram1D(Form("Untagged/Stage%i",i),"Pi01InvM","Unfitted ;M_{#pi^{0}1}^{Unfitted} (GeV)",0.001,0.1,0.16);
			m_maphUPi02InvM[i] = MakeIHistogram1D(Form("Untagged/Stage%i",i),"Pi02InvM","Unfitted ;M_{#pi^{0}2}^{Unfitted} (GeV)",0.001,0.1,0.16);
			m_maphUFitPi01InvM[i] = MakeIHistogram1D(Form("Untagged/Stage%i",i),"FitPi01InvM","Fitted ;M_{#pi^{0}1} (GeV)",0.001,0.1,0.16);
			m_maphUFitPi02InvM[i] = MakeIHistogram1D(Form("Untagged/Stage%i",i),"FitPi02InvM","Fitted ;M_{#pi^{0}2} (GeV)",0.001,0.1,0.16);
			m_maphU2Pi0InvM[i] = MakeIHistogram1D(Form("Untagged/Stage%i",i),"2Pi0InvM","Unfitted ; M_{#pi^{0}#pi^{0}}^{Unfitted} (GeV)",0.02,0.,4.);
			m_maphUFit2Pi0InvM[i] = MakeIHistogram1D(Form("Untagged/Stage%i",i),"2FitPi0InvM","Fit ; M_{#pi^{0}#pi^{0}} (GeV)",0.02,0.,4.);
			m_maphUFit2Pi0InvMvs2Pi0InvM[i] = histoSvc()->book(Form("Untagged/Stage%i",i),"Fit2Pi0InvMvs2Pi0InvM",";M_{#pi^{0}#pi^{0}}^{Unfitted} (GeV);M_{#pi^{0}#pi^{0}}^{Fit} (GeV)",400,0.,4.,400,0.,4.);
			m_maphUFitEAsymPi01[i] = histoSvc()->book(Form("Untagged/Stage%i",i),"FitEAsymPi01","; E_{Asym #pi^{0}1}",100,0.,2.);
			m_maphUFitEAsymPi02[i] = histoSvc()->book(Form("Untagged/Stage%i",i),"FitEAsymPi02","; E_{Asym #pi^{0}2}",100,0.,2.);
			m_maphUSumPt[i]	=	MakeIHistogram1D(Form("Untagged/Stage%i",i),"SumPt",";|#vec{P^{*}_{#pi^{O}1}}+#vec{P^{*}_{#pi^{O}2}}|^{Unfitted} (GeV/c)",0.01,0.,5.);
			m_maphUFitSumPt[i]	=	MakeIHistogram1D(Form("Untagged/Stage%i",i),"FitSumPt",";|#vec{P^{*}_{#pi^{O}1}}+#vec{P^{*}_{#pi^{O}2}}| (GeV/c)",0.01,0.,5.);		
			m_maphUFitcosThetaPi01CM2Gam[i]	=	histoSvc()->book(Form("Untagged/Stage%i",i),"FitcosThetaPi01CM2Gam",";cos #theta*",100,-1.,1.);
		}	
			
		}
		
		NTuplePtr ut(ntupleSvc(), "FILE1/Untagged");
		
		if   ( ut ) {m_NUntagged = ut;}
    else {m_NUntagged = ntupleSvc()->book("FILE1/Untagged", CLID_ColumnWiseTuple, "untagged");}
		
		if ( m_NUntagged ) {
     m_NUntagged->addItem("URunNumber"    ,m_dURunNumber);
     m_NUntagged->addItem("UEvtNumber"    ,m_dUEvtNumber);
     m_NUntagged->addItem("UCMSEnergy"    ,m_dUCMSEnergy);
     m_NUntagged->addItem("iUAnzGoodP"    ,m_iUAnzGoodP);
     m_NUntagged->addItem("iUAnzGoodM"    ,m_iUAnzGoodM);
     m_NUntagged->addItem("iUAnzGoodGamma",m_iUAnzGoodGamma);
     m_NUntagged->addItem("iUAnzPi0Tracks",m_iUAnzPi0Tracks);
     m_NUntagged->addItem("iUAnz2Combs"  ,m_iUAnz2Combs);
     m_NUntagged->addItem("iUAnz3Combs"  ,m_iUAnz3Combs);
     m_NUntagged->addItem("UTotalChi2"   ,m_dUTotalChi2);
     m_NUntagged->addItem("UPi01Chi2"    ,m_dUPi01Chi2);
     m_NUntagged->addItem("UPi02Chi2"    ,m_dUPi02Chi2);
     m_NUntagged->addItem("iUmcTruePi0"  ,m_iUmcTruePi0);
     m_NUntagged->addItem("iUmcTrueFSR"  ,m_iUmcTrueFSR);
     m_NUntagged->addItem("iUmcTrueGammaFromJpsi"  ,m_iUmcTrueGammaFromJpsi);
     m_NUntagged->addItem("iUmcTrueOtherGamma"  ,m_iUmcTrueOtherGamma);
     m_NUntagged->addItem("iUmcTrueOther" ,m_iUmcTrueOther);
     m_UPi01    .AttachToNtuple(m_NUntagged,"UPi01");
     m_UPi02    .AttachToNtuple(m_NUntagged,"UPi02");
		 m_UPi01CM    .AttachToNtuple(m_NUntagged,"UPi01CM");
     m_UPi02CM    .AttachToNtuple(m_NUntagged,"UPi02CM");
     m_UFitPi01    .AttachToNtuple(m_NUntagged,"UFitPi01");
     m_UFitPi02    .AttachToNtuple(m_NUntagged,"UFitPi02");
     //for (int a=0;a<GAMMAS+5;a++) {m_UGammas   [a].AttachToNtuple(m_NUntagged,"UG"   +SSTR(a)+"emc",true); m_NUntagged->addItem("ClosestTr"+SSTR(a),m_dUClosestTrack[a]);}
		 for (int a=0;a<GAMMAS+5;a++) {m_UGammas   [a].AttachToNtuple(m_NUntagged,"UG"   +SSTR(a)+"emc",true);}
     for (int a=0;a<5;a++)        {m_UFitGammas[a].AttachToNtuple(m_NUntagged,"UFitG"+SSTR(a),true);}
     m_NUntagged->addItem("UVtxChi2"  ,m_dUVtxChi2);
     m_NUntagged->addItem("VtxPosX"   ,m_dUVtxPosX);
     m_NUntagged->addItem("VtxPosY"   ,m_dUVtxPosY);
     m_NUntagged->addItem("VtxPosZ"   ,m_dUVtxPosZ);
     m_NUntagged->addItem("IPposX"    ,m_dUIPposX);
     m_NUntagged->addItem("IPposY"    ,m_dUIPposY);
     m_NUntagged->addItem("IPposZ"    ,m_dUIPposZ);
		 m_NUntagged->addItem("dU2pi0InvM"    ,m_dU2pi0InvM);
		 m_NUntagged->addItem("dUFit2pi0InvM"    ,m_dUFit2pi0InvM);
		 m_NUntagged->addItem("dUPi01InvM"    ,m_dUPi01InvM);
		 m_NUntagged->addItem("dUFitPi01InvM"    ,m_dUFitPi01InvM);
		 m_NUntagged->addItem("dUPi02InvM"    ,m_dUPi02InvM);
		 m_NUntagged->addItem("dUFitPi02InvM"    ,m_dUFitPi02InvM);		 
		 m_NUntagged->addItem("dUEAsymPi01"    ,m_dUEAsymPi01);
		 m_NUntagged->addItem("dUFitEAsymPi01"    ,m_dUFitEAsymPi01);
		 m_NUntagged->addItem("dUEAsymPi02"    ,m_dUEAsymPi02);
		 m_NUntagged->addItem("dUFitEAsymPi02"    ,m_dUFitEAsymPi02);			 
		 m_NUntagged->addItem("dUSumPtCM"    ,m_dUSumPtCM);
		 m_NUntagged->addItem("dUFitSumPtCM"    ,m_dUFitSumPtCM);
		 m_NUntagged->addItem("dUPCMbeamelecpositron"    ,m_dUPCMbeamelecpositron);
		 m_NUntagged->addItem("dUPbeamelecpositron"    ,m_dUPbeamelecpositron);
		 m_NUntagged->addItem("dUPi01CMPx"    ,m_dUPi01CMPx);
		 m_NUntagged->addItem("dUPi01CMPy"    ,m_dUPi01CMPy);
		 m_NUntagged->addItem("dUPi01CMPz"    ,m_dUPi01CMPz);
		 m_NUntagged->addItem("dUPi01CMPt"    ,m_dUPi01CMPt);
		 m_NUntagged->addItem("dUPi02CMPx"    ,m_dUPi02CMPx);
		 m_NUntagged->addItem("dUPi02CMPy"    ,m_dUPi02CMPy);
		 m_NUntagged->addItem("dUPi02CMPz"    ,m_dUPi02CMPz);
		 m_NUntagged->addItem("dUPi02CMPt"    ,m_dUPi02CMPt);
		 m_NUntagged->addItem("dUSumEemc"    ,m_dUSumEemc);
		 m_NUntagged->addItem("dUFitcosThetaPi01CM2Gam"    ,m_dUFitcosThetaPi01CM2Gam);
		 m_NUntagged->addItem("iIsU2pi0"    ,m_iIsU2pi0);
  }
   else {
      log << MSG::ERROR << "  Cannot book N-tuple1 untagged: " << long(m_NUntagged) << endreq;
      return StatusCode::FAILURE;
   }
	 	 
	 //Electron Tagged
	 m_heTDist	=	histoSvc()->book("eTagged","Dist","Distance to IP - Charged Tracks",128,0.,50.);
		m_heTCylRad	=	histoSvc()->book("eTagged","CylRad","Radial distance of closest approach to IP - Charged Tracks; |dr| (cm)",128,0.,10.);		
		m_heTCylHeight	=	histoSvc()->book("eTagged","CylHeight","Axial distance of closest approach to IP - Charged Tracks; |dz| (cm) ",128,0.,50.);
		m_heTPChargedTracks	=	histoSvc()->book("eTagged","PChargedTracks","Momentum - Charged Tracks; P (GeV/c)",128,0.,6.);
		m_heTEtotOvrPvsP	=	histoSvc()->book("eTagged","EtotOvrPvsP","Momentum - Charged Tracks and valid EMC shower; P (GeV/c)",128,0.,6.,128,0.,1.5);
	
	for(int i=0;i<7;i++){
		m_mapheTNumEvt[i]	=	histoSvc()->book(Form("eTagged/Stage%i",i),"NumEvt","Number of events ; ;",1,0.,1.);
		
		if(i<=1){
			m_mapheTEtotOvrPvsP[i]	=	histoSvc()->book(Form("eTagged/Stage%i",i),"EtotOvrPvsP",";P (GeV/c); E_{tot}/P",128,0.,6.,128,0.,1.5);
			m_mapheTNCharged[i]	=	histoSvc()->book(Form("eTagged/Stage%i",i),"NCharged",";N_{Charged}",100,0.,100.);
			m_mapheTNNeutral[i]	=	histoSvc()->book(Form("eTagged/Stage%i",i),"NNeutral",";N_{Neutral}",100,0.,100.);
		}

		if(i==2){
		m_mapheT2GCThetaMiss[i]=	histoSvc()->book(Form("eTagged/Stage%i",i),"2GCThetaMiss","; cos #theta_{Miss}",100,-1.,1.);
		m_mapheT2GCThetaHelicity[i]=	histoSvc()->book(Form("eTagged/Stage%i",i),"2GCThetaHelicity","; cos #theta_{H}",100,-1.,1.);
		m_mapheT2GOpeningAngle[i]=	histoSvc()->book(Form("eTagged/Stage%i",i),"2GOpeningAngle","; #alpha_{#gamma #gamma}",100,0.,3.2);
		m_mapheT2GDeltaTheta[i]=	histoSvc()->book(Form("eTagged/Stage%i",i),"2GDeltaTheta","; #Delta#theta_{#gamma #gamma}",100,-3.2,3.2);
		m_mapheT2GDeltaPhi[i]=	histoSvc()->book(Form("eTagged/Stage%i",i),"2GDeltaPhi","; #Delta#phi_{#gamma #gamma}",100,-6.4,6.4);
		}

		if(i>=2){
			m_mapheTSumEemc[i]	=	histoSvc()->book(Form("eTagged/Stage%i",i),"SumEemc","Total Energy sum in EMC ; E_{EMC} (GeV);",512,0.,6.);
		}
		
		if(i>=2 && i<=3){
			m_mapheT2GamvsEn2Gam[i] = histoSvc()->book(Form("eTagged/Stage%i",i),"2GamvsEn2Gam","; E_{2#gamma} (GeV); #gamma_{1} index",256,0.,5.,50,0,50);
			m_mapheT2GammasInvM[i] =	histoSvc()->book(Form("eTagged/Stage%i",i),"2GammasInvM"," ;M_{2#gamma} (GeV)",500,0.,5.);	
			m_mapheT2G1vs2G2InvM[i] =	histoSvc()->book(Form("eTagged/Stage%i",i),"2G1vs2G2InvM","M_{2#gamma}1 vs M_{2#gamma}2; M_{(2#gamma)1} (GeV); M_{(2#gamma)2} (GeV)",100,0.,1.,100,0.,1.);
			
		}

		if(i>=3 && i<=5){
			m_mapheTValid2Combs[i]	=	histoSvc()->book(Form("eTagged/Stage%i",i),"Valid2Combs"," ; N_{Valid 2x(2 #gamma)};",30,0.,30.);
			m_mapheT2CombswSharedGammas[i] =	histoSvc()->book(Form("eTagged/Stage%i",i),"2CombswSharedGammas","Number of (2#gamma) pairs with shared photons ; N_{2#gamma}[Shared #gamma]",30,0.,30.);
		}

		if(i==4){
			m_mapheT2G1InvM[i] = MakeIHistogram1D(Form("eTagged/Stage%i",i),"2G1InvM",";M_{(2#gamma)1} (GeV)",0.001,0.1,0.16);
			m_mapheT2G2InvM[i] = MakeIHistogram1D(Form("eTagged/Stage%i",i),"2G2InvM",";M_{(2#gamma)2} (GeV)",0.001,0.1,0.16);
			m_mapheTEAsym2G1[i] = histoSvc()->book(Form("eTagged/Stage%i",i),"EAsym2G1","; E_{Asym (2#gamma)1}",100,0.,2.);
			m_mapheTEAsym2G2[i] = histoSvc()->book(Form("eTagged/Stage%i",i),"EAsym2G2","; E_{Asym (2#gamma)2}",100,0.,2.);
			m_mapheT2x2GCThetaMiss[i]=	histoSvc()->book(Form("eTagged/Stage%i",i),"2x2GCThetaMiss","; cos #theta_{Miss}",100,-1.,1.);
			m_mapheT2x2GCTheta2G1HelicityG1[i]= histoSvc()->book(Form("eTagged/Stage%i",i),"x2GCTheta2G1HelicityG1","; cos #theta_{H (2#gamma)1}",100,-1.,1.);
			m_mapheT2x2GCTheta2G2HelicityG1[i]= histoSvc()->book(Form("eTagged/Stage%i",i),"x2GCTheta2G2HelicityG1","; cos #theta_{H (2#gamma)1}",100,-1.,1.);
			m_mapheT2x2GInvM[i] = MakeIHistogram1D(Form("eTagged/Stage%i",i),"2x2GInvM","; M_{2x(2#gamma)} (GeV)",0.02,0.,4.);		
			m_mapheT2x2GSumPt[i]	=	MakeIHistogram1D(Form("eTagged/Stage%i",i),"2x2GSumPt",";|#vec{P^{*}_{e-}}+#vec{P^{*}_{(2#gamma)1}}+#vec{P^{*}_{(2#gamma)2}}| (GeV/c)",0.01,0.,5.);	
			m_mapheT2x2GcosTheta2G1CM2Gam[i]	=	histoSvc()->book(Form("eTagged/Stage%i",i),"2x2GcosTheta2G1CM2Gam",";cos #theta*_{(2#gamma)1}",100,-1.,1.);
			m_mapheT2x2GRGam[i] = histoSvc()->book(Form("eTagged/Stage%i",i),"2x2GRGam",";R_{#gamma}",100,-1.,1.);
		}		

		if(i>=4 && i<=5){
			m_mapheTKinFitChi2[i] = histoSvc()->book(Form("eTagged/Stage%i",i),"KinFitChi2","Total fit; #Chi^{2}",2000,0.,2000.);
		}

		if(i==5){
			m_mapheTPi01InvM[i] = MakeIHistogram1D(Form("eTagged/Stage%i",i),"Pi01InvM","Unfitted ;M_{#pi^{0}1}^{Unfitted} (GeV)",0.001,0.1,0.16);
			m_mapheTPi02InvM[i] = MakeIHistogram1D(Form("eTagged/Stage%i",i),"Pi02InvM","Unfitted ;M_{#pi^{0}2}^{Unfitted} (GeV)",0.001,0.1,0.16);
			m_mapheTFitPi01InvM[i] = MakeIHistogram1D(Form("eTagged/Stage%i",i),"FitPi01InvM","Fitted ;M_{#pi^{0}1} (GeV)",0.001,0.1,0.16);
			m_mapheTFitPi02InvM[i] = MakeIHistogram1D(Form("eTagged/Stage%i",i),"FitPi02InvM","Fitted ;M_{#pi^{0}2} (GeV)",0.001,0.1,0.16);
			m_mapheT2Pi0InvM[i] = MakeIHistogram1D(Form("eTagged/Stage%i",i),"2Pi0InvM","Unfitted ; M_{#pi^{0}#pi^{0}}^{Unfitted} (GeV)",0.02,0.,4.);
			m_mapheTFit2Pi0InvM[i] = MakeIHistogram1D(Form("eTagged/Stage%i",i),"2FitPi0InvM","Fit ; M_{#pi^{0}#pi^{0}} (GeV)",0.02,0.,4.);
			m_mapheTFit2Pi0InvMvs2Pi0InvM[i] = histoSvc()->book(Form("eTagged/Stage%i",i),"Fit2Pi0InvMvs2Pi0InvM",";M_{#pi^{0}#pi^{0}}^{Unfitted} (GeV);M_{#pi^{0}#pi^{0}}^{Fit} (GeV)",200,0.,4.,200,0.,4.);	
			m_mapheTFitEAsymPi01[i] = histoSvc()->book(Form("eTagged/Stage%i",i),"FitEAsymPi01","; E_{Asym #pi^{0}1}",100,0.,2.);
			m_mapheTFitEAsymPi02[i] = histoSvc()->book(Form("eTagged/Stage%i",i),"FitEAsymPi02","; E_{Asym #pi^{0}2}",100,0.,2.);				
			m_mapheTSumPt[i]	=	MakeIHistogram1D(Form("eTagged/Stage%i",i),"eTSumPt",";|#vec{P^{*}_{e-}}+#vec{P^{*}_{#pi^{O}1}}+#vec{P^{*}_{#pi^{O}2}}|^{Unfitted} (GeV/c)",0.01,0.,5.);
			m_mapheTFitSumPt[i]	=	MakeIHistogram1D(Form("eTagged/Stage%i",i),"eTFitSumPt",";|#vec{P^{*}_{e-}}+#vec{P^{*}_{#pi^{O}1}}+#vec{P^{*}_{#pi^{O}2}}| (GeV/c)",0.01,0.,5.);		
			m_mapheTFitcosThetaPi01CM2Gam[i]	=	histoSvc()->book(Form("eTagged/Stage%i",i),"FitcosThetaPi01CM2Gam",";cos #theta*",100,-1.,1.);
			m_mapheTRGam[i] = histoSvc()->book(Form("eTagged/Stage%i",i),"RGam",";R^{Unfitted}_{#gamma}",100,-1.,1.);
			m_mapheTFitRGam[i] = histoSvc()->book(Form("eTagged/Stage%i",i),"FitRGam",";R_{#gamma}",100,-1.,1.);
		}

		}
			
	 NTuplePtr et2G(ntupleSvc(), "FILE1/eTagged2G");
	 if   ( et2G ) {m_NeT2G = et2G;}
    else {m_NeT2G = ntupleSvc()->book("FILE1/eTagged2G", CLID_ColumnWiseTuple, "electron tagged");}
		
		if ( m_NeT2G) {
		m_NeT2G->addItem("eT2GRunNumber"    ,m_deT2GRunNumber);
		m_NeT2G->addItem("eT2GEvtNumber"    ,m_deT2GEvtNumber);
    m_NeT2G->addItem("eT2GCMSEnergy"    ,m_deT2GCMSEnergy);
		m_NeT2G->addItem("ieT2GN2Gs"    ,m_ieT2GN2Gs,0,50);
		m_NeT2G->addIndexedItem("deT2GEG1",m_ieT2GN2Gs,m_deT2GEG1);
		m_NeT2G->addIndexedItem("deT2GEG2",m_ieT2GN2Gs,m_deT2GEG2);
		m_NeT2G->addIndexedItem("deT2GEAsymmetry",m_ieT2GN2Gs,m_deT2GEAsymmetry);		
		m_NeT2G->addIndexedItem("deT2GMass2G",m_ieT2GN2Gs,m_deT2GMass2G);
		m_NeT2G->addIndexedItem("deT2GCThetaMiss",m_ieT2GN2Gs,m_deT2GCThetaMiss);
		m_NeT2G->addIndexedItem("deT2GCThetaHelicity",m_ieT2GN2Gs,m_deT2GCThetaHelicity);
		m_NeT2G->addIndexedItem("deT2GOpeningAngle",m_ieT2GN2Gs,m_deT2GOpeningAngle);
		m_NeT2G->addIndexedItem("deT2GDeltaTheta",m_ieT2GN2Gs,m_deT2GDeltaTheta);
		m_NeT2G->addIndexedItem("deT2GDeltaPhi",m_ieT2GN2Gs,m_deT2GDeltaPhi);
		
		}
	 
	 NTuplePtr et2x2G(ntupleSvc(), "FILE1/eTagged2x2G");
	 if   ( et2x2G ) {m_NeT2x2G = et2x2G;}
    else {m_NeT2x2G = ntupleSvc()->book("FILE1/eTagged2x2G", CLID_ColumnWiseTuple, "electron tagged");}
		
		if ( m_NeT2x2G) {
		m_NeT2x2G->addItem("eT2x2GRunNumber"    ,m_deT2x2GRunNumber);
		m_NeT2x2G->addItem("eT2x2GEvtNumber"    ,m_deT2x2GEvtNumber);
    m_NeT2x2G->addItem("eT2x2GCMSEnergy"    ,m_deT2x2GCMSEnergy);
		m_NeT2x2G->addItem("ieT2x2GNSizePi0TrackTable"    ,m_ieT2x2GNSizePi0TrackTable);
		//m_NeT2x2G->addItem("ieT2x2GN2x2G"    ,m_ieT2x2GN2x2G,0,50);
		m_NeT2x2G->addItem("ieT2x2GN2x2G"    ,m_ieT2x2GN2x2G,0,1024);
		m_NeT2x2G->addIndexedItem("deT2x2GE2G1",m_ieT2x2GN2x2G,m_deT2x2GE2G1);
		m_NeT2x2G->addIndexedItem("deT2x2GE2G2",m_ieT2x2GN2x2G,m_deT2x2GE2G2);
		m_NeT2x2G->addIndexedItem("deT2x2GInvM2G1",m_ieT2x2GN2x2G,m_deT2x2GInvM2G1);
		m_NeT2x2G->addIndexedItem("deT2x2GInvM2G2",m_ieT2x2GN2x2G,m_deT2x2GInvM2G2);
		m_NeT2x2G->addIndexedItem("deT2x2GEAsym2G1",m_ieT2x2GN2x2G,m_deT2x2GEAsym2G1);
		m_NeT2x2G->addIndexedItem("deT2x2GEAsym2G2",m_ieT2x2GN2x2G,m_deT2x2GEAsym2G2);
		m_NeT2x2G->addIndexedItem("deT2x2GCThetaMiss",m_ieT2x2GN2x2G,m_deT2x2GCThetaMiss);
		m_NeT2x2G->addIndexedItem("deT2x2GInvM2x2G",m_ieT2x2GN2x2G,m_deT2x2GInvM2x2G);
		m_NeT2x2G->addIndexedItem("deT2x2GSumPtCM",m_ieT2x2GN2x2G,m_deT2x2GSumPtCM);
		m_NeT2x2G->addIndexedItem("deT2x2GCTheta2G1HelicityG1",m_ieT2x2GN2x2G,m_deT2x2GCTheta2G1HelicityG1);
		m_NeT2x2G->addIndexedItem("deT2x2GCTheta2G2HelicityG1",m_ieT2x2GN2x2G,m_deT2x2GCTheta2G2HelicityG1);
		m_NeT2x2G->addIndexedItem("deT2x2GRGam",m_ieT2x2GN2x2G,m_deT2x2GRGam);
		}
		
			
	 NTuplePtr et(ntupleSvc(), "FILE1/eTagged");
	 if   ( et ) {m_NeTagged = et;}
    else {m_NeTagged = ntupleSvc()->book("FILE1/eTagged", CLID_ColumnWiseTuple, "electron tagged");}
		
		if ( m_NeTagged ) {
     m_NeTagged->addItem("eTRunNumber"    ,m_deTRunNumber);
		 m_NeTagged->addItem("eTEvtNumber"    ,m_deTEvtNumber);
     m_NeTagged->addItem("eTCMSEnergy"    ,m_deTCMSEnergy);
		 m_NeTagged->addItem("ieTQTag"    ,m_ieTQTag);
		 m_NeTagged->addItem("deTSumEemc"    ,m_deTSumEemc);
     m_NeTagged->addItem("ieTAnzGoodP"    ,m_ieTAnzGoodP);
     m_NeTagged->addItem("ieTAnzGoodM"    ,m_ieTAnzGoodM);
     m_NeTagged->addItem("ieTAnzGoodGamma",m_ieTAnzGoodGamma);
     m_NeTagged->addItem("ieTAnzPi0Tracks",m_ieTAnzPi0Tracks);
     m_NeTagged->addItem("ieTAnz2Combs"  ,m_ieTAnz2Combs);
     m_NeTagged->addItem("ieTAnz3Combs"  ,m_ieTAnz3Combs);
     m_NeTagged->addItem("eTTotalChi2"   ,m_deTTotalChi2);
     m_NeTagged->addItem("eTPi01Chi2"    ,m_deTPi01Chi2);
     m_NeTagged->addItem("eTPi02Chi2"    ,m_deTPi02Chi2);
     m_NeTagged->addItem("ieTmcTruePi0"  ,m_ieTmcTruePi0);
     m_NeTagged->addItem("ieTmcTrueFSR"  ,m_ieTmcTrueFSR);
     m_NeTagged->addItem("ieTmcTrueGammaFromJpsi"  ,m_ieTmcTrueGammaFromJpsi);
     m_NeTagged->addItem("ieTmcTrueOtherGamma"  ,m_ieTmcTrueOtherGamma);
     m_NeTagged->addItem("ieTmcTrueOther" ,m_ieTmcTrueOther);
		 m_eTLepton    .AttachToNtuple(m_NeTagged,"eTLepton");
     m_eTPi01    .AttachToNtuple(m_NeTagged,"eTPi01");
     m_eTPi02    .AttachToNtuple(m_NeTagged,"eTPi02");
		 m_eTFitLepton    .AttachToNtuple(m_NeTagged,"eTFitLepton");
     m_eTFitPi01    .AttachToNtuple(m_NeTagged,"eTFitPi01");
     m_eTFitPi02    .AttachToNtuple(m_NeTagged,"eTFitPi02");
     //for (int a=0;a<GAMMAS+5;a++) {m_eTGammas   [a].AttachToNtuple(m_NeTagged,"eTG"   +SSTR(a)+"emc",true); m_NeTagged->addItem("ClosestTr"+SSTR(a),m_deTClosestTrack[a]);}
		 for (int a=0;a<GAMMAS+5;a++) {m_eTGammas   [a].AttachToNtuple(m_NeTagged,"eTG"   +SSTR(a)+"emc",true);}
     for (int a=0;a<5;a++)        {m_eTFitGammas[a].AttachToNtuple(m_NeTagged,"eTFitG"+SSTR(a),true);}
     m_NeTagged->addItem("eTVtxChi2"  ,m_deTVtxChi2);
     m_NeTagged->addItem("etVtxPosX"   ,m_deTVtxPosX);
     m_NeTagged->addItem("etVtxPosY"   ,m_deTVtxPosY);
     m_NeTagged->addItem("etVtxPosZ"   ,m_deTVtxPosZ);
     m_NeTagged->addItem("etIPposX"    ,m_deTIPposX);
     m_NeTagged->addItem("etIPposY"    ,m_deTIPposY);
		 m_NeTagged->addItem("etIPposZ"    ,m_deTIPposZ);
		 m_NeTagged->addItem("deT2pi0InvM"    ,m_deT2pi0InvM);
		 m_NeTagged->addItem("deTFit2pi0InvM"    ,m_deTFit2pi0InvM);
		 m_NeTagged->addItem("deTPi01InvM"    ,m_deTPi01InvM);
		 m_NeTagged->addItem("deTFitPi01InvM"    ,m_deTFitPi01InvM);
		 m_NeTagged->addItem("deTPi02InvM"    ,m_deTPi02InvM);
		 m_NeTagged->addItem("deTFitPi02InvM"    ,m_deTFitPi02InvM);
		 m_NeTagged->addItem("deTQ2"    ,m_deTQ2);
		 m_NeTagged->addItem("deTFitQ2"    ,m_deTFitQ2);
		 m_NeTagged->addItem("deTEAsymPi01"    ,m_deTEAsymPi01);
		 m_NeTagged->addItem("deTFitEAsymPi01"    ,m_deTFitEAsymPi01);
		 m_NeTagged->addItem("deTEAsymPi02"    ,m_deTEAsymPi02);
		 m_NeTagged->addItem("deTFitEAsymPi02"    ,m_deTFitEAsymPi02);
		 m_NeTagged->addItem("deTSumPtCM"    ,m_deTSumPtCM);
		 m_NeTagged->addItem("deTFitSumPtCM"    ,m_deTFitSumPtCM);
		 m_NeTagged->addItem("deTcosThetaPi01CM2Gam"    ,m_deTcosThetaPi01CM2Gam);
		 m_NeTagged->addItem("deTFitcosThetaPi01CM2Gam"    ,m_deTFitcosThetaPi01CM2Gam);
		 m_NeTagged->addItem("deTPhiPi01CM2Gam"    ,m_deTPhiPi01CM2Gam);
		 m_NeTagged->addItem("deTFitPhiPi01CM2Gam"    ,m_deTFitPhiPi01CM2Gam);
		 m_NeTagged->addItem("deTCThetaHelicity"    ,m_deTCThetaHelicity);
		 m_NeTagged->addItem("deTFitCThetaHelicity"    ,m_deTFitCThetaHelicity);
		 m_NeTagged->addItem("deTRGam"    ,m_deTRGam);
		 m_NeTagged->addItem("deTFitRGam"    ,m_deTFitRGam);
		 m_NeTagged->addItem("iIseT2pi0"    ,m_iIseT2pi0);

		 //m_NeTagged->addItem("deTMCQ2",m_deTMCQ2);
		 m_NeTagged->addItem("deTMC2pi0InvM",m_deTMC2pi0InvM);
		 m_NeTagged->addItem("deTMCSumPtCM",m_deTMCSumPtCM);
  }
   else {
      log << MSG::ERROR << "  Cannot book N-tuple1 e- tagged: " << long(m_NeTagged) << endreq;
      return StatusCode::FAILURE;
   }
	 
	 
	 StatusCode sc = Gaudi::svcLocator()->service( "VertexDbSvc", m_vtxSvc,true);
   if(sc.isFailure())
     {
     //assert(false); //terminate program
     log << MSG::ERROR << "Cannot book VertexDbSvc!!" <<endmsg;
     return StatusCode::FAILURE;
   }
   //m_particleTable = m_partPropSvc->PDT();
	 
   return StatusCode::SUCCESS;
}
//----------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------
StatusCode Bes2Gamto2Pi0::execute()
//----------------------------------------------------------------------------------------------------------
{

//cout<<"begin execute"<<endl;
    MsgStream log(msgSvc(), name());
    log << MSG::INFO << "in execute()" << endreq;

    SmartDataPtr<Event::EventHeader> eventHeader(eventSvc(),"/Event/EventHeader");
    

		//log << MSG::WARNING <<  m_iAll << endreq;
    
    m_iAll ++;

		if(m_iAll%10000==0) log << MSG::WARNING << "Event # " << m_iAll<< " " << endreq;
		//log << MSG::WARNING << "Event # " << m_iAll<< " " << endreq;
		
		m_dRunNumber   = eventHeader->runNumber();
		m_dEvtNumber  = eventHeader->eventNumber(); 
		m_dCMSEnergy = beam_energy(abs(eventHeader->runNumber()));
    if (m_dCMSEnergy == -1) {log<<MSG::FATAL<<"unknown runNr, can not determine CMS Energy!"<<endreq; return StatusCode::FAILURE;}  
		
		//------ initial state kinematics ------//
		double TotEngBeam = m_dCMSEnergy/2 + m_dMasselec;
		double Abs3MomBeam = sqrt(TotEngBeam*TotEngBeam - m_dMasselec*m_dMasselec);
  
		m_lvBeamElectron.setPx(Abs3MomBeam*sin(m_dCrossingAngle));
		m_lvBeamElectron.setPy(0);
		m_lvBeamElectron.setPz(-Abs3MomBeam*cos(m_dCrossingAngle));
		m_lvBeamElectron.setE(TotEngBeam);
  
		m_lvBeamPositron.setPx(Abs3MomBeam*sin(m_dCrossingAngle));
		m_lvBeamPositron.setPy(0);
		m_lvBeamPositron.setPz(Abs3MomBeam*cos(m_dCrossingAngle));
		m_lvBeamPositron.setE(TotEngBeam);
		
		m_lvBoost        = m_lvBeamElectron+m_lvBeamPositron;
		//m_lvBoost            = HepLorentzVector(m_dCMSEnergy*sin(m_dCrossingAngle),0,0,m_dCMSEnergy);		

		m_vectboostCM=m_lvBoost.boostVector();		
		m_lvBeamElectronCM=m_lvBeamElectron;
		m_lvBeamElectronCM.boost(-m_vectboostCM);
		m_lvBeamPositronCM=m_lvBeamPositron;
		m_lvBeamPositronCM.boost(-m_vectboostCM);

		m_dPi0GcutHigh   = m_dCMSEnergy*0.6;
		
		//------ Get montecarlo truth event ------//
		SmartDataPtr<McParticleCol> mcParticles(eventSvc(),"/Event/MC/McParticleCol");
		
  	if(!mcParticles) {
		m_iMCel						 = -2;
		m_iMCelprimary		 = -2;
		m_iMCpositron			 = -2;
		m_iMCpositronprimary=-2;
    m_iMCpi0           = -2;
    m_iMCfsr           = -2;
    m_iMCGammaFromJpsi = -2;
    m_iMCOtherGamma    = -2;
    m_iMCOther         = -2;
    m_bMCrecorded      = false;
    m_dmTotFinalMCtruth = -2;
  	} 
		else { 
  	DoMCTruth();				
		}
		
		//------ Get reconstructed event ------//
		SmartDataPtr<EvtRecEvent> evtRecEvent(eventSvc(), "/Event/EvtRec/EvtRecEvent");
    if ( ! evtRecEvent ) {
        log << MSG::FATAL << "Could not find EvtRecEvent" << endreq;
        return StatusCode::FAILURE;
    }
    SmartDataPtr<EvtRecTrackCol> evtRecTrackCol(eventSvc(), "/Event/EvtRec/EvtRecTrackCol");
    if ( ! evtRecTrackCol ) {
        log << MSG::FATAL << "Could not find EvtRecTrackCol" << endreq;
        return StatusCode::FAILURE;
    }
    Gaudi::svcLocator()->service("VertexDbSvc", m_vtxSvc);
		
		
		//------ initialize virtual box ------//
    m_hp3IP.setX(0);
    m_hp3IP.setY(0);
    m_hp3IP.setZ(0);
    if (m_vtxSvc->isVertexValid()) {
      double* dbv =  m_vtxSvc->PrimaryVertex();
      m_hp3IP.setX(dbv[0]);
      m_hp3IP.setY(dbv[1]);
      m_hp3IP.setZ(dbv[2]);
    }
    else{std::cout<<"no IP"<<std::endl;}
	
		//------ initialize iterators ------//
    m_itBegin      = evtRecTrackCol->begin();
    m_itEndCharged = m_itBegin + evtRecEvent->totalCharged();
    m_itEndNeutral = m_itBegin + evtRecEvent->totalTracks();
    
    
		m_iNCharged= evtRecEvent->totalCharged();
		m_iNNeutral= evtRecEvent->totalNeutral();
		
		
		//----- Event selection ------//		
		if(m_bDoUntagged==true) DoUntagged();
		if(m_bDoeTagged==true) DoeTagged();
		
		//DoUntagged();
		//DoeTagged();
		
		return StatusCode::SUCCESS;
}
//----------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------- 
StatusCode Bes2Gamto2Pi0::finalize()
//----------------------------------------------------------------------------------------------------------
{
  MsgStream log(msgSvc(), name());
  log << MSG::INFO << "in finalize()" << endreq;
	log << MSG::WARNING << endreq;
	log << MSG::WARNING << "****** Total number of events " << m_iAll << " ******" << endreq;
	log << MSG::WARNING << "****** UNTAGGED : Number of kept events at  neutral particles preselection " << m_iUPreKept << " ******" << endreq;
	log << MSG::WARNING << "****** UNTAGGED :Number of kept events at preselect.+ good photons requirement " << m_iUPhotonKept << " ******" << endreq;
	log << MSG::WARNING << "****** UNTAGGED : Number of kept events at npreselect.+ good photons requirement + 2 pi0 candidates requirement " << m_iUPi0kept << " ******" << endreq;
  log << MSG::WARNING << "****** UNTAGGED : Number of kept events at npreselect.+ good photons requirement + 2 pi0 candidates + kalmanfit requirement " << m_iUFitKept << " ******" << endreq;
	log << MSG::WARNING << "****** UNTAGGED : Number of untagged e+e-2pi0 events " << m_iAcceptedU << " ******" << endreq;
	log << MSG::WARNING << endreq;
	log << MSG::WARNING << "****** TAGGED : Number of valid MDC charged tracks " << m_ieTValidMDCKept << " ******" << endreq;
	log << MSG::WARNING << "****** TAGGED : Number of valid MDC + close to IP charged tracks " << m_ieTValidMDCIPKept << " ******" << endreq;
	log << MSG::WARNING << "****** TAGGED : Number of good negative tracks " << m_ieTGoodNegKept << " ******" << endreq;
	log << MSG::WARNING << "****** TAGGED : Number of events with only one lepton " << m_ieT1Lepton << " ******" << endreq;
	log << MSG::WARNING << "****** TAGGED : Number of kept events at 1e- + good photons requirement " << m_ieTPhotonKept << " ******" << endreq;
  log << MSG::WARNING << "****** TAGGED : Number of kept events at npreselect.+ good photons + 2 pi0 candidates " << m_ieTPi0kept << " ******" << endreq;
	log << MSG::WARNING << "****** TAGGED : Number of kept events at npreselect.+ good photons + 2 pi0 candidates + kinematic fit " << m_ieTFitKept << " ******" << endreq;
	log << MSG::WARNING << "****** TAGGED : Number of untagged e+e-2pi0 events " << m_iAcceptedeT << " ******" << endreq;
	return StatusCode::SUCCESS;
}
//----------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------
double Bes2Gamto2Pi0::beam_energy(int runNo)
//----------------------------------------------------------------------------------------------------------
{
    double ecms = -1;

    if( runNo>=23463 && runNo<=23505 ){//psi(4040) dataset-I
      ecms = 4.0104;
    }
    else if( runNo>=23509 && runNo<24141 ){//psi(4040) dataset-II
      ecms = 4.0093;
    }
    else if( runNo>=-24141 && runNo<=-23463 ){// psi(4009)
      ecms = 4.009;
    }
    else if( runNo>=24151 && runNo<=24177 ){//psi(2S) data
      ecms = 3.686;
    }
    else if( runNo>=29677 && runNo<=30367 ){//Y(4260) data-I
      ecms = 4.26;
    }
    else if( runNo>=31561 && runNo<=31981 ){//Y(4260) data-II
      ecms = 4.26;
    }
    else if( runNo>=31327 && runNo<=31390 ){//4420 data
      ecms = 4.42;
    }
    else if( runNo>=31281 && runNo<=31325 ){//4390 data
      ecms = 4.39;
    }
    else if( runNo>=30616 && runNo<=31279 ){//Y(4360) data
      ecms = 4.36;
    }
    else if( runNo>=30492 && runNo<=30557 ){// 4310 data
      ecms = 4.31;
    }
    else if( runNo>=30438 && runNo<=30491 ){// 4230 data
      ecms = 4.23;
    }
    else if( runNo>=30372 && runNo<=30437 ){// 4190 data
      ecms = 4.19;
    }
    else if( runNo>=31983 && runNo<=32045 ){// 4210 data
      ecms = 4.21;
    }
    else if( runNo>=32046 && runNo<=32140 ){// 4220 data
      ecms = 4.22;
    }
    else if( runNo>=32141 && runNo<=32226 ){// 4245 data
      ecms = 4.245;
    }
    else if( runNo>=32239 && runNo<=32850 ){// new 4230 data-I
      ecms = 4.23;
    }
    else if( runNo>=32851 && runNo<=33484 ){// new 4230 data-II
      ecms = 4.228;
    }
    else if( runNo>=33490 && runNo<=33556 ){// 3810 data
      ecms = 3.81;
    }
    else if( runNo>=33571 && runNo<=33657 ){// 3900 data
      ecms = 3.90;
    }
    else if( runNo>=33659 && runNo<=33719 ){// 4090 data
      ecms = 4.09;
    }
    else if( runNo>=11397 && runNo<=23454 ){// psi(3770) data
      ecms = 3.773;
    }

    return ecms;
}

//---------------------------------------------------------------------------------------------------------- 
double Bes2Gamto2Pi0::GetSumPt(std::vector<HepLorentzVector> vectlv)
{
double SumPt;
HepLorentzVector Sumlv(0,0,0,0);
Hep3Vector Sum3v(0,0,0);
Hep3Vector Sum3vperp(0,0,0);

for(int k=0;k<vectlv.size();k++){
	Sumlv+=vectlv[k];
	Sum3v+=vectlv[k].vect();
	Sum3vperp+=(vectlv[k].vect()).perpPart();
	}
//SumPt=(Sum3v.perpPart()).r();	
//SumPt=Sum3vperp.r();
SumPt=Sumlv.perp();
/*
SumPt=sqrt(pow(Sumlv.x(),2)
					+pow(Sumlv.y(),2)
					);
*/

//m_dUSumPtCM=((m_lvPi01CM.vect()).perpPart()+(m_lvPi02CM.vect()).perpPart()).r();
//m_dUSumPtCM=((m_lvPi01CM.vect()+m_lvPi02CM.vect()).perpPart()).r();
//m_dUSumPtCM=(m_lvPi01CM+m_lvPi02CM).perp();

//Hep3Vector vZAxis(0,0,1);
//m_dUSumPtCM=((m_lvPi01CM.vect()+m_lvPi02CM.vect()).perpPart(vZAxis)).r();
//m_dUSumPtCM=sqrt(pow((m_lvPi01CM.vect()+m_lvPi02CM.vect()).x(),2)
//								+pow((m_lvPi01CM.vect()+m_lvPi02CM.vect()).y(),2)
//								);

return SumPt;
}
//---------------------------------------------------------------------------------------------------------- 

//---------------------------------------------------------------------------------------------------------- 
double Bes2Gamto2Pi0::GetPt(HepLorentzVector lv)
{
double Pt;
Pt=lv.perp();
return Pt;
}
//---------------------------------------------------------------------------------------------------------- 

//---------------------------------------------------------------------------------------------------------- 
bool Bes2Gamto2Pi0::IsPhoton(EvtRecTrack* pTrack)
{
if (!pTrack->isEmcShowerValid()) {return (false);}
  if (   (abs(cos(pTrack->emcShower()->theta())) <= m_dCosThetaBarrel  && pTrack->emcShower()->energy() < m_dPhEnergyCutBarrel)
      || (abs(cos(pTrack->emcShower()->theta())) >  m_dCosThetaBarrel  && pTrack->emcShower()->energy() < m_dPhEnergyCutEndCap)
      || abs(pTrack->emcShower()->time()) >  m_dMaxTime 
     )
     {return (false);}
	if (abs(RejectEMCtrack(pTrack)) < 0.349) {return (false);}
  return (true);

}
//---------------------------------------------------------------------------------------------------------- 

//---------------------------------------------------------------------------------------------------------- 
double Bes2Gamto2Pi0::RejectEMCtrack (EvtRecTrack* pCur)
//---------------------------------------------------------------------------------------------------------- 
{

  EvtRecTrackIterator  itERT;
  double dAngMin = 200.0, dAng;
  Hep3Vector extpos;
  Hep3Vector emcpos(pCur->emcShower()->x(), pCur->emcShower()->y(), pCur->emcShower()->z());
  for (itERT  = m_itBegin;itERT != m_itEndCharged;itERT ++) {
    if(!(*itERT)->isExtTrackValid()) {continue;}
    if((*itERT)->extTrack()->emcVolumeNumber() == -1) {continue;}
    extpos = (*itERT)->extTrack()->emcPosition();
    double dAng = extpos.angle(emcpos);
    if(dAng < dAngMin){
      dAngMin = dAng;
    }
  }
  //if(dAngMin >= 200) {dAngMin = -4.0;}
  //if(dAngMin >= 200) {return (true);}
  //dAngMin *= 180.0/(CLHEP::pi);
  //if(fabs(dAngMin) < m_dDeltaAngleCut) {return (true);}

  return dAngMin;
}	      
//---------------------------------------------------------------------------------------------------------- 
void Bes2Gamto2Pi0::DoMCTruth()
{
SmartDataPtr<McParticleCol> mcParticles(eventSvc(),"/Event/MC/McParticleCol");

int  iPin  = 0;
			m_iMCel						 = 0;
			m_iMCelprimary		 = 0;
			m_iMCpositron			 = 0;
			m_iMCpositronprimary=0;
  		m_iMCpi0           = 0;
  		m_iMCfsr           = 0;
  		m_iMCGammaFromJpsi = 0;
  		m_iMCOtherGamma    = 0;
  		m_iMCOther         = 0;
  		m_bMCrecorded      = true;
  		m_dmTotFinalMCtruth  = -1;
			
			m_iAmcNpart=0;
			//FOR GALUGA+MANUAL GENERATOR ONLY!
			//PARTICLES ADDED BY KKMC (CHARM PRODUCTION) INTO MANUAL GENERATOR
			//GALUGA OUTPUT FILES CONTAINS PID AND 4 MOMENTUM COORDINATES ONLY.
			//KKMC ADD FALSE VERTICES AND FALSE MC FLAG (PRIMARY)
			//POSITRON IS COUNTED AS SECONDARY INSTEAD OF PRIMARY PARTICLE!
  		for( McParticleCol::iterator itMC = mcParticles->begin();itMC != mcParticles->end();itMC ++) {
    		
			
			int PID = (*itMC)->particleProperty();
      HepLorentzVector Particle = (*itMC)->initialFourMomentum();
      
      bool primary = (*itMC)->primaryParticle();
      bool leaf = (*itMC)->leafParticle();
      bool generator = (*itMC)->decayFromGenerator();
      bool flight = (*itMC)->decayInFlight();
      
      Event::McParticle Mother = (*itMC)->mother();
      int MPID = Mother.particleProperty();
			
			if(m_iAmcNpart <= m_iAmcNpart->range().distance()) {
				m_iAmcPrimary[m_iAmcNpart]=primary;
				m_iAmcDecayGenerator[m_iAmcNpart]=generator;
				m_adAmcID[m_iAmcNpart]=PID;
				m_iAmcMother[m_iAmcNpart]=MPID;
				m_adAmcP[m_iAmcNpart]=((*itMC)->initialFourMomentum()).rho();
				m_adAmcTheta[m_iAmcNpart]=((*itMC)->initialFourMomentum()).theta();
				m_adAmcPhi[m_iAmcNpart]=((*itMC)->initialFourMomentum()).phi();
				m_adAmcE[m_iAmcNpart]=((*itMC)->initialFourMomentum()).e();
				m_iAmcNpart++;
      	}
				
			switch(PID) {
				case 11: //electron
					m_iMCel++; 
					//With KKMC Generator fed into ManualGenerator,
					//Final state particles (e+e-pi0pi0) are produced from Psipp(PID:30443)
					//These particles are selected by requiring they come from Psipp decay.
					// 2 electrons are contained in MC files:
					// 1-incoming electron beam (Mother ID=11)
					// 2- final state electron (Mother ID=30443)
					if(MPID==30443) 
						{m_iMCelprimary++;
						m_lvmcel = (*itMC)->initialFourMomentum(); 
						}
					break;
				case -11: //positron
					m_iMCpositron++; 
					if(MPID==30443) 
						{m_iMCpositronprimary++;
						m_lvmcpositron = (*itMC)->initialFourMomentum(); 
						}
					break;
    		case 111:// pi0
					m_iMCpi0 ++;      
      		switch(iPin) {
      		case 0: iPin ++;// bFirstP0G = true;
          	m_lvmcPi01       = (*itMC)->initialFourMomentum();
        		break;
      		case 1: iPin ++;// bFirstP0G = true;
          	m_lvmcPi02       = (*itMC)->initialFourMomentum();
        		break;
      		case 2: 
						m_lvmcPi03 = (*itMC)->initialFourMomentum(); 
						break;
      		}
				case   22: // gamma 
				if ((*itMC)->mother().particleProperty() != 111) {m_iMCOtherGamma ++;}    // all gammas except from pi0 decay
      	break;
				
				default:
					// count only final state particles except from pi0 decays
						if ((*itMC)->daughterList().size()                     == 0   // does not decay -> final state particle
								&& (*itMC)->mother().particleProperty()               != 111 // from pi0  decay
         		) {
          		m_iMCOther ++;
      		}
      		break;
					
    		}
				      
  		}
			
			bool Isee2pi0=(m_iMCpi0 == 2 && m_iMCelprimary==1 && m_iMCpositronprimary ==1);
  		if (Isee2pi0) {
    	//m_dmTotFinalMCtruth  = (m_lvmcel+m_lvmcpositron+m_lvmcPi01+m_lvmcPi02).mag();
			//m_dAmcUSumPt2Pi0=(m_lvmcPi01+m_lvmcPi02).perp();
			m_dMC2pi0InvM=(m_lvmcPi01+m_lvmcPi02).m();
			
			m_dAmc2pi0InvM=m_dMC2pi0InvM;
			//e+e- CM frame
			m_lvmcelCM=m_lvmcel;
			m_lvmcelCM.boost(-m_vectboostCM);		
			m_lvmcpositronCM=m_lvmcpositron;
			m_lvmcpositronCM.boost(-m_vectboostCM);
			m_lvmcPi01CM=m_lvmcPi01;
			m_lvmcPi01CM.boost(-m_vectboostCM);
			m_lvmcPi02CM=m_lvmcPi02;
			m_lvmcPi02CM.boost(-m_vectboostCM);
			
			m_dMCUSumPtCM=GetPt(m_lvmcPi01CM+m_lvmcPi02CM);
			m_dMCeTSumPtCM=GetPt(m_lvmcelCM+m_lvmcPi01CM+m_lvmcPi02CM);
			m_dMCpTSumPtCM=GetPt(m_lvmcpositronCM+m_lvmcPi01CM+m_lvmcPi02CM);
			
			/*
			m_dAmcUSumPt2Pi0=GetPt(m_lvmcPi01CM+m_lvmcPi02CM);			
			m_dAmceTSumPt2Pi0=GetPt(m_lvmcelCM+m_lvmcPi01CM+m_lvmcPi02CM);
			m_dAmcpTSumPt2Pi0=GetPt(m_lvmcpositronCM+m_lvmcPi01CM+m_lvmcPi02CM);
			*/
			
			m_dAmcUSumPt2Pi0=m_dMCUSumPtCM;			
			m_dAmceTSumPt2Pi0=m_dMCeTSumPtCM;
			m_dAmcpTSumPt2Pi0=m_dMCpTSumPtCM;
  		}
			
			/*
			for( McParticleCol::iterator itMC = mcParticles->begin();itMC != mcParticles->end();itMC ++) {
    		
			
			int PID = (*itMC)->particleProperty();
      HepLorentzVector Particle = (*itMC)->initialFourMomentum();
      
      bool primary = (*itMC)->primaryParticle();
      bool leaf = (*itMC)->leafParticle();
      bool generator = (*itMC)->decayFromGenerator();
      bool flight = (*itMC)->decayInFlight();
      
      Event::McParticle Mother = (*itMC)->mother();
      int MPID = Mother.particleProperty();
			
			if(m_iAmcNpart <= m_iAmcNpart->range().distance()) {
				m_adAmcID[m_iAmcNpart]=PID;
				m_adAmcP[m_iAmcNpart]=Particle.rho();
				m_iAmcNpart++;
      	}
				
			switch(PID) {
				case 11: //electron
					m_iMCel++; 
					if(primary) m_iMCelprimary++;
					m_lvmcel = (*itMC)->initialFourMomentum(); 
					break;
				case -11: //positron
					m_iMCpositron++; 
					if(primary) m_iMCpositronprimary++;
					m_lvmcpositron = (*itMC)->initialFourMomentum(); 
					break;
    		case 111:// pi0
					m_iMCpi0 ++;      
      		switch(iPin) {
      		case 0: iPin ++;// bFirstP0G = true;
          	m_lvmcPi01       = (*itMC)->initialFourMomentum();
        		break;
      		case 1: iPin ++;// bFirstP0G = true;
          	m_lvmcPi02       = (*itMC)->initialFourMomentum();
        		break;
      		case 2: 
						m_lvmcPi03 = (*itMC)->initialFourMomentum(); 
						break;
      		}
				case   22: // gamma 
				if ((*itMC)->mother().particleProperty() != 111) {m_iMCOtherGamma ++;}    // all gammas except from pi0 decay
      	break;
				
				default:
					// count only final state particles except from pi0 decays
						if ((*itMC)->daughterList().size()                     == 0   // does not decay -> final state particle
								&& (*itMC)->mother().particleProperty()               != 111 // from pi0  decay
         		) {
          		m_iMCOther ++;
      		}
      		break;
					
    		}
				      
  		}
			
			bool Isee2pi0=(m_iMCpi0 == 2 && m_iMCelprimary==1 && m_iMCpositronprimary ==1 && m_iMCOther == 0 && m_iMCOtherGamma == 0);
  		if (Isee2pi0) {
    	m_dmTotFinalMCtruth  = (m_lvmcel+m_lvmcpositron+m_lvmcPi01+m_lvmcPi02).mag();
			m_dAmcUSumPt2Pi0=(m_lvmcPi01+m_lvmcPi02).perp();
  		}
			*/
			
			m_NAmcTruth->write();
			
/*
  	else { 
  		int  iPin  = 0;
			m_iMCel						 = 0;
			m_iMCelprimary		 = 0;
			m_iMCpositron			 = 0;
			m_iMCpositronprimary=0;
  		m_iMCpi0           = 0;
  		m_iMCfsr           = 0;
  		m_iMCGammaFromJpsi = 0;
  		m_iMCOtherGamma    = 0;
  		m_iMCOther         = 0;
  		m_bMCrecorded      = true;
  		m_dmTotFinalMCtruth  = -1;
	
			//Count types of MC particles
			for( McParticleCol::iterator itMC = mcParticles->begin();itMC != mcParticles->end();itMC ++) {
    		if ((*itMC)->particleProperty() == 23 || (*itMC)->particleProperty() == 91) {continue;} // Generator internal particle
    		if (abs((*itMC)->particleProperty()) < 11) {continue;} // Generator internal particle
    		switch((*itMC)->particleProperty()) {
				case   11: //electron 
					m_iMCel++; 
					if((*itMC)->primaryParticle()) m_iMCelprimary++;
					break; 
				case  -11: 
					m_iMCpositron++; 
					if((*itMC)->primaryParticle()) m_iMCpositronprimary++;
					break;
    		case   22: // gamma 
					if ((*itMC)->mother().particleProperty() == 443) {m_iMCGammaFromJpsi ++;} // 443 = J/psi -> gamma + anything
      		else if ((*itMC)->mother().particleProperty() != 111) {m_iMCOtherGamma ++;}    // all other except from pi0 decay
      		break;
    		case  -22: m_iMCfsr ++;   break; // FSR gamma
    		case  111: m_iMCpi0 ++;   break; // pi0
    		default:
					// count only final state particles except from pi0 decays
						if ((*itMC)->daughterList().size()                     == 0   // does not decay -> final state particle
								&& (*itMC)->mother().particleProperty()               != 111 // from pi0  decay
         		) {
          		m_iMCOther ++;
      		}
      		break;
    		}
  		}
	
  		for( McParticleCol::iterator itMC = mcParticles->begin();itMC != mcParticles->end();itMC ++) {
    		if (!(*itMC)->primaryParticle () && (*itMC)->particleProperty() == 22) {continue;}
    		switch((*itMC)->particleProperty()) {
				case 11: //electron
					m_lvmcel = (*itMC)->initialFourMomentum(); break;
				case -11: //positron
					m_lvmcpositron = (*itMC)->initialFourMomentum(); break;
    		case 111:// pi0      
      		switch(iPin) {
      		case 0: iPin ++;// bFirstP0G = true;
          	m_lvmcPi01       = (*itMC)->initialFourMomentum();
        		break;
      		case 1: iPin ++;// bFirstP0G = true;
          	m_lvmcPi02       = (*itMC)->initialFourMomentum();
        		break;
      		case 2: m_lvmcPi03 = (*itMC)->initialFourMomentum(); break;
      		}
    		break;
    		}
				      
  		}

  		if (m_iMCpi0 == 2 && m_iMCelprimary==1 && m_iMCpositronprimary ==1 && m_iMCOther == 0 && m_iMCOtherGamma == 0 && m_iMCGammaFromJpsi == 0) {
    	m_dmTotFinalMCtruth  = (m_lvmcel+m_lvmcpositron+m_lvmcPi01+m_lvmcPi02).mag();
  		}
		}
		*/
}

//---------------------------------------------------------------------------------------------------------- 
void Bes2Gamto2Pi0::DoUntagged()
{

MsgStream log(msgSvc(), name());
    log << MSG::INFO << "in execute()" << endreq;
		
SmartDataPtr<Event::EventHeader> eventHeader(eventSvc(),"/Event/EventHeader");
SmartDataPtr<EvtRecEvent> evtRecEvent(eventSvc(), "/Event/EvtRec/EvtRecEvent");
    if ( ! evtRecEvent ) {
        log << MSG::FATAL << "Could not find EvtRecEvent" << endreq;
        return;
    }
    SmartDataPtr<EvtRecTrackCol> evtRecTrackCol(eventSvc(), "/Event/EvtRec/EvtRecTrackCol");
    if ( ! evtRecTrackCol ) {
        log << MSG::FATAL << "Could not find EvtRecTrackCol" << endreq;
        return;
    }
    Gaudi::svcLocator()->service("VertexDbSvc", m_vtxSvc);
		

		
//------ prepare ntuples ------//		
		//ResetNTuples();
		m_dURunNumber   = m_dRunNumber;
		m_dUEvtNumber  = m_dEvtNumber;
    m_dUCMSEnergy = m_dCMSEnergy;	

		//IP pos
    m_dUIPposX  = m_hp3IP.x();
    m_dUIPposY  = m_hp3IP.y();
    m_dUIPposZ  = m_hp3IP.z();
    
    m_iUmcTrueFSR           = m_iMCfsr;
    m_iUmcTruePi0           = m_iMCpi0;
    m_iUmcTrueGammaFromJpsi = m_iMCGammaFromJpsi;
    m_iUmcTrueOtherGamma    = m_iMCOtherGamma;
    m_iUmcTrueOther         = m_iMCOther;

		
    // signal untagged
    m_iUAnzGoodP        = -1;
    m_iUAnzGoodM        = -1;
    m_iUAnzGoodGamma    = -1;
    m_iUAnzPi0Tracks    = -1;
    m_iUAnz2Combs       = -1;
    m_iUAnz3Combs       = -1;
		m_dUTotalChi2       = m_dChisqMaxValue;
    m_dUPi01Chi2        = m_dChisqMaxValue;
    m_dUPi02Chi2        = m_dChisqMaxValue;
		m_dUSumEemc					=0;
		m_iIsU2pi0 					=0;
		m_NsharedGam				=0;
		
    m_UPi01.Clear();
    m_UPi02.Clear();
		
    //for (int a=0;a<GAMMAS+4;a++) {m_UGammas[a].Clear(); m_dUClosestTrack[a] = -4.0;}
		for (int a=0;a<GAMMAS+4;a++) {m_UGammas[a].Clear();}
			
    m_UFitPi01.Clear();
    m_UFitPi02.Clear();
    for (int a=0;a<5;a++) {m_UFitGammas[a].Clear();}
    m_dUVtxChi2 = -1;
    m_dUVtxPosX = -1;
    m_dUVtxPosY = -1;
    m_dUVtxPosZ = -20;
		
		
		 m_aU2GList.SetIterators (m_itBegin,m_itEndCharged,m_itEndNeutral);
m_aU2GList.Init(m_dPi0MassCutLow,m_dPi0MassCutHigh,m_dPhEnergyCutBarrel,m_dPhEnergyCutEndCap,m_dCosThetaBarrel,m_dMaxTime,m_dCMSEnergy/2.0);   

m_aUPi0List.SetIterators (m_itBegin,m_itEndCharged,m_itEndNeutral);    m_aUPi0List.Init(m_dPi0MassCutLow,m_dPi0MassCutHigh,m_dPhEnergyCutBarrel,m_dPhEnergyCutEndCap,m_dCosThetaBarrel,m_dMaxTime,m_dCMSEnergy/2.0);

		//m_lvBoost            = HepLorentzVector(m_dCMSEnergy*sin(m_dCrossingAngle),0,0,m_dCMSEnergy);
		
		//------ preselector: skip, if there are insufficient amounts of tracks ------//
    //if (m_iNCharged > 0 || m_iNNeutral > 50) {
		if (m_iNCharged > 0) {
        //cout<<"# insuficinet tracks: ch= "<<evtRecEvent->totalCharged()<<" , neut= "<<evtRecEvent->totalNeutral()<<endl;
        m_iUPreSkip ++;
        return;
    }
		m_iUPreKept++;
    //cout<<"passed preselection: charged= "<<evtRecEvent->totalCharged()<<" , neutral= "<<evtRecEvent->totalNeutral()<<endl;
		

		//------ Find 2 pi0 candidates ------//
		/*
    //if (!Find2pi0Candidates ()) {return;}
		
		EvtRecTrackIterator    itERT;

    // clear lists

    // count good photons
    int iGood = 0;
    for (itERT = m_itEndCharged;itERT != m_itEndNeutral;itERT ++) {
        if (m_aUPi0List.IsPhoton(*itERT)) {iGood ++;}
    }
    if (iGood < 4 || iGood > m_dMaxGoodPh) {m_iUPhotonSkip ++; return;}
		m_iUPhotonKept++;
    m_iUAnzGoodGamma = iGood;

    // find pi0 -> 2gamma
    m_aUPi0List.FillList();
    if (m_aUPi0List.m_aPi0TracksTable.size() > m_dMaxPi0Combs) {m_iUPi0Skip ++; return;}
    m_aUPi0List.CreatedValid2Combs ();
    m_aUPi0List.CreatedValid3Combs ();
    // insufficient number of 2pi0 candidates
    if (m_aUPi0List.m_aValidCombs2.size() < 1) {m_iUPi0Skip ++; return;}
		m_iUPi0kept++;
		
    // record combinatorics
    m_iUAnzPi0Tracks  = m_aUPi0List.m_aPi0TracksTable.size();
    m_iUAnz2Combs     = m_aUPi0List.m_aValidCombs2.size();
    m_iUAnz3Combs     = m_aUPi0List.m_aValidCombs3.size();
 		*/
		
		EvtRecTrackIterator    itERT;
		
		//------ Loop over neutral tracks ------//
    int iGood = 0;
    for (itERT = m_itEndCharged;itERT != m_itEndNeutral;itERT ++) {
				int index = std::distance(m_itEndCharged,itERT);
				m_maphUNumGamvsEnGam[0]->fill((*itERT)->emcShower()->energy(), index,1);
				if ((*itERT)->isEmcShowerValid()) m_dUSumEemc+=(*itERT)->emcShower()->energy();				
				// count good photons
        if (m_aUPi0List.IsPhoton(*itERT)) {
				iGood ++;}
				
    }	
		//------ End of loop over neutral tracks ------//
		m_maphUNumEvt[0]->fill(0,1.);
		m_maphUSumEemc[0]->fill(m_dUSumEemc,1.);
		
    if (iGood < 4 || iGood > m_dMaxGoodPh) {m_iUPhotonSkip ++; return;}
		m_iUPhotonKept++;
    m_iUAnzGoodGamma = iGood;
		
		//Add good photons to the pi0 candidates list
		double m_dBeamEnergy=m_dCMSEnergy/2.0;
		EvtRecTrackIterator    itERT1;
		EvtRecTrackIterator    itERT2;
		int index2pair1=0;
		int index2pair2=0;
		for (itERT1 = m_itEndCharged;itERT1 !=m_itEndNeutral;itERT1 ++) {
		int index1 = std::distance(m_itEndCharged,itERT1);
		m_maphUNumGamvsEnGam[1]->fill((*itERT1)->emcShower()->energy(), index1,1);
    if (   !m_aUPi0List.IsPhoton(*itERT1)
		|| (*itERT1)->emcShower()->energy() > m_dBeamEnergy
       ) 
			 {continue;}
    	for (itERT2 = itERT1+1;itERT2 != m_itEndNeutral;itERT2 ++) {
      	//y ++;     
      	if (    !m_aUPi0List.IsPhoton(*itERT2)
	   		|| (*itERT2)->emcShower()->energy() > m_dBeamEnergy
	 			) 
				{continue;}	
					
			m_maphU2GamvsEn2Gam[1]->fill((*itERT1)->emcShower()->energy()+(*itERT2)->emcShower()->energy(),index2pair1,1);
			index2pair1++;

      double d2GMass = m_aUPi0List.GetM(*itERT1,0.0,*itERT2,0.0);
			m_maphU2GammasInvM[1]->fill(d2GMass,1);
			m_aU2GList.Add(*itERT1,*itERT2,d2GMass);				
      if (d2GMass > m_dPi0MassCutHigh || d2GMass < m_dPi0MassCutLow) {continue;}
      m_aUPi0List.Add(*itERT1,*itERT2,d2GMass);		
			m_maphU2GamvsEn2Gam[2]->fill((*itERT1)->emcShower()->energy()+(*itERT2)->emcShower()->energy(),index2pair2,1);
			index2pair2++;
     }
  	}
		m_maphUNumEvt[1]->fill(0,1.);
		m_maphUSumEemc[1]->fill(m_dUSumEemc,1.);				   
		
		//------ Loops over pions candidates ------//
		//Find good 2 gamma combinations
		ValidCombs2 aVC2dummy;
		m_aU2GList.m_aValidCombs2.clear();
  	for (int i=0;i<m_aU2GList.m_aPi0TracksTable.size();i++) {
    	for (int k=i+1;k<m_aU2GList.m_aPi0TracksTable.size();k++) {
      	// Exclude 2 gammas combinations that shares same photons
      	if (m_aU2GList.m_aPi0TracksTable[k].m_pTrack1 == m_aU2GList.m_aPi0TracksTable[i].m_pTrack1 ||
	 				  m_aU2GList.m_aPi0TracksTable[k].m_pTrack1 == m_aU2GList.m_aPi0TracksTable[i].m_pTrack2 ||
	  				m_aU2GList.m_aPi0TracksTable[k].m_pTrack2 == m_aU2GList.m_aPi0TracksTable[i].m_pTrack1 ||
	  				m_aU2GList.m_aPi0TracksTable[k].m_pTrack2 == m_aU2GList.m_aPi0TracksTable[i].m_pTrack2
	  				) 
				{m_NsharedGam++;continue;}
      	aVC2dummy.Set(&(m_aU2GList.m_aPi0TracksTable[i]),&(m_aU2GList.m_aPi0TracksTable[k]));
      	m_aU2GList.m_aValidCombs2.push_back(aVC2dummy);
				
											m_maphU2G1vs2G2InvM[1]->fill(m_aU2GList.m_aPi0TracksTable[i].m_dMass,m_aU2GList.m_aPi0TracksTable[k].m_dMass,1);		
    		}
  	}
		
		if (m_aUPi0List.m_aPi0TracksTable.size() > m_dMaxPi0Combs) {m_iUPi0Skip ++; return;}
		
  	m_aUPi0List.m_aValidCombs2.clear();
  	for (int i=0;i<m_aUPi0List.m_aPi0TracksTable.size();i++) {
    	for (int k=i+1;k<m_aUPi0List.m_aPi0TracksTable.size();k++) {
      	// Exclude 2 gammas combinations that shares same photons
      	if (m_aUPi0List.m_aPi0TracksTable[k].m_pTrack1 == m_aUPi0List.m_aPi0TracksTable[i].m_pTrack1 ||
	 				  m_aUPi0List.m_aPi0TracksTable[k].m_pTrack1 == m_aUPi0List.m_aPi0TracksTable[i].m_pTrack2 ||
	  				m_aUPi0List.m_aPi0TracksTable[k].m_pTrack2 == m_aUPi0List.m_aPi0TracksTable[i].m_pTrack1 ||
	  				m_aUPi0List.m_aPi0TracksTable[k].m_pTrack2 == m_aUPi0List.m_aPi0TracksTable[i].m_pTrack2
	  				) 
				{m_NsharedGam++;continue;}
      	aVC2dummy.Set(&(m_aUPi0List.m_aPi0TracksTable[i]),&(m_aUPi0List.m_aPi0TracksTable[k]));
      	m_aUPi0List.m_aValidCombs2.push_back(aVC2dummy);
				
				m_maphU2G1vs2G2InvM[3]->fill(m_aUPi0List.m_aPi0TracksTable[i].m_dMass,m_aUPi0List.m_aPi0TracksTable[k].m_dMass,1);		
    		}
  	}
		
	 	//------ End of loops over pions candidates ------//
		
		m_maphUNumEvt[2]->fill(0,1.);
		m_maphUSumEemc[2]->fill(m_dUSumEemc,1.);
		m_maphUValid2Combs[2]->fill(m_aUPi0List.m_aValidCombs2.size(),1.);
		m_hU2CombswSharedGammas->fill(m_NsharedGam,1);
    if (m_aUPi0List.m_aValidCombs2.size() < 1) {
		m_dUTotalChi2 = -5; 
		m_dUPi01Chi2 = -5; 
		m_dUPi02Chi2 = -5;
		m_iUPi0Skip ++; 
		return;} // insufficient number of 2pi0 candidates
		m_iUPi0kept++;
		
    // record combinatorics
    m_iUAnzPi0Tracks  = m_aUPi0List.m_aPi0TracksTable.size();
    m_iUAnz2Combs     = m_aUPi0List.m_aValidCombs2.size();
		
		m_maphUNumEvt[3]->fill(0,1.);
		m_maphUSumEemc[3]->fill(m_dUSumEemc,1.);
		m_maphUValid2Combs[3]->fill(m_aUPi0List.m_aValidCombs2.size(),1.);
		//------ END of Find 2 pi0 candidates------//
		
		//------ Untagged 2pi0 selection ------//
		//Find good 2 photons combinations
		//bool Iselpos2pi0Untagged = Findelpos2pi0Untagged();
			 
		if(m_bKalFit){	 
    bool bDone = false;
		//------ With Kinematic FIT ------//
		// try untagged fit
		int Nfits=0;
    for(int k=0;k<m_aUPi0List.m_aValidCombs2.size();k++) {	
									
					cPi0Tracks* pt2G1 = (m_aUPi0List.m_aValidCombs2[k]).m_pTrack1;
					HepLorentzVector lv2G1G1= m_aUPi0List.Get4VecFromEMC(pt2G1->m_pTrack1,0.0);
					HepLorentzVector lv2G1G2= m_aUPi0List.Get4VecFromEMC(pt2G1->m_pTrack2,0.0);
					HepLorentzVector lv2G1 = lv2G1G1+lv2G1G2;
					cPi0Tracks* pt2G2 = (m_aUPi0List.m_aValidCombs2[k]).m_pTrack2;
					HepLorentzVector lv2G2G1= m_aUPi0List.Get4VecFromEMC(pt2G2->m_pTrack1,0.0);
					HepLorentzVector lv2G2G2= m_aUPi0List.Get4VecFromEMC(pt2G2->m_pTrack2,0.0);
					HepLorentzVector lv2G2 = lv2G2G1+lv2G2G2;

					//Kinematics in e+e- CM frame	
					HepLorentzVector lv2G1CM=lv2G1;
					lv2G1CM.boost(-m_vectboostCM);
					HepLorentzVector lv2G2CM=lv2G2;
					lv2G2CM.boost(-m_vectboostCM);
					m_dUSumPtCM=GetPt(lv2G1CM+lv2G2CM);
					
					//Helicity Angle
					Hep3Vector boost2x2G=(lv2G1+lv2G2).boostVector();
					HepLorentzVector lv2G1G12GCM=lv2G1G1;
					lv2G1G12GCM.boost(boost2x2G);
					m_dCosTheta2G1HelicityG1=(lv2G1G12GCM.vect()).cosTheta(lv2G1.vect());
					
					HepLorentzVector lv2G2G12GCM=lv2G2G1;
					lv2G2G12GCM.boost(boost2x2G);
					m_dCosTheta2G2HelicityG1=(lv2G2G12GCM.vect()).cosTheta(lv2G2.vect());
					m_maphU2G1InvM[3]->fill(lv2G1.m(),1);
					m_maphU2G2InvM[3]->fill(lv2G2.m(),1);
					m_maphUEAsym2G1[3]->fill(GetEAsym2Gam(lv2G1G1,lv2G1G2),1);
					m_maphUEAsym2G2[3]->fill(GetEAsym2Gam(lv2G2G1,lv2G2G2),1);
					m_maphU2x2GCThetaMiss[3]->fill(m_dCosThetaMissLepton,1);
					m_maphU2x2GCTheta2G1HelicityG1[3]->fill(m_dCosTheta2G1HelicityG1,1);
					m_maphU2x2GCTheta2G2HelicityG1[3]->fill(m_dCosTheta2G2HelicityG1,1);
					m_maphU2x2GInvM[3]->fill((lv2G1+lv2G2).m(),1);
					m_maphU2x2GSumPt[3]->fill(m_dUSumPtCM,1);
			
				
			//Pi0 mass contrained fit of the decay photons
			m_KalKinFit->init();
    	//m_KalKinFit->setChisqCut(m_dChisqMaxValue);
			cPi0Tracks* ptPi01 = m_aUPi0List.m_aValidCombs2[k].m_pTrack1;
				cPi0Tracks* ptPi02 = m_aUPi0List.m_aValidCombs2[k].m_pTrack2;
			m_KalKinFit->AddTrack(0,0.0,ptPi01->m_pTrack1->emcShower());
    	m_KalKinFit->AddTrack(1,0.0,ptPi01->m_pTrack2->emcShower());
      m_KalKinFit->AddTrack(2,0.0,ptPi02->m_pTrack1->emcShower());
      m_KalKinFit->AddTrack(3,0.0,ptPi02->m_pTrack2->emcShower());
      
			//Add pi0 mass constraint
			m_KalKinFit->AddResonance(0,m_dMassPi0,0,1);
			m_KalKinFit->AddResonance(1,m_dMassPi0,2,3);
			
			/*
			m_KalKinFit->AddMissTrack(4,0.0,m_lvBoost -m_aUPi0List.Get4VecFromEMC(ptPi01->m_pTrack1,0.0)
                                               -m_aUPi0List.Get4VecFromEMC(ptPi01->m_pTrack2,0.0)
                                               -m_aUPi0List.Get4VecFromEMC(ptPi02->m_pTrack1,0.0)
                                               -m_aUPi0List.Get4VecFromEMC(ptPi02->m_pTrack2,0.0));
			
			//Add 4-momentum constraint
			m_KalKinFit->AddFourMomentum(0,m_lvBoost);
			
			//Add pi0 mass constraint
			m_KalKinFit->AddResonance(1,m_dMassPi0,0,1);
			m_KalKinFit->AddResonance(2,m_dMassPi0,2,3);
			*/
																									 
			//Perform the fit
			if (!m_KalKinFit->Fit()) {continue;}
			Nfits++;
			m_maphUKinFitChi2[3]->fill((double)m_KalKinFit->chisq(),1);
			//Keep information for the 2pi0 combination that has the best chi2 fit
      if (m_KalKinFit->chisq() > 0 && m_KalKinFit->chisq() < m_dUTotalChi2) {
				//if (m_KalKinFit->chisq() > 0 && m_KalKinFit->chisq() < m_dUTotalChi2 && m_KalKinFit->chisq()<m_dChisqMaxValue ) {    
				//Prepare to write 
				m_dUTotalChi2 = m_KalKinFit->chisq();
				// detector
    		m_lvPi01   = m_aUPi0List.Get4VecFromEMC(ptPi01->m_pTrack1,0.0) + m_aUPi0List.Get4VecFromEMC(ptPi01->m_pTrack2,0.0);
    		m_lvPi01G1 = m_aUPi0List.Get4VecFromEMC(ptPi01->m_pTrack1,0.0);
    		m_lvPi01G2 = m_aUPi0List.Get4VecFromEMC(ptPi01->m_pTrack2,0.0);
    		m_lvPi02   = m_aUPi0List.Get4VecFromEMC(ptPi02->m_pTrack1,0.0) + m_aUPi0List.Get4VecFromEMC(ptPi02->m_pTrack2,0.0);
    		m_lvPi02G1 = m_aUPi0List.Get4VecFromEMC(ptPi02->m_pTrack1,0.0);
    		m_lvPi02G2 = m_aUPi0List.Get4VecFromEMC(ptPi02->m_pTrack2,0.0);
    		m_aPhIndexList.clear();
   		  m_aPhIndexList.push_back (ptPi01->m_pTrack1);
    		m_aPhIndexList.push_back (ptPi01->m_pTrack2);
    		m_aPhIndexList.push_back (ptPi02->m_pTrack1);
    		m_aPhIndexList.push_back (ptPi02->m_pTrack2);
    		// fit
    		m_lvFitPi01G1 = m_KalKinFit->pfit(0);
    		m_lvFitPi01G2 = m_KalKinFit->pfit(1);
    		m_lvFitPi02G1 = m_KalKinFit->pfit(2);
    		m_lvFitPi02G2 = m_KalKinFit->pfit(3);
    		m_lvFitPi01   = m_KalKinFit->pfit(0) + m_KalKinFit->pfit(1);
    		m_lvFitPi02   = m_KalKinFit->pfit(2) + m_KalKinFit->pfit(3);
    		
    		// the lines below MUSST stay below the settings of the 4vectors!
				if (m_KalKinFit->Fit(0)) {m_dUPi01Chi2 = m_KalKinFit->chisq(0);} else {m_dUPi01Chi2 = -5;}
    		if (m_KalKinFit->Fit(1)) {m_dUPi02Chi2 = m_KalKinFit->chisq(1);} else {m_dUPi02Chi2 = -5;}
				
				m_maphUKinFitChi2[4]->fill((double)m_KalKinFit->chisq(),1);
        bDone = true;
      }
    }
		
		m_hU2CombswValidFit->fill(Nfits,1);

if (!bDone) {m_dUTotalChi2 = -5;m_dUPi01Chi2 = -5; m_dUPi02Chi2 = -5; return;}
m_iUFitKept++;
		
		}	
		else{
			//------ Without Kinematic FIT ------//
			if (m_aUPi0List.m_aValidCombs2.size() != 1) {return;}
			for(int k=0;k<m_aUPi0List.m_aValidCombs2.size();k++) {
			cPi0Tracks* ptPi01 = m_aUPi0List.m_aValidCombs2[k].m_pTrack1;
			cPi0Tracks* ptPi02 = m_aUPi0List.m_aValidCombs2[k].m_pTrack2;
			
    		// detector
    		m_lvPi01   = m_aUPi0List.Get4VecFromEMC(ptPi01->m_pTrack1,0.0) + m_aUPi0List.Get4VecFromEMC(ptPi01->m_pTrack2,0.0);
    		m_lvPi01G1 = m_aUPi0List.Get4VecFromEMC(ptPi01->m_pTrack1,0.0);
    		m_lvPi01G2 = m_aUPi0List.Get4VecFromEMC(ptPi01->m_pTrack2,0.0);
    		m_lvPi02   = m_aUPi0List.Get4VecFromEMC(ptPi02->m_pTrack1,0.0) + m_aUPi0List.Get4VecFromEMC(ptPi02->m_pTrack2,0.0);
    		m_lvPi02G1 = m_aUPi0List.Get4VecFromEMC(ptPi02->m_pTrack1,0.0);
    		m_lvPi02G2 = m_aUPi0List.Get4VecFromEMC(ptPi02->m_pTrack2,0.0);
    		m_aPhIndexList.clear();
   		  m_aPhIndexList.push_back (ptPi01->m_pTrack1);
    		m_aPhIndexList.push_back (ptPi01->m_pTrack2);
    		m_aPhIndexList.push_back (ptPi02->m_pTrack1);
    		m_aPhIndexList.push_back (ptPi02->m_pTrack2);		
    }
		}
			
//Fill particle containers
m_UPi01.Fill(&m_lvPi01);
m_UPi02.Fill(&m_lvPi02);
m_UGammas[0].Fill(&m_lvPi01G1);
m_UGammas[1].Fill(&m_lvPi01G2);
m_UGammas[2].Fill(&m_lvPi02G1);
m_UGammas[3].Fill(&m_lvPi02G2);
m_UFitPi01.Fill(&m_lvFitPi01);
m_UFitPi02.Fill(&m_lvFitPi02);
m_UFitGammas[0].Fill(&m_lvFitPi01G1);
m_UFitGammas[1].Fill(&m_lvFitPi01G2);
m_UFitGammas[2].Fill(&m_lvFitPi02G1);
m_UFitGammas[3].Fill(&m_lvFitPi02G2);

//Invariant Mass
m_dUPi01InvM=m_lvPi01.m();
m_dUPi02InvM=m_lvPi02.m();
m_dU2pi0InvM=(m_lvPi01+m_lvPi02).m();

m_dUFitPi01InvM=m_lvFitPi01.m();
m_dUFitPi02InvM=m_lvFitPi02.m();
m_dUFit2pi0InvM=(m_lvFitPi01+m_lvFitPi02).m();

//Energy Asymmetry
m_dUEAsymPi01=GetEAsym2Gam(m_lvPi01G1,m_lvPi01G2);
m_dUEAsymPi02=GetEAsym2Gam(m_lvPi02G1,m_lvPi02G2);
m_dUFitEAsymPi01=GetEAsym2Gam(m_lvFitPi01G1,m_lvFitPi01G2);
m_dUFitEAsymPi02=GetEAsym2Gam(m_lvFitPi02G1,m_lvFitPi02G2);

//Kinematics in e+e- CM frame	
m_lvPi01CM=m_lvPi01;
m_lvPi01CM.boost(-m_vectboostCM);
m_lvPi02CM=m_lvPi02;
m_lvPi02CM.boost(-m_vectboostCM);

//Tranverse momentum
//m_dUSumPtCM=(m_lvPi01CM+m_lvPi02CM).perp();
m_dUSumPtCM=GetPt(m_lvPi01CM+m_lvPi02CM);

//vector<HepLorentzVector> vectSumPt;
//vectSumPt.push_back(m_lvPi01CM);
//vectSumPt.push_back(m_lvPi01CM);
//m_dUSumPtCM=GetSumPt(vectSumPt);

m_lvFitPi01CM=m_lvFitPi01;
m_lvFitPi01CM.boost(-m_vectboostCM);
m_lvFitPi02CM=m_lvFitPi02;
m_lvFitPi02CM.boost(-m_vectboostCM);
m_dUFitSumPtCM=GetPt(m_lvFitPi01CM+m_lvFitPi02CM);

//Energy momentum conservation test
m_dUPbeamelecpositron=(m_lvBeamElectron+m_lvBeamPositron).rho();
m_dUPCMbeamelecpositron=(m_lvBeamElectronCM+m_lvBeamPositronCM).rho();

m_dUPi01CMPx=(m_lvPi01CM.vect()).x();
m_dUPi01CMPy=(m_lvPi01CM.vect()).y();
m_dUPi01CMPz=(m_lvPi01CM.vect()).z();
//m_dUPi01CMPt=(m_lvPi01CM.vect()).perp();
m_dUPi01CMPt=GetPt(m_lvPi01CM);
m_dUPi02CMPx=(m_lvPi02CM.vect()).x();
m_dUPi02CMPy=(m_lvPi02CM.vect()).y();
m_dUPi02CMPz=(m_lvPi02CM.vect()).z();
//m_dUPi02CMPt=(m_lvPi02CM.vect()).perp();
m_dUPi02CMPt=GetPt(m_lvPi02CM);

m_UPi01CM.Fill(&m_lvPi01CM);
m_UPi02CM.Fill(&m_lvPi02CM);

//Kinematics in 2 Gammas CM frame	
m_boostFit2pi0=(m_lvFitPi01+m_lvFitPi02).boostVector();
m_lvFitPi01CM2Gam=m_lvFitPi01;
m_lvFitPi01CM2Gam.boost(-m_boostFit2pi0);

//Cos theta
//Z axis for Gamma-Gamma CM frame : Z direction from e+ beam in e+e- rest frame
Hep3Vector ZAxisGam2pi0CM = (m_lvBeamPositronCM.vect()).unit();
double ThetaPi01CM2Gam=m_lvFitPi01CM2Gam.angle(ZAxisGam2pi0CM);
m_dUFitcosThetaPi01CM2Gam=cos(ThetaPi01CM2Gam);


m_maphUNumEvt[4]->fill(0,1.);
m_maphUSumEemc[4]->fill(m_dUSumEemc,1.);
m_maphUValid2Combs[4]->fill(m_aUPi0List.m_aValidCombs2.size(),1.);
m_maphUPi01InvM[4]->fill(m_dUPi01InvM,1.);
m_maphUPi02InvM[4]->fill(m_dUPi02InvM,1.);
m_maphUFitPi01InvM[4]->fill(m_dUFitPi01InvM,1.);
m_maphUFitPi02InvM[4]->fill(m_dUFitPi02InvM,1.);
m_maphU2Pi0InvM[4]->fill(m_dU2pi0InvM,1.);
m_maphUFit2Pi0InvM[4]->fill(m_dUFit2pi0InvM,1.);
m_maphUFit2Pi0InvMvs2Pi0InvM[4]->fill(m_dU2pi0InvM,m_dUFit2pi0InvM,1.);
m_maphUFitEAsymPi01[4]->fill(m_dUFitEAsymPi01);
m_maphUFitEAsymPi02[4]->fill(m_dUFitEAsymPi02);
m_maphUSumPt[4]->fill(m_dUSumPtCM,1.);
m_maphUFitSumPt[4]->fill(m_dUFitSumPtCM,1.);
m_maphUFitcosThetaPi01CM2Gam[4]->fill(m_dUFitcosThetaPi01CM2Gam,1.);

bool bStage5OK=m_dUTotalChi2<m_dChisqMaxValue;
bool bStage6OK=m_dUSumEemc<m_dUSumEemcMax;
bool bStage7OK=m_dUFitSumPtCM<= m_dUSumPtMax;

if(bStage5OK && bStage6OK && bStage7OK) m_iIsU2pi0=1;

if(!bStage5OK){return;}
m_maphUNumEvt[5]->fill(0,1.);
m_maphUSumEemc[5]->fill(m_dUSumEemc,1.);
m_maphUValid2Combs[5]->fill(m_aUPi0List.m_aValidCombs2.size(),1.);
m_maphUPi01InvM[5]->fill(m_dUPi01InvM,1.);
m_maphUPi02InvM[5]->fill(m_dUPi02InvM,1.);
m_maphUFitPi01InvM[5]->fill(m_dUFitPi01InvM,1.);
m_maphUFitPi02InvM[5]->fill(m_dUFitPi02InvM,1.);
m_maphU2Pi0InvM[5]->fill(m_dU2pi0InvM,1.);
m_maphUFit2Pi0InvM[5]->fill(m_dUFit2pi0InvM,1.);
m_maphUFit2Pi0InvMvs2Pi0InvM[5]->fill(m_dU2pi0InvM,m_dUFit2pi0InvM,1.);
m_maphUFitEAsymPi01[5]->fill(m_dUFitEAsymPi01);
m_maphUFitEAsymPi02[5]->fill(m_dUFitEAsymPi02);
m_maphUSumPt[5]->fill(m_dUSumPtCM,1.);
m_maphUFitSumPt[5]->fill(m_dUFitSumPtCM,1.);
m_maphUFitcosThetaPi01CM2Gam[5]->fill(m_dUFitcosThetaPi01CM2Gam,1.);

if(!bStage6OK){return;}
m_maphUNumEvt[6]->fill(0,1.);
m_maphUSumEemc[6]->fill(m_dUSumEemc,1.);
m_maphUValid2Combs[6]->fill(m_aUPi0List.m_aValidCombs2.size(),1.);
m_maphUPi01InvM[6]->fill(m_dUPi01InvM,1.);
m_maphUPi02InvM[6]->fill(m_dUPi02InvM,1.);
m_maphUFitPi01InvM[6]->fill(m_dUFitPi01InvM,1.);
m_maphUFitPi02InvM[6]->fill(m_dUFitPi02InvM,1.);
m_maphU2Pi0InvM[6]->fill(m_dU2pi0InvM,1.);
m_maphUFit2Pi0InvM[6]->fill(m_dUFit2pi0InvM,1.);
m_maphUFit2Pi0InvMvs2Pi0InvM[6]->fill(m_dU2pi0InvM,m_dUFit2pi0InvM,1.);
m_maphUFitEAsymPi01[6]->fill(m_dUFitEAsymPi01);
m_maphUFitEAsymPi02[6]->fill(m_dUFitEAsymPi02);
m_maphUSumPt[6]->fill(m_dUSumPtCM,1.);
m_maphUFitSumPt[6]->fill(m_dUFitSumPtCM,1.);
m_maphUFitcosThetaPi01CM2Gam[6]->fill(m_dUFitcosThetaPi01CM2Gam,1.);

if(!bStage7OK){return;}
m_maphUNumEvt[7]->fill(0,1.);
m_maphUSumEemc[7]->fill(m_dUSumEemc,1.);
m_maphUValid2Combs[7]->fill(m_aUPi0List.m_aValidCombs2.size(),1.);
m_maphUPi01InvM[7]->fill(m_dUPi01InvM,1.);
m_maphUPi02InvM[7]->fill(m_dUPi02InvM,1.);
m_maphUFitPi01InvM[7]->fill(m_dUFitPi01InvM,1.);
m_maphUFitPi02InvM[7]->fill(m_dUFitPi02InvM,1.);
m_maphU2Pi0InvM[7]->fill(m_dU2pi0InvM,1.);
m_maphUFit2Pi0InvM[7]->fill(m_dUFit2pi0InvM,1.);
m_maphUFit2Pi0InvMvs2Pi0InvM[7]->fill(m_dU2pi0InvM,m_dUFit2pi0InvM,1.);
m_maphUFitEAsymPi01[7]->fill(m_dUFitEAsymPi01);
m_maphUFitEAsymPi02[7]->fill(m_dUFitEAsymPi02);
m_maphUSumPt[7]->fill(m_dUSumPtCM,1.);
m_maphUFitSumPt[7]->fill(m_dUFitSumPtCM,1.);
m_maphUFitcosThetaPi01CM2Gam[7]->fill(m_dUFitcosThetaPi01CM2Gam,1.);

m_NUntagged->write();

//------ END of Untagged 2pi0 selection ------//

/*
if(Iselpos2pi0Untagged){
	log << MSG::WARNING << "****** FILL! " << m_iAll << " ******" << endreq;
	m_NUntagged->write();
	m_iAcceptedU ++;
	}
*/

//log << MSG::WARNING << "****** FILL UNTAGGED " << m_iAll << " ******" << endreq;
//log << MSG::WARNING << m_dUSumPtCM << endreq;

m_iAcceptedU ++;

return;
}
//---------------------------------------------------------------------------------------------------------- 

//---------------------------------------------------------------------------------------------------------- 
void Bes2Gamto2Pi0::DoeTagged()
{

MsgStream log(msgSvc(), name());
    log << MSG::INFO << "in execute()" << endreq;


		
SmartDataPtr<Event::EventHeader> eventHeader(eventSvc(),"/Event/EventHeader");
SmartDataPtr<EvtRecEvent> evtRecEvent(eventSvc(), "/Event/EvtRec/EvtRecEvent");
    if ( ! evtRecEvent ) {
        log << MSG::FATAL << "Could not find EvtRecEvent" << endreq;
        return;
    }
    SmartDataPtr<EvtRecTrackCol> evtRecTrackCol(eventSvc(), "/Event/EvtRec/EvtRecTrackCol");
    if ( ! evtRecTrackCol ) {
        log << MSG::FATAL << "Could not find EvtRecTrackCol" << endreq;
        return;
    }
    Gaudi::svcLocator()->service("VertexDbSvc", m_vtxSvc);


m_deT2GRunNumber   = m_dRunNumber;
m_deT2GEvtNumber  = m_dEvtNumber;
m_deT2GCMSEnergy = m_dCMSEnergy;
m_ieT2GN2Gs=0;

m_deT2x2GRunNumber   = m_dRunNumber;
m_deT2x2GEvtNumber  = m_dEvtNumber;
m_deT2x2GCMSEnergy = m_dCMSEnergy;
m_ieT2x2GN2x2G=0;


m_deTRunNumber   = m_dRunNumber;
m_deTEvtNumber  = m_dEvtNumber;
m_deTCMSEnergy = m_dCMSEnergy;	

//IP pos
m_deTIPposX  = m_hp3IP.x();
m_deTIPposY  = m_hp3IP.y();
m_deTIPposZ  = m_hp3IP.z();


   
m_ieTmcTrueFSR           = m_iMCfsr;
m_ieTmcTruePi0           = m_iMCpi0;
m_ieTmcTrueGammaFromJpsi = m_iMCGammaFromJpsi;
m_ieTmcTrueOtherGamma    = m_iMCOtherGamma;
m_ieTmcTrueOther         = m_iMCOther;

		
// signal untagged
int NElectrons=0;
int idxElectron=-1;
bool Has1Electron=false;
m_ieTAnzGoodP        = -1;
m_ieTAnzGoodM        = -1;
m_ieTAnzGoodGamma    = -1;
m_ieTAnzPi0Tracks    = -1;
m_ieTAnz2Combs       = -1;
m_ieTAnz3Combs       = -1;
m_deTTotalChi2       = m_dChisqMaxValue;
m_deTPi01Chi2        = m_dChisqMaxValue;
m_deTPi02Chi2        = m_dChisqMaxValue;
m_ieTQTag						= -99;
m_deTSumEemc				=0;
m_iIseT2pi0					= 0;
m_NsharedGam				=0;

m_eTLepton.Clear();		
m_eTPi01.Clear();
m_eTPi02.Clear();	
//for (int a=0;a<GAMMAS+4;a++) {m_UGammas[a].Clear(); m_dUClosestTrack[a] = -4.0;}
for (int a=0;a<GAMMAS+4;a++) {m_eTGammas[a].Clear();}

m_eTFitLepton.Clear();			
m_eTFitPi01.Clear();
m_eTFitPi02.Clear();
for (int a=0;a<5;a++) {m_eTFitGammas[a].Clear();}

m_deTVtxChi2 = -1;
m_deTVtxPosX = -1;
m_deTVtxPosY = -1;
m_deTVtxPosZ = -20;


m_aeT2GList.SetIterators (m_itBegin,m_itEndCharged,m_itEndNeutral);
m_aeT2GList.Init(m_dPi0MassCutLow,m_dPi0MassCutHigh,m_dPhEnergyCutBarrel,m_dPhEnergyCutEndCap,m_dCosThetaBarrel,m_dMaxTime,m_dCMSEnergy/2.0);

m_aeTPi0List.SetIterators (m_itBegin,m_itEndCharged,m_itEndNeutral);		
m_aeTPi0List.Init(m_dPi0MassCutLow,m_dPi0MassCutHigh,m_dPhEnergyCutBarrel,m_dPhEnergyCutEndCap,m_dCosThetaBarrel,m_dMaxTime,m_dCMSEnergy/2.0);
 
EvtRecTrackIterator  itERT;
    double       dDist, dMinDistP = 100, dMinDistN = 100;
    double       dEoP,dEoPP,dEoPN;

bool bStage0OK;
//Electron Identification
for (itERT  = m_itBegin;itERT != m_itEndCharged;itERT ++) {
		if (!(*itERT)->isMdcKalTrackValid()) {continue;}
		double Dist = IPdist (*itERT);
    HepPoint3D   point0(0,0,0);
    VFHelix      helixipP(point0,(*itERT)->mdcKalTrack()->helix(),(*itERT)->mdcKalTrack()->err());
    helixipP.pivot(m_hp3IP);
		double CylRad=fabs(helixipP.a()[0]);
		double CylHeight=fabs(helixipP.a()[3]);
		
		double P=(*itERT)->mdcKalTrack()->p();
		double Q=(*itERT)->mdcKalTrack()->charge();
		
		m_heTDist->fill(Dist,1.);
		m_heTCylRad->fill(CylRad,1.);
		m_heTCylHeight->fill(CylHeight,1.);
		m_heTPChargedTracks->fill((*itERT)->mdcKalTrack()->p());
		
		m_ieTValidMDCKept++;
		//Skip if track origin is not in the virtual box
		bool FromInteractionPoint=fabs(helixipP.a()[0]) < m_dCylRad && fabs(helixipP.a()[3]) < m_dCylHeight;
		if(!FromInteractionPoint) continue;
		
		m_ieTValidMDCIPKept++;
		//Electron identification
		if (!(*itERT)->isEmcShowerValid()) continue;
		double eEMCTot=(*itERT)->emcShower()->energy() ;
		
		
		bStage0OK= FromInteractionPoint && Q<0;
			
		if(bStage0OK)
			{
			m_mapheTNumEvt[0]->fill(0.,1);
			m_mapheTEtotOvrPvsP[0]->fill(P,eEMCTot/P);
			m_mapheTNCharged[0]->fill(m_iNCharged,1.);
			m_mapheTNNeutral[0]->fill(m_iNNeutral,1.);
			m_ieTGoodNegKept++;
			}
			
		bool IsElectron=Q<0 && eEMCTot/P>0.8;
		if(IsElectron)
			{
			m_ieTQTag=Q;
			m_pERTLepton=*itERT;
			HepVector w_zHel(5,0);
			w_zHel = m_pERTLepton->mdcKalTrack()->getZHelix();
    	WTrackParameter wtpElec (m_dMasselec,w_zHel,m_pERTLepton->mdcKalTrack()->getZError());
			m_lvLepton = wtpElec.p();
			NElectrons++;
			}
		
		Has1Electron=IsElectron && NElectrons==1;
		//if(Has1Electron==true) m_lvLepton;
 }
 
if(Has1Electron==false) return;
m_ieT1Lepton++;
m_mapheTNumEvt[1]->fill(0.,1);
m_mapheTNCharged[1]->fill(m_iNCharged,1.);
m_mapheTNNeutral[1]->fill(m_iNNeutral,1.);

//Find good photons
    int iGood = 0;
    for (itERT = m_itEndCharged;itERT != m_itEndNeutral;itERT ++) {
				int index = std::distance(m_itEndCharged,itERT);
				if ((*itERT)->isEmcShowerValid()) {m_deTSumEemc+=(*itERT)->emcShower()->energy();}			
				// count good photons
        if (m_aeTPi0List.IsPhoton(*itERT)) {
				iGood ++;}
				
    }
		

    if (iGood < 4 || iGood > m_dMaxGoodPh) {return;}
		m_ieTPhotonKept++;
    m_ieTAnzGoodGamma = iGood;
		
		m_mapheTNumEvt[2]->fill(0.,1);
		m_mapheTSumEemc[2]->fill(m_deTSumEemc,1.);
		
		//Add good photons to the pi0 candidates list
		double m_dBeamEnergy=m_dCMSEnergy/2.0;
		EvtRecTrackIterator    itERT1;
		EvtRecTrackIterator    itERT2;
		int index2pair1=0;
		int index2pair2=0;
		for (itERT1 = m_itEndCharged;itERT1 !=m_itEndNeutral;itERT1 ++) {
		int index1 = std::distance(m_itEndCharged,itERT1);
    if (   !m_aeTPi0List.IsPhoton(*itERT1)
		|| (*itERT1)->emcShower()->energy() > m_dBeamEnergy
       ) 
			 {continue;}
    	for (itERT2 = itERT1+1;itERT2 != m_itEndNeutral;itERT2 ++) {
      	//y ++;     
      	if (    !m_aeTPi0List.IsPhoton(*itERT2)
	   		|| (*itERT2)->emcShower()->energy() > m_dBeamEnergy
	 			) 
				{continue;}	
				
			index2pair1++;
						
			HepLorentzVector lvG1(0,0,0,0);
			lvG1.setRThetaPhi((*itERT1)->emcShower()->energy(),
		      (*itERT1)->emcShower()->theta(),
		      (*itERT1)->emcShower()->phi());
			lvG1.setE((*itERT1)->emcShower()->energy());			
			HepLorentzVector lvG2(0,0,0,0);
			lvG2.setRThetaPhi((*itERT2)->emcShower()->energy(),
		      (*itERT2)->emcShower()->theta(),
		      (*itERT2)->emcShower()->phi());	
			lvG2.setE((*itERT2)->emcShower()->energy());	
			HepLorentzVector lv2G=lvG1+lvG2;
			Hep3Vector vectboost2G= lv2G.boostVector();
			HepLorentzVector lvG12GCM=lvG1;
			lvG12GCM.boost(-vectboost2G);
			
			HepLorentzVector lvMissLept=m_lvBeamElectron+m_lvBeamPositron-(m_lvLepton+lvG1+lvG2);
			
			m_dCosThetaMissLepton	= lvMissLept.cosTheta();			//m_dCosTheta2GHelicityG1=((lvG12GCM.vect()).dot(lv2G.vect()))/((lvG12GCM.vect()).mag()*(lv2G.vect()).mag());
			m_dCosTheta2GHelicityG1=(lvG12GCM.vect()).cosTheta(lv2G.vect());
			m_d2GOpeningAngle=(lvG1.vect()).angle(lvG2.vect());
			//m_d2GDeltaTheta=(lvG1.vect()).polarAngle(lvG2.vect());
			m_d2GDeltaTheta=(lvG1.theta())-(lvG2.theta());
			m_d2GDeltaPhi=(lvG1.vect()).deltaPhi(lvG2.vect());		
			//Fill NTuple
			m_deT2GEG1[m_ieT2GN2Gs]=lvG1.e();
			m_deT2GEG2[m_ieT2GN2Gs]=lvG2.e();
			m_deT2GEAsymmetry[m_ieT2GN2Gs]=(m_deT2GEG1[m_ieT2GN2Gs]-m_deT2GEG2[m_ieT2GN2Gs])/(m_deT2GEG1[m_ieT2GN2Gs]+m_deT2GEG2[m_ieT2GN2Gs]);
			m_deT2GMass2G[m_ieT2GN2Gs]=(lvG1+lvG2).mag();
			m_deT2GCThetaMiss[m_ieT2GN2Gs]=m_dCosThetaMissLepton;	
			m_deT2GCThetaHelicity[m_ieT2GN2Gs]=m_dCosTheta2GHelicityG1;
			m_deT2GOpeningAngle[m_ieT2GN2Gs]=m_d2GOpeningAngle;
			m_deT2GDeltaTheta[m_ieT2GN2Gs]=m_d2GDeltaTheta;
			m_deT2GDeltaPhi[m_ieT2GN2Gs]=m_d2GDeltaPhi;					
			m_ieT2GN2Gs++;
			
			//Fill Histograms
			m_mapheT2GCThetaMiss[2]->fill(m_dCosThetaMissLepton);
			m_mapheT2GCThetaHelicity[2]->fill(m_dCosTheta2GHelicityG1);
			m_mapheT2GOpeningAngle[2]->fill(m_d2GOpeningAngle);
			m_mapheT2GDeltaTheta[2]->fill(m_d2GDeltaTheta);
			m_mapheT2GDeltaPhi[2]->fill(m_d2GDeltaPhi);
			
			
			m_mapheT2GamvsEn2Gam[2]->fill((*itERT1)->emcShower()->energy()+(*itERT2)->emcShower()->energy(),index2pair1,1);
			
      double d2GMass = m_aeTPi0List.GetM(*itERT1,0.0,*itERT2,0.0);
			m_mapheT2GammasInvM[2]->fill(d2GMass,1);
			m_aeT2GList.Add(*itERT1,*itERT2,d2GMass);			
      if (d2GMass > m_dPi0MassCutHigh || d2GMass < m_dPi0MassCutLow) {continue;}
      m_aeTPi0List.Add(*itERT1,*itERT2,d2GMass);		
			index2pair2++;
			m_mapheT2GamvsEn2Gam[3]->fill((*itERT1)->emcShower()->energy()+(*itERT2)->emcShower()->energy(),index2pair1,1);
			m_mapheT2GammasInvM[3]->fill(d2GMass,1);
			
     }
  	}

if(m_bWriteNt2G==true) m_NeT2G->write();

//Loops over 2x(2gamma combination)
ValidCombs2 aVC2dummy;
m_aeT2GList.m_aValidCombs2.clear();
//m_ieT2x2GNSizePi0TrackTable=m_aeT2GList.m_aPi0TracksTable.size();
for (int i=0;i<m_aeT2GList.m_aPi0TracksTable.size();i++) {
    	for (int k=i+1;k<m_aeT2GList.m_aPi0TracksTable.size();k++) {
      	// Exclude 2 gammas combinations that shares same photons
      	if (m_aeT2GList.m_aPi0TracksTable[k].m_pTrack1 == m_aeT2GList.m_aPi0TracksTable[i].m_pTrack1 ||
	 				  m_aeT2GList.m_aPi0TracksTable[k].m_pTrack1 == m_aeT2GList.m_aPi0TracksTable[i].m_pTrack2 ||
	  				m_aeT2GList.m_aPi0TracksTable[k].m_pTrack2 == m_aeT2GList.m_aPi0TracksTable[i].m_pTrack1 ||
	  				m_aeT2GList.m_aPi0TracksTable[k].m_pTrack2 == m_aeT2GList.m_aPi0TracksTable[i].m_pTrack2
	  				)
					{continue;}
					
					aVC2dummy.Set(&(m_aeT2GList.m_aPi0TracksTable[i]),&(m_aeT2GList.m_aPi0TracksTable[k]));
      		m_aeT2GList.m_aValidCombs2.push_back(aVC2dummy);
					/*
					cPi0Tracks* pt2G1 = (m_aeT2GList.m_aValidCombs2).back().m_pTrack1;
					HepLorentzVector lv2G1G1= m_aeT2GList.Get4VecFromEMC(pt2G1->m_pTrack1,0.0);
					HepLorentzVector lv2G1G2= m_aeT2GList.Get4VecFromEMC(pt2G1->m_pTrack2,0.0);
					HepLorentzVector lv2G1 = lv2G1G1+lv2G1G2;
					cPi0Tracks* pt2G2 = (m_aeT2GList.m_aValidCombs2).back().m_pTrack2;
					HepLorentzVector lv2G2G1= m_aeT2GList.Get4VecFromEMC(pt2G2->m_pTrack1,0.0);
					HepLorentzVector lv2G2G2= m_aeT2GList.Get4VecFromEMC(pt2G2->m_pTrack2,0.0);
					HepLorentzVector lv2G2 = lv2G2G1+lv2G2G2;
					
					HepLorentzVector lvMissLept=m_lvBeamElectron+m_lvBeamPositron-(m_lvLepton+lv2G1+lv2G2);
					m_dCosThetaMissLepton	= lvMissLept.cosTheta();
					
					//Kinematics in e+e- CM frame	
					HepLorentzVector lvLeptonCM=m_lvLepton;
					lvLeptonCM.boost(-m_vectboostCM);
					HepLorentzVector lv2G1CM=lv2G1;
					lv2G1CM.boost(-m_vectboostCM);
					HepLorentzVector lv2G2CM=lv2G2;
					lv2G2CM.boost(-m_vectboostCM);
					m_deTSumPtCM=GetPt(lvLeptonCM+lv2G1CM+lv2G2CM);
					
					//Helicity Angle
					Hep3Vector boost2x2G=(lv2G1+lv2G2).boostVector();
					HepLorentzVector lv2G1G12GCM=lv2G1G1;
					lv2G1G12GCM.boost(boost2x2G);
					m_dCosTheta2G1HelicityG1=(lv2G1G12GCM.vect()).cosTheta(lv2G1.vect());
					
					HepLorentzVector lv2G2G12GCM=lv2G2G1;
					lv2G2G12GCM.boost(boost2x2G);
					m_dCosTheta2G2HelicityG1=(lv2G2G12GCM.vect()).cosTheta(lv2G2.vect());
					
					//RGamma
					double ELept2x2GCM;
					double AbsMomLept2x2GCM;

					ELept2x2GCM=(lvLeptonCM+lv2G1CM+lv2G2CM).e();
					AbsMomLept2x2GCM=((lvLeptonCM+lv2G1CM+lv2G2CM).vect()).rho();
					m_dRGam=(m_dCMSEnergy-ELept2x2GCM-AbsMomLept2x2GCM)/m_dCMSEnergy;
					//log << MSG::WARNING << "m_ieT2x2GN2x2G " << m_ieT2x2GN2x2G<< " " << endreq;
					m_deT2x2GE2G1[m_ieT2x2GN2x2G]=lv2G1.e();
					m_deT2x2GE2G2[m_ieT2x2GN2x2G]=lv2G2.e();	
					m_deT2x2GInvM2G1[m_ieT2x2GN2x2G]=lv2G1.m();					
					m_deT2x2GInvM2G2[m_ieT2x2GN2x2G]=lv2G2.m();	
					m_deT2x2GEAsym2G1[m_ieT2x2GN2x2G] =	GetEAsym2Gam(lv2G1G1,lv2G1G2);
					m_deT2x2GEAsym2G2[m_ieT2x2GN2x2G] =	GetEAsym2Gam(lv2G2G1,lv2G2G2);
					m_deT2x2GCThetaMiss[m_ieT2x2GN2x2G]=m_dCosThetaMissLepton;
					m_deT2x2GInvM2x2G[m_ieT2x2GN2x2G]=(lv2G1+lv2G2).m();
					m_deT2x2GSumPtCM[m_ieT2x2GN2x2G]=m_deTSumPtCM;
					m_deT2x2GCTheta2G1HelicityG1[m_ieT2x2GN2x2G]=m_dCosTheta2G1HelicityG1;
					m_deT2x2GCTheta2G2HelicityG1[m_ieT2x2GN2x2G]=m_dCosTheta2G2HelicityG1;
					m_deT2x2GRGam[m_ieT2x2GN2x2G]=m_dRGam;
					
					m_ieT2x2GN2x2G++;
					*/
					m_mapheT2G1vs2G2InvM[2]->fill(m_aeT2GList.m_aPi0TracksTable[i].m_dMass,m_aeT2GList.m_aPi0TracksTable[k].m_dMass,1);
					}
				}	
				
		

    if (m_aeTPi0List.m_aPi0TracksTable.size() > m_dMaxPi0Combs) {return;}
		
		//if(m_bWriteNt2x2G==true) m_NeT2x2G->write();
				
	  m_mapheTNumEvt[3]->fill(0.,1);

//Loops over pions candidates
		//Find good 2 gamma combinations
		//ValidCombs2 aVC2dummy;
  	m_aeTPi0List.m_aValidCombs2.clear();
			
  	for (int i=0;i<m_aeTPi0List.m_aPi0TracksTable.size();i++) {
    	for (int k=i+1;k<m_aeTPi0List.m_aPi0TracksTable.size();k++) {
      	// Exclude 2 gammas combinations that shares same photons
      	if (m_aeTPi0List.m_aPi0TracksTable[k].m_pTrack1 == m_aeTPi0List.m_aPi0TracksTable[i].m_pTrack1 ||
	 				  m_aeTPi0List.m_aPi0TracksTable[k].m_pTrack1 == m_aeTPi0List.m_aPi0TracksTable[i].m_pTrack2 ||
	  				m_aeTPi0List.m_aPi0TracksTable[k].m_pTrack2 == m_aeTPi0List.m_aPi0TracksTable[i].m_pTrack1 ||
	  				m_aeTPi0List.m_aPi0TracksTable[k].m_pTrack2 == m_aeTPi0List.m_aPi0TracksTable[i].m_pTrack2
	  				) 
				{m_NsharedGam++;continue;}
      	aVC2dummy.Set(&(m_aeTPi0List.m_aPi0TracksTable[i]),&(m_aeTPi0List.m_aPi0TracksTable[k]));
      	m_aeTPi0List.m_aValidCombs2.push_back(aVC2dummy);		
				
					m_mapheT2G1vs2G2InvM[3]->fill(m_aeTPi0List.m_aPi0TracksTable[i].m_dMass,m_aeTPi0List.m_aPi0TracksTable[k].m_dMass,1);	
    		}
  	}

m_mapheTSumEemc[3]->fill(m_deTSumEemc,1.);		
m_mapheTValid2Combs[3]->fill(m_aeTPi0List.m_aValidCombs2.size(),1.);
m_mapheT2CombswSharedGammas[3]->fill(m_NsharedGam,1);

//printf("%d %d \n",m_aeTPi0List.m_aPi0TracksTable.size(),m_aeTPi0List.m_aValidCombs2.size());


if (m_aeTPi0List.m_aValidCombs2.size() < 1) {
		m_deTTotalChi2 = -5; 
		m_deTPi01Chi2 = -5; 
		m_deTPi02Chi2 = -5;
		return;} // insufficient number of 2pi0 candidates
		m_ieTPi0kept++;
		
    // record combinatorics
    m_ieTAnzPi0Tracks  = m_aeTPi0List.m_aPi0TracksTable.size();
    m_ieTAnz2Combs     = m_aeTPi0List.m_aValidCombs2.size();
		
		m_mapheTNumEvt[4]->fill(0.,1);
		m_mapheTSumEemc[4]->fill(m_deTSumEemc,1.);
		m_mapheTValid2Combs[4]->fill(m_aeTPi0List.m_aValidCombs2.size(),1.);
		m_mapheT2CombswSharedGammas[4]->fill(m_NsharedGam,1);
		//------ END of Find 2 pi0 candidates------//
		
		//------ e-Tagged 2pi0 selection ------//		
		m_ieT2x2GNSizePi0TrackTable=m_aeTPi0List.m_aPi0TracksTable.size();
		//printf("%d %d \n",m_aeTPi0List.m_aPi0TracksTable.size(),m_aeTPi0List.m_aValidCombs2.size());
		
		if(m_bKalFit){	 
    bool bDone = false;
		//------ With Kinematic FIT ------//
		int Nfits=0;
    for(int k=0;k<m_aeTPi0List.m_aValidCombs2.size();k++) {
					HepVector w_zHel(5,0);
					w_zHel = m_pERTLepton->mdcKalTrack()->getZHelix();
    			WTrackParameter wtpElec (m_dMasselec,w_zHel,m_pERTLepton->mdcKalTrack()->getZError());
					m_lvLepton= wtpElec.p();
					cPi0Tracks* pt2G1 = (m_aeTPi0List.m_aValidCombs2[k]).m_pTrack1;
					HepLorentzVector lv2G1G1= m_aeTPi0List.Get4VecFromEMC(pt2G1->m_pTrack1,0.0);
					HepLorentzVector lv2G1G2= m_aeTPi0List.Get4VecFromEMC(pt2G1->m_pTrack2,0.0);
					HepLorentzVector lv2G1 = lv2G1G1+lv2G1G2;
					cPi0Tracks* pt2G2 = (m_aeTPi0List.m_aValidCombs2[k]).m_pTrack2;
					HepLorentzVector lv2G2G1= m_aeTPi0List.Get4VecFromEMC(pt2G2->m_pTrack1,0.0);
					HepLorentzVector lv2G2G2= m_aeTPi0List.Get4VecFromEMC(pt2G2->m_pTrack2,0.0);
					HepLorentzVector lv2G2 = lv2G2G1+lv2G2G2;
					
					HepLorentzVector lvMissLept=m_lvBeamElectron+m_lvBeamPositron-(m_lvLepton+lv2G1+lv2G2);
					m_dCosThetaMissLepton	= lvMissLept.cosTheta();
					
					//Kinematics in e+e- CM frame	
					HepLorentzVector lvLeptonCM=m_lvLepton;
					lvLeptonCM.boost(-m_vectboostCM);
					HepLorentzVector lv2G1CM=lv2G1;
					lv2G1CM.boost(-m_vectboostCM);
					HepLorentzVector lv2G2CM=lv2G2;
					lv2G2CM.boost(-m_vectboostCM);
					m_deTSumPtCM=GetPt(lvLeptonCM+lv2G1CM+lv2G2CM);
					
					//Helicity Angle
					Hep3Vector boost2x2G=(lv2G1+lv2G2).boostVector();
					HepLorentzVector lv2G1G12GCM=lv2G1G1;
					lv2G1G12GCM.boost(boost2x2G);
					m_dCosTheta2G1HelicityG1=(lv2G1G12GCM.vect()).cosTheta(lv2G1.vect());
					
					HepLorentzVector lv2G2G12GCM=lv2G2G1;
					lv2G2G12GCM.boost(boost2x2G);
					m_dCosTheta2G2HelicityG1=(lv2G2G12GCM.vect()).cosTheta(lv2G2.vect());
					
					//RGamma
					double ELept2x2GCM;
					double AbsMomLept2x2GCM;

					ELept2x2GCM=(lvLeptonCM+lv2G1CM+lv2G2CM).e();
					AbsMomLept2x2GCM=((lvLeptonCM+lv2G1CM+lv2G2CM).vect()).rho();
					m_dRGam=(m_dCMSEnergy-ELept2x2GCM-AbsMomLept2x2GCM)/m_dCMSEnergy;
					m_deT2x2GE2G1[m_ieT2x2GN2x2G]=lv2G1.e();
					m_deT2x2GE2G2[m_ieT2x2GN2x2G]=lv2G2.e();	
					m_deT2x2GInvM2G1[m_ieT2x2GN2x2G]=lv2G1.m();					
					m_deT2x2GInvM2G2[m_ieT2x2GN2x2G]=lv2G2.m();	
					m_deT2x2GEAsym2G1[m_ieT2x2GN2x2G] =	GetEAsym2Gam(lv2G1G1,lv2G1G2);
					m_deT2x2GEAsym2G2[m_ieT2x2GN2x2G] =	GetEAsym2Gam(lv2G2G1,lv2G2G2);
					m_deT2x2GCThetaMiss[m_ieT2x2GN2x2G]=m_dCosThetaMissLepton;
					m_deT2x2GInvM2x2G[m_ieT2x2GN2x2G]=(lv2G1+lv2G2).m();
					m_deT2x2GSumPtCM[m_ieT2x2GN2x2G]=m_deTSumPtCM;
					m_deT2x2GCTheta2G1HelicityG1[m_ieT2x2GN2x2G]=m_dCosTheta2G1HelicityG1;
					m_deT2x2GCTheta2G2HelicityG1[m_ieT2x2GN2x2G]=m_dCosTheta2G2HelicityG1;
					m_deT2x2GRGam[m_ieT2x2GN2x2G]=m_dRGam;
					
					m_ieT2x2GN2x2G++;	
							
					m_mapheT2G1InvM[4]->fill(lv2G1.m(),1);
					m_mapheT2G2InvM[4]->fill(lv2G2.m(),1);
					m_mapheTEAsym2G1[4]->fill(GetEAsym2Gam(lv2G1G1,lv2G1G2),1);
					m_mapheTEAsym2G2[4]->fill(GetEAsym2Gam(lv2G2G1,lv2G2G2),1);
					m_mapheT2x2GCThetaMiss[4]->fill(m_dCosThetaMissLepton,1);
					m_mapheT2x2GCTheta2G1HelicityG1[4]->fill(m_dCosTheta2G1HelicityG1,1);
					m_mapheT2x2GCTheta2G2HelicityG1[4]->fill(m_dCosTheta2G2HelicityG1,1);
					m_mapheT2x2GInvM[4]->fill((lv2G1+lv2G2).m(),1);
					m_mapheT2x2GSumPt[4]->fill(m_deTSumPtCM,1);
					//m_mapheT2x2GcosTheta2G1CM2Gam[4]->fill(,1);
					m_mapheT2x2GRGam[4]->fill(m_dRGam);		
			
				/*	
			//6C Kinematic fit
			
			//Pi0 mass contrained fit of the decay photons
			m_KalKinFit->init();
    	//m_KalKinFit->setChisqCut(m_dChisqMaxValue);;
			cPi0Tracks* ptPi01 = m_aeTPi0List.m_aValidCombs2[k].m_pTrack1;
			cPi0Tracks* ptPi02 = m_aeTPi0List.m_aValidCombs2[k].m_pTrack2;	
			m_KalKinFit->AddTrack(0,wtpElec);	
			m_KalKinFit->AddTrack(1,0.0,ptPi01->m_pTrack1->emcShower());
    	m_KalKinFit->AddTrack(2,0.0,ptPi01->m_pTrack2->emcShower());
      m_KalKinFit->AddTrack(3,0.0,ptPi02->m_pTrack1->emcShower());
      m_KalKinFit->AddTrack(4,0.0,ptPi02->m_pTrack2->emcShower());
      m_KalKinFit->AddMissTrack(5,0.0, m_lvBoost-wtpElec.p()
                                               -m_aeTPi0List.Get4VecFromEMC(ptPi01->m_pTrack1,0.0)
                                               -m_aeTPi0List.Get4VecFromEMC(ptPi01->m_pTrack2,0.0)
																							 -m_aeTPi0List.Get4VecFromEMC(ptPi02->m_pTrack1,0.0)
                                               -m_aeTPi0List.Get4VecFromEMC(ptPi02->m_pTrack2,0.0));
			
			//Add pi0 mass constraint
			m_KalKinFit->AddFourMomentum(0,m_lvBoost);
			m_KalKinFit->AddResonance(1,m_dMassPi0,1,2);
			m_KalKinFit->AddResonance(2,m_dMassPi0,3,4);
			
																							 
			//Perform the fit
			if (!m_KalKinFit->Fit()) {continue;}
			Nfits++;
				if (m_KalKinFit->chisq() > 0 && m_KalKinFit->chisq() < m_deTTotalChi2 ) {
				//if (m_KalKinFit->chisq() > 0 && m_KalKinFit->chisq() < m_deTTotalChi2 && m_KalKinFit->chisq()<m_dChisqMaxValue ) {    
				//Prepare to write 
				m_deTTotalChi2 = m_KalKinFit->chisq();
				// detector
				m_lvLepton = wtpElec.p();
				m_lvGammaStar = m_lvBeamElectron-m_lvLepton;
    		m_lvPi01   = m_aeTPi0List.Get4VecFromEMC(ptPi01->m_pTrack1,0.0) + m_aeTPi0List.Get4VecFromEMC(ptPi01->m_pTrack2,0.0);
    		m_lvPi01G1 = m_aeTPi0List.Get4VecFromEMC(ptPi01->m_pTrack1,0.0);
    		m_lvPi01G2 = m_aeTPi0List.Get4VecFromEMC(ptPi01->m_pTrack2,0.0);
    		m_lvPi02   = m_aeTPi0List.Get4VecFromEMC(ptPi02->m_pTrack1,0.0) + m_aeTPi0List.Get4VecFromEMC(ptPi02->m_pTrack2,0.0);
    		m_lvPi02G1 = m_aeTPi0List.Get4VecFromEMC(ptPi02->m_pTrack1,0.0);
    		m_lvPi02G2 = m_aeTPi0List.Get4VecFromEMC(ptPi02->m_pTrack2,0.0);
    		m_aPhIndexList.clear();
   		  m_aPhIndexList.push_back (ptPi01->m_pTrack1);
    		m_aPhIndexList.push_back (ptPi01->m_pTrack2);
    		m_aPhIndexList.push_back (ptPi02->m_pTrack1);
    		m_aPhIndexList.push_back (ptPi02->m_pTrack2);
    		// fit
				m_lvFitLepton = m_KalKinFit->pfit(0);
				m_lvFitGammaStar = m_lvBeamElectron-m_lvFitLepton;
    		m_lvFitPi01G1 = m_KalKinFit->pfit(1);
    		m_lvFitPi01G2 = m_KalKinFit->pfit(2);
    		m_lvFitPi02G1 = m_KalKinFit->pfit(3);
    		m_lvFitPi02G2 = m_KalKinFit->pfit(4);
    		m_lvFitPi01   = m_KalKinFit->pfit(1) + m_KalKinFit->pfit(2);
    		m_lvFitPi02   = m_KalKinFit->pfit(3) + m_KalKinFit->pfit(4);
    		
    		// the lines below MUSST stay below the settings of the 4vectors!
    		if (m_KalKinFit->Fit(1)) {m_deTPi01Chi2 = m_KalKinFit->chisq(1);} else {m_deTPi01Chi2 = -5;}
    		if (m_KalKinFit->Fit(2)) {m_deTPi02Chi2 = m_KalKinFit->chisq(2);} else {m_deTPi02Chi2 = -5;}			
				
        bDone = true;
				
				*/
				
				
				//2 C Kinematic Fit
				//Pi0 mass contrained fit of the decay photons
			m_KalKinFit->init();
    	//m_KalKinFit->setChisqCut(m_dChisqMaxValue);
			cPi0Tracks* ptPi01 = m_aeTPi0List.m_aValidCombs2[k].m_pTrack1;
			cPi0Tracks* ptPi02 = m_aeTPi0List.m_aValidCombs2[k].m_pTrack2;	
			m_KalKinFit->AddTrack(0,wtpElec);	
			m_KalKinFit->AddTrack(1,0.0,ptPi01->m_pTrack1->emcShower());
    	m_KalKinFit->AddTrack(2,0.0,ptPi01->m_pTrack2->emcShower());
      m_KalKinFit->AddTrack(3,0.0,ptPi02->m_pTrack1->emcShower());
      m_KalKinFit->AddTrack(4,0.0,ptPi02->m_pTrack2->emcShower());
			
			//Add pi0 mass constraint
			m_KalKinFit->AddResonance(0,m_dMassPi0,1,2);
			m_KalKinFit->AddResonance(1,m_dMassPi0,3,4);
			
																							 
			//Perform the fit
			if (!m_KalKinFit->Fit()) {continue;}
			Nfits++;
			m_mapheTKinFitChi2[4]->fill(m_KalKinFit->chisq(),1);
			
				if (m_KalKinFit->chisq() > 0 && m_KalKinFit->chisq() < m_deTTotalChi2 && m_KalKinFit->chisq()<m_dChisqMaxValue ) {
				//cout<<  m_KalKinFit->chisq() << endl;   
				//Prepare to write 
				m_deTTotalChi2 = m_KalKinFit->chisq();
				// detector
				m_lvLepton = wtpElec.p();
				m_lvGammaStar = m_lvBeamElectron-m_lvLepton;
    		m_lvPi01   = m_aeTPi0List.Get4VecFromEMC(ptPi01->m_pTrack1,0.0) + m_aeTPi0List.Get4VecFromEMC(ptPi01->m_pTrack2,0.0);
    		m_lvPi01G1 = m_aeTPi0List.Get4VecFromEMC(ptPi01->m_pTrack1,0.0);
    		m_lvPi01G2 = m_aeTPi0List.Get4VecFromEMC(ptPi01->m_pTrack2,0.0);
    		m_lvPi02   = m_aeTPi0List.Get4VecFromEMC(ptPi02->m_pTrack1,0.0) + m_aeTPi0List.Get4VecFromEMC(ptPi02->m_pTrack2,0.0);
    		m_lvPi02G1 = m_aeTPi0List.Get4VecFromEMC(ptPi02->m_pTrack1,0.0);
    		m_lvPi02G2 = m_aeTPi0List.Get4VecFromEMC(ptPi02->m_pTrack2,0.0);
    		m_aPhIndexList.clear();
   		  m_aPhIndexList.push_back (ptPi01->m_pTrack1);
    		m_aPhIndexList.push_back (ptPi01->m_pTrack2);
    		m_aPhIndexList.push_back (ptPi02->m_pTrack1);
    		m_aPhIndexList.push_back (ptPi02->m_pTrack2);
    		// fit
				m_lvFitLepton = m_KalKinFit->pfit(0);
				m_lvFitGammaStar = m_lvBeamElectron-m_lvFitLepton;
    		m_lvFitPi01G1 = m_KalKinFit->pfit(1);
    		m_lvFitPi01G2 = m_KalKinFit->pfit(2);
    		m_lvFitPi02G1 = m_KalKinFit->pfit(3);
    		m_lvFitPi02G2 = m_KalKinFit->pfit(4);
    		m_lvFitPi01   = m_KalKinFit->pfit(1) + m_KalKinFit->pfit(2);
    		m_lvFitPi02   = m_KalKinFit->pfit(3) + m_KalKinFit->pfit(4);
    		
    		// the lines below MUSST stay below the settings of the 4vectors!
    		if (m_KalKinFit->Fit(0)) {m_deTPi01Chi2 = m_KalKinFit->chisq(0);} else {m_deTPi01Chi2 = -5;}
    		if (m_KalKinFit->Fit(1)) {m_deTPi02Chi2 = m_KalKinFit->chisq(1);} else {m_deTPi02Chi2 = -5;}			
				
				
        bDone = true;
				
      }
			
    }
//cout << "FINAL CHI2 " << m_deTTotalChi2 <<endl;
//cout << endl;
if(m_bWriteNt2x2G==true) m_NeT2x2G->write();
if (!bDone) {m_deTTotalChi2 = -5;m_deTPi01Chi2 = -5; m_deTPi02Chi2 = -5; return;}
m_ieTFitKept++;
	}
	else{
	//------ Without Kinematic FIT ------//
			if (m_aeTPi0List.m_aValidCombs2.size() != 1) {return;}
			for(int k=0;k<m_aeTPi0List.m_aValidCombs2.size();k++) {
			HepVector w_zHel(5,0);
					w_zHel = m_pERTLepton->mdcKalTrack()->getZHelix();
    			WTrackParameter wtpElec (m_dMasselec,w_zHel,m_pERTLepton->mdcKalTrack()->getZError());
					m_lvLepton= wtpElec.p();
					cPi0Tracks* pt2G1 = (m_aeTPi0List.m_aValidCombs2[k]).m_pTrack1;
					HepLorentzVector lv2G1G1= m_aeTPi0List.Get4VecFromEMC(pt2G1->m_pTrack1,0.0);
					HepLorentzVector lv2G1G2= m_aeTPi0List.Get4VecFromEMC(pt2G1->m_pTrack2,0.0);
					HepLorentzVector lv2G1 = lv2G1G1+lv2G1G2;
					cPi0Tracks* pt2G2 = (m_aeTPi0List.m_aValidCombs2[k]).m_pTrack2;
					HepLorentzVector lv2G2G1= m_aeTPi0List.Get4VecFromEMC(pt2G2->m_pTrack1,0.0);
					HepLorentzVector lv2G2G2= m_aeTPi0List.Get4VecFromEMC(pt2G2->m_pTrack2,0.0);
					HepLorentzVector lv2G2 = lv2G2G1+lv2G2G2;
					
					HepLorentzVector lvMissLept=m_lvBeamElectron+m_lvBeamPositron-(m_lvLepton+lv2G1+lv2G2);
					m_dCosThetaMissLepton	= lvMissLept.cosTheta();
					
					//Kinematics in e+e- CM frame	
					HepLorentzVector lvLeptonCM=m_lvLepton;
					lvLeptonCM.boost(-m_vectboostCM);
					HepLorentzVector lv2G1CM=lv2G1;
					lv2G1CM.boost(-m_vectboostCM);
					HepLorentzVector lv2G2CM=lv2G2;
					lv2G2CM.boost(-m_vectboostCM);
					m_deTSumPtCM=GetPt(lvLeptonCM+lv2G1CM+lv2G2CM);
					
					//Helicity Angle
					Hep3Vector boost2x2G=(lv2G1+lv2G2).boostVector();
					HepLorentzVector lv2G1G12GCM=lv2G1G1;
					lv2G1G12GCM.boost(boost2x2G);
					m_dCosTheta2G1HelicityG1=(lv2G1G12GCM.vect()).cosTheta(lv2G1.vect());
					
					HepLorentzVector lv2G2G12GCM=lv2G2G1;
					lv2G2G12GCM.boost(boost2x2G);
					m_dCosTheta2G2HelicityG1=(lv2G2G12GCM.vect()).cosTheta(lv2G2.vect());
					
					//RGamma
					double ELept2x2GCM;
					double AbsMomLept2x2GCM;

					ELept2x2GCM=(lvLeptonCM+lv2G1CM+lv2G2CM).e();
					AbsMomLept2x2GCM=((lvLeptonCM+lv2G1CM+lv2G2CM).vect()).rho();
					m_dRGam=(m_dCMSEnergy-ELept2x2GCM-AbsMomLept2x2GCM)/m_dCMSEnergy;
					m_deT2x2GE2G1[m_ieT2x2GN2x2G]=lv2G1.e();
					m_deT2x2GE2G2[m_ieT2x2GN2x2G]=lv2G2.e();	
					m_deT2x2GInvM2G1[m_ieT2x2GN2x2G]=lv2G1.m();					
					m_deT2x2GInvM2G2[m_ieT2x2GN2x2G]=lv2G2.m();	
					m_deT2x2GEAsym2G1[m_ieT2x2GN2x2G] =	GetEAsym2Gam(lv2G1G1,lv2G1G2);
					m_deT2x2GEAsym2G2[m_ieT2x2GN2x2G] =	GetEAsym2Gam(lv2G2G1,lv2G2G2);
					m_deT2x2GCThetaMiss[m_ieT2x2GN2x2G]=m_dCosThetaMissLepton;
					m_deT2x2GInvM2x2G[m_ieT2x2GN2x2G]=(lv2G1+lv2G2).m();
					m_deT2x2GSumPtCM[m_ieT2x2GN2x2G]=m_deTSumPtCM;
					m_deT2x2GCTheta2G1HelicityG1[m_ieT2x2GN2x2G]=m_dCosTheta2G1HelicityG1;
					m_deT2x2GCTheta2G2HelicityG1[m_ieT2x2GN2x2G]=m_dCosTheta2G2HelicityG1;
					m_deT2x2GRGam[m_ieT2x2GN2x2G]=m_dRGam;
					
					m_ieT2x2GN2x2G++;	
							
					m_mapheT2G1InvM[4]->fill(lv2G1.m(),1);
					m_mapheT2G2InvM[4]->fill(lv2G2.m(),1);
					m_mapheTEAsym2G1[4]->fill(GetEAsym2Gam(lv2G1G1,lv2G1G2),1);
					m_mapheTEAsym2G2[4]->fill(GetEAsym2Gam(lv2G2G1,lv2G2G2),1);
					m_mapheT2x2GCThetaMiss[4]->fill(m_dCosThetaMissLepton,1);
					m_mapheT2x2GCTheta2G1HelicityG1[4]->fill(m_dCosTheta2G1HelicityG1,1);
					m_mapheT2x2GCTheta2G2HelicityG1[4]->fill(m_dCosTheta2G2HelicityG1,1);
					m_mapheT2x2GInvM[4]->fill((lv2G1+lv2G2).m(),1);
					m_mapheT2x2GSumPt[4]->fill(m_deTSumPtCM,1);
					//m_mapheT2x2GcosTheta2G1CM2Gam[4]->fill(,1);
					m_mapheT2x2GRGam[4]->fill(m_dRGam);
			
			cPi0Tracks* ptPi01 = m_aeTPi0List.m_aValidCombs2[k].m_pTrack1;
			cPi0Tracks* ptPi02 = m_aeTPi0List.m_aValidCombs2[k].m_pTrack2;
			
    		// detector
				m_lvLepton = wtpElec.p();
				m_lvGammaStar = m_lvBeamElectron-m_lvLepton;
    		m_lvPi01   = m_aeTPi0List.Get4VecFromEMC(ptPi01->m_pTrack1,0.0) + m_aeTPi0List.Get4VecFromEMC(ptPi01->m_pTrack2,0.0);
    		m_lvPi01G1 = m_aeTPi0List.Get4VecFromEMC(ptPi01->m_pTrack1,0.0);
    		m_lvPi01G2 = m_aeTPi0List.Get4VecFromEMC(ptPi01->m_pTrack2,0.0);
    		m_lvPi02   = m_aeTPi0List.Get4VecFromEMC(ptPi02->m_pTrack1,0.0) + m_aeTPi0List.Get4VecFromEMC(ptPi02->m_pTrack2,0.0);
    		m_lvPi02G1 = m_aeTPi0List.Get4VecFromEMC(ptPi02->m_pTrack1,0.0);
    		m_lvPi02G2 = m_aeTPi0List.Get4VecFromEMC(ptPi02->m_pTrack2,0.0);
    		m_aPhIndexList.clear();
   		  m_aPhIndexList.push_back (ptPi01->m_pTrack1);
    		m_aPhIndexList.push_back (ptPi01->m_pTrack2);
    		m_aPhIndexList.push_back (ptPi02->m_pTrack1);
    		m_aPhIndexList.push_back (ptPi02->m_pTrack2);		
    }
		
	if(m_bWriteNt2x2G==true) m_NeT2x2G->write();
	}

m_mapheTNumEvt[5]->fill(0.,1);
	
//Fill particle containers
m_eTLepton.Fill(&m_lvLepton);
m_eTPi01.Fill(&m_lvPi01);
m_eTPi02.Fill(&m_lvPi02);
m_eTGammas[0].Fill(&m_lvPi01G1);
m_eTGammas[1].Fill(&m_lvPi01G2);
m_eTGammas[2].Fill(&m_lvPi02G1);
m_eTGammas[3].Fill(&m_lvPi02G2);
m_eTFitLepton.Fill(&m_lvFitLepton);
m_eTFitPi01.Fill(&m_lvFitPi01);
m_eTFitPi02.Fill(&m_lvFitPi02);
m_eTFitGammas[0].Fill(&m_lvFitPi01G1);
m_eTFitGammas[1].Fill(&m_lvFitPi01G2);
m_eTFitGammas[2].Fill(&m_lvFitPi02G1);
m_eTFitGammas[3].Fill(&m_lvFitPi02G2);

//Tagged virtuality
m_deTQ2=-(m_lvGammaStar).m2();
m_deTFitQ2=-(m_lvFitGammaStar).m2();

//Invariant mass
m_deTPi01InvM=m_lvPi01.m();
m_deTPi02InvM=m_lvPi02.m();
m_deT2pi0InvM=(m_lvPi01+m_lvPi02).m();
	
m_deTFitPi01InvM=m_lvFitPi01.m();
m_deTFitPi02InvM=m_lvFitPi02.m();
m_deTFit2pi0InvM=(m_lvFitPi01+m_lvFitPi02).m();

m_deTMC2pi0InvM=m_dMC2pi0InvM;

//Energy Asymmetry
m_deTEAsymPi01=GetEAsym2Gam(m_lvPi01G1,m_lvPi01G2);
m_deTEAsymPi02=GetEAsym2Gam(m_lvPi02G1,m_lvPi02G2);

m_deTFitEAsymPi01=GetEAsym2Gam(m_lvFitPi01G1,m_lvFitPi01G2);
m_deTFitEAsymPi02=GetEAsym2Gam(m_lvFitPi02G1,m_lvFitPi02G2);

//Kinematics in e+e- CM frame	
m_lvLeptonCM=m_lvLepton;
m_lvLeptonCM.boost(-m_vectboostCM);
m_lvPi01CM=m_lvPi01;
m_lvPi01CM.boost(-m_vectboostCM);
m_lvPi02CM=m_lvPi02;
m_lvPi02CM.boost(-m_vectboostCM);

m_lvFitLeptonCM=m_lvFitLepton;
m_lvFitLeptonCM.boost(-m_vectboostCM);
m_lvFitPi01CM=m_lvFitPi01;
m_lvFitPi01CM.boost(-m_vectboostCM);
m_lvFitPi02CM=m_lvFitPi02;
m_lvFitPi02CM.boost(-m_vectboostCM);

//Tranverse momentum
m_deTSumPtCM=GetPt(m_lvLeptonCM+m_lvPi01CM+m_lvPi02CM);
m_deTFitSumPtCM=GetPt(m_lvFitLeptonCM+m_lvFitPi01CM+m_lvFitPi02CM);

if(m_ieTQTag==-1) m_deTMCSumPtCM=m_dMCeTSumPtCM;
if(m_ieTQTag==1) m_deTMCSumPtCM=m_dMCpTSumPtCM;

//Kinematics in 2 Gammas CM frame	
m_boost2pi0=(m_lvPi01+m_lvPi02).boostVector();
m_lvPi01CM2Gam=m_lvPi01;
m_lvPi01CM2Gam.boost(-m_boost2pi0);
m_lvLeptonCM2Gam=m_lvLeptonCM;
m_lvLeptonCM2Gam.boost(-m_boost2pi0);


m_boostFit2pi0=(m_lvFitPi01+m_lvFitPi02).boostVector();
m_lvFitPi01CM2Gam=m_lvFitPi01;
m_lvFitPi01CM2Gam.boost(-m_boostFit2pi0);
m_lvFitLeptonCM2Gam=m_lvFitLeptonCM;
m_lvFitLeptonCM2Gam.boost(-m_boostFit2pi0);

//Z axis for Gamma-Gamma CM frame : Z direction along gamma star
Hep3Vector ZAxisGam2pi0CM;
Hep3Vector YAxisGam2pi0CM;
Hep3Vector XAxisGam2pi0CM;
double ThetaPi01CM2Gam;
double PhiPi01CM2Gam;
//Without fit
ZAxisGam2pi0CM = (m_lvGammaStar.vect()).unit();
YAxisGam2pi0CM = (ZAxisGam2pi0CM.cross(m_lvLeptonCM2Gam)).unit();
XAxisGam2pi0CM = (YAxisGam2pi0CM.cross(ZAxisGam2pi0CM)).unit();
if((m_lvLeptonCM2Gam.vect()).dot(XAxisGam2pi0CM)<0) log << MSG::WARNING << "***** Px(e-) <0 IN GG FRAME !!!!!!!! *****" << endreq;
ThetaPi01CM2Gam=m_lvPi01CM2Gam.angle(ZAxisGam2pi0CM);
if((m_lvPi01CM2Gam.vect()).dot(XAxisGam2pi0CM)>=0) PhiPi01CM2Gam=m_lvPi01CM2Gam.angle(XAxisGam2pi0CM);
else PhiPi01CM2Gam=-m_lvPi01CM2Gam.angle(XAxisGam2pi0CM);
//if((m_lvFitPi01CM2Gam.vect()).dot(XAxisGam2pi0CM)<0) log << MSG::WARNING << "***** m_lvFitPi01CM2Gam.dot(XAxisGam2pi0CM)<0 *****" << endreq;

m_deTcosThetaPi01CM2Gam=cos(ThetaPi01CM2Gam);
m_deTPhiPi01CM2Gam=PhiPi01CM2Gam;

//With fit
ZAxisGam2pi0CM = (m_lvFitGammaStar.vect()).unit();
YAxisGam2pi0CM = (ZAxisGam2pi0CM.cross(m_lvFitLeptonCM2Gam)).unit();
XAxisGam2pi0CM = (YAxisGam2pi0CM.cross(ZAxisGam2pi0CM)).unit();
if((m_lvFitLeptonCM2Gam.vect()).dot(XAxisGam2pi0CM)<0) log << MSG::WARNING << "***** Px(e-) <0 IN GG FRAME !!!!!!!! *****" << endreq;
ThetaPi01CM2Gam=m_lvFitPi01CM2Gam.angle(ZAxisGam2pi0CM);
if((m_lvFitPi01CM2Gam.vect()).dot(XAxisGam2pi0CM)>=0) PhiPi01CM2Gam=m_lvFitPi01CM2Gam.angle(XAxisGam2pi0CM);
else PhiPi01CM2Gam=-m_lvFitPi01CM2Gam.angle(XAxisGam2pi0CM);
//if((m_lvFitPi01CM2Gam.vect()).dot(XAxisGam2pi0CM)<0) log << MSG::WARNING << "***** m_lvFitPi01CM2Gam.dot(XAxisGam2pi0CM)<0 *****" << endreq;

m_deTFitcosThetaPi01CM2Gam=cos(ThetaPi01CM2Gam);
m_deTFitPhiPi01CM2Gam=PhiPi01CM2Gam;

/*
//Alternate transformation into Gamma-Gamma frame


*/

//Helicity Angle
HepLorentzVector lv2G=m_lvPi01; 
HepLorentzVector lvG12GCM=m_lvPi01G1;
lvG12GCM.boost(-m_boost2pi0);		
m_dCosTheta2GHelicityG1=(lvG12GCM.vect()).cosTheta(lv2G.vect());
m_deTCThetaHelicity=m_dCosTheta2GHelicityG1;

HepLorentzVector lvFit2G=m_lvFitPi01; 
HepLorentzVector lvFitG12GCM=m_lvFitPi01G1;
lvFitG12GCM.boost(-m_boostFit2pi0);		
m_dCosTheta2GHelicityG1=(lvFitG12GCM.vect()).cosTheta(lvFit2G.vect());
m_deTFitCThetaHelicity=m_dCosTheta2GHelicityG1;

//RGamma
double ELept2Pi0CM;
double AbsMomLept2Pi0CM;

ELept2Pi0CM=(m_lvLeptonCM+m_lvPi01CM+m_lvPi02CM).e();
AbsMomLept2Pi0CM=((m_lvLeptonCM+m_lvPi01CM+m_lvPi02CM).vect()).rho();
m_dRGam=(m_dCMSEnergy-ELept2Pi0CM-AbsMomLept2Pi0CM)/m_dCMSEnergy;
m_deTRGam=m_dRGam;

ELept2Pi0CM=(m_lvFitLeptonCM+m_lvFitPi01CM+m_lvFitPi02CM).e();
AbsMomLept2Pi0CM=((m_lvFitLeptonCM+m_lvFitPi01CM+m_lvFitPi02CM).vect()).rho();
m_dRGam=(m_dCMSEnergy-ELept2Pi0CM-AbsMomLept2Pi0CM)/m_dCMSEnergy;
m_deTFitRGam=m_dRGam;
			
m_mapheTNumEvt[5]->fill(0,1.);
m_mapheTSumEemc[5]->fill(m_deTSumEemc,1.);
m_mapheTValid2Combs[5]->fill(m_aeTPi0List.m_aValidCombs2.size(),1.);
m_mapheT2CombswSharedGammas[5]->fill(m_NsharedGam,1);
m_mapheTKinFitChi2[5]->fill(m_deTTotalChi2,1);
m_mapheTPi01InvM[5]->fill(m_deTPi01InvM,1.);
m_mapheTPi02InvM[5]->fill(m_deTPi02InvM,1.);
m_mapheTFitPi01InvM[5]->fill(m_deTFitPi01InvM,1.);
m_mapheTFitPi02InvM[5]->fill(m_deTFitPi02InvM,1.);
m_mapheT2Pi0InvM[5]->fill(m_deT2pi0InvM,1.);
m_mapheTFit2Pi0InvM[5]->fill(m_deTFit2pi0InvM,1.);
m_mapheTFit2Pi0InvMvs2Pi0InvM[5]->fill(m_deT2pi0InvM,m_deTFit2pi0InvM,1.);
m_mapheTFitEAsymPi01[5]->fill(m_deTFitEAsymPi01);
m_mapheTFitEAsymPi02[5]->fill(m_deTFitEAsymPi02);
m_mapheTSumPt[5]->fill(m_deTSumPtCM,1.);
m_mapheTFitSumPt[5]->fill(m_deTFitSumPtCM,1.);
m_mapheTFitcosThetaPi01CM2Gam[5]->fill(m_deTFitcosThetaPi01CM2Gam,1.);
m_mapheTRGam[5]->fill(m_deTRGam,1.);
m_mapheTFitRGam[5]->fill(m_deTFitRGam,1.);

bool bStage5OK=m_deTTotalChi2<m_dChisqMaxValue;
bool bStage6OK=m_deTSumEemc<m_dUSumEemcMax;
bool bStage7OK=m_deTFitSumPtCM<= m_dUSumPtMax;

if(bStage5OK && bStage6OK && bStage7OK) m_iIseT2pi0=1;

if(!bStage5OK){return;}
if(!bStage6OK){return;}
if(!bStage7OK){return;}

m_NeTagged->write();
m_iAcceptedeT++;	

//if(m_deTSumEemc<m_dUSumEemcMax && m_deTTotalChi2<m_dChisqMaxValue && m_deTFitSumPtCM<m_dUSumPtMax) m_iIseT2pi0=1;
/*
bool bStage5OK=m_deTTotalChi2<m_dChisqMaxValue;
if(!bStage5OK){return;}

bool bStage6OK=m_deTSumEemc<m_dUSumEemcMax;
if(!bStage6OK){return;}

bool bStage7OK=m_deTFitSumPtCM<= m_dUSumPtMax;
if(!bStage7OK){return;}

m_iIseT2pi0=1;

m_iAcceptedeT++;				
*/
		
return;
}
//---------------------------------------------------------------------------------------------------------- 

//---------------------------------------------------------------------------------------------------------- 
double Bes2Gamto2Pi0::IPdist (EvtRecTrack* pCur)
//----------------------------------------------------------------------------------------------------------
{
    // Constructor with pivot, helix parameter a, and its error matrix.
    HepPoint3D   point0(0,0,0);
    VFHelix      helixipP(point0,pCur->mdcKalTrack()->helix(),pCur->mdcKalTrack()->err());
    helixipP.pivot(m_hp3IP);
    return (sqrt(pow(fabs(helixipP.a()[0]),2)+pow(helixipP.a()[3],2)));
}
//---------------------------------------------------------------------------------------------------------- 

//---------------------------------------------------------------------------------------------------------- 
double Bes2Gamto2Pi0::GetEAsym2Gam(HepLorentzVector lvG1,HepLorentzVector lvG2)
//----------------------------------------------------------------------------------------------------------
{
    double EAsym;
		double EG1=lvG1.e();
		double EG2=lvG2.e();
		EAsym=(fabs(EG1-EG2))/(EG1+EG2);
		return EAsym;
}
//---------------------------------------------------------------------------------------------------------- 

//---------------------------------------------------------------------------------------------------------- 
HepLorentzVector Bes2Gamto2Pi0::GetBoostedLV(HepLorentzVector lvIniFrame, Hep3Vector vectboost)
//----------------------------------------------------------------------------------------------------------
{
    HepLorentzVector lvBoostedFrame;
		lvBoostedFrame=lvIniFrame;
		lvBoostedFrame.boost(-vectboost);
		return lvBoostedFrame;
}
//----------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------- 
int Bes2Gamto2Pi0::GetNBins(double xmin, double xmax, double dx)
//----------------------------------------------------------------------------------------------------------
{
    int NBins=0;
		NBins=fabs(xmax-xmin)/dx;
		return NBins;
}
//----------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------- 
IHistogram1D* Bes2Gamto2Pi0::MakeIHistogram1D(const std::string &dirPath, const std::string &relPath,const std::string &title, double deltaX, double lowX, double highX)
//----------------------------------------------------------------------------------------------------------
{
    IHistogram1D* histo;
		int nbinsX=GetNBins(lowX,highX,deltaX);
		histo=histoSvc()->book(dirPath,relPath,title,nbinsX,lowX,highX);
		return histo;
}
//----------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------- 
IHistogram2D* Bes2Gamto2Pi0::MakeIHistogram2D(const std::string &dirPath, const std::string &relPath,const std::string &title, double deltaX, double lowX, double highX,double deltaY, double lowY, double highY)
//----------------------------------------------------------------------------------------------------------
{
    IHistogram2D* histo;
		int nbinsX=GetNBins(lowX,highX,deltaX);
		int nbinsY=GetNBins(lowY,highY,deltaY);
		histo=histoSvc()->book(dirPath,relPath,title,nbinsX,lowX,highX,nbinsY,lowY,highY);
		return histo;
}
//----------------------------------------------------------------------------------------------------------

/*
//---------------------------------------------------------------------------------------------------------- 
void Bes2Gamto2Pi0::BoostKine(std::vector<HepLorentzVector>& veclv, Hep3Vector boostvec)
//----------------------------------------------------------------------------------------------------------
{
 for(int k=0;k<veclv.size();k++){
	veclv[k].boost(-boostvec);
	}   
}
//----------------------------------------------------------------------------------------------------------
*/

/*
//---------------------------------------------------------------------------------------------------------- 
std::vector<HepLorentzVector> Bes2Gamto2Pi0::MakeBoostedVecLV(std::vector<HepLorentzVector> iniveclv, Hep3Vector boostvec)
//----------------------------------------------------------------------------------------------------------
{
 std::vector<HepLorentzVector> veclv;
 for(int k=0;k<iniveclv.size();k++){
	veclv.push_back(iniveclv[k]);
	(veclv.back()).boost(-boostvec);
	} 
return veclv;  
}
//----------------------------------------------------------------------------------------------------------
*/

/*
//---------------------------------------------------------------------------------------------------------- 
bool Bes2Gamto2Pi0::Find2pi0Candidates()
//----------------------------------------------------------------------------------------------------------
{
    EvtRecTrackIterator    itERT;

    // clear lists

    // count good photons
    int iGood = 0;
    for (itERT = m_itEndCharged;itERT != m_itEndNeutral;itERT ++) {
        if (m_aUPi0List.IsPhoton(*itERT)) {iGood ++;}
    }
    if (iGood < 4 || iGood > m_dMaxGoodPh) {m_iUPhotonSkip ++; return false;}
    m_iUAnzGoodGamma = iGood;

    // find pi0 -> 2gamma
    m_aUPi0List.FillList();
    if (m_aUPi0List.m_aPi0TracksTable.size() > m_dMaxPi0Combs) {m_iUPi0Skip ++; return false;}
    m_aUPi0List.CreatedValid2Combs ();
    m_aUPi0List.CreatedValid3Combs ();
    // insufficient number of 2pi0 candidates
    if (m_aUPi0List.m_aValidCombs2.size() < 1) {m_iUPi0Skip ++; return false;}

    // record combinatorics
    m_iUAnzPi0Tracks  = m_aUPi0List.m_aPi0TracksTable.size();
    m_iUAnz2Combs     = m_aUPi0List.m_aValidCombs2.size();
    m_iUAnz3Combs     = m_aUPi0List.m_aValidCombs3.size();
		
		return true;
}
//----------------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------------- 
bool Bes2Gamto2Pi0::Findelpos2pi0Untagged()
//----------------------------------------------------------------------------------------------------------
{
    if (m_aUPi0List.m_aValidCombs2.size() < 1)
       {m_dUTotalChi2 = -5; m_dUPi01Chi2 = -5; m_dUPi02Chi2 = -5; return false;}
    bool bDone = false;
	
    // try untagged fit
    for(int k=0;k<m_aUPi0List.m_aValidCombs2.size();k++) {
			//Pi0 mass contrained fit of the decay photons
			m_KalKinFit->init();
    	m_KalKinFit->setChisqCut(m_dChisqMaxValue);
			cPi0Tracks* ptPi01 = m_aUPi0List.m_aValidCombs2[k].m_pTrack1;
			cPi0Tracks* ptPi02 = m_aUPi0List.m_aValidCombs2[k].m_pTrack2;
			m_KalKinFit->AddTrack(0,0.0,ptPi01->m_pTrack1->emcShower());
    	m_KalKinFit->AddTrack(1,0.0,ptPi01->m_pTrack2->emcShower());
      m_KalKinFit->AddTrack(2,0.0,ptPi02->m_pTrack1->emcShower());
      m_KalKinFit->AddTrack(3,0.0,ptPi02->m_pTrack2->emcShower());
      
			m_KalKinFit->AddMissTrack(4,0.0,m_lvBoost -m_aUPi0List.Get4VecFromEMC(ptPi01->m_pTrack1,0.0)
                                               -m_aUPi0List.Get4VecFromEMC(ptPi01->m_pTrack2,0.0)
                                               -m_aUPi0List.Get4VecFromEMC(ptPi02->m_pTrack1,0.0)
                                               -m_aUPi0List.Get4VecFromEMC(ptPi02->m_pTrack2,0.0));
			//Add 4-momentum constraint
			m_KalKinFit->AddFourMomentum(0,m_lvBoost);
			//Add pi0 mass constraint
			m_KalKinFit->AddResonance(1,m_dMassPi0,0,1);
			m_KalKinFit->AddResonance(2,m_dMassPi0,2,3);																				 
			//Perform the fit
			if (!m_KalKinFit->Fit()) {continue;}
      if (m_KalKinFit->chisq() > 0 && m_KalKinFit->chisq() < m_dUTotalChi2) {
        //PrepareToWrite (m_aUPi0List.m_aValidCombs2[k].m_pTrack1,m_aUPi0List.m_aValidCombs2[k].m_pTrack2,0,0);
        //m_d4NPiIsrUChi2  = m_KalKinFit->chisq();
				    
				//Prepare to write 
				m_dUTotalChi2 = m_KalKinFit->chisq();
    		// fit
    		m_lvFitPi01G1 = m_KalKinFit->pfit(0);
    		m_lvFitPi01G2 = m_KalKinFit->pfit(1);
    		m_lvFitPi02G1 = m_KalKinFit->pfit(2);
    		m_lvFitPi02G2 = m_KalKinFit->pfit(3);
    		m_lvFitPi01   = m_KalKinFit->pfit(0) + m_KalKinFit->pfit(1);
    		m_lvFitPi02   = m_KalKinFit->pfit(2) + m_KalKinFit->pfit(3);
    		// detector
    		m_lvPi01   = m_aUPi0List.Get4VecFromEMC(ptPi01->m_pTrack1,0.0) + m_aUPi0List.Get4VecFromEMC(ptPi01->m_pTrack2,0.0);
    		m_lvPi01G1 = m_aUPi0List.Get4VecFromEMC(ptPi01->m_pTrack1,0.0);
    		m_lvPi01G2 = m_aUPi0List.Get4VecFromEMC(ptPi01->m_pTrack2,0.0);
    		m_lvPi02   = m_aUPi0List.Get4VecFromEMC(ptPi02->m_pTrack1,0.0) + m_aUPi0List.Get4VecFromEMC(ptPi02->m_pTrack2,0.0);
    		m_lvPi02G1 = m_aUPi0List.Get4VecFromEMC(ptPi02->m_pTrack1,0.0);
    		m_lvPi02G2 = m_aUPi0List.Get4VecFromEMC(ptPi02->m_pTrack2,0.0);
    		m_aPhIndexList.clear();
   		  m_aPhIndexList.push_back (ptPi01->m_pTrack1);
    		m_aPhIndexList.push_back (ptPi01->m_pTrack2);
    		m_aPhIndexList.push_back (ptPi02->m_pTrack1);
    		m_aPhIndexList.push_back (ptPi02->m_pTrack2);
    		// the lines below MUSST stay below the settings of the 4vectors!
    		if (m_KalKinFit->Fit(1)) {m_dUPi01Chi2 = m_KalKinFit->chisq(1);} else {m_dUPi01Chi2 = -5;}
    		if (m_KalKinFit->Fit(2)) {m_dUPi02Chi2 = m_KalKinFit->chisq(2);} else {m_dUPi02Chi2 = -5;}			
				
        bDone = true;
      }
    }
		


if (!bDone) {m_dUTotalChi2 = -5;m_dUPi01Chi2 = -5; m_dUPi02Chi2 = -5; return false;}

//Fill particle containers
m_UPi01.Fill(&m_lvPi01);
m_UPi02.Fill(&m_lvPi02);
m_UGammas[0].Fill(&m_lvPi01G1);
m_UGammas[1].Fill(&m_lvPi01G2);
m_UGammas[2].Fill(&m_lvPi02G1);
m_UGammas[3].Fill(&m_lvPi02G2);
m_UFitPi01.Fill(&m_lvFitPi01);
m_UFitPi02.Fill(&m_lvFitPi02);
m_UFitGammas[0].Fill(&m_lvFitPi01G1);
m_UFitGammas[1].Fill(&m_lvFitPi01G2);
m_UFitGammas[2].Fill(&m_lvFitPi02G1);
m_UFitGammas[3].Fill(&m_lvFitPi02G2);

m_dU2pi0InvM=(m_lvPi01+m_lvPi02).m();
m_dUFit2pi0InvM=(m_lvFitPi01+m_lvFitPi02).m();
m_dUPi01InvM=m_lvPi01.m();
m_dUFitPi01InvM=m_lvFitPi01.m();
m_dUPi02InvM=m_lvPi02.m();
m_dUFitPi02InvM=m_lvFitPi02.m();

double TotEngBeam = m_dCMSEnergy/2 + m_dMasselec;
double Abs3MomBeam = sqrt(TotEngBeam*TotEngBeam - m_dMasselec*m_dMasselec);
  
m_lvBeamElectron.setPx(0);
m_lvBeamElectron.setPy(0);
m_lvBeamElectron.setPz(-Abs3MomBeam);
m_lvBeamElectron.setE(TotEngBeam);
  
m_lvBeamPositron.setPx(Abs3MomBeam*sin(m_dCrossingAngle));
m_lvBeamPositron.setPy(0);
m_lvBeamPositron.setPz(Abs3MomBeam*cos(m_dCrossingAngle));
m_lvBeamPositron.setE(TotEngBeam);

Hep3Vector vectboostCM=-((m_lvBeamElectron+m_lvBeamPositron).boostVector());	
m_lvBeamElectronCM=m_lvBeamElectron;
m_lvBeamElectronCM.boost(vectboostCM);
m_lvBeamPositronCM=m_lvBeamPositron;
m_lvBeamPositronCM.boost(vectboostCM);
m_lvPi01CM=m_lvPi01;
m_lvPi01CM.boost(vectboostCM);
m_lvPi02CM=m_lvPi02;
m_lvPi02CM.boost(vectboostCM);

m_dUSumPtCM=fabs(m_lvPi01CM.perp()+m_lvPi02CM.perp());

return true;
}
//----------------------------------------------------------------------------------------------------------
*/
