#include <TFile.h>
#include <TDirectory.h>
#include <TTree.h>
#include <TChain.h>
#include <TCanvas.h>
#include <TH1.h>
#include <TH2.h>
#include <THStack.h>
#include <TLine.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <TKey.h>
#include <TList.h>
#include <TStyle.h>
#include <TROOT.h>

using namespace std;

const double nanotofemto=1E6;

//pch = strtok (str," ,.-"); //tokenize string
vector<TFile*> files;
vector<TTree*> trees;
vector<double> scales;
vector<string> description;

string mode;
string treename;
string histodir;


vector <string> Tokenize(string str, char delim){
vector <string> strsplit;

stringstream ss;
string strtmp;

ss.str(str);				
		while (getline(ss, strtmp, delim)) {
 		strsplit.push_back(strtmp);
		}	

return strsplit;		
}

TChain* MakeChain(string filelist,string treename)
{
TChain* chain;
ifstream readList(filelist.c_str());
chain = new TChain(treename.c_str());

if(readList.is_open())
	{
	while(readList.good())
		{
		string strline;
		getline(readList,strline);
		cout << strline << endl;
		TFile* file=TFile::Open(strline.c_str());
		if(file==NULL) continue;
		chain->Add(strline.c_str());
		}
	}

return chain;
}

double GetCrossSection(string logfile,string generator)
{double XSec=1;

ifstream readList(logfile.c_str());

if(readList.is_open())
	{
	while(readList.good())
		{
		string strline;
		getline(readList,strline);
		//cout << strline << endl;
			if(generator=="Galuga")
				{
				if(strline.find("Total e+e- cross section [nb]") != string::npos)
					{
					stringstream ss;
					ss.str(strline);					
					string strtmp;
					vector <string> strsplit;
					while (getline(ss, strtmp, '=')) {
 						strsplit.push_back(strtmp);
						}
					
					XSec=atof((strsplit.back()).c_str());
					printf("XSEC = %e nb \n", XSec);
					//Convert to fb
					XSec=XSec*nanotofemto;
					printf("XSEC = %e fb \n", XSec);
					break;
					}
				}
		}
	}

return XSec;	
}


void GetNGenAndCrossSection(string logfile,string generator, int& NGen, double &XSec)
{

ifstream readList(logfile.c_str());

if(readList.is_open())
	{
	while(readList.good())
		{
		string strline;
		getline(readList,strline);
		//cout << strline << endl;
			if(generator=="Galuga")
				{
				if(strline.find("Total e+e- cross section [nb]") != string::npos)
					{
					stringstream ss;
					ss.str(strline);					
					string strtmp;
					vector <string> strsplit;
					while (getline(ss, strtmp, '=')) {
 						strsplit.push_back(strtmp);
						}
					
					XSec=atof((strsplit.back()).c_str());
					printf("XSEC = %e nb \n", XSec);
					//Convert to fb
					XSec=XSec*nanotofemto;
					printf("XSEC = %e fb \n", XSec);
					}
				if(strline.find("time to generate") != string::npos)
					{
					stringstream ss;
					ss.str(strline);					
					string strtmp;
					vector <string> strsplit;
					while (getline(ss, strtmp, ' ')) {
 						strsplit.push_back(strtmp);
						}
						
					//for(int k=0;k<strsplit.size();k++)
					//	{
					//	printf("%d %s \n",k, strsplit[k].c_str());
					//	}
				  
					NGen=atoi(strsplit[9].c_str());
					//cout << NGen << endl;
					//sleep(4);
					}
				}
		}
	}

}

void GetNGenAndCrossSectionFromList(string filelist,string generator, int& NGenSum, double &XSecMean)
{

ifstream readList(filelist.c_str());
int Nline=0;
if(readList.is_open())
	{
	while(readList.good())
		{
		Nline++;
		string strline;
		getline(readList,strline);
		int NGen;
		double XSec;
		GetNGenAndCrossSection(strline,generator,NGen,XSec);
		NGenSum=NGenSum+NGen;
		XSecMean=XSecMean+XSec;
		}
	}
	
XSecMean=XSecMean/Nline;	
printf("NGenSum= %d NLine=%d XSecMean = %f \n",NGenSum,Nline,XSecMean);	

}



int ParseInputFile(string inputfile,string mode)
{
ifstream readList(inputfile.c_str());
//Parse the input file
if(readList.is_open())
	{
	
	while(readList.good())
		{
		string strline[3];
		stringstream ss;
		string strtmp;
		vector<string> strsplit;
		
		TTree* treeMCTruth;
		
		
		//First line : ROOT file to read
		getline(readList,strline[0]);
		//Split the string
		/*
		char* CStrline=strdup(strline[0].c_str());
		char* splitCstr;
		//splitCstr = strtok (strline[0].c_str()," ");
		//splitCstr = strtok ("TA GUEULE"," ");
		splitCstr = strtok (CStrline," ");		
		//free(CStrline);
		*/
		
				
		ss.str(strline[0]);				
		while (getline(ss, strtmp, ' ')) {
 		strsplit.push_back(strtmp);
		}	
		
		
		//Check for inline arguments	
		for(int k=0;k<strsplit.size();k++) cout << strsplit[k] << endl;
		if(strsplit[1]=="Single")
			{
			files.push_back(TFile::Open(strsplit[2].c_str()));
			if(mode=="--NTuple"){
				TTree* tree = (TTree*)files.back()->Get(treename.c_str());
				trees.push_back(tree);			
				treeMCTruth = (TTree*)files.back()->Get("AllMCTruth");
				}
			}
			
		else if(strsplit[1]=="List")
			{
				if(mode=="--NTuple"){
				TChain* chain = MakeChain(strsplit[2],treename);
				TTree* tree = chain;
				trees.push_back(tree);
			
				TChain* chainMC = MakeChain(strsplit[2],"AllMCTruth");
				treeMCTruth = chainMC;
				}
			}	
		ss.clear();
		strsplit.clear();
		
		
		
		//Second Line : Normalization
		getline(readList,strline[1]);
		
		//Split the string
		ss.str(strline[1]);
		while (getline(ss, strtmp, ' ')) {
 		strsplit.push_back(strtmp);
		}
		//Check for inline arguments	
		if(strsplit[1]=="Factor")
			{
			scales.push_back(atof(strsplit[2].c_str()));
			}
		if(strsplit[1]=="ToLum")
			{
			double luminosity = atof(strsplit[2].c_str()); //luminosity in fb-1
			string generator=strsplit[4];
			string readfiletype=strsplit[5];
			string XSecfile=strsplit[6]; //File that contains cross section information	
			double XSec;
			int NMCTruth;
			double NScaled;
			double scale;		
				if(readfiletype=="Single")
					{
					//XSec = GetCrossSection(XSecfile,generator);
					GetNGenAndCrossSection(XSecfile,generator,NMCTruth,XSec);
					if(mode=="--NTuple"){
					NMCTruth = treeMCTruth->GetEntries();
					}
					else if (mode=="--Histo"){
					char hname[256];
					//sprintf hname("AllMCTruth/NumEvt",histodir.c_str());
					sprintf(hname,"%s/NumEvt",histodir.c_str());
					TH1D* hNumEvents = (TH1D*)files.back()->Get(hname);
					//NMCTruth = hNumEvents->GetEntries();
						}							
					}					
				else if(readfiletype=="List")
					{
					GetNGenAndCrossSectionFromList(XSecfile,generator,NMCTruth,XSec);
					}
			NScaled=XSec*luminosity;
			scale=((double)NScaled)/((double)NMCTruth);	
			printf("XSec : %f fb Luminosity : %f fb-1 NMCTruth : %d NScaled : %f Scale : %.2e \n", XSec,luminosity, NMCTruth, NScaled,scale);					
			scales.push_back(scale);
			}
			
		//Third Line description
		getline(readList,strline[2]);
		description.push_back(strline[2]);
		
		}
	}

return 0;
}
	
void DrawAnalysis(string inputfile, string option, string outputname)
{
string outps=outputname+".ps";
string outpdf=outputname+".pdf";

vector<string> strsplit = Tokenize(option,' ');
mode=strsplit[0];
if(mode=="--NTuple") treename=strsplit[1];
else if (mode=="--Histo") histodir= strsplit[1];
else {printf("Option invalid \n"); return;}

ParseInputFile(inputfile,mode);

/*
ifstream readList(inputfile.c_str());
//Parse the input file
if(readList.is_open())
	{
	
	while(readList.good())
		{
		string strline[3];
		stringstream ss;
		string strtmp;
		vector<string> strsplit;
		
		TTree* treeMCTruth;
		
		
		//First line : ROOT file to read
		getline(readList,strline[0]);
		//Split the string
		
		//char* CStrline=strdup(strline[0].c_str());
		//char* splitCstr;
		//splitCstr = strtok (CStrline," ");		
		//free(CStrline);
		
		
				
		ss.str(strline[0]);				
		while (getline(ss, strtmp, ' ')) {
 		strsplit.push_back(strtmp);
		}	
		
		
		//Check for inline arguments	
		for(int k=0;k<strsplit.size();k++) cout << strsplit[k] << endl;
		if(strsplit[1]=="Single")
			{
			files.push_back(TFile::Open(strsplit[2].c_str()));
			if(mode=="--NTuple"){
				TTree* tree = (TTree*)files.back()->Get(treename.c_str());
				trees.push_back(tree);			
				treeMCTruth = (TTree*)files.back()->Get("AllMCTruth");
				}
			}
			
		else if(strsplit[1]=="List")
			{
				if(mode=="--NTuple"){
				TChain* chain = MakeChain(strsplit[2],treename);
				TTree* tree = chain;
				trees.push_back(tree);
			
				TChain* chainMC = MakeChain(strsplit[2],"AllMCTruth");
				treeMCTruth = chainMC;
				}
			}	
		ss.clear();
		strsplit.clear();
		
		
		
		//Second Line : Normalization
		getline(readList,strline[1]);
		
		//Split the string
		ss.str(strline[1]);
		while (getline(ss, strtmp, ' ')) {
 		strsplit.push_back(strtmp);
		}
		//Check for inline arguments	
		if(strsplit[1]=="Factor")
			{
			scales.push_back(atof(strsplit[2].c_str()));
			}
		if(strsplit[1]=="ToLum")
			{
			double luminosity = atof(strsplit[2].c_str()); //luminosity in fb-1
			string generator=strsplit[4];
			string readfiletype=strsplit[5];
			string XSecfile=strsplit[6]; //File that contains cross section information
				if(readfiletype=="Single")
					{
					double XSec = GetCrossSection(XSecfile,generator);
					double NMCTruth;
					if(mode=="--NTuple"){
					NMCTruth = treeMCTruth->GetEntries();
					}
					else if (mode=="--Histo"){
					char hname[256];
					//sprintf hname("AllMCTruth/NumEvt",histodir.c_str());
					sprintf(hname,"%s/NumEvt",histodir.c_str());
					TH1D* hNumEvents = (TH1D*)files.back()->Get(hname);
					}
					double NScaled=XSec*luminosity;
					double scale=NScaled/NMCTruth;
					printf("XSec : %f fb Luminosity : %f fb-1 NMCTruth : %f NScaled : %f Scale : %f \n", XSec,luminosity, NMCTruth, NScaled,scale);
					scales.push_back(scale);
					}
			}
			
		//Third Line description
		getline(readList,strline[2]);
		description.push_back(strline[2]);
		
		}
	}
*/

//Draw distributions
char hname[64];
char drawcmd[512];
char drawopt[64];
char selection[512];
char title[512];
	
cout << files.size() << endl;
cout << trees.size() << endl;


TCanvas *c = new TCanvas();
c->Print(Form("%s[",outps.c_str()));
	
/*
THStack *hs = new THStack("hs","");

sprintf(hname,"hdeTFit2pi0InvM0");
sprintf(drawcmd,"deTFit2pi0InvM>>%s(100,0.26,4.)",hname);
sprintf(selection,"");

trees[0]->Draw(drawcmd,selection,"goff");	
TH1D* h0 = (TH1D*) gROOT->FindObject(hname);
//h0->Scale(scales[0]);
hs->Add(h0);

sprintf(hname,"hdeTFit2pi0InvM1");
sprintf(drawcmd,"deTFit2pi0InvM>>%s(100,0.26,4.)",hname);
sprintf(selection,"");

trees[1]->Draw(drawcmd,selection,"goff");	
TH1D* h1 = (TH1D*) gROOT->FindObject(hname);
//h1->Scale(scales[1]);
hs->Add(h1);

hs->Draw();
*/

/*
THStack *hs = new THStack("hs","");
vector<TH1D*> histos;	
for(int k=0;k<files.size();k++)
	{
	sprintf(hname,"hdeTFit2pi0InvM%d",k);
	sprintf(drawcmd,"deTFit2pi0InvM>>%s(100,0.26,4.)",hname);
	sprintf(selection,"");
	sprintf(drawopt,"goff");
	cout << drawcmd << endl;
	
	//Make the histogram
	//if(k==0) trees[k]->Draw(drawcmd,selection,"E goff");
	//else trees[k]->Draw(drawcmd,selection,"HIST goff");
	//if(k==0) trees[k]->Draw(drawcmd,selection,"goff");
	//else trees[k]->Draw(drawcmd,selection,"goff");
	
	trees[k]->Draw(drawcmd,selection,"goff");
	
	//Scale histograms
	//TH1D* hist = (TH1D*) gROOT->FindObject(hname);
	//cout << scales[k] << endl;
	histos.push_back((TH1D*) gROOT->FindObject(hname));
	histos.back()->Scale(scales[k]);
	
	//Put the histograms in THStack object
	hs->Add(histos[k]);
	
	}
	hs->Draw();
	gPad->SetLogy();
*/
	
/*
THStack *hs = new THStack("hs","");
vector<TH1D*> histos;	
for(int k=0;k<files.size();k++)
	{
	sprintf(hname,"hdeTFit2pi0InvM%d",k);
	sprintf(drawcmd,"deTFit2pi0InvM>>%s(100,0.26,4.)",hname);
	sprintf(selection,"");
	//sprintf(selection,"deTFitSumPtCM<0.05");
	sprintf(drawopt,"goff");
	cout << drawcmd << endl;
	
	//if(k==0) trees[k]->Draw(drawcmd,selection,"E");
	//else trees[k]->Draw(drawcmd,selection,"same HIST");
	
	//Make the histogram
	if(k==0) trees[k]->Draw(drawcmd,selection,"E goff");
	else trees[k]->Draw(drawcmd,selection,"HIST goff");
	
	//Scale histograms
	TH1D* hist = (TH1D*) gROOT->FindObject(hname);
	//cout << scales[k] << endl;
	hist->Scale(scales[k]);
	
	//Put the histograms in THStack object
	histos.push_back(hist);
	//hs->Add(hist);
	hs->Add(histos.back());
	
	//gPad->SetLogy();
	
	}
	hs->Draw();
*/
if(mode=="--NTuple"){
for(int i=0;i<3;i++)
	{
	if(i==0){	
		sprintf(selection,"");
		c->Divide(2,3);
		
		c->cd(1);
		for(int k=0;k<files.size();k++)
		{
		sprintf(hname,"hdeTCThetaHelicity%d",k);
		sprintf(drawcmd,"deTCThetaHelicity>>%s(100,-1.,1.)",hname);
		sprintf(drawopt,"goff");
	
		if(k==0) trees[k]->Draw(drawcmd,selection,"E");
		else trees[k]->Draw(drawcmd,selection,"same HIST");
		
		//Scale histograms
		TH1D* hist = (TH1D*) gROOT->FindObject(hname);
		hist->Scale(scales[k]);
		hist->SetLineColor(k+1);
		}
		gPad->SetLogy();
		c->cd(2);
		for(int k=0;k<files.size();k++)
		{
		sprintf(hname,"hdeTSumPtCM%d",k);
		sprintf(drawcmd,"deTSumPtCM>>%s(100,0.,1.)",hname);
		sprintf(drawopt,"goff");
	
		if(k==0) trees[k]->Draw(drawcmd,selection,"E");
		else trees[k]->Draw(drawcmd,selection,"same HIST");
		
		//Scale histograms
		TH1D* hist = (TH1D*) gROOT->FindObject(hname);
		hist->Scale(scales[k]);
		hist->SetLineColor(k+1);
		hist->SetMinimum(1);
		}
		gPad->SetLogy();
		
		c->cd(3);
		for(int k=0;k<files.size();k++)
		{
		sprintf(hname,"hdeTSumEemc%d",k);
		sprintf(drawcmd,"deTSumEemc>>%s(100,0.,6.)",hname);
		sprintf(drawopt,"goff");
	
		if(k==0) trees[k]->Draw(drawcmd,selection,"E");
		else trees[k]->Draw(drawcmd,selection,"same HIST");
		
		//Scale histograms
		TH1D* hist = (TH1D*) gROOT->FindObject(hname);
		hist->Scale(scales[k]);
		hist->SetLineColor(k+1);
		hist->SetMinimum(1);
		}
		gPad->SetLogy();
		
		c->cd(4);
		for(int k=0;k<files.size();k++)
		{
		sprintf(hname,"hdeTRGam%d",k);
		sprintf(drawcmd,"deTRGam>>%s(100,0.,1.)",hname);
		sprintf(drawopt,"goff");
	
		if(k==0) trees[k]->Draw(drawcmd,selection,"E");
		else trees[k]->Draw(drawcmd,selection,"same HIST");
		
		//Scale histograms
		TH1D* hist = (TH1D*) gROOT->FindObject(hname);
		hist->Scale(scales[k]);
		hist->SetLineColor(k+1);
		hist->SetMinimum(1);
		}
		gPad->SetLogy();
		c->cd(5);
		for(int k=0;k<files.size();k++)
		{
		sprintf(hname,"hdeT2pi0InvM%d",k);
		sprintf(drawcmd,"deT2pi0InvM>>%s(100,0.26,4.)",hname);
		sprintf(drawopt,"goff");
	
		if(k==0) trees[k]->Draw(drawcmd,selection,"E");
		else trees[k]->Draw(drawcmd,selection,"same HIST");
		
		//Scale histograms
		TH1D* hist = (TH1D*) gROOT->FindObject(hname);
		hist->Scale(scales[k]);
		hist->SetLineColor(k+1);
		}
		gPad->SetLogy();
	}
	
	if(i==1){	
		sprintf(selection,"");
		c->Divide(2,3);
		
		c->cd(1);
		for(int k=0;k<files.size();k++)
		{
		sprintf(hname,"hdeTFitCThetaHelicity%d",k);
		sprintf(drawcmd,"deTFitCThetaHelicity>>%s(100,-1.,1.)",hname);
		sprintf(drawopt,"goff");
	
		if(k==0) trees[k]->Draw(drawcmd,selection,"E");
		else trees[k]->Draw(drawcmd,selection,"same HIST");
		
		//Scale histograms
		TH1D* hist = (TH1D*) gROOT->FindObject(hname);
		hist->Scale(scales[k]);
		hist->SetLineColor(k+1);
		}
		gPad->SetLogy();
		c->cd(2);
		for(int k=0;k<files.size();k++)
		{
		sprintf(hname,"hdeTFitSumPtCM%d",k);
		sprintf(drawcmd,"deTFitSumPtCM>>%s(100,0.,1.)",hname);
		sprintf(drawopt,"goff");
	
		if(k==0) trees[k]->Draw(drawcmd,selection,"E");
		else trees[k]->Draw(drawcmd,selection,"same HIST");
		
		//Scale histograms
		TH1D* hist = (TH1D*) gROOT->FindObject(hname);
		hist->Scale(scales[k]);
		hist->SetLineColor(k+1);
		hist->SetMinimum(1);
		}
		gPad->SetLogy();
		
		c->cd(3);
		for(int k=0;k<files.size();k++)
		{
		sprintf(hname,"hdeTSumEemc%d",k);
		sprintf(drawcmd,"deTSumEemc>>%s(100,0.,6.)",hname);
		sprintf(drawopt,"goff");
	
		if(k==0) trees[k]->Draw(drawcmd,selection,"E");
		else trees[k]->Draw(drawcmd,selection,"same HIST");
		
		//Scale histograms
		TH1D* hist = (TH1D*) gROOT->FindObject(hname);
		hist->Scale(scales[k]);
		hist->SetLineColor(k+1);
		hist->SetMinimum(1);
		}
		gPad->SetLogy();
		
		c->cd(4);
		for(int k=0;k<files.size();k++)
		{
		sprintf(hname,"hdeTFitRGam%d",k);
		sprintf(drawcmd,"deTFitRGam>>%s(100,0.,1.)",hname);
		sprintf(drawopt,"goff");
	
		if(k==0) trees[k]->Draw(drawcmd,selection,"E");
		else trees[k]->Draw(drawcmd,selection,"same HIST");
		
		//Scale histograms
		TH1D* hist = (TH1D*) gROOT->FindObject(hname);
		hist->Scale(scales[k]);
		hist->SetLineColor(k+1);
		hist->SetMinimum(1);
		}
		gPad->SetLogy();
		c->cd(5);
		for(int k=0;k<files.size();k++)
		{
		sprintf(hname,"hdeTFit2pi0InvM%d",k);
		sprintf(drawcmd,"deTFit2pi0InvM>>%s(100,0.26,4.)",hname);
		sprintf(drawopt,"goff");
	
		if(k==0) trees[k]->Draw(drawcmd,selection,"E");
		else trees[k]->Draw(drawcmd,selection,"same HIST");
		
		//Scale histograms
		TH1D* hist = (TH1D*) gROOT->FindObject(hname);
		hist->Scale(scales[k]);
		hist->SetLineColor(k+1);
		}
		gPad->SetLogy();
	}
	
	if(i==2){	
		sprintf(selection,"deTSumEemc<3.5 && deTSumPtCM<0.3 && deTRGam<0.5");
		c->Divide(2,3);
		
		c->cd(1);
		for(int k=0;k<files.size();k++)
		{
		sprintf(hname,"hdeTCThetaHelicity%d",k);
		sprintf(drawcmd,"deTCThetaHelicity>>%s(100,-1.,1.)",hname);
		sprintf(drawopt,"goff");
	
		if(k==0) trees[k]->Draw(drawcmd,selection,"E");
		else trees[k]->Draw(drawcmd,selection,"same HIST");
		
		//Scale histograms
		TH1D* hist = (TH1D*) gROOT->FindObject(hname);
		hist->Scale(scales[k]);
		hist->SetLineColor(k+1);
		}
		gPad->SetLogy();
		c->cd(2);
		for(int k=0;k<files.size();k++)
		{
		sprintf(hname,"hdeTSumPtCM%d",k);
		sprintf(drawcmd,"deTSumPtCM>>%s(100,0.,1.)",hname);
		sprintf(drawopt,"goff");
	
		if(k==0) trees[k]->Draw(drawcmd,selection,"E");
		else trees[k]->Draw(drawcmd,selection,"same HIST");
		
		//Scale histograms
		TH1D* hist = (TH1D*) gROOT->FindObject(hname);
		hist->Scale(scales[k]);
		hist->SetLineColor(k+1);
		hist->SetMinimum(1);
		}
		gPad->SetLogy();
		
		c->cd(3);
		for(int k=0;k<files.size();k++)
		{
		sprintf(hname,"hdeTSumEemc%d",k);
		sprintf(drawcmd,"deTSumEemc>>%s(100,0.,6.)",hname);
		sprintf(drawopt,"goff");
	
		if(k==0) trees[k]->Draw(drawcmd,selection,"E");
		else trees[k]->Draw(drawcmd,selection,"same HIST");
		
		//Scale histograms
		TH1D* hist = (TH1D*) gROOT->FindObject(hname);
		hist->Scale(scales[k]);
		hist->SetLineColor(k+1);
		hist->SetMinimum(1);
		}
		gPad->SetLogy();
		
		c->cd(4);
		for(int k=0;k<files.size();k++)
		{
		sprintf(hname,"hdeTRGam%d",k);
		sprintf(drawcmd,"deTRGam>>%s(100,0.,1.)",hname);
		sprintf(drawopt,"goff");
	
		if(k==0) trees[k]->Draw(drawcmd,selection,"E");
		else trees[k]->Draw(drawcmd,selection,"same HIST");
		
		//Scale histograms
		TH1D* hist = (TH1D*) gROOT->FindObject(hname);
		hist->Scale(scales[k]);
		hist->SetLineColor(k+1);
		hist->SetMinimum(1);
		}
		gPad->SetLogy();
		c->cd(5);
		for(int k=0;k<files.size();k++)
		{
		sprintf(hname,"hdeT2pi0InvM%d",k);
		sprintf(drawcmd,"deT2pi0InvM>>%s(100,0.26,4.)",hname);
		sprintf(drawopt,"goff");
	
		if(k==0) trees[k]->Draw(drawcmd,selection,"E");
		else trees[k]->Draw(drawcmd,selection,"same HIST");
		
		//Scale histograms
		TH1D* hist = (TH1D*) gROOT->FindObject(hname);
		hist->Scale(scales[k]);
		hist->SetLineColor(k+1);
		}
		gPad->SetLogy();
	}
	
	c->Print(Form("%s",outps.c_str()));
	c->Clear();
	}
	
	c->Print(Form("%s]",outps.c_str()));
}

else if(mode=="--Histo"){
for(int i=0;i<=3;i++){
if(i==0){sprintf(hname,"%s/SumEemc",histodir.c_str());}
if(i==1){sprintf(hname,"%s/SumPt",histodir.c_str());}
if(i==2){sprintf(hname,"%s/2Pi0InvM",histodir.c_str());}


printf("hname : %s \n", hname);
TH1D* hSumMC;
for(int k=0;k<files.size();k++)
		{	
		printf("file : %s \n",files[k]->GetName());	
		Color_t color;
		if(k==0) {sprintf(drawopt,"goff E");color=1;}
		else {sprintf(drawopt,"goff HIST SAME");color=k+2;}
		
		//Scale histograms
		TH1D* hist = (TH1D*) files[k]->Get(hname);
		hist->Scale(scales[k]);
		hist->SetLineColor(color);
		hist->Draw(drawopt);
		
		if(k>=1){
			if(k==1) hSumMC=(TH1D*) hist->Clone();
			else hSumMC->Add(hist);
			}
		}
		if(files.size()>1) {hSumMC->SetLineColor(2);hSumMC->Draw("goff HIST SAME");}
		gPad->SetLogy();
		c->Print(Form("%s",outps.c_str()));
	}
c->Print(Form("%s]",outps.c_str()));
}
	
//Convert ps file into pdf with ps2pdf (can be properly view by evince)
//On Himster environment, pdf file created by ROOT is corrupted when opened with evince
//To view the pdf : evince filename.pdf
char systcmd[256];
sprintf(systcmd,"ps2pdf %s %s",outps.c_str(), outpdf.c_str());
system(systcmd);
sprintf(systcmd,"rm %s",outps.c_str());
system(systcmd);
	
}
