#include <TROOT.h>
#include <TStyle.h>
#include <iostream>     // std::cout
#include <fstream>      // std::ofstream


using namespace std;

#define NQ2 7
#define NM2PIONS 45

//Array of kinematic bin edges;
double limbin_Q2[NQ2+1];
double limbin_M2Pions[NM2PIONS+1];

void Init_KinBin(){   
//Define bin intervals  
limbin_Q2[0]=1.5;
limbin_M2Pions[0]=0.26;
   
for(int k=1;k<5;k++) limbin_Q2[k]=limbin_Q2[k-1]+(2.8-limbin_Q2[0])/4;
for(int k=5;k<=NQ2;k++) limbin_Q2[k] = limbin_Q2[k-1]+(5.1-2.8)/3;
  
for(int k=1;k<=NM2PIONS;k++) limbin_M2Pions[k]=limbin_M2Pions[k-1]+(2-limbin_M2Pions[0])/NM2PIONS;
  
}

void WriteBinTable(const char* outpath)
{double meanbin_Q2[NQ2];
double meanbin_M2Pions[NM2PIONS];

ofstream TableBinQ2;
ofstream TableBinM2Pions;

char outfile[512];

sprintf(outfile,"%slimbin_Q2.dat",outpath);
//cout << outfile << endl;

TableBinQ2.open(outfile);
for(int iQ2=0;iQ2<NQ2+1;iQ2++) 
{if(iQ2<NQ2) TableBinQ2 << limbin_Q2[iQ2] << endl; else TableBinQ2 << limbin_Q2[iQ2];}
TableBinQ2.close();

sprintf(outfile,"%slimbin_M2Pions.dat",outpath);
TableBinM2Pions.open(outfile);
for(int iM2Pions=0;iM2Pions<NM2PIONS+1;iM2Pions++) 
{if(iM2Pions<NM2PIONS)TableBinM2Pions << limbin_M2Pions[iM2Pions] << endl;else TableBinM2Pions << limbin_M2Pions[iM2Pions];}
TableBinM2Pions.close();


}
