#include <TFile.h>
#include <TDirectory.h>
#include <TTree.h>
#include <TChain.h>
#include <TCanvas.h>
#include <TPad.h>
#include <TH1.h>
#include <TH2.h>
#include <THStack.h>
#include <TLine.h>
#include <stdio.h>
#include <string.h>
#include <iostream> //std::cout
#include <sstream>
#include <fstream> //std::ifstream,ofstream
#include <vector>
#include <algorithm> //std::sort
#include <TKey.h>
#include <TList.h>
#include <TStyle.h>
#include <TROOT.h>
#include <TLegend.h>
#include <TLegendEntry.h>
#include <TLorentzVector.h>
#include <TVector3.h>

using namespace std;

const int NMax=256;
const double CrossingAngle= 0.011;
const double Masselec=511e-6;
int NpartGEN;
int IDGEN[NMax];
double PGEN[NMax], ThetaGEN[NMax], PhiGEN[NMax], EGEN[NMax]
			;
double TwoPi0InvMGEN;
double Q2eGEN,Q2pGEN;
double WGEN;
			
void lundToNTuple(string inputfile,string option, string outpath){

string outname=outpath+".root";
TFile* outROOT = new TFile(outname.c_str(),"RECREATE");

TTree* tree = new TTree("AllMCTruth","");

tree->Branch("NpartGEN",&NpartGEN,"NpartGEN/I");
tree->Branch("IDGEN",IDGEN,"IDGEN[NpartGEN]/I");
tree->Branch("PGEN",PGEN,"PGEN[NpartGEN]/D");
tree->Branch("ThetaGEN",ThetaGEN,"ThetaGEN[NpartGEN]/D");
tree->Branch("PhiGEN",PhiGEN,"PhiGEN[NpartGEN]/D");
tree->Branch("EGEN",EGEN,"EGEN[NpartGEN]/D");
if(option.find("GamGam ")!=string::npos){
tree->Branch("Q2eGEN",&Q2eGEN,"Q2eGEN/D");
tree->Branch("Q2pGEN",&Q2pGEN,"Q2pGEN/D");
tree->Branch("WGEN",&WGEN,"WGEN/D");
}
if(option.find("2Pi ")!=string::npos){
tree->Branch("2Pi0InvMGEN",&TwoPi0InvMGEN,"2Pi0InvMGEN/D");
}

ifstream readFile(inputfile.c_str());

if(readFile.is_open()){
	while(readFile.good()){
		//Load informations from input file
		int nPart=0;
		readFile >> nPart;
		
		NpartGEN=nPart;
		double ECMS=0;
		for(int k=0;k<nPart;k++){
			int partID;
			double Px,Py,Pz,E;
		
			readFile >> partID >> Px >> Py >> Pz >> E;
			TLorentzVector lv(0.,0.,0.,0.);
			lv.SetPxPyPzE(Px,Py,Pz,E);
		
			IDGEN[k]=partID;
			PGEN[k]=lv.Rho();
			ThetaGEN[k]=lv.Theta();
			PhiGEN[k]=lv.Phi();
			EGEN[k]=lv.E();
			ECMS=ECMS+lv.E();
		}

		double Abs3MomBeam = sqrt((pow(ECMS,2)/2-pow(Masselec,2))/(1-cos(TMath::Pi()-CrossingAngle)));
		double TotEngBeam = sqrt(pow(Masselec,2)+pow(Abs3MomBeam,2));
		
		TLorentzVector lvBeamElectron,lvBeamPositron;		
		lvBeamElectron.SetPx(Abs3MomBeam*sin(CrossingAngle));
		lvBeamElectron.SetPy(0);
		lvBeamElectron.SetPz(-Abs3MomBeam*cos(CrossingAngle));
		lvBeamElectron.SetE(TotEngBeam);

		lvBeamPositron.SetPx(Abs3MomBeam*sin(CrossingAngle));
		lvBeamPositron.SetPy(0);
		lvBeamPositron.SetPz(Abs3MomBeam*cos(CrossingAngle));
		lvBeamPositron.SetE(TotEngBeam);

		if(option.find("GamGam ")!=string::npos){
			int nElec=0; int nPositron=0;
			TLorentzVector lvelec;
			TLorentzVector lvpositron;
			TLorentzVector lvHadronicSystem;
			for(int k=0;k<nPart;k++){
				TLorentzVector lv(0.,0.,0.,0.);	
				TVector3 vec3(0.,0.,0.);
				vec3.SetMagThetaPhi(PGEN[k],ThetaGEN[k],PhiGEN[k]);
				lv.SetPxPyPzE(vec3.Px(),vec3.Py(),vec3.Pz(),EGEN[k]);
				if(IDGEN[k]==11){nElec++;lvelec=lv;}
				if(IDGEN[k]==-11){nPositron++;lvpositron=lv;}
				if(IDGEN[k]==0){lvHadronicSystem=lv;}
			}
			if(nElec==1 && nPositron==1){
				Q2eGEN=-(lvBeamElectron-lvelec).M2();
				Q2pGEN=-(lvBeamPositron-lvpositron).M2();
			}
			WGEN=lvHadronicSystem.M();
		}
		
		if(option.find("2Pi ")!=string::npos){
			int NPi=0;
			vector< TLorentzVector> veclvPi;
			for(int k=0;k<nPart;k++){
				TLorentzVector lv(0.,0.,0.,0.);	
				TVector3 vec3(0.,0.,0.);
				vec3.SetMagThetaPhi(PGEN[k],ThetaGEN[k],PhiGEN[k]);
				lv.SetPxPyPzE(vec3.Px(),vec3.Py(),vec3.Pz(),EGEN[k]);
				if(IDGEN[k]==111 || fabs(IDGEN[k])==211) {
					NPi++; 				
					veclvPi.push_back(lv);
				}
			}
			if(veclvPi.size()==2) TwoPi0InvMGEN=(veclvPi[0]+veclvPi[1]).M();
		 }
		
		tree->Fill();
	}
}
outROOT->Write();
return;
}