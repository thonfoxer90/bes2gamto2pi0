#include <TFile.h>
#include <TDirectory.h>
#include <TTree.h>
#include <TChain.h>
#include <TCanvas.h>
#include <TPad.h>
#include <TH1.h>
#include <TH2.h>
#include <THStack.h>
#include <TLine.h>
#include <stdio.h>
#include <string.h>
#include <iostream> //std::cout
#include <sstream>
#include <fstream> //std::ifstream,ofstream
#include <vector>
#include <algorithm> //std::sort
#include <TKey.h>
#include <TList.h>
#include <TStyle.h>
#include <TROOT.h>
#include <TLegend.h>
#include <TLegendEntry.h>

#define SSTR( x ) static_cast< std::ostringstream & >( \
        ( std::ostringstream() << std::dec << x ) ).str()
				
using namespace std;

const double nanotofemto=1E6;
const double nanotopico=1E3;

string m_option;

vector <string> Tokenize(string str, char delim){
vector <string> strsplit;

stringstream ss;
string strtmp;

ss.str(str);				
		while (getline(ss, strtmp, delim)) {
 		strsplit.push_back(strtmp);
		}	

return strsplit;		
}


double GetCrossSection(string logfile,string generator)
{double XSec=1;

ifstream readList(logfile.c_str());

if(readList.is_open())
	{
	while(readList.good())
		{
		string strline;
		getline(readList,strline);
		//cout << strline << endl;
			if(generator=="Galuga")
				{
				if(strline.find("Total e+e- cross section [nb]") != string::npos)
					{
					stringstream ss;
					ss.str(strline);					
					string strtmp;
					vector <string> strsplit;
					while (getline(ss, strtmp, '=')) {
 						strsplit.push_back(strtmp);
						}
					
					XSec=atof((strsplit.back()).c_str());
					printf("XSEC = %e nb \n", XSec);
					//Convert to pb
					XSec=XSec*nanotopico;
					printf("XSEC = %e pb \n", XSec);
					break;
					}
				}
				
		}
	}

return XSec;	
}


void GetNGenAndCrossSection(string logfile,string generator, int& NGen, double &XSec)
{
ifstream readList(logfile.c_str());

if(readList.is_open())
	{
	while(readList.good())
		{
		string strline;
		getline(readList,strline);
		//cout << strline << endl;
			if(generator=="Galuga")
				{
				if(strline.find("Total e+e- cross section [nb]") != string::npos)
					{
					stringstream ss;
					ss.str(strline);					
					string strtmp;
					vector <string> strsplit;
					while (getline(ss, strtmp, '=')) {
 						strsplit.push_back(strtmp);
						}
					
					XSec=atof((strsplit.back()).c_str());
					printf("XSEC = %e nb \n", XSec);
					//Convert to pb
					XSec=XSec*nanotopico;
					printf("XSEC = %e pb \n", XSec);
					}
				if(strline.find("time to generate") != string::npos)
					{
					stringstream ss;
					ss.str(strline);					
					string strtmp;
					vector <string> strsplit;
					while (getline(ss, strtmp, ' ')) {
 						strsplit.push_back(strtmp);
						}
					
					int vecidx_events=0;	
					for(int k=0;k<strsplit.size();k++)
						{
						//printf("%d %s \n",k, strsplit[k].c_str());
						if(strsplit[k].find("events") != string::npos) vecidx_events=k; 
						}
				  
					NGen=atoi(strsplit[vecidx_events-1].c_str());
					//NGen=atoi(strsplit[9].c_str());
					//cout << NGen << endl;
					//sleep(2);
					
					
					}
				}
			if(generator=="Ekhara")
				{
				int NOccurences=0; //Number of occurences of same string
				
				if(strline.find("[nb]") != string::npos)
					{
					NOccurences++;
					if(NOccurences>1) continue;
					stringstream ss;
					ss.str(strline);					
					string strtmp;
					vector <string> strsplit;
					while (getline(ss, strtmp, '=')) {
 						strsplit.push_back(strtmp);
						}
					
					ss.clear();
					ss.str(strsplit.back());
					printf("GetCrossSection : %s \n",strsplit.back().c_str());
					vector <string> strvalsplit;
					while(getline(ss,strtmp, '+')){
					strvalsplit.push_back(strtmp);
					}
					XSec=atof((strvalsplit[0]).c_str());
					printf("XSEC = %e nb \n", XSec);
					//Convert to pb
					XSec=XSec*nanotopico;
					printf("XSEC = %e pb \n", XSec);
					}
				if(strline.find("Events output:") != string::npos)
					{
					stringstream ss;
					ss.str(strline);					
					string strtmp;
					vector <string> strsplit;
					while (getline(ss, strtmp, ':')) {
 						strsplit.push_back(strtmp);
						}
					cout << strsplit.back() << endl;
					NGen=atoi(strsplit.back().c_str());	
					}
				}
		}
	}

}

void GetNGenAndCrossSectionFromList(string filelist,string generator, int& NGenSum, double &XSecMean)
{
cout << "Read cross section from file list." << endl;
ifstream readList(filelist.c_str());
int Nline=0;
if(readList.is_open())
	{
	while(readList.good())
		{
		Nline++;
		string strline;
		getline(readList,strline);
		int NGen;
		double XSec;
		cout << NGenSum<< endl;
		GetNGenAndCrossSection(strline,generator,NGen,XSec);		
		NGenSum=NGenSum+NGen;		
		XSecMean=XSecMean+XSec;
		cout << NGenSum<< endl;
		}
	}
	
XSecMean=XSecMean/Nline;	
printf("NGenSum= %d NLine=%d XSecMean = %f \n",NGenSum,Nline,XSecMean);	

}

vector<string> GetSubDirectoryNames(TFile* file,string prefix){
vector<string> subdirnames;
TIter nextkey(file->GetListOfKeys());
TKey *key;
while ((key = (TKey*)nextkey())) {
	TObject *obj = (TObject*)key->ReadObj();
  cout << "Reading: " << obj->GetName() << " " << obj->ClassName()<< endl;
	string stlObjNameobj(obj->GetName());	
	//if(prefix==stlObjNameobj){
	if(obj->InheritsFrom("TDirectory") && prefix==stlObjNameobj){
		//Look for subdirectories
		TDirectory* dir = (TDirectory*) obj;
		dir->cd();
		TIter subnextkey(gDirectory->GetListOfKeys());
		TKey *subkey;
		while ((subkey = (TKey*)subnextkey())) {
			TObject *subobj = (TObject*)subkey->ReadObj();
			cout << "	Reading: " << subobj->GetName() << " " << subobj->ClassName()<< endl;
			if(subobj->InheritsFrom("TDirectory")){
			TDirectory* subdir = (TDirectory*) subobj;
			cout << subdir->GetPath() << endl;
			string str(subdir->GetPath());
			vector<string> substr=Tokenize(str,':');
			subdirnames.push_back(substr[1]);
			}
		}
	}
}

file->cd();

return subdirnames;
}

void normalizeToLum(string inputstr, string option, string outpath){
gROOT->SetBatch();



//Parse the option file
m_option=option;
bool isScalewLum=false;
string type="Histo";
string dirCategory="Tagged";
double scale=0;

TFile* file;

if(inputstr.find(".txt") != string::npos){
	//List of .root files
}
else{ 
	//Single root file
	file = TFile::Open(inputstr.c_str());	
}

string outROOTname = outpath+".root";
TFile* outfile;
if(outROOTname==inputstr) outfile = new TFile(outROOTname.c_str(),"UPDATE");
else outfile = new TFile(outROOTname.c_str(),"RECREATE");

vector <string> optionsplit = Tokenize(m_option,' ');
for(int k=0;k<optionsplit.size();k++){
	cout << optionsplit[k] << endl;
	vector <string> strsplit = Tokenize(optionsplit[k],'=');
	string arg=strsplit[0];
	string val=strsplit[1];
	cout << arg << " " << val << endl;
	cout << endl;
	if(arg.find("Type") != string::npos){
		type=val;
	}
	if(arg.find("Dir") != string::npos){
		dirCategory=val;
	}
	if(arg.find("Scale") != string::npos){
		scale = atof(val.c_str());	
	}
	if(arg.find("Lum") != string::npos){
		isScalewLum=true;
	}
	
}

if(isScalewLum){
cout << "Scaling with luminosity" << endl;
double luminosity; //luminosity in pb-1
string generator;
string readfiletype;
string XSecfile; //File that contains cross section information	
double XSec=0.;
int NMCTruth=0;
double NScaled=0.;
			
	for(int k=0;k<optionsplit.size();k++){
		vector <string> strsplit = Tokenize(optionsplit[k],'=');
		string arg=strsplit[0];
		string val=strsplit[1];
		if(arg.find("Lum") != string::npos){
		luminosity=atof(val.c_str());
		}
		if(arg.find("Gen") != string::npos){
		generator=val;
		}
		if(arg.find("XSecFile") != string::npos){
		XSecfile=val;			
		}
	}

if(XSecfile.find(".root")!= string::npos){
	char hname[256];
	sprintf (hname,"MCTruth/NumEvt");
	TH1D* hNumEvents = (TH1D*)file->Get(hname);		
	NMCTruth=hNumEvents->GetEntries();
}
else if(XSecfile.find(".txt")!= string::npos){
	GetNGenAndCrossSectionFromList(XSecfile,generator,NMCTruth,XSec);
}
				
NScaled=XSec*luminosity;
scale=((double)NScaled)/((double)NMCTruth);	
printf("XSec : %f pb Luminosity : %f pb-1 NMCTruth : %d NScaled : %f Scale : %.2e \n", XSec,luminosity, NMCTruth, NScaled,scale);
			
}

if(type=="NTuple"){
	TTree* tree;
	/*
	if(dirCategory=="Tagged") tree = (TTree*)file->Get("Tagged");
	if(tree==NULL) return;
	string treeHEADname=string(tree->GetName())+"HEAD";
	TTree* treeHEAD = new TTree(treeHEADname.c_str(),tree->GetTitle());
	treeHEAD->Branch("scale",&scale,"scale/D");
	treeHEAD->Fill();
	*/
	
	string treeHEADname=dirCategory+"HEAD";
	TTree* treeHEAD = new TTree(treeHEADname.c_str(),"");
	treeHEAD->Branch("scale",&scale,"scale/D");
	treeHEAD->Fill();
	
	outfile->Write();
}

if(type=="Histo"){
	vector<string> histodirs;	
	
	if(dirCategory=="Tagged"){
	histodirs.push_back("Tagged");
	histodirs.push_back("eTagged");
	histodirs.push_back("pTagged");
	}
	
	for(int k=0;k<histodirs.size();k++){
		TDirectory* directory= file->GetDirectory(histodirs[k].c_str());
		TDirectory* outdir = outfile->mkdir(histodirs[k].c_str());
		TIter nextkey(directory->GetListOfKeys());
		TKey *key;
		while ((key = (TKey*)nextkey())) {
			TObject *obj = (TObject*)key->ReadObj();
			cout << "Reading: " << obj->GetName() << " " << obj->ClassName()<< endl;
			outfile->cd(histodirs[k].c_str());
			
			if(obj->InheritsFrom("TH1D")) {TH1D* hist=(TH1D*) obj; hist->Scale(scale);hist->Write();}
			if(obj->InheritsFrom("TH2D")) {TH2D* hist=(TH2D*) obj; hist->Scale(scale);hist->Write();}
		if(obj->InheritsFrom("TDirectory")){
				
				TDirectory* subdirectory= (TDirectory*)obj;
				string strpath(subdirectory->GetPath());								
				vector<string> strpath_semicolonsplit = Tokenize(strpath,':');				
				vector<string> strpath_slashsplit = Tokenize(strpath_semicolonsplit.back(),'/');
				outdir->cd();
				TDirectory* outsubdir= outdir->mkdir((strpath_slashsplit.back()).c_str());
				cout << strpath_slashsplit.back() << endl;
				TIter nextsubkey(subdirectory->GetListOfKeys());
				TKey *subkey;
				while ((subkey = (TKey*)nextsubkey())) {
					TObject *subobj = (TObject*)subkey->ReadObj();
					outsubdir->cd();
					if(subobj->InheritsFrom("TH1D")) {TH1D* hist=(TH1D*) subobj; hist->Scale(scale);hist->Write();}
					if(subobj->InheritsFrom("TH2D")) {TH2D* hist=(TH2D*) subobj; hist->Scale(scale);hist->Write();}
					
				}
				
			}
			
		outfile->cd();
		
		}
	}
	
outfile->Write();	
}

}

void normalizeToLum(string inputcard){
ifstream readList(inputcard.c_str());
if(readList.is_open()){
		while(readList.good()){
		string strline;
		string inputfile;
		string option;
		string outpath;
		
		getline(readList,inputfile);
		getline(readList,option);
		getline(readList,outpath);
		
		cout << inputfile << endl;
		cout << option << endl;
		cout <<outpath << endl; 
		normalizeToLum(inputfile, option, outpath);	
		}	
}

return;
}



