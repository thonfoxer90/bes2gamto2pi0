#include <TROOT.h>
#include <TChain.h>
#include <TRint.h>
#include <TTree.h>
#include <TFile.h>
#include <TStyle.h>
#include <TString.h>
#include <TSelector.h>
#include <TF1.h>
#include <TH2.h>
#include <TH1.h>
#include <THnSparse.h>
#include <TVirtualIndex.h>
#include <iostream>             // std::cout, std::endl
#include <fstream>              // std::ifstream
#include <sstream>  
#include <TPad.h>
#include <TCanvas.h>
#include <TGaxis.h>
#include <TPave.h>
#include <TPaveText.h>
#include <TLatex.h>
#include <TVector3.h>
#include <TLorentzVector.h>
#include <TSystem.h>
#include <cstdlib>
#include <map>
#include <time.h>

#include "MyGlobal.cxx"

double m_Luminosity=0;
string m_DataFileName("");
string m_EffRecFileName("");
const double picoToFemto = 1e3;
const double picoToNano = 1e-3;
const double BF_Pi0to2Gam=0.98823;
void crossSection(string inputcard, string outpath){
ifstream readInput(inputcard.c_str());

if(readInput.is_open()){
	while(readInput.good()){
		string strline;
		getline(readInput,strline);
		
		vector<string> strsplit = MyGlobal::Tokenize(strline,' ');
		for(int k=0;k<strsplit.size();k++){
			vector<string> vecstr = MyGlobal::Tokenize(strsplit[k],'=');	
			string arg = vecstr[0];
			string val = vecstr[1];
			if(arg=="Luminosity") m_Luminosity=atof(val.c_str());
			if(arg=="DataSubtr") m_DataFileName=val;
			if(arg=="EffRec") m_EffRecFileName=val;
		}
	}
	
}

TFile* fileData;
fileData = TFile::Open(m_DataFileName.c_str());
if(fileData==NULL){cout << "****** CANNOT OPEN DATA FILE ******" <<endl;return;}
fileData->ls();
map<string, THnSparseD*> maphnDDataSubtr;
maphnDDataSubtr["eT"]=(THnSparseD*)fileData->Get("hnDeTSubtr");
maphnDDataSubtr["pT"]=(THnSparseD*)fileData->Get("hnDpTSubtr");
for (std::map<string, THnSparseD*>::iterator it=maphnDDataSubtr.begin(); it!=maphnDDataSubtr.end(); ++it){
	if(it->second==NULL){
		cout << "CANNOT ACCESS DATA THN" << endl; return;
	}
}

TFile* fileEffRec;
fileEffRec = TFile::Open(m_EffRecFileName.c_str());
if(fileEffRec==NULL){cout << "****** CANNOT OPEN RECONSTRUCTION EFFICIENCY FILE ******" << endl; return;}
fileEffRec->ls();
map<string,THnSparseD*> maphnDGEN;
maphnDGEN["eT"] =(THnSparseD*)fileEffRec->Get("hnDeTGEN");
maphnDGEN["pT"] =(THnSparseD*)fileEffRec->Get("hnDpTGEN");

map<string,THnSparseD*> maphnDREC;
maphnDREC["eT"] =(THnSparseD*)fileEffRec->Get("hnDeTREC");
maphnDREC["pT"] =(THnSparseD*)fileEffRec->Get("hnDpTREC");

map<string,THnSparseD*> maphnDEff;
maphnDEff["eT"] =(THnSparseD*)fileEffRec->Get("hnDeTEff"); 
maphnDEff["pT"] =(THnSparseD*)fileEffRec->Get("hnDpTEff");

for (std::map<string, THnSparseD*>::iterator it=maphnDEff.begin(); it!=maphnDEff.end(); ++it){
	if(it->second==NULL){
		cout << "CANNOT ACCESS Efficency THN" << endl; return;
	}
}

string outROOTname=outpath+".root";
TFile* outROOT;
outROOT = new TFile(outROOTname.c_str(),"RECREATE");

map<string, THnSparseD*> maphnDYieldCorrEff;
for (std::map<string, THnSparseD*>::iterator it=maphnDDataSubtr.begin(); it!=maphnDDataSubtr.end(); ++it){
	string key = it->first;
	maphnDYieldCorrEff[key] = (THnSparseD*) maphnDDataSubtr[key]->Clone();
}

const Int_t nDim=maphnDDataSubtr["eT"]->GetNdimensions();

for (std::map<string, THnSparseD*>::iterator it=maphnDDataSubtr.begin(); it!=maphnDDataSubtr.end(); ++it){
	string key=it->first;
	THnSparseD* hnDEff = maphnDEff[key];
	/*
	//Error calculation is wrong.
	for(Long64_t bin=0;bin<maphnDDataSubtr[key]->GetNbins();bin++){
		int bincoord[nDim];
		double yield=maphnDDataSubtr[key]->GetBinContent(bin,bincoord);
		double yieldErr=maphnDDataSubtr[key]->GetBinError(bincoord);
		
		double effRec=maphnDEff[key]->GetBinContent(bincoord);
		double effRecErr=maphnDEff[key]->GetBinError(bincoord);
		printf("%d %d %d %f %f %f %f \n",bincoord[0],bincoord[1],bincoord[2],yield,yieldErr,effRec,effRecErr);
		
		//Clear THn
		maphnDYieldCorrEff[key]->SetBinContent(bincoord,0);
		maphnDYieldCorrEff[key]->SetBinError(bincoord,0);
		
		if(yield>0 && effRec==0) {cout << "THn Data filled, No efficiency" << endl;}
		if(effRec==0)continue;
		double yieldCorrEff=yield/effRec;
		double yieldCorrEffErr=(1/pow(effRec,2))*pow(yieldErr,2)+(pow(yield,2)*pow(effRecErr,2))/(pow(effRec,4));
		maphnDYieldCorrEff[key]->SetBinContent(bincoord,yieldCorrEff);
		maphnDYieldCorrEff[key]->SetBinError(bincoord,yieldCorrEff);		
	}
	*/
	maphnDYieldCorrEff[key]->Divide(maphnDDataSubtr[key],maphnDEff[key]);
}

maphnDYieldCorrEff["All"]=(THnSparseD*)maphnDYieldCorrEff["eT"]->Clone();
maphnDYieldCorrEff["All"]->Add(maphnDYieldCorrEff["pT"]);
maphnDYieldCorrEff["All"]->SetName("hnDYieldCorrEff");

maphnDDataSubtr["All"]=(THnSparseD*)maphnDDataSubtr["eT"]->Clone();
maphnDDataSubtr["All"]->Add(maphnDDataSubtr["pT"]);
maphnDDataSubtr["All"]->SetName("hnDSubtr");

maphnDREC["All"]=(THnSparseD*)maphnDREC["eT"]->Clone();
maphnDREC["All"]->Add(maphnDREC["pT"]);
maphnDREC["All"]->SetName("hnDREC");

vector<string> axisNames;
map<int,double> mapBinMins;
map<int,double> mapBinMaxs;

for(int i=0;i<maphnDYieldCorrEff["All"]->GetNdimensions();i++){
cout << maphnDYieldCorrEff["All"]->GetAxis(i)->GetName() << endl;
axisNames.push_back(maphnDYieldCorrEff["All"]->GetAxis(i)->GetName());
//string key=axisNames.back();
int key=i;
mapBinMins[key]=maphnDYieldCorrEff["All"]->GetAxis(i)->GetFirst();
mapBinMaxs[key]=maphnDYieldCorrEff["All"]->GetAxis(i)->GetLast();
}


outROOT->cd();
//XSec Abs(cos Theta Star)  in Q2,M2Pi bins
for (std::map<string, THnSparseD*>::iterator it=maphnDYieldCorrEff.begin(); it!=maphnDYieldCorrEff.end(); ++it){
string key = it->first;
for(int binU=mapBinMins[0];binU<=mapBinMaxs[0];binU++){
	for(int binV=mapBinMins[2];binV<=mapBinMaxs[2];binV++){
		string UName; UName=string(maphnDYieldCorrEff[key]->GetAxis(0)->GetName());
		string VName; VName=string(maphnDYieldCorrEff[key]->GetAxis(2)->GetName());
		
		string UTitle; UTitle=string(maphnDYieldCorrEff[key]->GetAxis(0)->GetTitle());
		string VTitle; VTitle=string(maphnDYieldCorrEff[key]->GetAxis(2)->GetTitle());
		
		THnSparseD* hnD_Restrict = (THnSparseD*) maphnDYieldCorrEff[key]->Clone();
		hnD_Restrict->GetAxis(0)->SetRange(binU,binU);
		hnD_Restrict->GetAxis(2)->SetRange(binV,binV);
		
		THnSparseD* hnDData_Restrict = (THnSparseD*) maphnDDataSubtr[key]->Clone();
		hnDData_Restrict->GetAxis(0)->SetRange(binU,binU);
		hnDData_Restrict->GetAxis(2)->SetRange(binV,binV);
		
		double UMin=hnD_Restrict->GetAxis(0)->GetBinLowEdge(binU);
		double UMax=hnD_Restrict->GetAxis(0)->GetBinUpEdge(binU);
		double deltaU=(hnD_Restrict->GetAxis(0)->GetBinUpEdge(binU))
									-(hnD_Restrict->GetAxis(0)->GetBinLowEdge(binU));
		double VMin=hnD_Restrict->GetAxis(2)->GetBinLowEdge(binV);
		double VMax=hnD_Restrict->GetAxis(2)->GetBinUpEdge(binV);				
		double deltaV=(hnD_Restrict->GetAxis(2)->GetBinUpEdge(binV))
									-(hnD_Restrict->GetAxis(2)->GetBinLowEdge(binV));
																
		TH1D* hproj=(TH1D*) hnD_Restrict->Projection(1);
		hproj->Scale(1/((m_Luminosity)*deltaU*deltaV*BF_Pi0to2Gam*BF_Pi0to2Gam),"width");
		string hname;
		hname="dsigmad"+axisNames[1]+key+"_"
					+axisNames[0]+axisNames[2]+"_"
					+MyGlobal::intToStr(binU-1)+"-"+MyGlobal::intToStr(binV-1);
		hproj->SetName(hname.c_str());			
		string htitle;
		htitle=MyGlobal::doubleToStr(UMin,2)+"#leq "+ string(hnD_Restrict->GetAxis(0)->GetTitle())+"< "+MyGlobal::doubleToStr(UMax,2)+", "
					 +MyGlobal::doubleToStr(VMin,2)+"#leq "+ string(hnD_Restrict->GetAxis(2)->GetTitle())+"< "+MyGlobal::doubleToStr(VMax,2);
		hproj->SetTitle(htitle.c_str());
		string hXtitle;
		hXtitle=string(maphnDYieldCorrEff[key]->GetAxis(1)->GetTitle());
		hproj->GetXaxis()->SetTitle(hXtitle.c_str());
		string hYtitle;		
		hYtitle="d^{3}#sigma_{ee}/dQ^{2}d|cos #theta*|dW (pb/(GeV^{3}/c^{2}))";
		hproj->GetYaxis()->SetTitle(hYtitle.c_str());
		hproj->Write();
		
		hproj=(TH1D*) hnDData_Restrict->Projection(1);
		hname=axisNames[1]+key+"_"
					+axisNames[0]+axisNames[2]+"_"
					+MyGlobal::intToStr(binU-1)+"-"+MyGlobal::intToStr(binV-1);
		hproj->SetName(hname.c_str());			
		hproj->SetTitle(htitle.c_str());
		hproj->Write();					 			
	}
}
}

//M2Pi distribution
for (std::map<string, THnSparseD*>::iterator it=maphnDYieldCorrEff.begin(); it!=maphnDYieldCorrEff.end(); ++it){
	string key = it->first;
	THnSparseD* hnD_Restrict = (THnSparseD*) maphnDYieldCorrEff[key]->Clone();
	TH1D* hproj;														
	hproj=(TH1D*) hnD_Restrict->Projection(2);
	hproj->Scale(1/((m_Luminosity)*BF_Pi0to2Gam*BF_Pi0to2Gam),"width");
	string hname;
		hname="dsigmad"+axisNames[2]+key;
		hproj->SetName(hname.c_str());			
		string htitle("");
		hproj->SetTitle(htitle.c_str());
		string hXtitle;
		hXtitle=string(maphnDYieldCorrEff[key]->GetAxis(2)->GetTitle());
		hproj->GetXaxis()->SetTitle(hXtitle.c_str());
		string hYtitle;		
		hYtitle="d#sigma_{ee}/dW (pb/(GeV/c^{2}))";
		hproj->GetYaxis()->SetTitle(hYtitle.c_str());
		hproj->Write();
}

//M2Pi distributions  in Q2,Abs(cos Theta Star) bins
for (std::map<string, THnSparseD*>::iterator it=maphnDYieldCorrEff.begin(); it!=maphnDYieldCorrEff.end(); ++it){
string key = it->first;
for(int binU=mapBinMins[0];binU<=mapBinMaxs[0];binU++){
	for(int binV=mapBinMins[1];binV<=mapBinMaxs[1];binV++){
		string UName; UName=string(maphnDYieldCorrEff[key]->GetAxis(0)->GetName());
		string VName; VName=string(maphnDYieldCorrEff[key]->GetAxis(1)->GetName());
		
		string UTitle; UTitle=string(maphnDYieldCorrEff[key]->GetAxis(0)->GetTitle());
		string VTitle; VTitle=string(maphnDYieldCorrEff[key]->GetAxis(1)->GetTitle());
		
		THnSparseD* hnD_Restrict = (THnSparseD*) maphnDYieldCorrEff[key]->Clone();
		hnD_Restrict->GetAxis(0)->SetRange(binU,binU);
		hnD_Restrict->GetAxis(1)->SetRange(binV,binV);
		
		THnSparseD* hnDData_Restrict = (THnSparseD*) maphnDDataSubtr[key]->Clone();
		hnDData_Restrict->GetAxis(0)->SetRange(binU,binU);
		hnDData_Restrict->GetAxis(1)->SetRange(binV,binV);
		
		double UMin=hnD_Restrict->GetAxis(0)->GetBinLowEdge(binU);
		double UMax=hnD_Restrict->GetAxis(0)->GetBinUpEdge(binU);
		double deltaU=(hnD_Restrict->GetAxis(0)->GetBinUpEdge(binU))
									-(hnD_Restrict->GetAxis(0)->GetBinLowEdge(binU));
		double VMin=hnD_Restrict->GetAxis(1)->GetBinLowEdge(binV);
		double VMax=hnD_Restrict->GetAxis(1)->GetBinUpEdge(binV);				
		double deltaV=(hnD_Restrict->GetAxis(1)->GetBinUpEdge(binV))
									-(hnD_Restrict->GetAxis(1)->GetBinLowEdge(binV));
		
		TH1D* hproj;														
		hproj=(TH1D*) hnD_Restrict->Projection(2);
		hproj->Scale(1/((m_Luminosity)*deltaU*deltaV*BF_Pi0to2Gam*BF_Pi0to2Gam),"width");
		string hname;
		hname="dsigmad"+axisNames[2]+key+"_"
					+axisNames[0]+axisNames[1]+"_"
					+MyGlobal::intToStr(binU-1)+"-"+MyGlobal::intToStr(binV-1);
		hproj->SetName(hname.c_str());			
		string htitle;
		htitle=MyGlobal::doubleToStr(UMin,2)+"#leq "+ string(hnD_Restrict->GetAxis(0)->GetTitle())+"< "+MyGlobal::doubleToStr(UMax,2)+", "
					 +MyGlobal::doubleToStr(VMin,2)+"#leq "+ string(hnD_Restrict->GetAxis(1)->GetTitle())+"< "+MyGlobal::doubleToStr(VMax,2);
		hproj->SetTitle(htitle.c_str());
		string hXtitle;
		hXtitle=string(maphnDYieldCorrEff[key]->GetAxis(2)->GetTitle());
		hproj->GetXaxis()->SetTitle(hXtitle.c_str());
		string hYtitle;		
		hYtitle="d^{3}#sigma_{ee}/dQ^{2}d|cos #theta*|dW (pb/(GeV^{3}/c^{2}))";
		hproj->GetYaxis()->SetTitle(hYtitle.c_str());
		hproj->Write();
		
		hproj=(TH1D*) hnDData_Restrict->Projection(2);
		hname=axisNames[2]+key+"_"
					+axisNames[0]+axisNames[1]+"_"
					+MyGlobal::intToStr(binU-1)+"-"+MyGlobal::intToStr(binV-1);
		hproj->SetName(hname.c_str());
		hproj->SetTitle(htitle.c_str());
		hproj->Write();
		
		if(key=="eT" || key=="pT"){
			THnSparseD* hnDEff_Restrict = (THnSparseD*) maphnDEff[key]->Clone();
			hnDEff_Restrict->GetAxis(0)->SetRange(binU,binU);
			hnDEff_Restrict->GetAxis(1)->SetRange(binV,binV);
			
			hproj=(TH1D*) hnDEff_Restrict->Projection(2);
			hname="Eff"+axisNames[2]+key+"_"
					+axisNames[0]+axisNames[1]+"_"
					+MyGlobal::intToStr(binU-1)+"-"+MyGlobal::intToStr(binV-1);
			hproj->SetName(hname.c_str());
			hproj->SetTitle(htitle.c_str());
			hproj->Write();
		}							 			
	}
}
}

maphnDDataSubtr["eT"]->Write();
maphnDDataSubtr["pT"]->Write();
maphnDDataSubtr["All"]->Write();
maphnDGEN["eT"]->Write();
maphnDGEN["pT"]->Write();
maphnDREC["eT"]->Write();
maphnDREC["pT"]->Write();
maphnDREC["All"]->Write();
maphnDEff["eT"]->Write();
maphnDEff["pT"]->Write();
maphnDYieldCorrEff["All"]->Write();
return;
}