#include <TFile.h>
#include <TDirectory.h>
#include <TTree.h>
#include <TChain.h>
#include <TCanvas.h>
#include <TPad.h>
#include <TH1.h>
#include <TH2.h>
#include <THStack.h>
#include <TLine.h>
#include <stdio.h>
#include <string.h>
#include <iostream> //std::cout
#include <sstream>
#include <fstream> //std::ifstream,ofstream
#include <vector>
#include <algorithm> //std::sort
#include <TKey.h>
#include <TList.h>
#include <TStyle.h>
#include <TROOT.h>
#include <TLegend.h>
#include <TLegendEntry.h>

#include "MyGlobal.cxx"

using namespace std;

vector <string> m_files;
vector <string> m_tags;
vector < vector <double > > m_yields;

int getNumberOfSubDir(string inputfile,string dir, string keyword){
int nDirs=0;

TFile* file = TFile::Open(inputfile.c_str());

TDirectory* directory= file->GetDirectory(dir.c_str());

TIter nextkey(directory->GetListOfKeys());
TKey *key;
while ((key = (TKey*)nextkey())) {
      TObject *obj = (TObject*)key->ReadObj();
			string objname(obj->GetName());
			if (obj->InheritsFrom("TDirectory")){
				if(objname.find(keyword)!=string::npos){
					nDirs++;
				}
			}	
		}

return nDirs;		
}

void makeYieldTable(string inputcard, string option, string outpath){

string dir=option;

ifstream readList(inputcard.c_str());
if(readList.is_open()){
	while(readList.good()){
		string str;
		getline(readList,str);
		TFile* file = TFile::Open(str.c_str());
		if(file==NULL) continue;
		m_files.push_back(str);
		getline(readList,str);
		m_tags.push_back(str);
	}
}

m_yields.resize(m_files.size());

for(int i=0;i<m_yields.size();i++){
	int nStages=getNumberOfSubDir(string(m_files[i]),dir, "Stage");
	
	m_yields[i].resize(nStages);
	for(int j=0;j<m_yields[i].size();j++){
		TFile* file = TFile::Open(m_files[i].c_str());
		string path=dir+"/Stage"+MyGlobal::intToStr(j);
		file->cd(path.c_str());
		
		TH1D* hNumEvts = (TH1D*) gDirectory->Get("NumEvt");
		if(hNumEvts==NULL ) m_yields[i][j]=0;
		else m_yields[i][j] = (double) hNumEvts->Integral();
		//cout << m_yields[i][j] << endl;
	}
	
}

//Write to output file
string outname=outpath+".txt";
ofstream writeTable(outname.c_str());

//Write header
string strHeader("	");
for(int k=0;k<m_tags.size();k++) {
	if(k==m_tags.size()-1) strHeader=strHeader+m_tags[k];
	else strHeader=strHeader+m_tags[k]+"	";
}
writeTable << strHeader<<endl;
for(int i=0;i<m_yields[0].size();i++){
	string strline;
	strline = "Stage"+MyGlobal::intToStr(i)+" ";
	for(int j=0;j<m_yields.size();j++){
		strline=strline+MyGlobal::doubleToStr(m_yields[j][i])+" ";
	}
writeTable << strline <<endl;
}

}