

#include "Bes2Gamto2Pi0/Bes2Gamto2Pi0.h"
#include <sstream>

#define SSTR( x ) dynamic_cast< std::ostringstream & >( \
            ( std::ostringstream() << std::dec << x ) ).str()

using namespace Event;

///////////////////////////////////////////////////////////////////////////
//
//Analysis for e+ e- -> 2pi0 pi+ pi- gamma (ISR) at BES-III
//Author: Martin Ripka, Institut fuer Kernphysik, Uni Mainz, Germany 
//
///////////////////////////////////////////////////////////////////////////
const double DOF4PIGT = 15; // 7*3 - 6
const double DOF4PIGU = 18; // 7*3 - 3
const double DOF4PI   = 12; // 6*3 - 6

const double DOF5PIGT = 20; // 9*3 - 7
const double DOF5PIGU = 23; // 9*3 - 4
const double DOF5PI   = 17; // 8*3 - 7

const double DOF5PIU  = 21; // 8*3 - 3

const double DOF3PIGT = 10; // 5*3 - 5
const double DOF3PIGU = 13; // 5*3 - 2
const double DOF3PI   =  7; // 4*3 - 5

//----------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------
// class Bes2Gamto2Pi0
//----------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------
Bes2Gamto2Pi0::Bes2Gamto2Pi0(const std::string& name, ISvcLocator* pSvcLocator) :
   Algorithm(name, pSvcLocator)
//----------------------------------------------------------------------------------------------------------
{
  //m_dCMSEnergy         = 3.773;       // psi(3770)
  m_dMassPi0           = 0.134976;    // PDG 2010
  m_dMassPiCharged     = 0.139570;    // PDG 2010
  m_dISRcutLow         = 0.025;
  m_dEoPcut            = 1.2;//0.8;
  m_dPi0GcutHigh       = 2.5;//1.4;
  m_dPhEnergyCutBarrel = 0.025;
  m_dPhEnergyCutEndCap = 0.05;
  m_dMaxPi0Combs       = 20;
  m_dMaxGoodPh         = 15;
  m_dCylRad            = 4;//2.5;  // for point of closest approach
  m_dCylHeight         = 20;//15.0; // for point of closest approach
  m_dPi0MassCutLow     = 0.10;
  m_dPi0MassCutHigh    = 0.16;
  m_dCosThetaMax       = 0.92;
  m_dCosThetaBarrel    = 0.83;
  m_dMaxTime           = 14;  // *50ns
  m_dCosThetaISR       = 0.95;//0.83;
  //m_lvBoost            = HepLorentzVector(0.0415,0,0,m_dCMSEnergy);
  m_lvBoost            = HepLorentzVector(m_dCMSEnergy*sin(0.011),0,0,m_dCMSEnergy);
  m_dChisqMaxValue     = 200;
  m_bMCrecorded        = false;
}
//----------------------------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------------------------
StatusCode Bes2Gamto2Pi0::initialize()
//----------------------------------------------------------------------------------------------------------
{
    cout << "initialize begins" << endl;
    MsgStream log(msgSvc(), name());
    log << MSG::INFO << "in initialize()" << endreq;

    m_iAll        = 0;
    m_iPreSkip    = 0;
    m_iPhotonSkip = 0;
    m_iPionSkip   = 0;
    m_iPi0Skip    = 0;
    m_iUFitSkip   = 0;
    m_iAcceptedU  = 0;

    m_KalKinFit  = KalmanKinematicFit::instance();
    m_Vtxfit     = VertexFit::instance();
    m_PID        = ParticleID::instance();

    // Initialize ROOT tree and declare NTuples
    NTuplePtr ut(ntupleSvc(), "FILE88/Untagged");
    NTuplePtr tg(ntupleSvc(), "FILE88/Tagged");
    NTuplePtr ut5(ntupleSvc(), "FILE88/5PiUntagged");
    NTuplePtr tg5(ntupleSvc(), "FILE88/5PiTagged");
    NTuplePtr pi5n(ntupleSvc(), "FILE88/5Pi");
    NTuplePtr pi4n(ntupleSvc(), "FILE88/4Pi");

    if   ( ut ) {m_NUntagged = ut;}
    else {m_NUntagged = ntupleSvc()->book("FILE88/Untagged", CLID_ColumnWiseTuple, "untagged");}
    if   ( tg ) {m_NTagged = tg;}
    else {m_NTagged = ntupleSvc()->book("FILE88/Tagged", CLID_ColumnWiseTuple, "tagged");}
    if   ( ut5 ) {m_N5PiUntagged = ut5;}
    else {m_N5PiUntagged = ntupleSvc()->book("FILE88/5PiUntagged", CLID_ColumnWiseTuple, "5Piuntagged");}
    if   ( tg5 ) {m_N5PiTagged = tg5;}
    else {m_N5PiTagged = ntupleSvc()->book("FILE88/5PiTagged", CLID_ColumnWiseTuple, "5Pitagged");}
    if   ( pi5n ) {m_N5Pi = pi5n;}
    else {m_N5Pi = ntupleSvc()->book("FILE88/5Pi", CLID_ColumnWiseTuple, "5Pi");}
    if   ( pi4n ) {m_N4Pi = pi4n;}
    else {m_N4Pi = ntupleSvc()->book("FILE88/4Pi", CLID_ColumnWiseTuple, "4Pi");}

  if ( m_NUntagged ) {
     m_NUntagged->addItem("URunNumber"    ,m_dURunNumber);
     m_NUntagged->addItem("UEvtNumber"    ,m_dUEvtNumber);
     m_NUntagged->addItem("UCMSEnergy"    ,m_dUCMSEnergy);
     m_NUntagged->addItem("iUAnzGoodP"    ,m_iUAnzGoodP);
     m_NUntagged->addItem("iUAnzGoodM"    ,m_iUAnzGoodM);
     m_NUntagged->addItem("iUAnzGoodGamma",m_iUAnzGoodGamma);
     m_NUntagged->addItem("iUAnzPi0Tracks",m_iUAnzPi0Tracks);
     m_NUntagged->addItem("iUAnz2Combs"  ,m_iUAnz2Combs);
     m_NUntagged->addItem("iUAnz3Combs"  ,m_iUAnz3Combs);
     m_NUntagged->addItem("UTotalChi2"   ,m_dUTotalChi2);
     m_NUntagged->addItem("UPi01Chi2"    ,m_dUPi01Chi2);
     m_NUntagged->addItem("UPi02Chi2"    ,m_dUPi02Chi2);
     m_NUntagged->addItem("UMHadTagged"  ,m_dUMHadTagged);
     m_NUntagged->addItem("UsqrSHadSys"  ,m_dUsqrSHadSys);
     m_NUntagged->addItem("UsqrSHadSysFit",m_dUsqrSHadSysFit);
     m_NUntagged->addItem("UDistZp"      ,m_dUDistZp);
     m_NUntagged->addItem("UDistRp"      ,m_dUDistRp);
     m_NUntagged->addItem("UDistZm"      ,m_dUDistZm);
     m_NUntagged->addItem("UDistRm"      ,m_dUDistRm);
     m_NUntagged->addItem("iUBgrVeto"    ,m_iUBgrVeto);
     m_NUntagged->addItem("iUmcTruePi"   ,m_iUmcTruePi);
     m_NUntagged->addItem("iUmcTruePi0"  ,m_iUmcTruePi0);
     m_NUntagged->addItem("iUmcTrueISR"  ,m_iUmcTrueISR);
     m_NUntagged->addItem("iUmcTrueFSR"  ,m_iUmcTrueFSR);
     m_NUntagged->addItem("iUmcTrueGammaFromJpsi"  ,m_iUmcTrueGammaFromJpsi);
     m_NUntagged->addItem("iUmcTrueOtherGamma"  ,m_iUmcTrueOtherGamma);
     m_NUntagged->addItem("iUmcTrueOther" ,m_iUmcTrueOther);
     m_NUntagged->addItem("UmcMHad"       ,m_dUmcMHad);
     m_NUntagged->addItem("UmcMHad5pi"    ,m_dUmcMHad5pi);
     m_UPi01    .AttachToNtuple(m_NUntagged,"UPi01");
     m_UPi02    .AttachToNtuple(m_NUntagged,"UPi02");
     m_UPi1     .AttachToNtuple(m_NUntagged,"UPi1");
     m_UPi2     .AttachToNtuple(m_NUntagged,"UPi2");
     m_TaggedISR.AttachToNtuple(m_NUntagged,"UTagISR",true);
     m_UFitPi01    .AttachToNtuple(m_NUntagged,"UFitPi01");
     m_UFitPi02    .AttachToNtuple(m_NUntagged,"UFitPi02");
     m_UFitPi1     .AttachToNtuple(m_NUntagged,"UFitPi1");
     m_UFitPi2     .AttachToNtuple(m_NUntagged,"UFitPi2");
     m_UTrueISR    .AttachToNtuple(m_NUntagged,"TrueISR");
     for (int a=0;a<GAMMAS+5;a++) {m_UGammas   [a].AttachToNtuple(m_NUntagged,"UG"   +SSTR(a)+"emc",true); m_NUntagged->addItem("ClosestTr"+SSTR(a),m_dUClosestTrack[a]);}
     for (int a=0;a<5;a++)        {m_UFitGammas[a].AttachToNtuple(m_NUntagged,"UFitG"+SSTR(a),true);}
     m_NUntagged->addItem("UPi1EoP"          ,m_dUPi1EoP);
     m_NUntagged->addItem("UPi2EoP"          ,m_dUPi2EoP);
     m_NUntagged->addItem("UPi1MucDepth"     ,m_dUPi1MucDepth);
     m_NUntagged->addItem("UPi2MucDepth"     ,m_dUPi2MucDepth);
     m_NUntagged->addItem("U4PiNonRadChi2"   ,m_dU4PiNonRadChi2);
     m_NUntagged->addItem("UVtxChi2"  ,m_dUVtxChi2);
     m_NUntagged->addItem("VtxPosX"   ,m_dUVtxPosX);
     m_NUntagged->addItem("VtxPosY"   ,m_dUVtxPosY);
     m_NUntagged->addItem("VtxPosZ"   ,m_dUVtxPosZ);
     m_NUntagged->addItem("IPposX"    ,m_dUIPposX);
     m_NUntagged->addItem("IPposY"    ,m_dUIPposY);
     m_NUntagged->addItem("IPposZ"    ,m_dUIPposZ);
     // 5pi
     //m_NUntagged->addItem("5PiNonRadChi2",m_dU5PiNonRadChi2);
     m_NUntagged->addItem("5PiChi2"      ,m_dU5PiChi2);
     m_NUntagged->addItem("5PiMhad"      ,m_dU5PiMhad);
     m_U5PiISR.AttachToNtuple(m_NUntagged,"5PiISR");
     // Kskpi veto
     m_NUntagged->addItem("UProb1Pi"      ,m_dUProb1Pi);
     m_NUntagged->addItem("UProb1K"       ,m_dUProb1K);
     m_NUntagged->addItem("UProb2Pi"      ,m_dUProb2Pi);
     m_NUntagged->addItem("UProb2K"       ,m_dUProb2K);
     m_NUntagged->addItem("UProb1P"       ,m_dUProb1P);
     m_NUntagged->addItem("UProb2P"       ,m_dUProb2P);

  }
   else {
      log << MSG::ERROR << "  Cannot book N-tuple88 untagged: " << long(m_NUntagged) << endreq;
      return StatusCode::FAILURE;
   }
   if ( m_NTagged ) {
     m_NTagged->addItem("RunNumber"    ,m_dRunNumber);
     m_NTagged->addItem("EvtNumber"    ,m_dEvtNumber);
     m_NTagged->addItem("CMSEnergy"    ,m_dTCMSEnergy);
     m_NTagged->addItem("iAnzGoodGamma",m_iAnzGoodGamma);
     m_NTagged->addItem("iAnzGoodP"    ,m_iAnzGoodP);
     m_NTagged->addItem("iAnzGoodM"    ,m_iAnzGoodM);
     m_NTagged->addItem("iAnzPi0Tracks",m_iAnzPi0Tracks);
     m_NTagged->addItem("iAnz2Combs"  ,m_iAnz2Combs);
     m_NTagged->addItem("iAnz3Combs"  ,m_iAnz3Combs);
     m_NTagged->addItem("TotalChi2"   ,m_dTotalChi2);
     m_NTagged->addItem("Pi01Chi2"    ,m_dPi01Chi2);
     m_NTagged->addItem("Pi02Chi2"    ,m_dPi02Chi2);   
     m_NTagged->addItem("sqrSHadSys"  ,m_dsqrSHadSys);
     m_NTagged->addItem("sqrSHadSysFit",m_dsqrSHadSysFit);
     m_NTagged->addItem("DistZp"      ,m_dDistZp);
     m_NTagged->addItem("DistRp"      ,m_dDistRp);
     m_NTagged->addItem("DistZm"      ,m_dDistZm);
     m_NTagged->addItem("DistRm"      ,m_dDistRm);
     m_NTagged->addItem("iBgrVeto"    ,m_iBgrVeto);
     m_NTagged->addItem("imcTruePi"   ,m_imcTruePi);
     m_NTagged->addItem("imcTruePi0"  ,m_imcTruePi0);
     m_NTagged->addItem("imcTrueISR"  ,m_imcTrueISR);
     m_NTagged->addItem("imcTrueFSR"  ,m_imcTrueFSR);
     m_NTagged->addItem("imcTrueGammaFromJpsi"  ,m_imcTrueGammaFromJpsi);
     m_NTagged->addItem("imcTrueOtherGamma"  ,m_imcTrueOtherGamma);
     m_NTagged->addItem("imcTrueOther"  ,m_imcTrueOther);
     m_NTagged->addItem("mcMHad"      ,m_dmcMHad);
     m_NTagged->addItem("mcMHad5pi"       ,m_dmcMHad5pi);
     m_Pi01    .AttachToNtuple(m_NTagged,"Pi01");
     m_Pi02    .AttachToNtuple(m_NTagged,"Pi02");
     m_Pi1     .AttachToNtuple(m_NTagged,"Pi1");
     m_Pi2     .AttachToNtuple(m_NTagged,"Pi2");
     m_FitPi01    .AttachToNtuple(m_NTagged,"FitPi01");
     m_FitPi02    .AttachToNtuple(m_NTagged,"FitPi02");
     m_FitPi1     .AttachToNtuple(m_NTagged,"FitPi1");
     m_FitPi2     .AttachToNtuple(m_NTagged,"FitPi2");
     m_TrueISR    .AttachToNtuple(m_NTagged,"TrueISR");
     for (int a=0;a<GAMMAS+5;a++) {m_Gammas   [a].AttachToNtuple(m_NTagged,"G"   +SSTR(a)+"emc",true); m_NTagged->addItem("ClosestTr"+SSTR(a),m_dClosestTrack[a]);}
     for (int a=0;a<5;a++)        {m_FitGammas[a].AttachToNtuple(m_NTagged,"FitG"+SSTR(a),true);}
     m_NTagged->addItem("Pi1EoP"          ,m_dPi1EoP);
     m_NTagged->addItem("Pi2EoP"          ,m_dPi2EoP);
     m_NTagged->addItem("Pi1MucDepth"     ,m_dPi1MucDepth);
     m_NTagged->addItem("Pi2MucDepth"     ,m_dPi2MucDepth);
     m_NTagged->addItem("4PiNonRadChi2"   ,m_d4PiNonRadChi2);
     m_NTagged->addItem("VtxChi2"   ,m_dVtxChi2);
     m_NTagged->addItem("VtxPosX"   ,m_dVtxPosX);
     m_NTagged->addItem("VtxPosY"   ,m_dVtxPosY);
     m_NTagged->addItem("VtxPosZ"   ,m_dVtxPosZ);
     m_NTagged->addItem("IPposX"    ,m_dIPposX);
     m_NTagged->addItem("IPposY"    ,m_dIPposY);
     m_NTagged->addItem("IPposZ"    ,m_dIPposZ);
     // 5pi
     //m_NTagged->addItem("5PiNonRadChi2",m_d5PiNonRadChi2);
     m_NTagged->addItem("5PiChi2"      ,m_d5PiChi2);
     m_NTagged->addItem("5PiMhad"      ,m_d5PiMhad);
     m_5PiISR.AttachToNtuple(m_NTagged,"5PiISR");
     // Kskpi veto
     m_NTagged->addItem("Prob1Pi"      ,m_dProb1Pi);
     m_NTagged->addItem("Prob1K"       ,m_dProb1K);
     m_NTagged->addItem("Prob2Pi"      ,m_dProb2Pi);
     m_NTagged->addItem("Prob2K"       ,m_dProb2K);
     m_NTagged->addItem("Prob1P"       ,m_dProb1P);
     m_NTagged->addItem("Prob2P"       ,m_dProb2P);

   }
   else {
      log << MSG::ERROR << "  Cannot book N-tuple88 tagged: " << long(m_NTagged) << endreq;
      return StatusCode::FAILURE;
   }

   if ( m_N5PiUntagged ) {
      m_N5PiUntagged->addItem("CMSEnergy"     ,m_d5UCMSEnergy);
      m_N5PiUntagged->addItem("RunNumber"     ,m_d5URunNumber);
      m_N5PiUntagged->addItem("EvtNumber"     ,m_d5UEvtNumber);
      m_N5PiUntagged->addItem("iAnzGoodP"     ,m_i5UAnzGoodP);
      m_N5PiUntagged->addItem("iAnzGoodM"     ,m_i5UAnzGoodM);
      m_N5PiUntagged->addItem("iAnzGoodGamma" ,m_i5UAnzGoodGamma);
      m_N5PiUntagged->addItem("iAnzPi0Tracks" ,m_i5UAnzPi0Tracks);
      m_N5PiUntagged->addItem("iAnz2Combs"    ,m_i5UAnz2Combs);
      m_N5PiUntagged->addItem("iAnz3Combs"    ,m_i5UAnz3Combs);
      m_N5PiUntagged->addItem("TotalChi2"     ,m_d5UTotalChi2);
      m_N5PiUntagged->addItem("Pi01Chi2"      ,m_d5UPi01Chi2);
      m_N5PiUntagged->addItem("Pi02Chi2"      ,m_d5UPi02Chi2);
      m_N5PiUntagged->addItem("Pi03Chi2"      ,m_d5UPi03Chi2);
      m_N5PiUntagged->addItem("sqrSHadSysFit" ,m_d5UsqrSHadSysFit);
      m_N5PiUntagged->addItem("DistZp"        ,m_d5UDistZp);
      m_N5PiUntagged->addItem("DistRp"        ,m_d5UDistRp);
      m_N5PiUntagged->addItem("DistZm"        ,m_d5UDistZm);
      m_N5PiUntagged->addItem("DistRm"        ,m_d5UDistRm);
      m_N5PiUntagged->addItem("iBgrVeto"      ,m_i5UBgrVeto);
      m_N5PiUntagged->addItem("imcTruePi"     ,m_i5UmcTruePi);
      m_N5PiUntagged->addItem("imcTruePi0"    ,m_i5UmcTruePi0);
      m_N5PiUntagged->addItem("imcTrueISR"    ,m_i5UmcTrueISR);
      m_N5PiUntagged->addItem("imcTrueGammaFromJpsi"  ,m_i5UmcTrueGammaFromJpsi);
      m_N5PiUntagged->addItem("imcTrueOtherGamma"  ,m_i5UmcTrueOtherGamma);
      m_N5PiUntagged->addItem("imcTrueOther" ,m_i5UmcTrueOther);
      m_N5PiUntagged->addItem("mcMHad"       ,m_d5UmcMHad);
      m_5UPi01    .AttachToNtuple(m_N5PiUntagged,"Pi01");
      m_5UPi02    .AttachToNtuple(m_N5PiUntagged,"Pi02");
      m_5UPi03    .AttachToNtuple(m_N5PiUntagged,"Pi03");
      m_5UPi1     .AttachToNtuple(m_N5PiUntagged,"Pi1");
      m_5UPi2     .AttachToNtuple(m_N5PiUntagged,"Pi2");
      m_5UFitPi01    .AttachToNtuple(m_N5PiUntagged,"FitPi01");
      m_5UFitPi02    .AttachToNtuple(m_N5PiUntagged,"FitPi02");
      m_5UFitPi03    .AttachToNtuple(m_N5PiUntagged,"FitPi03");
      m_5UFitPi1     .AttachToNtuple(m_N5PiUntagged,"FitPi1");
      m_5UFitPi2     .AttachToNtuple(m_N5PiUntagged,"FitPi2");
      m_5UTrueISR    .AttachToNtuple(m_N5PiUntagged,"TrueISR");
      for (int a=0;a<GAMMAS+6;a++) {m_5UGammas   [a].AttachToNtuple(m_N5PiUntagged,"UG"   +SSTR(a)+"emc",true); m_N5PiUntagged->addItem("ClosestTr"+SSTR(a),m_d5UClosestTrack[a]);}
      for (int a=0;a<7;a++)        {m_5UFitGammas[a].AttachToNtuple(m_N5PiUntagged,"UFitG"+SSTR(a),true);}
      m_N5PiUntagged->addItem("U5PiNonRadChi2"   ,m_dU5PiNonRadChi2);
      m_N5PiUntagged->addItem("VtxChi2"   ,m_d5UVtxChi2);
      m_N5PiUntagged->addItem("VtxPosX"   ,m_d5UVtxPosX);
      m_N5PiUntagged->addItem("VtxPosY"   ,m_d5UVtxPosY);
      m_N5PiUntagged->addItem("VtxPosZ"   ,m_d5UVtxPosZ);
      m_N5PiUntagged->addItem("IPposX"    ,m_d5UIPposX);
      m_N5PiUntagged->addItem("IPposY"    ,m_d5UIPposY);
      m_N5PiUntagged->addItem("IPposZ"    ,m_d5UIPposZ);
      // Kskpi veto
      m_N5PiUntagged->addItem("Prob1Pi"      ,m_d5UProb1Pi);
      m_N5PiUntagged->addItem("Prob1K"       ,m_d5UProb1K);
      m_N5PiUntagged->addItem("Prob2Pi"      ,m_d5UProb2Pi);
      m_N5PiUntagged->addItem("Prob2K"       ,m_d5UProb2K);
      m_N5PiUntagged->addItem("UProb1P"      ,m_d5UProb1P);
      m_N5PiUntagged->addItem("UProb2P"      ,m_d5UProb2P);

    }
    else {
       log << MSG::ERROR << "  Cannot book N-tuple88 untagged: " << long(m_NUntagged) << endreq;
       return StatusCode::FAILURE;
    }
    if ( m_N5PiTagged ) {
      m_N5PiTagged->addItem("CMSEnergy"    ,m_d5TCMSEnergy);
      m_N5PiTagged->addItem("RunNumber"    ,m_d5RunNumber);
      m_N5PiTagged->addItem("EvtNumber"    ,m_d5EvtNumber);
      m_N5PiTagged->addItem("iAnzGoodP"    ,m_i5AnzGoodP);
      m_N5PiTagged->addItem("iAnzGoodM"    ,m_i5AnzGoodM);
      m_N5PiTagged->addItem("iAnzGoodGamma",m_i5AnzGoodGamma);
      m_N5PiTagged->addItem("iAnzPi0Tracks",m_i5AnzPi0Tracks);
      m_N5PiTagged->addItem("iAnz2Combs"   ,m_i5Anz2Combs);
      m_N5PiTagged->addItem("iAnz3Combs"   ,m_i5Anz3Combs);
      m_N5PiTagged->addItem("TotalChi2"    ,m_d5TotalChi2);
      m_N5PiTagged->addItem("Pi01Chi2"     ,m_d5Pi01Chi2);
      m_N5PiTagged->addItem("Pi02Chi2"     ,m_d5Pi02Chi2);
      m_N5PiTagged->addItem("Pi03Chi2"     ,m_d5Pi03Chi2);
      m_N5PiTagged->addItem("sqrSHadSysFit",m_d5sqrSHadSysFit);
      m_N5PiTagged->addItem("DistZp"       ,m_d5DistZp);
      m_N5PiTagged->addItem("DistRp"       ,m_d5DistRp);
      m_N5PiTagged->addItem("DistZm"       ,m_d5DistZm);
      m_N5PiTagged->addItem("DistRm"       ,m_d5DistRm);
      m_N5PiTagged->addItem("iBgrVeto"     ,m_i5BgrVeto);
      m_N5PiTagged->addItem("imcTruePi"    ,m_i5mcTruePi);
      m_N5PiTagged->addItem("imcTruePi0"   ,m_i5mcTruePi0);
      m_N5PiTagged->addItem("imcTrueISR"   ,m_i5mcTrueISR);
      m_N5PiTagged->addItem("imcTrueGammaFromJpsi"  ,m_i5mcTrueGammaFromJpsi);
      m_N5PiTagged->addItem("imcTrueOtherGamma"  ,m_i5mcTrueOtherGamma);
      m_N5PiTagged->addItem("imcTrueOther"  ,m_i5mcTrueOther);
      m_N5PiTagged->addItem("mcMHad"      ,m_d5mcMHad);
      m_5Pi01       .AttachToNtuple(m_N5PiTagged,"Pi01");
      m_5Pi02       .AttachToNtuple(m_N5PiTagged,"Pi02");
      m_5Pi03       .AttachToNtuple(m_N5PiTagged,"Pi03");
      m_5Pi1        .AttachToNtuple(m_N5PiTagged,"Pi1");
      m_5Pi2        .AttachToNtuple(m_N5PiTagged,"Pi2");
      m_5FitPi01    .AttachToNtuple(m_N5PiTagged,"FitPi01");
      m_5FitPi02    .AttachToNtuple(m_N5PiTagged,"FitPi02");
      m_5FitPi03    .AttachToNtuple(m_N5PiTagged,"FitPi03");
      m_5FitPi1     .AttachToNtuple(m_N5PiTagged,"FitPi1");
      m_5FitPi2     .AttachToNtuple(m_N5PiTagged,"FitPi2");
      m_5TrueISR    .AttachToNtuple(m_N5PiTagged,"TrueISR");
      for (int a=0;a<GAMMAS+7;a++) {m_5Gammas   [a].AttachToNtuple(m_N5PiTagged,"G"   +SSTR(a)+"emc",true); m_N5PiTagged->addItem("ClosestTr"+SSTR(a),m_d5ClosestTrack[a]);}
      for (int a=0;a<7;a++)        {m_5FitGammas[a].AttachToNtuple(m_N5PiTagged,"FitG"+SSTR(a),true);}
      m_N5PiTagged->addItem("5PiNonRadChi2"   ,m_d5PiNonRadChi2);
      m_N5PiTagged->addItem("VtxChi2"   ,m_d5VtxChi2);
      m_N5PiTagged->addItem("VtxPosX"   ,m_d5VtxPosX);
      m_N5PiTagged->addItem("VtxPosY"   ,m_d5VtxPosY);
      m_N5PiTagged->addItem("VtxPosZ"   ,m_d5VtxPosZ);
      m_N5PiTagged->addItem("IPposX"    ,m_d5IPposX);
      m_N5PiTagged->addItem("IPposY"    ,m_d5IPposY);
      m_N5PiTagged->addItem("IPposZ"    ,m_d5IPposZ);
      // Kskpi veto
      m_N5PiTagged->addItem("Prob1Pi"      ,m_d5Prob1Pi);
      m_N5PiTagged->addItem("Prob1K"       ,m_d5Prob1K);
      m_N5PiTagged->addItem("Prob2Pi"      ,m_d5Prob2Pi);
      m_N5PiTagged->addItem("Prob2K"       ,m_d5Prob2K);
      m_N5PiTagged->addItem("Prob1P"       ,m_d5Prob1P);
      m_N5PiTagged->addItem("Prob2P"       ,m_d5Prob2P);

    }
    else {
       log << MSG::ERROR << "  Cannot book N-tuple88 tagged: " << long(m_NTagged) << endreq;
       return StatusCode::FAILURE;
    }

    if ( m_N5Pi ) {
      m_N5Pi->addItem("CMSEnergy"    ,m_d5NCMSEnergy);
      m_N5Pi->addItem("RunNumber"    ,m_d5NRunNumber);
      m_N5Pi->addItem("iAnzGoodP"    ,m_i5NAnzGoodP);
      m_N5Pi->addItem("iAnzGoodM"    ,m_i5NAnzGoodM);
      m_N5Pi->addItem("iAnzGoodGamma",m_i5NAnzGoodGamma);
      m_N5Pi->addItem("iAnzPi0Tracks",m_i5NAnzPi0Tracks);
      m_N5Pi->addItem("iAnz2Combs"   ,m_i5NAnz2Combs);
      m_N5Pi->addItem("iAnz3Combs"   ,m_i5NAnz3Combs);
      m_N5Pi->addItem("5PiIsrUChi2"  ,m_d5NPiIsrUChi2);
      m_N5Pi->addItem("5PiIsrYChi2"  ,m_d5NPiIsrTChi2);
      m_N5Pi->addItem("5PiNonRadChi2",m_d5NPiNonRadChi2);
      m_N5Pi->addItem("chisq4piT"    ,m_d5Nchisq4piT);
      m_N5Pi->addItem("mhad4piT"     ,m_d5Nmhad4piT);
      m_N5Pi->addItem("chisq4piU"    ,m_d5Nchisq4piU);
      m_N5Pi->addItem("m_d5Nmhad4piU",m_d5Nmhad4piU);
      m_N5Pi->addItem("Pi01Chi2"     ,m_d5NPi01Chi2);
      m_N5Pi->addItem("Pi02Chi2"     ,m_d5NPi02Chi2);
      m_N5Pi->addItem("Pi03Chi2"     ,m_d5NPi03Chi2);
      m_N5Pi->addItem("DistZp"       ,m_d5NDistZp);
      m_N5Pi->addItem("DistRp"       ,m_d5NDistRp);
      m_N5Pi->addItem("DistZm"       ,m_d5NDistZm);
      m_N5Pi->addItem("DistRm"       ,m_d5NDistRm);
      m_N5Pi->addItem("iBgrVeto"     ,m_i5NBgrVeto);
      m_N5Pi->addItem("imcTruePi"    ,m_i5NmcTruePi);
      m_N5Pi->addItem("imcTruePi0"   ,m_i5NmcTruePi0);
      m_N5Pi->addItem("imcTrueISR"   ,m_i5NmcTrueISR);
      m_N5Pi->addItem("imcTrueGammaFromJpsi"  ,m_i5NmcTrueGammaFromJpsi);
      m_N5Pi->addItem("imcTrueOtherGamma"  ,m_i5NmcTrueOtherGamma);
      m_N5Pi->addItem("imcTrueOther"  ,m_i5NmcTrueOther);
      m_5NPi01       .AttachToNtuple(m_N5Pi,"Pi01");
      m_5NPi02       .AttachToNtuple(m_N5Pi,"Pi02");
      m_5NPi03       .AttachToNtuple(m_N5Pi,"Pi03");
      m_5NPi1        .AttachToNtuple(m_N5Pi,"Pi1");
      m_5NPi2        .AttachToNtuple(m_N5Pi,"Pi2");
      m_5NFitPi01    .AttachToNtuple(m_N5Pi,"FitPi01");
      m_5NFitPi02    .AttachToNtuple(m_N5Pi,"FitPi02");
      m_5NFitPi03    .AttachToNtuple(m_N5Pi,"FitPi03");
      m_5NFitPi1     .AttachToNtuple(m_N5Pi,"FitPi1");
      m_5NFitPi2     .AttachToNtuple(m_N5Pi,"FitPi2");
      for (int a=0;a<GAMMAS+6;a++) {m_5NGammas   [a].AttachToNtuple(m_N5Pi,"G"   +SSTR(a)+"emc",true); m_N5Pi->addItem("ClosestTr"+SSTR(a),m_d5NClosestTrack[a]);}
      for (int a=0;a<6;a++)        {m_5NFitGammas[a].AttachToNtuple(m_N5Pi,"FitG"+SSTR(a),true);}
      m_N5Pi->addItem("VtxChi2"   ,m_d5NVtxChi2);
      m_N5Pi->addItem("VtxPosX"   ,m_d5NVtxPosX);
      m_N5Pi->addItem("VtxPosY"   ,m_d5NVtxPosY);
      m_N5Pi->addItem("VtxPosZ"   ,m_d5NVtxPosZ);
      // Kskpi veto
      m_N5Pi->addItem("Prob1Pi"      ,m_d5NProb1Pi);
      m_N5Pi->addItem("Prob1K"       ,m_d5NProb1K);
      m_N5Pi->addItem("Prob2Pi"      ,m_d5NProb2Pi);
      m_N5Pi->addItem("Prob2K"       ,m_d5NProb2K);
      m_N5Pi->addItem("Prob1P"       ,m_d5NProb1P);
      m_N5Pi->addItem("Prob2P"       ,m_d5NProb2P);

    }
    else {
       log << MSG::ERROR << "  Cannot book N-tuple88 5pi: " << long(m_N5Pi) << endreq;
       return StatusCode::FAILURE;
    }


    if ( m_N4Pi ) {
      m_N4Pi->addItem("CMSEnergy"    ,m_d4NCMSEnergy);
      m_N4Pi->addItem("RunNumber"    ,m_d4NRunNumber);
      m_N4Pi->addItem("iAnzGoodP"    ,m_i4NAnzGoodP);
      m_N4Pi->addItem("iAnzGoodM"    ,m_i4NAnzGoodM);
      m_N4Pi->addItem("iAnzGoodGamma",m_i4NAnzGoodGamma);
      m_N4Pi->addItem("iAnzPi0Tracks",m_i4NAnzPi0Tracks);
      m_N4Pi->addItem("iAnz2Combs"   ,m_i4NAnz2Combs);
      m_N4Pi->addItem("iAnz3Combs"   ,m_i4NAnz3Combs);
      m_N4Pi->addItem("4PiIsrTChi2"  ,m_d4NPiIsrTChi2);
      m_N4Pi->addItem("4PiIsrUChi2"  ,m_d4NPiIsrUChi2);
      m_N4Pi->addItem("4PiNonRadChi2",m_d4NPiNonRadChi2);
      m_N4Pi->addItem("Pi01Chi2"     ,m_d4NPi01Chi2);
      m_N4Pi->addItem("Pi02Chi2"     ,m_d4NPi02Chi2);
      m_N4Pi->addItem("DistZp"       ,m_d4NDistZp);
      m_N4Pi->addItem("DistRp"       ,m_d4NDistRp);
      m_N4Pi->addItem("DistZm"       ,m_d4NDistZm);
      m_N4Pi->addItem("DistRm"       ,m_d4NDistRm);
      m_N4Pi->addItem("iBgrVeto"     ,m_i4NBgrVeto);
      m_N4Pi->addItem("imcTruePi"    ,m_i4NmcTruePi);
      m_N4Pi->addItem("imcTruePi0"   ,m_i4NmcTruePi0);
      m_N4Pi->addItem("imcTrueISR"   ,m_i4NmcTrueISR);
      m_N4Pi->addItem("imcTrueGammaFromJpsi"  ,m_i4NmcTrueGammaFromJpsi);
      m_N4Pi->addItem("imcTrueOtherGamma"  ,m_i4NmcTrueOtherGamma);
      m_N4Pi->addItem("imcTrueOther"  ,m_i4NmcTrueOther);
      m_4NPi01       .AttachToNtuple(m_N4Pi,"Pi01");
      m_4NPi02       .AttachToNtuple(m_N4Pi,"Pi02");
      m_4NPi1        .AttachToNtuple(m_N4Pi,"Pi1");
      m_4NPi2        .AttachToNtuple(m_N4Pi,"Pi2");
      m_4NFitPi01    .AttachToNtuple(m_N4Pi,"FitPi01");
      m_4NFitPi02    .AttachToNtuple(m_N4Pi,"FitPi02");
      m_4NFitPi1     .AttachToNtuple(m_N4Pi,"FitPi1");
      m_4NFitPi2     .AttachToNtuple(m_N4Pi,"FitPi2");
      for (int a=0;a<GAMMAS+5;a++) {m_4NGammas   [a].AttachToNtuple(m_N4Pi,"G"   +SSTR(a)+"emc",true); m_N4Pi->addItem("ClosestTr"+SSTR(a),m_d4NClosestTrack[a]);}
      for (int a=0;a<4;a++)        {m_4NFitGammas[a].AttachToNtuple(m_N4Pi,"FitG"+SSTR(a),true);}
      m_N4Pi->addItem("VtxChi2"   ,m_d4NVtxChi2);
      m_N4Pi->addItem("VtxPosX"   ,m_d4NVtxPosX);
      m_N4Pi->addItem("VtxPosY"   ,m_d4NVtxPosY);
      m_N4Pi->addItem("VtxPosZ"   ,m_d4NVtxPosZ);
      // Kskpi veto
      m_N4Pi->addItem("Prob1Pi"      ,m_d4NProb1Pi);
      m_N4Pi->addItem("Prob1K"       ,m_d4NProb1K);
      m_N4Pi->addItem("Prob2Pi"      ,m_d4NProb2Pi);
      m_N4Pi->addItem("Prob2K"       ,m_d4NProb2K);
      m_N4Pi->addItem("Prob1P"       ,m_d4NProb1P);
      m_N4Pi->addItem("Prob2P"       ,m_d4NProb2P);

    }
    else {
       log << MSG::ERROR << "  Cannot book N-tuple88 5pi: " << long(m_N4Pi) << endreq;
       return StatusCode::FAILURE;
    }


   StatusCode sc = Gaudi::svcLocator()->service( "VertexDbSvc", m_vtxSvc,true);
   if(sc.isFailure())
     {
     //assert(false); //terminate program
     log << MSG::ERROR << "Cannot book VertexDbSvc!!" <<endmsg;
     return StatusCode::FAILURE;
   }
   //m_particleTable = m_partPropSvc->PDT();
   return StatusCode::SUCCESS;
}
//----------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------
StatusCode Bes2Gamto2Pi0::execute()
//----------------------------------------------------------------------------------------------------------
{
    //cout<<"begin execute"<<endl;
    MsgStream log(msgSvc(), name());
    log << MSG::INFO << "in execute()" << endreq;

    SmartDataPtr<Event::EventHeader> eventHeader(eventSvc(),"/Event/EventHeader");
    SmartDataPtr<EvtRecEvent> evtRecEvent(eventSvc(), "/Event/EvtRec/EvtRecEvent");
    if ( ! evtRecEvent ) {
        log << MSG::FATAL << "Could not find EvtRecEvent" << endreq;
        return StatusCode::FAILURE;
    }
    SmartDataPtr<EvtRecTrackCol> evtRecTrackCol(eventSvc(), "/Event/EvtRec/EvtRecTrackCol");
    if ( ! evtRecTrackCol ) {
        log << MSG::FATAL << "Could not find EvtRecTrackCol" << endreq;
        return StatusCode::FAILURE;
    }
    Gaudi::svcLocator()->service("VertexDbSvc", m_vtxSvc);


    // get event with montecarlo truth
    m_iAll ++;
    WriteMonteCarloTrurh();
    // preselector: skip, if there are insufficient amounts of tracks
    if (evtRecEvent->totalCharged() <  2 || evtRecEvent->totalCharged() > 6 || evtRecEvent->totalNeutral() > 50) {
        //cout<<"# insuficinet tracks: ch= "<<evtRecEvent->totalCharged()<<" , neut= "<<evtRecEvent->totalNeutral()<<endl;
        m_iPreSkip ++;
        return StatusCode::SUCCESS;
    }
    //cout<<"passed preselection: charged= "<<evtRecEvent->totalCharged()<<" , neutral= "<<evtRecEvent->totalNeutral()<<endl;

    // initialize iterators
    m_itBegin      = evtRecTrackCol->begin();
    m_itEndCharged = m_itBegin + evtRecEvent->totalCharged();
    m_itEndNeutral = m_itBegin + evtRecEvent->totalTracks();
    m_aPi0List.SetIterators (m_itBegin,m_itEndCharged,m_itEndNeutral);

    // initialize virtual box
    m_hp3IP.setX(0);
    m_hp3IP.setY(0);
    m_hp3IP.setZ(0);
    if (m_vtxSvc->isVertexValid()) {
      double* dbv =  m_vtxSvc->PrimaryVertex();
      m_hp3IP.setX(dbv[0]);
      m_hp3IP.setY(dbv[1]);
      m_hp3IP.setZ(dbv[2]);
    }
    else{std::cout<<"no IP"<<std::endl;}

    // prepare ntuples
    ResetNTuples();
    m_dRunNumber   = eventHeader->runNumber();
    m_dURunNumber  = eventHeader->runNumber();
    m_d5RunNumber  = eventHeader->runNumber();
    m_d5URunNumber = eventHeader->runNumber();
    m_d5NRunNumber = eventHeader->runNumber();
    m_d4NRunNumber = eventHeader->runNumber();
    m_dEvtNumber   = eventHeader->eventNumber();
    m_dUEvtNumber  = eventHeader->eventNumber();
    m_d5EvtNumber  = eventHeader->eventNumber();
    m_d5UEvtNumber = eventHeader->eventNumber();
    m_dCMSEnergy = beam_energy(abs(eventHeader->runNumber()));
    if (m_dCMSEnergy == -1) {log<<MSG::FATAL<<"unknown runNr, can not determine CMS Energy!"<<endreq; return StatusCode::FAILURE;}
    m_dTCMSEnergy    = m_dCMSEnergy;
    m_dUCMSEnergy    = m_dCMSEnergy;
    m_d5TCMSEnergy   = m_dCMSEnergy;
    m_d5UCMSEnergy   = m_dCMSEnergy;
    m_d5NCMSEnergy   = m_dCMSEnergy;
    m_d4NCMSEnergy   = m_dCMSEnergy;
    //m_lvBoost        = HepLorentzVector(0.0415,0,0,m_dCMSEnergy);
    m_lvBoost        = HepLorentzVector(m_dCMSEnergy*sin(0.011),0,0,m_dCMSEnergy);
    m_dPi0GcutHigh   = m_dCMSEnergy*0.6,
    m_aPi0List.Init(m_dPi0MassCutLow,m_dPi0MassCutHigh,m_dPhEnergyCutBarrel,m_dPhEnergyCutEndCap,m_dCosThetaBarrel,m_dMaxTime,m_dCMSEnergy/2.0);

    // fill particle lists
    if (!FillParticleList ()) {return StatusCode::SUCCESS;}
    //RecordAngle ();
    Try4PiFit();
    Try5PiFit();
    Find2pi02picTagged();
    Find2pi02picUntagged();

    // write
/*    if (m_d4PiNonRadChi2 > 0 || m_dsqrSHadSysFit  > 0 || m_d5PiMhad > 0) {
      m_NTagged->write();
    }
    if (m_dU4PiNonRadChi2 > 0 || m_dUsqrSHadSysFit > 0 || m_dU5PiMhad > 0) {
      m_NUntagged->write();
      m_iAcceptedU ++;
    }
    else {m_iUFitSkip ++;}
    if (m_d5sqrSHadSysFit > 0 || m_d5PiNonRadChi2 > 0) {
        m_N5PiTagged->write();
    }
    if (m_d5UsqrSHadSysFit > 0 || m_dU5PiNonRadChi2 > 0) {
        m_N5PiUntagged->write();
    }*/

    if (m_dsqrSHadSysFit  > 0) {
        m_NTagged->write();
    }
    if (m_dUsqrSHadSysFit > 0) {
      m_NUntagged->write();
      m_iAcceptedU ++;
    }
    else {m_iUFitSkip ++;}
    if (m_d5sqrSHadSysFit > 0) {
        m_N5PiTagged->write();
    }
    if (m_d5UsqrSHadSysFit > 0 ) {
        m_N5PiUntagged->write();
    }
    if (m_d5NPiNonRadChi2 > 0 && m_d5NPiNonRadChi2 < m_dChisqMaxValue) {
        m_N5Pi->write();
    }
    if (m_d4NPiNonRadChi2 > 0 && m_d4NPiNonRadChi2 < m_dChisqMaxValue) {
        m_N4Pi->write();
    }

    return StatusCode::SUCCESS;
}
//----------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------- 
StatusCode Bes2Gamto2Pi0::finalize()
//----------------------------------------------------------------------------------------------------------
{
  MsgStream log(msgSvc(), name());
  log << MSG::INFO << "in finalize()" << endreq;

  cout<<"Events: "<<m_iAll<<endl;
  cout<<"PreSkip: "<<m_iPreSkip<<endl;
  cout<<"PhotonSkip: "<<m_iPhotonSkip<<endl;  
  cout<<"PionSkip: "<<m_iPionSkip<<endl;
  cout<<"Pi0Skip: "<<m_iPi0Skip<<endl;
  cout<<"UFitSkip: "<<m_iUFitSkip<<endl;
  cout<<"AcceptedU: "<<m_iAcceptedU<<endl;

  return StatusCode::SUCCESS;
}
//----------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------- 
void Bes2Gamto2Pi0::ResetNTuples()
//---------------------------------------------------------------------------------------------------------- 
{
    //IP pos
    m_dIPposX   = m_hp3IP.x();
    m_dIPposY   = m_hp3IP.y();
    m_dIPposZ   = m_hp3IP.z();
    m_dUIPposX  = m_hp3IP.x();
    m_dUIPposY  = m_hp3IP.y();
    m_dUIPposZ  = m_hp3IP.z();
    m_d5IPposX  = m_hp3IP.x();
    m_d5IPposY  = m_hp3IP.y();
    m_d5IPposZ  = m_hp3IP.z();
    m_d5UIPposX = m_hp3IP.x();
    m_d5UIPposY = m_hp3IP.y();
    m_d5UIPposZ = m_hp3IP.z();

    // count MC particles if available
    if (m_iMCisr > 0) {
        if (m_lvmcISR1.e() > m_lvmcISR2.e()) {
            m_TrueISR.Fill(&m_lvmcISR1);
            m_UTrueISR.Fill(&m_lvmcISR1);
            m_5TrueISR.Fill(&m_lvmcISR1);
            m_5UTrueISR.Fill(&m_lvmcISR1);
        }
        else {
            m_TrueISR.Fill(&m_lvmcISR2);
            m_UTrueISR.Fill(&m_lvmcISR2);
            m_5TrueISR.Fill(&m_lvmcISR2);
            m_5UTrueISR.Fill(&m_lvmcISR2);
        }
    }
    else {
        m_TrueISR.Clear();
        m_UTrueISR.Clear();
        m_5TrueISR.Clear();
        m_5UTrueISR.Clear();
    }

    m_imcTrueISR            = m_iMCisr;
    m_imcTrueFSR            = m_iMCfsr;
    m_imcTruePi             = m_iMCpi;
    m_imcTruePi0            = m_iMCpi0;
    m_imcTrueGammaFromJpsi  = m_iMCGammaFromJpsi;
    m_imcTrueOtherGamma     = m_iMCOtherGamma;
    m_imcTrueOther          = m_iMCOther;
    m_dmcMHad               = m_dmHadSysMCtruth;
    m_dmcMHad5pi            = m_d5mHadSysMCtruth;
    m_iUmcTrueISR           = m_iMCisr;
    m_iUmcTrueFSR           = m_iMCfsr;
    m_iUmcTruePi            = m_iMCpi;
    m_iUmcTruePi0           = m_iMCpi0;
    m_iUmcTrueGammaFromJpsi = m_iMCGammaFromJpsi;
    m_iUmcTrueOtherGamma    = m_iMCOtherGamma;
    m_iUmcTrueOther         = m_iMCOther;
    m_dUmcMHad              = m_dmHadSysMCtruth;
    m_dUmcMHad5pi           = m_d5mHadSysMCtruth;
    m_dTCMSEnergy           = -1;
    m_dUCMSEnergy           = -1;

    m_i5mcTrueISR            = m_iMCisr;
    m_i5mcTruePi             = m_iMCpi;
    m_i5mcTruePi0            = m_iMCpi0;
    m_i5mcTrueGammaFromJpsi  = m_iMCGammaFromJpsi;
    m_i5mcTrueOtherGamma     = m_iMCOtherGamma;
    m_i5mcTrueOther          = m_iMCOther;
    m_d5mcMHad               = m_d5mHadSysMCtruth;
    m_i5UmcTrueISR           = m_iMCisr;
    m_i5UmcTruePi            = m_iMCpi;
    m_i5UmcTruePi0           = m_iMCpi0;
    m_i5UmcTrueGammaFromJpsi = m_iMCGammaFromJpsi;
    m_i5UmcTrueOtherGamma    = m_iMCOtherGamma;
    m_i5UmcTrueOther         = m_iMCOther;
    m_d5UmcMHad              = m_d5mHadSysMCtruth;
    m_d5TCMSEnergy           = -1;
    m_d5UCMSEnergy           = -1;

    m_i5NmcTrueISR           = m_iMCisr;
    m_i5NmcTruePi            = m_iMCpi;
    m_i5NmcTruePi0           = m_iMCpi0;
    m_i5NmcTrueGammaFromJpsi = m_iMCGammaFromJpsi;
    m_i5NmcTrueOtherGamma    = m_iMCOtherGamma;
    m_i5NmcTrueOther         = m_iMCOther;
    m_i4NmcTrueISR           = m_iMCisr;
    m_i4NmcTruePi            = m_iMCpi;
    m_i4NmcTruePi0           = m_iMCpi0;
    m_i4NmcTrueGammaFromJpsi = m_iMCGammaFromJpsi;
    m_i4NmcTrueOtherGamma    = m_iMCOtherGamma;
    m_i4NmcTrueOther         = m_iMCOther;
    m_d5NCMSEnergy           = -1;
    m_d4NCMSEnergy           = -1;

    // 5pi untagged
    m_dU5PiNonRadChi2    = m_dChisqMaxValue;
    m_i5UAnzGoodP        = -1;
    m_i5UAnzGoodM        = -1;
    m_i5UAnzGoodGamma    = -1;
    m_i5UAnzPi0Tracks    = -1;
    m_i5UAnz2Combs       = -1;
    m_i5UAnz3Combs       = -1;
    m_d5UTotalChi2       = m_dChisqMaxValue;
    m_d5UPi01Chi2        = m_dChisqMaxValue;
    m_d5UPi02Chi2        = m_dChisqMaxValue;
    m_d5UPi03Chi2        = m_dChisqMaxValue;
    m_d5UDistZp          = -100;
    m_d5UDistRp          = -1;
    m_d5UDistZm          = -100;
    m_d5UDistRm          = -1;
    m_d5UsqrSHadSysFit   = -1;
    m_i5UBgrVeto         = -5;
    m_5UPi01.Clear();
    m_5UPi02.Clear();
    m_5UPi03.Clear();
    m_5UPi1.Clear();
    m_5UPi2.Clear();
    for (int a=0;a<GAMMAS+6;a++) {m_5UGammas[a].Clear(); m_d5UClosestTrack[a] = -4.0;}
    m_5UFitPi01.Clear();
    m_5UFitPi02.Clear();
    m_5UFitPi03.Clear();
    m_5UFitPi1.Clear();
    m_5UFitPi2.Clear();
    for (int a=0;a<7;a++) {m_5UFitGammas[a].Clear();}
    m_d5UVtxChi2 = -1;
    m_d5UVtxPosX = -1;
    m_d5UVtxPosY = -1;
    m_d5UVtxPosZ = -20;
    // Kskpi veto
    m_d5UProb1Pi = -1;
    m_d5UProb1K  = -1;
    m_d5UProb2Pi = -1;
    m_d5UProb2K  = -1;
    m_d5UProb1P  = -1;
    m_d5UProb2P  = -1;

    // 5pi tagged
    m_d5PiNonRadChi2    = m_dChisqMaxValue;
    m_i5AnzGoodP        = -1;
    m_i5AnzGoodM        = -1;
    m_i5AnzGoodGamma    = -1;
    m_i5AnzPi0Tracks    = -1;
    m_i5Anz2Combs       = -1;
    m_i5Anz3Combs       = -1;
    m_d5TotalChi2       = m_dChisqMaxValue;
    m_d5Pi01Chi2        = m_dChisqMaxValue;
    m_d5Pi02Chi2        = m_dChisqMaxValue;
    m_d5Pi03Chi2        = m_dChisqMaxValue;
    m_d5DistZp          = -100;
    m_d5DistRp          = -1;
    m_d5DistZm          = -100;
    m_d5DistRm          = -1;
    m_d5sqrSHadSysFit   = -1;
    m_i5BgrVeto         = -5;
    m_5Pi01.Clear();
    m_5Pi02.Clear();
    m_5Pi03.Clear();
    m_5Pi1.Clear();
    m_5Pi2.Clear();
    for (int a=0;a<GAMMAS+7;a++) {m_5Gammas[a].Clear(); m_d5ClosestTrack[a] = -4.0;}
    m_5FitPi01.Clear();
    m_5FitPi02.Clear();
    m_5FitPi03.Clear();
    m_5FitPi1.Clear();
    m_5FitPi2.Clear();
    for (int a=0;a<7;a++) {m_5FitGammas[a].Clear();}
    m_d5VtxChi2 = -1;
    m_d5VtxPosX = -1;
    m_d5VtxPosY = -1;
    m_d5VtxPosZ = -20;
    // Kskpi veto
    m_d5Prob1Pi = -1;
    m_d5Prob1K  = -1;
    m_d5Prob2Pi = -1;
    m_d5Prob2K  = -1;
    m_d5Prob1P  = -1;
    m_d5Prob2P  = -1;

    // signal tagged
    m_d4PiNonRadChi2   = m_dChisqMaxValue;
    m_iAnzGoodP        = -1;
    m_iAnzGoodM        = -1;
    m_iAnzGoodGamma    = -1;
    m_iAnzPi0Tracks    = -1;
    m_iAnz2Combs       = -1;
    m_iAnz3Combs       = -1;
    m_dTotalChi2       = m_dChisqMaxValue;
    m_dPi01Chi2        = m_dChisqMaxValue;
    m_dPi02Chi2        = m_dChisqMaxValue;
    m_dDistZp          = -100;
    m_dDistRp          = -1;
    m_dDistZm          = -100;
    m_dDistRm          = -1;
    m_dsqrSHadSys      = -1;
    m_dsqrSHadSysFit   = -1;
    m_iBgrVeto         = -5;
    m_dPi1EoP          = -1;
    m_dPi2EoP          = -1;
    m_dPi1MucDepth     = -1;
    m_dPi2MucDepth     = -1;
    m_Pi01.Clear();
    m_Pi02.Clear();
    m_Pi1.Clear();
    m_Pi2.Clear();
    for (int a=0;a<GAMMAS+5;a++) {m_Gammas[a].Clear(); m_dClosestTrack[a] = -4.0;}
    m_FitPi01.Clear();
    m_FitPi02.Clear();
    m_FitPi1.Clear();
    m_FitPi2.Clear();
    for (int a=0;a<5;a++) {m_FitGammas[a].Clear();}
    m_dVtxChi2 = -1;
    m_dVtxPosX = -1;
    m_dVtxPosY = -1;
    m_dVtxPosZ = -20;
    //m_d5PiNonRadChi2 = -1;
    m_d5PiChi2 = -1;
    m_d5PiMhad = -1;
    m_5PiISR.Clear();
    // Kskpi veto
    m_dProb1Pi = -1;
    m_dProb1K  = -1;
    m_dProb2Pi = -1;
    m_dProb2K  = -1;
    m_dProb1P  = -1;
    m_dProb2P  = -1;

    // signal untagged
    m_dU4PiNonRadChi2   = m_dChisqMaxValue;
    m_iUAnzGoodP        = -1;
    m_iUAnzGoodM        = -1;
    m_iUAnzGoodGamma    = -1;
    m_iUAnzPi0Tracks    = -1;
    m_iUAnz2Combs       = -1;
    m_iUAnz3Combs       = -1;
    m_dUTotalChi2       = m_dChisqMaxValue;
    m_dUPi01Chi2        = m_dChisqMaxValue;
    m_dUPi02Chi2        = m_dChisqMaxValue;
    m_dUDistZp          = -100;
    m_dUDistRp          = -1;
    m_dUDistZm          = -100;
    m_dUDistRm          = -1;
    m_dUsqrSHadSys      = -1;
    m_dUsqrSHadSysFit   = -1;
    m_iUBgrVeto         = -5;
    m_dUPi1EoP          = -1;
    m_dUPi2EoP          = -1;
    m_dUPi1MucDepth     = -1;
    m_dUPi2MucDepth     = -1;
    m_UPi01.Clear();
    m_UPi02.Clear();
    m_UPi1.Clear();
    m_UPi2.Clear();
    //m_TaggedISR.Clear(); // done in tagged fit
    for (int a=0;a<GAMMAS+4;a++) {m_UGammas[a].Clear(); m_dUClosestTrack[a] = -4.0;}
    m_UFitPi01.Clear();
    m_UFitPi02.Clear();
    m_UFitPi1.Clear();
    m_UFitPi2.Clear();
    for (int a=0;a<5;a++) {m_UFitGammas[a].Clear();}
    m_dUVtxChi2 = -1;
    m_dUVtxPosX = -1;
    m_dUVtxPosY = -1;
    m_dUVtxPosZ = -20;
    //m_dU5PiNonRadChi2 = -1;
    m_dU5PiChi2 = -1;
    m_dU5PiMhad = -1;
    m_U5PiISR.Clear();
    // Kskpi veto
    m_dUProb1Pi = -1;
    m_dUProb1K  = -1;
    m_dUProb2Pi = -1;
    m_dUProb2K  = -1;
    m_dUProb1P  = -1;
    m_dUProb2P  = -1;

    // 5pi
    m_d5NPiNonRadChi2    = m_dChisqMaxValue;
    m_i5NAnzGoodP        = -1;
    m_i5NAnzGoodM        = -1;
    m_i5NAnzGoodGamma    = -1;
    m_i5NAnzPi0Tracks    = -1;
    m_i5NAnz2Combs       = -1;
    m_i5NAnz3Combs       = -1;
    m_d5NPiIsrUChi2      = -1;
    m_d5NPiIsrTChi2      = -1;
    m_d5NPi01Chi2        = m_dChisqMaxValue;
    m_d5NPi02Chi2        = m_dChisqMaxValue;
    m_d5NPi03Chi2        = m_dChisqMaxValue;
    m_d5NDistZp          = -100;
    m_d5NDistRp          = -1;
    m_d5NDistZm          = -100;
    m_d5NDistRm          = -1;
    m_i5NBgrVeto         = -5;
    m_d5Nchisq4piT       = -1;
    m_d5Nmhad4piT        = -1;
    m_d5Nchisq4piU       = -1;
    m_d5Nmhad4piU        = -1;
    m_5NPi01.Clear();
    m_5NPi02.Clear();
    m_5NPi03.Clear();
    m_5NPi1.Clear();
    m_5NPi2.Clear();
    for (int a=0;a<GAMMAS+6;a++) {m_5NGammas[a].Clear(); m_d5NClosestTrack[a] = -4.0;}
    m_5NFitPi01.Clear();
    m_5NFitPi02.Clear();
    m_5NFitPi03.Clear();
    m_5NFitPi1.Clear();
    m_5NFitPi2.Clear();
    for (int a=0;a<6;a++) {m_5NFitGammas[a].Clear();}
    m_d5NVtxChi2 = -1;
    m_d5NVtxPosX = -1;
    m_d5NVtxPosY = -1;
    m_d5NVtxPosZ = -20;
    // Kskpi veto
    m_d5NProb1Pi = -1;
    m_d5NProb1K  = -1;
    m_d5NProb2Pi = -1;
    m_d5NProb2K  = -1;
    m_d5NProb1P  = -1;
    m_d5NProb2P  = -1;

    // 4pi
    m_d4NPiNonRadChi2    = m_dChisqMaxValue;
    m_i4NAnzGoodP        = -1;
    m_i4NAnzGoodM        = -1;
    m_i4NAnzGoodGamma    = -1;
    m_i4NAnzPi0Tracks    = -1;
    m_i4NAnz2Combs       = -1;
    m_i4NAnz3Combs       = -1;
    m_d4NPiIsrUChi2      = -1;
    m_d4NPiIsrTChi2      = -1;
    m_d4NPi01Chi2        = m_dChisqMaxValue;
    m_d4NPi02Chi2        = m_dChisqMaxValue;
    m_d4NDistZp          = -100;
    m_d4NDistRp          = -1;
    m_d4NDistZm          = -100;
    m_d4NDistRm          = -1;
    m_i4NBgrVeto         = -5;
    m_4NPi01.Clear();
    m_4NPi02.Clear();
    m_4NPi1.Clear();
    m_4NPi2.Clear();
    for (int a=0;a<GAMMAS+5;a++) {m_4NGammas[a].Clear(); m_d4NClosestTrack[a] = -4.0;}
    m_4NFitPi01.Clear();
    m_4NFitPi02.Clear();
    m_4NFitPi1.Clear();
    m_4NFitPi2.Clear();
    for (int a=0;a<4;a++) {m_4NFitGammas[a].Clear();}
    m_d4NVtxChi2 = -1;
    m_d4NVtxPosX = -1;
    m_d4NVtxPosY = -1;
    m_d4NVtxPosZ = -20;
    // Kskpi veto
    m_d4NProb1Pi = -1;
    m_d4NProb1K  = -1;
    m_d4NProb2Pi = -1;
    m_d4NProb2K  = -1;
    m_d4NProb1P  = -1;
    m_d4NProb2P  = -1;

}
//---------------------------------------------------------------------------------------------------------- 

//----------------------------------------------------------------------------------------------------------
void Bes2Gamto2Pi0::PrepareToWriteNonrad (cPi0Tracks* ptPi01,cPi0Tracks* ptPi02,cPi0Tracks* ptPi03)
//----------------------------------------------------------------------------------------------------------
{
    m_lvFitPi1    = m_KalKinFit->pfit(0);
    m_lvFitPi2    = m_KalKinFit->pfit(1);
    m_lvFitPi01   = m_KalKinFit->pfit(2) + m_KalKinFit->pfit(3);
    m_lvFitPi02   = m_KalKinFit->pfit(4) + m_KalKinFit->pfit(5);
    m_lvFitPi01G1 = m_KalKinFit->pfit(2);
    m_lvFitPi01G2 = m_KalKinFit->pfit(3);
    m_lvFitPi02G1 = m_KalKinFit->pfit(4);
    m_lvFitPi02G2 = m_KalKinFit->pfit(5);
    if (ptPi03 != 0) {
        m_lvFitPi03   = m_KalKinFit->pfit(6) + m_KalKinFit->pfit(7);
        m_lvFitPi03G1 = m_KalKinFit->pfit(6);
        m_lvFitPi03G2 = m_KalKinFit->pfit(7);
    }
    // detector
    WTrackParameter wtpPiMinus (m_dMassPiCharged,m_pERTPiMinus->mdcKalTrack()->getZHelix(),m_pERTPiMinus->mdcKalTrack()->getZError());
    WTrackParameter wtpPiPlus  (m_dMassPiCharged,m_pERTPiPlus->mdcKalTrack()->getZHelix() ,m_pERTPiPlus->mdcKalTrack()->getZError());
    m_lvPi1    = wtpPiMinus.p();
    m_lvPi2    = wtpPiPlus.p();
    m_lvPi01   = m_aPi0List.Get4VecFromEMC(ptPi01->m_pTrack1,0.0) + m_aPi0List.Get4VecFromEMC(ptPi01->m_pTrack2,0.0);
    m_lvPi01G1 = m_aPi0List.Get4VecFromEMC(ptPi01->m_pTrack1,0.0);
    m_lvPi01G2 = m_aPi0List.Get4VecFromEMC(ptPi01->m_pTrack2,0.0);
    m_lvPi02   = m_aPi0List.Get4VecFromEMC(ptPi02->m_pTrack1,0.0) + m_aPi0List.Get4VecFromEMC(ptPi02->m_pTrack2,0.0);
    m_lvPi02G1 = m_aPi0List.Get4VecFromEMC(ptPi02->m_pTrack1,0.0);
    m_lvPi02G2 = m_aPi0List.Get4VecFromEMC(ptPi02->m_pTrack2,0.0);
    if (ptPi03 != 0) {
        m_lvPi03G1 = m_aPi0List.Get4VecFromEMC(ptPi03->m_pTrack1,0.0);
        m_lvPi03G2 = m_aPi0List.Get4VecFromEMC(ptPi03->m_pTrack2,0.0);
        m_lvPi03   = m_aPi0List.Get4VecFromEMC(ptPi03->m_pTrack1,0.0) + m_aPi0List.Get4VecFromEMC(ptPi03->m_pTrack2,0.0);
    }
    m_aPhIndexList.clear();
    m_aPhIndexList.push_back (ptPi01->m_pTrack1);
    m_aPhIndexList.push_back (ptPi01->m_pTrack2);
    m_aPhIndexList.push_back (ptPi02->m_pTrack1);
    m_aPhIndexList.push_back (ptPi02->m_pTrack2);
    if (ptPi03 != 0) {
        m_aPhIndexList.push_back (ptPi03->m_pTrack1);
        m_aPhIndexList.push_back (ptPi03->m_pTrack2);
    }
    // the lines below MUSST stay below the settings of the 4vectors!
    if (ptPi03 != 0) {
        if (m_KalKinFit->Fit(1)) {m_d5NPi01Chi2 = m_KalKinFit->chisq(1);} else {m_d5NPi01Chi2 = -5;}
        if (m_KalKinFit->Fit(2)) {m_d5NPi02Chi2 = m_KalKinFit->chisq(2);} else {m_d5NPi02Chi2 = -5;}
        if (m_KalKinFit->Fit(3)) {m_d5NPi03Chi2 = m_KalKinFit->chisq(3);} else {m_d5NPi03Chi2 = -5;}
    }
    else {
        if (m_KalKinFit->Fit(1)) {m_d4NPi01Chi2 = m_KalKinFit->chisq(1);} else {m_d4NPi01Chi2 = -5;}
        if (m_KalKinFit->Fit(2)) {m_d4NPi02Chi2 = m_KalKinFit->chisq(2);} else {m_d4NPi02Chi2 = -5;}
    }
}
//----------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------
void Bes2Gamto2Pi0::PrepareToWrite (cPi0Tracks* ptPi01,cPi0Tracks* ptPi02,cPi0Tracks* ptPi03,EvtRecTrack* pISR)
//----------------------------------------------------------------------------------------------------------
{
    if (pISR == 0 && ptPi03 == 0) { // 4pi untagged
        m_dUTotalChi2 = m_KalKinFit->chisq();
    }
    else if (pISR != 0 && ptPi03 == 0) { // 4pi tagged
        m_dTotalChi2  = m_KalKinFit->chisq();
    }
    else if (pISR == 0 && ptPi03 != 0) { // 5pi untagged
        m_d5UTotalChi2 = m_KalKinFit->chisq();
    }
    else if (pISR != 0 && ptPi03 != 0) { // 5pi tagged
        m_d5TotalChi2 = m_KalKinFit->chisq();
    }
    // fit
    int offset = 6;
    if (ptPi03 != 0) {offset += 2;}
    m_lvFitPi1    = m_KalKinFit->pfit(0);
    m_lvFitPi2    = m_KalKinFit->pfit(1);
    m_lvFitPi01G1 = m_KalKinFit->pfit(2);
    m_lvFitPi01G2 = m_KalKinFit->pfit(3);
    m_lvFitPi02G1 = m_KalKinFit->pfit(4);
    m_lvFitPi02G2 = m_KalKinFit->pfit(5);
    m_lvFitISR    = m_KalKinFit->pfit(offset);
    m_lvFitPi01   = m_KalKinFit->pfit(2) + m_KalKinFit->pfit(3);
    m_lvFitPi02   = m_KalKinFit->pfit(4) + m_KalKinFit->pfit(5);
    if (ptPi03 != 0) {
        m_lvFitPi03G1 = m_KalKinFit->pfit(6);
        m_lvFitPi03G2 = m_KalKinFit->pfit(7);
        m_lvFitPi03   = m_KalKinFit->pfit(6) + m_KalKinFit->pfit(7);
    }
    // detector
    WTrackParameter wtpPiMinus (m_dMassPiCharged,m_pERTPiMinus->mdcKalTrack()->getZHelix(),m_pERTPiMinus->mdcKalTrack()->getZError());
    WTrackParameter wtpPiPlus  (m_dMassPiCharged,m_pERTPiPlus->mdcKalTrack()->getZHelix() ,m_pERTPiPlus->mdcKalTrack()->getZError());
    m_lvPi1    = wtpPiMinus.p();
    m_lvPi2    = wtpPiPlus.p();
    m_lvPi01   = m_aPi0List.Get4VecFromEMC(ptPi01->m_pTrack1,0.0) + m_aPi0List.Get4VecFromEMC(ptPi01->m_pTrack2,0.0);
    m_lvPi01G1 = m_aPi0List.Get4VecFromEMC(ptPi01->m_pTrack1,0.0);
    m_lvPi01G2 = m_aPi0List.Get4VecFromEMC(ptPi01->m_pTrack2,0.0);
    m_lvPi02   = m_aPi0List.Get4VecFromEMC(ptPi02->m_pTrack1,0.0) + m_aPi0List.Get4VecFromEMC(ptPi02->m_pTrack2,0.0);
    m_lvPi02G1 = m_aPi0List.Get4VecFromEMC(ptPi02->m_pTrack1,0.0);
    m_lvPi02G2 = m_aPi0List.Get4VecFromEMC(ptPi02->m_pTrack2,0.0);
    if (ptPi03 != 0) {
        m_lvPi03G1 = m_aPi0List.Get4VecFromEMC(ptPi03->m_pTrack1,0.0);
        m_lvPi03G2 = m_aPi0List.Get4VecFromEMC(ptPi03->m_pTrack2,0.0);
        m_lvPi03   = m_aPi0List.Get4VecFromEMC(ptPi03->m_pTrack1,0.0) + m_aPi0List.Get4VecFromEMC(ptPi03->m_pTrack2,0.0);
    }
    if (pISR == 0) {m_lvISR = m_lvFitISR;}
    else           {m_lvISR = m_aPi0List.Get4VecFromEMC(pISR,0.0);}
    m_aPhIndexList.clear();
    m_aPhIndexList.push_back (ptPi01->m_pTrack1);
    m_aPhIndexList.push_back (ptPi01->m_pTrack2);
    m_aPhIndexList.push_back (ptPi02->m_pTrack1);
    m_aPhIndexList.push_back (ptPi02->m_pTrack2);
    if (pISR != 0) {m_aPhIndexList.push_back (pISR);}
    if (ptPi03 != 0) {
        m_aPhIndexList.push_back (ptPi03->m_pTrack1);
        m_aPhIndexList.push_back (ptPi03->m_pTrack2);
    }
    // the lines below MUSST stay below the settings of the 4vectors!
    if (pISR == 0 && ptPi03 == 0) { // 4pi untagged
        if (m_KalKinFit->Fit(1)) {m_dUPi01Chi2 = m_KalKinFit->chisq(1);} else {m_dUPi01Chi2 = -5;}
        if (m_KalKinFit->Fit(2)) {m_dUPi02Chi2 = m_KalKinFit->chisq(2);} else {m_dUPi02Chi2 = -5;}
    }
    else if (pISR == 0 && ptPi03 != 0) { // 5pi untagged
        if (m_KalKinFit->Fit(1)) {m_d5UPi01Chi2 = m_KalKinFit->chisq(1);} else {m_d5UPi01Chi2 = -5;}
        if (m_KalKinFit->Fit(2)) {m_d5UPi02Chi2 = m_KalKinFit->chisq(2);} else {m_d5UPi02Chi2 = -5;}
        if (m_KalKinFit->Fit(3)) {m_d5UPi03Chi2 = m_KalKinFit->chisq(3);} else {m_d5UPi03Chi2 = -5;}
    }
    else if (pISR != 0 && ptPi03 == 0) { // 4pi tagged
        //if (m_KalKinFit->Fit(0)) {m_dEventChi2 = m_KalKinFit->chisq(0);} else {m_dEventChi2 = -5;}
        if (m_KalKinFit->Fit(1)) {m_dPi01Chi2 = m_KalKinFit->chisq(1);} else {m_dPi01Chi2  = -5;}
        if (m_KalKinFit->Fit(2)) {m_dPi02Chi2 = m_KalKinFit->chisq(2);} else {m_dPi02Chi2  = -5;}
    }
    else if (pISR != 0 && ptPi03 != 0) { // 5pi tagged
        if (m_KalKinFit->Fit(1)) {m_d5Pi01Chi2 = m_KalKinFit->chisq(1);} else {m_d5Pi01Chi2 = -5;}
        if (m_KalKinFit->Fit(2)) {m_d5Pi02Chi2 = m_KalKinFit->chisq(2);} else {m_d5Pi02Chi2 = -5;}
        if (m_KalKinFit->Fit(3)) {m_d5Pi03Chi2 = m_KalKinFit->chisq(3);} else {m_d5Pi03Chi2 = -5;}
    }
}
//----------------------------------------------------------------------------------------------------------



//----------------------------------------------------------------------------------------------------------
void Bes2Gamto2Pi0::RecordOtherPhotons(int iMode)
//----------------------------------------------------------------------------------------------------------
{

    for (int i=0;i<m_aPhIndexList.size();i++) {
        switch (iMode) {
        case 0: m_dClosestTrack  [i] = ClosestTrack(m_aPhIndexList[i]);  break;
        case 1: m_dUClosestTrack [i] = ClosestTrack(m_aPhIndexList[i]);  break;
        case 2: m_d5ClosestTrack [i] = ClosestTrack(m_aPhIndexList[i]);  break;
        case 3: m_d5UClosestTrack[i] = ClosestTrack(m_aPhIndexList[i]);  break;
        case 4: m_d5NClosestTrack[i] = ClosestTrack(m_aPhIndexList[i]);  break;
        case 5: m_d4NClosestTrack[i] = ClosestTrack(m_aPhIndexList[i]);  break;
        default: break;
        }
    }

    if ((iMode == 0 || iMode == 2) && m_iAnzGoodGamma == 5) {return;}
    if ((iMode == 1 || iMode == 3 || iMode == 4 || iMode == 5) && m_iAnzGoodGamma == 4) {return;}

    EvtRecTrackIterator    itERT;
    bool bWrite;
    int  iWrite = 0, iOffset = 4; // for 4piIsr untagged, 4pi -> iMode == 1,5
    if (iMode == 0) {iOffset = 5;} // 4piIsr tagged
    else if (iMode == 2) {iOffset = 7;} // 5piIsr tagged
    else if (iMode == 3 || iMode == 4) {iOffset = 6;} // 5piIsr untagged, 5pi

    for (itERT = m_itEndCharged;itERT != m_itEndNeutral;itERT ++) {
        if (!m_aPi0List.IsPhoton(*itERT)) {continue;}
        bWrite = true;
        for (int i=0;i<m_aPhIndexList.size();i++) {
          if (*itERT == m_aPhIndexList[i]) {bWrite = false; break;}
        }
        if (!bWrite) {continue;}
        switch (iMode) {
        case 0: m_Gammas  [iWrite+iOffset].FillFromEMC((*itERT)->emcShower()); m_dClosestTrack  [iWrite+iOffset] = ClosestTrack(*itERT); break;
        case 1: m_UGammas [iWrite+iOffset].FillFromEMC((*itERT)->emcShower()); m_dUClosestTrack [iWrite+iOffset] = ClosestTrack(*itERT); break;
        case 2: m_5Gammas [iWrite+iOffset].FillFromEMC((*itERT)->emcShower()); m_d5ClosestTrack [iWrite+iOffset] = ClosestTrack(*itERT); break;
        case 3: m_5UGammas[iWrite+iOffset].FillFromEMC((*itERT)->emcShower()); m_d5UClosestTrack[iWrite+iOffset] = ClosestTrack(*itERT); break;
        case 4: m_5NGammas[iWrite+iOffset].FillFromEMC((*itERT)->emcShower()); m_d5NClosestTrack[iWrite+iOffset] = ClosestTrack(*itERT); break;
        case 5: m_4NGammas[iWrite+iOffset].FillFromEMC((*itERT)->emcShower()); m_d4NClosestTrack[iWrite+iOffset] = ClosestTrack(*itERT); break;
        default: break;
        }
        iWrite ++;
        if (iWrite == GAMMAS+iOffset) {break;}
    }

}
//----------------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------------- 
void Bes2Gamto2Pi0::WriteMonteCarloTrurh ()
//----------------------------------------------------------------------------------------------------------
{
  SmartDataPtr<McParticleCol> mcParticles(eventSvc(),"/Event/MC/McParticleCol");
  if(!mcParticles) {
    m_iMCpi            = -2;
    m_iMCpi0           = -2;
    m_iMCisr           = -2;
    m_iMCfsr           = -2;
    m_iMCGammaFromJpsi = -2;
    m_iMCOtherGamma    = -2;
    m_iMCOther         = -2;
    m_bMCrecorded      = false;
    m_dmHadSysMCtruth  = -2;
    m_d5mHadSysMCtruth = -2;
    return;
  }  
   
  //int  iGamma; bool bFirstP0G  = true;
  int  iPin  = 0; bool bFirstISR = true;

  m_iMCpi            = 0;
  m_iMCpi0           = 0;
  m_iMCisr           = 0;
  m_iMCfsr           = 0;
  m_iMCGammaFromJpsi = 0;
  m_iMCOtherGamma    = 0;
  m_iMCOther         = 0;
  m_bMCrecorded      = true;
  m_dmHadSysMCtruth  = -1;
  m_d5mHadSysMCtruth = -1;
  // record MC particles
  //cout<<"---------- new event ---------------"<<endl;
  for( McParticleCol::iterator itMC = mcParticles->begin();itMC != mcParticles->end();itMC ++) {
    if ((*itMC)->particleProperty() == 23 || (*itMC)->particleProperty() == 91) {continue;} // Generator internal particle
    //if (abs((*itMC)->particleProperty()) < 22) {continue;} // leptons from pi decay
    if (abs((*itMC)->particleProperty()) < 11) {continue;} // Generator internal particle
    if ((*itMC)->particleProperty() == 11 && (*itMC)->mother().particleProperty() == 11) {continue;} // Generator internal particle
    switch((*itMC)->particleProperty()) {
    case   22: // gamma 
      //cout<<"mother: "<<(*itMC)->mother().particleProperty()<<" , primary: "<<(*itMC)->primaryParticle()<<endl;
      //m_imcGamma ++;
      //if ((*itMC)->mother().particleProperty() == 22 && (*itMC)->daughterList().size() == 0) {m_iMCisr ++;}
      //if ((*itMC)->mother().particleProperty() == 22 || (*itMC)->mother().particleProperty() == 443) {m_iMCisr ++;} // 443 = J/psi
      if      ((*itMC)->mother().particleProperty() == 22 ) {m_iMCisr ++;}         // ISR photon
      else if ((*itMC)->mother().particleProperty() == 443) {m_iMCGammaFromJpsi ++;} // 443 = J/psi -> gamma + anything
      else if ((*itMC)->mother().particleProperty() != 111) {m_iMCOtherGamma ++;}    // all other except from pi0 decay
      break;
    case  -22: m_iMCfsr ++;   break; // FSR gamma
    case  211: m_iMCpi ++;    break; // pi+
    case -211: m_iMCpi ++;    break; // pi-
    case  111: m_iMCpi0 ++;   break; // pi0
    default:
      // count only final state particles except from pi decays
      if (   abs((*itMC)->mother().particleProperty())          != 211 // from pi+- decay
          && (*itMC)->mother().particleProperty()               != 111 // from pi0  decay
          && abs((*itMC)->mother().mother().particleProperty()) != 211 // from pi+- -> mu+nu -> e+3nu
          && (*itMC)->daughterList().size()                     == 0   // does not decay -> final state particle
         ) {
          m_iMCOther ++;
          //cout<<"additional final state particle found: "<<ParticleName((*itMC)->particleProperty())<<" , mother: "<<ParticleName((*itMC)->mother().particleProperty())<<endl;
      }
      break;
    }
  }

  for( McParticleCol::iterator itMC = mcParticles->begin();itMC != mcParticles->end();itMC ++) {
    if (!(*itMC)->primaryParticle () && (*itMC)->particleProperty() == 22) {continue;}
    switch((*itMC)->particleProperty()) {
    case 22:// gamma
      {	 
	if (bFirstISR) {
	  bFirstISR      = false;
 	  m_lvmcISR1     = (*itMC)->initialFourMomentum();
	}
	else {
  	  m_lvmcISR2     = (*itMC)->initialFourMomentum();
	}
	break;
      } 
    case -211:// pi-
	m_lvmcPi1       = (*itMC)->initialFourMomentum();
	break;
    case 211:// pi+
	m_lvmcPi2       = (*itMC)->initialFourMomentum();
	break;
    case 111:// pi0      
      switch(iPin) {
      case 0: iPin ++;// bFirstP0G = true;
          m_lvmcPi01       = (*itMC)->initialFourMomentum();
          /*iGamma = 0;
          for (int j=0;j<(*itMC)->daughterList().size();j++) {
            if ((*itMC)->daughterList()[j]->particleProperty() == 22) {iGamma ++;}
          }
          if (iGamma == 2) {
            for (int i=0;i<(*itMC)->daughterList().size();i++) {
              if (bFirstP0G) {m_lvmcPi01G1 = (*itMC)->daughterList()[i]->initialFourMomentum(); bFirstP0G = false;}
              else           {m_lvmcPi01G2 = (*itMC)->daughterList()[i]->initialFourMomentum();}
            }
          }*/
        break;
      case 1: iPin ++;// bFirstP0G = true;
          m_lvmcPi02       = (*itMC)->initialFourMomentum();
          /*iGamma = 0;
          for (int k=0;k<(*itMC)->daughterList().size();k++) {
            if ((*itMC)->daughterList()[k]->particleProperty() == 22) {iGamma ++;}
          }
          if (iGamma == 2) {
            for (int j=0;j<(*itMC)->daughterList().size();j++) {
              if (bFirstP0G) {m_lvmcPi02G1 = (*itMC)->daughterList()[j]->initialFourMomentum(); bFirstP0G = false;}
              else           {m_lvmcPi02G2 = (*itMC)->daughterList()[j]->initialFourMomentum();}
                }
          }*/
        break;
      case 2: m_lvmcPi03 = (*itMC)->initialFourMomentum(); break;
      }
    break;
    }      
  }

  if (m_iMCpi == 2 && m_iMCpi0 == 2 && m_iMCisr >= 1 && m_iMCOther == 0 && m_iMCOtherGamma == 0 && m_iMCGammaFromJpsi == 0) {
    m_dmHadSysMCtruth  = (m_lvmcPi1+m_lvmcPi2+m_lvmcPi01+m_lvmcPi02).mag();
  }
  if (m_iMCpi == 2 && m_iMCpi0 == 3 && m_iMCisr >= 1 && m_iMCOther == 0 && m_iMCOtherGamma == 0 && m_iMCGammaFromJpsi == 0) {
      m_d5mHadSysMCtruth = (m_lvmcPi1+m_lvmcPi2+m_lvmcPi01+m_lvmcPi02+m_lvmcPi03).mag();
  }
}
//----------------------------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------------------------
void Bes2Gamto2Pi0::PrintMCDecayChain(int iMode)
//----------------------------------------------------------------------------------------------------------
{
    SmartDataPtr<McParticleCol> mcParticles(eventSvc(),"/Event/MC/McParticleCol");
    if(!mcParticles) {return;}

    TString sMode;
    switch (iMode) {
    case 0: sMode = "4PI TAGGED";   break;
    case 1: sMode = "4PI UNTAGGED"; break;
    case 2: sMode = "5PI TAGGED";   break;
    case 3: sMode = "5PI UNTAGGED"; break;
    case 4: sMode = "5PI"; break;
    default: sMode = "?"; break;
    }

    cout<<"---------- new event "+sMode+" ---------------"<<endl;
    cout<<"# pi= "<<m_iMCpi<<" , pi0= "<<m_iMCpi0<<" , ISR= "<<m_iMCisr<<" , other= "<<m_iMCOther<<" , otherG= "<<m_iMCOtherGamma<<" , Gjpsi= "<<m_iMCGammaFromJpsi<<endl;
    for( McParticleCol::iterator itMC = mcParticles->begin();itMC != mcParticles->end();itMC ++) {
        if ((*itMC)->particleProperty() == 23 || (*itMC)->particleProperty() == 91) {continue;} // Generator internal particle
        //if (abs((*itMC)->particleProperty()) < 11) {continue;} // Generator internal particle
        //if ((*itMC)->particleProperty() == 11 && (*itMC)->mother().particleProperty() == 11) {continue;} // Generator internal particle
        if (abs((*itMC)->particleProperty()) < 22) {continue;} // leptons from pi decay
        if (abs((*itMC)->particleProperty()) == 311) {continue;} // K0
        if ((*itMC)->particleProperty() == 22 && (*itMC)->mother().particleProperty() == 111) {continue;}
        cout<<"particle found: "<<ParticleName((*itMC)->particleProperty())<<" , mother: "<<ParticleName((*itMC)->mother().particleProperty())<<endl;
        if ((*itMC)->daughterList().size() != 0) {
            if ((*itMC)->particleProperty() != 111 && abs((*itMC)->particleProperty()) != 211) {
              for (int b=0;b<(*itMC)->daughterList().size();b++) {
                cout<<"    daughter: "<<ParticleName((*itMC)->daughterList()[b]->particleProperty())<<endl;
              }
            }
        }
    }

}
//----------------------------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------------------------
TString Bes2Gamto2Pi0::ParticleName(int iID)
//----------------------------------------------------------------------------------------------------------
{
  TString sText;

  switch (iID) {
    case  211: sText = "pi+"; break;
    case -211: sText = "pi-"; break;
    case  111: sText = "pi0"; break;
    case  223: sText = "omega"; break;
    case  221: sText = "eta"; break;
    case   22: sText = "gamma"; break;
    case  213: sText = "rho+"; break;
    case -213: sText = "rho-"; break;
    case  113: sText = "rho0"; break;
    case  9000111: sText = "a00"; break;
    case  9000211: sText = "a0+"; break;
    case -9000211: sText = "a0-"; break;
    case  10223: sText = "h1"; break;
    case  20113: sText = "a10"; break;
    case  20213: sText = "a1+"; break;
    case -20213: sText = "a1-"; break;
    case  9010221: sText = "f0(980)"; break;
    case  9000221: sText = "f0(600)"; break;
    case  225: sText = "f2(1270)"; break;
    case  115: sText = "a20"; break;
    case  215: sText = "a2+"; break;
    case -215: sText = "a2-"; break;
    case  10113: sText = "b10"; break;
    case  10213: sText = "b1+"; break;
    case -10213: sText = "b1-"; break;
    case  333: sText = "phi"; break;
    case  20223: sText = "f1"; break;
    case  331: sText = "etap"; break;
    case  321: sText = "K+"; break;
    case -321: sText = "K-"; break;
    case  311: sText = "K0"; break;
    case -311: sText = "K0b"; break;
    case  310: sText = "Ks"; break;
    case  130: sText = "Kl"; break;
    case  313: sText = "K*0"; break;
    case  323: sText = "K*+"; break;
    case -323: sText = "K*-"; break;
    case  441: sText = "etac"; break;
    case  443: sText = "Jpsi"; break;
    case  100443: sText = "Psi(2S)"; break;
    case  30443: sText = "Psi(3770)"; break;
    case  10441: sText = "Chi_c0"; break;
    case  20443: sText = "Chi_c1"; break;
    case  10443: sText = "h_c"; break;
    case  445: sText = "Chi_c2"; break;
    //case : sText = ""; break;
    default: sText = SSTR(iID); break;
  }

  return(sText);
}
//----------------------------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------------------------
double Bes2Gamto2Pi0::beam_energy(int runNo)
//----------------------------------------------------------------------------------------------------------
{
    double ecms = -1;

    if( runNo>=23463 && runNo<=23505 ){//psi(4040) dataset-I
      ecms = 4.0104;
    }
    else if( runNo>=23509 && runNo<24141 ){//psi(4040) dataset-II
      ecms = 4.0093;
    }
    else if( runNo>=-24141 && runNo<=-23463 ){// psi(4009)
      ecms = 4.009;
    }
    else if( runNo>=24151 && runNo<=24177 ){//psi(2S) data
      ecms = 3.686;
    }
    else if( runNo>=29677 && runNo<=30367 ){//Y(4260) data-I
      ecms = 4.26;
    }
    else if( runNo>=31561 && runNo<=31981 ){//Y(4260) data-II
      ecms = 4.26;
    }
    else if( runNo>=31327 && runNo<=31390 ){//4420 data
      ecms = 4.42;
    }
    else if( runNo>=31281 && runNo<=31325 ){//4390 data
      ecms = 4.39;
    }
    else if( runNo>=30616 && runNo<=31279 ){//Y(4360) data
      ecms = 4.36;
    }
    else if( runNo>=30492 && runNo<=30557 ){// 4310 data
      ecms = 4.31;
    }
    else if( runNo>=30438 && runNo<=30491 ){// 4230 data
      ecms = 4.23;
    }
    else if( runNo>=30372 && runNo<=30437 ){// 4190 data
      ecms = 4.19;
    }
    else if( runNo>=31983 && runNo<=32045 ){// 4210 data
      ecms = 4.21;
    }
    else if( runNo>=32046 && runNo<=32140 ){// 4220 data
      ecms = 4.22;
    }
    else if( runNo>=32141 && runNo<=32226 ){// 4245 data
      ecms = 4.245;
    }
    else if( runNo>=32239 && runNo<=32850 ){// new 4230 data-I
      ecms = 4.23;
    }
    else if( runNo>=32851 && runNo<=33484 ){// new 4230 data-II
      ecms = 4.228;
    }
    else if( runNo>=33490 && runNo<=33556 ){// 3810 data
      ecms = 3.81;
    }
    else if( runNo>=33571 && runNo<=33657 ){// 3900 data
      ecms = 3.90;
    }
    else if( runNo>=33659 && runNo<=33719 ){// 4090 data
      ecms = 4.09;
    }
    else if( runNo>=11397 && runNo<=23454 ){// psi(3770) data
      ecms = 3.773;
    }

    return ecms;
}
//----------------------------------------------------------------------------------------------------------


//*************************************************************************
// *****************************************************************
// ** A macro to create correlated Gaussian-distributed variables **
// *****************************************************************

//----------------------------------------------------------------------------------------------------------
void Bes2Gamto2Pi0::corset(HepSymMatrix &V, HepMatrix &C, int n)
//----------------------------------------------------------------------------------------------------------
{
  double sum;

  // Compute square root of matrix sigma
  for (int j=0; j<n; j++) {
    sum = 0;
    for (int k=0; k<j; k++) {
      sum = sum + C[j][k]*C[j][k];
      
    }
    C[j][j] = sqrt(abs(V[j][j] - sum));
    // Off Diagonal terms
    for (int i=j+1; i<n; i++) {
      sum = 0;
      for (int k=0; k<j; k++) {
	sum = sum + C[i][k]*C[j][k];
      }
      C[i][j] = (V[i][j] - sum)/C[j][j];
    }
  }
}
//----------------------------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------------------------
void Bes2Gamto2Pi0::corgen(HepMatrix &C, HepVector &x, int n)
//----------------------------------------------------------------------------------------------------------
{
  // corgen(): generates a set of n random numbers Gaussian-distributed with covariance
  // matrix V (V = C*C') and mean values zero.
  int i,j;
  int nmax = 100;
  
  if (n > nmax ) {
    printf("Error in corgen: array overflown");
  }
  
  double tmp[3];
  for(int p = 0 ; p < n; p ++){
      tmp[p] = gRandom->Gaus(0,1);
//cout<<"tmp["<<p<<"]="<<tmp[p]<<endl;
  }
  for ( i=0; i<n; i++) {
    x[i] = 0.0;
    for (j=0; j<=i; j++) {
      x[i] = x[i]+C[i][j]*tmp[j];
    }
  }
}
//----------------------------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------------------------
void Bes2Gamto2Pi0::calibration(RecMdcKalTrack *trk , HepVector &wtrk_zHel, int n )
//----------------------------------------------------------------------------------------------------------
{
   if (!m_bMCrecorded) {
     wtrk_zHel[0] = trk->getZHelix()[0];
     wtrk_zHel[1] = trk->getZHelix()[1];
     wtrk_zHel[2] = trk->getZHelix()[2];
     wtrk_zHel[3] = trk->getZHelix()[3];
     wtrk_zHel[4] = trk->getZHelix()[4];
     return;
   }

   HepVector calerr_d2[8];
   HepVector calmean_d2[8];
   for (int i=0;i<8;i++) {
     calerr_d2[i]  = HepVector(5,0);
     calmean_d2[i] = HepVector(5,0);
   }
   // pi+
   calerr_d2[0][0] = 1.0;
   calerr_d2[0][1] = 1.16;//1.05*1.188;
   calerr_d2[0][2] = 1.22;//1.042*1.195;
   calerr_d2[0][3] = 1.0;
   calerr_d2[0][4] = 1.08;//1.0128*1.134;
   // pi-
   calerr_d2[1][0] = 1.0;
   calerr_d2[1][1] = 1.19;//1.038*1.173;
   calerr_d2[1][2] = 1.17;//1.0364*1.182;
   calerr_d2[1][3] = 1.0;
   calerr_d2[1][4] = 1.08;//1.004*1.129;
   // k+
   calerr_d2[2][0] = 1.0;
   calerr_d2[2][1] = 1.0094*1.165;
   calerr_d2[2][2] = 1.055*1.205;
   calerr_d2[2][3] = 1.0;
   calerr_d2[2][4] = 1.008*1.132;
   // k-
   calerr_d2[3][0] = 1.0;
   calerr_d2[3][1] = 1.02*1.167;
   calerr_d2[3][2] = 1.044*1.188;
   calerr_d2[3][3] = 1.0;
   calerr_d2[3][4] = 0.9969*1.115;
   // mu+
   calerr_d2[4][0] = 1.0;
   calerr_d2[4][1] = 1.19;//1.273;
   calerr_d2[4][2] = 1.09;//1.218;
   calerr_d2[4][3] = 1.0;
   calerr_d2[4][4] = 1.09;//1.255;
   // mu-
   calerr_d2[5][0] = 1.0;
   calerr_d2[5][1] = 1.18;//1.275;
   calerr_d2[5][2] = 1.11;//1.220;
   calerr_d2[5][3] = 1.0;
   calerr_d2[5][4] = 1.08;//1.264;
   // e+
   calerr_d2[6][0] = 1.0;
   calerr_d2[6][1] = 1.10;//1.207;
   calerr_d2[6][2] = 1.01;//1.117;
   calerr_d2[6][3] = 1.0;
   calerr_d2[6][4] = 0.98;//1.186;
   // e-
   calerr_d2[7][0] = 1.0;
   calerr_d2[7][1] = 1.10;//1.202;
   calerr_d2[7][2] = 1.04;//1.101;
   calerr_d2[7][3] = 1.0;
   calerr_d2[7][4] = 1.02;//1.179;
   
   for (int i=0;i<8;i++) {
     for (int j=0;j<5;j++) {
       calmean_d2[i][j] = 0;
     }
   }

   // change wtrk parameters
   HepSymMatrix w_zerr(5,0);
   w_zerr = trk->getZError();
   HepSymMatrix w_zcal(3,0);

   w_zcal[0][0] = (pow(calerr_d2[n][1],2)-1)*w_zerr[1][1];
   w_zcal[1][1] = (pow(calerr_d2[n][2],2)-1)*w_zerr[2][2];
   w_zcal[2][2] = (pow(calerr_d2[n][4],2)-1)*w_zerr[4][4];

   HepMatrix w_zerrc(3,3,0);
   corset(w_zcal,w_zerrc,3);
   HepVector w_zgen(3,0);
   corgen(w_zerrc,w_zgen,3);
     
   wtrk_zHel[0] = trk->getZHelix()[0];
   wtrk_zHel[1] = trk->getZHelix()[1]+calmean_d2[n][1]*sqrt(w_zerr[1][1])+w_zgen[0];
   wtrk_zHel[2] = trk->getZHelix()[2]+calmean_d2[n][2]*sqrt(w_zerr[2][2])+w_zgen[1];
   wtrk_zHel[3] = trk->getZHelix()[3];
   wtrk_zHel[4] = trk->getZHelix()[4]+calmean_d2[n][4]*sqrt(w_zerr[4][4])+w_zgen[2];
      
}
//----------------------------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------------------------
double Bes2Gamto2Pi0::ClosestTrack(EvtRecTrack* pCurNeutral)
//----------------------------------------------------------------------------------------------------------
{
    EvtRecTrackIterator  itERT;
    RecEmcShower*        pEmcTrk = pCurNeutral->emcShower();
    Hep3Vector           vEmcpos(pEmcTrk->x(), pEmcTrk->y(), pEmcTrk->z());
    double               fAngCur = 200.0, fAngMin = 200.0;

    // find the nearest charged track
    for (itERT  = m_itBegin;itERT != m_itEndCharged;itERT ++) {
        if(!(*itERT)->isExtTrackValid()) {continue;}//{cout<<"invalid ext track"<<endl;continue;}
        if((*itERT)->extTrack()->emcVolumeNumber() == -1) {continue;}//{cout<<"invalid emc volume nr"<<endl;continue;}
        fAngCur = vEmcpos.angle((*itERT)->extTrack()->emcPosition())*180/3.1415926;
        if (fAngCur < fAngMin) {fAngMin = fAngCur;}
    }
    if (fAngCur == 200.0) {fAngCur = -1;}
    //cout<<"angle= "<<fAngCur<<endl;
    return (fAngMin);
}
//----------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------- 
bool Bes2Gamto2Pi0::IsFromInteractionPoint (EvtRecTrack* pCur)
//---------------------------------------------------------------------------------------------------------- 
{
    if (!pCur->isMdcKalTrackValid()) {return(false);}
    // Constructor with pivot, helix parameter a, and its error matrix.
    HepPoint3D   point0(0,0,0);
    VFHelix      helixipP(point0,pCur->mdcKalTrack()->helix(),pCur->mdcKalTrack()->err());
    helixipP.pivot(m_hp3IP);
    if (fabs(helixipP.a()[0]) < m_dCylRad && fabs(helixipP.a()[3]) < m_dCylHeight) {
      return (true);
    }
    return(false);
}
//---------------------------------------------------------------------------------------------------------- 


//---------------------------------------------------------------------------------------------------------- 
double Bes2Gamto2Pi0::IPdist (EvtRecTrack* pCur)
//----------------------------------------------------------------------------------------------------------
{
    // Constructor with pivot, helix parameter a, and its error matrix.
    HepPoint3D   point0(0,0,0);
    VFHelix      helixipP(point0,pCur->mdcKalTrack()->helix(),pCur->mdcKalTrack()->err());
    helixipP.pivot(m_hp3IP);
    return (sqrt(pow(fabs(helixipP.a()[0]),2)+pow(helixipP.a()[3],2)));
}
//---------------------------------------------------------------------------------------------------------- 

//---------------------------------------------------------------------------------------------------------- 
bool Bes2Gamto2Pi0::FillParticleList()
//----------------------------------------------------------------------------------------------------------
{
    EvtRecTrackIterator    itERT;

    // clear lists
    m_pERTPiMinus = 0;
    m_pERTPiPlus  = 0;
    m_aISRphotons.clear();
    m_pISRphoton  = 0;

    // count good photons
    int iGood = 0;
    for (itERT = m_itEndCharged;itERT != m_itEndNeutral;itERT ++) {
        if (m_aPi0List.IsPhoton(*itERT)) {iGood ++;}
    }
    if (iGood < 4 || iGood > m_dMaxGoodPh) {m_iPhotonSkip ++; return (false);}
    m_iAnzGoodGamma  = iGood;
    m_iUAnzGoodGamma = iGood;
    m_i5AnzGoodGamma  = iGood;
    m_i5UAnzGoodGamma = iGood;
    m_i5NAnzGoodGamma = iGood;
    m_i4NAnzGoodGamma = iGood;
    // count charged tracks in fiducial volume
    int iGoodP = 0, iGoodM = 0, iP = 0, iM = 0;
    for (itERT =  m_itBegin;itERT != m_itEndCharged;itERT ++) {
        if (!(*itERT)->isMdcKalTrackValid()) {continue;}
        if (IsFromInteractionPoint(*itERT)) {
            if ((*itERT)->mdcKalTrack()->charge() < 0) {iGoodM ++;}
            else                                       {iGoodP ++;}
        }
        else {
            if ((*itERT)->mdcKalTrack()->charge() < 0) {iM ++;}
            else                                       {iP ++;}
        }
    }
    if (iGoodP != 1 || iGoodM != 1) {m_iPionSkip ++; return (false);}
    m_iAnzGoodP   = iP;
    m_iAnzGoodM   = iM;
    m_iUAnzGoodP  = iP;
    m_iUAnzGoodM  = iM;
    m_i5UAnzGoodP = iP;
    m_i5UAnzGoodM = iM;
    m_i5AnzGoodP  = iP;
    m_i5AnzGoodM  = iM;
    m_i5NAnzGoodP = iP;
    m_i5NAnzGoodM = iM;
    m_i4NAnzGoodP = iP;
    m_i4NAnzGoodM = iM;

    // get charged Pions
    if (!FindCharged()) {m_iPionSkip ++; return false;}

    // find pi0 -> 2gamma
    m_aPi0List.FillList();
    if (m_aPi0List.m_aPi0TracksTable.size() > m_dMaxPi0Combs) {m_iPi0Skip ++; return false;}
    m_aPi0List.CreatedValid2Combs ();
    m_aPi0List.CreatedValid3Combs ();
    // insufficient number of 2pi0 candidates
    if (m_aPi0List.m_aValidCombs2.size() < 1) {m_iPi0Skip ++; return false;}

    // ISR photon
    for (itERT = m_itEndCharged;itERT != m_itEndNeutral;itERT ++) {
        if (!m_aPi0List.IsPhoton(*itERT) ||
        (*itERT)->emcShower()->energy() < m_dISRcutLow ||
        (*itERT)->emcShower()->energy() > (m_dCMSEnergy+0.1)/2.0
        ) {continue;}
        m_aISRphotons.push_back(*itERT);
    }

    // record combinatorics
    m_iAnzPi0Tracks   = m_aPi0List.m_aPi0TracksTable.size();
    m_iUAnzPi0Tracks  = m_aPi0List.m_aPi0TracksTable.size();
    m_iAnz2Combs      = m_aPi0List.m_aValidCombs2.size();
    m_iUAnz2Combs     = m_aPi0List.m_aValidCombs2.size();
    m_iAnz3Combs      = m_aPi0List.m_aValidCombs3.size();
    m_iUAnz3Combs     = m_aPi0List.m_aValidCombs3.size();
    m_i5AnzPi0Tracks  = m_aPi0List.m_aPi0TracksTable.size();
    m_i5UAnzPi0Tracks = m_aPi0List.m_aPi0TracksTable.size();
    m_i5Anz2Combs     = m_aPi0List.m_aValidCombs2.size();
    m_i5UAnz2Combs    = m_aPi0List.m_aValidCombs2.size();
    m_i5Anz3Combs     = m_aPi0List.m_aValidCombs3.size();
    m_i5UAnz3Combs    = m_aPi0List.m_aValidCombs3.size();
    m_i5NAnzPi0Tracks = m_aPi0List.m_aPi0TracksTable.size();
    m_i5NAnz2Combs    = m_aPi0List.m_aValidCombs2.size();
    m_i5NAnz3Combs    = m_aPi0List.m_aValidCombs3.size();
    m_i4NAnzPi0Tracks = m_aPi0List.m_aPi0TracksTable.size();
    m_i4NAnz2Combs    = m_aPi0List.m_aValidCombs2.size();
    m_i4NAnz3Combs    = m_aPi0List.m_aValidCombs3.size();

    return true;
}
//----------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------- 
bool Bes2Gamto2Pi0::FindCharged()
//----------------------------------------------------------------------------------------------------------
{
    EvtRecTrackIterator  itERT;
    double       dDist, dMinDistP = 100, dMinDistN = 100;
    double       dEoP,dEoPP,dEoPN;

    // loop over all charged events
    for (itERT  = m_itBegin;itERT != m_itEndCharged;itERT ++) {
      if (!(*itERT)->isMdcKalTrackValid()) {continue;}
      if (!IsFromInteractionPoint(*itERT)) {continue;} // skip if track origin is not in the virtual box
      //if ((*itERT)->isMucTrackValid() && (*itERT)->mucTrack()->depth() >= 40) {continue;}
      if ((*itERT)->isEmcShowerValid()) {
          // calculate EoP
          dEoP = (*itERT)->emcShower()->energy()/(*itERT)->mdcTrack()->p3().mag();
      }
      else {dEoP = -2;} // no valid emc track, but track will be accepted
      if (dEoP > m_dEoPcut) {continue;}
      // find closest P, N
      dDist = IPdist (*itERT);
      if ((*itERT)->mdcKalTrack()->charge() < 0 && dDist < dMinDistN) {
          dMinDistN     = dDist;
          m_pERTPiMinus = *itERT;
          dEoPN         = dEoP;
      }
      else if ((*itERT)->mdcKalTrack()->charge() > 0 && dDist < dMinDistP) {
          dMinDistP     = dDist;
          m_pERTPiPlus  = *itERT;
          dEoPP         = dEoP;
      }
    }
    if (m_pERTPiMinus == 0 || m_pERTPiPlus == 0) {return false;}
    // make PID and record
    AddToPionLists (m_pERTPiMinus,dEoPN);
    AddToPionLists (m_pERTPiPlus ,dEoPP);
    FitVertexPos();

    return true;
}
//----------------------------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------------------------
void Bes2Gamto2Pi0::FitVertexPos ()
//----------------------------------------------------------------------------------------------------------
{
    double dChisq;
    bool bFit = false;
    HepPoint3D vx(0., 0., 0.);
    HepSymMatrix Evx(3, 0);
    double bx = 1E+6;
    Evx[0][0] = bx*bx;
    Evx[1][1] = bx*bx;
    Evx[2][2] = bx*bx;
    VertexParameter vxpar;
    vxpar.setVx(vx);
    vxpar.setEvx(Evx);

    WTrackParameter wtpPiMinus (m_dMassPiCharged,m_pERTPiMinus->mdcKalTrack()->getZHelix(),m_pERTPiMinus->mdcKalTrack()->getZError());
    WTrackParameter wtpPiPlus  (m_dMassPiCharged,m_pERTPiPlus->mdcKalTrack ()->getZHelix(),m_pERTPiPlus->mdcKalTrack ()->getZError());

    m_Vtxfit->init();
    m_Vtxfit->setChisqCut(9998.0, 0.01);
    m_Vtxfit->AddTrack(0,wtpPiMinus);
    m_Vtxfit->AddTrack(1,wtpPiPlus);
    m_Vtxfit->AddVertex(0, vxpar,0, 1);
    bFit = m_Vtxfit->Fit();
    if (!bFit) {return;}
    m_Vtxfit->Swim(0);
    dChisq = m_Vtxfit->chisq(0);

    // chisq
    m_dVtxChi2   = dChisq;
    m_dUVtxChi2  = dChisq;
    m_d5VtxChi2  = dChisq;
    m_d5UVtxChi2 = dChisq;
    m_d5NVtxChi2 = dChisq;
    m_d4NVtxChi2 = dChisq;
    // vertex pos
    m_dVtxR = sqrt(pow(m_Vtxfit->Vx(0)[0],2)+pow(m_Vtxfit->Vx(0)[1],2));
    m_dVtxPosX   = m_Vtxfit->Vx(0)[0];
    m_dVtxPosY   = m_Vtxfit->Vx(0)[1];
    m_dVtxPosZ   = m_Vtxfit->Vx(0)[2];
    m_dUVtxPosX  = m_Vtxfit->Vx(0)[0];
    m_dUVtxPosY  = m_Vtxfit->Vx(0)[1];
    m_dUVtxPosZ  = m_Vtxfit->Vx(0)[2];
    m_d5VtxPosX  = m_Vtxfit->Vx(0)[0];
    m_d5VtxPosY  = m_Vtxfit->Vx(0)[1];
    m_d5VtxPosZ  = m_Vtxfit->Vx(0)[2];
    m_d5UVtxPosX = m_Vtxfit->Vx(0)[0];
    m_d5UVtxPosY = m_Vtxfit->Vx(0)[1];
    m_d5UVtxPosZ = m_Vtxfit->Vx(0)[2];
    m_d5NVtxPosX = m_Vtxfit->Vx(0)[0];
    m_d5NVtxPosY = m_Vtxfit->Vx(0)[1];
    m_d5NVtxPosZ = m_Vtxfit->Vx(0)[2];
    m_d4NVtxPosX = m_Vtxfit->Vx(0)[0];
    m_d4NVtxPosY = m_Vtxfit->Vx(0)[1];
    m_d4NVtxPosZ = m_Vtxfit->Vx(0)[2];

}
//----------------------------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------------------------
void Bes2Gamto2Pi0::AddToPionLists (EvtRecTrack* pTrack, double dEoP)
//----------------------------------------------------------------------------------------------------------
{
    HepPoint3D   point0(0,0,0);
    if (pTrack->mdcKalTrack()->charge() < 0) { // pi -
        //m_aERTPiMinus.push_back (pTrack);
        m_dPi1EoP           = dEoP;
        m_dUPi1EoP          = dEoP;
        if (pTrack->isMucTrackValid()) {
            m_dPi1MucDepth  = pTrack->mucTrack()->depth();
            m_dUPi1MucDepth = pTrack->mucTrack()->depth();
        }
        if (pTrack->isMdcKalTrackValid()) {
            VFHelix      helixipP(point0,pTrack->mdcKalTrack()->helix(),pTrack->mdcKalTrack()->err());
            helixipP.pivot(m_hp3IP);
            m_dDistRm  = fabs(helixipP.a()[0]);
            m_dUDistRm = fabs(helixipP.a()[0]);
            m_dDistZm  = fabs(helixipP.a()[3]);
            m_dUDistZm = fabs(helixipP.a()[3]);
            m_d5DistRm  = fabs(helixipP.a()[0]);
            m_d5UDistRm = fabs(helixipP.a()[0]);
            m_d5DistZm  = fabs(helixipP.a()[3]);
            m_d5UDistZm = fabs(helixipP.a()[3]);
            m_d5NDistZm  = fabs(helixipP.a()[3]);
            m_d4NDistZm = fabs(helixipP.a()[3]);
            m_d5NDistRm  = fabs(helixipP.a()[0]);
            m_d4NDistRm = fabs(helixipP.a()[0]);
        }
    }
    else { // pi+
        //m_aERTPiPlus.push_back (pTrack);
        m_dPi2EoP           = dEoP;
        m_dUPi2EoP          = dEoP;
        if (pTrack->isMucTrackValid()) {
          m_dPi2MucDepth  = pTrack->mucTrack()->depth();
          m_dUPi2MucDepth = pTrack->mucTrack()->depth();
        }
        if (pTrack->isMdcKalTrackValid()) {
            VFHelix      helixipP(point0,pTrack->mdcKalTrack()->helix(),pTrack->mdcKalTrack()->err());
            helixipP.pivot(m_hp3IP);
            m_dDistRp  = fabs(helixipP.a()[0]);
            m_dUDistRp = fabs(helixipP.a()[0]);
            m_dDistZp  = fabs(helixipP.a()[3]);
            m_dUDistZp = fabs(helixipP.a()[3]);
            m_d5DistRp  = fabs(helixipP.a()[0]);
            m_d5UDistRp = fabs(helixipP.a()[0]);
            m_d5DistZp  = fabs(helixipP.a()[3]);
            m_d5UDistZp = fabs(helixipP.a()[3]);
            m_d5NDistZp  = fabs(helixipP.a()[3]);
            m_d4NDistZp = fabs(helixipP.a()[3]);
            m_d5NDistRp  = fabs(helixipP.a()[0]);
            m_d4NDistRp = fabs(helixipP.a()[0]);
        }
    }
}
//----------------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------------- 
void Bes2Gamto2Pi0::AddAllToKinFit(cPi0Tracks* ptPi01,cPi0Tracks* ptPi02,cPi0Tracks* ptPi03,double iISR,EvtRecTrack* pISR, bool b5C)
//---------------------------------------------------------------------------------------------------------- 
{
    int offset = 0;
    HepVector w_zHel(5,0);

    m_KalKinFit->init();
    m_KalKinFit->setChisqCut(m_dChisqMaxValue);
    // add charged tracks
  #if 1
    if (ptPi03 != 0) {calibration(m_pERTPiMinus->mdcKalTrack(),w_zHel,1);}
    else {w_zHel = m_pERTPiMinus->mdcKalTrack()->getZHelix();}
    WTrackParameter wtpPiMinus (m_dMassPiCharged,w_zHel,m_pERTPiMinus->mdcKalTrack()->getZError());
    if (ptPi03 != 0) {calibration(m_pERTPiPlus ->mdcKalTrack(),w_zHel,0);}
    else {w_zHel = m_pERTPiPlus->mdcKalTrack()->getZHelix();}
    WTrackParameter wtpPiPlus  (m_dMassPiCharged,w_zHel,m_pERTPiPlus ->mdcKalTrack()->getZError());
  #else
    WTrackParameter wtpPiMinus (m_dMassPiCharged,m_pERTPiMinus->mdcKalTrack()->getZHelix(),m_pERTPiMinus->mdcKalTrack()->getZError());
    WTrackParameter wtpPiPlus  (m_dMassPiCharged,m_pERTPiPlus->mdcKalTrack ()->getZHelix(),m_pERTPiPlus->mdcKalTrack ()->getZError());
  #endif
    m_KalKinFit->AddTrack(0,wtpPiMinus);
    m_KalKinFit->AddTrack(1,wtpPiPlus);

    // fit with gammas and pi0-mass constraints
    m_KalKinFit->AddTrack(2,0.0,ptPi01->m_pTrack1->emcShower());
    m_KalKinFit->AddTrack(3,0.0,ptPi01->m_pTrack2->emcShower());
    if (ptPi02 != 0) { // fit with 2pi0
      m_KalKinFit->AddTrack(4,0.0,ptPi02->m_pTrack1->emcShower());
      m_KalKinFit->AddTrack(5,0.0,ptPi02->m_pTrack2->emcShower());
      offset += 2;
    }
    // wPi03 = 0 -> fit with 2pi0
    if (ptPi03 != 0) { // fit with 3pi0
      m_KalKinFit->AddTrack(4+offset,0.0,ptPi03->m_pTrack1->emcShower());
      m_KalKinFit->AddTrack(5+offset,0.0,ptPi03->m_pTrack2->emcShower());
      offset += 2;
    }
    // iISR == -1 fit without ISR-Photon, no additional requirements need to be added to the fit
    if (iISR == 0.0) { // untagged fit
      if (ptPi02 == 0 && ptPi03 == 0) {
        m_KalKinFit->AddMissTrack(4+offset,0.0, m_lvBoost-wtpPiMinus.p()-wtpPiMinus.p()
                                               -m_aPi0List.Get4VecFromEMC(ptPi01->m_pTrack1,0.0)
                                               -m_aPi0List.Get4VecFromEMC(ptPi01->m_pTrack2,0.0));
      }
      else if (ptPi02 != 0 && ptPi03 == 0) {
        m_KalKinFit->AddMissTrack(4+offset,0.0,m_lvBoost-wtpPiMinus.p()-wtpPiMinus.p()
                                               -m_aPi0List.Get4VecFromEMC(ptPi01->m_pTrack1,0.0)
                                               -m_aPi0List.Get4VecFromEMC(ptPi01->m_pTrack2,0.0)
                                               -m_aPi0List.Get4VecFromEMC(ptPi02->m_pTrack1,0.0)
                                               -m_aPi0List.Get4VecFromEMC(ptPi02->m_pTrack2,0.0));
      }
      else if (ptPi02 != 0 && ptPi03 != 0) {
        m_KalKinFit->AddMissTrack(4+offset,0.0 ,m_lvBoost-wtpPiMinus.p()-wtpPiMinus.p()
                                               -m_aPi0List.Get4VecFromEMC(ptPi01->m_pTrack1,0.0)
                                               -m_aPi0List.Get4VecFromEMC(ptPi01->m_pTrack2,0.0)
                                               -m_aPi0List.Get4VecFromEMC(ptPi02->m_pTrack1,0.0)
                                               -m_aPi0List.Get4VecFromEMC(ptPi02->m_pTrack2,0.0)
                                               -m_aPi0List.Get4VecFromEMC(ptPi03->m_pTrack1,0.0)
                                               -m_aPi0List.Get4VecFromEMC(ptPi03->m_pTrack2,0.0));
      }
    }
    else if (iISR >= 1 && pISR != 0) { // tagged fit
      m_KalKinFit->AddTrack(4+offset,0.0,pISR->emcShower());
    }
    else if (iISR == -2 && pISR != 0) { // pi untagged fit
      m_KalKinFit->AddTrack(4+offset,0.0,pISR->emcShower());
      m_KalKinFit->AddMissTrack(5+offset,0.0,m_lvBoost-wtpPiMinus.p()-wtpPiMinus.p()
                                             -m_aPi0List.Get4VecFromEMC(ptPi01->m_pTrack1,0.0)
                                             -m_aPi0List.Get4VecFromEMC(ptPi01->m_pTrack2,0.0)
                                             -m_aPi0List.Get4VecFromEMC(ptPi02->m_pTrack1,0.0)
                                             -m_aPi0List.Get4VecFromEMC(ptPi02->m_pTrack2,0.0)
                                             -m_aPi0List.Get4VecFromEMC(pISR,0.0)
                               );
    }
    m_KalKinFit->AddFourMomentum(0,m_lvBoost);
    m_KalKinFit->AddResonance(1,m_dMassPi0,2,3);
    if (ptPi02 != 0 && !b5C) {m_KalKinFit->AddResonance(2,m_dMassPi0,4,5);}
    if (ptPi03 != 0 || (iISR == -2 && pISR != 0)) {m_KalKinFit->AddResonance(3,m_dMassPi0,6,7);}

}
//---------------------------------------------------------------------------------------------------------- 

//---------------------------------------------------------------------------------------------------------- 
void Bes2Gamto2Pi0::Try4PiFit()
//---------------------------------------------------------------------------------------------------------- 
{
    if (m_pERTPiMinus == 0 || m_pERTPiPlus == 0 || m_aPi0List.m_aValidCombs2.size() < 1) {
        m_d4PiNonRadChi2    = -5;
        m_dU4PiNonRadChi2   = -5;
        m_d4PiNonRadChi2    = -5;
        return;
    }

    double dChisq = m_dChisqMaxValue, dChisqMin = m_dChisqMaxValue;
    bool bDone = false;

    //cout<<"new 4pi fit"<<endl;
    for(int k=0;k<m_aPi0List.m_aValidCombs2.size();k++) {
        // 2pi2pi0 fit
        AddAllToKinFit(m_aPi0List.m_aValidCombs2[k].m_pTrack1,m_aPi0List.m_aValidCombs2[k].m_pTrack2,0,-1,0);
        if (m_KalKinFit->Fit()) {
            dChisq = m_KalKinFit->chisq();
            if (dChisq > 0 && dChisq < dChisqMin) {
              m_d4PiNonRadChi2  = dChisq;
              m_dU4PiNonRadChi2 = dChisq;
              m_d4NPiNonRadChi2 = dChisq;
              dChisqMin         = dChisq;
              PrepareToWriteNonrad(m_aPi0List.m_aValidCombs2[k].m_pTrack1,m_aPi0List.m_aValidCombs2[k].m_pTrack2,0);
              bDone = true;
              //cout<<"4pi fit succesfull: chi2= "<<dChisq<<endl;
            }
        }
    }
    if (bDone) {
        m_4NPi01.Fill(&m_lvPi01);
        m_4NPi02.Fill(&m_lvPi02);
        m_4NPi1 .Fill(&m_lvPi1);
        m_4NPi2 .Fill(&m_lvPi2);
        m_4NGammas[0].Fill(&m_lvPi01G1);
        m_4NGammas[1].Fill(&m_lvPi01G2);
        m_4NGammas[2].Fill(&m_lvPi02G1);
        m_4NGammas[3].Fill(&m_lvPi02G2);
        m_4NFitPi01.Fill(&m_lvFitPi01);
        m_4NFitPi02.Fill(&m_lvFitPi02);
        m_4NFitPi1.Fill(&m_lvFitPi1);
        m_4NFitPi2.Fill(&m_lvFitPi2);
        m_4NFitGammas[0].Fill(&m_lvFitPi01G1);
        m_4NFitGammas[1].Fill(&m_lvFitPi01G2);
        m_4NFitGammas[2].Fill(&m_lvFitPi02G1);
        m_4NFitGammas[3].Fill(&m_lvFitPi02G2);
        KsKpiVeto(5);
        RecordOtherPhotons(5);
    }
    else { // no 4pi nonrad fit recorded
        m_d4PiNonRadChi2  = -5;
        m_dU4PiNonRadChi2 = -5;
        m_d4PiNonRadChi2  = -5;
    }

    m_iBgrVeto   = -2;
    m_iUBgrVeto  = -2;
    m_i4NBgrVeto = -2;


}
//---------------------------------------------------------------------------------------------------------- 


//---------------------------------------------------------------------------------------------------------- 
void Bes2Gamto2Pi0::Try5PiFit()
//---------------------------------------------------------------------------------------------------------- 
{
    if (m_pERTPiMinus == 0 || m_pERTPiPlus == 0 || m_aPi0List.m_aValidCombs3.size() < 1) {
        m_d5UTotalChi2    = -5;
        m_d5TotalChi2     = -5;
        m_d5PiNonRadChi2  = -5;
        m_dU5PiNonRadChi2 = -5;
        m_d5Pi01Chi2      = -5;
        m_d5Pi02Chi2      = -5;
        m_d5Pi03Chi2      = -5;
        m_d5PiChi2        = -5;
        m_dU5PiChi2       = -5;
        m_d5NPiNonRadChi2 = -5;
        return;
    }

    double dChisqMin,dChisq = m_dChisqMaxValue, dMhad;
    bool bDone = false;

    dChisqMin = m_dChisqMaxValue;
    for (int k=0;k<m_aPi0List.m_aValidCombs3.size();k++) {
       // 2pi3pi0 fit
       AddAllToKinFit(m_aPi0List.m_aValidCombs3[k].m_pTrack1,m_aPi0List.m_aValidCombs3[k].m_pTrack2,m_aPi0List.m_aValidCombs3[k].m_pTrack3,-1.0,0);
       if (m_KalKinFit->Fit()) {
         dChisq = m_KalKinFit->chisq();
         if (dChisq > 0 && dChisq < dChisqMin) {
           m_d5PiNonRadChi2  = dChisq;
           m_dU5PiNonRadChi2 = dChisq;
           m_d5NPiNonRadChi2 = dChisq;
           dChisqMin         = dChisq;
           PrepareToWriteNonrad(m_aPi0List.m_aValidCombs3[k].m_pTrack1,m_aPi0List.m_aValidCombs3[k].m_pTrack2,m_aPi0List.m_aValidCombs3[k].m_pTrack3);
           bDone = true;
         }
       }
    }
    if (bDone) {
        m_5NPi01.Fill(&m_lvPi01);
        m_5NPi02.Fill(&m_lvPi02);
        m_5NPi03.Fill(&m_lvPi03);
        m_5NPi1 .Fill(&m_lvPi1);
        m_5NPi2 .Fill(&m_lvPi2);
        m_5NGammas[0].Fill(&m_lvPi01G1);
        m_5NGammas[1].Fill(&m_lvPi01G2);
        m_5NGammas[2].Fill(&m_lvPi02G1);
        m_5NGammas[3].Fill(&m_lvPi02G2);
        m_5NGammas[4].Fill(&m_lvPi03G1);
        m_5NGammas[5].Fill(&m_lvPi03G2);
        m_5NFitPi01.Fill(&m_lvFitPi01);
        m_5NFitPi02.Fill(&m_lvFitPi02);
        m_5NFitPi03.Fill(&m_lvFitPi03);
        m_5NFitPi1.Fill(&m_lvFitPi1);
        m_5NFitPi2.Fill(&m_lvFitPi2);
        m_5NFitGammas[0].Fill(&m_lvFitPi01G1);
        m_5NFitGammas[1].Fill(&m_lvFitPi01G2);
        m_5NFitGammas[2].Fill(&m_lvFitPi02G1);
        m_5NFitGammas[3].Fill(&m_lvFitPi02G2);
        m_5NFitGammas[4].Fill(&m_lvFitPi03G1);
        m_5NFitGammas[5].Fill(&m_lvFitPi03G2);
        KsKpiVeto(4);
        RecordOtherPhotons(4);
/*
        float fM3pi1 = (m_lvFitPi1+m_lvFitPi2+m_lvFitPi01).mag();
        float fM3pi2 = (m_lvFitPi1+m_lvFitPi2+m_lvFitPi02).mag();
        float fM3pi3 = (m_lvFitPi1+m_lvFitPi2+m_lvFitPi03).mag();
        if (fM3pi1 > 3.13 || fM3pi2 > 3.13 || fM3pi3 > 3.13) {PrintMCDecayChain(4);}*/
    }

    dChisqMin = m_dChisqMaxValue;
    for (int k=0;k<m_aPi0List.m_aValidCombs3.size();k++) {
       // 2pi3pi0gamma untagged fit
       AddAllToKinFit(m_aPi0List.m_aValidCombs3[k].m_pTrack1,m_aPi0List.m_aValidCombs3[k].m_pTrack2,m_aPi0List.m_aValidCombs3[k].m_pTrack3,0.0,0);
       if (m_KalKinFit->Fit()) {
           dChisq = m_KalKinFit->chisq();
           if (dChisq > 0 && dChisq < dChisqMin)  {
           dMhad  = (m_KalKinFit->pfit(0)+m_KalKinFit->pfit(1)+m_KalKinFit->pfit(2)+m_KalKinFit->pfit(3)+
                     m_KalKinFit->pfit(4)+m_KalKinFit->pfit(5)+m_KalKinFit->pfit(6)+m_KalKinFit->pfit(7)).mag();
           m_dU5PiMhad        = dMhad;
           m_d5UsqrSHadSysFit = dMhad;
           dChisqMin          = dChisq;
           m_dU5PiChi2        = dChisq;
           m_d5NPiIsrUChi2    = dChisq;
           PrepareToWrite(m_aPi0List.m_aValidCombs3[k].m_pTrack1,m_aPi0List.m_aValidCombs3[k].m_pTrack2,m_aPi0List.m_aValidCombs3[k].m_pTrack3,0);
           bDone = true;
         }
       }
    }
    if (bDone) {
        m_5UPi01.Fill(&m_lvPi01);
        m_5UPi02.Fill(&m_lvPi02);
        m_5UPi03.Fill(&m_lvPi03);
        m_5UPi1 .Fill(&m_lvPi1);
        m_5UPi2 .Fill(&m_lvPi2);
        m_5UGammas[0].Fill(&m_lvPi01G1);
        m_5UGammas[1].Fill(&m_lvPi01G2);
        m_5UGammas[2].Fill(&m_lvPi02G1);
        m_5UGammas[3].Fill(&m_lvPi02G2);
        m_5UGammas[4].Fill(&m_lvPi03G1);
        m_5UGammas[5].Fill(&m_lvPi03G2);
        m_5UFitPi01.Fill(&m_lvFitPi01);
        m_5UFitPi02.Fill(&m_lvFitPi02);
        m_5UFitPi03.Fill(&m_lvFitPi03);
        m_5UFitPi1.Fill(&m_lvFitPi1);
        m_5UFitPi2.Fill(&m_lvFitPi2);
        m_5UFitGammas[0].Fill(&m_lvFitPi01G1);
        m_5UFitGammas[1].Fill(&m_lvFitPi01G2);
        m_5UFitGammas[2].Fill(&m_lvFitPi02G1);
        m_5UFitGammas[3].Fill(&m_lvFitPi02G2);
        m_5UFitGammas[4].Fill(&m_lvFitPi03G1);
        m_5UFitGammas[5].Fill(&m_lvFitPi03G2);
        m_5UFitGammas[6].Fill(&m_lvFitISR);
        m_U5PiISR.Fill(&m_lvFitISR);
        KsKpiVeto(3);
        RecordOtherPhotons(3);
        //if (m_iMCpi == 2 && m_iMCpi0 == 3 && m_iMCisr > 0 && m_iMCOther == 0 && m_iMCOtherGamma == 0) {PrintMCDecayChain(3);}
    }

    bDone     = false;
    dChisqMin = m_dChisqMaxValue;
    for (int k=0;k<m_aPi0List.m_aValidCombs3.size();k++) {
       // 2pi3pi0gamma tagged fit
       for(int l=0;l<m_aISRphotons.size();l++) {
          if (m_aPi0List.IsUsedForPi0(m_aISRphotons[l]
                                     ,m_aPi0List.m_aValidCombs3[k].m_pTrack1
                                     ,m_aPi0List.m_aValidCombs3[k].m_pTrack2,m_aPi0List.m_aValidCombs3[k].m_pTrack3
                                     )
             ) {continue;}
          AddAllToKinFit(m_aPi0List.m_aValidCombs3[k].m_pTrack1
                        ,m_aPi0List.m_aValidCombs3[k].m_pTrack2
                        ,m_aPi0List.m_aValidCombs3[k].m_pTrack3,1,m_aISRphotons[l]
                        );
          if (m_KalKinFit->Fit()) {
            dChisq = m_KalKinFit->chisq();
            if (dChisq > 0 && dChisq < dChisqMin)  {
              dMhad  = (m_KalKinFit->pfit(0)+m_KalKinFit->pfit(1)+m_KalKinFit->pfit(2)+m_KalKinFit->pfit(3)+
                        m_KalKinFit->pfit(4)+m_KalKinFit->pfit(5)+m_KalKinFit->pfit(6)+m_KalKinFit->pfit(7)).mag();
              m_d5PiMhad        = dMhad;
              m_d5sqrSHadSysFit = dMhad;
              dChisqMin         = dChisq;
              m_d5PiChi2        = dChisq;
              m_d5NPiIsrTChi2   = dChisq;
              PrepareToWrite(m_aPi0List.m_aValidCombs3[k].m_pTrack1,m_aPi0List.m_aValidCombs3[k].m_pTrack2,m_aPi0List.m_aValidCombs3[k].m_pTrack3,m_aISRphotons[l]);
              bDone = true;
            }
          }
       }
    }
    if (bDone) {
        m_5Pi01.Fill(&m_lvPi01);
        m_5Pi02.Fill(&m_lvPi02);
        m_5Pi03.Fill(&m_lvPi03);
        m_5Pi1 .Fill(&m_lvPi1);
        m_5Pi2 .Fill(&m_lvPi2);
        m_5Gammas[0].Fill(&m_lvPi01G1);
        m_5Gammas[1].Fill(&m_lvPi01G2);
        m_5Gammas[2].Fill(&m_lvPi02G1);
        m_5Gammas[3].Fill(&m_lvPi02G2);
        m_5Gammas[4].Fill(&m_lvPi03G1);
        m_5Gammas[5].Fill(&m_lvPi03G2);
        m_5Gammas[6].Fill(&m_lvISR);
        m_5FitPi01.Fill(&m_lvFitPi01);
        m_5FitPi02.Fill(&m_lvFitPi02);
        m_5FitPi03.Fill(&m_lvFitPi03);
        m_5FitPi1.Fill(&m_lvFitPi1);
        m_5FitPi2.Fill(&m_lvFitPi2);
        m_5FitGammas[0].Fill(&m_lvFitPi01G1);
        m_5FitGammas[1].Fill(&m_lvFitPi01G2);
        m_5FitGammas[2].Fill(&m_lvFitPi02G1);
        m_5FitGammas[3].Fill(&m_lvFitPi02G2);
        m_5FitGammas[4].Fill(&m_lvFitPi03G1);
        m_5FitGammas[5].Fill(&m_lvFitPi03G2);
        m_5FitGammas[6].Fill(&m_lvFitISR);
        m_5PiISR.Fill(&m_lvFitISR);
        RecordOtherPhotons(2);
        KsKpiVeto(2);
        //if (m_iMCpi == 2 && m_iMCpi0 == 3 && m_iMCisr > 0 && m_iMCOther == 0 && m_iMCOtherGamma == 0) {PrintMCDecayChain(2);}
    }

    m_iBgrVeto   = -2;
    m_iUBgrVeto  = -2;
    m_i5BgrVeto  = -2;
    m_i5UBgrVeto = -2;
    m_i5NBgrVeto = -2;
    // no 5pi fit recorded
    if (m_d5TotalChi2     == m_dChisqMaxValue) {m_d5TotalChi2     = -5; m_d5PiChi2  = -5;}
    if (m_d5UTotalChi2    == m_dChisqMaxValue) {m_d5UTotalChi2    = -5; m_dU5PiChi2 = -5;}
    if (m_d5PiNonRadChi2  == m_dChisqMaxValue) {m_d5PiNonRadChi2  = -5;}
    if (m_dU5PiNonRadChi2 == m_dChisqMaxValue) {m_dU5PiNonRadChi2 = -5;}
    if (m_d5NPiNonRadChi2 == m_dChisqMaxValue) {m_d5NPiNonRadChi2 = -5;}

}
//---------------------------------------------------------------------------------------------------------- 


//---------------------------------------------------------------------------------------------------------- 
bool Bes2Gamto2Pi0::Find2pi02picTagged()
//----------------------------------------------------------------------------------------------------------
{
    m_dUMHadTagged = -1;
    m_TaggedISR.Clear();
    // 2 charged pions, 2 neutral pions and at least 1 ISR photon
    if (m_pERTPiMinus == 0 || m_pERTPiPlus == 0 || m_aPi0List.m_aValidCombs2.size() < 1 || m_aISRphotons.size() < 1)
       {m_dTotalChi2 = -5;/* m_dEventChi2 = -5;*/ m_dPi01Chi2 = -5; m_dPi02Chi2 = -5; return false;}
    int iDone = -1;

    // try tagged fit for all possibilities of isr and valid2combs
    for(int k=0;k<m_aPi0List.m_aValidCombs2.size();k++) {
      for(int l=0;l<m_aISRphotons.size();l++) {
        if (m_aPi0List.IsUsedForPi0(m_aISRphotons[l],m_aPi0List.m_aValidCombs2[k].m_pTrack1,m_aPi0List.m_aValidCombs2[k].m_pTrack2,0)) {continue;}
        // normal fit
        AddAllToKinFit(m_aPi0List.m_aValidCombs2[k].m_pTrack2,m_aPi0List.m_aValidCombs2[k].m_pTrack1,0,1.0,m_aISRphotons[l]);
        if (m_KalKinFit->Fit()) {
            if (m_KalKinFit->chisq() > 0 && m_KalKinFit->chisq() < m_dTotalChi2) {
              PrepareToWrite (m_aPi0List.m_aValidCombs2[k].m_pTrack2,m_aPi0List.m_aValidCombs2[k].m_pTrack1,0,m_aISRphotons[l]);
              m_d4NPiIsrTChi2  = m_KalKinFit->chisq();
              iDone  = 1;
            }
        }
      }
    }
    if (iDone != 1) {m_dTotalChi2 = -5;/* m_dEventChi2 = -5; */m_dPi01Chi2 = -5; m_dPi02Chi2 = -5; return false;}
    double dMhadSysFit = (m_lvFitPi1+m_lvFitPi2+m_lvFitPi01+m_lvFitPi02).mag();
    double dMhadSys    = (m_lvPi1+m_lvPi2+m_lvPi01+m_lvPi02).mag();
    if (dMhadSys > m_dCMSEnergy || dMhadSys < 0.3) {m_dTotalChi2 = -5;/* m_dEventChi2 = -5;*/ m_dPi01Chi2 = -5; m_dPi02Chi2 = -5; return false;}
    if (m_lvPi1.e() >= 0.55*m_dCMSEnergy || m_lvPi2.e() >= 0.55*m_dCMSEnergy){m_dTotalChi2 = -5;/* m_dEventChi2 = -5;*/ m_dPi01Chi2 = -5; m_dPi02Chi2 = -5; return false;}
    m_Pi01.Fill(&m_lvPi01);
    m_Pi02.Fill(&m_lvPi02);
    m_Pi1 .Fill(&m_lvPi1);
    m_Pi2 .Fill(&m_lvPi2);
    m_Gammas[0].Fill(&m_lvPi01G1);
    m_Gammas[1].Fill(&m_lvPi01G2);
    m_Gammas[2].Fill(&m_lvPi02G1);
    m_Gammas[3].Fill(&m_lvPi02G2);
    m_Gammas[4].Fill(&m_lvISR);
    m_FitPi01.Fill(&m_lvFitPi01);
    m_FitPi02.Fill(&m_lvFitPi02);
    m_FitPi1.Fill(&m_lvFitPi1);
    m_FitPi2.Fill(&m_lvFitPi2);
    m_FitGammas[0].Fill(&m_lvFitPi01G1);
    m_FitGammas[1].Fill(&m_lvFitPi01G2);
    m_FitGammas[2].Fill(&m_lvFitPi02G1);
    m_FitGammas[3].Fill(&m_lvFitPi02G2);
    m_FitGammas[4].Fill(&m_lvFitISR);

    RecordOtherPhotons(0);
    // had sys
    m_dsqrSHadSys    = dMhadSys;
    m_dsqrSHadSysFit = dMhadSysFit;
    m_dUMHadTagged   = dMhadSysFit;
    m_TaggedISR.Fill(&m_lvFitISR);

    // Delta E_cms
    //m_dDetDeltaEcms  = (m_lvBoost-m_lvPi1-m_lvPi2-m_lvPi01-m_lvPi02-m_lvISR).mag();
    // decide bgrVeto
    m_iBgrVeto   = 0;
    m_i5BgrVeto  = 0;
    m_i5NBgrVeto = 0;
    m_i4NBgrVeto = 0;
    double bestChisq = m_dTotalChi2/DOF4PIGT;
    if (m_d5TotalChi2      > 0 && m_d5TotalChi2/DOF5PIGU     < bestChisq) {m_iBgrVeto = 5; m_i5BgrVeto = 5; m_i5NBgrVeto = 5; m_i4NBgrVeto = 5;}
    if (m_d5PiNonRadChi2   > 0 && m_d5PiNonRadChi2/DOF5PI    < bestChisq) {m_iBgrVeto = 6; m_i5BgrVeto = 6; m_i5NBgrVeto = 6; m_i4NBgrVeto = 6;}
    if (m_d4PiNonRadChi2   > 0 && m_d4PiNonRadChi2/DOF4PI    < bestChisq) {m_iBgrVeto = 2; m_i5BgrVeto = 2; m_i5NBgrVeto = 2; m_i4NBgrVeto = 2;}

    KsKpiVeto(0);

    //cout << "<< tagged fit successfully: chi2= "<<m_KalKinFit->chisq()<<"  >>" << endl;
    //if (m_iMCpi == 2 && m_iMCpi0 == 2 && m_iMCOther == 0 && m_iMCOtherGamma > 0) {PrintMCDecayChain(0);}
    m_d5Nchisq4piT = bestChisq*DOF4PIGT;
    m_d5Nmhad4piT  = dMhadSysFit;

    return true;
}
//----------------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------------- 
bool Bes2Gamto2Pi0::Find2pi02picUntagged()
//----------------------------------------------------------------------------------------------------------
{ 
    // 2 charged pions, 2 neutral pions
    if (m_pERTPiMinus == 0 || m_pERTPiPlus == 0 || m_aPi0List.m_aValidCombs2.size() < 1)
       {m_dUTotalChi2 = -5;/* m_dUEventChi2 = -5; */m_dUPi01Chi2 = -5; m_dUPi02Chi2 = -5; return false;}
    bool bDone = false;

    // try untagged fit
    for(int k=0;k<m_aPi0List.m_aValidCombs2.size();k++) {
      // normal fit
      AddAllToKinFit(m_aPi0List.m_aValidCombs2[k].m_pTrack1,m_aPi0List.m_aValidCombs2[k].m_pTrack2,0,0.0,0);
      if (!m_KalKinFit->Fit()) {continue;}
      if (m_KalKinFit->chisq() > 0 && m_KalKinFit->chisq() < m_dUTotalChi2) {
        PrepareToWrite (m_aPi0List.m_aValidCombs2[k].m_pTrack1,m_aPi0List.m_aValidCombs2[k].m_pTrack2,0,0);
        m_d4NPiIsrUChi2  = m_KalKinFit->chisq();
        bDone = true;
      }
    }
    if (!bDone) {m_dUTotalChi2 = -5;/* m_dUEventChi2 = -5;*/ m_dUPi01Chi2 = -5; m_dUPi02Chi2 = -5; return false;}
    double dMhadSysFit = (m_lvFitPi1+m_lvFitPi2+m_lvFitPi01+m_lvFitPi02).mag();
    double dMhadSys    = (m_lvPi1+m_lvPi2+m_lvPi01+m_lvPi02).mag();
    //cout<<"dMhadSys= "<<dMhadSys<<" , dMhadSysFit= "<<dMhadSysFit<<endl;
    if (dMhadSys > m_dCMSEnergy || dMhadSys < 0.3)
       {m_dUTotalChi2 = -5;/* m_dUEventChi2 = -5;*/ m_dUPi01Chi2 = -5; m_dUPi02Chi2 = -5; return false;}
    if (m_lvFitISR.e() > 0.6*m_dCMSEnergy || m_lvFitISR.e() < m_dISRcutLow)
       {m_dUTotalChi2 = -5;/* m_dUEventChi2 = -5;*/ m_dUPi01Chi2 = -5; m_dUPi02Chi2 = -5; return false;}
    //if (abs(cos(m_lvFitISR.theta())) < m_dCosThetaISR)
    //   {m_dUTotalChi2 = -5;/* m_dUEventChi2 = -5;*/ m_dUPi01Chi2 = -5; m_dUPi02Chi2 = -5; return false;}
    if (m_lvPi1.e() >= 0.6*m_dCMSEnergy || m_lvPi2.e() >= 0.6*m_dCMSEnergy)
       {m_dUTotalChi2 = -5;/* m_dUEventChi2 = -5;*/ m_dUPi01Chi2 = -5; m_dUPi02Chi2 = -5; return false;}
    m_UPi01.Fill(&m_lvPi01);
    m_UPi02.Fill(&m_lvPi02);
    m_UPi1 .Fill(&m_lvPi1);
    m_UPi2 .Fill(&m_lvPi2);
    m_UGammas[0].Fill(&m_lvPi01G1);
    m_UGammas[1].Fill(&m_lvPi01G2);
    m_UGammas[2].Fill(&m_lvPi02G1);
    m_UGammas[3].Fill(&m_lvPi02G2);
    //m_UGammas[4].Fill(&m_lvISR);
    m_UFitPi01.Fill(&m_lvFitPi01);
    m_UFitPi02.Fill(&m_lvFitPi02);
    m_UFitPi1.Fill(&m_lvFitPi1);
    m_UFitPi2.Fill(&m_lvFitPi2);
    m_UFitGammas[0].Fill(&m_lvFitPi01G1);
    m_UFitGammas[1].Fill(&m_lvFitPi01G2);
    m_UFitGammas[2].Fill(&m_lvFitPi02G1);
    m_UFitGammas[3].Fill(&m_lvFitPi02G2);
    m_UFitGammas[4].Fill(&m_lvFitISR);
    RecordOtherPhotons(1);
    // had sys
    m_dUsqrSHadSys    = dMhadSys;
    m_dUsqrSHadSysFit = dMhadSysFit;
    // Delta E_cms
    //m_dUDetDeltaEcms = (m_lvBoost-m_lvPi1-m_lvPi2-m_lvPi01-m_lvPi02-m_lvFitISR).mag();
    // decide bgrVeto
    m_iUBgrVeto  = 0;
    m_i5UBgrVeto = 0;
    m_i5NBgrVeto = 0;
    m_i4NBgrVeto = 0;
    double bestChisq = m_dUTotalChi2/DOF4PIGU;
    if (m_d5UTotalChi2      > 0 && m_d5UTotalChi2/DOF5PIGU  < bestChisq) {m_iBgrVeto = 5; m_i5BgrVeto = 5; m_i5NBgrVeto = 5; m_i4NBgrVeto = 5;}
    if (m_dU5PiNonRadChi2   > 0 && m_dU5PiNonRadChi2/DOF5PI < bestChisq) {m_iBgrVeto = 6; m_i5BgrVeto = 6; m_i5NBgrVeto = 6; m_i4NBgrVeto = 6;}
    if (m_dU4PiNonRadChi2   > 0 && m_dU4PiNonRadChi2/DOF4PI < bestChisq) {m_iBgrVeto = 2; m_i5BgrVeto = 2; m_i5NBgrVeto = 2; m_i4NBgrVeto = 2;}

    KsKpiVeto(1);

    //cout << "<< untagged fit successfully written: chi2= " << m_KalKinFit->chisq(0) << " >>" << endl;
    //if ((m_iMCpi == 2 && m_iMCpi0 == 2 && m_iMCOther == 0 && m_iMCOtherGamma > 0) || (m_iMCpi == 1 && m_iMCOther == 1)) {PrintMCDecayChain(1);}
    m_d5Nchisq4piU = bestChisq*DOF4PIGU;
    m_d5Nmhad4piU = dMhadSysFit;


    return true;
}
//----------------------------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------------------------
void Bes2Gamto2Pi0::KsKpiVeto(int iMode)
//----------------------------------------------------------------------------------------------------------
{   
    if (iMode < 2 || iMode == 5) {// 4pi (+isr)
        float fMpi0pi0 = (m_lvFitPi01+m_lvFitPi02).mag();
        if (!(fMpi0pi0 > 0.45 && fMpi0pi0 < 0.55) && m_dVtxR < 2) {return;}
    }
    else {
        float fMpi0pi0[3];
        fMpi0pi0[0] = (m_lvFitPi01+m_lvFitPi02).mag();
        fMpi0pi0[1] = (m_lvFitPi01+m_lvFitPi03).mag();
        fMpi0pi0[2] = (m_lvFitPi02+m_lvFitPi03).mag();
        if (!((fMpi0pi0[0] > 0.45 && fMpi0pi0[0] < 0.55) || (fMpi0pi0[1] > 0.45 && fMpi0pi0[1] < 0.55) || (fMpi0pi0[2] > 0.45 && fMpi0pi0[2] < 0.55)) && m_dVtxR < 2) {return;}
    }

    PerformPID(m_pERTPiMinus);
    if(m_PID->IsPidInfoValid()) {
        switch (iMode) {
        case 0:
            m_dProb1Pi = m_PID->probPion();
            m_dProb1K  = m_PID->probKaon();
            m_dProb1P  = m_PID->probProton();
            break;
        case 1:
            m_dUProb1Pi = m_PID->probPion();
            m_dUProb1K  = m_PID->probKaon();
            m_dUProb1P  = m_PID->probProton();
            break;
        case 2:
            m_d5Prob1Pi = m_PID->probPion();
            m_d5Prob1K  = m_PID->probKaon();
            m_d5Prob1P  = m_PID->probProton();
            break;
        case 3:
            m_d5UProb1Pi = m_PID->probPion();
            m_d5UProb1K  = m_PID->probKaon();
            m_d5UProb1P  = m_PID->probProton();
            break;
        case 4:
            m_d5NProb1Pi = m_PID->probPion();
            m_d5NProb1K  = m_PID->probKaon();
            m_d5NProb1P  = m_PID->probProton();
            break;
        case 5:
            m_d4NProb1Pi = m_PID->probPion();
            m_d4NProb1K  = m_PID->probKaon();
            m_d4NProb1P  = m_PID->probProton();
            break;
        default: break;
        }
    }

    PerformPID(m_pERTPiPlus);
    if(m_PID->IsPidInfoValid()) {
        switch (iMode) {
        case 0:
            m_dProb2Pi = m_PID->probPion();
            m_dProb2K  = m_PID->probKaon();
            m_dProb2P  = m_PID->probProton();
            break;
        case 1:
            m_dUProb2Pi = m_PID->probPion();
            m_dUProb2K  = m_PID->probKaon();
            m_dUProb2P  = m_PID->probProton();
            break;
        case 2:
            m_d5Prob2Pi = m_PID->probPion();
            m_d5Prob2K  = m_PID->probKaon();
            m_d5Prob2P  = m_PID->probProton();
            break;
        case 3:
            m_d5UProb2Pi = m_PID->probPion();
            m_d5UProb2K  = m_PID->probKaon();
            m_d5UProb2P  = m_PID->probProton();
            break;
        case 4:
            m_d5NProb2Pi = m_PID->probPion();
            m_d5NProb2K  = m_PID->probKaon();
            m_d5NProb2P  = m_PID->probProton();
            break;
        case 5:
            m_d4NProb2Pi = m_PID->probPion();
            m_d4NProb2K  = m_PID->probKaon();
            m_d4NProb2P  = m_PID->probProton();
            break;
        default: break;
        }
    }

}
//----------------------------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------------------------
void Bes2Gamto2Pi0::PerformPID(EvtRecTrack* pTrack)
//----------------------------------------------------------------------------------------------------------
{
    m_PID->init();
    m_PID->setMethod(m_PID->methodProbability());// | m_PID->methodLikelihood());
    //m_PID->setChiMinCut(4.0);
    m_PID->usePidSys(m_PID->useDedx());
    m_PID->usePidSys(m_PID->useEmc());
    m_PID->usePidSys(m_PID->useTof());
    m_PID->usePidSys(m_PID->useTof1());
    m_PID->usePidSys(m_PID->useTof2());
    m_PID->usePidSys(m_PID->useTofE());
    //m_PID->usePidSys(m_PID->useTofQ());
    //m_PID->usePidSys(m_PID->useTofC());
    //m_PID->usePidSys(m_PID->useMuc());
    m_PID->identify(m_PID->onlyPion() | m_PID->onlyKaon() | m_PID->onlyProton());
    m_PID->setRecTrack(pTrack);
    m_PID->calculate();
}
//----------------------------------------------------------------------------------------------------------
