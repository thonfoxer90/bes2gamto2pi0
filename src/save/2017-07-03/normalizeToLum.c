#include <TFile.h>
#include <TDirectory.h>
#include <TTree.h>
#include <TChain.h>
#include <TCanvas.h>
#include <TPad.h>
#include <TH1.h>
#include <TH2.h>
#include <THStack.h>
#include <TLine.h>
#include <stdio.h>
#include <string.h>
#include <iostream> //std::cout
#include <sstream>
#include <fstream> //std::ifstream,ofstream
#include <vector>
#include <algorithm> //std::sort
#include <TKey.h>
#include <TList.h>
#include <TStyle.h>
#include <TROOT.h>
#include <TLegend.h>
#include <TLegendEntry.h>

#define SSTR( x ) static_cast< std::ostringstream & >( \
        ( std::ostringstream() << std::dec << x ) ).str()
				
using namespace std;

const double nanotofemto=1E6;
const double nanotopico=1E6;

string m_option;

double m_scale =0;

vector <string> Tokenize(string str, char delim){
vector <string> strsplit;

stringstream ss;
string strtmp;

ss.str(str);				
		while (getline(ss, strtmp, delim)) {
 		strsplit.push_back(strtmp);
		}	

return strsplit;		
}


double GetCrossSection(string logfile,string generator)
{double XSec=1;

ifstream readList(logfile.c_str());

if(readList.is_open())
	{
	while(readList.good())
		{
		string strline;
		getline(readList,strline);
		//cout << strline << endl;
			if(generator=="Galuga")
				{
				if(strline.find("Total e+e- cross section [nb]") != string::npos)
					{
					stringstream ss;
					ss.str(strline);					
					string strtmp;
					vector <string> strsplit;
					while (getline(ss, strtmp, '=')) {
 						strsplit.push_back(strtmp);
						}
					
					XSec=atof((strsplit.back()).c_str());
					printf("XSEC = %e nb \n", XSec);
					//Convert to fb
					XSec=XSec*nanotofemto;
					printf("XSEC = %e fb \n", XSec);
					break;
					}
				}
				
		}
	}

return XSec;	
}


void GetNGenAndCrossSection(string logfile,string generator, int& NGen, double &XSec)
{

ifstream readList(logfile.c_str());

if(readList.is_open())
	{
	while(readList.good())
		{
		string strline;
		getline(readList,strline);
		//cout << strline << endl;
			if(generator=="Galuga")
				{
				if(strline.find("Total e+e- cross section [nb]") != string::npos)
					{
					stringstream ss;
					ss.str(strline);					
					string strtmp;
					vector <string> strsplit;
					while (getline(ss, strtmp, '=')) {
 						strsplit.push_back(strtmp);
						}
					
					XSec=atof((strsplit.back()).c_str());
					printf("XSEC = %e nb \n", XSec);
					//Convert to fb
					XSec=XSec*nanotofemto;
					printf("XSEC = %e fb \n", XSec);
					}
				if(strline.find("time to generate") != string::npos)
					{
					stringstream ss;
					ss.str(strline);					
					string strtmp;
					vector <string> strsplit;
					while (getline(ss, strtmp, ' ')) {
 						strsplit.push_back(strtmp);
						}
						
					//for(int k=0;k<strsplit.size();k++)
					//	{
					//	printf("%d %s \n",k, strsplit[k].c_str());
					//	}
				  
					NGen=atoi(strsplit[9].c_str());
					//cout << NGen << endl;
					//sleep(4);
					}
				}
			if(generator=="Ekhara")
				{
				int NOccurences=0; //Number of occurences of same string
				
				if(strline.find("[nb]") != string::npos)
					{
					NOccurences++;
					if(NOccurences>1) continue;
					stringstream ss;
					ss.str(strline);					
					string strtmp;
					vector <string> strsplit;
					while (getline(ss, strtmp, '=')) {
 						strsplit.push_back(strtmp);
						}
					
					ss.clear();
					ss.str(strsplit.back());
					printf("GetCrossSection : %s \n",strsplit.back().c_str());
					vector <string> strvalsplit;
					while(getline(ss,strtmp, '+')){
					strvalsplit.push_back(strtmp);
					}
					XSec=atof((strvalsplit[0]).c_str());
					printf("XSEC = %e nb \n", XSec);
					//Convert to fb
					XSec=XSec*nanotofemto;
					printf("XSEC = %e fb \n", XSec);
					}
				if(strline.find("Events output:") != string::npos)
					{
					stringstream ss;
					ss.str(strline);					
					string strtmp;
					vector <string> strsplit;
					while (getline(ss, strtmp, ':')) {
 						strsplit.push_back(strtmp);
						}
					cout << strsplit.back() << endl;
					NGen=atoi(strsplit.back().c_str());	
					}
				}
		}
	}

}

void GetNGenAndCrossSectionFromList(string filelist,string generator, int& NGenSum, double &XSecMean)
{

ifstream readList(filelist.c_str());
int Nline=0;
if(readList.is_open())
	{
	while(readList.good())
		{
		Nline++;
		string strline;
		getline(readList,strline);
		int NGen;
		double XSec;
		//cout << NGenSum<< endl;
		GetNGenAndCrossSection(strline,generator,NGen,XSec);
		//cout << NGenSum<< endl;
		NGenSum=NGenSum+NGen;
		XSecMean=XSecMean+XSec;
		}
	}
	
XSecMean=XSecMean/Nline;	
printf("NGenSum= %d NLine=%d XSecMean = %f \n",NGenSum,Nline,XSecMean);	

}

void normalizeToLum(string inputstr, string option, string outputname){
gROOT->SetBatch();


if(inputstr.find(".txt") != string::npos){
	//List of .root files
}
else{ 
	//Single root file
}

//Parse the option file
m_option=option;

vector <string> optionsplit = Tokenize(m_option,' ');
for(int k=0;k<optionsplit.size();k++){
	cout << optionsplit[k] << endl;
	vector <string> strsplit = Tokenize(optionsplit[k],'=');
	string arg=strsplit[0];
	string val=strsplit[1];
	cout << arg << " " << val << endl;
	cout << endl;
	if(arg.find("Scale") != string::npos){
	m_scale = atof(val.c_str());	
	}
	else if(inputstr.find("XSec") != string::npos){
	
	}
}

}




