

#include "Bes2Gamto2Pi0/Bes2Gamto2Pi0.h"
#include <sstream>

#define SSTR( x ) dynamic_cast< std::ostringstream & >( \
            ( std::ostringstream() << std::dec << x ) ).str()

using namespace Event;

///////////////////////////////////////////////////////////////////////////
//
//Analysis for e+ e- -> e+ e- pi0 pi0 at BES-III
//Author: Brice Garillon, Institut fuer Kernphysik, Uni Mainz, Germany 
//
///////////////////////////////////////////////////////////////////////////

//----------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------
// class Bes2Gamto2Pi0
//----------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------
Bes2Gamto2Pi0::Bes2Gamto2Pi0(const std::string& name, ISvcLocator* pSvcLocator) :
   Algorithm(name, pSvcLocator)
//----------------------------------------------------------------------------------------------------------
{
  //Declare the properties
  //declareProperty("MonteCarloAnalysis", m_MC_Analysis = false);
  //m_dCMSEnergy         = 3.773;       // psi(3770)
  m_dMassPi0           = 0.134976;    // PDG 2010
  m_dMassPiCharged     = 0.139570;    // PDG 2010
  m_dISRcutLow         = 0.025;
  m_dEoPcut            = 1.2;//0.8;
  m_dPi0GcutHigh       = 2.5;//1.4;
  m_dPhEnergyCutBarrel = 0.025;
  m_dPhEnergyCutEndCap = 0.05;
  m_dMaxPi0Combs       = 20;
  m_dMaxGoodPh         = 15;
  m_dCylRad            = 4;//2.5;  // for point of closest approach
  m_dCylHeight         = 20;//15.0; // for point of closest approach
  m_dPi0MassCutLow     = 0.10;
  m_dPi0MassCutHigh    = 0.16;
  m_dCosThetaMax       = 0.92;
  m_dCosThetaBarrel    = 0.83;
  m_dMaxTime           = 14;  // *50ns
  m_dCosThetaISR       = 0.95;//0.83;
  //m_lvBoost            = HepLorentzVector(0.0415,0,0,m_dCMSEnergy);
  m_lvBoost            = HepLorentzVector(m_dCMSEnergy*sin(0.011),0,0,m_dCMSEnergy);
  m_dChisqMaxValue     = 200;
  m_bMCrecorded        = false;
}
//----------------------------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------------------------
StatusCode Bes2Gamto2Pi0::initialize()
//----------------------------------------------------------------------------------------------------------
{
    cout << "initialize begins" << endl;
    MsgStream log(msgSvc(), name());
    log << MSG::INFO << "in initialize()" << endreq;

		m_iAll        = 0;
    m_iPreSkip    = 0;
    m_iPhotonSkip = 0;
    m_iPionSkip   = 0;
    m_iPi0Skip    = 0;
    m_iUFitSkip   = 0;
    m_iAcceptedU  = 0;
		
    m_KalKinFit  = KalmanKinematicFit::instance();
    m_Vtxfit     = VertexFit::instance();
    m_PID        = ParticleID::instance();
    
    //Initialize ROOT trees and declare Ntuples
		NTuplePtr ut(ntupleSvc(), "FILE1/Untagged");
		
		if   ( ut ) {m_NUntagged = ut;}
    else {m_NUntagged = ntupleSvc()->book("FILE1/Untagged", CLID_ColumnWiseTuple, "untagged");}
		
		if ( m_NUntagged ) {
     m_NUntagged->addItem("URunNumber"    ,m_dURunNumber);
     m_NUntagged->addItem("UEvtNumber"    ,m_dUEvtNumber);
     m_NUntagged->addItem("UCMSEnergy"    ,m_dUCMSEnergy);
     m_NUntagged->addItem("iUAnzGoodP"    ,m_iUAnzGoodP);
     m_NUntagged->addItem("iUAnzGoodM"    ,m_iUAnzGoodM);
     m_NUntagged->addItem("iUAnzGoodGamma",m_iUAnzGoodGamma);
     m_NUntagged->addItem("iUAnzPi0Tracks",m_iUAnzPi0Tracks);
     m_NUntagged->addItem("iUAnz2Combs"  ,m_iUAnz2Combs);
     m_NUntagged->addItem("iUAnz3Combs"  ,m_iUAnz3Combs);
     m_NUntagged->addItem("UTotalChi2"   ,m_dUTotalChi2);
     m_NUntagged->addItem("UPi01Chi2"    ,m_dUPi01Chi2);
     m_NUntagged->addItem("UPi02Chi2"    ,m_dUPi02Chi2);
     m_NUntagged->addItem("UMHadTagged"  ,m_dUMHadTagged);
     m_NUntagged->addItem("UsqrSHadSys"  ,m_dUsqrSHadSys);
     m_NUntagged->addItem("UsqrSHadSysFit",m_dUsqrSHadSysFit);
     m_NUntagged->addItem("UDistZp"      ,m_dUDistZp);
     m_NUntagged->addItem("UDistRp"      ,m_dUDistRp);
     m_NUntagged->addItem("UDistZm"      ,m_dUDistZm);
     m_NUntagged->addItem("UDistRm"      ,m_dUDistRm);
     m_NUntagged->addItem("iUBgrVeto"    ,m_iUBgrVeto);
     m_NUntagged->addItem("iUmcTruePi"   ,m_iUmcTruePi);
     m_NUntagged->addItem("iUmcTruePi0"  ,m_iUmcTruePi0);
     m_NUntagged->addItem("iUmcTrueISR"  ,m_iUmcTrueISR);
     m_NUntagged->addItem("iUmcTrueFSR"  ,m_iUmcTrueFSR);
     m_NUntagged->addItem("iUmcTrueGammaFromJpsi"  ,m_iUmcTrueGammaFromJpsi);
     m_NUntagged->addItem("iUmcTrueOtherGamma"  ,m_iUmcTrueOtherGamma);
     m_NUntagged->addItem("iUmcTrueOther" ,m_iUmcTrueOther);
     m_NUntagged->addItem("UmcMHad"       ,m_dUmcMHad);
     m_NUntagged->addItem("UmcMHad5pi"    ,m_dUmcMHad5pi);
     m_UPi01    .AttachToNtuple(m_NUntagged,"UPi01");
     m_UPi02    .AttachToNtuple(m_NUntagged,"UPi02");
     m_TaggedISR.AttachToNtuple(m_NUntagged,"UTagISR",true);
     m_UFitPi01    .AttachToNtuple(m_NUntagged,"UFitPi01");
     m_UFitPi02    .AttachToNtuple(m_NUntagged,"UFitPi02");
     m_UTrueISR    .AttachToNtuple(m_NUntagged,"TrueISR");
     for (int a=0;a<GAMMAS+5;a++) {m_UGammas   [a].AttachToNtuple(m_NUntagged,"UG"   +SSTR(a)+"emc",true); m_NUntagged->addItem("ClosestTr"+SSTR(a),m_dUClosestTrack[a]);}
     for (int a=0;a<5;a++)        {m_UFitGammas[a].AttachToNtuple(m_NUntagged,"UFitG"+SSTR(a),true);}
     m_NUntagged->addItem("UPi1EoP"          ,m_dUPi1EoP);
     m_NUntagged->addItem("UPi2EoP"          ,m_dUPi2EoP);
     m_NUntagged->addItem("UPi1MucDepth"     ,m_dUPi1MucDepth);
     m_NUntagged->addItem("UPi2MucDepth"     ,m_dUPi2MucDepth);
     m_NUntagged->addItem("U4PiNonRadChi2"   ,m_dU4PiNonRadChi2);
     m_NUntagged->addItem("UVtxChi2"  ,m_dUVtxChi2);
     m_NUntagged->addItem("VtxPosX"   ,m_dUVtxPosX);
     m_NUntagged->addItem("VtxPosY"   ,m_dUVtxPosY);
     m_NUntagged->addItem("VtxPosZ"   ,m_dUVtxPosZ);
     m_NUntagged->addItem("IPposX"    ,m_dUIPposX);
     m_NUntagged->addItem("IPposY"    ,m_dUIPposY);
     m_NUntagged->addItem("IPposZ"    ,m_dUIPposZ);

  }
   else {
      log << MSG::ERROR << "  Cannot book N-tuple88 untagged: " << long(m_NUntagged) << endreq;
      return StatusCode::FAILURE;
   }
	 
	 StatusCode sc = Gaudi::svcLocator()->service( "VertexDbSvc", m_vtxSvc,true);
   if(sc.isFailure())
     {
     //assert(false); //terminate program
     log << MSG::ERROR << "Cannot book VertexDbSvc!!" <<endmsg;
     return StatusCode::FAILURE;
   }
   //m_particleTable = m_partPropSvc->PDT();
   return StatusCode::SUCCESS;
}
//----------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------
StatusCode Bes2Gamto2Pi0::execute()
//----------------------------------------------------------------------------------------------------------
{

//cout<<"begin execute"<<endl;
    MsgStream log(msgSvc(), name());
    log << MSG::INFO << "in execute()" << endreq;

    SmartDataPtr<Event::EventHeader> eventHeader(eventSvc(),"/Event/EventHeader");
    SmartDataPtr<EvtRecEvent> evtRecEvent(eventSvc(), "/Event/EvtRec/EvtRecEvent");
    if ( ! evtRecEvent ) {
        log << MSG::FATAL << "Could not find EvtRecEvent" << endreq;
        return StatusCode::FAILURE;
    }
    SmartDataPtr<EvtRecTrackCol> evtRecTrackCol(eventSvc(), "/Event/EvtRec/EvtRecTrackCol");
    if ( ! evtRecTrackCol ) {
        log << MSG::FATAL << "Could not find EvtRecTrackCol" << endreq;
        return StatusCode::FAILURE;
    }
    Gaudi::svcLocator()->service("VertexDbSvc", m_vtxSvc);


    // get event with montecarlo truth
    m_iAll ++;
    WriteMonteCarloTruth();
    // preselector: skip, if there are insufficient amounts of tracks
    if (evtRecEvent->totalCharged() <  2 || evtRecEvent->totalCharged() > 6 || evtRecEvent->totalNeutral() > 50) {
        //cout<<"# insuficinet tracks: ch= "<<evtRecEvent->totalCharged()<<" , neut= "<<evtRecEvent->totalNeutral()<<endl;
        m_iPreSkip ++;
        return StatusCode::SUCCESS;
    }
    //cout<<"passed preselection: charged= "<<evtRecEvent->totalCharged()<<" , neutral= "<<evtRecEvent->totalNeutral()<<endl;
/*
//MC Truth
SmartDataPtr<McParticleCol> mcParticles(eventSvc(),"/Event/MC/McParticleCol");
if(mcParticles){

  for( McParticleCol::iterator itMC = mcParticles->begin();itMC != mcParticles->end();itMC ++) {

    }
    
  }	
*/

		// initialize virtual box
    m_hp3IP.setX(0);
    m_hp3IP.setY(0);
    m_hp3IP.setZ(0);
    if (m_vtxSvc->isVertexValid()) {
      double* dbv =  m_vtxSvc->PrimaryVertex();
      m_hp3IP.setX(dbv[0]);
      m_hp3IP.setY(dbv[1]);
      m_hp3IP.setZ(dbv[2]);
    }
    else{std::cout<<"no IP"<<std::endl;}
	
		// initialize iterators
    m_itBegin      = evtRecTrackCol->begin();
    m_itEndCharged = m_itBegin + evtRecEvent->totalCharged();
    m_itEndNeutral = m_itBegin + evtRecEvent->totalTracks();
    m_aPi0List.SetIterators (m_itBegin,m_itEndCharged,m_itEndNeutral);
    
// prepare ntuples 
    //ResetNTuples();
    m_dURunNumber   = eventHeader->runNumber();   
    m_dUCMSEnergy = beam_energy(abs(eventHeader->runNumber()));
    m_aPi0List.Init(m_dPi0MassCutLow,m_dPi0MassCutHigh,m_dPhEnergyCutBarrel,m_dPhEnergyCutEndCap,m_dCosThetaBarrel,m_dMaxTime,m_dCMSEnergy/2.0);
}
//----------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------- 
StatusCode Bes2Gamto2Pi0::finalize()
//----------------------------------------------------------------------------------------------------------
{
  MsgStream log(msgSvc(), name());
  log << MSG::INFO << "in finalize()" << endreq;

  return StatusCode::SUCCESS;
}
//----------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------
double Bes2Gamto2Pi0::beam_energy(int runNo)
//----------------------------------------------------------------------------------------------------------
{
    double ecms = -1;

    if( runNo>=23463 && runNo<=23505 ){//psi(4040) dataset-I
      ecms = 4.0104;
    }
    else if( runNo>=23509 && runNo<24141 ){//psi(4040) dataset-II
      ecms = 4.0093;
    }
    else if( runNo>=-24141 && runNo<=-23463 ){// psi(4009)
      ecms = 4.009;
    }
    else if( runNo>=24151 && runNo<=24177 ){//psi(2S) data
      ecms = 3.686;
    }
    else if( runNo>=29677 && runNo<=30367 ){//Y(4260) data-I
      ecms = 4.26;
    }
    else if( runNo>=31561 && runNo<=31981 ){//Y(4260) data-II
      ecms = 4.26;
    }
    else if( runNo>=31327 && runNo<=31390 ){//4420 data
      ecms = 4.42;
    }
    else if( runNo>=31281 && runNo<=31325 ){//4390 data
      ecms = 4.39;
    }
    else if( runNo>=30616 && runNo<=31279 ){//Y(4360) data
      ecms = 4.36;
    }
    else if( runNo>=30492 && runNo<=30557 ){// 4310 data
      ecms = 4.31;
    }
    else if( runNo>=30438 && runNo<=30491 ){// 4230 data
      ecms = 4.23;
    }
    else if( runNo>=30372 && runNo<=30437 ){// 4190 data
      ecms = 4.19;
    }
    else if( runNo>=31983 && runNo<=32045 ){// 4210 data
      ecms = 4.21;
    }
    else if( runNo>=32046 && runNo<=32140 ){// 4220 data
      ecms = 4.22;
    }
    else if( runNo>=32141 && runNo<=32226 ){// 4245 data
      ecms = 4.245;
    }
    else if( runNo>=32239 && runNo<=32850 ){// new 4230 data-I
      ecms = 4.23;
    }
    else if( runNo>=32851 && runNo<=33484 ){// new 4230 data-II
      ecms = 4.228;
    }
    else if( runNo>=33490 && runNo<=33556 ){// 3810 data
      ecms = 3.81;
    }
    else if( runNo>=33571 && runNo<=33657 ){// 3900 data
      ecms = 3.90;
    }
    else if( runNo>=33659 && runNo<=33719 ){// 4090 data
      ecms = 4.09;
    }
    else if( runNo>=11397 && runNo<=23454 ){// psi(3770) data
      ecms = 3.773;
    }

    return ecms;
}
//----------------------------------------------------------------------------------------------------------

void Bes2Gamto2Pi0::WriteMonteCarloTruth()
//----------------------------------------------------------------------------------------------------------
{
 SmartDataPtr<McParticleCol> mcParticles(eventSvc(),"/Event/MC/McParticleCol");
  if(!mcParticles) {
		m_iMCel						 = -2;
		m_iMCpositron			 = -2;
    m_iMCpi0           = -2;
    m_iMCisr           = -2;
    m_iMCfsr           = -2;
    m_iMCGammaFromJpsi = -2;
    m_iMCOtherGamma    = -2;
    m_iMCOther         = -2;
    m_bMCrecorded      = false;
    m_dmHadSysMCtruth  = -2;
    return;
  }  
   
  int  iPin  = 0; bool bFirstISR = true;
	m_iMCel						 = 0;
	m_iMCpositron			 = 0;
  m_iMCpi0           = 0;
  m_iMCisr           = 0;
  m_iMCfsr           = 0;
  m_iMCGammaFromJpsi = 0;
  m_iMCOtherGamma    = 0;
  m_iMCOther         = 0;
  m_bMCrecorded      = true;
  m_dmHadSysMCtruth  = -1;
	
	//Count types of MC particles
	for( McParticleCol::iterator itMC = mcParticles->begin();itMC != mcParticles->end();itMC ++) {
    if ((*itMC)->particleProperty() == 23 || (*itMC)->particleProperty() == 91) {continue;} // Generator internal particle
    if (abs((*itMC)->particleProperty()) < 11) {continue;} // Generator internal particle
    switch((*itMC)->particleProperty()) {
		case   11: m_iMCel++; break; //electron 
		case  -11: m_iMCpositron++; break;
    case   22: // gamma 
      if      ((*itMC)->mother().particleProperty() == 22 ) {m_iMCisr ++;}         // ISR photon
      else if ((*itMC)->mother().particleProperty() == 443) {m_iMCGammaFromJpsi ++;} // 443 = J/psi -> gamma + anything
      else if ((*itMC)->mother().particleProperty() != 111) {m_iMCOtherGamma ++;}    // all other except from pi0 decay
      break;
    case  -22: m_iMCfsr ++;   break; // FSR gamma
    case  111: m_iMCpi0 ++;   break; // pi0
    default:
      // count only final state particles except from pi decays
			/*
      if (   abs((*itMC)->mother().particleProperty())          != 211 // from pi+- decay
          && (*itMC)->mother().particleProperty()               != 111 // from pi0  decay
          && abs((*itMC)->mother().mother().particleProperty()) != 211 // from pi+- -> mu+nu -> e+3nu
          && (*itMC)->daughterList().size()                     == 0   // does not decay -> final state particle
         ) {
				*/
			// count only final state particles except from pi0 decays
				if ((*itMC)->daughterList().size()                     == 0   // does not decay -> final state particle
						&& (*itMC)->mother().particleProperty()               != 111 // from pi0  decay
         ) {
          m_iMCOther ++;
          //cout<<"additional final state particle found: "<<ParticleName((*itMC)->particleProperty())<<" , mother: "<<ParticleName((*itMC)->mother().particleProperty())<<endl;
      }
      break;
    }
  }
	
  for( McParticleCol::iterator itMC = mcParticles->begin();itMC != mcParticles->end();itMC ++) {
    if (!(*itMC)->primaryParticle () && (*itMC)->particleProperty() == 22) {continue;}
    switch((*itMC)->particleProperty()) {
    case 22:// gamma
      {	 
	if (bFirstISR) {
	  bFirstISR      = false;
 	  m_lvmcISR1     = (*itMC)->initialFourMomentum();
	}
	else {
  	  m_lvmcISR2     = (*itMC)->initialFourMomentum();
	}
	break;
      } 
    case 111:// pi0      
      switch(iPin) {
      case 0: iPin ++;// bFirstP0G = true;
          m_lvmcPi01       = (*itMC)->initialFourMomentum();
        break;
      case 1: iPin ++;// bFirstP0G = true;
          m_lvmcPi02       = (*itMC)->initialFourMomentum();
        break;
      case 2: m_lvmcPi03 = (*itMC)->initialFourMomentum(); break;
      }
    break;
    }      
  }

  if (m_iMCpi0 == 2 && m_iMCisr == 0 && m_iMCOther == 0 && m_iMCOtherGamma == 0 && m_iMCGammaFromJpsi == 0) {
    m_dmHadSysMCtruth  = (m_lvmcPi02).mag();
  }
		
}
//----------------------------------------------------------------------------------------------------------

/*
//---------------------------------------------------------------------------------------------------------- 
void Bes2Gamto2Pi0::ResetNTuples()
//---------------------------------------------------------------------------------------------------------- 
{
    //IP pos
    m_dUIPposX  = m_hp3IP.x();
    m_dUIPposY  = m_hp3IP.y();
    m_dUIPposZ  = m_hp3IP.z();

    // count MC particles if available
    if (m_iMCisr > 0) {
        if (m_lvmcISR1.e() > m_lvmcISR2.e()) {
            m_UTrueISR.Fill(&m_lvmcISR1);
        }
        else {
            m_UTrueISR.Fill(&m_lvmcISR2);

        }
    }
    else {
        m_UTrueISR.Clear();
    }

    m_imcTrueISR            = m_iMCisr;
    m_imcTrueFSR            = m_iMCfsr;
    m_imcTruePi             = m_iMCpi;
    m_imcTruePi0            = m_iMCpi0;
    m_imcTrueGammaFromJpsi  = m_iMCGammaFromJpsi;
    m_imcTrueOtherGamma     = m_iMCOtherGamma;
    m_imcTrueOther          = m_iMCOther;
    m_dmcMHad               = m_dmHadSysMCtruth;
    m_iUmcTrueISR           = m_iMCisr;
    m_iUmcTrueFSR           = m_iMCfsr;
    m_iUmcTruePi            = m_iMCpi;
    m_iUmcTruePi0           = m_iMCpi0;
    m_iUmcTrueGammaFromJpsi = m_iMCGammaFromJpsi;
    m_iUmcTrueOtherGamma    = m_iMCOtherGamma;
    m_iUmcTrueOther         = m_iMCOther;
    m_dUmcMHad              = m_dmHadSysMCtruth;
    m_dUmcMHad5pi           = m_d5mHadSysMCtruth;
    m_dTCMSEnergy           = -1;
    m_dUCMSEnergy           = -1;


    // signal untagged
    m_dU4PiNonRadChi2   = m_dChisqMaxValue;
    m_iUAnzGoodP        = -1;
    m_iUAnzGoodM        = -1;
    m_iUAnzGoodGamma    = -1;
    m_iUAnzPi0Tracks    = -1;
    m_iUAnz2Combs       = -1;
    m_iUAnz3Combs       = -1;
    m_dUTotalChi2       = m_dChisqMaxValue;
    m_dUPi01Chi2        = m_dChisqMaxValue;
    m_dUPi02Chi2        = m_dChisqMaxValue;
    m_dUDistZp          = -100;
    m_dUDistRp          = -1;
    m_dUDistZm          = -100;
    m_dUDistRm          = -1;
    m_dUsqrSHadSys      = -1;
    m_dUsqrSHadSysFit   = -1;
    m_iUBgrVeto         = -5;
    m_dUPi1EoP          = -1;
    m_dUPi2EoP          = -1;
    m_dUPi1MucDepth     = -1;
    m_dUPi2MucDepth     = -1;
    m_UPi01.Clear();
    m_UPi02.Clear();
    m_UPi1.Clear();
    m_UPi2.Clear();
    //m_TaggedISR.Clear(); // done in tagged fit
    for (int a=0;a<GAMMAS+4;a++) {m_UGammas[a].Clear(); m_dUClosestTrack[a] = -4.0;}
    m_UFitPi01.Clear();
    m_UFitPi02.Clear();
    m_UFitPi1.Clear();
    m_UFitPi2.Clear();
    for (int a=0;a<5;a++) {m_UFitGammas[a].Clear();}
    m_dUVtxChi2 = -1;
    m_dUVtxPosX = -1;
    m_dUVtxPosY = -1;
    m_dUVtxPosZ = -20;
    //m_dU5PiNonRadChi2 = -1;
    m_dU5PiChi2 = -1;
    m_dU5PiMhad = -1;
    m_U5PiISR.Clear();
    // Kskpi veto
    m_dUProb1Pi = -1;
    m_dUProb1K  = -1;
    m_dUProb2Pi = -1;
    m_dUProb2K  = -1;
    m_dUProb1P  = -1;
    m_dUProb2P  = -1;

}
*/
