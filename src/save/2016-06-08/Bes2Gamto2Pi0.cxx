

#include "Bes2Gamto2Pi0/Bes2Gamto2Pi0.h"
#include <sstream>

#define SSTR( x ) dynamic_cast< std::ostringstream & >( \
            ( std::ostringstream() << std::dec << x ) ).str()

using namespace Event;

///////////////////////////////////////////////////////////////////////////
//
//Analysis for e+ e- -> e+ e- pi0 pi0 at BES-III
//Author: Brice Garillon, Institut fuer Kernphysik, Uni Mainz, Germany 
//
///////////////////////////////////////////////////////////////////////////

//----------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------
// class Bes2Gamto2Pi0
//----------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------
Bes2Gamto2Pi0::Bes2Gamto2Pi0(const std::string& name, ISvcLocator* pSvcLocator) :
   Algorithm(name, pSvcLocator)
//----------------------------------------------------------------------------------------------------------
{
  //Declare the properties
  //declareProperty("MonteCarloAnalysis", m_MC_Analysis = false);
  //m_dCMSEnergy         = 3.773;       // psi(3770)
	m_dMasselec					 = 511e-6;
  m_dMassPi0           = 0.134976;    // PDG 2010
  m_dMassPiCharged     = 0.139570;    // PDG 2010
	m_dCrossingAngle		 = 0.011;
  m_dEoPcut            = 1.2;//0.8;
  m_dPi0GcutHigh       = 2.5;//1.4;
  m_dPhEnergyCutBarrel = 0.025;
  m_dPhEnergyCutEndCap = 0.05;
  m_dMaxPi0Combs       = 20;
  m_dMaxGoodPh         = 15;
  m_dCylRad            = 4;//2.5;  // for point of closest approach
  m_dCylHeight         = 20;//15.0; // for point of closest approach
  m_dPi0MassCutLow     = 0.10;
  m_dPi0MassCutHigh    = 0.16;
  m_dCosThetaMax       = 0.92;
  m_dCosThetaBarrel    = 0.83;
  m_dMaxTime           = 14;  // *50ns
  //m_lvBoost            = HepLorentzVector(0.0415,0,0,m_dCMSEnergy);
  m_lvBoost            = HepLorentzVector(m_dCMSEnergy*sin(m_dCrossingAngle),0,0,m_dCMSEnergy);
  m_dChisqMaxValue     = 200;
  m_bMCrecorded        = false;
	
	
}
//----------------------------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------------------------
StatusCode Bes2Gamto2Pi0::initialize()
//----------------------------------------------------------------------------------------------------------
{
    cout << "initialize begins" << endl;
    MsgStream log(msgSvc(), name());
    log << MSG::INFO << "in initialize()" << endreq;

		m_iAll        = 0;
    m_iPreSkip    = 0;
    m_iPhotonSkip = 0;
    m_iPi0Skip    = 0;
    m_iUFitSkip   = 0;
		m_iPreKept		=	0;
		m_iPhotonKept	=	0;
		m_iPi0kept		=	0;
		m_iUFitKept		=	0;
    m_iAcceptedU  = 0;
		
    m_KalKinFit  = KalmanKinematicFit::instance();
    m_Vtxfit     = VertexFit::instance();
    m_PID        = ParticleID::instance();

    //Initialize ROOT trees and declare Ntuples
		NTuplePtr allmctruth(ntupleSvc(), "FILE1/AllMCTruth");
		
		if(allmctruth){m_NAmcTruth=allmctruth;}
		else{m_NAmcTruth = ntupleSvc()->book("FILE1/AllMCTruth",CLID_ColumnWiseTuple,"All Monte Carlo-Truth");}
		
		if(m_NAmcTruth){
			m_NAmcTruth->addItem("AmcNpart",m_iAmcNpart,0,50);
			//**********Exec pb after this line***********
			m_NAmcTruth->addItem("AmcID",m_iAmcNpart,m_adAmcID);	
			m_NAmcTruth->addItem("AmcP",m_iAmcNpart,m_adAmcP);	
			}
		else {
      log << MSG::ERROR << "  Cannot book N-tuple1 MC truth: " << long(m_NAmcTruth) << endreq;
      return StatusCode::FAILURE;
   	}
		
		NTuplePtr ut(ntupleSvc(), "FILE1/Untagged");
		
		if   ( ut ) {m_NUntagged = ut;}
    else {m_NUntagged = ntupleSvc()->book("FILE1/Untagged", CLID_ColumnWiseTuple, "untagged");}
		
		if ( m_NUntagged ) {
     m_NUntagged->addItem("URunNumber"    ,m_dURunNumber);
     m_NUntagged->addItem("UEvtNumber"    ,m_dUEvtNumber);
     m_NUntagged->addItem("UCMSEnergy"    ,m_dUCMSEnergy);
     m_NUntagged->addItem("iUAnzGoodP"    ,m_iUAnzGoodP);
     m_NUntagged->addItem("iUAnzGoodM"    ,m_iUAnzGoodM);
     m_NUntagged->addItem("iUAnzGoodGamma",m_iUAnzGoodGamma);
     m_NUntagged->addItem("iUAnzPi0Tracks",m_iUAnzPi0Tracks);
     m_NUntagged->addItem("iUAnz2Combs"  ,m_iUAnz2Combs);
     m_NUntagged->addItem("iUAnz3Combs"  ,m_iUAnz3Combs);
     m_NUntagged->addItem("UTotalChi2"   ,m_dUTotalChi2);
     m_NUntagged->addItem("UPi01Chi2"    ,m_dUPi01Chi2);
     m_NUntagged->addItem("UPi02Chi2"    ,m_dUPi02Chi2);
     m_NUntagged->addItem("UDistZp"      ,m_dUDistZp);
     m_NUntagged->addItem("UDistRp"      ,m_dUDistRp);
     m_NUntagged->addItem("UDistZm"      ,m_dUDistZm);
     m_NUntagged->addItem("UDistRm"      ,m_dUDistRm);
     m_NUntagged->addItem("iUBgrVeto"    ,m_iUBgrVeto);
     m_NUntagged->addItem("iUmcTruePi"   ,m_iUmcTruePi);
     m_NUntagged->addItem("iUmcTruePi0"  ,m_iUmcTruePi0);
     m_NUntagged->addItem("iUmcTrueFSR"  ,m_iUmcTrueFSR);
     m_NUntagged->addItem("iUmcTrueGammaFromJpsi"  ,m_iUmcTrueGammaFromJpsi);
     m_NUntagged->addItem("iUmcTrueOtherGamma"  ,m_iUmcTrueOtherGamma);
     m_NUntagged->addItem("iUmcTrueOther" ,m_iUmcTrueOther);
     m_UPi01    .AttachToNtuple(m_NUntagged,"UPi01");
     m_UPi02    .AttachToNtuple(m_NUntagged,"UPi02");
		 m_UPi01CM    .AttachToNtuple(m_NUntagged,"UPi01CM");
     m_UPi02CM    .AttachToNtuple(m_NUntagged,"UPi02CM");
     m_UFitPi01    .AttachToNtuple(m_NUntagged,"UFitPi01");
     m_UFitPi02    .AttachToNtuple(m_NUntagged,"UFitPi02");
     for (int a=0;a<GAMMAS+5;a++) {m_UGammas   [a].AttachToNtuple(m_NUntagged,"UG"   +SSTR(a)+"emc",true); m_NUntagged->addItem("ClosestTr"+SSTR(a),m_dUClosestTrack[a]);}
     for (int a=0;a<5;a++)        {m_UFitGammas[a].AttachToNtuple(m_NUntagged,"UFitG"+SSTR(a),true);}
     m_NUntagged->addItem("UVtxChi2"  ,m_dUVtxChi2);
     m_NUntagged->addItem("VtxPosX"   ,m_dUVtxPosX);
     m_NUntagged->addItem("VtxPosY"   ,m_dUVtxPosY);
     m_NUntagged->addItem("VtxPosZ"   ,m_dUVtxPosZ);
     m_NUntagged->addItem("IPposX"    ,m_dUIPposX);
     m_NUntagged->addItem("IPposY"    ,m_dUIPposY);
     m_NUntagged->addItem("IPposZ"    ,m_dUIPposZ);
		 m_NUntagged->addItem("dU2pi0InvM"    ,m_dU2pi0InvM);
		 m_NUntagged->addItem("dUFit2pi0InvM"    ,m_dUFit2pi0InvM);
		 m_NUntagged->addItem("dUpi01InvM"    ,m_dUpi01InvM);
		 m_NUntagged->addItem("dUFitpi01InvM"    ,m_dUFitpi01InvM);
		 m_NUntagged->addItem("dUpi02InvM"    ,m_dUpi02InvM);
		 m_NUntagged->addItem("dUFitpi02InvM"    ,m_dUFitpi02InvM);
		 m_NUntagged->addItem("dUSumPtCM"    ,m_dUSumPtCM);
		 m_NUntagged->addItem("dUPCMbeamelecpositron"    ,m_dUPCMbeamelecpositron);
		 m_NUntagged->addItem("dUPbeamelecpositron"    ,m_dUPbeamelecpositron);
		 m_NUntagged->addItem("dUPi01CMPx"    ,m_dUPi01CMPx);
		 m_NUntagged->addItem("dUPi01CMPy"    ,m_dUPi01CMPy);
		 m_NUntagged->addItem("dUPi01CMPz"    ,m_dUPi01CMPz);
		 m_NUntagged->addItem("dUPi01CMPt"    ,m_dUPi01CMPt);
		 m_NUntagged->addItem("dUPi02CMPx"    ,m_dUPi02CMPx);
		 m_NUntagged->addItem("dUPi02CMPy"    ,m_dUPi02CMPy);
		 m_NUntagged->addItem("dUPi02CMPz"    ,m_dUPi02CMPz);
		 m_NUntagged->addItem("dUPi02CMPt"    ,m_dUPi02CMPt);
  }
   else {
      log << MSG::ERROR << "  Cannot book N-tuple1 untagged: " << long(m_NUntagged) << endreq;
      return StatusCode::FAILURE;
   }
	 
	 StatusCode sc = Gaudi::svcLocator()->service( "VertexDbSvc", m_vtxSvc,true);
   if(sc.isFailure())
     {
     //assert(false); //terminate program
     log << MSG::ERROR << "Cannot book VertexDbSvc!!" <<endmsg;
     return StatusCode::FAILURE;
   }
   //m_particleTable = m_partPropSvc->PDT();
	 
   return StatusCode::SUCCESS;
}
//----------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------
StatusCode Bes2Gamto2Pi0::execute()
//----------------------------------------------------------------------------------------------------------
{

//cout<<"begin execute"<<endl;
    MsgStream log(msgSvc(), name());
    log << MSG::INFO << "in execute()" << endreq;

    SmartDataPtr<Event::EventHeader> eventHeader(eventSvc(),"/Event/EventHeader");
    SmartDataPtr<EvtRecEvent> evtRecEvent(eventSvc(), "/Event/EvtRec/EvtRecEvent");
    if ( ! evtRecEvent ) {
        log << MSG::FATAL << "Could not find EvtRecEvent" << endreq;
        return StatusCode::FAILURE;
    }
    SmartDataPtr<EvtRecTrackCol> evtRecTrackCol(eventSvc(), "/Event/EvtRec/EvtRecTrackCol");
    if ( ! evtRecTrackCol ) {
        log << MSG::FATAL << "Could not find EvtRecTrackCol" << endreq;
        return StatusCode::FAILURE;
    }
    Gaudi::svcLocator()->service("VertexDbSvc", m_vtxSvc);


    //------ Get event with montecarlo truth ------//
    m_iAll ++;

		SmartDataPtr<McParticleCol> mcParticles(eventSvc(),"/Event/MC/McParticleCol");
  	if(!mcParticles) {
		m_iMCel						 = -2;
		m_iMCelprimary		 = -2;
		m_iMCpositron			 = -2;
		m_iMCpositronprimary=-2;
    m_iMCpi0           = -2;
    m_iMCfsr           = -2;
    m_iMCGammaFromJpsi = -2;
    m_iMCOtherGamma    = -2;
    m_iMCOther         = -2;
    m_bMCrecorded      = false;
    m_dmTotFinalMCtruth = -2;
  	} 
		else { 
  		int  iPin  = 0;
			m_iMCel						 = 0;
			m_iMCelprimary		 = 0;
			m_iMCpositron			 = 0;
			m_iMCpositronprimary=0;
  		m_iMCpi0           = 0;
  		m_iMCfsr           = 0;
  		m_iMCGammaFromJpsi = 0;
  		m_iMCOtherGamma    = 0;
  		m_iMCOther         = 0;
  		m_bMCrecorded      = true;
  		m_dmTotFinalMCtruth  = -1;
	
			m_iAmcNpart=0;
			
  		for( McParticleCol::iterator itMC = mcParticles->begin();itMC != mcParticles->end();itMC ++) {
    		
			
			int PID = (*itMC)->particleProperty();
      HepLorentzVector Particle = (*itMC)->initialFourMomentum();
      
      bool primary = (*itMC)->primaryParticle();
      bool leaf = (*itMC)->leafParticle();
      bool generator = (*itMC)->decayFromGenerator();
      bool flight = (*itMC)->decayInFlight();
      
      Event::McParticle Mother = (*itMC)->mother();
      int MPID = Mother.particleProperty();
			
			if(m_iAmcNpart <= m_iAmcNpart->range().distance()) {
				m_adAmcID[m_iAmcNpart]=PID;
				m_adAmcP[m_iAmcNpart]=Particle.rho();
				m_iAmcNpart++;
      	}
				
			switch((*itMC)->particleProperty()) {
				case 11: //electron
					m_iMCel++; 
					if((*itMC)->primaryParticle()) m_iMCelprimary++;
					m_lvmcel = (*itMC)->initialFourMomentum(); 
					break;
				case -11: //positron
					m_iMCpositron++; 
					if((*itMC)->primaryParticle()) m_iMCpositronprimary++;
					m_lvmcpositron = (*itMC)->initialFourMomentum(); 
					break;
    		case 111:// pi0
					m_iMCpi0 ++;      
      		switch(iPin) {
      		case 0: iPin ++;// bFirstP0G = true;
          	m_lvmcPi01       = (*itMC)->initialFourMomentum();
        		break;
      		case 1: iPin ++;// bFirstP0G = true;
          	m_lvmcPi02       = (*itMC)->initialFourMomentum();
        		break;
      		case 2: m_lvmcPi03 = (*itMC)->initialFourMomentum(); break;
      		}
				case   22: // gamma 
				if ((*itMC)->mother().particleProperty() == 443) {m_iMCGammaFromJpsi ++;} // 443 = J/psi -> gamma + anything
      	else if ((*itMC)->mother().particleProperty() != 111) {m_iMCOtherGamma ++;}    // all other except from pi0 decay
      	break;
				
				default:
					// count only final state particles except from pi0 decays
						if ((*itMC)->daughterList().size()                     == 0   // does not decay -> final state particle
								&& (*itMC)->mother().particleProperty()               != 111 // from pi0  decay
         		) {
          		m_iMCOther ++;
      		}
      		break;
					
    		}
				      
  		}
			m_NAmcTruth->write();
			
  		if (m_iMCpi0 == 2 && m_iMCelprimary==1 && m_iMCpositronprimary ==1 && m_iMCOther == 0 && m_iMCOtherGamma == 0 && m_iMCGammaFromJpsi == 0) {
    	m_dmTotFinalMCtruth  = (m_lvmcel+m_lvmcpositron+m_lvmcPi01+m_lvmcPi02).mag();
  		}
		}
		/*
  	else { 
  		int  iPin  = 0;
			m_iMCel						 = 0;
			m_iMCelprimary		 = 0;
			m_iMCpositron			 = 0;
			m_iMCpositronprimary=0;
  		m_iMCpi0           = 0;
  		m_iMCfsr           = 0;
  		m_iMCGammaFromJpsi = 0;
  		m_iMCOtherGamma    = 0;
  		m_iMCOther         = 0;
  		m_bMCrecorded      = true;
  		m_dmTotFinalMCtruth  = -1;
	
			//Count types of MC particles
			for( McParticleCol::iterator itMC = mcParticles->begin();itMC != mcParticles->end();itMC ++) {
    		if ((*itMC)->particleProperty() == 23 || (*itMC)->particleProperty() == 91) {continue;} // Generator internal particle
    		if (abs((*itMC)->particleProperty()) < 11) {continue;} // Generator internal particle
    		switch((*itMC)->particleProperty()) {
				case   11: //electron 
					m_iMCel++; 
					if((*itMC)->primaryParticle()) m_iMCelprimary++;
					break; 
				case  -11: 
					m_iMCpositron++; 
					if((*itMC)->primaryParticle()) m_iMCpositronprimary++;
					break;
    		case   22: // gamma 
					if ((*itMC)->mother().particleProperty() == 443) {m_iMCGammaFromJpsi ++;} // 443 = J/psi -> gamma + anything
      		else if ((*itMC)->mother().particleProperty() != 111) {m_iMCOtherGamma ++;}    // all other except from pi0 decay
      		break;
    		case  -22: m_iMCfsr ++;   break; // FSR gamma
    		case  111: m_iMCpi0 ++;   break; // pi0
    		default:
					// count only final state particles except from pi0 decays
						if ((*itMC)->daughterList().size()                     == 0   // does not decay -> final state particle
								&& (*itMC)->mother().particleProperty()               != 111 // from pi0  decay
         		) {
          		m_iMCOther ++;
      		}
      		break;
    		}
  		}
	
  		for( McParticleCol::iterator itMC = mcParticles->begin();itMC != mcParticles->end();itMC ++) {
    		if (!(*itMC)->primaryParticle () && (*itMC)->particleProperty() == 22) {continue;}
    		switch((*itMC)->particleProperty()) {
				case 11: //electron
					m_lvmcel = (*itMC)->initialFourMomentum(); break;
				case -11: //positron
					m_lvmcpositron = (*itMC)->initialFourMomentum(); break;
    		case 111:// pi0      
      		switch(iPin) {
      		case 0: iPin ++;// bFirstP0G = true;
          	m_lvmcPi01       = (*itMC)->initialFourMomentum();
        		break;
      		case 1: iPin ++;// bFirstP0G = true;
          	m_lvmcPi02       = (*itMC)->initialFourMomentum();
        		break;
      		case 2: m_lvmcPi03 = (*itMC)->initialFourMomentum(); break;
      		}
    		break;
    		}
				      
  		}

  		if (m_iMCpi0 == 2 && m_iMCelprimary==1 && m_iMCpositronprimary ==1 && m_iMCOther == 0 && m_iMCOtherGamma == 0 && m_iMCGammaFromJpsi == 0) {
    	m_dmTotFinalMCtruth  = (m_lvmcel+m_lvmcpositron+m_lvmcPi01+m_lvmcPi02).mag();
  		}
		}
		*/
		
    //------ preselector: skip, if there are insufficient amounts of tracks ------//
    if (evtRecEvent->totalCharged() > 0 || evtRecEvent->totalNeutral() > 50) {
        //cout<<"# insuficinet tracks: ch= "<<evtRecEvent->totalCharged()<<" , neut= "<<evtRecEvent->totalNeutral()<<endl;
        m_iPreSkip ++;
        return StatusCode::SUCCESS;
    }
		m_iPreKept++;
    //cout<<"passed preselection: charged= "<<evtRecEvent->totalCharged()<<" , neutral= "<<evtRecEvent->totalNeutral()<<endl;

		//------ initialize virtual box ------//
    m_hp3IP.setX(0);
    m_hp3IP.setY(0);
    m_hp3IP.setZ(0);
    if (m_vtxSvc->isVertexValid()) {
      double* dbv =  m_vtxSvc->PrimaryVertex();
      m_hp3IP.setX(dbv[0]);
      m_hp3IP.setY(dbv[1]);
      m_hp3IP.setZ(dbv[2]);
    }
    else{std::cout<<"no IP"<<std::endl;}
	
		//------ initialize iterators ------//
    m_itBegin      = evtRecTrackCol->begin();
    m_itEndCharged = m_itBegin + evtRecEvent->totalCharged();
    m_itEndNeutral = m_itBegin + evtRecEvent->totalTracks();
    m_aPi0List.SetIterators (m_itBegin,m_itEndCharged,m_itEndNeutral);
    
		//------ prepare ntuples ------//
    //ResetNTuples();
		//IP pos
    m_dUIPposX  = m_hp3IP.x();
    m_dUIPposY  = m_hp3IP.y();
    m_dUIPposZ  = m_hp3IP.z();
    
    m_iUmcTrueFSR           = m_iMCfsr;
    m_iUmcTruePi0           = m_iMCpi0;
    m_iUmcTrueGammaFromJpsi = m_iMCGammaFromJpsi;
    m_iUmcTrueOtherGamma    = m_iMCOtherGamma;
    m_iUmcTrueOther         = m_iMCOther;
    m_dUCMSEnergy           = -1;

		
    // signal untagged
    m_iUAnzGoodP        = -1;
    m_iUAnzGoodM        = -1;
    m_iUAnzGoodGamma    = -1;
    m_iUAnzPi0Tracks    = -1;
    m_iUAnz2Combs       = -1;
    m_iUAnz3Combs       = -1;
    m_dUTotalChi2       = m_dChisqMaxValue;
    m_dUPi01Chi2        = m_dChisqMaxValue;
    m_dUPi02Chi2        = m_dChisqMaxValue;
    m_dUDistZp          = -100;
    m_dUDistRp          = -1;
    m_dUDistZm          = -100;
    m_dUDistRm          = -1;
    m_iUBgrVeto         = -5;
		
    m_UPi01.Clear();
    m_UPi02.Clear();
    //m_TaggedISR.Clear(); // done in tagged fit
		
    for (int a=0;a<GAMMAS+4;a++) {m_UGammas[a].Clear(); m_dUClosestTrack[a] = -4.0;}
    m_UFitPi01.Clear();
    m_UFitPi02.Clear();
    for (int a=0;a<5;a++) {m_UFitGammas[a].Clear();}
    m_dUVtxChi2 = -1;
    m_dUVtxPosX = -1;
    m_dUVtxPosY = -1;
    m_dUVtxPosZ = -20;
		
		
    m_dURunNumber   = eventHeader->runNumber();
		m_dUEvtNumber  = eventHeader->eventNumber(); 
		m_dCMSEnergy = beam_energy(abs(eventHeader->runNumber()));
    if (m_dCMSEnergy == -1) {log<<MSG::FATAL<<"unknown runNr, can not determine CMS Energy!"<<endreq; return StatusCode::FAILURE;}  
    m_dUCMSEnergy = m_dCMSEnergy;
		m_lvBoost        = HepLorentzVector(m_dCMSEnergy*sin(m_dCrossingAngle),0,0,m_dCMSEnergy);
    m_dPi0GcutHigh   = m_dCMSEnergy*0.6;
    m_aPi0List.Init(m_dPi0MassCutLow,m_dPi0MassCutHigh,m_dPhEnergyCutBarrel,m_dPhEnergyCutEndCap,m_dCosThetaBarrel,m_dMaxTime,m_dCMSEnergy/2.0);

		//------ Find 2 pi0 candidates ------//
    //if (!Find2pi0Candidates ()) {return StatusCode::SUCCESS;}
		
		EvtRecTrackIterator    itERT;

    // clear lists

    // count good photons
    int iGood = 0;
    for (itERT = m_itEndCharged;itERT != m_itEndNeutral;itERT ++) {
        if (m_aPi0List.IsPhoton(*itERT)) {iGood ++;}
    }
    if (iGood < 4 || iGood > m_dMaxGoodPh) {m_iPhotonSkip ++; return StatusCode::SUCCESS;}
		m_iPhotonKept++;
    m_iUAnzGoodGamma = iGood;

    // find pi0 -> 2gamma
    m_aPi0List.FillList();
    if (m_aPi0List.m_aPi0TracksTable.size() > m_dMaxPi0Combs) {m_iPi0Skip ++; return StatusCode::SUCCESS;}
    m_aPi0List.CreatedValid2Combs ();
    m_aPi0List.CreatedValid3Combs ();
    // insufficient number of 2pi0 candidates
    if (m_aPi0List.m_aValidCombs2.size() < 1) {m_iPi0Skip ++; return StatusCode::SUCCESS;}
		m_iPi0kept++;
		
    // record combinatorics
    m_iUAnzPi0Tracks  = m_aPi0List.m_aPi0TracksTable.size();
    m_iUAnz2Combs     = m_aPi0List.m_aValidCombs2.size();
    m_iUAnz3Combs     = m_aPi0List.m_aValidCombs3.size();
 		
		//------ END of Find 2 pi0 candidates------//
		
		
		//------ Untagged 2pi0 selection ------//
		//bool Iselpos2pi0Untagged = Findelpos2pi0Untagged();
		
		if (m_aPi0List.m_aValidCombs2.size() < 1)
       {m_dUTotalChi2 = -5; m_dUPi01Chi2 = -5; m_dUPi02Chi2 = -5; return StatusCode::SUCCESS;}
    bool bDone = false;
		
		//------ With Kinematic FIT ------//
		/*	
    // try untagged fit
    for(int k=0;k<m_aPi0List.m_aValidCombs2.size();k++) {
			//Pi0 mass contrained fit of the decay photons
			m_KalKinFit->init();
    	m_KalKinFit->setChisqCut(m_dChisqMaxValue);
			cPi0Tracks* ptPi01 = m_aPi0List.m_aValidCombs2[k].m_pTrack1;
			cPi0Tracks* ptPi02 = m_aPi0List.m_aValidCombs2[k].m_pTrack2;
			m_KalKinFit->AddTrack(0,0.0,ptPi01->m_pTrack1->emcShower());
    	m_KalKinFit->AddTrack(1,0.0,ptPi01->m_pTrack2->emcShower());
      m_KalKinFit->AddTrack(2,0.0,ptPi02->m_pTrack1->emcShower());
      m_KalKinFit->AddTrack(3,0.0,ptPi02->m_pTrack2->emcShower());
      
			m_KalKinFit->AddMissTrack(4,0.0,m_lvBoost -m_aPi0List.Get4VecFromEMC(ptPi01->m_pTrack1,0.0)
                                               -m_aPi0List.Get4VecFromEMC(ptPi01->m_pTrack2,0.0)
                                               -m_aPi0List.Get4VecFromEMC(ptPi02->m_pTrack1,0.0)
                                               -m_aPi0List.Get4VecFromEMC(ptPi02->m_pTrack2,0.0));
			//Add 4-momentum constraint
			m_KalKinFit->AddFourMomentum(0,m_lvBoost);
			//Add pi0 mass constraint
			m_KalKinFit->AddResonance(1,m_dMassPi0,0,1);
			m_KalKinFit->AddResonance(2,m_dMassPi0,2,3);																				 
			//Perform the fit
			if (!m_KalKinFit->Fit()) {continue;}
      if (m_KalKinFit->chisq() > 0 && m_KalKinFit->chisq() < m_dUTotalChi2) {
        //m_d4NPiIsrUChi2  = m_KalKinFit->chisq();
				    
				//Prepare to write 
				m_dUTotalChi2 = m_KalKinFit->chisq();
    		// fit
    		m_lvFitPi01G1 = m_KalKinFit->pfit(0);
    		m_lvFitPi01G2 = m_KalKinFit->pfit(1);
    		m_lvFitPi02G1 = m_KalKinFit->pfit(2);
    		m_lvFitPi02G2 = m_KalKinFit->pfit(3);
    		m_lvFitPi01   = m_KalKinFit->pfit(0) + m_KalKinFit->pfit(1);
    		m_lvFitPi02   = m_KalKinFit->pfit(2) + m_KalKinFit->pfit(3);
    		// detector
    		m_lvPi01   = m_aPi0List.Get4VecFromEMC(ptPi01->m_pTrack1,0.0) + m_aPi0List.Get4VecFromEMC(ptPi01->m_pTrack2,0.0);
    		m_lvPi01G1 = m_aPi0List.Get4VecFromEMC(ptPi01->m_pTrack1,0.0);
    		m_lvPi01G2 = m_aPi0List.Get4VecFromEMC(ptPi01->m_pTrack2,0.0);
    		m_lvPi02   = m_aPi0List.Get4VecFromEMC(ptPi02->m_pTrack1,0.0) + m_aPi0List.Get4VecFromEMC(ptPi02->m_pTrack2,0.0);
    		m_lvPi02G1 = m_aPi0List.Get4VecFromEMC(ptPi02->m_pTrack1,0.0);
    		m_lvPi02G2 = m_aPi0List.Get4VecFromEMC(ptPi02->m_pTrack2,0.0);
    		m_aPhIndexList.clear();
   		  m_aPhIndexList.push_back (ptPi01->m_pTrack1);
    		m_aPhIndexList.push_back (ptPi01->m_pTrack2);
    		m_aPhIndexList.push_back (ptPi02->m_pTrack1);
    		m_aPhIndexList.push_back (ptPi02->m_pTrack2);
    		// the lines below MUSST stay below the settings of the 4vectors!
    		if (m_KalKinFit->Fit(1)) {m_dUPi01Chi2 = m_KalKinFit->chisq(1);} else {m_dUPi01Chi2 = -5;}
    		if (m_KalKinFit->Fit(2)) {m_dUPi02Chi2 = m_KalKinFit->chisq(2);} else {m_dUPi02Chi2 = -5;}			
				
        bDone = true;
      }
    }
		


if (!bDone) {m_dUTotalChi2 = -5;m_dUPi01Chi2 = -5; m_dUPi02Chi2 = -5; return StatusCode::SUCCESS;}
//if (!bDone) {m_dUTotalChi2 = -5;m_dUPi01Chi2 = -5; m_dUPi02Chi2 = -5;}
m_iUFitKept++;
		*/

			//------ Without Kinematic FIT ------//
			
			for(int k=0;k<m_aPi0List.m_aValidCombs2.size();k++) {
			cPi0Tracks* ptPi01 = m_aPi0List.m_aValidCombs2[k].m_pTrack1;
			cPi0Tracks* ptPi02 = m_aPi0List.m_aValidCombs2[k].m_pTrack2;
			
    		// detector
    		m_lvPi01   = m_aPi0List.Get4VecFromEMC(ptPi01->m_pTrack1,0.0) + m_aPi0List.Get4VecFromEMC(ptPi01->m_pTrack2,0.0);
    		m_lvPi01G1 = m_aPi0List.Get4VecFromEMC(ptPi01->m_pTrack1,0.0);
    		m_lvPi01G2 = m_aPi0List.Get4VecFromEMC(ptPi01->m_pTrack2,0.0);
    		m_lvPi02   = m_aPi0List.Get4VecFromEMC(ptPi02->m_pTrack1,0.0) + m_aPi0List.Get4VecFromEMC(ptPi02->m_pTrack2,0.0);
    		m_lvPi02G1 = m_aPi0List.Get4VecFromEMC(ptPi02->m_pTrack1,0.0);
    		m_lvPi02G2 = m_aPi0List.Get4VecFromEMC(ptPi02->m_pTrack2,0.0);
    		m_aPhIndexList.clear();
   		  m_aPhIndexList.push_back (ptPi01->m_pTrack1);
    		m_aPhIndexList.push_back (ptPi01->m_pTrack2);
    		m_aPhIndexList.push_back (ptPi02->m_pTrack1);
    		m_aPhIndexList.push_back (ptPi02->m_pTrack2);		
    }
			
//Fill particle containers
m_UPi01.Fill(&m_lvPi01);
m_UPi02.Fill(&m_lvPi02);
m_UGammas[0].Fill(&m_lvPi01G1);
m_UGammas[1].Fill(&m_lvPi01G2);
m_UGammas[2].Fill(&m_lvPi02G1);
m_UGammas[3].Fill(&m_lvPi02G2);
m_UFitPi01.Fill(&m_lvFitPi01);
m_UFitPi02.Fill(&m_lvFitPi02);
m_UFitGammas[0].Fill(&m_lvFitPi01G1);
m_UFitGammas[1].Fill(&m_lvFitPi01G2);
m_UFitGammas[2].Fill(&m_lvFitPi02G1);
m_UFitGammas[3].Fill(&m_lvFitPi02G2);

m_dU2pi0InvM=(m_lvPi01+m_lvPi02).m();
m_dUFit2pi0InvM=(m_lvFitPi01+m_lvFitPi02).m();
m_dUpi01InvM=m_lvPi01.m();
m_dUFitpi01InvM=m_lvFitPi01.m();
m_dUpi02InvM=m_lvPi02.m();
m_dUFitpi02InvM=m_lvFitPi02.m();

double TotEngBeam = m_dCMSEnergy/2 + m_dMasselec;
double Abs3MomBeam = sqrt(TotEngBeam*TotEngBeam - m_dMasselec*m_dMasselec);
  
m_lvBeamElectron.setPx(0);
m_lvBeamElectron.setPy(0);
m_lvBeamElectron.setPz(-Abs3MomBeam);
m_lvBeamElectron.setE(TotEngBeam);
  
m_lvBeamPositron.setPx(Abs3MomBeam*sin(m_dCrossingAngle));
m_lvBeamPositron.setPy(0);
m_lvBeamPositron.setPz(Abs3MomBeam*cos(m_dCrossingAngle));
m_lvBeamPositron.setE(TotEngBeam);

Hep3Vector vectboostCM=-((m_lvBeamElectron+m_lvBeamPositron).boostVector());	
m_lvBeamElectronCM=m_lvBeamElectron;
m_lvBeamElectronCM.boost(vectboostCM);
m_lvBeamPositronCM=m_lvBeamPositron;
m_lvBeamPositronCM.boost(vectboostCM);
m_lvPi01CM=m_lvPi01;
m_lvPi01CM.boost(vectboostCM);
m_lvPi02CM=m_lvPi02;
m_lvPi02CM.boost(vectboostCM);

vector<HepLorentzVector> vectSumPt;
vectSumPt.push_back(m_lvPi01CM);
vectSumPt.push_back(m_lvPi01CM);

m_dUSumPtCM=GetSumPt(vectSumPt);

m_dUPbeamelecpositron=(m_lvBeamElectron+m_lvBeamPositron).rho();
m_dUPCMbeamelecpositron=(m_lvBeamElectronCM+m_lvBeamPositronCM).rho();

m_dUPi01CMPx=(m_lvPi01CM.vect()).x();
m_dUPi01CMPy=(m_lvPi01CM.vect()).y();
m_dUPi01CMPz=(m_lvPi01CM.vect()).z();
m_dUPi01CMPt=(m_lvPi01CM.vect()).perp();
m_dUPi02CMPx=(m_lvPi02CM.vect()).x();
m_dUPi02CMPy=(m_lvPi02CM.vect()).y();
m_dUPi02CMPz=(m_lvPi02CM.vect()).z();
m_dUPi02CMPt=(m_lvPi02CM.vect()).perp();

m_UPi01CM.Fill(&m_lvPi01CM);
m_UPi02CM.Fill(&m_lvPi02CM);

//------ END of Untagged 2pi0 selection ------//

//------ Fill NTuples ------//
/*
if(Iselpos2pi0Untagged){
	log << MSG::WARNING << "****** FILL! " << m_iAll << " ******" << endreq;
	m_NUntagged->write();
	m_iAcceptedU ++;
	}
*/

log << MSG::WARNING << "****** FILL! " << m_iAll << " ******" << endreq;
m_NUntagged->write();
m_iAcceptedU ++;
			
return StatusCode::SUCCESS;
}
//----------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------- 
StatusCode Bes2Gamto2Pi0::finalize()
//----------------------------------------------------------------------------------------------------------
{
  MsgStream log(msgSvc(), name());
  log << MSG::INFO << "in finalize()" << endreq;

	log << MSG::WARNING << "****** Total number of events " << m_iAll << " ******" << endreq;
	log << MSG::WARNING << "****** Number of kept events at  track preselection " << m_iPreKept << " ******" << endreq;
	log << MSG::WARNING << "****** Number of kept events at preselect.+ good photons requirement " << m_iPhotonKept << " ******" << endreq;
	log << MSG::WARNING << "****** Number of kept events at npreselect.+ good photons requirement + 2 pi0 candidates requirement " << m_iPi0kept << " ******" << endreq;
  log << MSG::WARNING << "****** Number of kept events at npreselect.+ good photons requirement + 2 pi0 candidates + kalmanfit requirement " << m_iUFitKept << " ******" << endreq;
	log << MSG::WARNING << "****** Number of untagged e+e-2pi0 events " << m_iAcceptedU << " ******" << endreq;
	return StatusCode::SUCCESS;
}
//----------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------
double Bes2Gamto2Pi0::beam_energy(int runNo)
//----------------------------------------------------------------------------------------------------------
{
    double ecms = -1;

    if( runNo>=23463 && runNo<=23505 ){//psi(4040) dataset-I
      ecms = 4.0104;
    }
    else if( runNo>=23509 && runNo<24141 ){//psi(4040) dataset-II
      ecms = 4.0093;
    }
    else if( runNo>=-24141 && runNo<=-23463 ){// psi(4009)
      ecms = 4.009;
    }
    else if( runNo>=24151 && runNo<=24177 ){//psi(2S) data
      ecms = 3.686;
    }
    else if( runNo>=29677 && runNo<=30367 ){//Y(4260) data-I
      ecms = 4.26;
    }
    else if( runNo>=31561 && runNo<=31981 ){//Y(4260) data-II
      ecms = 4.26;
    }
    else if( runNo>=31327 && runNo<=31390 ){//4420 data
      ecms = 4.42;
    }
    else if( runNo>=31281 && runNo<=31325 ){//4390 data
      ecms = 4.39;
    }
    else if( runNo>=30616 && runNo<=31279 ){//Y(4360) data
      ecms = 4.36;
    }
    else if( runNo>=30492 && runNo<=30557 ){// 4310 data
      ecms = 4.31;
    }
    else if( runNo>=30438 && runNo<=30491 ){// 4230 data
      ecms = 4.23;
    }
    else if( runNo>=30372 && runNo<=30437 ){// 4190 data
      ecms = 4.19;
    }
    else if( runNo>=31983 && runNo<=32045 ){// 4210 data
      ecms = 4.21;
    }
    else if( runNo>=32046 && runNo<=32140 ){// 4220 data
      ecms = 4.22;
    }
    else if( runNo>=32141 && runNo<=32226 ){// 4245 data
      ecms = 4.245;
    }
    else if( runNo>=32239 && runNo<=32850 ){// new 4230 data-I
      ecms = 4.23;
    }
    else if( runNo>=32851 && runNo<=33484 ){// new 4230 data-II
      ecms = 4.228;
    }
    else if( runNo>=33490 && runNo<=33556 ){// 3810 data
      ecms = 3.81;
    }
    else if( runNo>=33571 && runNo<=33657 ){// 3900 data
      ecms = 3.90;
    }
    else if( runNo>=33659 && runNo<=33719 ){// 4090 data
      ecms = 4.09;
    }
    else if( runNo>=11397 && runNo<=23454 ){// psi(3770) data
      ecms = 3.773;
    }

    return ecms;
}

//---------------------------------------------------------------------------------------------------------- 
double Bes2Gamto2Pi0::GetSumPt(std::vector<HepLorentzVector> vectlv)
{
double SumPt;
HepLorentzVector Sumlv(0,0,0,0);
Hep3Vector Sum3v(0,0,0);
Hep3Vector Sum3vperp(0,0,0);

for(int k=0;k<vectlv.size();k++){
	Sumlv+=vectlv[k];
	Sum3v+=vectlv[k].vect();
	Sum3vperp+=(vectlv[k].vect()).perpPart();
	}
	
SumPt=Sum3vperp.r();
//SumPt=Sumlv.perp();
/*
SumPt=sqrt(pow(Sumlv.x(),2)
					+pow(Sumlv.y(),2)
					);
*/

//m_dUSumPtCM=((m_lvPi01CM.vect()).perpPart()+(m_lvPi02CM.vect()).perpPart()).r();
//m_dUSumPtCM=((m_lvPi01CM.vect()+m_lvPi02CM.vect()).perpPart()).r();
//m_dUSumPtCM=(m_lvPi01CM+m_lvPi02CM).perp();

//Hep3Vector vZAxis(0,0,1);
//m_dUSumPtCM=((m_lvPi01CM.vect()+m_lvPi02CM.vect()).perpPart(vZAxis)).r();
//m_dUSumPtCM=sqrt(pow((m_lvPi01CM.vect()+m_lvPi02CM.vect()).x(),2)
//								+pow((m_lvPi01CM.vect()+m_lvPi02CM.vect()).y(),2)
//								);

return SumPt;
}
//---------------------------------------------------------------------------------------------------------- 
/*
//---------------------------------------------------------------------------------------------------------- 
bool Bes2Gamto2Pi0::Find2pi0Candidates()
//----------------------------------------------------------------------------------------------------------
{
    EvtRecTrackIterator    itERT;

    // clear lists

    // count good photons
    int iGood = 0;
    for (itERT = m_itEndCharged;itERT != m_itEndNeutral;itERT ++) {
        if (m_aPi0List.IsPhoton(*itERT)) {iGood ++;}
    }
    if (iGood < 4 || iGood > m_dMaxGoodPh) {m_iPhotonSkip ++; return false;}
    m_iUAnzGoodGamma = iGood;

    // find pi0 -> 2gamma
    m_aPi0List.FillList();
    if (m_aPi0List.m_aPi0TracksTable.size() > m_dMaxPi0Combs) {m_iPi0Skip ++; return false;}
    m_aPi0List.CreatedValid2Combs ();
    m_aPi0List.CreatedValid3Combs ();
    // insufficient number of 2pi0 candidates
    if (m_aPi0List.m_aValidCombs2.size() < 1) {m_iPi0Skip ++; return false;}

    // record combinatorics
    m_iUAnzPi0Tracks  = m_aPi0List.m_aPi0TracksTable.size();
    m_iUAnz2Combs     = m_aPi0List.m_aValidCombs2.size();
    m_iUAnz3Combs     = m_aPi0List.m_aValidCombs3.size();
		
		return true;
}
//----------------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------------- 
bool Bes2Gamto2Pi0::Findelpos2pi0Untagged()
//----------------------------------------------------------------------------------------------------------
{
    if (m_aPi0List.m_aValidCombs2.size() < 1)
       {m_dUTotalChi2 = -5; m_dUPi01Chi2 = -5; m_dUPi02Chi2 = -5; return false;}
    bool bDone = false;
	
    // try untagged fit
    for(int k=0;k<m_aPi0List.m_aValidCombs2.size();k++) {
			//Pi0 mass contrained fit of the decay photons
			m_KalKinFit->init();
    	m_KalKinFit->setChisqCut(m_dChisqMaxValue);
			cPi0Tracks* ptPi01 = m_aPi0List.m_aValidCombs2[k].m_pTrack1;
			cPi0Tracks* ptPi02 = m_aPi0List.m_aValidCombs2[k].m_pTrack2;
			m_KalKinFit->AddTrack(0,0.0,ptPi01->m_pTrack1->emcShower());
    	m_KalKinFit->AddTrack(1,0.0,ptPi01->m_pTrack2->emcShower());
      m_KalKinFit->AddTrack(2,0.0,ptPi02->m_pTrack1->emcShower());
      m_KalKinFit->AddTrack(3,0.0,ptPi02->m_pTrack2->emcShower());
      
			m_KalKinFit->AddMissTrack(4,0.0,m_lvBoost -m_aPi0List.Get4VecFromEMC(ptPi01->m_pTrack1,0.0)
                                               -m_aPi0List.Get4VecFromEMC(ptPi01->m_pTrack2,0.0)
                                               -m_aPi0List.Get4VecFromEMC(ptPi02->m_pTrack1,0.0)
                                               -m_aPi0List.Get4VecFromEMC(ptPi02->m_pTrack2,0.0));
			//Add 4-momentum constraint
			m_KalKinFit->AddFourMomentum(0,m_lvBoost);
			//Add pi0 mass constraint
			m_KalKinFit->AddResonance(1,m_dMassPi0,0,1);
			m_KalKinFit->AddResonance(2,m_dMassPi0,2,3);																				 
			//Perform the fit
			if (!m_KalKinFit->Fit()) {continue;}
      if (m_KalKinFit->chisq() > 0 && m_KalKinFit->chisq() < m_dUTotalChi2) {
        //PrepareToWrite (m_aPi0List.m_aValidCombs2[k].m_pTrack1,m_aPi0List.m_aValidCombs2[k].m_pTrack2,0,0);
        //m_d4NPiIsrUChi2  = m_KalKinFit->chisq();
				    
				//Prepare to write 
				m_dUTotalChi2 = m_KalKinFit->chisq();
    		// fit
    		m_lvFitPi01G1 = m_KalKinFit->pfit(0);
    		m_lvFitPi01G2 = m_KalKinFit->pfit(1);
    		m_lvFitPi02G1 = m_KalKinFit->pfit(2);
    		m_lvFitPi02G2 = m_KalKinFit->pfit(3);
    		m_lvFitPi01   = m_KalKinFit->pfit(0) + m_KalKinFit->pfit(1);
    		m_lvFitPi02   = m_KalKinFit->pfit(2) + m_KalKinFit->pfit(3);
    		// detector
    		m_lvPi01   = m_aPi0List.Get4VecFromEMC(ptPi01->m_pTrack1,0.0) + m_aPi0List.Get4VecFromEMC(ptPi01->m_pTrack2,0.0);
    		m_lvPi01G1 = m_aPi0List.Get4VecFromEMC(ptPi01->m_pTrack1,0.0);
    		m_lvPi01G2 = m_aPi0List.Get4VecFromEMC(ptPi01->m_pTrack2,0.0);
    		m_lvPi02   = m_aPi0List.Get4VecFromEMC(ptPi02->m_pTrack1,0.0) + m_aPi0List.Get4VecFromEMC(ptPi02->m_pTrack2,0.0);
    		m_lvPi02G1 = m_aPi0List.Get4VecFromEMC(ptPi02->m_pTrack1,0.0);
    		m_lvPi02G2 = m_aPi0List.Get4VecFromEMC(ptPi02->m_pTrack2,0.0);
    		m_aPhIndexList.clear();
   		  m_aPhIndexList.push_back (ptPi01->m_pTrack1);
    		m_aPhIndexList.push_back (ptPi01->m_pTrack2);
    		m_aPhIndexList.push_back (ptPi02->m_pTrack1);
    		m_aPhIndexList.push_back (ptPi02->m_pTrack2);
    		// the lines below MUSST stay below the settings of the 4vectors!
    		if (m_KalKinFit->Fit(1)) {m_dUPi01Chi2 = m_KalKinFit->chisq(1);} else {m_dUPi01Chi2 = -5;}
    		if (m_KalKinFit->Fit(2)) {m_dUPi02Chi2 = m_KalKinFit->chisq(2);} else {m_dUPi02Chi2 = -5;}			
				
        bDone = true;
      }
    }
		


if (!bDone) {m_dUTotalChi2 = -5;m_dUPi01Chi2 = -5; m_dUPi02Chi2 = -5; return false;}

//Fill particle containers
m_UPi01.Fill(&m_lvPi01);
m_UPi02.Fill(&m_lvPi02);
m_UGammas[0].Fill(&m_lvPi01G1);
m_UGammas[1].Fill(&m_lvPi01G2);
m_UGammas[2].Fill(&m_lvPi02G1);
m_UGammas[3].Fill(&m_lvPi02G2);
m_UFitPi01.Fill(&m_lvFitPi01);
m_UFitPi02.Fill(&m_lvFitPi02);
m_UFitGammas[0].Fill(&m_lvFitPi01G1);
m_UFitGammas[1].Fill(&m_lvFitPi01G2);
m_UFitGammas[2].Fill(&m_lvFitPi02G1);
m_UFitGammas[3].Fill(&m_lvFitPi02G2);

m_dU2pi0InvM=(m_lvPi01+m_lvPi02).m();
m_dUFit2pi0InvM=(m_lvFitPi01+m_lvFitPi02).m();
m_dUpi01InvM=m_lvPi01.m();
m_dUFitpi01InvM=m_lvFitPi01.m();
m_dUpi02InvM=m_lvPi02.m();
m_dUFitpi02InvM=m_lvFitPi02.m();

double TotEngBeam = m_dCMSEnergy/2 + m_dMasselec;
double Abs3MomBeam = sqrt(TotEngBeam*TotEngBeam - m_dMasselec*m_dMasselec);
  
m_lvBeamElectron.setPx(0);
m_lvBeamElectron.setPy(0);
m_lvBeamElectron.setPz(-Abs3MomBeam);
m_lvBeamElectron.setE(TotEngBeam);
  
m_lvBeamPositron.setPx(Abs3MomBeam*sin(m_dCrossingAngle));
m_lvBeamPositron.setPy(0);
m_lvBeamPositron.setPz(Abs3MomBeam*cos(m_dCrossingAngle));
m_lvBeamPositron.setE(TotEngBeam);

Hep3Vector vectboostCM=-((m_lvBeamElectron+m_lvBeamPositron).boostVector());	
m_lvBeamElectronCM=m_lvBeamElectron;
m_lvBeamElectronCM.boost(vectboostCM);
m_lvBeamPositronCM=m_lvBeamPositron;
m_lvBeamPositronCM.boost(vectboostCM);
m_lvPi01CM=m_lvPi01;
m_lvPi01CM.boost(vectboostCM);
m_lvPi02CM=m_lvPi02;
m_lvPi02CM.boost(vectboostCM);

m_dUSumPtCM=fabs(m_lvPi01CM.perp()+m_lvPi02CM.perp());

return true;
}
//----------------------------------------------------------------------------------------------------------
*/
