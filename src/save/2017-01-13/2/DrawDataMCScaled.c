#include <TFile.h>
#include <TDirectory.h>
#include <TTree.h>
#include <TChain.h>
#include <TCanvas.h>
#include <TPad.h>
#include <TH1.h>
#include <TH2.h>
#include <THStack.h>
#include <TLine.h>
#include <stdio.h>
#include <string.h>
#include <iostream> //std::cout
#include <sstream>
#include <fstream> //std::ifstream,ofstream
#include <vector>
#include <algorithm> //std::sort
#include <TKey.h>
#include <TList.h>
#include <TStyle.h>
#include <TROOT.h>
#include <TLegend.h>
#include <TLegendEntry.h>

#define SSTR( x ) static_cast< std::ostringstream & >( \
        ( std::ostringstream() << std::dec << x ) ).str()
				
using namespace std;

const double nanotofemto=1E6;

//pch = strtok (str," ,.-"); //tokenize string
vector<TFile*> files;
vector<TTree*> trees;
vector<double> scales;
vector<string> description;

string mode;
string treename;
string histodir;

int isLogScale=1;
//int isLogScale=0;

//int isStacked=0;
int isStacked=1;

vector <string> Tokenize(string str, char delim){
vector <string> strsplit;

stringstream ss;
string strtmp;

ss.str(str);				
		while (getline(ss, strtmp, delim)) {
 		strsplit.push_back(strtmp);
		}	

return strsplit;		
}

/*
void MakeFileList( const char* inputpath)
{

return;
}
*/

bool HistHasLargerIntg(TH1D* h1, TH1D * h2)
{
double val1 = h1->Integral();
double val2 = h2->Integral();

return (val1>val2);
}

bool HistHasSmallerIntg(TH1D* h1, TH1D * h2)
{
double val1 = h1->Integral();
double val2 = h2->Integral();

return (val1<val2);
}

TChain* MakeChain(string filelist,string treename)
{
TChain* chain;
ifstream readList(filelist.c_str());
chain = new TChain(treename.c_str());

if(readList.is_open())
	{
	while(readList.good())
		{
		string strline;
		getline(readList,strline);
		cout << strline << endl;
		TFile* file=TFile::Open(strline.c_str());
		if(file==NULL) continue;
		chain->Add(strline.c_str());
		}
	}

return chain;
}

double GetCrossSection(string logfile,string generator)
{double XSec=1;

ifstream readList(logfile.c_str());

if(readList.is_open())
	{
	while(readList.good())
		{
		string strline;
		getline(readList,strline);
		//cout << strline << endl;
			if(generator=="Galuga")
				{
				if(strline.find("Total e+e- cross section [nb]") != string::npos)
					{
					stringstream ss;
					ss.str(strline);					
					string strtmp;
					vector <string> strsplit;
					while (getline(ss, strtmp, '=')) {
 						strsplit.push_back(strtmp);
						}
					
					XSec=atof((strsplit.back()).c_str());
					printf("XSEC = %e nb \n", XSec);
					//Convert to fb
					XSec=XSec*nanotofemto;
					printf("XSEC = %e fb \n", XSec);
					break;
					}
				}
				
		}
	}

return XSec;	
}


void GetNGenAndCrossSection(string logfile,string generator, int& NGen, double &XSec)
{

ifstream readList(logfile.c_str());

if(readList.is_open())
	{
	while(readList.good())
		{
		string strline;
		getline(readList,strline);
		//cout << strline << endl;
			if(generator=="Galuga")
				{
				if(strline.find("Total e+e- cross section [nb]") != string::npos)
					{
					stringstream ss;
					ss.str(strline);					
					string strtmp;
					vector <string> strsplit;
					while (getline(ss, strtmp, '=')) {
 						strsplit.push_back(strtmp);
						}
					
					XSec=atof((strsplit.back()).c_str());
					printf("XSEC = %e nb \n", XSec);
					//Convert to fb
					XSec=XSec*nanotofemto;
					printf("XSEC = %e fb \n", XSec);
					}
				if(strline.find("time to generate") != string::npos)
					{
					stringstream ss;
					ss.str(strline);					
					string strtmp;
					vector <string> strsplit;
					while (getline(ss, strtmp, ' ')) {
 						strsplit.push_back(strtmp);
						}
						
					//for(int k=0;k<strsplit.size();k++)
					//	{
					//	printf("%d %s \n",k, strsplit[k].c_str());
					//	}
				  
					NGen=atoi(strsplit[9].c_str());
					//cout << NGen << endl;
					//sleep(4);
					}
				}
			if(generator=="Ekhara")
				{
				int NOccurences=0; //Number of occurences of same string
				
				if(strline.find("[nb]") != string::npos)
					{
					NOccurences++;
					if(NOccurences>1) continue;
					stringstream ss;
					ss.str(strline);					
					string strtmp;
					vector <string> strsplit;
					while (getline(ss, strtmp, '=')) {
 						strsplit.push_back(strtmp);
						}
					
					ss.clear();
					ss.str(strsplit.back());
					printf("GetCrossSection : %s \n",strsplit.back().c_str());
					vector <string> strvalsplit;
					while(getline(ss,strtmp, '+')){
					strvalsplit.push_back(strtmp);
					}
					XSec=atof((strvalsplit[0]).c_str());
					printf("XSEC = %e nb \n", XSec);
					//Convert to fb
					XSec=XSec*nanotofemto;
					printf("XSEC = %e fb \n", XSec);
					}
				if(strline.find("Events output:") != string::npos)
					{
					stringstream ss;
					ss.str(strline);					
					string strtmp;
					vector <string> strsplit;
					while (getline(ss, strtmp, ':')) {
 						strsplit.push_back(strtmp);
						}
					cout << strsplit.back() << endl;
					NGen=atoi(strsplit.back().c_str());	
					}
				}
		}
	}

}

void GetNGenAndCrossSectionFromList(string filelist,string generator, int& NGenSum, double &XSecMean)
{

ifstream readList(filelist.c_str());
int Nline=0;
if(readList.is_open())
	{
	while(readList.good())
		{
		Nline++;
		string strline;
		getline(readList,strline);
		int NGen;
		double XSec;
		//cout << NGenSum<< endl;
		GetNGenAndCrossSection(strline,generator,NGen,XSec);
		//cout << NGenSum<< endl;
		NGenSum=NGenSum+NGen;
		XSecMean=XSecMean+XSec;
		}
	}
	
XSecMean=XSecMean/Nline;	
printf("NGenSum= %d NLine=%d XSecMean = %f \n",NGenSum,Nline,XSecMean);	

}



int ParseInputFile(string inputfile,string mode)
{
ifstream readList(inputfile.c_str());
//Parse the input file
if(readList.is_open())
	{
	
	while(readList.good())
		{
		string strline[3];
		stringstream ss;
		string strtmp;
		vector<string> strsplit;
		
		TTree* treeMCTruth;
		
		
		//First line : ROOT file to read
		getline(readList,strline[0]);
		//Split the string
		/*
		char* CStrline=strdup(strline[0].c_str());
		char* splitCstr;
		//splitCstr = strtok (strline[0].c_str()," ");
		//splitCstr = strtok ("TA GUEULE"," ");
		splitCstr = strtok (CStrline," ");		
		//free(CStrline);
		*/
		
				
		ss.str(strline[0]);				
		while (getline(ss, strtmp, ' ')) {
 		strsplit.push_back(strtmp);
		}	
		
		
		//Check for inline arguments	
		for(int k=0;k<strsplit.size();k++) cout << strsplit[k] << endl;
		if(strsplit[1]=="Single")
			{
			files.push_back(TFile::Open(strsplit[2].c_str()));
			if(mode=="--NTuple"){
				TTree* tree = (TTree*)files.back()->Get(treename.c_str());
				trees.push_back(tree);			
				treeMCTruth = (TTree*)files.back()->Get("AllMCTruth");
				}
			}
			
		else if(strsplit[1]=="List")
			{
				if(mode=="--NTuple"){
				TChain* chain = MakeChain(strsplit[2],treename);
				TTree* tree = chain;
				trees.push_back(tree);
			
				TChain* chainMC = MakeChain(strsplit[2],"AllMCTruth");
				treeMCTruth = chainMC;
				}
			}	
		ss.clear();
		strsplit.clear();
		
		
		
		//Second Line : Normalization
		getline(readList,strline[1]);
		
		//Split the string
		ss.str(strline[1]);
		while (getline(ss, strtmp, ' ')) {
 		strsplit.push_back(strtmp);
		}
		//Check for inline arguments	
		if(strsplit[1]=="Factor")
			{
			scales.push_back(atof(strsplit[2].c_str()));
			}
		if(strsplit[1]=="ToLum")
			{
			double luminosity = atof(strsplit[2].c_str()); //luminosity in fb-1
			string generator=strsplit[4];
			string readfiletype=strsplit[5];
			string XSecfile=strsplit[6]; //File that contains cross section information	
			double XSec=0.;
			int NMCTruth=0;
			double NScaled=0.;
			double scale=0.;		
				if(readfiletype=="Single")
					{
					//XSec = GetCrossSection(XSecfile,generator);
					GetNGenAndCrossSection(XSecfile,generator,NMCTruth,XSec);
					if(mode=="--NTuple"){
					NMCTruth = treeMCTruth->GetEntries();
					}
					else if (mode=="--Histo"){
					char hname[256];
					//sprintf hname("AllMCTruth/NumEvt",histodir.c_str());
					sprintf(hname,"%s/NumEvt",histodir.c_str());
					TH1D* hNumEvents = (TH1D*)files.back()->Get(hname);
					//NMCTruth = hNumEvents->GetEntries();
						}							
					}					
				else if(readfiletype=="List")
					{
					GetNGenAndCrossSectionFromList(XSecfile,generator,NMCTruth,XSec);
					}
			NScaled=XSec*luminosity;
			scale=((double)NScaled)/((double)NMCTruth);	
			printf("XSec : %f fb Luminosity : %f fb-1 NMCTruth : %d NScaled : %f Scale : %.2e \n", XSec,luminosity, NMCTruth, NScaled,scale);					
			scales.push_back(scale);
			}
			
		//Third Line description
		getline(readList,strline[2]);
		description.push_back(strline[2]);
		
		}
	}

return 0;
}

vector <double> FindFilledMinMax(TH1D* histo)
{
vector<double> vec;
double min=(histo->GetXaxis())->GetBinLowEdge(histo->FindFirstBinAbove(0.,1));
double max=(histo->GetXaxis())->GetBinUpEdge(histo->FindLastBinAbove(0.,1));

vec.push_back(min);vec.push_back(max);
return vec;
}	
void DrawAnalysis(string inputfile, string option, string outputname)
{
gROOT->SetBatch();

gStyle->SetOptTitle(0);
gStyle->SetOptStat(0);
/*
gStyle->SetPadBottomMargin(0.12); 
gStyle->SetPadTopMargin(0.10); 
gStyle->SetPadLeftMargin(0.10); 
gStyle->SetPadRightMargin(0.30);
*/

gStyle->SetPadBottomMargin(0.12); 
gStyle->SetPadTopMargin(0.10); 
gStyle->SetPadLeftMargin(0.10); 
gStyle->SetPadRightMargin(0.05);
	 
string outps=outputname+".ps";
string outpdf=outputname+".pdf";

vector<string> strsplit = Tokenize(option,' ');
mode=strsplit[0];
if(mode=="--NTuple") treename=strsplit[1];
else if (mode=="--Histo") histodir= strsplit[1];
else {printf("Option invalid \n"); return;}

ParseInputFile(inputfile,mode);

//Draw distributions
char hname[64];
char drawcmd[512];
char drawopt[64];
char selection[512];
char title[512];
	
cout << files.size() << endl;
cout << trees.size() << endl;


TCanvas *c = new TCanvas();
c->Print(Form("%s[",outps.c_str()));
	
if(mode=="--NTuple"){
for(int i=0;i<3;i++)
	{
	if(i==0){	
		sprintf(selection,"");
		c->Divide(2,3);
		
		c->cd(1);
		for(int k=0;k<files.size();k++)
		{
		sprintf(hname,"hdeTCThetaHelicity%d",k);
		sprintf(drawcmd,"deTCThetaHelicity>>%s(100,-1.,1.)",hname);
		sprintf(drawopt,"goff");
	
		if(k==0) trees[k]->Draw(drawcmd,selection,"E");
		else trees[k]->Draw(drawcmd,selection,"same HIST");
		
		//Scale histograms
		TH1D* hist = (TH1D*) gROOT->FindObject(hname);
		hist->Scale(scales[k]);
		hist->SetLineColor(k+1);
		}
		gPad->SetLogy();
		c->cd(2);
		for(int k=0;k<files.size();k++)
		{
		sprintf(hname,"hdeTSumPtCM%d",k);
		sprintf(drawcmd,"deTSumPtCM>>%s(100,0.,1.)",hname);
		sprintf(drawopt,"goff");
	
		if(k==0) trees[k]->Draw(drawcmd,selection,"E");
		else trees[k]->Draw(drawcmd,selection,"same HIST");
		
		//Scale histograms
		TH1D* hist = (TH1D*) gROOT->FindObject(hname);
		hist->Scale(scales[k]);
		hist->SetLineColor(k+1);
		hist->SetMinimum(1);
		}
		gPad->SetLogy();
		
		c->cd(3);
		for(int k=0;k<files.size();k++)
		{
		sprintf(hname,"hdeTSumEemc%d",k);
		sprintf(drawcmd,"deTSumEemc>>%s(100,0.,6.)",hname);
		sprintf(drawopt,"goff");
	
		if(k==0) trees[k]->Draw(drawcmd,selection,"E");
		else trees[k]->Draw(drawcmd,selection,"same HIST");
		
		//Scale histograms
		TH1D* hist = (TH1D*) gROOT->FindObject(hname);
		hist->Scale(scales[k]);
		hist->SetLineColor(k+1);
		hist->SetMinimum(1);
		}
		gPad->SetLogy();
		
		c->cd(4);
		for(int k=0;k<files.size();k++)
		{
		sprintf(hname,"hdeTRGam%d",k);
		sprintf(drawcmd,"deTRGam>>%s(100,0.,1.)",hname);
		sprintf(drawopt,"goff");
	
		if(k==0) trees[k]->Draw(drawcmd,selection,"E");
		else trees[k]->Draw(drawcmd,selection,"same HIST");
		
		//Scale histograms
		TH1D* hist = (TH1D*) gROOT->FindObject(hname);
		hist->Scale(scales[k]);
		hist->SetLineColor(k+1);
		hist->SetMinimum(1);
		}
		gPad->SetLogy();
		c->cd(5);
		for(int k=0;k<files.size();k++)
		{
		sprintf(hname,"hdeT2pi0InvM%d",k);
		sprintf(drawcmd,"deT2pi0InvM>>%s(100,0.26,4.)",hname);
		sprintf(drawopt,"goff");
	
		if(k==0) trees[k]->Draw(drawcmd,selection,"E");
		else trees[k]->Draw(drawcmd,selection,"same HIST");
		
		//Scale histograms
		TH1D* hist = (TH1D*) gROOT->FindObject(hname);
		hist->Scale(scales[k]);
		hist->SetLineColor(k+1);
		}
		gPad->SetLogy();
	}
	
	if(i==1){	
		sprintf(selection,"");
		c->Divide(2,3);
		
		c->cd(1);
		for(int k=0;k<files.size();k++)
		{
		sprintf(hname,"hdeTFitCThetaHelicity%d",k);
		sprintf(drawcmd,"deTFitCThetaHelicity>>%s(100,-1.,1.)",hname);
		sprintf(drawopt,"goff");
	
		if(k==0) trees[k]->Draw(drawcmd,selection,"E");
		else trees[k]->Draw(drawcmd,selection,"same HIST");
		
		//Scale histograms
		TH1D* hist = (TH1D*) gROOT->FindObject(hname);
		hist->Scale(scales[k]);
		hist->SetLineColor(k+1);
		}
		gPad->SetLogy();
		c->cd(2);
		for(int k=0;k<files.size();k++)
		{
		sprintf(hname,"hdeTFitSumPtCM%d",k);
		sprintf(drawcmd,"deTFitSumPtCM>>%s(100,0.,1.)",hname);
		sprintf(drawopt,"goff");
	
		if(k==0) trees[k]->Draw(drawcmd,selection,"E");
		else trees[k]->Draw(drawcmd,selection,"same HIST");
		
		//Scale histograms
		TH1D* hist = (TH1D*) gROOT->FindObject(hname);
		hist->Scale(scales[k]);
		hist->SetLineColor(k+1);
		hist->SetMinimum(1);
		}
		gPad->SetLogy();
		
		c->cd(3);
		for(int k=0;k<files.size();k++)
		{
		sprintf(hname,"hdeTSumEemc%d",k);
		sprintf(drawcmd,"deTSumEemc>>%s(100,0.,6.)",hname);
		sprintf(drawopt,"goff");
	
		if(k==0) trees[k]->Draw(drawcmd,selection,"E");
		else trees[k]->Draw(drawcmd,selection,"same HIST");
		
		//Scale histograms
		TH1D* hist = (TH1D*) gROOT->FindObject(hname);
		hist->Scale(scales[k]);
		hist->SetLineColor(k+1);
		hist->SetMinimum(1);
		}
		gPad->SetLogy();
		
		c->cd(4);
		for(int k=0;k<files.size();k++)
		{
		sprintf(hname,"hdeTFitRGam%d",k);
		sprintf(drawcmd,"deTFitRGam>>%s(100,0.,1.)",hname);
		sprintf(drawopt,"goff");
	
		if(k==0) trees[k]->Draw(drawcmd,selection,"E");
		else trees[k]->Draw(drawcmd,selection,"same HIST");
		
		//Scale histograms
		TH1D* hist = (TH1D*) gROOT->FindObject(hname);
		hist->Scale(scales[k]);
		hist->SetLineColor(k+1);
		hist->SetMinimum(1);
		}
		gPad->SetLogy();
		c->cd(5);
		for(int k=0;k<files.size();k++)
		{
		sprintf(hname,"hdeTFit2pi0InvM%d",k);
		sprintf(drawcmd,"deTFit2pi0InvM>>%s(100,0.26,4.)",hname);
		sprintf(drawopt,"goff");
	
		if(k==0) trees[k]->Draw(drawcmd,selection,"E");
		else trees[k]->Draw(drawcmd,selection,"same HIST");
		
		//Scale histograms
		TH1D* hist = (TH1D*) gROOT->FindObject(hname);
		hist->Scale(scales[k]);
		hist->SetLineColor(k+1);
		}
		gPad->SetLogy();
	}
	
	if(i==2){	
		sprintf(selection,"deTSumEemc<3.5 && deTSumPtCM<0.3 && deTRGam<0.5");
		c->Divide(2,3);
		
		c->cd(1);
		for(int k=0;k<files.size();k++)
		{
		sprintf(hname,"hdeTCThetaHelicity%d",k);
		sprintf(drawcmd,"deTCThetaHelicity>>%s(100,-1.,1.)",hname);
		sprintf(drawopt,"goff");
	
		if(k==0) trees[k]->Draw(drawcmd,selection,"E");
		else trees[k]->Draw(drawcmd,selection,"same HIST");
		
		//Scale histograms
		TH1D* hist = (TH1D*) gROOT->FindObject(hname);
		hist->Scale(scales[k]);
		hist->SetLineColor(k+1);
		}
		gPad->SetLogy();
		c->cd(2);
		for(int k=0;k<files.size();k++)
		{
		sprintf(hname,"hdeTSumPtCM%d",k);
		sprintf(drawcmd,"deTSumPtCM>>%s(100,0.,1.)",hname);
		sprintf(drawopt,"goff");
	
		if(k==0) trees[k]->Draw(drawcmd,selection,"E");
		else trees[k]->Draw(drawcmd,selection,"same HIST");
		
		//Scale histograms
		TH1D* hist = (TH1D*) gROOT->FindObject(hname);
		hist->Scale(scales[k]);
		hist->SetLineColor(k+1);
		hist->SetMinimum(1);
		}
		gPad->SetLogy();
		
		c->cd(3);
		for(int k=0;k<files.size();k++)
		{
		sprintf(hname,"hdeTSumEemc%d",k);
		sprintf(drawcmd,"deTSumEemc>>%s(100,0.,6.)",hname);
		sprintf(drawopt,"goff");
	
		if(k==0) trees[k]->Draw(drawcmd,selection,"E");
		else trees[k]->Draw(drawcmd,selection,"same HIST");
		
		//Scale histograms
		TH1D* hist = (TH1D*) gROOT->FindObject(hname);
		hist->Scale(scales[k]);
		hist->SetLineColor(k+1);
		hist->SetMinimum(1);
		}
		gPad->SetLogy();
		
		c->cd(4);
		for(int k=0;k<files.size();k++)
		{
		sprintf(hname,"hdeTRGam%d",k);
		sprintf(drawcmd,"deTRGam>>%s(100,0.,1.)",hname);
		sprintf(drawopt,"goff");
	
		if(k==0) trees[k]->Draw(drawcmd,selection,"E");
		else trees[k]->Draw(drawcmd,selection,"same HIST");
		
		//Scale histograms
		TH1D* hist = (TH1D*) gROOT->FindObject(hname);
		hist->Scale(scales[k]);
		hist->SetLineColor(k+1);
		hist->SetMinimum(1);
		}
		gPad->SetLogy();
		c->cd(5);
		for(int k=0;k<files.size();k++)
		{
		sprintf(hname,"hdeT2pi0InvM%d",k);
		sprintf(drawcmd,"deT2pi0InvM>>%s(100,0.26,4.)",hname);
		sprintf(drawopt,"goff");
	
		if(k==0) trees[k]->Draw(drawcmd,selection,"E");
		else trees[k]->Draw(drawcmd,selection,"same HIST");
		
		//Scale histograms
		TH1D* hist = (TH1D*) gROOT->FindObject(hname);
		hist->Scale(scales[k]);
		hist->SetLineColor(k+1);
		}
		gPad->SetLogy();
	}
	
	c->Print(Form("%s",outps.c_str()));
	c->Clear();
	}
	
	c->Print(Form("%s]",outps.c_str()));
}

else if(mode=="--Histo"){

TDirectory* directory= files[0]->GetDirectory(histodir.c_str());
TIter nextkey(directory->GetListOfKeys());
TKey *key;
while ((key = (TKey*)nextkey())) {
      TObject *obj = (TObject*)key->ReadObj();
      cout << "Reading: " << obj->GetName() << " " << obj->ClassName()<< endl;				
			if (obj->InheritsFrom("TH1D")) {
				//obj->Draw();
				//c->Print(Form("%s",outps.c_str()));
				//c->Divide(1,2,0.,0.);
						 
				char hname[512];
				sprintf(hname,"%s/%s",histodir.c_str(),obj->GetName());				
				
				vector<double> vecMinMax;
				double min,max;
				double maxHeight=0.;				
				TH1D* hSumMC;
				TH1D* hData;
				TH1D* hDataOverMC= new TH1D();
				vector<TH1D*> vechMC;				
				THStack *hs = new THStack("hs","");				
				
				for(int k=0;k<files.size();k++)
				{
								
				TH1D* hist = (TH1D*) files[k]->Get(hname);
				Color_t color;
				if(k==0) {
				color=1;
				vecMinMax=FindFilledMinMax(hist);
				min=vecMinMax[0];
				max=vecMinMax[1];
				maxHeight=(hist->GetMaximum())*2.;
				}
				else {
				if(k>=8) color=k+3;
				else color=k+2;
				}
				//Scale histograms		
				hist->Scale(scales[k]);
				double intg = hist->Integral();
				char title[256];
				sprintf(title,"%s (%.2e)", description[k].c_str(), intg);
				hist->SetTitle(title);				
				hist->GetXaxis()->SetRangeUser(min,max);
				hist->SetMinimum(1);
				hist->SetMaximum(maxHeight);
				if(k==0)
					{
					hist->SetLineColor(color);
					hist->SetMarkerStyle(8);
					hData=hist;
					}
				else{					
					if(k==1) {hSumMC=(TH1D*) hist->Clone();
					hSumMC->SetTitle("#Sigma MC");
					hSumMC->SetLineColor(2);					
					}
					else hSumMC->Add(hist);
					
					hist->SetFillColor(color);
					hist->SetLineColor(color);
					vechMC.push_back(hist);
					}
				}
				
				if(files.size()>=2){
				//hDataOverMC->Divide(hData,hSumMC);
				hDataOverMC = (TH1D*)hData->Clone();
				hDataOverMC->Divide(hSumMC);
				double maxratio = hDataOverMC->GetBinContent(hDataOverMC->GetMaximumBin());
				hDataOverMC->SetMaximum(maxratio);
				}
				
				//TPad* pads[3];
				//pads[0] = new TPad("pad1","",0.10,0.30,0.70,0.90);
				//pads[1] = new TPad("pad2","",0.10,0.05,0.70,0.30);
				//pads[3] = new TPad("pad3","",0.70,0.05,0.95,0.90);
				
				//pads[0]->cd();
				//pads[0]->Draw();
				
				
				TPad* pads[2];
				 pads[0] = new TPad("upperPad", "upperPad", 
			       .005, .30, .99, .90);
					pads[0]->SetTopMargin(0.);
					pads[0]->SetBottomMargin(0.);
  			 pads[1] = new TPad("lowerPad", "lowerPad", 
			       .005, .005, .99, .30);
				pads[1]->SetTopMargin(0.);	 	 
				pads[0]->Draw();
				pads[1]->Draw();		
				
				/*
				c->cd(1);
				gPad->SetPad("upperPad", "upperPad", 
			       .005, .7525, .995, .995);
				*/	
				pads[0]->cd();	 		 		 
				hData->Draw("goff E");
				if(files.size()>=2){	
				if(isStacked==1){
					vector<TH1D*> vechMCSorted = vechMC;
					std::sort(vechMCSorted.begin(),vechMCSorted.end(),HistHasSmallerIntg);
					for(int k=0;k<vechMCSorted.size();k++){
						hs->Add(vechMCSorted[k]);
						}
					hs->Draw("goff HIST SAME");
					
					}
					
				else{	
					vector<TH1D*> vechMCSorted = vechMC;
					std::sort(vechMCSorted.begin(),vechMCSorted.end(),HistHasLargerIntg);
					for(int k=0;k<vechMCSorted.size();k++){
						if(k==0) vechMCSorted[k]->Draw("goff HIST");
						else vechMCSorted[k]->Draw("goff HIST SAME");
						}
				}
				
				hSumMC->Draw("goff E HIST SAME");
				}
				hData->Draw("goff E SAME");
				
				gPad->SetLogy(isLogScale);
				
				pads[1]->cd();
				if(hDataOverMC!=NULL) hDataOverMC->Draw("E");
				
				//pads[1]->Draw();
				
				//pads[2]->Draw();
				c->cd();
				//TLegend* leg = c->BuildLegend(0.72,0.1,0.99,0.90);
				//TLegend* leg = new TLegend(0.72,0.1,0.99,0.90);
				TLegend* leg = new TLegend(0.1,0.90,0.95,0.99);
				leg->SetNColumns(3);
				leg->SetFillColor(0);
				leg->AddEntry(hData,hData->GetTitle());
				leg->AddEntry(hSumMC,hSumMC->GetTitle());
				for(int k=0;k<vechMC.size();k++) leg->AddEntry(vechMC[k],vechMC[k]->GetTitle());
				leg->Draw();				
				
				c->Print(Form("%s",outps.c_str()));
					
				for(int k=0;k<2;k++) {delete pads[k];}		
				c->Clear();
				
				delete leg;
				delete hs;
				delete hDataOverMC;
				
       }
			if(obj->InheritsFrom("TH2D")) {
				char hname[512];
				sprintf(hname,"%s/%s",histodir.c_str(),obj->GetName());
								
				c->Divide(4,4);
				gStyle->SetOptTitle(1);
				for(int k=0;k<files.size();k++){
					TH2D* hist = (TH2D*) files[k]->Get(hname);
					hist->SetTitle(description[k].c_str());
					c->cd(k+1);
					hist->Scale(scales[k]);
					hist->Draw("goff colz");
					gPad->SetLogz(isLogScale);
					}			
				c->Print(Form("%s",outps.c_str()));
				gStyle->SetOptTitle(0);	
				c->Clear();
				}
			}
c->Print(Form("%s]",outps.c_str()));
}
	
//Convert ps file into pdf with ps2pdf (can be properly view by evince)
//On Himster environment, pdf file created by ROOT is corrupted when opened with evince
//To view the pdf : evince filename.pdf
char systcmd[256];
sprintf(systcmd,"ps2pdf %s %s",outps.c_str(), outpdf.c_str());
system(systcmd);
sprintf(systcmd,"rm %s",outps.c_str());
system(systcmd);
	
//Clear variables
files.clear();
trees.clear();
scales.clear();
description.clear();

delete c;

return;
}

void MultiDrawAnalysis(string option, string outname)
{
string fullOutname;
char fullOption[256];
if(option=="--Histo Untagged")
	{
	for(int k=0;k<=7;k++)
		{
		fullOutname=outname+"/"+"Untagged"+"-"+SSTR(k);
		sprintf(fullOption,"--Histo Untagged/Stage%d",k);
		DrawAnalysis("DrawDataMCScaled.in",fullOption,fullOutname);
		}
	}	
else if(option=="--Histo Tagged")
	{
	//for(int k=0;k<=8;k++)
	for(int k=0;k<=10;k++)
		{
		fullOutname=outname+"/"+"Tagged"+"-"+SSTR(k);
		sprintf(fullOption,"--Histo Tagged/Stage%d",k);
		DrawAnalysis("DrawDataMCScaled.in",fullOption,fullOutname);
		/*
		fullOutname=outname+"/"+"eTagged"+"-"+SSTR(k);
		sprintf(fullOption,"--Histo eTagged/Stage%d",k);
		DrawAnalysis("DrawDataMCScaled.in",fullOption,fullOutname);
		fullOutname=outname+"/"+"pTagged"+"-"+SSTR(k);
		sprintf(fullOption,"--Histo pTagged/Stage%d",k);
		DrawAnalysis("DrawDataMCScaled.in",fullOption,fullOutname);
		*/
		}
	}
return;		
}
