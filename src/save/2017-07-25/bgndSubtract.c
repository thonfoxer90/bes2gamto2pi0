#include <TROOT.h>
#include <TChain.h>
#include <TRint.h>
#include <TTree.h>
#include <TFile.h>
#include <TStyle.h>
#include <TString.h>
#include <TSelector.h>
#include <TF1.h>
#include <TH2.h>
#include <TH1.h>
#include <THnSparse.h>
#include <TVirtualIndex.h>
#include <iostream>             // std::cout, std::endl
#include <fstream>              // std::ifstream
#include <sstream>  
#include <TPad.h>
#include <TCanvas.h>
#include <TGaxis.h>
#include <TPave.h>
#include <TPaveText.h>
#include <TLatex.h>
#include <TVector3.h>
#include <TLorentzVector.h>
#include <TSystem.h>
#include <cstdlib>
#include <map>
#include <time.h>

#include "MyGlobal.cxx"

using namespace std;

void bgndSubtract(string inputcard,string option,string outpath){
ifstream readList(inputcard.c_str());

THnSparseD* hnDData;
vector<THnSparseD*> vechnDBgnd;
vector<string> vecstrTagBgnd;
THnSparseD* hnDSumBgnd;
THnSparseD* hnDSubtr;
 
if(readList.is_open()){
		while(readList.good()){
			string strline;
		
			getline(readList,strline);
			cout << strline << endl;
			//Parse options
			vector<string> strlineSplit = MyGlobal::Tokenize(strline,' ');
			bool dIsBgnd=false;
			
			for(int k=0;k<strlineSplit.size();k++){
				vector<string> strSplit=MyGlobal::Tokenize(strlineSplit[k],'=');
				string arg=strSplit[0];
				string val=strSplit[1];
				cout << val << endl;
				if(arg.find("Data") != string::npos){
					dIsBgnd=false;
					string filename=val;					
					TFile* file = TFile::Open(filename.c_str());
					if(file==NULL){cout << "****** Data FILE CANNOT BE OPENED ******"<< endl; return;}
					hnDData = (THnSparseD*)file->Get("hnD");
					if(hnDData==NULL) {cout << "****** THnSparseD cannot be found" << endl;}
				}
				if(arg.find("Bgnd") != string::npos){
					dIsBgnd=true;
					string filename=val;
					TFile* file = TFile::Open(filename.c_str());
					if(file==NULL){cout << "****** Background FILE CANNOT BE OPENED ******"<< endl; continue;}
					//THnSparseD* hnDBgnd= (THnSparseD*)file->Get("hnD");
					THnSparseD* hnDBgnd= (THnSparseD*)gROOT->FindObject("hnD");
					if(hnDBgnd==NULL) {cout << "****** THnSparseD cannot be found" << endl;continue;}
					else {vechnDBgnd.push_back(hnDBgnd);}
				}
				if(arg.find("Tag") != string::npos && dIsBgnd==true){
				vecstrTagBgnd.push_back(val);
				}
			}
		}
}

string outrootname=outpath+".root";
TFile* outROOT = new TFile(outrootname.c_str(),"RECREATE");

cout << vechnDBgnd.size() << endl;
cout << vecstrTagBgnd.size()<< endl;

for(int k=0;k<vechnDBgnd.size();k++){
	string name="hnD"+vecstrTagBgnd[k];
	vechnDBgnd[k]->SetName(name.c_str());
}

hnDSumBgnd=(THnSparseD*) vechnDBgnd[0]->Clone();
hnDSumBgnd->SetName("hnDSumBgnd");
hnDSumBgnd->SetTitle("Sum of Background;;;");
for(int k=1;k<vechnDBgnd.size();k++){
hnDSumBgnd->Add(vechnDBgnd[k],1);
}

hnDSubtr=(THnSparseD*) hnDData->Clone();
hnDSubtr->SetName("hnDSubtr");
hnDSubtr->SetTitle("Background subtracted;;;");
hnDSubtr->Add(hnDSumBgnd,-1);

for(int k=0;k<vechnDBgnd.size();k++){vechnDBgnd[k]->Write();}
hnDData->Write();
hnDSumBgnd->Write();
hnDSubtr->Write();
outROOT->Write();
outROOT->Close();

return;
}