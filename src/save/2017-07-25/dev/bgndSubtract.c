#include <TROOT.h>
#include <TChain.h>
#include <TRint.h>
#include <TTree.h>
#include <TFile.h>
#include <TStyle.h>
#include <TString.h>
#include <TSelector.h>
#include <TF1.h>
#include <TH2.h>
#include <TH1.h>
#include <THnSparse.h>
#include <TVirtualIndex.h>
#include <iostream>             // std::cout, std::endl
#include <fstream>              // std::ifstream
#include <sstream>  
#include <TPad.h>
#include <TCanvas.h>
#include <TGaxis.h>
#include <TPave.h>
#include <TPaveText.h>
#include <TLatex.h>
#include <TVector3.h>
#include <TLorentzVector.h>
#include <TSystem.h>
#include <cstdlib>
#include <map>
#include <time.h>

#include "MyGlobal.cxx"

using namespace std;

void bgndSubtract(string inputcard,string option,string outpath){
ifstream readList(inputcard.c_str());

THnSparseD* hnDData[3];
THnSparseD* hnDBgnd[3];
vector<THnSparseD*> vechnDBgnd[3];
vector<string> vecstrTagBgnd;
THnSparseD* hnDSumBgnd[3];
THnSparseD* hnDSubtr[3];
 
//string hname("hnD");
//string hname("hnDeT");
//string hname("hnDpT");
vector<string> hnames; 
hnames.resize(3);
hnames[0]=string("hnD");
hnames[1]=string("hnDeT");
hnames[2]=string("hnDpT");

if(readList.is_open()){
		while(readList.good()){
			string strline;
		
			getline(readList,strline);
			cout << strline << endl;
			//Parse options
			vector<string> strlineSplit = MyGlobal::Tokenize(strline,' ');
			bool dIsBgnd=false;
			
			for(int k=0;k<strlineSplit.size();k++){
				vector<string> strSplit=MyGlobal::Tokenize(strlineSplit[k],'=');
				string arg=strSplit[0];
				string val=strSplit[1];
				cout << val << endl;
				if(arg.find("Data") != string::npos){
					dIsBgnd=false;
					string filename=val;					
					TFile* file = TFile::Open(filename.c_str());
					if(file==NULL){cout << "****** Data FILE CANNOT BE OPENED ******"<< endl; return;}
					for(int k=0;k<3;k++){
						hnDData[k] = (THnSparseD*)file->Get(hnames[k].c_str());
						if(hnDData[k]==NULL) {cout << "****** THnSparseD cannot be found" << endl;}
					}
				}
				if(arg.find("Bgnd") != string::npos){
					dIsBgnd=true;
					string filename=val;
					TFile* file = TFile::Open(filename.c_str());
					if(file==NULL){cout << "****** Background FILE CANNOT BE OPENED ******"<< endl; continue;}
					//THnSparseD* hnDBgnd= (THnSparseD*)file->Get(hname.c_str());
					for(int k=0;k<3;k++){
						hnDBgnd[k] = (THnSparseD*)gROOT->FindObject(hnames[k].c_str());
						if(hnDBgnd[k]==NULL) {cout << "****** THnSparseD cannot be found" << endl;continue;}
						else {vechnDBgnd[k].push_back(hnDBgnd[k]);}
					}
				}
				if(arg.find("Tag") != string::npos && dIsBgnd==true){
				vecstrTagBgnd.push_back(val);
				}
			}
		}
}

string outrootname=outpath+".root";
TFile* outROOT = new TFile(outrootname.c_str(),"RECREATE");

cout << vecstrTagBgnd.size()<< endl;
for(int i=0;i<hnames.size();i++){
	cout << vechnDBgnd[i].size() << endl;
	string name;
	for(int k=0;k<vechnDBgnd[i].size();k++){
		name=hnames[i]+vecstrTagBgnd[k];
		vechnDBgnd[i][k]->SetName(name.c_str());
	}

	hnDSumBgnd[i]=(THnSparseD*) vechnDBgnd[i][0]->Clone();
	name=hnames[i]+"SumBgnd";
	hnDSumBgnd[i]->SetName(name.c_str());
	hnDSumBgnd[i]->SetTitle("Sum of Background;;;");
	for(int k=1;k<vechnDBgnd[i].size();k++){
		hnDSumBgnd->Add(vechnDBgnd[i][k],1);
	}

	hnDSubtr[i]=(THnSparseD*) hnDData->Clone();
	name=hnames[i]+"Subtr";
	hnDSubtr[i]->SetName(name);
	hnDSubtr[i]->SetTitle("Background subtracted;;;");
	hnDSubtr[i]->Add(hnDSumBgnd[i],-1);

	for(int k=0;k<vechnDBgnd[i].size();k++){vechnDBgnd[i][k]->Write();}
	hnDData[i]->Write();
	hnDSumBgnd[i]->Write();
	hnDSubtr[i]->Write();
	
}

outROOT->Write();
outROOT->Close();

return;
}