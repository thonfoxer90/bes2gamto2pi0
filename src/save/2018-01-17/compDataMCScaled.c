#include <TFile.h>
#include <TDirectory.h>
#include <TTree.h>
#include <TChain.h>
#include <TCanvas.h>
#include <TPad.h>
#include <TH1.h>
#include <TH2.h>
#include <THStack.h>
#include <TLine.h>
#include <stdio.h>
#include <string.h>
#include <iostream> //std::cout
#include <sstream>
#include <fstream> //std::ifstream,ofstream
#include <vector>
#include <algorithm> //std::sort
#include <TKey.h>
#include <TList.h>
#include <TStyle.h>
#include <TROOT.h>
#include <TLegend.h>
#include <TLegendEntry.h>

#define SSTR( x ) static_cast< std::ostringstream & >( \
        ( std::ostringstream() << std::dec << x ) ).str()
				
using namespace std;

vector<TFile*> files;
vector<TTree*> trees;
vector<double> scales;
vector<string> colors;
vector<string> descriptions;

string mode;
string treename;
string histodir;

//int isLogScale=1;
int isLogScale=0;

//int isStacked=0;
int isStacked=1;

int zoomToFilledXRange=1;
//int zoomToFilledXRange=0;

//int showDatatoMCRatio=1;
int showDatatoMCRatio=0;

int showData=1;
//int showData=0;

bool firstCall=true;

vector <string> Tokenize(string str, char delim){
vector <string> strsplit;

stringstream ss;
string strtmp;

ss.str(str);				
		while (getline(ss, strtmp, delim)) {
 		strsplit.push_back(strtmp);
		}	

return strsplit;		
}

bool HistHasLargerIntg(TH1D* h1, TH1D * h2)
{
double val1 = h1->Integral();
double val2 = h2->Integral();

return (val1>val2);
}

bool HistHasSmallerIntg(TH1D* h1, TH1D * h2)
{
double val1 = h1->Integral();
double val2 = h2->Integral();

return (val1<val2);
}

TChain* makeChain(string filelist,string treename)
{
TChain* chain;
ifstream readList(filelist.c_str());
chain = new TChain(treename.c_str());

if(readList.is_open())
	{
	while(readList.good())
		{
		string strline;
		getline(readList,strline);
		cout << strline << endl;
		TFile* file=TFile::Open(strline.c_str());
		if(file==NULL) continue;
		chain->Add(strline.c_str());
		}
	}

return chain;
}

void InitGlobalVar(){
files.clear();
colors.clear();
descriptions.clear();
}

vector <double> FindFilledMinMax(TH1D* histo)
{
vector<double> vec;
double min=(histo->GetXaxis())->GetBinLowEdge(histo->FindFirstBinAbove(0.,1));
double max=(histo->GetXaxis())->GetBinUpEdge(histo->FindLastBinAbove(0.,1));

vec.push_back(min);vec.push_back(max);
return vec;
}	

void ParseInputFile(string inputcard){
InitGlobalVar();
ifstream readInput(inputcard.c_str());
if(readInput.is_open()){
	while(readInput.good()){
		string strline;
	
		getline(readInput,strline);
		if(strline.find(".root") != string::npos){
		files.push_back(TFile::Open(strline.c_str()));
		}
		else return;
		
		getline(readInput,strline);
		colors.push_back(strline);
		
		getline(readInput,strline);
		descriptions.push_back(strline);
	}
}

return;
}

int getNumberOfSubDir(string inputfile,string dir, string keyword){
int nDirs=0;

TFile* file = TFile::Open(inputfile.c_str());

TDirectory* directory= file->GetDirectory(dir.c_str());

TIter nextkey(directory->GetListOfKeys());
TKey *key;
while ((key = (TKey*)nextkey())) {
      TObject *obj = (TObject*)key->ReadObj();
      cout << "Reading: " << obj->GetName() << " " << obj->ClassName()<< endl;
			string objname(obj->GetName());
			if (obj->InheritsFrom("TDirectory")){
				if(objname.find(keyword)!=string::npos){
					nDirs++;
				}
			}	
		}

return nDirs;		
}

void compDataMCScaled(string inputcard, string option, string outputname){
gROOT->SetBatch();

gStyle->SetOptTitle(0);
gStyle->SetOptStat(0);

gStyle->SetPadBottomMargin(0.12); 
gStyle->SetPadTopMargin(0.10); 
gStyle->SetPadLeftMargin(0.10); 
gStyle->SetPadRightMargin(0.05);
	 
ParseInputFile(inputcard);

vector<string> strsplit = Tokenize(option,' ');
/*
mode=strsplit[0];
if(mode=="NTuple") treename=strsplit[1];
else if (mode=="Histo") histodir= strsplit[1];
else {printf("Option invalid \n"); return;}
*/

for(int k=0;k<strsplit.size();k++){
vector<string> strArgVal = Tokenize(strsplit[k],'=');
string arg=strArgVal[0];
string val=strArgVal[1];
if(arg=="NTuple" ) {mode=arg; treename=val;}
if(arg=="Histo" ) {mode=arg;histodir= val;}
if(arg=="isLogScale"){isLogScale=atoi(val.c_str());}
if(arg=="showData"){showData=atoi(val.c_str());}
}

string outps=outputname+".ps";
string outpdf=outputname+".pdf";

string outROOT = outputname+".root";

TFile* outfile = new TFile(outROOT.c_str(),"RECREATE");

//Draw distributions
char hname[64];
char drawcmd[512];
char drawopt[64];
char selection[512];
char title[512];

TCanvas *c = new TCanvas();
c->Print(Form("%s[",outps.c_str()));

if(mode=="Histo"){

TDirectory* directory= files[0]->GetDirectory(histodir.c_str());
TIter nextkey(directory->GetListOfKeys());
TKey *key;
while ((key = (TKey*)nextkey())) {
      TObject *obj = (TObject*)key->ReadObj();
      cout << "Reading: " << obj->GetName() << " " << obj->ClassName()<< endl;
			c->SetName(obj->GetName());				
			if (obj->InheritsFrom("TH1D")) {
				//obj->Draw();
				//c->Print(Form("%s",outps.c_str()));
				//c->Divide(1,2,0.,0.);
						 
				char hname[512];
				sprintf(hname,"%s/%s",histodir.c_str(),obj->GetName());				
				
				vector<double> vecMinMax;
				double min,max;
				double maxHeight=0.;				
				TH1D* hSumMC;
				TH1D* hData;
				TH1D* hDataOverMC= new TH1D();
				vector<TH1D*> vechMC;				
				THStack *hs = new THStack("hs","");				
				
				for(int k=0;k<files.size();k++)
				{
								
				TH1D* hist = (TH1D*) files[k]->Get(hname);
				if(hist==NULL) continue;
				Color_t color=atof(colors[k].c_str());
				if(k==0) {
				vecMinMax=FindFilledMinMax(hist);
				min=vecMinMax[0];
				max=vecMinMax[1];
				//maxHeight=(hist->GetMaximum())*2.;
				maxHeight=(hist->GetMaximum())*1.;
				}

				double intg = hist->Integral();
				char title[256];
				sprintf(title,"%s (%.2e)", descriptions[k].c_str(), intg);
				hist->SetTitle(title);				
				if(zoomToFilledXRange==1) hist->GetXaxis()->SetRangeUser(min,max);
				if(isLogScale==1) hist->SetMinimum(1);
				else hist->SetMinimum(0);
				hist->SetMaximum(maxHeight);
				if(k==0)
					{
					hist->SetLineColor(color);
					hist->SetMarkerStyle(8);
					hData=hist;
					}
				else{					
					if(k==1) {hSumMC=(TH1D*) hist->Clone();
					hSumMC->SetTitle("#Sigma MC");
					hSumMC->SetLineColor(2);					
					}
					else hSumMC->Add(hist);
					
					hist->SetFillColor(color);
					//hist->SetLineColor(color);
					hist->SetLineColor(1);
					vechMC.push_back(hist);
					}
				}
				
				if(files.size()>=2){
				//hDataOverMC->Divide(hData,hSumMC);
				hDataOverMC = (TH1D*)hData->Clone();
				hDataOverMC->Divide(hSumMC);
				double maxratio = hDataOverMC->GetBinContent(hDataOverMC->GetMaximumBin());
				hDataOverMC->SetMaximum(maxratio);
				}
				
				//TPad* pads[3];
				//pads[0] = new TPad("pad1","",0.10,0.30,0.70,0.90);
				//pads[1] = new TPad("pad2","",0.10,0.05,0.70,0.30);
				//pads[3] = new TPad("pad3","",0.70,0.05,0.95,0.90);
				
				//pads[0]->cd();
				//pads[0]->Draw();
				
				
				TPad* pads[2];
				if(showDatatoMCRatio==1){
				 pads[0] = new TPad("upperPad", "upperPad", 
			       .005, .30, .99, .90);
				pads[0]->SetTopMargin(0.);
				pads[0]->SetBottomMargin(0.);
  			 	pads[1] = new TPad("lowerPad", "lowerPad", 
			       .005, .005, .99, .30);
				pads[1]->SetTopMargin(0.);	 	 
				pads[0]->Draw();
				pads[1]->Draw();		
				}
				else{
				pads[0] = new TPad("upperPad", "upperPad", 
			       .005, .1, .99, .90);
				pads[0]->SetTopMargin(0.);	 
				pads[0]->Draw();
				}
				/*
				c->cd(1);
				gPad->SetPad("upperPad", "upperPad", 
			       .005, .7525, .995, .995);
				*/	
				pads[0]->cd();
					 		 		 
				//if(showData==1) hData->Draw("goff E");
				//else{hData->Draw("AXIS");}
				
				//hData->Draw("goff E");
				//hData->Draw("AXIS");
				
				if(showData==1) hData->Draw("goff E");
				else{
					hData->SetLineColor(0);
					hData->SetMarkerColor(0);
					hData->Draw("HIST");
				}
				
				if(files.size()>=2){	
				if(isStacked==1){
					vector<TH1D*> vechMCSorted = vechMC;
					std::sort(vechMCSorted.begin(),vechMCSorted.end(),HistHasSmallerIntg);
					for(int k=0;k<vechMCSorted.size();k++){
						hs->Add(vechMCSorted[k]);
						}
					if(showData==1) hs->Draw("goff HIST SAME");
					else hs->Draw("goff HIST");
					hs->GetXaxis()->SetTitle(hSumMC->GetXaxis()->GetTitle());
					}
					
				else{	
					vector<TH1D*> vechMCSorted = vechMC;
					std::sort(vechMCSorted.begin(),vechMCSorted.end(),HistHasLargerIntg);
					for(int k=0;k<vechMCSorted.size();k++){
						if(k==0) vechMCSorted[k]->Draw("goff HIST");
						else vechMCSorted[k]->Draw("goff HIST SAME");
						}
				}
				
				hSumMC->Draw("goff HIST SAME");
				}

				if(showData==1) hData->Draw("goff E SAME");
				
				gPad->SetLogy(isLogScale);
				if(showDatatoMCRatio==1){
				pads[1]->cd();
				if(hDataOverMC!=NULL) hDataOverMC->Draw("E");
				}
				
				c->cd();
				//TLegend* leg = new TLegend(0.1,0.90,0.95,0.99);
				TLegend* leg = new TLegend(0.105,0.90,0.945,0.99);
				leg->SetNColumns(3);
				leg->SetFillColor(0);
				if(showData==1) leg->AddEntry(hData,hData->GetTitle());
				leg->AddEntry(hSumMC,hSumMC->GetTitle());
				for(int k=0;k<vechMC.size();k++) leg->AddEntry(vechMC[k],vechMC[k]->GetTitle());
				leg->Draw();				
				
				c->Write();
				c->Print(Form("%s",outps.c_str()));
					
				if(showDatatoMCRatio==1){for(int k=0;k<2;k++) {delete pads[k];}	}
				else{for(int k=0;k<1;k++) {delete pads[k];}}	
				c->Clear();
				
				delete leg;
				delete hs;
				delete hDataOverMC;
				
				/*
				if(files.size()>=2){	
				if(isStacked==1){
					vector<TH1D*> vechMCSorted = vechMC;
					std::sort(vechMCSorted.begin(),vechMCSorted.end(),HistHasSmallerIntg);
					for(int k=0;k<vechMCSorted.size();k++){
						hs->Add(vechMCSorted[k]);
						}
					hs->Draw("goff HIST");
					
					}
					
				else{	
					vector<TH1D*> vechMCSorted = vechMC;
					std::sort(vechMCSorted.begin(),vechMCSorted.end(),HistHasLargerIntg);
					for(int k=0;k<vechMCSorted.size();k++){
						if(k==0) vechMCSorted[k]->Draw("goff HIST");
						else vechMCSorted[k]->Draw("goff HIST SAME");
						}
				}
				
				hSumMC->Draw("goff HIST SAME");
				}
				
				gPad->SetLogy(isLogScale);

				c->cd();
				//TLegend* leg = new TLegend(0.1,0.90,0.95,0.99);
				TLegend* leg = new TLegend(0.105,0.90,0.945,0.99);
				leg->SetNColumns(3);
				leg->SetFillColor(0);
				leg->AddEntry(hSumMC,hSumMC->GetTitle());
				for(int k=0;k<vechMC.size();k++) leg->AddEntry(vechMC[k],vechMC[k]->GetTitle());
				leg->Draw();				
				
				c->Print(Form("%s",outps.c_str()));
					
				if(showDatatoMCRatio==1){for(int k=0;k<2;k++) {delete pads[k];}	}
				else{for(int k=0;k<1;k++) {delete pads[k];}}	
				c->Clear();
				
				delete leg;
				delete hs;
				delete hDataOverMC;
				*/
       }	
					 
			if(obj->InheritsFrom("TH2D")) {
				char hname[512];
				sprintf(hname,"%s/%s",histodir.c_str(),obj->GetName());
								
				c->Divide(4,4);
				gStyle->SetOptTitle(1);
				for(int k=0;k<files.size();k++){
					TH2D* hist = (TH2D*) files[k]->Get(hname);
					if(hist==NULL) continue;
					hist->SetTitle(descriptions[k].c_str());
					c->cd(k+1);
					hist->Draw("goff colz");
					gPad->SetLogz(isLogScale);
					}			
				c->Print(Form("%s",outps.c_str()));
				gStyle->SetOptTitle(0);	
				c->Clear();
				}
				
			}
c->Print(Form("%s]",outps.c_str()));
outfile->Close();
}
//Convert ps file into pdf with ps2pdf (can be properly view by evince)
//On Himster environment, pdf file created by ROOT is corrupted when opened with evince
//To view the pdf : evince filename.pdf
char systcmd[256];
sprintf(systcmd,"ps2pdf %s %s",outps.c_str(), outpdf.c_str());
system(systcmd);
sprintf(systcmd,"rm %s",outps.c_str());
system(systcmd);

return;
}

void MultiCompDataMCScaled(string inputcard, string option, string outname){

string fullOutname;
char fullOption[256];
//Get the first file
ParseInputFile(inputcard);
string firstFileName(files[0]->GetName());
if(option.find("Histo=Untagged")!=string::npos)
	{
	int nStages=getNumberOfSubDir(firstFileName,"Tagged", "Stage");
	cout << nStages << endl;
	for(int k=0;k<nStages;k++)
		{
		fullOutname=outname+"/Untagged-"+SSTR(k);
		sprintf(fullOption,"Histo=Untagged/Stage%d",k);
		compDataMCScaled("DrawDataMCScaled.in",fullOption,fullOutname);
		}
	}	
else if(option.find("Histo=Tagged")!=string::npos)
	{	
	int nStages=getNumberOfSubDir(firstFileName,"Tagged", "Stage");
	cout << nStages << endl;
	
	for(int k=0;k<nStages;k++)
	//for(int k=4;k<=4;k++)
		{
		
		if(option.find("isLogScale=0")!=string::npos){
		 fullOutname=outname+"/Tagged-"+SSTR(k);
		sprintf(fullOption,"Histo=Tagged/Stage%d isLogScale=0",k);
		compDataMCScaled(inputcard,fullOption,fullOutname);
		
		fullOutname=outname+"/eTagged-"+SSTR(k);
		sprintf(fullOption,"Histo=eTagged/Stage%d isLogScale=0",k);
		compDataMCScaled(inputcard,fullOption,fullOutname);
		
		fullOutname=outname+"/pTagged-"+SSTR(k);
		sprintf(fullOption,"Histo=pTagged/Stage%d isLogScale=0",k);
		compDataMCScaled(inputcard,fullOption,fullOutname);
		}
		else{
		fullOutname=outname+"/Tagged-"+SSTR(k);
		sprintf(fullOption,"Histo=Tagged/Stage%d isLogScale=1",k);
		compDataMCScaled(inputcard,fullOption,fullOutname);
		
		fullOutname=outname+"/eTagged-"+SSTR(k);
		sprintf(fullOption,"Histo=eTagged/Stage%d isLogScale=1",k);
		compDataMCScaled(inputcard,fullOption,fullOutname);
		
		fullOutname=outname+"/pTagged-"+SSTR(k);
		sprintf(fullOption,"Histo=pTagged/Stage%d isLogScale=1",k);
		compDataMCScaled(inputcard,fullOption,fullOutname);
		}
		}
	}
return;	
}