#include <TROOT.h>
#include <TChain.h>
#include <TRint.h>
#include <TTree.h>
#include <TFile.h>
#include <TStyle.h>
#include <TString.h>
#include <TSelector.h>
#include <TF1.h>
#include <TH2.h>
#include <TH1.h>
#include <THnSparse.h>
#include <TVirtualIndex.h>
#include <iostream>             // std::cout, std::endl
#include <fstream>              // std::ifstream
#include <sstream>  
#include <TPad.h>
#include <TCanvas.h>
#include <TGaxis.h>
#include <TPave.h>
#include <TPaveText.h>
#include <TArrow.h>
#include <TLatex.h>
#include <TVector3.h>
#include <TLorentzVector.h>
#include <TSystem.h>
#include <cstdlib>
#include <map>
#include <time.h>

#include "MyGlobal.cxx"

bool m_isBinning=false;
int m_DrawMode=1; //1 : 1D  2: 2D
string m_binningdir;
vector<vector<double> > m_vec_limbins;
int m_UseFittedKinematics=1;

void DrawBinLim(TH2* histo,vector <double> limbin)
{
const int dim = (limbin.size())-1;
TArrow* lmin[dim];
TArrow* lmax[dim];
double deltamin;
double deltamax;
	for(int k=0;k<dim;k++) 
	{deltamin=(limbin[k]-limbin[k+1])/2;
	cout << k << " " << deltamin << endl;
	lmin[k] = new TArrow(deltamin,limbin[k],deltamin,limbin[k+1],0.1,"<|>");
	lmin[k]->SetLineColor(2);
	lmin[k]->SetLineWidth(2);
	deltamax=(limbin[k+1]-limbin[k])/2;
	lmax[k] = new TArrow(deltamax,limbin[k],deltamax,limbin[k+1],0.1,"<|>");
	lmax[k]->SetLineColor(2);
	lmax[k]->SetLineWidth(2);
	lmin[k]->Draw("same");
	lmax[k]->Draw("same");
	}
}

void DrawCILines(vector <double> CI, TH1D* histo){
TLine* lCILow = new TLine(CI[0],histo->GetMinimum(),CI[0],histo->GetMaximum());
lCILow->SetLineColor(4);
TLine* lCIUp = new TLine(CI[1],histo->GetMinimum(),CI[1],histo->GetMaximum());
lCIUp->SetLineColor(4);
TLine* lCICenter = new TLine(CI[2],histo->GetMinimum(),CI[2],histo->GetMaximum());
lCICenter->SetLineColor(4);
lCICenter->SetLineStyle(2);
lCILow->Draw();
lCIUp->Draw();
lCICenter->Draw();
}

void DrawDeltaBinning(vector<double> limbin, double mean, TH1D* histo){
double delta=(limbin[1]-limbin[0])/2;
cout << mean << " " << delta-mean << " " << delta+mean << endl;
TLine* lDeltaLow = new TLine(mean-delta,histo->GetMinimum(),mean-delta,histo->GetMaximum());
lDeltaLow->SetLineColor(2);
TLine* lDeltaUp = new TLine(mean+delta,histo->GetMinimum(),mean+delta,histo->GetMaximum());
lDeltaUp->SetLineColor(2);
lDeltaLow->Draw();
lDeltaUp->Draw();
}

void deltaRECGEN(string inputname,string option, string outpath){
TFile* file;
file=TFile::Open(inputname.c_str());
TTree* tree=(TTree*)file->Get("Tagged");

vector<string> optionSplit = MyGlobal::tokenize(option,' ');
cout << optionSplit.size() << endl;
for(int k=0;k<optionSplit.size();k++){
	vector<string> strsplit = MyGlobal::tokenize(optionSplit[k],'=');
	string arg=strsplit[0];
	string val=strsplit[1];
	if(arg=="BinningDir"){m_isBinning=true; m_binningdir=val;}
	if(arg=="Mode"){m_DrawMode=atoi(val.c_str());}
}

if(m_isBinning){
const Int_t nDim = 3;
m_vec_limbins.resize(nDim);
for(int k=0;k<nDim;k++){
		string limbinFullPath;
		string limbinFileName;
		if(k==0) limbinFileName="limbin_Q2.dat";
		if(k==1) limbinFileName="limbin_AbsCThetaSt.dat";
		if(k==2) limbinFileName="limbin_M2Pions.dat";
		limbinFullPath=m_binningdir+"/"+limbinFileName;
		m_vec_limbins[k]=MyGlobal::getLimBin(limbinFullPath);
}
}

string outps=outpath+".ps";
string outpdf=outpath+".pdf";
TCanvas *c;
gStyle->SetOptStat(0);
if(m_DrawMode==1){
	if(m_UseFittedKinematics==0){
		tree->Draw("Q2-Q2GEN>>hDeltaQ2(256,-0.5,0.5)");
		tree->Draw("TMath::Abs(cosThetaStarPi01)-TMath::Abs(cosThetaStarPi01GEN)>>hDeltaCosThetaStar(256,-0.1,0.1)");
		tree->Draw("2Pi0InvM-2Pi0InvMGEN>>hDeltaM2Pi(256,-0.2,0.2)");
	}
	else{
		tree->Draw("Q2Fit-Q2GEN>>hDeltaQ2(256,-0.5,0.5)");
		tree->Draw("TMath::Abs(cosThetaStarPi0Fit1)-TMath::Abs(cosThetaStarPi01GEN)>>hDeltaCosThetaStar(256,-0.1,0.1)");
		tree->Draw("2Pi0InvMFit-2Pi0InvMGEN>>hDeltaM2Pi(256,-0.2,0.2)");
	}
	
	TH1D* hDeltaQ2 = (TH1D*) gROOT->FindObject("hDeltaQ2");
	hDeltaQ2->SetTitle("");
	hDeltaQ2->GetXaxis()->SetTitle("Q^{2}-Q^{2}_{GEN} (GeV^{2})");
	hDeltaQ2->GetYaxis()->SetTitle(Form("Events/%f GeV^{2}",MyGlobal::getDeltaX(hDeltaQ2)));
	TH1D* hDeltaCosThetaStar = (TH1D*) gROOT->FindObject("hDeltaCosThetaStar");
	hDeltaCosThetaStar->SetTitle("");
	hDeltaCosThetaStar->GetXaxis()->SetTitle("|cos #theta*|-|cos #theta*|_{GEN}");
	hDeltaCosThetaStar->GetYaxis()->SetTitle(Form("Events/%f ",MyGlobal::getDeltaX(hDeltaCosThetaStar)));
	TH1D* hDeltaM2Pi = (TH1D*) gROOT->FindObject("hDeltaM2Pi");
	hDeltaM2Pi->SetTitle("");
	hDeltaM2Pi->GetXaxis()->SetTitle("W-W_{GEN}(GeV/c^{2})");
	hDeltaM2Pi->GetYaxis()->SetTitle(Form("Events/%f GeV/c^{2}",MyGlobal::getDeltaX(hDeltaM2Pi)));

	vector<double> CIQ2=MyGlobal::getConfidenceInterval(hDeltaQ2, 0.9);
	vector<double> CICosThetaStar=MyGlobal::getConfidenceInterval(hDeltaCosThetaStar, 0.9);
	vector<double> CIM2Pi=MyGlobal::getConfidenceInterval(hDeltaM2Pi, 0.9);


	c = new TCanvas();
	c->Divide(2,2);
	c->cd(1);
	hDeltaQ2->Draw();
	DrawCILines(CIQ2,hDeltaQ2);
	if(m_isBinning)DrawDeltaBinning(m_vec_limbins[0], CIQ2[2],hDeltaQ2);
	c->cd(2);
	hDeltaCosThetaStar->Draw();
	DrawCILines(CICosThetaStar,hDeltaCosThetaStar);
	if(m_isBinning)DrawDeltaBinning(m_vec_limbins[1], CICosThetaStar[2],hDeltaCosThetaStar);
	c->cd(3);
	hDeltaM2Pi->Draw();
	DrawCILines(CIM2Pi,hDeltaM2Pi);
	if(m_isBinning)DrawDeltaBinning(m_vec_limbins[2], CIM2Pi[2],hDeltaM2Pi);
}

else{
char drawcmd[512];
if(m_UseFittedKinematics==0){
	sprintf(drawcmd,"Q2:Q2-Q2GEN>>hQ2(256,-0.4,0.4,256,0.,5.)");
	tree->Draw(drawcmd);
	sprintf(drawcmd,"TMath::Abs(cosThetaStarPi01):TMath::Abs(cosThetaStarPi01)-TMath::Abs(cosThetaStarPi01GEN)>>hCosThetaStar(256,0.,0.,256,0.,0.)");
	tree->Draw(drawcmd);
	sprintf(drawcmd,"2Pi0InvM:2Pi0InvM-2Pi0InvMGEN>>hM2Pi(256,0.,0.,256,0.,0.)");
	tree->Draw(drawcmd);
}
else{
	sprintf(drawcmd,"Q2Fit:Q2-Q2GEN>>hQ2(256,-0.4,0.4,256,0.,5.)");
	tree->Draw(drawcmd);	sprintf(drawcmd,"TMath::Abs(cosThetaStarPi0Fit1):TMath::Abs(cosThetaStarPi0Fit1)-TMath::Abs(cosThetaStarPi01GEN)>>hCosThetaStar(256,0.,0.,256,0.,0.)");
	tree->Draw(drawcmd);
	sprintf(drawcmd,"2Pi0InvMFit:2Pi0InvMFit-2Pi0InvMGEN>>hM2Pi(256,0.,0.,256,0.,0.)");
	tree->Draw(drawcmd);
}

TH2D* hQ2 = (TH2D*) gROOT->FindObject("hQ2");
TH2D* hCosThetaStar = (TH2D*) gROOT->FindObject("hCosThetaStar");
TH2D* hM2Pi = (TH2D*) gROOT->FindObject("hM2Pi");

c = new TCanvas();
c->Divide(2,2);
c->cd(1);
hQ2->Draw("colz");
hQ2->SetTitle("");
hQ2->GetXaxis()->SetTitle("Q^{2}-Q^{2}_{GEN} (GeV^{2})");
hQ2->GetYaxis()->SetTitle("Q^{2} (GeV^{2})");
DrawBinLim(hQ2,m_vec_limbins[0]);
c->cd(2);
hCosThetaStar->Draw("colz");
hCosThetaStar->SetTitle("");
hCosThetaStar->GetXaxis()->SetTitle("|cos #theta*|-|cos #theta*|_{GEN}");
hCosThetaStar->GetYaxis()->SetTitle("|cos #theta*|");
DrawBinLim(hCosThetaStar,m_vec_limbins[1]);
c->cd(3);
hM2Pi->Draw("colz");
hM2Pi->SetTitle("");
hM2Pi->GetXaxis()->SetTitle("W-W_{GEN}(GeV/c^{2})");
hM2Pi->GetYaxis()->SetTitle("W (GeV/c^{2})");
DrawBinLim(hM2Pi,m_vec_limbins[2]);
}
c->Print(Form("%s",outps.c_str()));

char systcmd[256];
sprintf(systcmd,"ps2pdf %s %s",outps.c_str(), outpdf.c_str());
system(systcmd);
sprintf(systcmd,"rm %s",outps.c_str());
system(systcmd);

}