#include <TFile.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TPad.h>
#include <TH1.h>
#include <TH2.h>
#include <TROOT.h>
#include <TStyle.h>

using namespace std;

void DrawAnalysis(string inputname, string opt)
{

gStyle->SetOptStat(0);
gStyle->SetOptTitle(0);

bool IsAllMC=false;
if(opt=="AllMC") IsAllMC=true;

TFile* file = TFile::Open(inputname.c_str());
TTree* treeUntagged = (TTree*) file->Get("Untagged");
TTree* treeAllMC;
if(IsAllMC) treeAllMC = (TTree*) file->Get("AllMCTruth");

TCanvas *c = new TCanvas();
c->Divide(2,1);
char drawcmd[512];
char selection[512];
char drawopt[16];
	for(int i=0;i<2;i++){
		c->cd(i+1);
		if(i==0){
			treeUntagged->Draw("dUFit2pi0InvM>>hInvM(50,0.27,4.)","","E");
			TH1D* hInvM = (TH1D*) gROOT->FindObject("hInvM");
			hInvM->GetXaxis()->SetTitle("M_{#pi^{0}#pi^{0}} (GeV)");
			gPad->SetLogy();
			}
		if(i==1){
			if(IsAllMC){
				treeAllMC->Draw("AmcUSumPt2Pi0>>hSumPtAllMC(50,0.,0.5)");
				treeUntagged->Draw("dUFitSumPtCM>>hSumPt(50,0.,0.5)","","same E");
				TH1D* h = (TH1D*) gROOT->FindObject("hSumPtAllMC");
				h->SetTitle("All MC-Truth");
				h->GetXaxis()->SetTitle("|#vec{Pt_{#pi^{0}1}}*+#vec{Pt_{#pi^{0}2}}*| (GeV/c)");
				h->SetLineColor(2);
				TH1D* h2 = (TH1D*) gROOT->FindObject("hSumPt");
				h2->SetTitle("Selected data");
				h2->SetMarkerStyle(8);
				//c->BuildLegend();
				}	
			else{
				treeUntagged->Draw("dUFitSumPtCM>>hSumPt(50,0.,0.5)","","E");
				TH1D* h = (TH1D*) gROOT->FindObject("hSumPt");
				h->GetXaxis()->SetTitle("|#vec{Pt_{#pi^{0}1}}*+#vec{Pt_{#pi^{0}2}}*| (GeV/c)");
				}		
			}
		}
	
}
