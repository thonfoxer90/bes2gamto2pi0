#include <TFile.h>
#include <TDirectory.h>
#include <TTree.h>
#include <TChain.h>
#include <TCanvas.h>
#include <TPad.h>
#include <TH1.h>
#include <TH2.h>
#include <THStack.h>
#include <TLine.h>
#include <stdio.h>
#include <string.h>
#include <iostream> //std::cout
#include <sstream>
#include <fstream> //std::ifstream,ofstream
#include <vector>
#include <TKey.h>
#include <TList.h>
#include <TStyle.h>
#include <TROOT.h>
#include <TLegend.h>
#include <TLegendEntry.h>

void rmEmptyNtuples(string inputname, string option){
TFile* file = new TFile(inputname.c_str(),"UPDATE");

//Remove unfilled trees
//Unfilled tree is defined as a tree with one entry and event number=-1
TIter nextkey(file->GetListOfKeys());
TKey *key;
while ((key = (TKey*)nextkey())){
	TObject *obj = (TObject*)key->ReadObj();
  cout << "Reading: " << obj->GetName() << " " << obj->ClassName()<< endl;	
	if(obj->InheritsFrom("TTree")){
		TTree* tree =(TTree*)obj;	
		char fulltreename[256];
		sprintf (fulltreename,"%s;*",obj->GetName());	
		if(tree->GetEntries()==1){
			int EvtNumber=-2;
			tree->SetBranchAddress("EvtNumber",&EvtNumber);
			tree->GetEntry(0);
			if(EvtNumber==-1) {
				cout << "***** Removing : " << fulltreename << " ******"<< endl;
				gDirectory->Delete(fulltreename);
			}
		}
	}
}

file->Close();
}