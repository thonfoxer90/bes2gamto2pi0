#include <TFile.h>
#include <TDirectory.h>
#include <TTree.h>
#include <TBranch.h>
#include <TLeaf.h>
#include <TCanvas.h>
#include <TClass.h>
#include <TDataType.h>
#include <TH1.h>
#include <TH2.h>
#include <TLine.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <utility> //std::pair
#include <TKey.h>
#include <TList.h>
#include <TStyle.h>
#include <TROOT.h>

using namespace std;

void M2pi0vsXs(string inputfile, string outputname, string option)
{

TFile *infile = TFile::Open(inputfile.c_str());
string treename;
if(option.find("--Untagged") != string::npos) {treename="Untagged";}
else if (option.find("--Tagged") != string::npos) {treename="eTagged";}
else {cout << "Invalid input NTuple name" << endl; return;}

if(treename=="Untagged"){
	TTree* tree = (TTree*) infile->Get(treename.c_str());
	
	//Define the list of branch names from NTuple to be read	
	vector<string> varNameList;
	/*
	varNameList.push_back("dU2Pi0InvM");
	varNameList.push_back("UPi01E");
	varNameList.push_back("UPi02E");
	varNameList.push_back("UG0emcE");
	varNameList.push_back("UG1emcE");
	varNameList.push_back("UG2emcE");
	varNameList.push_back("UG3emcE");
	*/
	for(int k=0;k<(tree->GetListOfBranches())->GetEntries();k++)
		{
		TBranch* br = (TBranch*) (tree->GetListOfBranches())->At(k);
		TLeaf* lf = br->GetLeaf(br->GetName());
		// Leaf name = branch name only if branch has only one leaf.
		
		string lfname(lf->GetName());
		string lftypename(lf->GetTypeName());
		if(lftypename=="Double_t") varNameList.push_back(lfname);
		
		}
	
	
	map<string, double> varList;
	//Assign for each branch an internal variable where its value will be stored.
	//The internal variable is labeled using the corresponding branch name.
	//Its value can be retrieved as the following ;
	// double variable = varList[Name of variable in NTuple]
	
	for(int k=0;k<varNameList.size();k++)
		{
		string varname = varNameList[k];
		varList[varname]=0;
				
		double& var=(varList[varname]);
		tree->SetBranchAddress(varname.c_str(),&var);
		
		}
	//Define histograms
	//map<vector<string>,TH2D*> histo2DList;
	vector<pair<vector<string>,TH2D*> > histo2DList;
	vector<string> key;
	pair<vector<string>,TH2D*> pr;
	TH2D* histo;
	char hname[256];
	char htitle[256];
	//1st element of key : X-Axis Variable,  2nd element of key : Y-Axis variable
	key.push_back("dU2Pi0InvM");key.push_back("UG0emcE");
	sprintf(hname,"%svs%s",key[1].c_str(),key[0].c_str());
	sprintf(htitle,";W (GeV/c^{2}); E_{#gamma_{1}} (GeV)");
	histo = new TH2D(hname,htitle,100,0.,2.,100,0.,1.);
	pr=make_pair(key,histo);
	histo2DList.push_back(pr);
	key.clear();
	
	key.push_back("dU2Pi0InvM");key.push_back("UG1emcE");
	sprintf(hname,"%svs%s",key[1].c_str(),key[0].c_str());
	sprintf(htitle,";W (GeV/c^{2}); E_{#gamma_{2}} (GeV)");
	histo = new TH2D(hname,htitle,100,0.,2.,100,0.,1.);
	pr=make_pair(key,histo);
	histo2DList.push_back(pr);
	key.clear();
	
	
	key.push_back("dU2Pi0InvM");key.push_back("UG2emcE");
	sprintf(hname,"%svs%s",key[1].c_str(),key[0].c_str());
	sprintf(htitle,";W (GeV/c^{2}); E_{#gamma_{3}} (GeV)");
	histo = new TH2D(hname,htitle,100,0.,2.,100,0.,1.);
	pr=make_pair(key,histo);
	histo2DList.push_back(pr);
	key.clear();
	
	key.push_back("dU2Pi0InvM");key.push_back("UG3emcE");
	sprintf(hname,"%svs%s",key[1].c_str(),key[0].c_str());
	sprintf(htitle,";W (GeV/c^{2}); E_{#gamma_{4}} (GeV)");
	histo = new TH2D(hname,htitle,100,0.,2.,100,0.,1.);
	pr=make_pair(key,histo);
	histo2DList.push_back(pr);
	key.clear();
	
	key.push_back("dU2Pi0InvM");key.push_back("UPi01E");
	sprintf(hname,"%svs%s",key[1].c_str(),key[0].c_str());
	sprintf(htitle,";W (GeV/c^{2}); E_{#pi^{0}_{1}} (GeV)");
	histo=new TH2D(hname,htitle,100,0.,2.,100,0.,1.);
	pr=make_pair(key,histo);
	histo2DList.push_back(pr);
	key.clear();
	
	key.push_back("dU2Pi0InvM");key.push_back("UPi02E");
	sprintf(hname,"%svs%s",key[1].c_str(),key[0].c_str());
	sprintf(htitle,";W (GeV/c^{2}); E_{#pi^{0}_{2}} (GeV)");
	histo = new TH2D(hname,htitle,100,0.,2.,100,0.,1.);
	pr=make_pair(key,histo);
	histo2DList.push_back(pr);
	key.clear();
	
	//Read the Ntuple and fill histograms
	for(int entry=0;entry<tree->GetEntries();entry++)
		{
		tree->GetEntry(entry);
		
		for(int k=0;k<histo2DList.size();k++)
			{
			pr=histo2DList[k];
			key = pr.first;
			TH2D* histo2D = pr.second;
			
			histo2D->Fill(varList[key[0]],varList[key[1]]);
			key.clear();
			}
			
		}
	
	//Draw histograms and save them into a .pdf
	string outps=outputname+".ps";
string outpdf=outputname+".pdf";
	
	TCanvas *c = new TCanvas();
	c->Print(Form("%s[",outps.c_str()));

	for(int k=0;k<histo2DList.size();k++)
		{
		pr=histo2DList[k];
		key = pr.first;
		TH2D* histo2D = pr.second;
		
		histo2D->Draw("colz");
		c->SetLogz();
		c->Print(Form("%s",outps.c_str()));
		}
	c->Print(Form("%s]",outps.c_str()));
		
//Convert ps file into pdf with ps2pdf (can be properly view by evince)
//On Himster environment, pdf file created by ROOT is corrupted when opened with evince
//To view the pdf : evince filename.pdf
char systcmd[256];
sprintf(systcmd,"ps2pdf %s %s",outps.c_str(), outpdf.c_str());
system(systcmd);
sprintf(systcmd,"rm %s",outps.c_str());
system(systcmd);
	
	}
	
return;

}