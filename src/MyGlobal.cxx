#include "../Bes2Gamto2Pi0/MyGlobal.h"

using namespace std;

double MyGlobal::phi0to2Pi(double phi){
	double newphi;
	if(phi<0.) newphi=2*TMath::Pi()+phi;
	else newphi=phi;
	return newphi;
}

vector <string> MyGlobal::tokenize(string str, char delim){
	vector <string> strsplit;

	stringstream ss;
	string strtmp;

	ss.str(str);				
		while (getline(ss, strtmp, delim)) {
 		strsplit.push_back(strtmp);
		}	

	return strsplit;		
}

vector<string> MyGlobal::tokenizeArgs(string command){
	vector<string> qargs;
  int len = command.length();
  bool qot = false, sqot = false;
  int arglen;
  for(int i = 0; i < len; i++) {
          int start = i;
          if(command[i] == '\"') {
                  qot = true;
          }
          else if(command[i] == '\'') sqot = true;

          if(qot) {
                  i++;
                  start++;
                  while(i<len && command[i] != '\"')
                          i++;
                  if(i<len)
                          qot = false;
                  arglen = i-start;
                  i++;
          }
          else if(sqot) {
                  i++;
                  while(i<len && command[i] != '\'')
                          i++;
                  if(i<len)
                          sqot = false;
                  arglen = i-start;
                  i++;
          }
          else{
                  while(i<len && command[i]!=' ')
                          i++;
                  arglen = i-start;
          }
          qargs.push_back(command.substr(start, arglen));
  }
  if(qot || sqot) std::cout<<"One of the quotes is open\n";
	return qargs;
}

vector<double> MyGlobal::getLimBin(string tablepath){
	vector<double> limbin;

	ifstream readTable(tablepath.c_str());

	if(readTable.is_open()){
		while(readTable.good()){
			limbin.push_back(0.);
			readTable >> limbin.back();
		}
	}
	return limbin;
}

double MyGlobal::getDeltaX(TH1D* histo){
	double delta=0;
	int nBinsX = histo->GetNbinsX();
	double xmin = histo->GetXaxis()->GetXmin();
	double xmax = histo->GetXaxis()->GetXmax();

	delta=(xmax-xmin)/((double)nBinsX);
	return delta;
}

string MyGlobal::intToStr(int val){
	string str;
	ostringstream convert;   // stream used for the conversion
	convert << val;      // insert the textual representation of 'Number' in the characters in the stream
	str= convert.str(); // set 'Result' to the contents of the stream

	return str;
}

string MyGlobal::doubleToStr(double val){
	string str;
	ostringstream convert;   // stream used for the conversion
	convert << val;      // insert the textual representation of 'Number' in the characters in the stream
	str= convert.str(); // set 'Result' to the contents of the stream

	return str;
}

string MyGlobal::doubleToStr(double val, int precision){
	string str;
	ostringstream convert;   // stream used for the conversion
	convert << std::fixed<< std::setprecision(precision) << val;      // insert the textual representation of 'Number' in the characters in the stream
	str= convert.str(); // set 'Result' to the contents of the stream

	return str;
}

vector< double>  MyGlobal::getConfidenceInterval(TH1D* histo, double fracintg)
{
	vector<double> CI;
	CI.resize(3);
	//double center = histo->GetMean();
	double center = histo->GetXaxis()->GetBinCenter(histo->GetMaximumBin());
	int bincenter = histo->FindBin(center);


	double intgTot=histo->Integral();
	double intg=0;

	for(int bin=1;bin<=(histo->GetNbinsX()/2);bin++ )
		{
			intg = histo->Integral(bincenter-bin,bincenter+bin);
			if(intg/intgTot>=fracintg) 
			{
				CI[0]=histo->GetXaxis()->GetBinLowEdge(bincenter-bin);
				CI[1]=histo->GetXaxis()->GetBinUpEdge(bincenter+bin);
				break;
			}
		}

	CI[2]=center;
	printf("C[0] = %f C[1] = %f \n",CI[0],CI[1]);
return CI;
}

TChain* MyGlobal::makeChain(string filelist,string treename)
{
	TChain* chain;
	ifstream readList(filelist.c_str());
	chain = new TChain(treename.c_str());

	if(readList.is_open())
		{
		while(readList.good())
			{
				string strline;
				getline(readList,strline);
				cout << strline << endl;
				TFile* file=TFile::Open(strline.c_str());
				if(file==NULL) continue;
				chain->Add(strline.c_str());
			}
		}

	return chain;
}
