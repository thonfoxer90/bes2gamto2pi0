using namespace std;

void dLggdzTodLggdW(string inputfile,double ECMS,string outpath){
ifstream readFile(inputfile.c_str());

vector<double> vecX;
vector<double> vecY;

if(readFile.is_open()){
	while(readFile.good()){
		double X,Y;
		readFile >> X >> Y;
		vecX.push_back(X);
		vecY.push_back(Y);
	}
}

string outROOTname=outpath+".root";
TFile* outROOT = new TFile(outROOTname.c_str(),"RECREATE");

gr1D = new TGraph();

int Npt=0;
for(int k=0;k<vecX.size();k++){
					double z=vecX[k];
					double dLggdz=vecY[k];
					
					double W=ECMS*z;
					double dLggdW=dLggdz/ECMS;
					if(dLggdW!=dLggdW) continue;
					gr1D->SetPoint(Npt,W,dLggdW);
					Npt++;
}
gr1D->Sort();

gr1D->SetName("gr1D");
gr1D->SetTitle("");
gr1D->GetXaxis()->SetTitle("W (GeV/c^{2})");
gr1D->GetXaxis()->SetName("W");
gr1D->GetYaxis()->SetTitle("dL_{#gamma #gamma}/dW");
gr1D->GetYaxis()->SetName("LGamGam");

gr1D->Write();
outROOT->Close();
}