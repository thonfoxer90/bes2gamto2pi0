#include <TROOT.h>
#include <TChain.h>
#include <TRint.h>
#include <TTree.h>
#include <TFile.h>
#include <TStyle.h>
#include <TString.h>
#include <TSelector.h>
#include <TF1.h>
#include <TH2.h>
#include <TH1.h>
#include <THnSparse.h>
#include <TVirtualIndex.h>
#include <iostream>             // std::cout, std::endl
#include <fstream>              // std::ifstream
#include <sstream>  
#include <TPad.h>
#include <TCanvas.h>
#include <TGaxis.h>
#include <TPave.h>
#include <TPaveText.h>
#include <TLatex.h>
#include <TVector3.h>
#include <TLorentzVector.h>
#include <TSystem.h>
#include <cstdlib>
#include <map>
#include <time.h>

#include "MyGlobal.cxx"

using namespace std;

void drawCompWUnfitFit(string input, string option, string outpath){
TFile* file = TFile::Open(input.c_str());

outpdf=outpath+".pdf";

gStyle->SetOptStat(0);
gStyle->SetOptTitle(0);

TCanvas *c = new TCanvas();
c->Divide(1,2);
c->cd(1);
TH1D* hUnfit = (TH1D*) file->Get("Tagged/Stage11/2Pi0InvM");
hUnfit->GetXaxis()->SetTitle("W (GeV/c^{2})");
hUnfit->Rebin(2);
hUnfit->GetYaxis()->SetTitle(Form("Events/%.2f (GeV/c^{2})",MyGlobal::getDeltaX(hUnfit)));
TH1D* hFit = (TH1D*) file->Get("Tagged/Stage11/2Pi0InvMFit");
hFit->GetXaxis()->SetTitle("W (GeV/c^{2})");
hFit->SetLineColor(2);
hFit->Rebin(2);
hFit->GetYaxis()->SetTitle(Form("Events/%.2f (GeV/c^{2})",MyGlobal::getDeltaX(hFit)));
		
hUnfit->Draw("E1");
hFit->Draw("E1 SAME");

TLegend* leg = new TLegend(0.70,0.80,0.90,0.90);
leg->SetFillColor(0);
leg->AddEntry(hUnfit,"Unfitted kinematics");
leg->AddEntry(hFit,"Fitted kinematics");
leg->Draw();

c->cd(2);
TH1D* hRatio = (TH1D*) hFit->Clone();
hRatio->SetLineColor(1);
hRatio->Divide(hUnfit);
hRatio->Draw("E1");

TLine* lRatio = new TLine(0.,1.,hRatio->GetXaxis()->GetXmax(),1.);
lRatio->SetLineColor(4);
lRatio->SetLineWidth(2);
lRatio->Draw();

c->Print(outpdf.c_str());
}