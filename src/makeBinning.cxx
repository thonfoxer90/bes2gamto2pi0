#include <TROOT.h>
#include <TMath.h>
#include <TStyle.h>
#include <iostream>     // std::cout
#include <fstream>      // std::ofstream
#include <vector>

using namespace std;

const double MPi0=134.9766e-3; //GeV

void writeTable(vector <double> limbin, char* outfilename){
ofstream Table;
Table.open(outfilename);
cout << outfilename << endl;
for(int k=0;k<limbin.size();k++) 
{if(k<limbin.size()-1) Table << limbin[k] << endl; else Table << limbin[k];}
Table.close();

return;
}

void makeBinning(string option, string outpath){


	
	if(option.find("Q2M2PiAbsCThetaSt_10x45x5") != string::npos)
	{
	vector <double> limbin_Q2;
	vector <double> limbin_M2Pions;
	vector <double> limbin_AbsCThetaSt;
	
	int nBinsQ2=10;
	int nBinsM2Pions=45;
	int nBinsAbsCThetaSt=5;
	
	limbin_Q2.resize(nBinsQ2+1);
	limbin_M2Pions.resize(nBinsM2Pions+1);
	limbin_AbsCThetaSt.resize(nBinsAbsCThetaSt+1);
	
	limbin_Q2.front()=0.1; limbin_Q2.back()=3.;
	limbin_M2Pions.front()=2*MPi0; limbin_M2Pions.back()=3.;
	limbin_AbsCThetaSt.front()=0.;limbin_AbsCThetaSt.back()=1.;
	
	for(int k=1;k<limbin_Q2.size();k++) limbin_Q2[k]=limbin_Q2[k-1]+(limbin_Q2.back()-limbin_Q2.front())/nBinsQ2;
	for(int k=1;k<limbin_M2Pions.size();k++) limbin_M2Pions[k]=limbin_M2Pions[k-1]+(limbin_M2Pions.back()-limbin_M2Pions.front())/nBinsM2Pions;
	for(int k=1;k<limbin_AbsCThetaSt.size();k++) limbin_AbsCThetaSt[k]=limbin_AbsCThetaSt[k-1]+(limbin_AbsCThetaSt.back()-limbin_AbsCThetaSt.front())/nBinsAbsCThetaSt;
	
	char outfilename[256];
	
	sprintf(outfilename,"%s/limbin_Q2.dat",outpath.c_str());
	writeTable(limbin_Q2, outfilename);
	
	sprintf(outfilename,"%s/limbin_M2Pions.dat",outpath.c_str());
	writeTable(limbin_M2Pions, outfilename);
	
	sprintf(outfilename,"%s/limbin_AbsCThetaSt.dat",outpath.c_str());
	writeTable(limbin_AbsCThetaSt, outfilename);
	}
	
	if(option.find("Q2M2PiAbsCThetaSt_32x32x5") != string::npos)
	{
	vector <double> limbin_Q2;
	vector <double> limbin_M2Pions;
	vector <double> limbin_AbsCThetaSt;
	
	int nBinsQ2=32;
	int nBinsM2Pions=32;
	int nBinsAbsCThetaSt=5;
	
	limbin_Q2.resize(nBinsQ2+1);
	limbin_M2Pions.resize(nBinsM2Pions+1);
	limbin_AbsCThetaSt.resize(nBinsAbsCThetaSt+1);
	
	limbin_Q2.front()=0.1; limbin_Q2.back()=3.;
	limbin_M2Pions.front()=2*MPi0; limbin_M2Pions.back()=3.;
	limbin_AbsCThetaSt.front()=0.;limbin_AbsCThetaSt.back()=1.;
	
	for(int k=1;k<limbin_Q2.size();k++) limbin_Q2[k]=limbin_Q2[k-1]+(limbin_Q2.back()-limbin_Q2.front())/nBinsQ2;
	for(int k=1;k<limbin_M2Pions.size();k++) limbin_M2Pions[k]=limbin_M2Pions[k-1]+(limbin_M2Pions.back()-limbin_M2Pions.front())/nBinsM2Pions;
	for(int k=1;k<limbin_AbsCThetaSt.size();k++) limbin_AbsCThetaSt[k]=limbin_AbsCThetaSt[k-1]+(limbin_AbsCThetaSt.back()-limbin_AbsCThetaSt.front())/nBinsAbsCThetaSt;
	
	char outfilename[256];
	
	sprintf(outfilename,"%s/limbin_Q2.dat",outpath.c_str());
	writeTable(limbin_Q2, outfilename);
	
	sprintf(outfilename,"%s/limbin_M2Pions.dat",outpath.c_str());
	writeTable(limbin_M2Pions, outfilename);
	
	sprintf(outfilename,"%s/limbin_AbsCThetaSt.dat",outpath.c_str());
	writeTable(limbin_AbsCThetaSt, outfilename);
	}
	
	if(option.find("ECMS15GeV_Q2M2PiAbsCThetaSt_10x45x5") != string::npos)
	{
	vector <double> limbin_Q2;
	vector <double> limbin_M2Pions;
	vector <double> limbin_AbsCThetaSt;
	
	int nBinsQ2=10;
	int nBinsM2Pions=45;
	int nBinsAbsCThetaSt=5;
	
	limbin_Q2.resize(nBinsQ2+1);
	limbin_M2Pions.resize(nBinsM2Pions+1);
	limbin_AbsCThetaSt.resize(nBinsAbsCThetaSt+1);
	
	limbin_Q2.front()=0.; limbin_Q2.back()=3.;
	limbin_M2Pions.front()=2*MPi0; limbin_M2Pions.back()=15.;
	limbin_AbsCThetaSt.front()=0.;limbin_AbsCThetaSt.back()=1.;
	
	for(int k=1;k<limbin_Q2.size();k++) limbin_Q2[k]=limbin_Q2[k-1]+(limbin_Q2.back()-limbin_Q2.front())/nBinsQ2;
	for(int k=1;k<limbin_M2Pions.size();k++) limbin_M2Pions[k]=limbin_M2Pions[k-1]+(limbin_M2Pions.back()-limbin_M2Pions.front())/nBinsM2Pions;
	for(int k=1;k<limbin_AbsCThetaSt.size();k++) limbin_AbsCThetaSt[k]=limbin_AbsCThetaSt[k-1]+(limbin_AbsCThetaSt.back()-limbin_AbsCThetaSt.front())/nBinsAbsCThetaSt;
	
	char outfilename[256];
	
	sprintf(outfilename,"%s/limbin_Q2.dat",outpath.c_str());
	writeTable(limbin_Q2, outfilename);
	
	sprintf(outfilename,"%s/limbin_M2Pions.dat",outpath.c_str());
	writeTable(limbin_M2Pions, outfilename);
	
	sprintf(outfilename,"%s/limbin_AbsCThetaSt.dat",outpath.c_str());
	writeTable(limbin_AbsCThetaSt, outfilename);
	}	
	
	if(option.find("Q2M2PiAbsCThetaStPhiSt_10x45x5x5") != string::npos)
	{
	vector <double> limbin_Q2;
	vector <double> limbin_M2Pions;
	vector <double> limbin_AbsCThetaSt;
	vector <double> limbin_PhiSt;
	
	int nBinsQ2=10;
	int nBinsM2Pions=45;
	int nBinsAbsCThetaSt=5;
	int nBinsPhiSt=5;
	
	limbin_Q2.resize(nBinsQ2+1);
	limbin_M2Pions.resize(nBinsM2Pions+1);
	limbin_AbsCThetaSt.resize(nBinsAbsCThetaSt+1);
	limbin_PhiSt.resize(nBinsPhiSt+1);
	
	//limbin_Q2.front()=0.; limbin_Q2.back()=3.;
	limbin_Q2.front()=0.1; limbin_Q2.back()=3.;
	limbin_M2Pions.front()=2*MPi0; limbin_M2Pions.back()=3.;
	limbin_AbsCThetaSt.front()=0.;limbin_AbsCThetaSt.back()=1.;
	//limbin_PhiSt.front()=0.;limbin_PhiSt.back()=2*(TMath::Pi());
	limbin_PhiSt.front()=0.;limbin_PhiSt.back()=(TMath::Pi());
	
	for(int k=1;k<limbin_Q2.size();k++) limbin_Q2[k]=limbin_Q2[k-1]+(limbin_Q2.back()-limbin_Q2.front())/nBinsQ2;
	for(int k=1;k<limbin_M2Pions.size();k++) limbin_M2Pions[k]=limbin_M2Pions[k-1]+(limbin_M2Pions.back()-limbin_M2Pions.front())/nBinsM2Pions;
	for(int k=1;k<limbin_AbsCThetaSt.size();k++) limbin_AbsCThetaSt[k]=limbin_AbsCThetaSt[k-1]+(limbin_AbsCThetaSt.back()-limbin_AbsCThetaSt.front())/nBinsAbsCThetaSt;
	for(int k=1;k<limbin_PhiSt.size();k++) limbin_PhiSt[k]=limbin_PhiSt[k-1]+(limbin_PhiSt.back()-limbin_PhiSt.front())/nBinsPhiSt;
	
	char outfilename[256];
	
	sprintf(outfilename,"%s/limbin_Q2.dat",outpath.c_str());
	writeTable(limbin_Q2, outfilename);
	
	sprintf(outfilename,"%s/limbin_M2Pions.dat",outpath.c_str());
	writeTable(limbin_M2Pions, outfilename);
	
	sprintf(outfilename,"%s/limbin_AbsCThetaSt.dat",outpath.c_str());
	writeTable(limbin_AbsCThetaSt, outfilename);
	
	sprintf(outfilename,"%s/limbin_PhiSt.dat",outpath.c_str());
	writeTable(limbin_PhiSt, outfilename);
	}
	
	if(option.find("Q2M2PiAbsCThetaStPhiSt_10x45x5x9") != string::npos)
	{
	vector <double> limbin_Q2;
	vector <double> limbin_M2Pions;
	vector <double> limbin_AbsCThetaSt;
	vector <double> limbin_PhiSt;
	
	int nBinsQ2=10;
	int nBinsM2Pions=45;
	int nBinsAbsCThetaSt=5;
	int nBinsPhiSt=9;
	
	limbin_Q2.resize(nBinsQ2+1);
	limbin_M2Pions.resize(nBinsM2Pions+1);
	limbin_AbsCThetaSt.resize(nBinsAbsCThetaSt+1);
	limbin_PhiSt.resize(nBinsPhiSt+1);
	
	//limbin_Q2.front()=0.; limbin_Q2.back()=3.;
	limbin_Q2.front()=0.1; limbin_Q2.back()=3.;
	limbin_M2Pions.front()=2*MPi0; limbin_M2Pions.back()=3.;
	limbin_AbsCThetaSt.front()=0.;limbin_AbsCThetaSt.back()=1.;
	
	//limbin_PhiSt.front()=0.;limbin_PhiSt.back()=2*(TMath::Pi());
	//limbin_PhiSt.front()=-(TMath::Pi());limbin_PhiSt.back()=(TMath::Pi());
	limbin_PhiSt.front()=0;limbin_PhiSt.back()=(TMath::Pi());
	
	for(int k=1;k<limbin_Q2.size();k++) limbin_Q2[k]=limbin_Q2[k-1]+(limbin_Q2.back()-limbin_Q2.front())/nBinsQ2;
	for(int k=1;k<limbin_M2Pions.size();k++) limbin_M2Pions[k]=limbin_M2Pions[k-1]+(limbin_M2Pions.back()-limbin_M2Pions.front())/nBinsM2Pions;
	for(int k=1;k<limbin_AbsCThetaSt.size();k++) limbin_AbsCThetaSt[k]=limbin_AbsCThetaSt[k-1]+(limbin_AbsCThetaSt.back()-limbin_AbsCThetaSt.front())/nBinsAbsCThetaSt;
	for(int k=1;k<limbin_PhiSt.size();k++) limbin_PhiSt[k]=limbin_PhiSt[k-1]+(limbin_PhiSt.back()-limbin_PhiSt.front())/nBinsPhiSt;
	
	char outfilename[256];
	
	sprintf(outfilename,"%s/limbin_Q2.dat",outpath.c_str());
	writeTable(limbin_Q2, outfilename);
	
	sprintf(outfilename,"%s/limbin_M2Pions.dat",outpath.c_str());
	writeTable(limbin_M2Pions, outfilename);
	
	sprintf(outfilename,"%s/limbin_AbsCThetaSt.dat",outpath.c_str());
	writeTable(limbin_AbsCThetaSt, outfilename);
	
	sprintf(outfilename,"%s/limbin_PhiSt.dat",outpath.c_str());
	writeTable(limbin_PhiSt, outfilename);
	}
	
	if(option.find("Q2M2PiAbsCThetaStPhiSt_5x36x5x9") != string::npos)
	{
	vector <double> limbin_Q2;
	vector <double> limbin_M2Pions;
	vector <double> limbin_AbsCThetaSt;
	vector <double> limbin_PhiSt;
	
	int nBinsQ2=5;
	int nBinsM2Pions=36;
	int nBinsAbsCThetaSt=5;
	int nBinsPhiSt=9;
	
	limbin_Q2.resize(nBinsQ2+1);
	limbin_M2Pions.resize(nBinsM2Pions+1);
	limbin_AbsCThetaSt.resize(nBinsAbsCThetaSt+1);
	limbin_PhiSt.resize(nBinsPhiSt+1);
	
	limbin_Q2.front()=0.1; limbin_Q2[1]=0.3;limbin_Q2[2]=0.5;limbin_Q2[3]=0.7;limbin_Q2[4]=1.;
	limbin_Q2.back()=3.;
	
	limbin_M2Pions.front()=2*MPi0; limbin_M2Pions.back()=3.;
	limbin_AbsCThetaSt.front()=0.;limbin_AbsCThetaSt.back()=1.;
	
	limbin_PhiSt.front()=0;limbin_PhiSt.back()=(TMath::Pi());
	
	
	for(int k=1;k<limbin_M2Pions.size();k++) limbin_M2Pions[k]=limbin_M2Pions[k-1]+(limbin_M2Pions.back()-limbin_M2Pions.front())/nBinsM2Pions;
	for(int k=1;k<limbin_AbsCThetaSt.size();k++) limbin_AbsCThetaSt[k]=limbin_AbsCThetaSt[k-1]+(limbin_AbsCThetaSt.back()-limbin_AbsCThetaSt.front())/nBinsAbsCThetaSt;
	for(int k=1;k<limbin_PhiSt.size();k++) limbin_PhiSt[k]=limbin_PhiSt[k-1]+(limbin_PhiSt.back()-limbin_PhiSt.front())/nBinsPhiSt;
	
	char outfilename[256];
	
	sprintf(outfilename,"%s/limbin_Q2.dat",outpath.c_str());
	writeTable(limbin_Q2, outfilename);
	
	sprintf(outfilename,"%s/limbin_M2Pions.dat",outpath.c_str());
	writeTable(limbin_M2Pions, outfilename);
	
	sprintf(outfilename,"%s/limbin_AbsCThetaSt.dat",outpath.c_str());
	writeTable(limbin_AbsCThetaSt, outfilename);
	
	sprintf(outfilename,"%s/limbin_PhiSt.dat",outpath.c_str());
	writeTable(limbin_PhiSt, outfilename);
	}
	
	if(option.find("Q2M2PiAbsCThetaStPhiSt_5x36x10x9") != string::npos)
	{
	vector <double> limbin_Q2;
	vector <double> limbin_M2Pions;
	vector <double> limbin_AbsCThetaSt;
	vector <double> limbin_PhiSt;
	
	int nBinsQ2=5;
	int nBinsM2Pions=36;
	int nBinsAbsCThetaSt=10;
	int nBinsPhiSt=9;
	
	limbin_Q2.resize(nBinsQ2+1);
	limbin_M2Pions.resize(nBinsM2Pions+1);
	limbin_AbsCThetaSt.resize(nBinsAbsCThetaSt+1);
	limbin_PhiSt.resize(nBinsPhiSt+1);
	
	limbin_Q2.front()=0.1; limbin_Q2[1]=0.3;limbin_Q2[2]=0.5;limbin_Q2[3]=0.7;limbin_Q2[4]=1.;
	limbin_Q2.back()=3.;
	
	limbin_M2Pions.front()=2*MPi0; limbin_M2Pions.back()=3.;
	limbin_AbsCThetaSt.front()=0.;limbin_AbsCThetaSt.back()=1.;
	
	limbin_PhiSt.front()=0;limbin_PhiSt.back()=(TMath::Pi());
	
	
	for(int k=1;k<limbin_M2Pions.size();k++) limbin_M2Pions[k]=limbin_M2Pions[k-1]+(limbin_M2Pions.back()-limbin_M2Pions.front())/nBinsM2Pions;
	for(int k=1;k<limbin_AbsCThetaSt.size();k++) limbin_AbsCThetaSt[k]=limbin_AbsCThetaSt[k-1]+(limbin_AbsCThetaSt.back()-limbin_AbsCThetaSt.front())/nBinsAbsCThetaSt;
	for(int k=1;k<limbin_PhiSt.size();k++) limbin_PhiSt[k]=limbin_PhiSt[k-1]+(limbin_PhiSt.back()-limbin_PhiSt.front())/nBinsPhiSt;
	
	char outfilename[256];
	
	sprintf(outfilename,"%s/limbin_Q2.dat",outpath.c_str());
	writeTable(limbin_Q2, outfilename);
	
	sprintf(outfilename,"%s/limbin_M2Pions.dat",outpath.c_str());
	writeTable(limbin_M2Pions, outfilename);
	
	sprintf(outfilename,"%s/limbin_AbsCThetaSt.dat",outpath.c_str());
	writeTable(limbin_AbsCThetaSt, outfilename);
	
	sprintf(outfilename,"%s/limbin_PhiSt.dat",outpath.c_str());
	writeTable(limbin_PhiSt, outfilename);
	}
	
	if(option.find("fracQ2fracW_32") != string::npos){
		vector <double> limbin_fracW;
		vector <double> limbin_fracQ2;
		
		int nBinsfracW=32;
		int nBinsfracQ2=32;
		
		limbin_fracW.resize(nBinsfracW+1);
		limbin_fracQ2.resize(nBinsfracQ2+1);
		
		limbin_fracW.front()=0.; limbin_fracW.back()=1.;
		limbin_fracQ2.front()=0.; limbin_fracQ2.back()=1.;
		
		for(int k=1;k<limbin_fracW.size();k++) limbin_fracW[k]=limbin_fracW[k-1]+(limbin_fracW.back()-limbin_fracW.front())/nBinsfracW;
		for(int k=1;k<limbin_fracQ2.size();k++) limbin_fracQ2[k]=limbin_fracQ2[k-1]+(limbin_fracQ2.back()-limbin_fracQ2.front())/nBinsfracQ2;
		
		char outfilename[256];
	
		sprintf(outfilename,"%s/limbin_fracW.dat",outpath.c_str());
		writeTable(limbin_fracW, outfilename);

		sprintf(outfilename,"%s/limbin_fracQ2.dat",outpath.c_str());
		writeTable(limbin_fracQ2, outfilename);
	
	}
}
