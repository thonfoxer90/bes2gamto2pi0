#include "GaudiKernel/DeclareFactoryEntries.h"
#include "Bes2Gamto2Pi0/Bes2Gamto2Pi0.h"

DECLARE_ALGORITHM_FACTORY( Bes2Gamto2Pi0 )
DECLARE_FACTORY_ENTRIES( Bes2Gamto2Pi0 ) {
  DECLARE_ALGORITHM( Bes2Gamto2Pi0 );
 }
