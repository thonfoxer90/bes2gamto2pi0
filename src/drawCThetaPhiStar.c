using namespace std;

double phiminPitominPi(double phi){
	double newphi;
	if(phi>TMath::Pi()) newphi=phi-2*TMath::Pi();
	else newphi=phi;
	return newphi;
}

void drawCThetaPhiStar(string inputname, string treename,string option, string outpath){
	TFile* file = TFile::Open(inputname.c_str());
	TTree* tree = (TTree*) file->Get(treename.c_str());
	string outps=outpath+".ps";
	string outpdf=outpath+".pdf";
	double PhiStarPi01,PhiStarPi02;
	double CThetaStarPi01,CThetaStarPi02;
	if(treename=="AllMCTruth"){
		tree->SetBranchAddress("eTPhiStarPi01GEN",&PhiStarPi01);
		tree->SetBranchAddress("eTPhiStarPi02GEN",&PhiStarPi02);
		tree->SetBranchAddress("eTcosThetaStarPi01GEN",&CThetaStarPi01);
		tree->SetBranchAddress("eTcosThetaStarPi02GEN",&CThetaStarPi02);
	}
	else{
		tree->SetBranchAddress("cosThetaStarPi01",&CThetaStarPi01);
		tree->SetBranchAddress("cosThetaStarPi02",&CThetaStarPi02);
		tree->SetBranchAddress("PhiStarPi01",&PhiStarPi01);
		tree->SetBranchAddress("PhiStarPi02",&PhiStarPi02);
	}
	
	double phimin,phimax;
	double cthetamin,cthetamax;
	cthetamin=-1;cthetamax=1;
	if(option.find("-Pito+Pi")!=string::npos){
		phimin=-TMath::Pi();
		phimax=TMath::Pi();
	}
	else{phimin=0;phimax=2*TMath::Pi();}
	
	TH1D* hCThetaStarPi01 = new TH1D("hCThetaStarPi01",";cos #theta*_{#pi^{0}_{1}}",256,cthetamin,cthetamax);
	TH1D* hCThetaStarPi02 = new TH1D("hCThetaStarPi02",";cos #theta*_{#pi^{0}_{2}}",256,cthetamin,cthetamax);
	TH1D* hCThetaStarPi01minAbsCThetaStarPi02 = new TH1D("hCThetaStarPi01minAbsCThetaStarPi02",";|cos #theta*_{#pi^{0}_{1}}|-|cos #theta*_{#pi^{0}_{2}}|",256,-1,1);
	
	TH2D* hCThetaStarPi02CThetaStarPi01 = new TH2D("hCThetaStarPi02CThetaStarPi01",";cos #theta*_{#pi^{0}_{1}};cos #theta*_{#pi^{0}_{2}}",256,cthetamin,cthetamax,256,cthetamin,cthetamax);
	
	TH1D* hPhiStarPi01 = new TH1D("hPhiStarPi01",";#phi*_{#pi^{0}_{1}}",256,phimin,phimax);	
	TH1D* hPhiStarPi02 = new TH1D("hPhiStarPi02",";#phi*_{#pi^{0}_{2}}",256,phimin,phimax);
	TH1D* hAbsPhiStarPi01minAbsPhiStarPi02 = new TH1D("hAbsPhiStarPi01minAbsPhiStarPi02",";|#phi*_{#pi^{0}_{1}}|-|#phi*_{#pi^{0}_{2}}|",256,phimin,phimax);
	TH1D* hPhiStarPi01minPhiStarPi02 = new TH1D("hPhiStarPi01minPhiStarPi02",";#phi*_{#pi^{0}_{1}}-#phi*_{#pi^{0}_{2}}",256,-2*TMath::Pi(),2*TMath::Pi());
	TH2D* hPhiStarPi02PhiStarPi01 = new TH2D("hPhiStarPi02PhiStarPi01",";#phi*_{#pi^{0}_{1}};#phi*_{#pi^{0}_{2}}",256,phimin,phimax,256,phimin,phimax);
	
	TH2D* hPhiStarPi01AbsCThetaStarPi01 = new TH2D("hPhiStarPi01AbsCThetaStarPi01",";|cos #theta*_{#pi^{0}_{1}}|;#phi*_{#pi^{0}_{1}}",256,0,cthetamax,256,phimin,phimax);
	TH2D* hPhiStarPi02AbsCThetaStarPi01 = new TH2D("hPhiStarPi02AbsCThetaStarPi01",";|cos #theta*_{#pi^{0}_{1}}|;#phi*_{#pi^{0}_{2}}",256,0,cthetamax,256,phimin,phimax);
	
	//for(int entry=0;entry<tree->GetEntries();entry++){
	for(int entry=0;entry<100000;entry++){
		tree->GetEntry(entry);
		
		double phi1,phi2;
		if(option.find("-Pito+Pi")!=string::npos){
			phi1 = phiminPitominPi(PhiStarPi01);
			phi2 = phiminPitominPi(PhiStarPi02);
		}
		else{
			phi1=PhiStarPi01;
			phi2 = PhiStarPi02;
		}
		hCThetaStarPi01->Fill(CThetaStarPi01);
		hCThetaStarPi02->Fill(CThetaStarPi02);
		hCThetaStarPi02CThetaStarPi01->Fill(CThetaStarPi01,CThetaStarPi02);
		hCThetaStarPi01minAbsCThetaStarPi02->Fill(fabs(CThetaStarPi01)-fabs(CThetaStarPi02));
		
		hPhiStarPi01->Fill(phi1);
		hPhiStarPi02->Fill(phi2);		
		hPhiStarPi02PhiStarPi01->Fill(phi1,phi2);
		hPhiStarPi01minPhiStarPi02->Fill(phi1-phi2);
		hAbsPhiStarPi01minAbsPhiStarPi02->Fill(fabs(phi1)-fabs(phi2));
		
		hPhiStarPi01AbsCThetaStarPi01->Fill(fabs(CThetaStarPi01),phi1);
		hPhiStarPi02AbsCThetaStarPi01->Fill(fabs(CThetaStarPi01),phi2);
	}
	
	gStyle->SetOptStat(0);
	
	TCanvas* c = new TCanvas();
	c->Print(Form("%s[",outps.c_str()));
	hCThetaStarPi01->Draw();
	c->Print(Form("%s",outps.c_str()));
	hCThetaStarPi02->Draw();
	c->Print(Form("%s",outps.c_str()));
	hCThetaStarPi02CThetaStarPi01->Draw();
	c->Print(Form("%s",outps.c_str()));
	hCThetaStarPi01minAbsCThetaStarPi02->Draw();
	c->Print(Form("%s",outps.c_str()));
	hPhiStarPi01->Draw();
	c->Print(Form("%s",outps.c_str()));
	hPhiStarPi02->Draw();
	c->Print(Form("%s",outps.c_str()));	
	hPhiStarPi02PhiStarPi01->Draw();
	c->Print(Form("%s",outps.c_str()));
	hPhiStarPi01minPhiStarPi02->Draw();	
	c->Print(Form("%s",outps.c_str()));
	hAbsPhiStarPi01minAbsPhiStarPi02->Draw();	
	c->Print(Form("%s",outps.c_str()));
	
	c->SetLogz();
	hPhiStarPi01AbsCThetaStarPi01->Draw("colz");	
	c->Print(Form("%s",outps.c_str()));
	c->SetLogz();
	hPhiStarPi02AbsCThetaStarPi01->Draw("colz");	
	c->Print(Form("%s",outps.c_str()));
	
	c->Print(Form("%s]",outps.c_str()));
	
	char systcmd[256];
	sprintf(systcmd,"ps2pdf %s %s",outps.c_str(), outpdf.c_str());
	system(systcmd);
	sprintf(systcmd,"rm %s",outps.c_str());
	system(systcmd);
}