#include <TFile.h>
#include <TDirectory.h>
#include <TTree.h>
#include <TChain.h>
#include <TCanvas.h>
#include <TPad.h>
#include <TH1.h>
#include <TH2.h>
#include <THStack.h>
#include <TLine.h>
#include <stdio.h>
#include <string.h>
#include <iostream> //std::cout
#include <sstream>
#include <fstream> //std::ifstream,ofstream
#include <vector>
#include <algorithm> //std::sort
#include <TKey.h>
#include <TList.h>
#include <TStyle.h>
#include <TROOT.h>
#include <TLegend.h>
#include <TLegendEntry.h>

#include "MyGlobal.cxx"

using namespace std;

vector <string> m_files;
vector <string> m_tags;
vector < vector <double > > m_yields;

int getNumberOfSubDir(string inputfile,string dir, string keyword){
int nDirs=0;

TFile* file = TFile::Open(inputfile.c_str());

TDirectory* directory= file->GetDirectory(dir.c_str());

TIter nextkey(directory->GetListOfKeys());
TKey *key;
while ((key = (TKey*)nextkey())) {
      TObject *obj = (TObject*)key->ReadObj();
			string objname(obj->GetName());
			if (obj->InheritsFrom("TDirectory")){
				if(objname.find(keyword)!=string::npos){
					nDirs++;
				}
			}	
		}

return nDirs;		
}

void makeYieldTable(string inputcard, string option, string outpath){

string dir=option;

ifstream readList(inputcard.c_str());
if(readList.is_open()){
	while(readList.good()){
		string strFile;
		getline(readList,strFile);
		string strTag;
		getline(readList,strTag);
		
		TFile* file = TFile::Open(strFile.c_str());
		if(file==NULL) continue;
		int nStages;
		if(dir=="MCTruth") nStages=1;
		else nStages=getNumberOfSubDir(strFile,dir, "Stage");
		vector<double> vecStages;
		vecStages.resize(nStages);
		for(int k=0;k<vecStages.size();k++){
			string path;
			if(dir=="MCTruth") path=dir;
			else path=dir+"/Stage"+MyGlobal::intToStr(k);
			file->cd(path.c_str());
			
			//TH1D* hNumEvts = (TH1D*) gDirectory->Get("NumEvt");
			TH1D* hNumEvts = (TH1D*) gDirectory->Get("NumEvts");
			if(hNumEvts==NULL ) {vecStages[k] =0;cout << "Histo not found"<<endl;}
			else vecStages[k] = (double) hNumEvts->Integral();
		}
		m_files.push_back(strFile);		
		m_tags.push_back(strTag);
		m_yields.push_back(vecStages);
	}
}

//Write to output file
string outname=outpath+".txt";
ofstream writeTable(outname.c_str());

//Write header
string strHeader("	");
for(int k=0;k<m_tags.size();k++) {
	if(k==m_tags.size()-1) strHeader=strHeader+m_tags[k];
	else strHeader=strHeader+m_tags[k]+"	";
}
writeTable << strHeader<<endl;
for(int i=0;i<m_yields[0].size();i++){
	string strline;
	strline = "Stage"+MyGlobal::intToStr(i)+" ";
	for(int j=0;j<m_yields.size();j++){
		strline=strline+MyGlobal::doubleToStr(m_yields[j][i])+" ";
	}
writeTable << strline <<endl;
}

}