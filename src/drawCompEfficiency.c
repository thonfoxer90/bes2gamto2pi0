#include <TROOT.h>
#include <TChain.h>
#include <TRint.h>
#include <TTree.h>
#include <TFile.h>
#include <TStyle.h>
#include <TString.h>
#include <TSelector.h>
#include <TF1.h>
#include <TH2.h>
#include <TH1.h>
#include <THnSparse.h>
#include <TVirtualIndex.h>
#include <iostream>             // std::cout, std::endl
#include <fstream>              // std::ifstream
#include <sstream>  
#include <TPad.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TGaxis.h>
#include <TPave.h>
#include <TPaveText.h>
#include <TLatex.h>
#include <TVector3.h>
#include <TLorentzVector.h>
#include <TSystem.h>
#include <cstdlib>
#include <map>
#include <time.h>

using namespace std;

void drawCompEfficiency(string inputfile, string option, string outpath){

gStyle->SetOptStat(0);

TFile* file = TFile::Open(inputfile.c_str());

string outps=outpath+".ps";
string outpdf=outpath+".pdf";

TCanvas *c = new TCanvas();
c->Divide(3,1);

for(int k=1;k<=3;k++){
	c->cd(k);
	
	std::vector< TH1D* > vechist; //0 : eT 1: pT 2:eT/pT
	vechist.resize(3);

	if(k==1) {vechist[0]=(TH1D*)file->Get("eTEffQ2");vechist[1]=(TH1D*)file->Get("pTEffQ2");vechist[2]=(TH1D*)file->Get("EffeTOverEffpTQ2");}
	if(k==2) {vechist[0]=(TH1D*)file->Get("eTEffAbsCThetaSt");vechist[1]=(TH1D*)file->Get("pTEffAbsCThetaSt");vechist[2]=(TH1D*)file->Get("EffeTOverEffpTAbsCThetaSt");}
	if(k==3) {vechist[0]=(TH1D*)file->Get("eTEffM2Pi");vechist[1]=(TH1D*)file->Get("pTEffM2Pi");vechist[2]=(TH1D*)file->Get("EffeTOverEffpTM2Pi");}
	
	TPad* pads[2];
	pads[0] = new TPad(Form("upperPad_%d_%d",k,1), "upperPad", 
			       .005, .30, .99, .90);
	pads[0]->SetBottomMargin(0.);
  pads[1] = new TPad(Form("lowerPad_%d_%d",k,2), "lowerPad", 
			       .005, .005, .99, .30);
	pads[1]->SetTopMargin(0.);	 	 
	pads[0]->Draw();
	pads[1]->Draw();
	
	pads[0]->cd();	
	vechist[0]->GetYaxis()->SetTitle("#varepsilon_{x-Tag}");
	vechist[0]->Draw("E");
	vechist[1]->SetLineColor(2);
	vechist[1]->Draw("E SAME");
	if(k==1){
		TLegend* leg = new TLegend(0.70,0.20,0.90,0.30);
		leg->SetFillColor(0);
		leg->AddEntry(vechist[0],"e-Tag");
		leg->AddEntry(vechist[1],"p-Tag");
		leg->Draw();
	}
	pads[1]->cd();
	vechist[2]->SetMinimum(0);
	vechist[2]->Draw("E");
	TLine* lRatio= new TLine(vechist[2]->GetXaxis()->GetXmin(),1.,vechist[2]->GetXaxis()->GetXmax(),1.);
	lRatio->SetLineColor(4);
	lRatio->Draw();
	}
	
c->Print(Form("%s",outps.c_str()));

char systcmd[256];
sprintf(systcmd,"ps2pdf %s %s",outps.c_str(), outpdf.c_str());
system(systcmd);
sprintf(systcmd,"rm %s",outps.c_str());
system(systcmd);

}