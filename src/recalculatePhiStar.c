#include <iostream>             // std::cout, std::endl
#include <fstream>              // std::ifstream
#include <sstream>
#include <cstdlib>
#include <map>
#include <time.h>
#include <string>
#include <vector>

#include <TROOT.h>
#include <TSystem.h>
#include <TObject.h>
#include <TRint.h>
#include <TString.h>
#include <TTree.h>
#include <TChain.h>
#include <TFile.h>
#include <TKey.h>
#include <TList.h>
#include <TStyle.h>
#include <TSelector.h>
#include <TF1.h>
#include <TH2.h>
#include <TH1.h>
#include <THnSparse.h>
#include <TGraph2D.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TMultiGraph.h>
#include <TVirtualIndex.h>
#include <TPad.h>
#include <TCanvas.h>
#include <TGaxis.h>
#include <TPave.h>
#include <TPaveText.h>
#include <TLatex.h>
#include <TVector3.h>
#include <TLorentzVector.h>

using namespace std;

double phi0to2Pi(double phi){
	double newphi;
	if(phi<0.) newphi=2*TMath::Pi()+phi;
	else newphi=phi;
	return newphi;
}

TLorentzVector LVPart(double P,double Theta,double Phi,double E){
	TLorentzVector lv;
	TVector3 v3;
	v3.SetMagThetaPhi(P,Theta,Phi);
	lv.SetPxPyPzE(v3.Px(),v3.Py(),v3.Pz(),E);	
	return lv;	
}

TLorentzVector LVGamma(double E,double Theta,double Phi){
	TLorentzVector lv;
	TVector3 v3;
	v3.SetMagThetaPhi(E,Theta,Phi);
	lv.SetPxPyPzE(v3.Px(),v3.Py(),v3.Pz(),E);	
	return lv;	
}

std::vector<TLorentzVector> MakeBoostedVecLV(std::vector<TLorentzVector> iniveclv, TVector3 boostvec)
{
 std::vector<TLorentzVector> veclv;
 for(int k=0;k<iniveclv.size();k++){
	veclv.push_back(iniveclv[k]);
	(veclv.back()).Boost(-boostvec);
	} 
	
return veclv;  
}

std::vector<double> CThetaPhiStarCM2Gam(std::vector<TLorentzVector> veclvCM2Gam, TLorentzVector lvPi0CM2Gam)
{
		
    vector<double> vecAngles;
		vecAngles.resize(2);
		
		TVector3 ZAxisGam2Pi0CM;
		TVector3 YAxisGam2Pi0CM;
		TVector3 XAxisGam2Pi0CM;
		double ThetaPi0CM2Gam;
		double PhiPi0CM2Gam;
		TLorentzVector lvLeptonCM2Gam=veclvCM2Gam[2];
		TLorentzVector lvGammaStarCM2Gam=veclvCM2Gam[9];

		ZAxisGam2Pi0CM = (lvGammaStarCM2Gam.Vect()).Unit();
		YAxisGam2Pi0CM = (ZAxisGam2Pi0CM.Cross(lvLeptonCM2Gam.Vect())).Unit();
		XAxisGam2Pi0CM = (YAxisGam2Pi0CM.Cross(ZAxisGam2Pi0CM)).Unit();
		if((lvLeptonCM2Gam.Vect()).Dot(XAxisGam2Pi0CM)<0) cout << "***** Px(e-) <0 IN GG FRAME !!!!!!!! *****" << endl;
		ThetaPi0CM2Gam=lvPi0CM2Gam.Angle(ZAxisGam2Pi0CM);
		if((lvPi0CM2Gam.Vect()).Dot(YAxisGam2Pi0CM)>=0) PhiPi0CM2Gam=lvPi0CM2Gam.Angle(XAxisGam2Pi0CM);
		else PhiPi0CM2Gam=-lvPi0CM2Gam.Angle(XAxisGam2Pi0CM);
		//if((m_lvFitPi0CM2Gam.vect()).Dot(XAxisGam2Pi0CM)<0) cout << "***** m_lvPi01CM2GamFit.dot(XAxisGam2Pi0CM)<0 *****" << endl;

		vecAngles[0]=cos(ThetaPi0CM2Gam);
		vecAngles[1]=phi0to2Pi(PhiPi0CM2Gam);
		
		return vecAngles;
}

void recalculatePhiStar(string inputname, string option, string outpath){
	TFile* file;
	file = TFile::Open(inputname.c_str());
	TTree* tree;
	if(option.find("Tagged")!= string::npos) tree =(TTree*) file->Get("Tagged");
	if(option.find("AllMCTruth")!= string::npos) tree =(TTree*) file->Get("AllMCTruth");
	
	TFile* outfile;
	TTree* outTree;

	const double CrossingAngle= 0.011;
	const double Masselec					 = 511e-6;
  const double MassPi0           = 0.134976;    // PDG 2010
  const double MassPiCharged     = 0.139570;    // PDG 2010
	
	double CMSEnergy;
	
	if(option.find("AllMCTruth")!= string::npos){
		double eTPhiStarPi01GEN,eTPhiStarPi02GEN;
		double eTcosThetaStarPi01GEN,eTcosThetaStarPi02GEN;
		double pTPhiStarPi01GEN,pTPhiStarPi02GEN;
		double pTcosThetaStarPi01GEN,pTcosThetaStarPi02GEN;
		
		const int NMax=256;
		int NpartGEN;
		int IDGEN[NMax];
		double PGEN[NMax], ThetaGEN[NMax], PhiGEN[NMax], EGEN[NMax]
			;
			
		tree->Branch("NpartGEN",&NpartGEN,"NpartGEN/I");
		tree->Branch("IDGEN",IDGEN,"IDGEN[NpartGEN]/I");
		tree->Branch("PGEN",PGEN,"PGEN[NpartGEN]/D");
		tree->Branch("ThetaGEN",ThetaGEN,"ThetaGEN[NpartGEN]/D");
		tree->Branch("PhiGEN",PhiGEN,"PhiGEN[NpartGEN]/D");
		tree->Branch("EGEN",EGEN,"EGEN[NpartGEN]/D");

		
		string outname=outpath+".root";
		TFile* outfile = new TFile(outname.c_str(),"RECREATE");
		TTree* outTree = tree->CloneTree(0);
		
		double outeTPhiStarPi01GEN,outeTPhiStarPi02GEN;
		double outeTcosThetaStarPi01GEN,outeTcosThetaStarPi02GEN;
		double outpTPhiStarPi01GEN,outpTPhiStarPi02GEN;
		double outpTcosThetaStarPi01GEN,outpTcosThetaStarPi02GEN;
		
		outTree->SetBranchAddress("eTPhiStarPi01GEN",&outeTPhiStarPi01GEN);
		outTree->SetBranchAddress("eTPhiStarPi02GEN",&outeTPhiStarPi02GEN);

		outTree->SetBranchAddress("eTcosThetaStarPi01GEN",&outeTcosThetaStarPi01GEN);
		outTree->SetBranchAddress("eTcosThetaStarPi02GEN",&outeTcosThetaStarPi02GEN);

		outTree->SetBranchAddress("pTPhiStarPi01GEN",&outpTPhiStarPi01GEN);
		outTree->SetBranchAddress("pTPhiStarPi02GEN",&outpTPhiStarPi02GEN);

		outTree->SetBranchAddress("pTcosThetaStarPi01GEN",&outpTcosThetaStarPi01GEN);
		outTree->SetBranchAddress("pTcosThetaStarPi02GEN",&outpTcosThetaStarPi02GEN);
		
		for (Long64_t i=0;i<tree->GetEntries(); i++) {
				if(i%1000==0) cout << "#Event " << i << endl;
				/*
				for( McParticleCol::iterator itMC = mcParticles->begin();itMC != mcParticles->end();itMC ++) {
    		
			
					int PID = (*itMC)->particleProperty();
      		HepLorentzVector Particle = (*itMC)->initialFourMomentum();

      		bool primary = (*itMC)->primaryParticle();
      		bool leaf = (*itMC)->leafParticle();
      		bool generator = (*itMC)->decayFromGenerator();
      		bool flight = (*itMC)->decayInFlight();

      		Event::McParticle Mother = (*itMC)->mother();
      		int MPID = Mother.particleProperty();

					if(m_iAmcNpartGEN <= m_iAmcNpartGEN->range().distance()) {
						m_iAmcPrimaryGEN[m_iAmcNpartGEN]=primary;
						m_iAmcDecayGeneratorGEN[m_iAmcNpartGEN]=generator;
						m_adAmcIDGEN[m_iAmcNpartGEN]=PID;
						m_iAmcMotherGEN[m_iAmcNpartGEN]=MPID;
						m_adAmcPGEN[m_iAmcNpartGEN]=((*itMC)->initialFourMomentum()).mag();
						m_adAmcThetaGEN[m_iAmcNpartGEN]=((*itMC)->initialFourMomentum()).theta();
						m_adAmcPhiGEN[m_iAmcNpartGEN]=MyGlobal::phi0to2Pi(((*itMC)->initialFourMomentum()).phi());
						m_adAmcEGEN[m_iAmcNpartGEN]=((*itMC)->initialFourMomentum()).e();
						m_iAmcNpartGEN++;
      			}

					switch(PID) {
						case 11: //electron
							m_iMCel++; 
							//With KKMC Generator fed into ManualGenerator,
							//Final state particles (e+e-pi0pi0) are produced from Psipp(PID:30443)
							//These particles are selected by requiring they come from Psipp decay.
							// 2 electrons are contained in MC files:
							// 1-incoming electron beam (Mother ID=11)
							// 2- final state electron (Mother ID=30443)
							if(MPID==30443) 
								{m_iMCelprimary++;
								m_lvmcel = (*itMC)->initialFourMomentum(); 
								}
							break;
						case -11: //positron
							m_iMCpositron++; 
							if(MPID==30443) 
								{m_iMCpositronprimary++;
								m_lvmcpositron = (*itMC)->initialFourMomentum(); 
								}
							break;
    				case 111:// pi0
							m_iMCpi0 ++;      
      				switch(iPin) {
      				case 0: {
								iPin ++;// bFirstP0G = true;
          			m_lvmcPi01       = (*itMC)->initialFourMomentum();

								const SmartRefVector< McParticle > & daughters= (*itMC)->daughterList();											
								for(int k=0;k<daughters.size();k++){
									if(daughters.size()!=2) break;
									if((daughters[k].data())->particleProperty()==22){
										if(k==0) m_lvmcPi01G1=(daughters[k].data())->initialFourMomentum();
										if(k==1) m_lvmcPi01G2=(daughters[k].data())->initialFourMomentum();
									}						
								}
							//printf("%d %f %f %f %f \n", daughters.size(),m_lvmcPi01G1.px(),m_lvmcPi01G1.py(),m_lvmcPi01G1.pz(),m_lvmcPi01G1.e());	
							//printf("%d %f %f %f %f \n", daughters.size(),m_lvmcPi01G2.px(),m_lvmcPi01G2.py(),m_lvmcPi01G2.pz(),m_lvmcPi01G2.e());
							}
        				break;
      				case 1: {
								iPin ++;// bFirstP0G = true;
          			m_lvmcPi02       = (*itMC)->initialFourMomentum();

								const SmartRefVector< McParticle > & daughters= (*itMC)->daughterList();											
								for(int k=0;k<daughters.size();k++){
									if(daughters.size()!=2) break;
									if((daughters[k].data())->particleProperty()==22){
										if(k==0) m_lvmcPi02G1=(daughters[k].data())->initialFourMomentum();
										if(k==1) m_lvmcPi02G2=(daughters[k].data())->initialFourMomentum();
									}						
								}

							}
        				break;
      				case 2: 
								m_lvmcPi03 = (*itMC)->initialFourMomentum(); 
								break;
      				}
						case   22: // gamma 
						if ((*itMC)->mother().particleProperty() != 111) {m_iMCOtherGamma ++;}    // all gammas except from pi0 decay
						else{


						}
      			break;

						case 9010221:

						default:
							// count only final state particles except from pi0 decays
								if ((*itMC)->daughterList().size()                     == 0   // does not decay -> final state particle
										&& (*itMC)->mother().particleProperty()               != 111 // from pi0  decay
         				) {
          				m_iMCOther ++;
      				}
      				break;

    				}

  				}
			
				bool Isee2Pi0=(m_sMCGenerator=="Galuga" && m_iMCpi0 == 2 && m_iMCelprimary==1 && m_iMCpositronprimary ==1);
				
  			if (Isee2Pi0) {

				map <string, HepLorentzVector> maplv;
				maplv["Beame-"]=m_lvBeamElectron;
				maplv["Beame+"]=m_lvBeamPositron;			
				maplv["Scate-"]=m_lvmcel;
				maplv["Scate+"]=m_lvmcpositron;
				maplv["Pi01"]=m_lvmcPi01;
				maplv["Pi01G1"]=m_lvmcPi01G1;
				maplv["Pi01G2"]=m_lvmcPi01G2;
				maplv["Pi02"]=m_lvmcPi02;
				maplv["Pi02G1"]=m_lvmcPi02G1;
				maplv["Pi02G2"]=m_lvmcPi02G2;

				map <string, HepLorentzVector> maplvCM = MakeBoostedMapLV(maplv,m_vectboostCM);
				HepLorentzVector m_lvmcelCM=maplvCM["Scate-"];
				HepLorentzVector m_lvmcpositronCM=maplvCM["Scate+"];
				HepLorentzVector m_lvmcPi01CM=maplvCM["Pi01"];
				HepLorentzVector m_lvmcPi02CM=maplvCM["Pi02"];

				//2 pi invariant mass
				m_dMC2Pi0InvM=(m_lvmcPi01+m_lvmcPi02).m();
				m_dAmc2Pi0InvMGEN=m_dMC2Pi0InvM;

				//Q2
				HepLorentzVector lvGammaStarElectron = m_lvBeamElectron-m_lvmcel;
				//m_adAmcQ2GEN[0]=-(lvGammaStarElectron).m2();
				//m_adQ2GEN[0]=m_adAmcQ2GEN[0];
				m_adQ2GEN[0]=-(lvGammaStarElectron).m2();
				m_adAmcQ2GEN[0]=m_adQ2GEN[0];
				HepLorentzVector lvGammaStarPositron = m_lvBeamPositron-m_lvmcpositron;
				//m_adAmcQ2GEN[1]=-(lvGammaStarPositron).m2();
				//m_adQ2GEN[1]=m_adAmcQ2GEN[1];
				m_adQ2GEN[1]=-(lvGammaStarPositron).m2();
				m_adAmcQ2GEN[1]=m_adQ2GEN[1];

				//Transverse balance quantities
				m_dUSumPtCMGEN=GetPt(m_lvmcPi01CM+m_lvmcPi02CM);
				m_adTSumPtCMGEN[0]=GetPt(m_lvmcelCM+m_lvmcPi01CM+m_lvmcPi02CM);
				m_adTSumPtCMGEN[1]=GetPt(m_lvmcpositronCM+m_lvmcPi01CM+m_lvmcPi02CM);		

				m_dAmcUSumPt2Pi0GEN=m_dUSumPtCMGEN;			
				m_dAmcTSumPt2Pi0GEN[0]=m_adTSumPtCMGEN[0];
				m_dAmcTSumPt2Pi0GEN[1]=m_adTSumPtCMGEN[1];

				//RGamma
				m_dAmcTRGamGEN[0]=RGam(maplvCM,m_lvmcel);
				m_dAmcTRGamGEN[1]=RGam(maplvCM,m_lvmcpositron);

				//2 Gamma CM frame
				Hep3Vector boost2Pi0=(m_lvmcPi01+m_lvmcPi02).boostVector();
				map <string, HepLorentzVector> maplvCM2Gam = MakeBoostedMapLV(maplv,boost2Pi0);

				vector<double> vecCThetaPhiStar;

				for(int i=0;i<2;i++){
					string lept;
					string pi;
					if(i==0) lept="e-";
					else if (i==1) lept="e+";
					for(int j=0;j<2;j++){
						if(j==0) pi="Pi01";
						else if(j==1) pi="Pi02";
						vecCThetaPhiStar=CThetaPhiStarCM2Gam(maplvCM2Gam,lept,pi);
						m_adTcosThetaStarPi0GEN[i][j]=vecCThetaPhiStar[0];
						m_adTPhiStarPi0GEN[i][j]=vecCThetaPhiStar[1];

						//if(i==0)m_adTcosThetaStarPi0GEN[i][j]=cos(maplvCM2Gam["Pi01"].theta());
						//else if(i==1) 
						//m_adTPhiStarPi0GEN[i][j]=vecCThetaPhiStar[1];

						m_adAmcTcosThetaStarGEN[i][j]=m_adTcosThetaStarPi0GEN[i][j];
						m_adAmcTPhiStarGEN[i][j]=m_adTPhiStarPi0GEN[i][j];
					}				
				}
			
  		}
			*/
		}
	}
	
	if(option.find("Tagged")!= string::npos){
		
		int QTag;
		double LeptonE,LeptonP,LeptonTheta,LeptonPhi;
		double Pi01E,Pi01P,Pi01Theta,Pi01Phi;
		double Pi02E,Pi02P,Pi02Theta,Pi02Phi;
		double G0emcE,G0emcTheta, G0emcPhi;
		double G1emcE,G1emcTheta, G1emcPhi;
		double G2emcE,G2emcTheta, G2emcPhi;
		double G3emcE,G3emcTheta, G3emcPhi;
		double cosThetaStarPi01,cosThetaStarPi02;
		double PhiStarPi01,PhiStarPi02;

		double LeptonFitE,LeptonFitP,LeptonFitTheta,LeptonFitPhi;
		double Pi01FitE,Pi01FitP,Pi01FitTheta,Pi01FitPhi;
		double Pi02FitE,Pi02FitP,Pi02FitTheta,Pi02FitPhi;
		double GFit0E,GFit0Theta, GFit0Phi;
		double GFit1E,GFit1Theta, GFit1Phi;
		double GFit2E,GFit2Theta, GFit2Phi;
		double GFit3E,GFit3Theta, GFit3Phi;

		double cosThetaStarPi0Fit1,cosThetaStarPi0Fit2;
		double PhiStarPi0Fit1,PhiStarPi0Fit2;
	
		tree->SetBranchAddress("CMSEnergy",&CMSEnergy);
		tree->SetBranchAddress("QTag",&QTag);
		
		tree->SetBranchAddress("LeptonE",&LeptonE);
		tree->SetBranchAddress("LeptonP",&LeptonP);
		tree->SetBranchAddress("LeptonTheta",&LeptonTheta);
		tree->SetBranchAddress("LeptonPhi",&LeptonPhi);
		
		tree->SetBranchAddress("Pi01E",&Pi01E);
		tree->SetBranchAddress("Pi01P",&Pi01P);
		tree->SetBranchAddress("Pi01Theta",&Pi01Theta);
		tree->SetBranchAddress("Pi01Phi",&Pi01Phi);
		
		tree->SetBranchAddress("Pi02E",&Pi02E);
		tree->SetBranchAddress("Pi02P",&Pi02P);
		tree->SetBranchAddress("Pi02Theta",&Pi02Theta);
		tree->SetBranchAddress("Pi02Phi",&Pi02Phi);
		
		tree->SetBranchAddress("G0emcE",&G0emcE);
		tree->SetBranchAddress("G0emcTheta",&G0emcTheta);
		tree->SetBranchAddress("G0emcPhi",&G0emcPhi);
		
		tree->SetBranchAddress("G1emcE",&G1emcE);
		tree->SetBranchAddress("G1emcTheta",&G1emcTheta);
		tree->SetBranchAddress("G1emcPhi",&G1emcPhi);
		
		tree->SetBranchAddress("G2emcE",&G2emcE);
		tree->SetBranchAddress("G2emcTheta",&G2emcTheta);
		tree->SetBranchAddress("G2emcPhi",&G2emcPhi);
		
		tree->SetBranchAddress("G3emcE",&G3emcE);
		tree->SetBranchAddress("G3emcTheta",&G3emcTheta);
		tree->SetBranchAddress("G3emcPhi",&G3emcPhi);
		
		tree->SetBranchAddress("PhiStarPi01",&PhiStarPi01);
		tree->SetBranchAddress("PhiStarPi02",&PhiStarPi02);
		
		tree->SetBranchAddress("cosThetaStarPi01",&cosThetaStarPi01);
		tree->SetBranchAddress("cosThetaStarPi02",&cosThetaStarPi02);
		
		
		tree->SetBranchAddress("LeptonFitE",&LeptonFitE);
		tree->SetBranchAddress("LeptonFitP",&LeptonFitP);
		tree->SetBranchAddress("LeptonFitTheta",&LeptonFitTheta);
		tree->SetBranchAddress("LeptonFitPhi",&LeptonFitPhi);
		
		tree->SetBranchAddress("Pi01FitE",&Pi01FitE);
		tree->SetBranchAddress("Pi01FitP",&Pi01FitP);
		tree->SetBranchAddress("Pi01FitTheta",&Pi01FitTheta);
		tree->SetBranchAddress("Pi01FitPhi",&Pi01FitPhi);
		
		tree->SetBranchAddress("Pi02FitE",&Pi02FitE);
		tree->SetBranchAddress("Pi02FitP",&Pi02FitP);
		tree->SetBranchAddress("Pi02FitTheta",&Pi02FitTheta);
		tree->SetBranchAddress("Pi02FitPhi",&Pi02FitPhi);
		
		tree->SetBranchAddress("GFit0E",&GFit0E);
		tree->SetBranchAddress("GFit0Theta",&GFit0Theta);
		tree->SetBranchAddress("GFit0Phi",&GFit0Phi);
		
		tree->SetBranchAddress("GFit1E",&GFit1E);
		tree->SetBranchAddress("GFit1Theta",&GFit1Theta);
		tree->SetBranchAddress("GFit1Phi",&GFit1Phi);
		
		tree->SetBranchAddress("GFit2E",&GFit2E);
		tree->SetBranchAddress("GFit2Theta",&GFit2Theta);
		tree->SetBranchAddress("GFit2Phi",&GFit2Phi);
		
		tree->SetBranchAddress("GFit3E",&GFit3E);
		tree->SetBranchAddress("GFit3Theta",&GFit3Theta);
		tree->SetBranchAddress("GFit3Phi",&GFit3Phi);
		
		tree->SetBranchAddress("PhiStarPi0Fit1",&PhiStarPi0Fit1);
		tree->SetBranchAddress("PhiStarPi0Fit2",&PhiStarPi0Fit2);
		
		tree->SetBranchAddress("cosThetaStarPi0Fit1",&cosThetaStarPi0Fit1);
		tree->SetBranchAddress("cosThetaStarPi0Fit2",&cosThetaStarPi0Fit2);
	
	
		string outname=outpath+".root";
		TFile* outfile = new TFile(outname.c_str(),"RECREATE");
		TTree* outTree = tree->CloneTree(0);
		double outPhiStarPi01,outPhiStarPi02;
		double outcosThetaStarPi01,outcosThetaStarPi02;

		double outPhiStarPi0Fit1,outPhiStarPi0Fit2;
		double outcosThetaStarPi0Fit1,outcosThetaStarPi0Fit2;

		outTree->SetBranchAddress("PhiStarPi01",&outPhiStarPi01);
		outTree->SetBranchAddress("PhiStarPi02",&outPhiStarPi02);

		outTree->SetBranchAddress("cosThetaStarPi01",&outcosThetaStarPi01);
		outTree->SetBranchAddress("cosThetaStarPi02",&outcosThetaStarPi02);

		outTree->SetBranchAddress("PhiStarPi0Fit1",&outPhiStarPi0Fit1);
		outTree->SetBranchAddress("PhiStarPi0Fit2",&outPhiStarPi0Fit2);

		outTree->SetBranchAddress("cosThetaStarPi0Fit1",&outcosThetaStarPi0Fit1);
		outTree->SetBranchAddress("cosThetaStarPi0Fit2",&outcosThetaStarPi0Fit2);

		for (Long64_t i=0;i<tree->GetEntries(); i++) {
				if(i%1000==0) cout << "#Event " << i << endl;
    		tree->GetEntry(i);
				//cout << CMSEnergy << " " << PhiStarPi01<< endl;			
				double Abs3MomBeam = sqrt((pow(CMSEnergy,2)/2-pow(Masselec,2))/(1-cos(TMath::Pi()-CrossingAngle)));
			double TotEngBeam = sqrt(pow(Masselec,2)+pow(Abs3MomBeam,2));

				TLorentzVector lvBeamElectron;
				TLorentzVector lvBeamPositron;

				lvBeamElectron.SetPx(Abs3MomBeam*sin(CrossingAngle));
				lvBeamElectron.SetPy(0);
				lvBeamElectron.SetPz(-Abs3MomBeam*cos(CrossingAngle));
				lvBeamElectron.SetE(TotEngBeam);

				lvBeamPositron.SetPx(Abs3MomBeam*sin(CrossingAngle));
				lvBeamPositron.SetPy(0);
				lvBeamPositron.SetPz(Abs3MomBeam*cos(CrossingAngle));
				lvBeamPositron.SetE(TotEngBeam);

				TLorentzVector lvBoost  = lvBeamElectron+lvBeamPositron;

				TLorentzVector lvLepton;
				lvLepton=LVPart(LeptonP,LeptonTheta,LeptonPhi,LeptonE);

				TLorentzVector lvPi01,lvPi02;
				lvPi01=LVPart(Pi01P,Pi01Theta,Pi01Phi,Pi01E);
				lvPi02=LVPart(Pi02P,Pi02Theta,Pi02Phi,Pi02E);

				TLorentzVector lvPi01G1,lvPi01G2;
				lvPi01G1=LVGamma(G0emcE,G0emcTheta,G0emcPhi);
				lvPi01G2=LVGamma(G1emcE,G1emcTheta,G1emcPhi);
				TLorentzVector lvPi02G1,lvPi02G2;
				lvPi02G1=LVGamma(G2emcE,G2emcTheta,G2emcPhi);
				lvPi02G2=LVGamma(G3emcE,G3emcTheta,G3emcPhi);

				TLorentzVector lvGammaStar;
				if(QTag==-1)	lvGammaStar = lvBeamElectron-lvLepton;
				if(QTag==1) 	lvGammaStar = lvBeamPositron-lvLepton;

				vector <TLorentzVector> veclv;
				veclv.push_back(lvBeamElectron);
				veclv.push_back(lvBeamPositron);
				veclv.push_back(lvLepton);
				veclv.push_back(lvPi01);
				veclv.push_back(lvPi01G1);
				veclv.push_back(lvPi01G2);
				veclv.push_back(lvPi02);
				veclv.push_back(lvPi02G1);
				veclv.push_back(lvPi02G2);
				veclv.push_back(lvGammaStar);

				TVector3 boost2Pi0=(lvPi01+lvPi02).BoostVector();
				vector <TLorentzVector> veclvCM2Gam = MakeBoostedVecLV(veclv,boost2Pi0);
				TLorentzVector lvLeptonCM2Gam=veclvCM2Gam[2];
				TLorentzVector lvPi01CM2Gam=veclvCM2Gam[3];
				TLorentzVector lvPi02CM2Gam=veclvCM2Gam[6];
				TLorentzVector lvGammaStarCM2Gam=veclvCM2Gam[9];

				for(int k=0;k<2;k++){
					vector<double> CThetaPhiStarPi0;
					if(k==0) CThetaPhiStarPi0=CThetaPhiStarCM2Gam(veclvCM2Gam,lvPi01CM2Gam);
					if(k==1) CThetaPhiStarPi0=CThetaPhiStarCM2Gam(veclvCM2Gam,lvPi02CM2Gam);

					if(k==0){
						outcosThetaStarPi01=CThetaPhiStarPi0[0];
						outPhiStarPi01=CThetaPhiStarPi0[1];
					}
					if(k==1){
						outcosThetaStarPi02=CThetaPhiStarPi0[0];
						outPhiStarPi02=CThetaPhiStarPi0[1];
					}


				}	

				//Fitted kinematics 
				TLorentzVector lvLeptonFit;
				lvLeptonFit=LVPart(LeptonFitP,LeptonFitTheta,LeptonFitPhi,LeptonFitE);
				//printf("%.2f %.2f %.2f %.2f \n",LeptonFitP,LeptonFitTheta,LeptonFitPhi,LeptonFitE);
				TLorentzVector lvPi01Fit,lvPi02Fit;
				lvPi01Fit=LVPart(Pi01FitP,Pi01FitTheta,Pi01FitPhi,Pi01FitE);
				lvPi02Fit=LVPart(Pi02FitP,Pi02FitTheta,Pi02FitPhi,Pi02FitE);

				TLorentzVector lvPi01G1Fit,lvPi01G2Fit;
				lvPi01G1Fit=LVGamma(GFit0E,GFit0Theta,GFit0Phi);
				lvPi01G2Fit=LVGamma(GFit1E,GFit1Theta,GFit1Phi);
				TLorentzVector lvPi02G1Fit,lvPi02G2Fit;
				lvPi02G1Fit=LVGamma(GFit2E,GFit2Theta,GFit2Phi);
				lvPi02G2Fit=LVGamma(GFit3E,GFit3Theta,GFit3Phi);

				TLorentzVector lvGammaStarFit;
				if(QTag==-1)	lvGammaStarFit = lvBeamElectron-lvLeptonFit;
				if(QTag==1) 	lvGammaStarFit = lvBeamPositron-lvLeptonFit;

				vector <TLorentzVector> veclvFit;
				veclvFit.push_back(lvBeamElectron);
				veclvFit.push_back(lvBeamPositron);
				veclvFit.push_back(lvLeptonFit);
				veclvFit.push_back(lvPi01Fit);
				veclvFit.push_back(lvPi01G1Fit);
				veclvFit.push_back(lvPi01G2Fit);
				veclvFit.push_back(lvPi02Fit);
				veclvFit.push_back(lvPi02G1Fit);
				veclvFit.push_back(lvPi02G2Fit);
				veclvFit.push_back(lvGammaStarFit);
				//printf("%.2f %.2f %.2f %.2f %.2f %.2f %.2f \n",lvLeptonFit.E(),lvPi01Fit.E(),lvPi01G1Fit.E(),lvPi01G2Fit.E(),lvPi02Fit.E(),lvPi02G1Fit.E(),lvPi02G2Fit.E(),lvGammaStarFit.E());
				//printf("%.2f %.2f %.2f %.2f %.2f %.2f %.2f \n",lvLepton.E(),lvPi01.E(),lvPi01G1.E(),lvPi01G2.E(),lvPi02.E(),lvPi02G1.E(),lvPi02G2.E(),lvGammaStar.E());

				TVector3 boost2Pi0Fit=(lvPi01Fit+lvPi02Fit).BoostVector();
				vector <TLorentzVector> veclvCM2GamFit = MakeBoostedVecLV(veclvFit,boost2Pi0Fit);
				TLorentzVector lvLeptonCM2GamFit=veclvCM2GamFit[2];
				TLorentzVector lvPi01CM2GamFit=veclvCM2GamFit[3];
				TLorentzVector lvPi02CM2GamFit=veclvCM2GamFit[6];
				TLorentzVector lvGammaStarCM2GamFit=veclvCM2GamFit[9];
				//printf("%.2f %.2f %.2f %.2f \n",lvLeptonCM2Gam.E(),lvPi01CM2Gam.E(),lvPi02CM2Gam.E(),lvGammaStarCM2Gam.E());
				//printf("%.2f %.2f %.2f %.2f \n",lvLeptonCM2GamFit.E(),lvPi01CM2GamFit.E(),lvPi02CM2GamFit.E(),lvGammaStarCM2GamFit.E());

				for(int k=0;k<2;k++){
					vector<double> CThetaPhiStarPi0Fit;

					if(k==0) CThetaPhiStarPi0Fit=CThetaPhiStarCM2Gam(veclvCM2GamFit,lvPi01CM2GamFit);
					if(k==1) CThetaPhiStarPi0Fit=CThetaPhiStarCM2Gam(veclvCM2GamFit,lvPi02CM2GamFit);

					if(k==0){
						outcosThetaStarPi0Fit1=CThetaPhiStarPi0Fit[0];
						outPhiStarPi0Fit1=CThetaPhiStarPi0Fit[1];
					}
					if(k==1){
						outcosThetaStarPi0Fit2=CThetaPhiStarPi0Fit[0];
						outPhiStarPi0Fit2=CThetaPhiStarPi0Fit[1];
					}


				}
				//printf("%.2f %.2f \n",outcosThetaStarPi01,outPhiStarPi01);
				//printf("%.2f %.2f \n",outcosThetaStarPi0Fit1,outPhiStarPi0Fit1);
				//printf("\n");
				outTree->Fill();
		 }
		 
		outTree->AutoSave();
   	delete file;
   	delete outfile;
	 }

   
	 
}