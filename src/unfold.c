#if not defined(__CINT__) || defined(__MAKECINT__)
#include "TSVDUnfold.h"
#include "TUnfold.h"
#endif
#include <TROOT.h>
#include <TChain.h>
#include <TRint.h>
#include <TTree.h>
#include <TFile.h>
#include <TStyle.h>
#include <TString.h>
#include <TSelector.h>
#include <TF1.h>
#include <TH2.h>
#include <TH1.h>
#include <THnSparse.h>
#include <TGraph2D.h>
#include <TVirtualIndex.h>
#include <iostream>             // std::cout, std::endl
#include <fstream>              // std::ifstream
#include <sstream>  
#include <TPad.h>
#include <TCanvas.h>
#include <TGaxis.h>
#include <TPave.h>
#include <TPaveText.h>
#include <TLegend.h>
#include <TColor.h>
#include <TLatex.h>
#include <TVector3.h>
#include <TLorentzVector.h>
#include <TSystem.h>
#include "TRandom.h"
#include <cstdlib>
#include <map>
#include <time.h>

#include "MyGlobal.cxx"

using namespace std;

string m_FileNameMC("");
string m_FileNameData("");
string m_NTupleName("");
string m_LimBinPath("");
string m_Algorithm("");
string m_strRegPar;
double m_RegPar;
int m_UseFittedKinematics=1;

void unfold(string inputcard, string outpath){

gStyle->SetOptStat(0);
gStyle->SetOptTitle(0);

ifstream readInput(inputcard.c_str());
if(readInput.is_open()){
	while(readInput.good()){
		string strline;
		getline(readInput,strline);
		/*
		vector<string> strsplit = MyGlobal::tokenize(strline,' ');
		for(int k=0;k<strsplit.size();k++){
			vector<string> vecstr = MyGlobal::tokenize(strsplit[k],'=');	
			string arg = vecstr[0];
			string val = vecstr[1];
			if(arg=="Data") m_FileNameData=val;
			if(arg=="MC") m_FileNameMC=val;
			if(arg=="NTuple") m_NTupleName=val;
			if(arg=="Binning") m_LimBinPath=val;
			if(arg=="Algorithm") m_Algorithm=val;
			if(arg=="RegPar") m_strRegPar=val;
			if(arg=="UseFittedKinematics") m_UseFittedKinematics=atoi(val.c_str());
		}
		*/
		vector<string> vecstr = MyGlobal::tokenize(strline,'=');	
		string arg = vecstr[0];
		string val = vecstr[1];
		if(arg=="Data") m_FileNameData=val;
		if(arg=="MC") m_FileNameMC=val;
		if(arg=="NTuple") m_NTupleName=val;
		if(arg=="Binning") m_LimBinPath=val;
		if(arg=="Algorithm") m_Algorithm=val;
		if(arg=="RegPar") m_strRegPar=val;
		if(arg=="UseFittedKinematics") m_UseFittedKinematics=atoi(val.c_str());
	}
	
}

const Int_t nDim = 1;
	vector<vector<double> > vec_limbins;
	vec_limbins.resize(nDim);
	for(int k=0;k<nDim;k++){
		string limbinFullPath;
		string limbinFileName;
		if(k==0) limbinFileName="limbin_M2Pions.dat";
		limbinFullPath=m_LimBinPath+"/"+limbinFileName;
		vec_limbins[k]=MyGlobal::getLimBin(limbinFullPath);
		//for(int i=0;i<vec_limbins[k].size();i++){
		//	cout << vec_limbins[k][i] << endl;
		//}
	}
	Int_t nBins[nDim];
	Double_t min[nDim];
	Double_t max[nDim];
	
	for(int k=0;k<nDim;k++) {
		nBins[k]=vec_limbins[k].size()-1;
		min[k]=vec_limbins[k][0];
		max[k]=vec_limbins[k][vec_limbins[k].size()-1];
	}

TTree* treeMC;
TTree* treeMCGEN;

if(m_FileNameMC.find(".txt")!=string::npos){
	treeMC = (TTree*) MyGlobal::makeChain(m_FileNameMC.c_str(),m_NTupleName.c_str());
	treeMCGEN = (TTree*) MyGlobal::makeChain(m_FileNameMC.c_str(),"AllMCTruth");
}
else{
	TFile* fileMC = TFile::Open(m_FileNameMC.c_str());
	treeMC = (TTree*) fileMC->Get(m_NTupleName.c_str());
	treeMCGEN = (TTree*) fileMC->Get("AllMCTruth");
}

TTree* treeData;
TTree* treeDataGEN;
if(m_FileNameData.find(".txt")!=string::npos){
	treeData = (TTree*) MyGlobal::makeChain(m_FileNameData.c_str(),m_NTupleName.c_str());
	treeDataGEN = (TTree*) MyGlobal::makeChain(m_FileNameData.c_str(),"AllMCTruth");
}
else{
	TFile* fileData = TFile::Open(m_FileNameData.c_str());
	treeData = (TTree*) fileData->Get(m_NTupleName.c_str());
	treeDataGEN = (TTree*) fileData->Get("AllMCTruth");
}

double WMeas,WTrue;
treeMC->SetBranchAddress("2Pi0InvMGEN", &WTrue);
if(m_UseFittedKinematics=0) treeMC->SetBranchAddress("2Pi0InvM", &WMeas);
else 	treeMC->SetBranchAddress("2Pi0InvMFit", &WMeas);

double WGEN;
treeMCGEN->SetBranchAddress("2Pi0InvMGEN", &WGEN);

double WData,WDataGEN;
treeData->SetBranchAddress("2Pi0InvM", &WData);
treeDataGEN->SetBranchAddress("2Pi0InvMGEN", &WDataGEN);

string outROOTname=outpath+".root";
string outpdf=outpath+".pdf";
TFile* outROOT = new TFile(outROOTname.c_str(),"RECREATE");

TH1D* hTrue= new TH1D ("true", "Test Truth",    vec_limbins[0].size()-1,&vec_limbins[0][0]);
TH1D* hMeas= new TH1D ("meas", "Test Measured", vec_limbins[0].size()-1,&vec_limbins[0][0]);
TH2D* hMeasTruth = new TH2D("hMeasTruth",";Measured;Truth",
vec_limbins[0].size()-1,&vec_limbins[0][0],
vec_limbins[0].size()-1,&vec_limbins[0][0]);

TH2D* hDeltaMeasTrueMeas = new TH2D("hMeasvsDeltaTrueMeas",";meas-true;meas",50,-0.2,0.2,vec_limbins[0].size()-1,vec_limbins[0].front(),vec_limbins[0].back());

TH1D* hData= new TH1D ("data", "Test Measured", vec_limbins[0].size()-1,&vec_limbins[0][0]);
TH1D* hDataTrue= new TH1D ("datatrue", "Test Truth",    vec_limbins[0].size()-1,&vec_limbins[0][0]);
TH2D* hstatcov = new TH2D("statcov", "covariance matrix",
vec_limbins[0].size()-1,&vec_limbins[0][0],
vec_limbins[0].size()-1,&vec_limbins[0][0]);

cout << "Fill distributions" << endl; 
//Fill histograms
for(int entry=0;entry<treeData->GetEntries();entry++){
	treeData->GetEntry(entry);
	hData->Fill(WData);
}

for(int entry=0;entry<treeDataGEN->GetEntries();entry++){
	treeDataGEN->GetEntry(entry);
	hDataTrue->Fill(WDataGEN);
}

// Fill the data covariance matrix
for (int i=1; i<=hData->GetNbinsX(); i++) {
       hstatcov->SetBinContent(i,i,hData->GetBinError(i)*hData->GetBinError(i)); 
}
	 
for(int entry=0;entry<treeMC->GetEntries();entry++){
	treeMC->GetEntry(entry);
	hMeas->Fill(WMeas);
	hMeasTruth->Fill(WMeas,WTrue);
	hDeltaMeasTrueMeas->Fill(WMeas-WTrue,WMeas);
}

for(int entry=0;entry<treeMCGEN->GetEntries();entry++){
	treeMCGEN->GetEntry(entry);
	hTrue->Fill(WGEN);
}

hTrue->Write();
hMeas->Write();
hMeasTruth->Write();
hData->Write();
hDataTrue->Write();

TCanvas* c0 = new TCanvas("c0","");
c0->Divide(2,2);
c0->cd(1);
gPad->SetTitle("Data");
hTrue->SetMinimum(1);
hTrue->Draw("HIST");
hMeas->SetLineColor(2);
hMeas->Draw("HIST same");
gPad->SetLogy();
c0->cd(2);
hDataTrue->SetMinimum(1);
hDataTrue->Draw("HIST");
hData->SetLineColor(2);
hData->Draw("HIST same");
gPad->SetLogy();
c0->cd(3);
hMeasTruth->Draw("colz");

if(m_Algorithm=="SVD"){
	cout << "SVD Unfolding" << endl;   
	
	// Create TSVDUnfold object and initialise
	TSVDUnfold *tsvdunf = new TSVDUnfold( hData, hstatcov, hMeas, hTrue, hMeasTruth );
	
	// It is possible to normalise unfolded spectrum to unit area
   tsvdunf->SetNormalize( kFALSE ); // no normalisation here
		 
	if(m_strRegPar.find("FindBestPar")!=string::npos){
		double RegParMax=(double)hMeas->GetNbinsX();

		// Perform an initial unfolding with regularisation parameter as central value
		// - the larger kreg, the finer grained the unfolding, but the more fluctuations occur
		// - the smaller kreg, the stronger is the regularisation and the bias
		tsvdunf->Unfold(RegParMax/2);

		// Get the distribution of the d to cross check the regularization
		// - choose kreg to be the point where |d_i| stop being statistically significantly >>1
		TH1D* hddist = tsvdunf->GetD();
		hddist->SetName( "hddist" );
		hddist->SetTitle( "TSVDUnfold |d_{i}|" );
		hddist->GetXaxis()->SetTitle( "i" );
		hddist->GetYaxis()->SetTitle( "|d_{i}|" );

		// Get the distribution of the singular values
		TH1D* hsvdist = tsvdunf->GetSV();
		hsvdist->SetName("hsvdist");
		hsvdist->GetXaxis()->SetTitle( "i" );
		hsvdist->GetYaxis()->SetTitle( "s_{i}" );

		TH1D* hsvdist2 = (TH1D*) hsvdist->Clone();
		hsvdist2->Multiply(hsvdist);
		hsvdist2->SetName("hsvdist2");
		hsvdist2->GetXaxis()->SetTitle( "i" );
		hsvdist2->GetYaxis()->SetTitle( "s_{i}^{2}" );
		
		// using the measured covariance matrix as input to generate the toys
   // 100 toys should usually be enough
   // The same method can be used for different covariance matrices separately.
   TH2D* hustatcov = tsvdunf->GetUnfoldCovMatrix( hstatcov, 100 );   

   // Now compute the error matrix on the unfolded distribution originating
   // from the finite detector matrix statistics
   TH2D* huadetcov = tsvdunf->GetAdetCovMatrix( 100 );   

   // Sum up the two (they are uncorrelated)
   hustatcov->Add( huadetcov );

   //Get the computed regularized covariance matrix (always corresponding to total uncertainty passed in constructor) and add uncertainties from finite MC statistics. 
   TH2D* hutaucov = tsvdunf->GetXtau();
   hutaucov->Add( huadetcov );

   //Get the computed inverse of the covariance matrix
   TH2D* huinvcov = tsvdunf->GetXinv();
	 	
		vector<string > strSplit = MyGlobal::tokenize(m_strRegPar,' ');
		if(strSplit[1]=="1"){
			const int nScans = atoi(strSplit[2].c_str());

			TCanvas* c = new TCanvas();
			c->Print(Form("%s[",outpdf.c_str()));
			//for(int k=0;k<nScans;k++){
				//double RegPar=k*RegParMax/nScans;
			for(int k=0;k<hsvdist2->GetNbinsX();k++){	
				double RegPar=hsvdist2->GetBinContent(k+1);
				char hname[256];
				char htitle[256];
				TH1D* hunfres = tsvdunf->Unfold( RegPar );
				sprintf(hname,"hunfres_tau_%.2f",RegPar);
				hunfres->SetName(hname);
				sprintf(htitle,"#tau = %.2f",RegPar);
				hunfres->SetTitle(htitle);
				TH1D* hddisttau = (TH1D*) hddist->Clone();
				hddisttau->Reset(); //Reset bin contents and error
				sprintf(hname,"hddisttau_tau_%.2f",RegPar);
				hddisttau->SetName(hname);
				sprintf(htitle,"|d^{(#tau)}_{i}|, #tau = s^{2}_{%d}=%.2f",k+1,RegPar);
				hddisttau->SetTitle(htitle);
				hddisttau->GetXaxis()->SetTitle("i");
				hddisttau->GetYaxis()->SetTitle("|d^{(#tau)}_{i}}|");
				for(int bin=1;bin<=hddisttau->GetNbinsX();bin++){
					double ddist = hddist->GetBinContent(bin);
					double svdist2 = pow((double)hsvdist->GetBinContent(bin),2);
					double ddisttau = ddist*(svdist2/(svdist2+RegPar));
					hddisttau->SetBinContent(bin,ddisttau);
				}

				hddisttau->Write();
				hunfres->Write();

				hddist->SetFillColor(46);
				hddist->Draw("HIST");
				hddisttau->SetFillColor(4);
				hddisttau->Draw("HIST SAME");
				TLine* leq1 = new TLine(hddist->GetXaxis()->GetXmin(),1,hddist->GetXaxis()->GetXmax(),1);			
				leq1->SetLineStyle(2);
				leq1->Draw();	
				TLine* lk = new TLine(k+1,hddist->GetYaxis()->GetXmin(),k+1,hddist->GetYaxis()->GetXmax());		
				lk->Draw();
				gPad->SetLogy();
				TLegend *leg = new TLegend(0.58,0.68,0.9,0.88);
				leg->SetFillColor(0);
				leg->SetBorderSize(0);
				leg->AddEntry(hddist,"|d_{i}|","l");
				leg->AddEntry(hddisttau,hddisttau->GetTitle(),"l");
				leg->Draw();
				c->Print(Form("%s",outpdf.c_str()));
			}
			c->Print(Form("%s]",outpdf.c_str()));

			hddist->Write();
			hsvdist->Write();
			hsvdist2->Write();
		}
		else if(strSplit[1]=="2"){
			const int nScans = atoi(strSplit[2].c_str());
			TCanvas* c = new TCanvas();
			c->Print(Form("%s[",outpdf.c_str()));
			//for(int k=0;k<hsvdist2->GetNbinsX();k++){	
			//	double RegPar=hsvdist2->GetBinContent(k+1);
			for(int k=0;k<nScans;k++){
				double RegPar=k*RegParMax/nScans;
				char hname[256];
				char htitle[256];
				TH1D* hunfres = tsvdunf->Unfold( RegPar );
				for (int i=1; i<=hunfres->GetNbinsX(); i++) hunfres->SetBinError(i, TMath::Sqrt(hutaucov->GetBinContent(i,i)));
				sprintf(hname,"hunfres_tau_%.2f",RegPar);
				hunfres->SetName(hname);
				sprintf(htitle,"#tau = %.2f;W(GeV/c^{2})",RegPar);
				hunfres->SetTitle(htitle);
				TH1D* hRatio = (TH1D*) hunfres->Clone();
				hRatio->Divide(hDataTrue);
				hRatio->SetMinimum(0);
				sprintf(hname,"hratio_tau_%.2f",RegPar);
				hRatio->SetName(hname);
				char fname[256];
				sprintf(fname,"fit_tau_%.2f",RegPar);
				TF1* fit = new TF1(fname,"pol0[0]",hRatio->GetXaxis()->GetXmin(),hRatio->GetXaxis()->GetXmax());				
				
				c->Divide(1,2,0.,0.);
				c->cd(1);
				gPad->SetTopMargin(0.1);
				hunfres->SetMinimum(1);
				hunfres->SetLineColor(1);
				hunfres->SetLineStyle(1);	
				hunfres->SetMarkerStyle(8);
				hunfres->Draw("E1");
				hDataTrue->SetLineColor(4);
				hDataTrue->Draw("HIST SAME");
				hData->SetLineColor(2);
				hData->SetLineStyle(2);
				hData->Draw("HIST SAME");
				TLegend* leg = new TLegend(0.58,0.68,0.9,0.88);
				leg->SetFillColor(0);
				leg->SetBorderSize(0);
				leg->AddEntry(hunfres,Form("Unfolded %s",hunfres->GetTitle()),"lp");
				leg->AddEntry(hDataTrue,hDataTrue->GetTitle(),"l");
				leg->AddEntry(hData,hData->GetTitle(),"l");
				leg->Draw();
				c->cd(2);
				gPad->SetBottomMargin(0.1);
				hRatio->Fit(fname,"R");
				hRatio->SetLineColor(1);
				hRatio->SetLineStyle(1);
				hRatio->Draw("E1");
				TLine* leq1 = new TLine(hRatio->GetXaxis()->GetXmin(),1,hRatio->GetXaxis()->GetXmax(),1);			
				leq1->SetLineStyle(2);	
				leq1->Draw();					
				c->Print(Form("%s",outpdf.c_str()));
				c->Clear();
				
				hunfres->Write();
				hRatio->Write();
				fit->Write();
			}

		c->Print(Form("%s]",outpdf.c_str()));
		}
		
	}
	else{
	
		m_RegPar=atof(m_strRegPar.c_str());
		// Perform the unfolding with regularisation parameter
		// - the larger kreg, the finer grained the unfolding, but the more fluctuations occur
		// - the smaller kreg, the stronger is the regularisation and the bias
		TH1D* hunfres = tsvdunf->Unfold( m_RegPar );
		
		// Get the distribution of the d to cross check the regularization
		// - choose kreg to be the point where |d_i| stop being statistically significantly >>1
		TH1D* hddist = tsvdunf->GetD();
		hddist->SetTitle( "TSVDUnfold |d_{i}|" );
		hddist->GetXaxis()->SetTitle( "i" );
		hddist->GetYaxis()->SetTitle( "|d_{i}|" );
		
		// Get the distribution of the singular values
		TH1D* hsvdist = tsvdunf->GetSV();
		hsvdist->GetXaxis()->SetTitle( "i" );
		hsvdist->GetYaxis()->SetTitle( "s_{i}" );
		
		TH1D* hsvdist2 = (TH1D*) hsvdist->Clone();
		hsvdist2->Multiply(hsvdist);
		hsvdist2->GetXaxis()->SetTitle( "i" );
		hsvdist2->GetYaxis()->SetTitle( "s_{i}^{2}" );
		
		// Compute the error matrix for the unfolded spectrum using toy MC
		// using the measured covariance matrix as input to generate the toys
		// 100 toys should usually be enough
		// The same method can be used for different covariance matrices separately.
		TH2D* ustatcov = tsvdunf->GetUnfoldCovMatrix( hstatcov, 100 );   

		// Now compute the error matrix on the unfolded distribution originating
		// from the finite detector matrix statistics
		TH2D* uadetcov = tsvdunf->GetAdetCovMatrix( 100 );   

		// Sum up the two (they are uncorrelated)
		ustatcov->Add( uadetcov );

		//Get the computed regularized covariance matrix (always corresponding to total uncertainty passed in constructor) and add uncertainties from finite MC statistics. 
		TH2D* utaucov = tsvdunf->GetXtau();
		utaucov->Add( uadetcov );

		//Get the computed inverse of the covariance matrix
		TH2D* uinvcov = tsvdunf->GetXinv();

		hunfres->Write();
		hddist->Write();
		hsvdist->Write();
		hsvdist2->Write();
		ustatcov->Write();
		uadetcov->Write();
		// --- Only plotting stuff below ------------------------------

		 for (int i=1; i<=hunfres->GetNbinsX(); i++) {
    		hunfres->SetBinError(i, TMath::Sqrt(utaucov->GetBinContent(i,i)));
		 }

		 // Renormalize just to be able to plot on the same scale
		 //hTrue->Scale(0.7*hDataTrue->Integral()/hTrue->Integral());

		 TLegend *leg = new TLegend(0.58,0.68,0.99,0.88);
		 leg->SetBorderSize(0);
		 leg->SetFillColor(0);
		 leg->SetFillStyle(0);
		 leg->AddEntry(hunfres,"Unfolded Data","p");
		 leg->AddEntry(hDataTrue,"True Data","l");
		 leg->AddEntry(hData,"Reconstructed Data","l");
		 leg->AddEntry(hTrue,"True MC","l");

		 TCanvas *c1 = new TCanvas( "c1", "Unfolding toy example with TSVDUnfold", 900, 800 );

		 // --- Style settings -----------------------------------------
		 Int_t c_Canvas    = 0;
		 Int_t c_FrameFill = 0;
		 Int_t c_TitleBox  = TColor::GetColor( "#6D7B8D" );
		 Int_t c_TitleText = TColor::GetColor( "#FFFFFF" );
		 
		 c1->SetFrameFillColor( c_FrameFill );
		 c1->SetFillColor     ( c_Canvas    );
		 c1->Divide(1,2);
		 TVirtualPad * c11 = c1->cd(1);
		 c11->SetFrameFillColor( c_FrameFill );
		 c11->SetFillColor     ( c_Canvas    );
		 
		 TH1D* frame = new TH1D( *hunfres );
		 frame->SetTitle( "Unfolding toy example with TSVDUnfold" );
		 frame->GetXaxis()->SetTitle( "x variable" );
		 frame->GetYaxis()->SetTitle( "Events" );
		 frame->GetXaxis()->SetTitleOffset( 1.25 );
		 frame->GetYaxis()->SetTitleOffset( 1.29 );
		 frame->Draw();

		 hData->SetLineStyle(2);
		 hData->SetLineColor(4);
		 hData->SetLineWidth(2);
		 hunfres->SetMarkerStyle(20);
		 hDataTrue->SetLineColor(2);
		 hDataTrue->SetLineWidth(2);
		 hTrue->SetLineStyle(2);
		 hTrue->SetLineColor(8);
		 hTrue->SetLineWidth(2);
		 // ------------------------------------------------------------

		 // add histograms
		 hunfres->Draw("same");
		 hDataTrue->Draw("same");
		 hData->Draw("same");
		 hTrue->Draw("same");

		 leg->Draw();

		 // covariance matrix
		 gStyle->SetPalette(1,0);
		 TVirtualPad * c12 = c1->cd(2);
		 c12->Divide(2,1);
		 TVirtualPad * c2 = c12->cd(1);
		 c2->SetFrameFillColor( c_FrameFill );
		 c2->SetFillColor     ( c_Canvas    );
		 c2->SetRightMargin   ( 0.15         );

		 TH2D* covframe = new TH2D( *ustatcov );
		 covframe->SetTitle( "TSVDUnfold covariance matrix" );
		 covframe->GetXaxis()->SetTitle( "x variable" );
		 covframe->GetYaxis()->SetTitle( "x variable" );
		 covframe->GetXaxis()->SetTitleOffset( 1.25 );
		 covframe->GetYaxis()->SetTitleOffset( 1.29 );
		 covframe->Draw();

		 ustatcov->SetLineWidth( 2 );
		 ustatcov->Draw( "colzsame" );

		 // distribution of the d quantity
		 TVirtualPad * c3 = c12->cd(2);
		 c3->SetFrameFillColor( c_FrameFill );
		 c3->SetFillColor     ( c_Canvas    );
		 c3->SetLogy();

		 TLine *line = new TLine( 0.,1.,40.,1. );
		 line->SetLineStyle(2);

		 TH1D* dframe = new TH1D( *hddist );		 
		 dframe->GetXaxis()->SetTitleOffset( 1.25 );
		 dframe->GetYaxis()->SetTitleOffset( 1.29 );
		 dframe->SetMinimum( 0.001 );
		 dframe->Draw();

		 hddist->SetLineWidth( 2 );
		 hddist->Draw( "same" );
		 line->Draw();

		 TCanvas* c4 = new TCanvas("c4");
		 hsvdist2->Draw("HIST");
		 gPad->SetLogy();
	 }
}

}

