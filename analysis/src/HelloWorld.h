/*
 * HelloWorld.h
 *
 *  Created on: Apr 23, 2018
 *      Author: bgarillo
 */

#ifndef HELLOWORLD_H_
#define HELLOWORLD_H_
#include <iostream>
#include "TApplication.h"
#include "TH1F.h"
#include "TCanvas.h"

using namespace std;

class HelloWorld {
public:
	HelloWorld();
	virtual ~HelloWorld();
	void run();
};

#endif /* HELLOWORLD_H_ */
