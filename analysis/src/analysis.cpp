//============================================================================
// Name        : analysis.cpp
// Author      : Brice Garillon
// Version     :
// Copyright   : 
// Description : Analysis program for gamma gamma physics in C++
//============================================================================

/*
 #include <iostream>
 #include "TH1F.h"
 #include "TCanvas.h"

 using namespace std;

 int main() {
 cout << "!!!Hello ROOT!!!" << endl; // prints !!!Hello World!!!
 TCanvas *c1 = new TCanvas("c1","test", 600,600);

 TH1F *hist = new TH1F("test", "test", 100,0,1);
 hist->Fill(0.1);
 hist->Fill(0.5);
 hist->Draw();

 c1->SaveAs("test.png");

 delete c1;
 return 0;
 }
*/
/*
#include <stdio.h>
#include <stdlib.h>

#include <iostream>
#include <string>

#include "Normalizer.h"
#include "MyGlobal.h"

using namespace std;

string srcPath("/data/work/kphpbb/bgarillo/ee-ee2Pi0/Bes2Gamto2Pi0-00-00-01");
string dataPath("/data/work/kphpbb/bgarillo/ee-ee2Pi0");

void execSys(string cmd) {
	cout << cmd << endl;

	//system(cmd.c_str());
}

bool find(string str, string pattern) {
	if (str.find(pattern) != string::npos)
		return true;
	else
		return false;
}

int main(int argc, char* argv[]) {
	cout << "****** Analysis for gamma gamma physics******" << endl;
	if (argc == 1) {
		cout << "Usage : " << endl;
		return 0;
	}

	string option(argv[1]);

	if (find(option, "--Selection ")) {
		cout << "Selection" << endl;
	}

	if (find(option, "--cpToWorkDir ")) {
		cout << "Copy files to analysis directory" << endl;
		execSys(srcPath + "/run/HIMSTER/scpMCIHEPtoHIMSTER.sh .");
		execSys("rm pipiList.txt");
		execSys("ls -1 pipi* > pipiList.txt");

		execSys("sed -i 's@Ekhara@'\"$PWD\"'/Ekhara@g' fileList.txt");

	}

	if (find(option, "--Normalize ")) {
		Normalizer* normmng = new Normalizer();
		delete normmng;
	}
	return 0;
}
*/

#include <stdio.h>
#include <stdlib.h>

#include <iostream>
#include <string>

#include "MyGlobal.h"

#include "HelloWorld.h"

#include "YieldTableMaker.h"
#include "LumGamGam.h"
#include "Normalizer.h"
#include "CompDataMCScaledPlotter.h"
#include "THnSparseMaker.h"
#include "BgndSubtractor.h"
#include "Efficiency.h"
#include "HoleFactors.h"
#include "CrossSection.h"
#include "PartialWaveFitter1D.h"
#include "PartialWaveFitter.h"

using namespace std;

string srcPath("/data/work/kphpbb/bgarillo/ee-ee2Pi0/Bes2Gamto2Pi0-00-00-01");
string dataPath("/data/work/kphpbb/bgarillo/ee-ee2Pi0");

vector<string> g_vecTasks;
vector<vector<string> > g_vecTaskArgs;

void execSys(string cmd) {
	cout << cmd << endl;

	//system(cmd.c_str());
}

bool find(string str, string pattern) {
	if (str.find(pattern) != string::npos)
		return true;
	else
		return false;
}

int main(int argc, char* argv[]) {
	//Parse the command lines
	if(argc==1){
		cout << "Usage : " << endl;
	}
	else if(argc==2){
		cout << "Reading command lines configuration file" << endl;
		ifstream readFile(argv[1]);
		if(readFile.is_open()){
			while(readFile.good()){
				string strline;
				getline(readFile,strline);
				//cout << strline << endl;
				vector<string> splitstr = MyGlobal::tokenizeArgs(strline);
				g_vecTasks.push_back(splitstr[0]);
				vector<string> taskArgs;
				for(int i=1;i<splitstr.size()-1;i++){
					taskArgs.push_back(splitstr[i]);
				}
				g_vecTaskArgs.push_back(taskArgs);
			}
		}

	}
	else{
		cout << "Reading single command line" << endl;
		g_vecTasks.resize(1);
		g_vecTasks[0]=string(argv[1]);
		g_vecTaskArgs.resize(1);
		for(int i=0;i<argc-2;i++){
			g_vecTaskArgs[0].push_back(string(argv[i+2]));
		}
	}
	cout << endl;
	//Execute the commands
	cout << "Executing commands" << endl;
	for(int i=0;i<g_vecTasks.size();i++){
		cout << "	" << g_vecTasks[i] << endl;
		if(g_vecTasks[i].find("HelloWorld") != string::npos){
			HelloWorld* obj = new HelloWorld();
			obj->run();
		}

		if(g_vecTasks[i].find("YieldTable") != string::npos){
			YieldTableMaker* obj = new YieldTableMaker(g_vecTaskArgs[i][0],g_vecTaskArgs[i][1],g_vecTaskArgs[i][2]);
			obj->run();
			delete obj;
		}
		if(g_vecTasks[i].find("LumGamGam") != string::npos){
			LumGamGam* obj = new LumGamGam();
			obj->run(g_vecTaskArgs[i][0],g_vecTaskArgs[i][1],g_vecTaskArgs[i][2]);
			delete obj;
		}
		if(g_vecTasks[i].find("NormalizeToLum") != string::npos){
			Normalizer* obj = new Normalizer();
			obj->run(g_vecTaskArgs[i][0]);
			delete obj;
		}
		if(g_vecTasks[i].find("CompDataMCScaledPlotter") != string::npos){
			CompDataMCScaledPlotter* obj = new CompDataMCScaledPlotter();
			obj->runAll(g_vecTaskArgs[i][0],g_vecTaskArgs[i][1],g_vecTaskArgs[i][2]);
			delete obj;
		}
		if(g_vecTasks[i].find("THnSparseMaker") != string::npos){
			THnSparseMaker* obj = new THnSparseMaker();
			obj->run(g_vecTaskArgs[i][0],g_vecTaskArgs[i][1]);
			delete obj;
		}
		if(g_vecTasks[i].find("BgndSubtractor") != string::npos){
			BgndSubtractor* obj = new BgndSubtractor();
			obj->run(g_vecTaskArgs[i][0],g_vecTaskArgs[i][1],g_vecTaskArgs[i][2]);
			delete obj;
		}
		if(g_vecTasks[i].find("Efficiency") != string::npos){
			Efficiency* obj = new Efficiency();
			obj->run(g_vecTaskArgs[i][0],g_vecTaskArgs[i][1],g_vecTaskArgs[i][2]);
			delete obj;
		}
		if(g_vecTasks[i].find("HoleFactors") != string::npos){
			HoleFactors* obj = new HoleFactors(g_vecTaskArgs[i][0],g_vecTaskArgs[i][1],g_vecTaskArgs[i][2]);
			obj->run();
			delete obj;
		}
		if(g_vecTasks[i].find("CrossSection") != string::npos){
			CrossSection* obj = new CrossSection();
			obj->run(g_vecTaskArgs[i][0],g_vecTaskArgs[i][1]);
			delete obj;
		}
		if(g_vecTasks[i].find("PartialWaveFitter1D") != string::npos){
			PartialWaveFitter1D* obj = new PartialWaveFitter1D(g_vecTaskArgs[i][0],g_vecTaskArgs[i][1],g_vecTaskArgs[i][2]);
			obj->run();
			delete obj;
		}
		if(g_vecTasks[i].find("PartialWaveFitter") != string::npos){
			//PartialWaveFitter* obj = new PartialWaveFitter(g_vecTaskArgs[i][0],g_vecTaskArgs[i][1],g_vecTaskArgs[i][2]);
			PartialWaveFitter* obj = new PartialWaveFitter();
			obj->run();
			delete obj;
		}
	}

	return 0;
}
