/*
 * HelloWorld.cxx
 *
 *  Created on: Apr 23, 2018
 *      Author: bgarillo
 */

#include "HelloWorld.h"

HelloWorld::HelloWorld() {
	// TODO Auto-generated constructor stub

}

HelloWorld::~HelloWorld() {
	// TODO Auto-generated destructor stub
}

void HelloWorld::run(){
	TApplication *app = new TApplication("app", 0, 0);
	 cout << "!!!Hello ROOT!!!" << endl; // prints !!!Hello World!!!
	 TCanvas *c1 = new TCanvas("c1","test", 600,600);

	 TH1F *hist = new TH1F("test", "test", 100,0,1);
	 hist->Fill(0.1);
	 hist->Fill(0.5);
	 hist->Draw();

	 //c1->SaveAs("test.png");

	 //delete c1;

	 app->Run();
	 return ;
}

