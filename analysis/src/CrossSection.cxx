/*
 * CrossSection.cxx
 *
 *  Created on: Jan 19, 2018
 *      Author: bgarillo
 */

#include "CrossSection.h"

CrossSection::CrossSection():
	m_CalcMode(0),
	m_Luminosity(0),
	m_LimBinPath(""),
	m_DataNTFileName(""),
	m_DataSubtrFileName(""),
	m_BgndFracFileName(""),
	m_EffRecFileName(""),
	m_LumGamGamFileName(""),
	m_UseFittedKinematics(1),
	m_picoToFemto(1e3),
	m_picoToNano(1e-3),
	m_BF_Pi0to2Gam(0.98823)
	{
	// TODO Auto-generated constructor stub

}

CrossSection::~CrossSection() {
	// TODO Auto-generated destructor stub
}

void CrossSection::run(string inputcard, string outpath){

	ifstream readInput(inputcard.c_str());

	if(readInput.is_open()){
		while(readInput.good()){
			string strline;
			getline(readInput,strline);

			vector<string> strsplit = MyGlobal::tokenize(strline,' ');
			for(int k=0;k<strsplit.size();k++){
				vector<string> vecstr = MyGlobal::tokenize(strsplit[k],'=');
				string arg = vecstr[0];
				string val = vecstr[1];
				if(arg=="Luminosity") m_Luminosity=atof(val.c_str());
				if(arg=="CalcMode") 	m_CalcMode=atoi(val.c_str());
				if(arg=="DataNTuple") m_DataNTFileName=val;
				if(arg=="DataSubtr") m_DataSubtrFileName=val;
				if(arg=="BgndFrac") m_BgndFracFileName=val;
				if(arg=="EffRec") m_EffRecFileName=val;
				if(arg=="LumGamGam") m_LumGamGamFileName=val;
			}
		}

	}

	if(m_CalcMode==0){
		//Data NTuple
		TFile* fileData;
		fileData = TFile::Open(m_DataNTFileName.c_str());
		if(fileData==NULL){cout << "****** CANNOT OPEN DATA FILE ******" <<endl;return;}
		TTree* treeData = (TTree*) fileData->Get("Tagged");
		if(treeData==NULL){cout << "****** CANNOT NTUPLE DATA FILE ******" <<endl;return;}
		double Q2,W,CThetaSt,absCThetaSt,PhiStarPi01;
		int QTag;

		if(m_UseFittedKinematics==0){
			treeData->SetBranchAddress("Q2",&Q2);
			treeData->SetBranchAddress("2Pi0InvM", &W);
			treeData->SetBranchAddress("cosThetaStarPi01", &CThetaSt);
			treeData->SetBranchAddress("QTag", &QTag);
			treeData->SetBranchAddress("PhiStarPi01", &PhiStarPi01);
		}
		else{
			treeData->SetBranchAddress("Q2Fit",&Q2);
			treeData->SetBranchAddress("2Pi0InvMFit", &W);
			treeData->SetBranchAddress("cosThetaStarPi0Fit1", &CThetaSt);
			treeData->SetBranchAddress("QTag", &QTag);
			treeData->SetBranchAddress("PhiStarPi0Fit1", &PhiStarPi01);
		}
		//Reconstruction efficiency
		TFile* fileEffRec;
		fileEffRec = TFile::Open(m_EffRecFileName.c_str());
		if(fileEffRec==NULL){cout << "****** CANNOT OPEN RECONSTRUCTION EFFICIENCY FILE ******" << endl; return;}
		fileEffRec->ls();
		map<string,THnSparseD*> maphnDGEN;
		maphnDGEN["eT"] =(THnSparseD*)fileEffRec->Get("hnDeTGEN");
		maphnDGEN["pT"] =(THnSparseD*)fileEffRec->Get("hnDpTGEN");

		map<string,THnSparseD*> maphnDREC;
		maphnDREC["eT"] =(THnSparseD*)fileEffRec->Get("hnDeTREC");
		maphnDREC["pT"] =(THnSparseD*)fileEffRec->Get("hnDpTREC");

		map<string,THnSparseD*> maphnDEff;
		maphnDEff["eT"] =(THnSparseD*)fileEffRec->Get("hnDeTEff");
		maphnDEff["pT"] =(THnSparseD*)fileEffRec->Get("hnDpTEff");

		for (std::map<string, THnSparseD*>::iterator it=maphnDEff.begin(); it!=maphnDEff.end(); ++it){
			if(it->second==NULL){
				cout << "CANNOT ACCESS Efficency THN" << endl; return;
			}
		}

		//Background fraction
		TFile* fileBgndFrac;
		fileBgndFrac = TFile::Open(m_BgndFracFileName.c_str());
		map<string, THnSparseD*> maphnDBgndFrac;
		maphnDBgndFrac["eT"]=(THnSparseD*)fileBgndFrac->Get("hnDeTSumBgndRatio");
		maphnDBgndFrac["pT"]=(THnSparseD*)fileBgndFrac->Get("hnDpTSumBgndRatio");
		for (std::map<string, THnSparseD*>::iterator it=maphnDBgndFrac.begin(); it!=maphnDBgndFrac.end(); ++it){
			if(it->second==NULL){
				cout << "CANNOT ACCESS Background fraction THN" << endl; return;
			}
		}

		const Int_t nDim = maphnDBgndFrac["eT"]->GetNdimensions();

		//Data yield
		map<string, THnSparseD*> maphnDYieldee;
		maphnDYieldee["eT"] = (THnSparseD*) maphnDBgndFrac["eT"]->Clone();
		maphnDYieldee["pT"] = (THnSparseD*) maphnDBgndFrac["pT"]->Clone();
		for (std::map<string, THnSparseD*>::iterator it=maphnDYieldee.begin(); it!=maphnDYieldee.end(); ++it){
			string histoname;
			histoname="hnD"+(it->first)+"Yieldee";
			(it->second)->SetName(histoname.c_str());
			(it->second)->Reset();
			(it->second)->Sumw2();
		}

		//Efficiency corrected yield
		map<string, THnSparseD*> maphnDWYieldee;
		maphnDWYieldee["eT"] = (THnSparseD*) maphnDBgndFrac["eT"]->Clone();
		maphnDWYieldee["pT"] = (THnSparseD*) maphnDBgndFrac["pT"]->Clone();
		for (std::map<string, THnSparseD*>::iterator it=maphnDWYieldee.begin(); it!=maphnDWYieldee.end(); ++it){
			string histoname;
			histoname="hnD"+(it->first)+"WYieldee";
			(it->second)->SetName(histoname.c_str());
			(it->second)->Reset();
			(it->second)->Sumw2();
		}

		map<string, THnSparseD*> maphnDWYieldLgg;
		maphnDWYieldLgg["eT"] = (THnSparseD*) maphnDBgndFrac["eT"]->Clone();
		maphnDWYieldLgg["pT"] = (THnSparseD*) maphnDBgndFrac["pT"]->Clone();
		for (std::map<string, THnSparseD*>::iterator it=maphnDWYieldLgg.begin(); it!=maphnDWYieldLgg.end(); ++it){
			string histoname;
			histoname="hnD"+(it->first)+"WYieldLgg";
			(it->second)->SetName(histoname.c_str());
			(it->second)->Reset();
			(it->second)->Sumw2();
		}

		map<string, THnSparseD*> maphnDWYieldgg;
		maphnDWYieldgg["eT"] = (THnSparseD*) maphnDBgndFrac["eT"]->Clone();
		maphnDWYieldgg["pT"] = (THnSparseD*) maphnDBgndFrac["pT"]->Clone();
		for (std::map<string, THnSparseD*>::iterator it=maphnDWYieldgg.begin(); it!=maphnDWYieldgg.end(); ++it){
			string histoname;
			histoname="hnD"+(it->first)+"WYieldgg";
			(it->second)->SetName(histoname.c_str());
			(it->second)->Reset();
			(it->second)->Sumw2();
		}

		//Cross section histogram
		/*
		map<string, THnSparseD*> maphnDXSec;
		maphnDXSec["eT"] = (THnSparseD*) maphnDBgndFrac["eT"]->Clone();
		maphnDXSec["pT"] = (THnSparseD*) maphnDBgndFrac["pT"]->Clone();
		for (std::map<string, THnSparseD*>::iterator it=maphnDXSec.begin(); it!=maphnDXSec.end(); ++it){
					string histoname;
					histoname="hnD"+(it->first)+"XSec";
					(it->second)->SetName(histoname.c_str());
					(it->second)->Reset();
					(it->second)->Sumw2();
		}
		*/

		//Gamma Gamma luminosity function
		TFile* fileLGamGam = TFile::Open(m_LumGamGamFileName.c_str());
		if(fileLGamGam==NULL){cout << "****** CANNOT OPEN GAMMA GAMMA LUMINOSITY FILE ******" << endl; return;}
		TGraph2D* grLGamGam = (TGraph2D*) fileLGamGam->Get("gr2D");

		string outROOTname=outpath+".root";
		TFile* outROOT;
		outROOT = new TFile(outROOTname.c_str(),"RECREATE");

		//Cross section Ntuple
		m_ntXSecgg = new TTree("XSecTree","");
		m_ntXSecgg->Branch("Q2",&m_Q2,"Q2/D");
		m_ntXSecgg->Branch("W",&m_W,"W/D");
		m_ntXSecgg->Branch("cosThetaStarPi01",&m_CThetaSt,"cosThetaStarPi01/D");
		m_ntXSecgg->Branch("PhiStarPi01",&m_PhiStarPi01,"PhiStarPi01/D");
		m_ntXSecgg->Branch("weightgg",&m_weightgg,"weightgg/D");

		//Fill histograms
		cout << treeData->GetEntries() << endl; sleep(2);
		for(int entry=0;entry<treeData->GetEntries();entry++){
			if (entry % 1 == 0) cout << "Event counter = " << entry << endl;

			treeData->GetEntry(entry);
			double absCThetaSt = fabs(CThetaSt);

			vector<double> binval; binval.resize(nDim);
			if(nDim==3){binval[0]=Q2;binval[1]=W;binval[2]=absCThetaSt;}
			if(nDim==4){binval[0]=Q2;binval[1]=W;binval[2]=absCThetaSt;binval[3]=MyGlobal::getPhiStar(PhiStarPi01);}

			double weight=0;
			double fracbgnd=0;
			double eff=0;
			double errEff=0;
			double lgg=0;

			string key("");
			cout << "QTag " << QTag << endl;
			if(QTag==-1){
				key="eT";
			}
			else if(QTag==1){
				key="pT";
			}
			else continue;
			cout << key << endl;


			Long64_t binBgndFrac=maphnDBgndFrac[key]->GetBin(&(binval[0]),kFALSE);
			if(binBgndFrac!=-1)fracbgnd=maphnDBgndFrac[key]->GetBinContent(binBgndFrac);

			Long64_t binEff=maphnDEff[key]->GetBin(&(binval[0]),kFALSE);
			//if(binEff!=-1) eff=maphnDEff[key]->GetBinContent(binEff);
			if(binEff!=-1) {
				vector<int> coord; coord.resize(maphnDEff[key]->GetNdimensions());
				eff=maphnDEff[key]->GetBinContent(binEff,&(coord[0]));
				errEff=maphnDEff[key]->GetBinError(&(coord[0]));
			}

			lgg=grLGamGam->Interpolate(Q2,W);

			//for(int k=0;k<nDim;k++) cout << binval[k] << " " ;
			//cout << endl;
			//cout << fracbgnd << " " << eff << " " << lgg << endl;

			if(eff==0) continue;
			//if(errEff/eff>0.5) continue;
			if(lgg==0) continue;
			//Weighted Yield histograms
			maphnDYieldee[key]->Fill(&(binval[0]));

			weight = (1-fracbgnd)/eff;
			maphnDWYieldee[key]->Fill(&(binval[0]),weight);

			weight = 1/(2*lgg);
			maphnDWYieldLgg[key]->Fill(&(binval[0]),weight);

			weight = (1-fracbgnd)/(eff*2*lgg);
			maphnDWYieldgg[key]->Fill(&(binval[0]),weight);

			m_Q2=Q2;
			m_W=W;
			m_CThetaSt=absCThetaSt;
			m_PhiStarPi01=MyGlobal::getPhiStar(PhiStarPi01);
			m_weightgg=weight;
			m_ntXSecgg->Fill();
		}

		//Recalculation of statistical error for weighted events
		for (std::map<string, THnSparseD*>::iterator it=maphnDWYieldee.begin(); it!=maphnDWYieldee.end(); ++it){
			string key = it->first;

			for(int bin=0;bin<maphnDWYieldee[key]->GetNbins();bin++){
				vector<int> coord; coord.resize(maphnDWYieldee[key]->GetNdimensions());
				double weightOld = maphnDWYieldee[key]->GetBinContent(bin,&(coord[0]));
				double yield=maphnDYieldee[key]->GetBinContent(&(coord[0]));
				double errYield=maphnDYieldee[key]->GetBinError(&(coord[0]));
				double fracbgnd=maphnDBgndFrac[key]->GetBinContent(&(coord[0]));
				double errFracBgnd=maphnDBgndFrac[key]->GetBinError(&(coord[0]));
				double eff=maphnDEff[key]->GetBinContent(&(coord[0]));
				double errEff=maphnDEff[key]->GetBinError(&(coord[0]));

				double errWeight2=pow((1-fracbgnd)*errYield/eff,2)
												 	+pow(-yield*errFracBgnd/eff,2)
													+pow(-yield*(1-fracbgnd)*errEff/(eff*eff),2)
													;
				double errWeight=sqrt(errWeight2);
				if(eff!=0) maphnDWYieldee[key]->SetBinError(&(coord[0]),errWeight);
			}
		}

		for (std::map<string, THnSparseD*>::iterator it=maphnDWYieldgg.begin(); it!=maphnDWYieldgg.end(); ++it){
			string key = it->first;

			for(int bin=0;bin<maphnDWYieldgg[key]->GetNbins();bin++){
				vector<int> coord; coord.resize(maphnDWYieldgg[key]->GetNdimensions());
				double weightOld = maphnDWYieldgg[key]->GetBinContent(bin,&(coord[0]));
				double yield=maphnDWYieldLgg[key]->GetBinContent(&(coord[0]));
				double errYield=maphnDWYieldLgg[key]->GetBinError(&(coord[0]));
				double fracbgnd=maphnDBgndFrac[key]->GetBinContent(&(coord[0]));
				double errFracBgnd=maphnDBgndFrac[key]->GetBinError(&(coord[0]));
				double eff=maphnDEff[key]->GetBinContent(&(coord[0]));
				double errEff=maphnDEff[key]->GetBinError(&(coord[0]));

				double errWeight2=pow((1-fracbgnd)*errYield/eff,2)
												 	+pow(-yield*errFracBgnd/eff,2)
													+pow(-yield*(1-fracbgnd)*errEff/(eff*eff),2)
													;
				double errWeight=sqrt(errWeight2);
				if(eff!=0) maphnDWYieldgg[key]->SetBinError(&(coord[0]),errWeight);
			}
		}

		//Sum eT and pT
		maphnDWYieldee["All"]=(THnSparseD*)maphnDWYieldee["eT"]->Clone();
		maphnDWYieldee["All"]->Add(maphnDWYieldee["pT"]);
		maphnDWYieldee["All"]->SetName("hnDWYieldee");

		maphnDWYieldgg["All"]=(THnSparseD*)maphnDWYieldgg["eT"]->Clone();
		maphnDWYieldgg["All"]->Add(maphnDWYieldgg["pT"]);
		maphnDWYieldgg["All"]->SetName("hnDWYieldgg");

		map<string, THnSparseD*> maphnDXSecee;
		maphnDXSecee["eT"]=(THnSparseD*)maphnDWYieldee["eT"]->Clone();
		maphnDXSecee["eT"]->SetName("hnDeTXSecee");

		maphnDXSecee["pT"]=(THnSparseD*)maphnDWYieldee["pT"]->Clone();
		maphnDXSecee["pT"]->SetName("hnDpTXSecee");

		maphnDXSecee["All"]=(THnSparseD*)maphnDWYieldee["All"]->Clone();
		maphnDXSecee["All"]->SetName("hnDXSecee");

		for(std::map<string, THnSparseD*>::iterator it=maphnDXSecee.begin(); it!=maphnDXSecee.end(); ++it){
			string key = it->first;
			MyGlobal::scaleTHnSparse(maphnDXSecee[key],m_picoToNano/((m_Luminosity)*m_BF_Pi0to2Gam*m_BF_Pi0to2Gam),"width");
		}

		map<string, THnSparseD*> maphnDXSecgg;
		maphnDXSecgg["eT"]=(THnSparseD*)maphnDWYieldgg["eT"]->Clone();
		maphnDXSecgg["eT"]->SetName("hnDeTXSecgg");

		maphnDXSecgg["pT"]=(THnSparseD*)maphnDWYieldgg["pT"]->Clone();
		maphnDXSecgg["pT"]->SetName("hnDpTXSecgg");

		maphnDXSecgg["All"]=(THnSparseD*)maphnDWYieldgg["All"]->Clone();
		maphnDXSecgg["All"]->SetName("hnDXSecgg");

		for(std::map<string, THnSparseD*>::iterator it=maphnDXSecgg.begin(); it!=maphnDXSecgg.end(); ++it){
			string key = it->first;
			MyGlobal::scaleTHnSparse(maphnDXSecgg[key],m_picoToNano/((m_Luminosity)*m_BF_Pi0to2Gam*m_BF_Pi0to2Gam),"width");
		}

		//map<string, THnSparseD*> maphnDXSecee;
		//Projection
		vector<string> axisNames;
		map<int,double> mapBinMins;
		map<int,double> mapBinMaxs;

		for(int i=0;i<maphnDWYieldee["All"]->GetNdimensions();i++){
		axisNames.push_back(maphnDWYieldee["All"]->GetAxis(i)->GetName());
		int key=i;
		mapBinMins[key]=maphnDWYieldee["All"]->GetAxis(i)->GetFirst();
		mapBinMaxs[key]=maphnDWYieldee["All"]->GetAxis(i)->GetLast();
		}

		outROOT->cd();
		//XSec Abs(cos Theta Star)  in Q2,M2Pi bins
		for (std::map<string, THnSparseD*>::iterator it=maphnDWYieldee.begin(); it!=maphnDWYieldee.end(); ++it){
		string key = it->first;
		for(int binU=mapBinMins[0];binU<=mapBinMaxs[0];binU++){
			for(int binV=mapBinMins[1];binV<=mapBinMaxs[1];binV++){
				string UName; UName=string(maphnDWYieldee[key]->GetAxis(0)->GetName());
				string VName; VName=string(maphnDWYieldee[key]->GetAxis(1)->GetName());

				string UTitle; UTitle=string(maphnDWYieldee[key]->GetAxis(0)->GetTitle());
				string VTitle; VTitle=string(maphnDWYieldee[key]->GetAxis(1)->GetTitle());

				THnSparseD* hnDWYieldee_Restrict = (THnSparseD*) maphnDWYieldee[key]->Clone();
				hnDWYieldee_Restrict->GetAxis(0)->SetRange(binU,binU);
				hnDWYieldee_Restrict->GetAxis(1)->SetRange(binV,binV);

				THnSparseD* hnDWYieldgg_Restrict = (THnSparseD*) maphnDWYieldgg[key]->Clone();
				hnDWYieldgg_Restrict->GetAxis(0)->SetRange(binU,binU);
				hnDWYieldgg_Restrict->GetAxis(1)->SetRange(binV,binV);

				THnSparseD* hnDXSecee_Restrict = (THnSparseD*) maphnDXSecee[key]->Clone();
				hnDXSecee_Restrict->GetAxis(0)->SetRange(binU,binU);
				hnDXSecee_Restrict->GetAxis(1)->SetRange(binV,binV);

				THnSparseD* hnDXSecgg_Restrict = (THnSparseD*) maphnDXSecgg[key]->Clone();
				hnDXSecgg_Restrict->GetAxis(0)->SetRange(binU,binU);
				hnDXSecgg_Restrict->GetAxis(1)->SetRange(binV,binV);

				double UMin=hnDWYieldee_Restrict->GetAxis(0)->GetBinLowEdge(binU);
				double UMax=hnDWYieldee_Restrict->GetAxis(0)->GetBinUpEdge(binU);
				double deltaU=(hnDWYieldee_Restrict->GetAxis(0)->GetBinUpEdge(binU))
											-(hnDWYieldee_Restrict->GetAxis(0)->GetBinLowEdge(binU));
				double VMin=hnDWYieldee_Restrict->GetAxis(1)->GetBinLowEdge(binV);
				double VMax=hnDWYieldee_Restrict->GetAxis(1)->GetBinUpEdge(binV);
				double deltaV=(hnDWYieldee_Restrict->GetAxis(1)->GetBinUpEdge(binV))
											-(hnDWYieldee_Restrict->GetAxis(1)->GetBinLowEdge(binV));
				/*
				TH1D* hproj=(TH1D*) hnDWYieldee_Restrict->Projection(2);
				hproj->Scale(1/((m_Luminosity)*deltaU*deltaV*m_BF_Pi0to2Gam*m_BF_Pi0to2Gam),"width");
				//printf("%f %f \n",deltaU,deltaV);sleep(1);
				string hname;
				hname="dsigmaeed"+axisNames[2]+key+"_"
							+axisNames[0]+axisNames[1]+"_"
							+MyGlobal::intToStr(binU-1)+"-"+MyGlobal::intToStr(binV-1);
				hproj->SetName(hname.c_str());
				string htitle;
				htitle=MyGlobal::doubleToStr(UMin,2)+"#leq "+ string(hnDWYieldee_Restrict->GetAxis(0)->GetTitle())+"< "+MyGlobal::doubleToStr(UMax,2)+", "
							 +MyGlobal::doubleToStr(VMin,2)+"#leq "+ string(hnDWYieldee_Restrict->GetAxis(2)->GetTitle())+"< "+MyGlobal::doubleToStr(VMax,2);
				hproj->SetTitle(htitle.c_str());
				string hXtitle;
				hXtitle=string(maphnDWYieldee[key]->GetAxis(2)->GetTitle());
				hproj->GetXaxis()->SetTitle(hXtitle.c_str());
				string hYtitle;
				hYtitle="d^{3}#sigma_{ee}/dQ^{2}d|cos #theta*|dW (pb/(GeV^{3}/c^{2}))";
				hproj->GetYaxis()->SetTitle(hYtitle.c_str());
				hproj->Write();
				*/
				/*
				hproj=(TH1D*) hnDWYieldgg_Restrict->Projection(2);
				hproj->Scale(m_picoToNano/((m_Luminosity)*deltaU*deltaV*m_BF_Pi0to2Gam*m_BF_Pi0to2Gam),"width");
				hname="dsigmaggd"+axisNames[2]+key+"_"
							+axisNames[0]+axisNames[1]+"_"
							+MyGlobal::intToStr(binU-1)+"-"+MyGlobal::intToStr(binV-1);
				hproj->SetName(hname.c_str());
				htitle=MyGlobal::doubleToStr(UMin,2)+"#leq "+ string(hnDWYieldgg_Restrict->GetAxis(0)->GetTitle())+"< "+MyGlobal::doubleToStr(UMax,2)+", "
							 +MyGlobal::doubleToStr(VMin,2)+"#leq "+ string(hnDWYieldgg_Restrict->GetAxis(1)->GetTitle())+"< "+MyGlobal::doubleToStr(VMax,2);
				hproj->SetTitle(htitle.c_str());
				hXtitle=string(maphnDWYieldgg[key]->GetAxis(2)->GetTitle());
				hproj->GetXaxis()->SetTitle(hXtitle.c_str());
				hYtitle="d^{3}#sigma_{#gamma#gamma}/dQ^{2}d|cos #theta*|dW (nb/(GeV^{3}/c^{2}))";
				hproj->GetYaxis()->SetTitle(hYtitle.c_str());
				hproj->Write();
				*/
				TH1D* hproj=(TH1D*) hnDXSecee_Restrict->Projection(2);
				string hname;
				hname="dsigmaeed"+axisNames[2]+key+"_"
							+axisNames[0]+axisNames[1]+"_"
							+MyGlobal::intToStr(binU-1)+"-"+MyGlobal::intToStr(binV-1);
				hproj->SetName(hname.c_str());
				string htitle;
				htitle=MyGlobal::doubleToStr(UMin,2)+"#leq "+ string(hnDWYieldee_Restrict->GetAxis(0)->GetTitle())+"< "+MyGlobal::doubleToStr(UMax,2)+", "
							 +MyGlobal::doubleToStr(VMin,2)+"#leq "+ string(hnDWYieldee_Restrict->GetAxis(2)->GetTitle())+"< "+MyGlobal::doubleToStr(VMax,2);
				hproj->SetTitle(htitle.c_str());
				string hXtitle;
				hXtitle=string(maphnDWYieldee[key]->GetAxis(2)->GetTitle());
				hproj->GetXaxis()->SetTitle(hXtitle.c_str());
				string hYtitle;
				hYtitle="d^{3}#sigma_{ee}/dQ^{2}d|cos #theta*|dW (pb/(GeV^{3}/c^{2}))";
				hproj->GetYaxis()->SetTitle(hYtitle.c_str());
				hproj->Write();

				hproj=(TH1D*) hnDXSecgg_Restrict->Projection(2);
				hname="dsigmaggd"+axisNames[2]+key+"_"
											+axisNames[0]+axisNames[1]+"_"
											+MyGlobal::intToStr(binU-1)+"-"+MyGlobal::intToStr(binV-1);
				hproj->SetName(hname.c_str());
				htitle=MyGlobal::doubleToStr(UMin,2)+"#leq "+ string(hnDXSecgg_Restrict->GetAxis(0)->GetTitle())+"< "+MyGlobal::doubleToStr(UMax,2)+", "
											 +MyGlobal::doubleToStr(VMin,2)+"#leq "+ string(hnDXSecgg_Restrict->GetAxis(1)->GetTitle())+"< "+MyGlobal::doubleToStr(VMax,2);
				hproj->SetTitle(htitle.c_str());
				hXtitle=string(maphnDXSecgg[key]->GetAxis(2)->GetTitle());
				hproj->GetXaxis()->SetTitle(hXtitle.c_str());
				hYtitle="d#sigma_{#gamma#gamma}/d|cos #theta*| (nb)";
				hproj->GetYaxis()->SetTitle(hYtitle.c_str());
				hproj->Write();
			}
		}
		}

		//M2Pi distribution
		for (std::map<string, THnSparseD*>::iterator it=maphnDWYieldee.begin(); it!=maphnDWYieldee.end(); ++it){
			string key = it->first;
			THnSparseD* hnDWYieldee_Restrict = (THnSparseD*) maphnDWYieldee[key]->Clone();
			THnSparseD* hnDWYieldgg_Restrict = (THnSparseD*) maphnDWYieldgg[key]->Clone();

			TH1D* hproj;
			hproj=(TH1D*) hnDWYieldee_Restrict->Projection(1);
			hproj->Scale(m_picoToNano/((m_Luminosity)*m_BF_Pi0to2Gam*m_BF_Pi0to2Gam),"width");
			string hname;
			hname="dsigmaeed"+axisNames[1]+key;
			hproj->SetName(hname.c_str());
			string htitle("");
			hproj->SetTitle(htitle.c_str());
			string hXtitle;
			hXtitle=string(maphnDWYieldee[key]->GetAxis(1)->GetTitle());
			hproj->GetXaxis()->SetTitle(hXtitle.c_str());
			string hYtitle;
			hYtitle="d#sigma_{ee}/dW (pb/(GeV/c^{2}))";
			hproj->GetYaxis()->SetTitle(hYtitle.c_str());
			hproj->Write();

			hproj=(TH1D*) hnDWYieldgg_Restrict->Projection(1);
			hproj->Scale(m_picoToNano/((m_Luminosity)*m_BF_Pi0to2Gam*m_BF_Pi0to2Gam),"width");
			hname="dsigmaggd"+axisNames[1]+key;
			hproj->SetName(hname.c_str());
			hproj->SetTitle(htitle.c_str());
			hXtitle=string(maphnDWYieldgg[key]->GetAxis(1)->GetTitle());
			hproj->GetXaxis()->SetTitle(hXtitle.c_str());
			hYtitle="d#sigma_{#gamma#gamma}/dW (nb/(GeV/c^{2}))";
			hproj->GetYaxis()->SetTitle(hYtitle.c_str());
			hproj->Write();
		}

		//M2Pi distributions  in Q2,Abs(cos Theta Star) bins
		for (std::map<string, THnSparseD*>::iterator it=maphnDWYieldee.begin(); it!=maphnDWYieldee.end(); ++it){
		string key = it->first;
		for(int binU=mapBinMins[0];binU<=mapBinMaxs[0];binU++){
			for(int binV=mapBinMins[2];binV<=mapBinMaxs[2];binV++){
				string UName; UName=string(maphnDWYieldee[key]->GetAxis(0)->GetName());
				string VName; VName=string(maphnDWYieldee[key]->GetAxis(2)->GetName());

				string UTitle; UTitle=string(maphnDWYieldee[key]->GetAxis(0)->GetTitle());
				string VTitle; VTitle=string(maphnDWYieldee[key]->GetAxis(2)->GetTitle());

				THnSparseD* hnDWYieldee = (THnSparseD*) maphnDWYieldee[key]->Clone();
				hnDWYieldee->GetAxis(0)->SetRange(binU,binU);
				hnDWYieldee->GetAxis(2)->SetRange(binV,binV);

				THnSparseD* hnDWYieldgg = (THnSparseD*) maphnDWYieldgg[key]->Clone();
				hnDWYieldgg->GetAxis(0)->SetRange(binU,binU);
				hnDWYieldgg->GetAxis(2)->SetRange(binV,binV);

				double UMin=hnDWYieldee->GetAxis(0)->GetBinLowEdge(binU);
				double UMax=hnDWYieldee->GetAxis(0)->GetBinUpEdge(binU);
				double deltaU=(hnDWYieldee->GetAxis(0)->GetBinUpEdge(binU))
											-(hnDWYieldee->GetAxis(0)->GetBinLowEdge(binU));
				double VMin=hnDWYieldee->GetAxis(2)->GetBinLowEdge(binV);
				double VMax=hnDWYieldee->GetAxis(2)->GetBinUpEdge(binV);
				double deltaV=(hnDWYieldee->GetAxis(2)->GetBinUpEdge(binV))
											-(hnDWYieldee->GetAxis(2)->GetBinLowEdge(binV));

				TH1D* hproj;
				hproj=(TH1D*) hnDWYieldee->Projection(1);
				hproj->Scale(1/((m_Luminosity)*deltaU*deltaV*m_BF_Pi0to2Gam*m_BF_Pi0to2Gam),"width");
				string hname;
				hname="dsigmaeed"+axisNames[1]+key+"_"
							+axisNames[0]+axisNames[2]+"_"
							+MyGlobal::intToStr(binU-1)+"-"+MyGlobal::intToStr(binV-1);
				hproj->SetName(hname.c_str());
				string htitle;
				htitle=MyGlobal::doubleToStr(UMin,2)+"#leq "+ string(hnDWYieldee->GetAxis(0)->GetTitle())+"< "+MyGlobal::doubleToStr(UMax,2)+", "
							 +MyGlobal::doubleToStr(VMin,2)+"#leq "+ string(hnDWYieldee->GetAxis(2)->GetTitle())+"< "+MyGlobal::doubleToStr(VMax,2);
				hproj->SetTitle(htitle.c_str());
				string hXtitle;
				hXtitle=string(maphnDWYieldee[key]->GetAxis(1)->GetTitle());
				hproj->GetXaxis()->SetTitle(hXtitle.c_str());
				string hYtitle;
				hYtitle="d^{3}#sigma_{ee}/dQ^{2}d|cos #theta*|dW (pb/(GeV^{3}/c^{2}))";
				hproj->GetYaxis()->SetTitle(hYtitle.c_str());
				hproj->Write();

				hproj=(TH1D*) hnDWYieldgg->Projection(1);
				hproj->Scale(m_picoToNano/((m_Luminosity)*deltaU*deltaV*m_BF_Pi0to2Gam*m_BF_Pi0to2Gam),"width");
				hname="dsigmaggd"+axisNames[1]+key+"_"
							+axisNames[0]+axisNames[2]+"_"
							+MyGlobal::intToStr(binU-1)+"-"+MyGlobal::intToStr(binV-1);
				hproj->SetName(hname.c_str());
				htitle=MyGlobal::doubleToStr(UMin,2)+"#leq "+ string(hnDWYieldgg->GetAxis(0)->GetTitle())+"< "+MyGlobal::doubleToStr(UMax,2)+", "
							 +MyGlobal::doubleToStr(VMin,2)+"#leq "+ string(hnDWYieldgg->GetAxis(2)->GetTitle())+"< "+MyGlobal::doubleToStr(VMax,2);
				hproj->SetTitle(htitle.c_str());
				hXtitle=string(maphnDWYieldgg[key]->GetAxis(1)->GetTitle());
				hproj->GetXaxis()->SetTitle(hXtitle.c_str());
				hYtitle="d^{3}#sigma_{#gamma#gamma}/dQ^{2}d|cos #theta*|dW (nb/(GeV^{3}/c^{2}))";
				hproj->GetYaxis()->SetTitle(hYtitle.c_str());
				hproj->Write();

				if(key=="eT" || key=="pT"){
					THnSparseD* hnDEff_Restrict = (THnSparseD*) maphnDEff[key]->Clone();
					hnDEff_Restrict->GetAxis(0)->SetRange(binU,binU);
					hnDEff_Restrict->GetAxis(2)->SetRange(binV,binV);

					hproj=(TH1D*) hnDEff_Restrict->Projection(1);
					hname="Eff"+axisNames[1]+key+"_"
							+axisNames[0]+axisNames[2]+"_"
							+MyGlobal::intToStr(binU-1)+"-"+MyGlobal::intToStr(binV-1);
					hproj->SetName(hname.c_str());
					hproj->SetTitle(htitle.c_str());
					hproj->Write();
				}
			}
		}
		}

		//Write histograms
		maphnDWYieldee["eT"]->Write();
		maphnDWYieldee["pT"]->Write();
		maphnDWYieldee["All"]->Write();
		maphnDWYieldgg["eT"]->Write();
		maphnDWYieldgg["pT"]->Write();
		maphnDWYieldgg["All"]->Write();
		maphnDXSecgg["eT"]->Write();
		maphnDXSecgg["pT"]->Write();
		maphnDXSecgg["All"]->Write();

		//Write NTuple
		m_ntXSecgg->Write();
	}
	else{
		TFile* fileData;
		fileData = TFile::Open(m_DataSubtrFileName.c_str());
		if(fileData==NULL){cout << "****** CANNOT OPEN DATA FILE ******" <<endl;return;}
		fileData->ls();
		map<string, THnSparseD*> maphnDDataSubtr;
		maphnDDataSubtr["eT"]=(THnSparseD*)fileData->Get("hnDeTSubtr");
		maphnDDataSubtr["pT"]=(THnSparseD*)fileData->Get("hnDpTSubtr");

		for (std::map<string, THnSparseD*>::iterator it=maphnDDataSubtr.begin(); it!=maphnDDataSubtr.end(); ++it){
			if(it->second==NULL){
				cout << "CANNOT ACCESS DATA THN" << endl; return;
			}
		}

		TFile* fileEffRec;
		fileEffRec = TFile::Open(m_EffRecFileName.c_str());
		if(fileEffRec==NULL){cout << "****** CANNOT OPEN RECONSTRUCTION EFFICIENCY FILE ******" << endl; return;}
		fileEffRec->ls();
		map<string,THnSparseD*> maphnDGEN;
		maphnDGEN["eT"] =(THnSparseD*)fileEffRec->Get("hnDeTGEN");
		maphnDGEN["pT"] =(THnSparseD*)fileEffRec->Get("hnDpTGEN");

		map<string,THnSparseD*> maphnDREC;
		maphnDREC["eT"] =(THnSparseD*)fileEffRec->Get("hnDeTREC");
		maphnDREC["pT"] =(THnSparseD*)fileEffRec->Get("hnDpTREC");

		map<string,THnSparseD*> maphnDEff;
		maphnDEff["eT"] =(THnSparseD*)fileEffRec->Get("hnDeTEff");
		maphnDEff["pT"] =(THnSparseD*)fileEffRec->Get("hnDpTEff");

		for (std::map<string, THnSparseD*>::iterator it=maphnDEff.begin(); it!=maphnDEff.end(); ++it){
			if(it->second==NULL){
				cout << "CANNOT ACCESS Efficency THN" << endl; return;
			}
		}

		string outROOTname=outpath+".root";
		TFile* outROOT;
		outROOT = new TFile(outROOTname.c_str(),"RECREATE");

		map<string, THnSparseD*> maphnDWYield;
		for (std::map<string, THnSparseD*>::iterator it=maphnDDataSubtr.begin(); it!=maphnDDataSubtr.end(); ++it){
			string key = it->first;
			maphnDWYield[key] = (THnSparseD*) maphnDDataSubtr[key]->Clone();
		}

		const Int_t nDim=maphnDDataSubtr["eT"]->GetNdimensions();

		for (std::map<string, THnSparseD*>::iterator it=maphnDDataSubtr.begin(); it!=maphnDDataSubtr.end(); ++it){
			string key=it->first;
			THnSparseD* hnDEff = maphnDEff[key];

//			//Error calculation is wrong.
//			for(Long64_t bin=0;bin<maphnDDataSubtr[key]->GetNbins();bin++){
//				int bincoord[nDim];
//				double yield=maphnDDataSubtr[key]->GetBinContent(bin,bincoord);
//				double yieldErr=maphnDDataSubtr[key]->GetBinError(bincoord);
//
//				double effRec=maphnDEff[key]->GetBinContent(bincoord);
//				double effRecErr=maphnDEff[key]->GetBinError(bincoord);
//				printf("%d %d %d %f %f %f %f \n",bincoord[0],bincoord[1],bincoord[2],yield,yieldErr,effRec,effRecErr);
//
//				//Clear THn
//				maphnDWYield[key]->SetBinContent(bincoord,0);
//				maphnDWYield[key]->SetBinError(bincoord,0);
//
//				if(yield>0 && effRec==0) {cout << "THn Data filled, No efficiency" << endl;}
//				if(effRec==0)continue;
//				double WYield=yield/effRec;
//				double WYieldErr=(1/pow(effRec,2))*pow(yieldErr,2)+(pow(yield,2)*pow(effRecErr,2))/(pow(effRec,4));
//				maphnDWYield[key]->SetBinContent(bincoord,WYield);
//				maphnDWYield[key]->SetBinError(bincoord,WYield);
//			}

			maphnDWYield[key]->Divide(maphnDDataSubtr[key],maphnDEff[key]);
		}

		maphnDWYield["All"]=(THnSparseD*)maphnDWYield["eT"]->Clone();
		maphnDWYield["All"]->Add(maphnDWYield["pT"]);
		maphnDWYield["All"]->SetName("hnDWYield");

		maphnDDataSubtr["All"]=(THnSparseD*)maphnDDataSubtr["eT"]->Clone();
		maphnDDataSubtr["All"]->Add(maphnDDataSubtr["pT"]);
		maphnDDataSubtr["All"]->SetName("hnDSubtr");

		maphnDREC["All"]=(THnSparseD*)maphnDREC["eT"]->Clone();
		maphnDREC["All"]->Add(maphnDREC["pT"]);
		maphnDREC["All"]->SetName("hnDREC");

		vector<string> axisNames;
		map<int,double> mapBinMins;
		map<int,double> mapBinMaxs;

		for(int i=0;i<maphnDWYield["All"]->GetNdimensions();i++){
		cout << maphnDWYield["All"]->GetAxis(i)->GetName() << endl;
		axisNames.push_back(maphnDWYield["All"]->GetAxis(i)->GetName());
		//string key=axisNames.back();
		int key=i;
		mapBinMins[key]=maphnDWYield["All"]->GetAxis(i)->GetFirst();
		mapBinMaxs[key]=maphnDWYield["All"]->GetAxis(i)->GetLast();
		}


		outROOT->cd();
		//XSec Abs(cos Theta Star)  in Q2,M2Pi bins
		for (std::map<string, THnSparseD*>::iterator it=maphnDWYield.begin(); it!=maphnDWYield.end(); ++it){
		string key = it->first;
		for(int binU=mapBinMins[0];binU<=mapBinMaxs[0];binU++){
			for(int binV=mapBinMins[1];binV<=mapBinMaxs[1];binV++){
				string UName; UName=string(maphnDWYield[key]->GetAxis(0)->GetName());
				string VName; VName=string(maphnDWYield[key]->GetAxis(1)->GetName());

				string UTitle; UTitle=string(maphnDWYield[key]->GetAxis(0)->GetTitle());
				string VTitle; VTitle=string(maphnDWYield[key]->GetAxis(1)->GetTitle());

				THnSparseD* hnD_Restrict = (THnSparseD*) maphnDWYield[key]->Clone();
				hnD_Restrict->GetAxis(0)->SetRange(binU,binU);
				hnD_Restrict->GetAxis(1)->SetRange(binV,binV);

				THnSparseD* hnDData_Restrict = (THnSparseD*) maphnDDataSubtr[key]->Clone();
				hnDData_Restrict->GetAxis(0)->SetRange(binU,binU);
				hnDData_Restrict->GetAxis(1)->SetRange(binV,binV);

				double UMin=hnD_Restrict->GetAxis(0)->GetBinLowEdge(binU);
				double UMax=hnD_Restrict->GetAxis(0)->GetBinUpEdge(binU);
				double deltaU=(hnD_Restrict->GetAxis(0)->GetBinUpEdge(binU))
											-(hnD_Restrict->GetAxis(0)->GetBinLowEdge(binU));
				double VMin=hnD_Restrict->GetAxis(1)->GetBinLowEdge(binV);
				double VMax=hnD_Restrict->GetAxis(1)->GetBinUpEdge(binV);
				double deltaV=(hnD_Restrict->GetAxis(1)->GetBinUpEdge(binV))
											-(hnD_Restrict->GetAxis(1)->GetBinLowEdge(binV));

				TH1D* hproj=(TH1D*) hnD_Restrict->Projection(2);
				hproj->Scale(1/((m_Luminosity)*deltaU*deltaV*m_BF_Pi0to2Gam*m_BF_Pi0to2Gam),"width");
				string hname;
				hname="dsigmaeed"+axisNames[2]+key+"_"
							+axisNames[0]+axisNames[1]+"_"
							+MyGlobal::intToStr(binU-1)+"-"+MyGlobal::intToStr(binV-1);
				hproj->SetName(hname.c_str());
				string htitle;
				htitle=MyGlobal::doubleToStr(UMin,2)+"#leq "+ string(hnD_Restrict->GetAxis(0)->GetTitle())+"< "+MyGlobal::doubleToStr(UMax,2)+", "
							 +MyGlobal::doubleToStr(VMin,2)+"#leq "+ string(hnD_Restrict->GetAxis(1)->GetTitle())+"< "+MyGlobal::doubleToStr(VMax,2);
				hproj->SetTitle(htitle.c_str());
				string hXtitle;
				hXtitle=string(maphnDWYield[key]->GetAxis(2)->GetTitle());
				hproj->GetXaxis()->SetTitle(hXtitle.c_str());
				string hYtitle;
				hYtitle="d^{3}#sigma_{ee}/dQ^{2}d|cos #theta*|dW (pb/(GeV^{3}/c^{2}))";
				hproj->GetYaxis()->SetTitle(hYtitle.c_str());
				hproj->Write();

				hproj=(TH1D*) hnDData_Restrict->Projection(2);
				hname=axisNames[2]+key+"_"
							+axisNames[0]+axisNames[1]+"_"
							+MyGlobal::intToStr(binU-1)+"-"+MyGlobal::intToStr(binV-1);
				hproj->SetName(hname.c_str());
				hproj->SetTitle(htitle.c_str());
				hproj->Write();
			}
		}
		}

//		//M2Pi distribution
//		for (std::map<string, THnSparseD*>::iterator it=maphnDWYield.begin(); it!=maphnDWYield.end(); ++it){
//			string key = it->first;
//			THnSparseD* hnD_Restrict = (THnSparseD*) maphnDWYield[key]->Clone();
//			TH1D* hproj;
//			hproj=(TH1D*) hnD_Restrict->Projection(1);
//			hproj->Scale(1/((m_Luminosity)*m_BF_Pi0to2Gam*m_BF_Pi0to2Gam),"width");
//			string hname;
//				hname="dsigmaeed"+axisNames[1]+key;
//				hproj->SetName(hname.c_str());
//				string htitle("");
//				hproj->SetTitle(htitle.c_str());
//				string hXtitle;
//				hXtitle=string(maphnDWYield[key]->GetAxis(1)->GetTitle());
//				hproj->GetXaxis()->SetTitle(hXtitle.c_str());
//				string hYtitle;
//				hYtitle="d#sigma_{ee}/dW (pb/(GeV/c^{2}))";
//				hproj->GetYaxis()->SetTitle(hYtitle.c_str());
//				hproj->Write();
//		}
//
//		//M2Pi distributions  in Q2,Abs(cos Theta Star) bins
//		for (std::map<string, THnSparseD*>::iterator it=maphnDWYield.begin(); it!=maphnDWYield.end(); ++it){
//		string key = it->first;
//		for(int binU=mapBinMins[0];binU<=mapBinMaxs[0];binU++){
//			for(int binV=mapBinMins[2];binV<=mapBinMaxs[2];binV++){
//				string UName; UName=string(maphnDWYield[key]->GetAxis(0)->GetName());
//				string VName; VName=string(maphnDWYield[key]->GetAxis(2)->GetName());
//
//				string UTitle; UTitle=string(maphnDWYield[key]->GetAxis(0)->GetTitle());
//				string VTitle; VTitle=string(maphnDWYield[key]->GetAxis(2)->GetTitle());
//
//				THnSparseD* hnD_Restrict = (THnSparseD*) maphnDWYield[key]->Clone();
//				hnD_Restrict->GetAxis(0)->SetRange(binU,binU);
//				hnD_Restrict->GetAxis(2)->SetRange(binV,binV);
//
//				THnSparseD* hnDData_Restrict = (THnSparseD*) maphnDDataSubtr[key]->Clone();
//				hnDData_Restrict->GetAxis(0)->SetRange(binU,binU);
//				hnDData_Restrict->GetAxis(2)->SetRange(binV,binV);
//
//				double UMin=hnD_Restrict->GetAxis(0)->GetBinLowEdge(binU);
//				double UMax=hnD_Restrict->GetAxis(0)->GetBinUpEdge(binU);
//				double deltaU=(hnD_Restrict->GetAxis(0)->GetBinUpEdge(binU))
//											-(hnD_Restrict->GetAxis(0)->GetBinLowEdge(binU));
//				double VMin=hnD_Restrict->GetAxis(2)->GetBinLowEdge(binV);
//				double VMax=hnD_Restrict->GetAxis(2)->GetBinUpEdge(binV);
//				double deltaV=(hnD_Restrict->GetAxis(2)->GetBinUpEdge(binV))
//											-(hnD_Restrict->GetAxis(2)->GetBinLowEdge(binV));
//
//				TH1D* hproj;
//				hproj=(TH1D*) hnD_Restrict->Projection(1);
//				hproj->Scale(1/((m_Luminosity)*deltaU*deltaV*m_BF_Pi0to2Gam*m_BF_Pi0to2Gam),"width");
//				string hname;
//				hname="dsigmaeed"+axisNames[1]+key+"_"
//							+axisNames[0]+axisNames[2]+"_"
//							+MyGlobal::intToStr(binU-1)+"-"+MyGlobal::intToStr(binV-1);
//				hproj->SetName(hname.c_str());
//				string htitle;
//				htitle=MyGlobal::doubleToStr(UMin,2)+"#leq "+ string(hnD_Restrict->GetAxis(0)->GetTitle())+"< "+MyGlobal::doubleToStr(UMax,2)+", "
//							 +MyGlobal::doubleToStr(VMin,2)+"#leq "+ string(hnD_Restrict->GetAxis(2)->GetTitle())+"< "+MyGlobal::doubleToStr(VMax,2);
//				hproj->SetTitle(htitle.c_str());
//				string hXtitle;
//				hXtitle=string(maphnDWYield[key]->GetAxis(1)->GetTitle());
//				hproj->GetXaxis()->SetTitle(hXtitle.c_str());
//				string hYtitle;
//				hYtitle="d^{3}#sigma_{ee}/dQ^{2}d|cos #theta*|dW (pb/(GeV^{3}/c^{2}))";
//				hproj->GetYaxis()->SetTitle(hYtitle.c_str());
//				hproj->Write();
//
//				hproj=(TH1D*) hnDData_Restrict->Projection(1);
//				hname=axisNames[1]+key+"_"
//							+axisNames[0]+axisNames[2]+"_"
//							+MyGlobal::intToStr(binU-1)+"-"+MyGlobal::intToStr(binV-1);
//				hproj->SetName(hname.c_str());
//				hproj->SetTitle(htitle.c_str());
//				hproj->Write();
//
//				if(key=="eT" || key=="pT"){
//					THnSparseD* hnDEff_Restrict = (THnSparseD*) maphnDEff[key]->Clone();
//					hnDEff_Restrict->GetAxis(0)->SetRange(binU,binU);
//					hnDEff_Restrict->GetAxis(2)->SetRange(binV,binV);
//
//					hproj=(TH1D*) hnDEff_Restrict->Projection(1);
//					hname="Eff"+axisNames[1]+key+"_"
//							+axisNames[0]+axisNames[2]+"_"
//							+MyGlobal::intToStr(binU-1)+"-"+MyGlobal::intToStr(binV-1);
//					hproj->SetName(hname.c_str());
//					hproj->SetTitle(htitle.c_str());
//					hproj->Write();
//				}
//			}
//		}
//		}

		maphnDDataSubtr["eT"]->Write();
		maphnDDataSubtr["pT"]->Write();
		maphnDDataSubtr["All"]->Write();
		maphnDGEN["eT"]->Write();
		maphnDGEN["pT"]->Write();
		maphnDREC["eT"]->Write();
		maphnDREC["pT"]->Write();
		maphnDREC["All"]->Write();
		maphnDEff["eT"]->Write();
		maphnDEff["pT"]->Write();
		maphnDWYield["All"]->Write();
	}
	return;

	/*
	ifstream readInput(inputcard.c_str());

		if(readInput.is_open()){
			while(readInput.good()){
				string strline;
				getline(readInput,strline);

				vector<string> strsplit = MyGlobal::tokenize(strline,' ');
				for(int k=0;k<strsplit.size();k++){
					vector<string> vecstr = MyGlobal::tokenize(strsplit[k],'=');
					string arg = vecstr[0];
					string val = vecstr[1];
					if(arg=="Luminosity") m_Luminosity=atof(val.c_str());
					if(arg=="CalcMode") 	m_CalcMode=atoi(val.c_str());
					if(arg=="DataNTuple") m_DataNTFileName=val;
					if(arg=="DataSubtr") m_DataSubtrFileName=val;
					if(arg=="BgndFrac") m_BgndFracFileName=val;
					if(arg=="EffRec") m_EffRecFileName=val;
					if(arg=="LumGamGam") m_LumGamGamFileName=val;
				}
			}

		}

		if(m_CalcMode==0){
				//Data NTuple
				TFile* fileData;
				fileData = TFile::Open(m_DataNTFileName.c_str());
				if(fileData==NULL){cout << "****** CANNOT OPEN DATA FILE ******" <<endl;return;}
				TTree* treeData = (TTree*) fileData->Get("Tagged");
				if(treeData==NULL){cout << "****** CANNOT NTUPLE DATA FILE ******" <<endl;return;}
				double Q2,W,CThetaSt,absCThetaSt,PhiStarPi01;
				int QTag;

				if(m_UseFittedKinematics==0){
					treeData->SetBranchAddress("Q2",&Q2);
					treeData->SetBranchAddress("2Pi0InvM", &W);
					treeData->SetBranchAddress("cosThetaStarPi01", &CThetaSt);
					treeData->SetBranchAddress("QTag", &QTag);
					treeData->SetBranchAddress("PhiStarPi01", &PhiStarPi01);
				}
				else{
					treeData->SetBranchAddress("Q2Fit",&Q2);
					treeData->SetBranchAddress("2Pi0InvMFit", &W);
					treeData->SetBranchAddress("cosThetaStarPi0Fit1", &CThetaSt);
					treeData->SetBranchAddress("QTag", &QTag);
					treeData->SetBranchAddress("PhiStarPi0Fit1", &PhiStarPi01);
				}
				//Reconstruction efficiency
				TFile* fileEffRec;
				fileEffRec = TFile::Open(m_EffRecFileName.c_str());
				if(fileEffRec==NULL){cout << "****** CANNOT OPEN RECONSTRUCTION EFFICIENCY FILE ******" << endl; return;}
				fileEffRec->ls();
				map<string,THnSparseD*> maphnDGEN;
				maphnDGEN["eT"] =(THnSparseD*)fileEffRec->Get("hnDeTGEN");
				maphnDGEN["pT"] =(THnSparseD*)fileEffRec->Get("hnDpTGEN");

				map<string,THnSparseD*> maphnDREC;
				maphnDREC["eT"] =(THnSparseD*)fileEffRec->Get("hnDeTREC");
				maphnDREC["pT"] =(THnSparseD*)fileEffRec->Get("hnDpTREC");

				map<string,THnSparseD*> maphnDEff;
				maphnDEff["eT"] =(THnSparseD*)fileEffRec->Get("hnDeTEff");
				maphnDEff["pT"] =(THnSparseD*)fileEffRec->Get("hnDpTEff");

				for (std::map<string, THnSparseD*>::iterator it=maphnDEff.begin(); it!=maphnDEff.end(); ++it){
					if(it->second==NULL){
						cout << "CANNOT ACCESS Efficency THN" << endl; return;
					}
				}

				//Background fraction
				TFile* fileBgndFrac;
				fileBgndFrac = TFile::Open(m_BgndFracFileName.c_str());
				map<string, THnSparseD*> maphnDBgndFrac;
				maphnDBgndFrac["eT"]=(THnSparseD*)fileBgndFrac->Get("hnDeTSumBgndRatio");
				maphnDBgndFrac["pT"]=(THnSparseD*)fileBgndFrac->Get("hnDpTSumBgndRatio");
				for (std::map<string, THnSparseD*>::iterator it=maphnDBgndFrac.begin(); it!=maphnDBgndFrac.end(); ++it){
					if(it->second==NULL){
						cout << "CANNOT ACCESS Background fraction THN" << endl; return;
					}
				}

				const Int_t nDim = maphnDBgndFrac["eT"]->GetNdimensions();

				//Data yield
				map<string, THnSparseD*> maphnDYieldee;
				maphnDYieldee["eT"] = (THnSparseD*) maphnDBgndFrac["eT"]->Clone();
				maphnDYieldee["pT"] = (THnSparseD*) maphnDBgndFrac["pT"]->Clone();
				for (std::map<string, THnSparseD*>::iterator it=maphnDYieldee.begin(); it!=maphnDYieldee.end(); ++it){
					string histoname;
					histoname="hnD"+(it->first)+"Yieldee";
					(it->second)->SetName(histoname.c_str());
					(it->second)->Reset();
					(it->second)->Sumw2();
				}

				//Efficiency corrected yield
				map<string, THnSparseD*> maphnDWYieldee;
				maphnDWYieldee["eT"] = (THnSparseD*) maphnDBgndFrac["eT"]->Clone();
				maphnDWYieldee["pT"] = (THnSparseD*) maphnDBgndFrac["pT"]->Clone();
				for (std::map<string, THnSparseD*>::iterator it=maphnDWYieldee.begin(); it!=maphnDWYieldee.end(); ++it){
					string histoname;
					histoname="hnD"+(it->first)+"WYieldee";
					(it->second)->SetName(histoname.c_str());
					(it->second)->Reset();
					(it->second)->Sumw2();
				}

				map<string, THnSparseD*> maphnDWYieldLgg;
				maphnDWYieldLgg["eT"] = (THnSparseD*) maphnDBgndFrac["eT"]->Clone();
				maphnDWYieldLgg["pT"] = (THnSparseD*) maphnDBgndFrac["pT"]->Clone();
				for (std::map<string, THnSparseD*>::iterator it=maphnDWYieldLgg.begin(); it!=maphnDWYieldLgg.end(); ++it){
					string histoname;
					histoname="hnD"+(it->first)+"WYieldLgg";
					(it->second)->SetName(histoname.c_str());
					(it->second)->Reset();
					(it->second)->Sumw2();
				}

				map<string, THnSparseD*> maphnDWYieldgg;
				maphnDWYieldgg["eT"] = (THnSparseD*) maphnDBgndFrac["eT"]->Clone();
				maphnDWYieldgg["pT"] = (THnSparseD*) maphnDBgndFrac["pT"]->Clone();
				for (std::map<string, THnSparseD*>::iterator it=maphnDWYieldgg.begin(); it!=maphnDWYieldgg.end(); ++it){
					string histoname;
					histoname="hnD"+(it->first)+"WYieldgg";
					(it->second)->SetName(histoname.c_str());
					(it->second)->Reset();
					(it->second)->Sumw2();
				}

				//Gamma Gamma luminosity function
				TFile* fileLGamGam = TFile::Open(m_LumGamGamFileName.c_str());
				if(fileLGamGam==NULL){cout << "****** CANNOT OPEN GAMMA GAMMA LUMINOSITY FILE ******" << endl; return;}
				TGraph2D* grLGamGam = (TGraph2D*) fileLGamGam->Get("gr2D");

				string outROOTname=outpath+".root";
				TFile* outROOT;
				outROOT = new TFile(outROOTname.c_str(),"RECREATE");

				//Fill weighted yield histograms
				cout << treeData->GetEntries() << endl; sleep(2);
				for(int entry=0;entry<treeData->GetEntries();entry++){
					if (entry % 1 == 0) cout << "Event counter = " << entry << endl;

					treeData->GetEntry(entry);
					double absCThetaSt = fabs(CThetaSt);

					vector<double> binval; binval.resize(nDim);
					cout << "binval.size() " << binval.size() << endl;
					if(nDim==3){binval[0]=Q2;binval[1]=W;binval[2]=absCThetaSt;}
					if(nDim==4){binval[0]=Q2;binval[1]=W;binval[2]=absCThetaSt;binval[3]=MyGlobal::getPhiStar(PhiStarPi01);}

					double weight=0;
					double fracbgnd=0;
					double eff=0;
					double errEff=0;
					double lgg=0;

					string key;
					cout << "QTag " << QTag << endl;
					if(QTag==-1){
						key="eT";
					}
					else if(QTag==1){
						key="pT";
					}
					else continue;
					cout << key << endl;




				}

			}

			return;
			*/
}
