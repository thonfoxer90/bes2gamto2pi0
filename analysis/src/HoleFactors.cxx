/*
 * HoleFactors.cxx
 *
 *  Created on: Feb 9, 2018
 *      Author: bgarillo
 */

#include "HoleFactors.h"

HoleFactors::HoleFactors() {
	// TODO Auto-generated constructor stub

}

void HoleFactors::calcHoleFactors1D(int varAxis,vector<int> binnedAxes){
	if(binnedAxes.size()==2){
			//Integrate over 3 variables
		for(std::map<string, THnSparseD*>::iterator it=m_maphnDRecCorr.begin(); it!=m_maphnDRecCorr.end(); ++it){
			string key= it->first;
			THnSparse* hnD = it->second;
			cout <<key<<endl;
			for(int binU=hnD->GetAxis(binnedAxes[0])->GetFirst();binU<=hnD->GetAxis(binnedAxes[0])->GetLast();binU++){
				for(int binV=hnD->GetAxis(binnedAxes[1])->GetFirst();binV<=hnD->GetAxis(binnedAxes[1])->GetLast();binV++){
					string UName; UName=string(hnD->GetAxis(binnedAxes[0])->GetName());
					string VName; VName=string(hnD->GetAxis(binnedAxes[1])->GetName());

					string UTitle; UTitle=string(hnD->GetAxis(binnedAxes[0])->GetTitle());
					string VTitle; VTitle=string(hnD->GetAxis(binnedAxes[1])->GetTitle());

					THnSparseD* hnD_Restrict = (THnSparseD*) hnD->Clone();
					hnD_Restrict->GetAxis(binnedAxes[0])->SetRange(binU,binU);
					hnD_Restrict->GetAxis(binnedAxes[1])->SetRange(binV,binV);

					THnSparseD* hnDGEN_Restrict = (THnSparseD*) m_maphnDGEN[key]->Clone();
					hnDGEN_Restrict->GetAxis(binnedAxes[0])->SetRange(binU,binU);
					hnDGEN_Restrict->GetAxis(binnedAxes[1])->SetRange(binV,binV);

					double UMin=hnD->GetAxis(binnedAxes[0])->GetBinLowEdge(binU);
					double UMax=hnD->GetAxis(binnedAxes[0])->GetBinUpEdge(binU);
					double deltaU=(hnD->GetAxis(binnedAxes[0])->GetBinUpEdge(binU))
																-(hnD->GetAxis(0)->GetBinLowEdge(binU));
					double VMin=hnD->GetAxis(binnedAxes[1])->GetBinLowEdge(binV);
					double VMax=hnD->GetAxis(binnedAxes[1])->GetBinUpEdge(binV);
					double deltaV=(hnD->GetAxis(binnedAxes[1])->GetBinUpEdge(binV))
																-(hnD->GetAxis(1)->GetBinLowEdge(binV));

					TH1D* hCorr=(TH1D*) hnD_Restrict->Projection(varAxis);
					TH1D* hGEN=(TH1D*) hnDGEN_Restrict->Projection(varAxis);
					hCorr->Divide(hGEN);
					hCorr->SetMinimum(0);
					string hname;
					hname="HF"+string(hnD->GetAxis(varAxis)->GetName())+key+"_"
							+string(hnD->GetAxis(binnedAxes[0])->GetName())
							+string(hnD->GetAxis(binnedAxes[1])->GetName())+"_"
							+MyGlobal::intToStr(binU-1)+"-"+
							MyGlobal::intToStr(binV-1)
							;
					hCorr->SetName(hname.c_str());
					string htitle;
					htitle=MyGlobal::doubleToStr(UMin,2)+"#leq "+ string(hnD->GetAxis(binnedAxes[0])->GetTitle())+"< "+MyGlobal::doubleToStr(UMax,2)+", "
												 +MyGlobal::doubleToStr(VMin,2)+"#leq "+ string(hnD->GetAxis(binnedAxes[1])->GetTitle())+"< "+MyGlobal::doubleToStr(VMax,2);
					hCorr->SetTitle(htitle.c_str());
					hCorr->GetXaxis()->SetTitle(hnD->GetAxis(varAxis)->GetTitle());
					hCorr->GetYaxis()->SetTitle("#varepsilon_{HF}");
					hCorr->Write();

					delete hCorr;
					delete hGEN;
					delete hnD_Restrict;
					delete hnDGEN_Restrict;
				}
			}
		}
	}

}
HoleFactors::HoleFactors(string nameEffRec, string option, string outpath):
		m_nameEffRec(nameEffRec),m_option(option),m_outpath(outpath){
	// TODO Auto-generated constructor stub

	//Reconstruction efficiency
	m_fileEffRec = TFile::Open(m_nameEffRec.c_str());
	if(m_fileEffRec==NULL){cout << "****** CANNOT OPEN RECONSTRUCTION EFFICIENCY FILE ******" << endl; return;}
		m_fileEffRec->ls();

		m_maphnDGEN["eT"] =(THnSparseD*)m_fileEffRec->Get("hnDeTGEN");
		m_maphnDGEN["pT"] =(THnSparseD*)m_fileEffRec->Get("hnDpTGEN");
		m_maphnDGEN["All"] =(THnSparseD*)m_maphnDGEN["eT"]->Clone();
		m_maphnDGEN["All"]->Reset();
		m_maphnDGEN["All"]->Sumw2();
		m_maphnDGEN["All"]->Add(m_maphnDGEN["eT"]);m_maphnDGEN["All"]->Add(m_maphnDGEN["pT"]);

		m_maphnDREC["eT"] =(THnSparseD*)m_fileEffRec->Get("hnDeTREC");
		m_maphnDREC["pT"] =(THnSparseD*)m_fileEffRec->Get("hnDpTREC");

		m_maphnDEff["eT"] =(THnSparseD*)m_fileEffRec->Get("hnDeTEff");
		m_maphnDEff["pT"] =(THnSparseD*)m_fileEffRec->Get("hnDpTEff");

		for (std::map<string, THnSparseD*>::iterator it=m_maphnDEff.begin(); it!=m_maphnDEff.end(); ++it){
			if(it->second==NULL){
				cout << "CANNOT ACCESS Efficency THN" << endl; return;
			}
		}
		m_nDim=m_maphnDEff["eT"]->GetNdimensions();
		for(std::map<string, THnSparseD*>::iterator it=m_maphnDREC.begin(); it!=m_maphnDREC.end(); ++it){
			string key = it->first;
			m_maphnDRecCorr[key] = (THnSparseD*)m_maphnDREC[key]->Clone();
			m_maphnDRecCorr[key]->SetName((string("hnD")+key+string("RECCorr")).c_str());
		}
		m_maphnDRecCorr["All"] = (THnSparseD*)m_maphnDREC["eT"]->Clone();
		m_maphnDRecCorr["All"]->Reset();
		m_maphnDRecCorr["All"]->SetName((string("hnD")+string("All")+string("RECCorr")).c_str());
		m_maphnDRecCorr["All"]->Sumw2();

		string outname=outpath+".root";
		TFile* m_outROOT = new TFile(outname.c_str(),"RECREATE");
}

HoleFactors::~HoleFactors() {
	// TODO Auto-generated destructor stub
}

void HoleFactors::run(){
	for(std::map<string, THnSparseD*>::iterator it=m_maphnDRecCorr.begin(); it!=m_maphnDRecCorr.end(); ++it){
		string key = it->first;
		if(key=="All") continue;
		m_maphnDRecCorr[key]->Divide(m_maphnDEff[key]);
		m_maphnDRecCorr["All"]->Add(m_maphnDRecCorr[key]);
	}

	vector<int> binnedAxes;
	binnedAxes.resize(2);
	binnedAxes[0]=0;binnedAxes[1]=1; //Q2,W
	int varAxis=2; //cos theta*
	calcHoleFactors1D(varAxis,binnedAxes);

	for(std::map<string, THnSparseD*>::iterator it=m_maphnDRecCorr.begin(); it!=m_maphnDRecCorr.end(); ++it){
		string key = it->first;
		m_maphnDRecCorr[key]->Write();
	}
}
