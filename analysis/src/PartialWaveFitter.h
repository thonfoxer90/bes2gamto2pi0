/*
 * PartialWaveFitter.h
 *
 *  Created on: Apr 23, 2018
 *      Author: bgarillo
 */

#ifndef PARTIALWAVEFITTER_H_
#define PARTIALWAVEFITTER_H_

#include <iostream>             // std::cout, std::endl
#include <fstream>              // std::ifstream
#include <sstream>
#include <cstdlib>
#include <map>
#include <time.h>

#include <TROOT.h>
#include <TSystem.h>
#include <TObject.h>
#include <TRint.h>
#include <TString.h>
#include <TTree.h>
#include <TChain.h>
#include <TFile.h>
#include <TKey.h>
#include <TList.h>
#include <TStyle.h>
#include <TSelector.h>
#include <TF1.h>
#include <TH2.h>
#include <TH1.h>
#include <THnSparse.h>
#include <TGraph2D.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TMultiGraph.h>
#include <TVirtualIndex.h>
#include <TPad.h>
#include <TCanvas.h>
#include <TAxis.h>
#include <TGaxis.h>
#include <TPave.h>
#include <TPaveText.h>
#include <TLatex.h>
#include <TVector3.h>
#include <TLorentzVector.h>

#include "RooAbsPdf.h"
#include "RooGlobalFunc.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooConstVar.h"
#include "RooAddPdf.h"
#include "RooGaussian.h"
#include "RooCBShape.h"
#include "RooArgusBG.h"
#include "RooPlot.h"
#include "RooSpHarmonic.h"

#include "MyPdfV3.h"

#include "MyGlobal.h"

using namespace std;

class PartialWaveFitter {
public:
	PartialWaveFitter();
	PartialWaveFitter(string input, string option, string outpath);
	virtual ~PartialWaveFitter();
	void parseInput();
	RooDataHist* getRooDataHist(THnSparseD* hnD);
	void run();
private:
	string m_inputcard;
	string m_inputpath;
	string m_option;
	string m_outpath;

	TFile* m_infile;
	TFile* m_outfile;

	THnSparseD* m_hnDXSec;
	RooDataHist* m_DataHist;

	bool m_TestRun;
	RooRealVar m_a;
	RooRealVar m_b;
	RooRealVar m_y;
	MyPdfV3 m_pdf;
};

#endif /* PARTIALWAVEFITTER_H_ */
