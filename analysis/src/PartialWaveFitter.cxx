/*
 * PartialWaveFitter.cxx
 *
 *  Created on: Apr 23, 2018
 *      Author: bgarillo
 */

#include "PartialWaveFitter.h"

PartialWaveFitter::PartialWaveFitter():
m_TestRun(true),m_a("a","a",1),m_b("b","b",2,-10,10),m_y("y","y",-10,10),
m_pdf("pdf","pdf",m_y,m_a,m_b)
{
	// TODO Auto-generated constructor stub

}

PartialWaveFitter::PartialWaveFitter(string input, string option, string outpath):
	m_TestRun(false),m_inputcard(input),m_option(option), m_outpath(outpath)
{
	parseInput();
	m_infile = TFile::Open(m_inputpath.c_str());
	m_hnDXSec = (THnSparseD*) m_infile->Get("hnDXSecgg");


}

PartialWaveFitter::~PartialWaveFitter() {
	// TODO Auto-generated destructor stub
}

void PartialWaveFitter::parseInput(){
	ifstream readFile(m_inputcard.c_str());
	if(readFile.is_open()){
		while(readFile.good()){
			string strline;
			getline(readFile,strline);
			if(strline.find("Input") != string::npos){
				vector <string> strsplit=MyGlobal::tokenize(strline,'=');
				m_inputpath=strsplit.back();
			}
		}
	}
}

RooDataHist* getRooDataHist(THnSparseD* hnD){
	RooDataHist* dh;
	if(hnD->GetNdimensions()==3){
		RooRealVar Q2(hnD->GetAxis(0)->GetName(),hnD->GetAxis(0)->GetTitle(),hnD->GetAxis(0)->GetXmin(),hnD->GetAxis(0)->GetXmax(),"GeV^{2}");
		RooRealVar W(hnD->GetAxis(1)->GetName(),hnD->GetAxis(1)->GetTitle(),hnD->GetAxis(1)->GetXmin(),hnD->GetAxis(1)->GetXmax(),"GeV");
		RooRealVar cosThetast(hnD->GetAxis(2)->GetName(),hnD->GetAxis(2)->GetTitle(),hnD->GetAxis(2)->GetXmin(),hnD->GetAxis(2)->GetXmax(),"");
		//char name[256];
		//char title[256];
		//Double_t val;
		//sprintf(name,"name");
		//sprintf(title,"title");
		//val=5;
		//RooRealVar mean(name,title,val);
		TH3D* h3D = hnD->Projection(0,1,2,"E");
		dh = new RooDataHist("name","title",RooArgSet(Q2,W,cosThetast),(TH1D*)h3D);
	}
	return dh;
}
void PartialWaveFitter::run(){
	if(m_TestRun){
		// Generate toy data from pdf and plot data and p.d.f on frame
		RooPlot* frame1 = m_y.frame() ;
		RooDataSet* data = m_pdf.generate(m_y,1000) ;
		m_pdf.fitTo(*data) ;
		data->plotOn(frame1) ;
		m_pdf.plotOn(frame1) ;

		TCanvas* c = new TCanvas("rf104_classfactory","rf104_classfactory",800,400) ;
		frame1->Draw() ;
		c->Print("test.pdf");

	}
	else{

	}
}
