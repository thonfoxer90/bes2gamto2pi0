/*
 * HoleFactors.h
 *
 *  Created on: Feb 9, 2018
 *      Author: bgarillo
 */

#ifndef HOLEFACTORS_H_
#define HOLEFACTORS_H_

#include <iostream>             // std::cout, std::endl
#include <fstream>              // std::ifstream
#include <sstream>
#include <cstdlib>
#include <map>
#include <time.h>

#include <TROOT.h>
#include <TSystem.h>
#include <TObject.h>
#include <TRint.h>
#include <TString.h>
#include <TTree.h>
#include <TChain.h>
#include <TFile.h>
#include <TKey.h>
#include <TList.h>
#include <TStyle.h>
#include <TSelector.h>
#include <TF1.h>
#include <TH2.h>
#include <TH1.h>
#include <THnSparse.h>
#include <TGraph2D.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TMultiGraph.h>
#include <TVirtualIndex.h>
#include <TPad.h>
#include <TCanvas.h>
#include <TGaxis.h>
#include <TPave.h>
#include <TPaveText.h>
#include <TLatex.h>
#include <TVector3.h>
#include <TLorentzVector.h>


#include "RooGlobalFunc.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooConstVar.h"
#include "RooAddPdf.h"
#include "RooGaussian.h"
#include "RooCBShape.h"
#include "RooArgusBG.h"
#include "RooPlot.h"

#include "MyGlobal.h"

class HoleFactors {
public:
	HoleFactors();
	HoleFactors(string nameEffRec, string option, string outpath);
	virtual ~HoleFactors();
	void run();
	void calcHoleFactors1D(int varAxis,vector<int> binnedAxes);
private:
	string m_nameEffRec;
	string m_nameNTFile;
	string m_option;
	string m_outpath;

	int m_nDim;

	int calcMode; //0 : THnSparse division 1: Event-by-event correction

	TFile* m_fileEffRec;
	TFile* m_fileMCtoCorr;
	TFile* m_outROOT;

	map<string,THnSparseD*> m_maphnDGEN; //0: e-Tagged 1: e+Tagged
	map<string,THnSparseD*> m_maphnDREC; //0: e-Tagged 1: e+Tagged
	map<string,THnSparseD*> m_maphnDEff; //0: e-Tagged 1: e+Tagged
	map<string,THnSparseD*> m_maphnDRecCorr; //0: e-Tagged 1: e+Tagged

};

#endif /* HOLEFACTORS_H_ */
