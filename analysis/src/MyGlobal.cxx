#include "MyGlobal.h"

using namespace std;

double MyGlobal::phi0to2Pi(double phi){
	double newphi;
	if(phi<0.) newphi=2*TMath::Pi()+phi;
	else newphi=phi;
	return newphi;
}

double MyGlobal::phiminPitominPi(double phi){
	double newphi;
	if(phi>TMath::Pi()) newphi=phi-2*TMath::Pi();
	else newphi=phi;
	return newphi;
}

vector <string> MyGlobal::tokenize(string str, char delim){
	vector <string> strsplit;

	stringstream ss;
	string strtmp;

	ss.str(str);				
		while (getline(ss, strtmp, delim)) {
 		strsplit.push_back(strtmp);
		}	

	return strsplit;		
}

vector<string> MyGlobal::tokenizeArgs(string command){
	vector<string> qargs;
  int len = command.length();
  bool qot = false, sqot = false;
  int arglen;
  for(int i = 0; i < len; i++) {
          int start = i;
          if(command[i] == '\"') {
                  qot = true;
          }
          else if(command[i] == '\'') sqot = true;

          if(qot) {
                  i++;
                  start++;
                  while(i<len && command[i] != '\"')
                          i++;
                  if(i<len)
                          qot = false;
                  arglen = i-start;
                  i++;
          }
          else if(sqot) {
                  i++;
                  while(i<len && command[i] != '\'')
                          i++;
                  if(i<len)
                          sqot = false;
                  arglen = i-start;
                  i++;
          }
          else{
                  while(i<len && command[i]!=' ')
                          i++;
                  arglen = i-start;
          }
          qargs.push_back(command.substr(start, arglen));
  }
  if(qot || sqot) std::cout<<"One of the quotes is open\n";
	return qargs;
}

vector<double> MyGlobal::getLimBin(string tablepath){
	vector<double> limbin;

	ifstream readTable(tablepath.c_str());

	if(readTable.is_open()){
		while(readTable.good()){
			limbin.push_back(0.);
			readTable >> limbin.back();
		}
	}
	return limbin;
}

double MyGlobal::getDeltaX(TH1D* histo){
	double delta=0;
	int nBinsX = histo->GetNbinsX();
	double xmin = histo->GetXaxis()->GetXmin();
	double xmax = histo->GetXaxis()->GetXmax();

	delta=(xmax-xmin)/((double)nBinsX);
	return delta;
}

string MyGlobal::intToStr(int val){
	string str;
	ostringstream convert;   // stream used for the conversion
	convert << val;      // insert the textual representation of 'Number' in the characters in the stream
	str= convert.str(); // set 'Result' to the contents of the stream

	return str;
}

string MyGlobal::doubleToStr(double val){
	string str;
	ostringstream convert;   // stream used for the conversion
	convert << val;      // insert the textual representation of 'Number' in the characters in the stream
	str= convert.str(); // set 'Result' to the contents of the stream

	return str;
}

string MyGlobal::doubleToStr(double val, int precision){
	string str;
	ostringstream convert;   // stream used for the conversion
	convert << std::fixed<< std::setprecision(precision) << val;      // insert the textual representation of 'Number' in the characters in the stream
	str= convert.str(); // set 'Result' to the contents of the stream

	return str;
}

vector< double>  MyGlobal::getConfidenceInterval(TH1D* histo, double fracintg)
{
	vector<double> CI;
	CI.resize(3);
	//double center = histo->GetMean();
	double center = histo->GetXaxis()->GetBinCenter(histo->GetMaximumBin());
	int bincenter = histo->FindBin(center);


	double intgTot=histo->Integral();
	double intg=0;

	for(int bin=1;bin<=(histo->GetNbinsX()/2);bin++ )
		{
			intg = histo->Integral(bincenter-bin,bincenter+bin);
			if(intg/intgTot>=fracintg) 
			{
				CI[0]=histo->GetXaxis()->GetBinLowEdge(bincenter-bin);
				CI[1]=histo->GetXaxis()->GetBinUpEdge(bincenter+bin);
				break;
			}
		}

	CI[2]=center;
	printf("C[0] = %f C[1] = %f \n",CI[0],CI[1]);
return CI;
}

TChain* MyGlobal::makeChain(string filelist,string treename)
{
	TChain* chain;
	ifstream readList(filelist.c_str());
	chain = new TChain(treename.c_str());

	if(readList.is_open())
		{
		while(readList.good())
			{
				string strline;
				getline(readList,strline);
				cout << strline << endl;
				TFile* file=TFile::Open(strline.c_str());
				if(file==NULL) continue;
				chain->Add(strline.c_str());
			}
		}

	return chain;
}

vector<int> MyGlobal::getBinCoord(string str){
	vector<string> strSplit =MyGlobal::tokenize(str,'_');
	string strCoord =strSplit.back();
	vector<string> strCoords=MyGlobal::tokenize(strCoord,'-');
	vector<int> coords;
	coords.resize(strCoords.size());
	for(int k=0;k<strCoords.size();k++){
		coords[k]=atoi(strCoords[k].c_str());
		//cout << coords[k] << " ";
	}
	//cout<<endl;
	return coords;
}

double MyGlobal::getPhiStar(double phiStarPi0,string option){
	//Convert current phistar range from [0,2pi] to [-pi,pi]
	double newPhiStar;
	//Due to symmetry of pipi state,
	//phipi1=phipi2+-pi
	//PhiStar is defined in [0,pi]
	/*
	newPhiStar=phiminPitominPi(phiStarPi0); //Keep phistar from considered pion if positive
	if(newPhiStar<0) newPhiStar=newPhiStar+TMath::Pi(); //Use phistar from the other pion otherwise
	*/

	if(phiStarPi0<=TMath::Pi()) newPhiStar=phiStarPi0;
	else newPhiStar=phiStarPi0-TMath::Pi();
	return newPhiStar;
}

double MyGlobal::getBinVolume(THnSparseD* hnD,int bin){
	double binVolume=0;
	vector<int> coord; coord.resize(hnD->GetNdimensions());
	double val = hnD->GetBinContent(bin,&(coord[0]));
	binVolume=getBinVolume(hnD,coord);
	return binVolume;
}

double MyGlobal::getBinVolume(THnSparseD* hnD,vector<int> coord){
	double binVolume=1;
	for(int i=0;i<coord.size();i++){
			int bin = coord[i];
			double min = hnD->GetAxis(i)->GetBinLowEdge(bin);
			double max = hnD->GetAxis(i)->GetBinUpEdge(bin);
			double delta = max-min;
			binVolume=binVolume*delta;
			printf("%d %f %f \n", bin,min,max);
		}
	printf("\n");
	return binVolume;
}

/*
void MyGlobal::scaletoBinWidth(THnSparseD* hnD){
	THnSparseD* hnDNorm;
	hnDNorm = (THnSparseD*) hnD->Clone();
	hnDNorm->SetName("hnDNorm");
	hnDNorm->Sumw2();
	hnDNorm->Reset();

	for(int bin=0;bin<hnD->GetNbins();bin++){
		vector<int> coord; coord.resize(hnD->GetNdimensions());
		hnD->GetBinContent(bin,&(coord[0]));
		double binVolume = getBinVolume(hnD,coord);
		hnDNorm->SetBinContent(&(coord[0]),binVolume);
		hnDNorm->SetBinError(&(coord[0]),0);
	}
	hnD->Multiply(hnDNorm);
}
*/

void  MyGlobal::scaleTHnSparse(THnSparseD* hnD, Double_t c, string option){
	Double_t nEntries = hnD->GetEntries();
	// Scale the contents & errors
	Bool_t haveErrors = hnD->GetCalculateErrors();
	//Long64_t i = 0;
	//THnIter iter(hnD);
	for(Long64_t i=0;i<hnD->GetNbins();i++) {
	       // Get the content of the bin from the current histogram
		   vector<int> coord; coord.resize(hnD->GetNdimensions());
	       Double_t v = hnD->GetBinContent(i,&(coord[0]));
	       Double_t scale;
	       if(option.find("width") != string::npos){
	    	   double binVolume=getBinVolume(hnD,coord);
	    	   scale=c/binVolume;
	    	   printf("MyGlobal::scaleTHnSparse %f %f %f \n",c,binVolume,scale);
	       }
	       else scale=c;

	       hnD->SetBinContent(&(coord[0]), scale * v);
	       if (haveErrors) {
	          Double_t err = hnD->GetBinError(i);
	          hnD->SetBinError(&(coord[0]), scale* err);
	       }
	   }
	hnD->SetEntries(nEntries);
}
