################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CXX_SRCS += \
../src/BgndSubtractor.cxx \
../src/CompDataMCScaledPlotter.cxx \
../src/CrossSection.cxx \
../src/Efficiency.cxx \
../src/MyGlobal.cxx \
../src/Normalizer.cxx \
../src/THnSparseMaker.cxx \
../src/YieldTableMaker.cxx 

CPP_SRCS += \
../src/analysis.cpp 

CXX_DEPS += \
./src/BgndSubtractor.d \
./src/CompDataMCScaledPlotter.d \
./src/CrossSection.d \
./src/Efficiency.d \
./src/MyGlobal.d \
./src/Normalizer.d \
./src/THnSparseMaker.d \
./src/YieldTableMaker.d 

OBJS += \
./src/BgndSubtractor.o \
./src/CompDataMCScaledPlotter.o \
./src/CrossSection.o \
./src/Efficiency.o \
./src/MyGlobal.o \
./src/Normalizer.o \
./src/THnSparseMaker.o \
./src/YieldTableMaker.o \
./src/analysis.o 

CPP_DEPS += \
./src/analysis.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cxx
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ `root-config --cflags` -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ `root-config --cflags` -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


