# CMakeLists.txt for event package. It creates a library with dictionary and a main program
cmake_minimum_required(VERSION 3.0 FATAL_ERROR)
set(mytarget    analysis       )  # equivalent to "name := myexample" in G4 GNUmakefiles
set(useROOT     true            )  # use true or false (or comment to set to false)
#set(myROOTclass Event.h)  #commented if no ROOT shared library from user class(please see http://root.cern.ch/phpBB3//viewtopic.php?t=6172)
#set(myROOTclassname Event) #commented if no ROOT shared library from user class
#Set shared ROOT library does not work atm

############ What's below should not need to be changed ############ 

# http://www.cmake.org/cmake/help/cmake_tutorial.html
# http://www.cmake.org/cmake/help/cmake2.6docs.html
# http://www.cmake.org/Wiki/CMake_FAQ

set(mymain ${mytarget}.cpp)
set(myexe  ${mytarget}   )
set(myproj ${mytarget}_  )  # any name as long as it is different from myexe

project(${myproj})

# You need to tell CMake where to find the ROOT installation. This can be done in a number of ways:
#   - ROOT built with classic configure/make use the provided $ROOTSYS/etc/cmake/FindROOT.cmake
#   - ROOT built with CMake. Add in CMAKE_PREFIX_PATH the installation prefix for ROOT
list(APPEND CMAKE_PREFIX_PATH $ENV{ROOTSYS})

#---Locate the ROOT package and defines a number of variables (e.g. ROOT_INCLUDE_DIRS)
find_package(ROOT REQUIRED COMPONENTS MathCore RIO Hist Tree Net)

#---Define useful ROOT functions and macros (e.g. ROOT_GENERATE_DICTIONARY)
include(${ROOT_USE_FILE})

include_directories(${CMAKE_SOURCE_DIR} ${ROOT_INCLUDE_DIRS})
add_definitions(${ROOT_CXX_FLAGS})

#ROOT_GENERATE_DICTIONARY(G__${myROOTclassname} ${myROOTclassname}.h LINKDEF ${myROOTclassname}LinkDef.h) #commented if no ROOT shared library from user class

#----------------------------------------------------------------------------
# Locate sources and headers for this project
# NB: headers are included so they will show up in IDEs
#
file(GLOB sources ${PROJECT_SOURCE_DIR}/*.cxx) 
file(GLOB headers ${PROJECT_SOURCE_DIR}/*.h)

#---Create a shared library with geneated dictionary
#add_library(${myROOTclassname} SHARED ${myROOTclassname}.cxx G__${myROOTclassname}.cxx) #commented if no ROOT shared library from user class
#target_link_libraries(${myROOTclassname} ${ROOT_LIBRARIES}) #commenedt if no ROOT shared library from user class

# Add the executable, and link it to the ROOT libraries
add_executable(${myexe} ${mymain} ${sources} ${headers})
#target_link_libraries(${myexe} ${ROOT_LIBRARIES} ${myROOTclassname}) #comment if no ROOT shared library from user class
target_link_libraries(${myexe} ${ROOT_LIBRARIES})

#----------------------------------------------------------------------------
# Install the executable to 'bin' directory under CMAKE_INSTALL_PREFIX
#
install(TARGETS ${myexe} DESTINATION bin)
