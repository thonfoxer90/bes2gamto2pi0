/*
 * CrossSection.h
 *
 *  Created on: Jan 19, 2018
 *      Author: bgarillo
 */

#ifndef CROSSSECTION_H_
#define CROSSSECTION_H_

#include <TROOT.h>
#include <TChain.h>
#include <TRint.h>
#include <TTree.h>
#include <TFile.h>
#include <TStyle.h>
#include <TString.h>
#include <TSelector.h>
#include <TF1.h>
#include <TH2.h>
#include <TH1.h>
#include <THnSparse.h>
#include <TGraph2D.h>
#include <TVirtualIndex.h>
#include <iostream>             // std::cout, std::endl
#include <fstream>              // std::ifstream
#include <sstream>
#include <TPad.h>
#include <TCanvas.h>
#include <TGaxis.h>
#include <TPave.h>
#include <TPaveText.h>
#include <TLatex.h>
#include <TVector3.h>
#include <TLorentzVector.h>
#include <TSystem.h>
#include <cstdlib>
#include <map>
#include <time.h>

#include "MyGlobal.h"

using namespace std;

class CrossSection {
public:
	CrossSection();
	virtual ~CrossSection();
	void run(string inputcard, string outpath);
private:
	int m_CalcMode; //0: Event by event weighting, 1: Calculation with THn
	double m_Luminosity;
	string m_LimBinPath;
	string m_DataNTFileName;
	string m_DataSubtrFileName;
	string m_BgndFracFileName;
	string m_EffRecFileName;
	string m_LumGamGamFileName;
	int m_UseFittedKinematics;
	const double m_picoToFemto;
	const double m_picoToNano;
	const double m_BF_Pi0to2Gam;
	Int_t m_nDim;

};

#endif /* CROSSSECTION_H_ */
