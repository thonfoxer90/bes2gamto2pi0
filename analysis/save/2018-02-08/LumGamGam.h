/*
 * LumGamGam.h
 *
 *  Created on: Feb 1, 2018
 *      Author: bgarillo
 */

#ifndef LUMGAMGAM_H_
#define LUMGAMGAM_H_

#include <TROOT.h>
#include <TChain.h>
#include <TRint.h>
#include <TTree.h>
#include <TFile.h>
#include <TStyle.h>
#include <TString.h>
#include <TSelector.h>
#include <TF1.h>
#include <TH2.h>
#include <TH3.h>
#include <TH1.h>
#include <THnSparse.h>
#include <TGraphErrors.h>
#include <TGraph2D.h>
#include <TVirtualIndex.h>
#include <iostream>             // std::cout, std::endl
#include <fstream>              // std::ifstream
#include <sstream>
#include <TPad.h>
#include <TCanvas.h>
#include <TGaxis.h>
#include <TPave.h>
#include <TPaveText.h>
#include <TLatex.h>
#include <TVector3.h>
#include <TLorentzVector.h>
#include <TSystem.h>
#include <cstdlib>
#include <map>
#include <time.h>

#include "MyGlobal.h"

#define SSTR( x ) static_cast< std::ostringstream & >( \
        ( std::ostringstream() << std::dec << x ) ).str()

using namespace std;

class LumGamGam {
public:
	LumGamGam();
	virtual ~LumGamGam();
	double GetIntegral(string inputfile);
	void ParseLogFile(string inputfile);
	void ParseDatFile(string inputfile);
	void run(string inputcard,string option, string outpath);
private:
	string m_Generator;
	string m_NTuplefile;
	string m_Intgfile;
	string m_binningdir;
	int m_dim;

	TGraph2D *m_gr2D;
	TGraphErrors *m_gr1D;

	map<string,double> m_integral;
	map<string,double> m_integralError;
	map<string,double> m_Wmin;
	map<string,double> m_Wmax;
	map<string,double> m_W;
	map<string,double> m_ECMS;
	map<string,double> m_t2min;
	map<string,double> m_t2max;
	map<string,double> m_t2;

	double m_Q2eGEN,m_Q2pGEN;
	double m_WGEN;
};

#endif /* LUMGAMGAM_H_ */
