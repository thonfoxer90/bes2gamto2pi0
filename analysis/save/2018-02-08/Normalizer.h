/*
 * Normalizer.h
 *
 *  Created on: Dec 11, 2017
 *      Author: bgarillo
 */

#ifndef NORMALIZER_H_
#define NORMALIZER_H_
#include <TFile.h>
#include <TDirectory.h>
#include <TTree.h>
#include <TChain.h>
#include <TCanvas.h>
#include <TPad.h>
#include <TH1.h>
#include <TH2.h>
#include <THStack.h>
#include <TLine.h>
#include <stdio.h>
#include <string.h>
#include <iostream> //std::cout
#include <sstream>
#include <fstream> //std::ifstream,ofstream
#include <vector>
#include <algorithm> //std::sort
#include <TKey.h>
#include <TList.h>
#include <TStyle.h>
#include <TROOT.h>
#include <TLegend.h>
#include <TLegendEntry.h>

#define SSTR( x ) static_cast< std::ostringstream & >( \
        ( std::ostringstream() << std::dec << x ) ).str()

using namespace std;

class Normalizer {
public:
	Normalizer();
	Normalizer(string inputcard);
	void run();
	void run(string inputcard);
	vector <string> Tokenize(string str, char delim);
	double GetCrossSection(string logfile,string generator);
	void GetNGenAndCrossSection(string logfile,string generator, int& NGen, double &XSec);
	void GetNGenAndCrossSectionFromList(string filelist,string generator, int& NGenSum, double &XSecMean);
	vector<string> GetSubDirectoryNames(TFile* file,string prefix);
	void run(string inputstr, string option, string outpath);
	virtual ~Normalizer();
private:
	static const double millitopico=1E9;
	static const double nanotofemto=1E6;
	static const double nanotopico=1E3;

	string m_option;
	string m_inputcard;
};

#endif /* NORMALIZER_H_ */
