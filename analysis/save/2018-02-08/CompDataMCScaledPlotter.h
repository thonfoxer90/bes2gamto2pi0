/*
 * CompDataMCScaledPlotter.h
 *
 *  Created on: Jan 19, 2018
 *      Author: bgarillo
 */

#ifndef COMPDATAMCSCALEDPLOTTER_H_
#define COMPDATAMCSCALEDPLOTTER_H_

#include <TFile.h>
#include <TDirectory.h>
#include <TTree.h>
#include <TChain.h>
#include <TCanvas.h>
#include <TPad.h>
#include <TH1.h>
#include <TH2.h>
#include <THStack.h>
#include <TLine.h>
#include <stdio.h>
#include <string.h>
#include <iostream> //std::cout
#include <sstream>
#include <fstream> //std::ifstream,ofstream
#include <vector>
#include <algorithm> //std::sort
#include <TKey.h>
#include <TList.h>
#include <TStyle.h>
#include <TROOT.h>
#include <TLegend.h>
#include <TLegendEntry.h>

#define SSTR( x ) static_cast< std::ostringstream & >( \
        ( std::ostringstream() << std::dec << x ) ).str()

using namespace std;

class CompDataMCScaledPlotter {
public:
	CompDataMCScaledPlotter();
	virtual ~CompDataMCScaledPlotter();
	vector <string> Tokenize(string str, char delim);
	static bool HistHasLargerIntg(TH1D* h1, TH1D * h2);
	static bool HistHasSmallerIntg(TH1D* h1, TH1D * h2);
	TChain* makeChain(string filelist,string treename);
	void InitGlobalVar();
	vector <double> FindFilledMinMax(TH1D* histo);
	void ParseInputFile(string inputcard);
	int getNumberOfSubDir(string inputfile,string dir, string keyword);
	void run(string inputcard, string option, string outputname);
	void runAll(string inputcard, string option, string outname);
private:
	vector<TFile*> m_files;
	vector<TTree*> m_trees;
	vector<double> m_scales;
	vector<string> m_colors;
	vector<string> m_descriptions;

	string m_mode;
	string m_treename;
	string m_histodir;

	int m_isLogScale;
	int m_isStacked;
	int m_zoomToFilledXRange;
	int m_showDatatoMCRatio;
	int m_showData;
	int m_hasData;
	bool m_firstCall;
};

#endif /* COMPDATAMCSCALEDPLOTTER_H_ */
