#ifndef MYGLOBAL_H_
#define MYGLOBAL_H_

#include <string>
#include <vector>
#include <iomanip>
#include <iostream>             // std::cout, std::endl
#include <fstream>              // std::ifstream
#include <TFile.h>
#include <TDirectory.h>
#include <TTree.h>
#include <TChain.h>
#include <sstream> 
#include <TH1D.h>
#include <TAxis.h>
#include <TMath.h>

using namespace std;

class MyGlobal{
	public:
		static double phi0to2Pi(double phi);
		static vector<string> tokenize(string str, char delim);
		static vector<string> tokenizeArgs(string command);
		static vector<double> getLimBin(string tablepath);
		static double getDeltaX(TH1D* histo);
		static string intToStr(int val);
		static string doubleToStr(double val);
		static string doubleToStr(double val, int precision);
		static vector<double> getConfidenceInterval(TH1D* histo, double fracintg);
		static TChain* makeChain(string filelist,string treename);
		static vector<int> getBinCoord(string str);
	private:

};

#endif
