/*
 * BgndSubtractor.h
 *
 *  Created on: Jan 19, 2018
 *      Author: bgarillo
 */

#ifndef BGNDSUBTRACTOR_H_
#define BGNDSUBTRACTOR_H_

#include <TROOT.h>
#include <TChain.h>
#include <TRint.h>
#include <TTree.h>
#include <TFile.h>
#include <TStyle.h>
#include <TString.h>
#include <TSelector.h>
#include <TF1.h>
#include <TH2.h>
#include <TH1.h>
#include <THnSparse.h>
#include <TVirtualIndex.h>
#include <iostream>             // std::cout, std::endl
#include <fstream>              // std::ifstream
#include <sstream>
#include <TPad.h>
#include <TCanvas.h>
#include <TGaxis.h>
#include <TPave.h>
#include <TPaveText.h>
#include <TLatex.h>
#include <TLegend.h>
#include <TLegendEntry.h>
#include <TVector3.h>
#include <TLorentzVector.h>
#include <TSystem.h>
#include <cstdlib>
#include <map>
#include <time.h>

#include "MyGlobal.h"

using namespace std;

class BgndSubtractor {
public:
	BgndSubtractor();
	virtual ~BgndSubtractor();
	void run(string inputcard,string option,string outpath);
};

#endif /* BGNDSUBTRACTOR_H_ */
