/*
 * SphericalHarmonics.h
 *
 *  Created on: Feb 5, 2018
 *      Author: bgarillo
 */

#ifndef SPHERICALHARMONICS_H_
#define SPHERICALHARMONICS_H_

#include <utility>
#include <string>
#include <complex>
#include <vector>
#include <cassert>
#include <iostream>
#include <cstdlib>

#include <TLorentzVector.h>

using std::complex;
using namespace std;

class SphericalHarmonics {
public:
	SphericalHarmonics();
	SphericalHarmonics( const vector< string > &args );
	virtual ~SphericalHarmonics();
	//complex< double > calcAmplitude( double** pKin ) const;
	complex< double > calcAmplitude( double** pKin );
	complex<double> calcAmplitude(double theta,double phi);
	double calcBase(int J, int M, double theta, double phi);
private:
	vector <int> m_idx;
	int m_J;
	int m_M;
};

#endif /* SPHERICALHARMONICS_H_ */
