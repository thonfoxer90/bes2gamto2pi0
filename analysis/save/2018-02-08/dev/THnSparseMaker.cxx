/*
 * THnSparseMaker.cxx
 *
 *  Created on: Jan 19, 2018
 *      Author: bgarillo
 */

#include "THnSparseMaker.h"

THnSparseMaker::THnSparseMaker():m_UseFittedKinematics(1) {
	// TODO Auto-generated constructor stub

}

THnSparseMaker::~THnSparseMaker() {
	// TODO Auto-generated destructor stub
}

void THnSparseMaker::run(string inputfilename, string option, string binningdir,string outpath){
	TTree* tree;
	TFile* file;

	string ntname("");
	int bScale=0;
	double scale=1;

	//Parse options
	vector<string> optionSplit = MyGlobal::tokenize(option,' ');
	for(int k=0;k<optionSplit.size();k++){
		vector<string> strsplit = MyGlobal::tokenize(optionSplit[k],'=');
		string arg=strsplit[0];
		string val=strsplit[1];
		if(arg.find("Type") != string::npos){
			ntname=val;
		}
		if(arg.find("Normalized") != string::npos){
			bScale=atoi(val.c_str());
		}
	}

	if(inputfilename.find(".txt") != string::npos){
		//List of root files
	}
	else {
	file = TFile::Open(inputfilename.c_str());
	tree=(TTree*) file->Get(ntname.c_str());
	if(tree==NULL) return;
	}
	string treename(tree->GetName());

	if(bScale==1){
	string treeHEADname=treename+"HEAD";
	TTree* treeHEAD = (TTree*) file->Get(treeHEADname.c_str());
	treeHEAD->SetBranchAddress("scale",&scale);
	treeHEAD->GetEntry(0);
	}

	string outname=outpath+".root";
	TFile* outROOT = new TFile(outname.c_str(),"RECREATE");

	if(treename=="Tagged"){
		THnSparseD* hnD[3]; //0: All 1:eTagged 2:pTagged
		double Q2,InvM2Pi0,CThetaSt,absCThetaSt,PhiStar;
		int QTag;

		const Int_t nDim = 3;
		vector<vector<double> > vec_limbins;
		vec_limbins.resize(nDim);
		for(int k=0;k<nDim;k++){
			string limbinFullPath;
			string limbinFileName;
			if(k==0) limbinFileName="limbin_Q2.dat";
			if(k==1) limbinFileName="limbin_M2Pions.dat";
			if(k==2) limbinFileName="limbin_AbsCThetaSt.dat";
			limbinFullPath=binningdir+"/"+limbinFileName;
			vec_limbins[k]=MyGlobal::getLimBin(limbinFullPath);
			//for(int i=0;i<vec_limbins[k].size();i++) cout << vec_limbins[k][i] << endl;
			//cout << endl;
		}
		Int_t nBins[nDim];
		Double_t min[nDim];
		Double_t max[nDim];

		for(int k=0;k<nDim;k++) {
			nBins[k]=vec_limbins[k].size()-1;
			min[k]=vec_limbins[k][0];
			max[k]=vec_limbins[k][vec_limbins[k].size()-1];
			//cout << nBins[k] << endl;
			//cout << min[k] << endl;
			//cout << max[k] << endl;
		}

		for(int i=0;i<3;i++){
			string histoname;
			if(i==0) histoname="hnD";
			else if(i==1) histoname="hnDeT";
			else if(i==2) histoname="hnDpT";
			hnD[i]= new THnSparseD(histoname.c_str(),"hnD",nDim,nBins,min, max);
			//Define the binning explicitly for variable bin size case
			for(int k=0;k<nDim;k++){
				vector<double> limbin=vec_limbins[k];
				hnD[i]->GetAxis(k)->Set(nBins[k],&limbin[0]);
			}
			hnD[i]->Sumw2();
			//hnD[i]->SetTitle("Title;Q^{2} (Gev^{2});cos(#theta^{*});M_{#pi#pi} (GeV)");
			hnD[i]->GetAxis(0)->SetTitle("Q^{2} (Gev^{2})");
			hnD[i]->GetAxis(0)->SetName("Q2");
			hnD[i]->GetAxis(1)->SetTitle("W (GeV/c^{2})");
		    hnD[i]->GetAxis(1)->SetName("M2Pi");
			hnD[i]->GetAxis(2)->SetTitle("|cos #theta^{*}|");
			hnD[i]->GetAxis(2)->SetName("AbsCThetaSt");

		}

		if(m_UseFittedKinematics==0){
			tree->SetBranchAddress("Q2",&Q2);
			tree->SetBranchAddress("2Pi0InvM", &InvM2Pi0);
			tree->SetBranchAddress("cosThetaStarPi01", &CThetaSt);
			tree->SetBranchAddress("QTag", &QTag);
			tree->SetBranchAddress("PhiStarPi01", &PhiStar);
		}
		else{
			tree->SetBranchAddress("Q2Fit",&Q2);
			tree->SetBranchAddress("2Pi0InvMFit", &InvM2Pi0);
			tree->SetBranchAddress("cosThetaStarPi0Fit1", &CThetaSt);
			tree->SetBranchAddress("QTag", &QTag);
			tree->SetBranchAddress("PhiStarPi0Fit1", &PhiStar);
		}

		cout << "Number of entries in tree : " << tree->GetEntries() << endl;
		for(int entry=0;entry<tree->GetEntries();entry++){
			tree->GetEntry(entry);
			if (entry % 100 == 0) cout << "Event counter = " << entry << endl;
			absCThetaSt=fabs(CThetaSt);
			vector<double> binval; binval.resize(nDim);
			binval[0]=Q2;binval[1]=InvM2Pi0;binval[2]=absCThetaSt;
			hnD[0]->Fill(&(binval[0]),scale);
			if(QTag==-1) hnD[1]->Fill(&(binval[0]),scale);
			if(QTag==1) hnD[2]->Fill(&(binval[0]),scale);
		}

	for(int i=0;i<3;i++) hnD[i]->Write();
	}

	if(treename=="AllMCTruth"){
	THnSparseD* hnD[3];

	double Q2GEN[2];
	double InvM2Pi0GEN;
	double CThetaStGEN[2][2];

	double Q2REC,InvM2Pi0REC,CThetaStREC;
	double Q2,InvM2Pi0,CThetaSt,absCThetaSt;

	const Int_t nDim = 3;
		vector<vector<double> > vec_limbins;
		vec_limbins.resize(nDim);
		for(int k=0;k<nDim;k++){
			string limbinFullPath;
			string limbinFileName;
			if(k==0) limbinFileName="limbin_Q2.dat";
			if(k==1) limbinFileName="limbin_M2Pions.dat";
			if(k==2) limbinFileName="limbin_AbsCThetaSt.dat";
			limbinFullPath=binningdir+"/"+limbinFileName;
			vec_limbins[k]=MyGlobal::getLimBin(limbinFullPath);
			//for(int i=0;i<vec_limbins[k].size();i++) cout << vec_limbins[k][i] << endl;
			//cout << endl;
		}
		Int_t nBins[nDim];
		Double_t min[nDim];
		Double_t max[nDim];

		for(int k=0;k<nDim;k++) {
			nBins[k]=vec_limbins[k].size()-1;
			min[k]=vec_limbins[k][0];
			max[k]=vec_limbins[k][vec_limbins[k].size()-1];
			//cout << nBins[k] << endl;
			//cout << min[k] << endl;
			//cout << max[k] << endl;
		}

		/*
		hnD= new THnSparseD("hnD","hnD",nDim,nBins,min, max);
		//Define the binning explicitly for variable bin size case
		for(int k=0;k<nDim;k++){
			vector<double> limbin=vec_limbins[k];
			hnD->GetAxis(k)->Set(nBins[k],&limbin[0]);
		}
		*/

		for(int i=0;i<3;i++){
			string histoname;
			if(i==0) histoname="hnDGEN";
			else if(i==1) histoname="hnDeTGEN";
			else if(i==2) histoname="hnDpTGEN";
			hnD[i]= new THnSparseD(histoname.c_str(),"hnD",nDim,nBins,min, max);
			//Define the binning explicitly for variable bin size case
			for(int k=0;k<nDim;k++){
				vector<double> limbin=vec_limbins[k];
				hnD[i]->GetAxis(k)->Set(nBins[k],&limbin[0]);
			}
			hnD[i]->Sumw2();
			//hnD[i]->SetTitle(";Q^{2} (Gev^{2});cos(#theta^{*});M_{#pi#pi} (GeV);");
			hnD[i]->GetAxis(0)->SetTitle("Q^{2} (Gev^{2})");
			hnD[i]->GetAxis(1)->SetTitle("W (GeV/c^{2})");
			hnD[i]->GetAxis(2)->SetTitle("|cos #theta^{*}|");

		}

		TTree* treeREC= (TTree*) file->Get("Tagged");
		if(treeREC==NULL) return;
		treeREC->SetBranchAddress("Q2",&Q2REC);
		treeREC->SetBranchAddress("2Pi0InvM", &InvM2Pi0REC);
		treeREC->SetBranchAddress("cosThetaStarPi01", &CThetaStREC);

		treeREC->SetBranchAddress("2Pi0InvMGEN", &InvM2Pi0GEN);
		for(int i=0;i<2;i++){
			string lept;
			if(i==0) lept="e";
			else if(i==1) lept="p";
			string varname;
			varname="Q2"+lept+"GEN";
			tree->SetBranchAddress(varname.c_str(),&Q2GEN[i]);
			for(int j=0;j<2;j++){
				string pi0;
				if(j==0) pi0="Pi01";
				if(j==1) pi0="Pi02";
				varname=lept+"TcosThetaStar"+pi0+"GEN";
				tree->SetBranchAddress(varname.c_str(),&CThetaStGEN[i][j]);
			}
		}

		cout << "Number of entries in tree : " << tree->GetEntries() << endl;
		for(int entry=0;entry<tree->GetEntries();entry++){
			tree->GetEntry(entry);
			if (entry % 100 == 0) cout << "Event counter = " << entry << endl;
			//Find the MC Truth Q2 closest to the reconstructed Q2
			double delta=0;
			for(int i=0;i<2;i++){
			}
			vector<double> binval; binval.resize(nDim);
			binval[0]=Q2;binval[1]=InvM2Pi0;binval[2]=absCThetaSt;
			hnD[0]->Fill(&(binval[0]));
		}
		hnD[0]->Write();

	}


	outROOT->Write();
}

void THnSparseMaker::run(string inputcard,string binningdir){
	ifstream readList(inputcard.c_str());
	if(readList.is_open()){
			while(readList.good()){
			string inputfile;
			string option;
			string outpath;

			getline(readList,inputfile);
			getline(readList,option);
			getline(readList,outpath);
			run(inputfile,option,binningdir,outpath);
			}
	}
}
