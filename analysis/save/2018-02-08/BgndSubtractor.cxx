/*
 * BgndSubtractor.cxx
 *
 *  Created on: Jan 19, 2018
 *      Author: bgarillo
 */

#include "BgndSubtractor.h"

BgndSubtractor::BgndSubtractor() {
	// TODO Auto-generated constructor stub
	gROOT->ProcessLine(".x ~/.rootlogon.C");
}

BgndSubtractor::~BgndSubtractor() {
	// TODO Auto-generated destructor stub
}

void BgndSubtractor::run(string inputcard,string option,string outpath){
	ifstream readList(inputcard.c_str());

	THnSparseD* hnDData;
	vector<THnSparseD*> vechnDBgnd;
	vector<string> vecstrTagBgnd;
	THnSparseD* hnDSumBgnd;
	THnSparseD* hnDSumBgndRatio;
	THnSparseD* hnDSubtr;

	string prefix=option;

	if(readList.is_open()){
			while(readList.good()){
				string strline;

				getline(readList,strline);
				cout << strline << endl;
				//Parse options
				vector<string> strlineSplit = MyGlobal::tokenize(strline,' ');
				bool dIsBgnd=false;

				for(int k=0;k<strlineSplit.size();k++){
					vector<string> strSplit=MyGlobal::tokenize(strlineSplit[k],'=');
					string arg=strSplit[0];
					string val=strSplit[1];
					cout << val << endl;
					if(arg.find("Data") != string::npos){
						dIsBgnd=false;
						string filename=val;
						TFile* file = TFile::Open(filename.c_str());
						if(file==NULL){cout << "****** Data FILE CANNOT BE OPENED ******"<< endl; return;}
						hnDData = (THnSparseD*)file->Get(prefix.c_str());
						if(hnDData==NULL) {cout << "****** THnSparseD cannot be found" << endl;}
					}
					if(arg.find("Bgnd") != string::npos){
						dIsBgnd=true;
						string filename=val;
						TFile* file = TFile::Open(filename.c_str());
						if(file==NULL){cout << "****** Background FILE CANNOT BE OPENED ******"<< endl; continue;}
						//THnSparseD* hnDBgnd= (THnSparseD*)file->Get("hnD");
						THnSparseD* hnDBgnd= (THnSparseD*)gROOT->FindObject(prefix.c_str());
						if(hnDBgnd==NULL) {cout << "****** THnSparseD cannot be found" << endl;continue;}
						else {vechnDBgnd.push_back(hnDBgnd);}
					}
					if(arg.find("Tag") != string::npos && dIsBgnd==true){
					vecstrTagBgnd.push_back(val);
					}
				}
			}
	}

	string outrootname=outpath+".root";
	TFile* outROOT = new TFile(outrootname.c_str(),"RECREATE");

	string outps=outpath+".ps";
	string outpdf=outpath+".pdf";

	cout << vechnDBgnd.size() << endl;
	cout << vecstrTagBgnd.size()<< endl;
	string name;
	for(int k=0;k<vechnDBgnd.size();k++){
		name=prefix+vecstrTagBgnd[k];
		vechnDBgnd[k]->SetName(name.c_str());
	}

	hnDSumBgnd=(THnSparseD*) vechnDBgnd[0]->Clone();
	name=prefix+"SumBgnd";
	hnDSumBgnd->SetName(name.c_str());
	hnDSumBgnd->SetTitle("Sum of Background;;;");
	for(int k=1;k<vechnDBgnd.size();k++){
	hnDSumBgnd->Add(vechnDBgnd[k],1);
	}

	hnDSubtr=(THnSparseD*) hnDData->Clone();
	name=prefix+"Subtr";
	hnDSubtr->SetName(name.c_str());
	hnDSubtr->SetTitle("Background subtracted;;;");
	hnDSubtr->Add(hnDSumBgnd,-1);

	hnDSumBgndRatio=(THnSparseD*) hnDSumBgnd->Clone();
	name=prefix+"SumBgndRatio";
	hnDSumBgndRatio->SetName(name.c_str());
	hnDSumBgndRatio->SetTitle("Background fraction;;;");
	hnDSumBgndRatio->Divide(hnDData);

	for(int k=0;k<vechnDBgnd.size();k++){vechnDBgnd[k]->Write();}
	hnDData->Write();
	hnDSumBgnd->Write();
	hnDSubtr->Write();
	hnDSumBgndRatio->Write();
	outROOT->Write();
	outROOT->Close();

	gStyle->SetOptStat(0);
	TCanvas *c = new TCanvas();
	c->Divide(2,1);
	c->cd(1);
	TH1D* hM2pi = (TH1D*)hnDData->Projection(2);
	double deltax;
	deltax = MyGlobal::getDeltaX(hM2pi);
	cout << deltax << endl;
	char hname[256];
	char htitle[256];
	sprintf(htitle,"Data;W (GeV/c^{2});Events/%.2f (GeV/c^{2})",deltax);
	hM2pi->SetTitle(htitle);
	hM2pi->SetMarkerStyle(8);
	hM2pi->Draw("E");
	gPad->SetLogy();

	TLegend* leg = new TLegend(0.60,0.60,0.90,0.90);
	leg->SetFillColor(0);
	leg->AddEntry(hM2pi,"Data");

	vector<TH1D*> vechM2piBgnd;
	for(int k=0;k<vechnDBgnd.size();k++){
		TH1D* histo = (TH1D*) vechnDBgnd[k]->Projection(2);
		cout << vechnDBgnd[k]->GetName() << endl;
		cout << vecstrTagBgnd[k] << endl;
		cout << endl;
		sprintf(hname,"%s",vecstrTagBgnd[k].c_str());
		histo->SetName(hname);
		if(vecstrTagBgnd[k]=="qqbar") histo->SetLineColor(4);
		if(vecstrTagBgnd[k]=="Bhabha") histo->SetLineColor(5);
		if(vecstrTagBgnd[k]=="eta") histo->SetLineColor(6);
		if(vecstrTagBgnd[k]=="D0D0bar") histo->SetLineColor(7);
		if(vecstrTagBgnd[k]=="DpDm") histo->SetLineColor(8);
		if(vecstrTagBgnd[k]=="GammaPsiP") histo->SetLineColor(9);

		string str;
		if(vecstrTagBgnd[k]=="qqbar") str="q#bar{a}";
		if(vecstrTagBgnd[k]=="Bhabha") str="e^{+}e^{-} #rightarrow e^{+}e^{-}n(#gamma)";
		if(vecstrTagBgnd[k]=="eta") str="e^{+}e^{-} #rightarrow e^{+}e^{-}#eta";
		if(vecstrTagBgnd[k]=="D0D0bar") str="D^{0}#bar{D^{0}}";
		if(vecstrTagBgnd[k]=="DpDm") str="D^{+}D^{-}";
		if(vecstrTagBgnd[k]=="GammaPsiP") str="#gamma #Psi(2S)";

		histo->Draw("HIST SAME");

		leg->AddEntry(histo,str.c_str());
	}
	TH1D* hM2piBgnd = (TH1D*)hnDSumBgnd->Projection(2);
	sprintf(htitle,"Background (total) ;W (GeV/c^{2});Events/%.2f (GeV/c^{2})",deltax);
	hM2piBgnd->SetTitle(htitle);
	hM2piBgnd->SetLineColor(2);
	hM2piBgnd->Draw("same");

	leg->AddEntry(hM2piBgnd,"#sum Backgrounds");
	leg->Draw();

	c->cd(2);
	TH1D* hM2piSubtr = (TH1D*)hnDSubtr->Projection(2);
	deltax = MyGlobal::getDeltaX(hM2piSubtr);
	sprintf(htitle,"After background subtraction;W (GeV/c^{2});Events/%.2f (GeV/c^{2})",deltax);
	hM2piSubtr->SetTitle(htitle);
	hM2piSubtr->SetMarkerStyle(8);
	hM2piSubtr->Draw("E");

	c->Print(Form("%s",outps.c_str()));
	//Convert ps file into pdf with ps2pdf (can be properly view by evince)
	//On Himster environment, pdf file created by ROOT is corrupted when opened with evince
	//To view the pdf : evince filename.pdf
	char systcmd[256];
	sprintf(systcmd,"ps2pdf %s %s",outps.c_str(), outpdf.c_str());
	system(systcmd);
	sprintf(systcmd,"rm %s",outps.c_str());
	system(systcmd);

	return;
}
