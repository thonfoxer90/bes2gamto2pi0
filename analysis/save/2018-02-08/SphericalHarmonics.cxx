/*
 * SphericalHarmonics.cxx
 *
 *  Created on: Feb 5, 2018
 *      Author: bgarillo
 */

#include "SphericalHarmonics.h"

SphericalHarmonics::SphericalHarmonics() {
	// TODO Auto-generated constructor stub

}

SphericalHarmonics::SphericalHarmonics( const vector< string > &args ) {
	// TODO Auto-generated constructor stub
	assert (args.size()==3);
	  m_J=atoi(args[0].c_str());
	  m_M=atoi(args[1].c_str());
	  for(int k=2;k<args.size();k++)
	  {m_idx.push_back(atoi(args[k].c_str()));
	  }

	  if (m_J<0){
	    cout<<"SphericalHarmonics::SphericalHarmonics error, J should be >= 0 "<<endl;
	    exit(1);
	  }

	  if (abs(m_M)>m_J){
	    cout<<"SphericalHarmonics::SphericalHarmonics error, M should be <= J "<<endl;
	    exit(1);
	  }
}

double SphericalHarmonics::calcBase(int J, int M, double theta, double phi){
	double base;
	switch (J){
	  case 0:
		base=sqrt(1./(4*M_PI));
		break;
	  case 1:
		switch (M){
		case -1:
		  base=sqrt(3./(8*M_PI))*sin(theta);
		  break;
		case 0:
		  base=sqrt(3./(4*M_PI))*cos(theta);
		  break;
		case 1:
		  base=-sqrt(3./(8*M_PI))*sin(theta);
		  break;
		}
		break;
	  case 2:
		switch (M){
		case -2:
		  base=sqrt(15./(32*M_PI))*sin(theta)*sin(theta);
		  break;
		case -1:
		  base=sqrt(15./(8*M_PI))*sin(theta)*cos(theta);
		  break;
		case 0:
		  base=sqrt(5./(16*M_PI))*(3*cos(theta)*cos(theta)-1);
		  break;
		case 1:
		  base=-sqrt(15./(8*M_PI))*sin(theta)*cos(theta);
		  break;
		case 2:
		  base=sqrt(15./(32*M_PI))*sin(theta)*sin(theta);
		  break;
		}
		break;
	  case 3:
		switch (M){
		case -3:
			base=sqrt(35./(64*M_PI))*pow(sin(theta),3);
			break;
		case -2:
			base=sqrt(105./(32*M_PI))*sin(theta)*sin(theta)*cos(theta);
			break;
		case -1:
			base=sqrt(21./(64*M_PI))*sin(theta)*(5*cos(theta)*cos(theta)-1);
			break;
		case 0:
			base=sqrt(7./(16*M_PI))*(5*pow(cos(theta),3)-3*cos(theta));
			break;
		case 1:
			base=-sqrt(21./(64*M_PI))*sin(theta)*(5*cos(theta)*cos(theta)-1);
			break;
		case 2:
			base=sqrt(105./(32*M_PI))*sin(theta)*sin(theta)*cos(theta);
			break;
		case 3:
			base=-sqrt(35./(64*M_PI))*pow(sin(theta),3);
			break;
		  }
		  break;
	  case 4:
		  switch (M){
		  case -4:
			  base=(3./16.)*sqrt(35./(2*M_PI))*pow(sin(theta),4);
			  break;
		  case -3:
			  base=(3./8.)*sqrt(35./M_PI)*pow(sin(theta),3)*cos(theta);
			  break;
		  case -2:
			  base=(3./8.)*sqrt(5./(2*M_PI))*sin(theta)*sin(theta)*(7*pow(cos(theta),2)-1);
			  break;
		  case -1:
			  base=(3./8.)*sqrt(5./M_PI)*sin(theta)*(7*pow(cos(theta),3)-3*cos(theta));
			  break;
		  case 0:
			  base=(3./16.)*sqrt(1./M_PI)*(35*pow(cos(theta),4)-30*pow(cos(theta),2)+1);
			  break;
		  case 1:
			  base=-(3./8.)*sqrt(5./M_PI)*sin(theta)*(7*pow(cos(theta),3)-3*cos(theta));
			  break;
		  case 2:
			  base=(3./8.)*sqrt(5./(2*M_PI))*sin(theta)*sin(theta)*(7*pow(cos(theta),2)-1);
			  break;
		  case 3:
			  base=-(3./8.)*sqrt(35./M_PI)*pow(sin(theta),3)*cos(theta);
			  break;
		  case 4:
			  base=(3./16.)*sqrt(35./(2*M_PI))*pow(sin(theta),4);
			  break;
		  }
		  break;


	  default:
		base=0;
		break;
	  }
	return base;
}

complex< double >SphericalHarmonics::calcAmplitude( double** pKin ) {

  double theta,phi;
  double real,imaginary,base;

  vector <TLorentzVector> vi;
  TLorentzVector v;
  for(int k=0;k<m_idx.size();k++)
  	{
  	vi.push_back(TLorentzVector(pKin[m_idx[k]][0],pKin[m_idx[k]][1],pKin[m_idx[k]][2],pKin[m_idx[k]][3]));
  	v=v+vi.back();
  	}

  theta=v.Theta();
  phi=v.Phi();

  real=cos(m_M*phi);
  imaginary=sin(m_M*phi);

  base = calcBase(m_J,m_M,theta,phi);

	//return  complex<GDouble>(cos(phi),0); //test


  return  complex<double>(base*real,base*imaginary);
}

complex< double >SphericalHarmonics::calcAmplitude(double theta,double phi){
	double real,imaginary,base;
	real=cos(m_M*phi);
	imaginary=sin(m_M*phi);

	base = calcBase(m_J,m_M,theta,phi);

		//return  complex<GDouble>(cos(phi),0); //test


	return  complex<double>(base*real,base*imaginary);
}
SphericalHarmonics::~SphericalHarmonics() {
	// TODO Auto-generated destructor stub


}

