/*
 * CompDataMCScaledPlotter.cxx
 *
 *  Created on: Jan 19, 2018
 *      Author: bgarillo
 */

#include "CompDataMCScaledPlotter.h"

CompDataMCScaledPlotter::CompDataMCScaledPlotter():
	m_isLogScale(0),
	m_isStacked(1),
	m_zoomToFilledXRange(1),
	m_showDatatoMCRatio(0),
	m_showData(1),
	m_hasData(1),
	m_firstCall(true){
	// TODO Auto-generated constructor stub
	gROOT->ProcessLine(".x ~/.rootlogon.C");
}

CompDataMCScaledPlotter::~CompDataMCScaledPlotter() {
	// TODO Auto-generated destructor stub
}

vector <string> CompDataMCScaledPlotter::Tokenize(string str, char delim){
vector <string> strsplit;

stringstream ss;
string strtmp;

ss.str(str);
		while (getline(ss, strtmp, delim)) {
 		strsplit.push_back(strtmp);
		}

return strsplit;
}

bool CompDataMCScaledPlotter::HistHasLargerIntg(TH1D* h1, TH1D * h2)
{
double val1 = h1->Integral();
double val2 = h2->Integral();

return (val1>val2);
}

bool CompDataMCScaledPlotter::HistHasSmallerIntg(TH1D* h1, TH1D * h2)
{
double val1 = h1->Integral();
double val2 = h2->Integral();

return (val1<val2);
}

TChain* CompDataMCScaledPlotter::makeChain(string filelist,string treename)
{
TChain* chain;
ifstream readList(filelist.c_str());
chain = new TChain(treename.c_str());

if(readList.is_open())
	{
	while(readList.good())
		{
		string strline;
		getline(readList,strline);
		cout << strline << endl;
		TFile* file=TFile::Open(strline.c_str());
		if(file==NULL) continue;
		chain->Add(strline.c_str());
		}
	}

return chain;
}

void CompDataMCScaledPlotter::InitGlobalVar(){
m_files.clear();
m_colors.clear();
m_descriptions.clear();
}

vector <double> CompDataMCScaledPlotter::FindFilledMinMax(TH1D* histo)
{
vector<double> vec;
double min=(histo->GetXaxis())->GetBinLowEdge(histo->FindFirstBinAbove(0.,1));
double max=(histo->GetXaxis())->GetBinUpEdge(histo->FindLastBinAbove(0.,1));

vec.push_back(min);vec.push_back(max);
return vec;
}

void CompDataMCScaledPlotter::ParseInputFile(string inputcard){
InitGlobalVar();

ifstream readInput(inputcard.c_str());
if(readInput.is_open()){
	while(readInput.good()){
		string strline;

		getline(readInput,strline);
		if(strline.find(".root") != string::npos){
		m_files.push_back(TFile::Open(strline.c_str()));
		}
		else return;

		getline(readInput,strline);
		m_colors.push_back(strline);

		getline(readInput,strline);
		m_descriptions.push_back(strline);
	}
}

return;
}

int CompDataMCScaledPlotter::getNumberOfSubDir(string inputfile,string dir, string keyword){
int nDirs=0;

TFile* file = TFile::Open(inputfile.c_str());

TDirectory* directory= file->GetDirectory(dir.c_str());

TIter nextkey(directory->GetListOfKeys());
TKey *key;
while ((key = (TKey*)nextkey())) {
      TObject *obj = (TObject*)key->ReadObj();
      cout << "Reading: " << obj->GetName() << " " << obj->ClassName()<< endl;
			string objname(obj->GetName());
			if (obj->InheritsFrom("TDirectory")){
				if(objname.find(keyword)!=string::npos){
					nDirs++;
				}
			}
		}

return nDirs;
}

void CompDataMCScaledPlotter::run(string inputcard, string option, string outputname){
gROOT->SetBatch();

gStyle->SetOptTitle(0);
gStyle->SetOptStat(0);

gStyle->SetPadBottomMargin(0.12);
gStyle->SetPadTopMargin(0.10);
gStyle->SetPadLeftMargin(0.10);
gStyle->SetPadRightMargin(0.05);

ParseInputFile(inputcard);

vector<string> strsplit = Tokenize(option,' ');
/*
m_mode=strsplit[0];
if(m_mode=="NTuple") m_treename=strsplit[1];
else if (m_mode=="Histo") m_histodir= strsplit[1];
else {printf("Option invalid \n"); return;}
*/

for(int k=0;k<strsplit.size();k++){
vector<string> strArgVal = Tokenize(strsplit[k],'=');
string arg=strArgVal[0];
string val=strArgVal[1];
if(arg=="NTuple" ) {m_mode=arg; m_treename=val;}
if(arg=="Histo" ) {m_mode=arg;m_histodir= val;}
if(arg=="isLogScale"){m_isLogScale=atoi(val.c_str());}
if(arg=="hasData"){m_hasData=atoi(val.c_str());}
if(arg=="showData"){m_showData=atoi(val.c_str());}
if(arg=="zoomToFilledXRange"){m_zoomToFilledXRange=atoi(val.c_str());}
}

string outps=outputname+".ps";
string outpdf=outputname+".pdf";

string outROOT = outputname+".root";

TFile* outfile = new TFile(outROOT.c_str(),"RECREATE");

//Draw distributions
char hname[64];
char drawcmd[512];
char drawopt[64];
char selection[512];
char title[512];

TCanvas *c = new TCanvas();
c->Print(Form("%s[",outps.c_str()));

if(m_mode=="Histo"){
	TDirectory* directory= m_files[0]->GetDirectory(m_histodir.c_str());
	TIter nextkey(directory->GetListOfKeys());
	TKey *key;
	while ((key = (TKey*)nextkey())) {
			TObject *obj = (TObject*)key->ReadObj();
      cout << "Reading: " << obj->GetName() << " " << obj->ClassName()<< endl;
			c->SetName(obj->GetName());
			if(m_hasData==1){
				if (obj->InheritsFrom("TH1D")) {
					//obj->Draw();
					//c->Print(Form("%s",outps.c_str()));
					//c->Divide(1,2,0.,0.);

					char hname[512];
					sprintf(hname,"%s/%s",m_histodir.c_str(),obj->GetName());

					vector<double> vecMinMax;
					double min,max;
					double maxHeight=0.;
					TH1D* hSumMC;
					TH1D* hData;
					TH1D* hDataOverMC= new TH1D();
					vector<TH1D*> vechMC;
					THStack *hs = new THStack("hs","");

					for(int k=0;k<m_files.size();k++)
					{

					TH1D* hist = (TH1D*) m_files[k]->Get(hname);
					if(hist==NULL) continue;
					Color_t color=atof(m_colors[k].c_str());
					if(k==0) {
					vecMinMax=FindFilledMinMax(hist);
					min=vecMinMax[0];
					max=vecMinMax[1];
					//maxHeight=(hist->GetMaximum())*2.;
					maxHeight=(hist->GetMaximum())*1.;
					}

					double intg = hist->Integral();
					char title[256];
					sprintf(title,"%s (%.2e)", m_descriptions[k].c_str(), intg);
					hist->SetTitle(title);
					if(m_zoomToFilledXRange==1) hist->GetXaxis()->SetRangeUser(min,max);
					if(m_isLogScale==1) hist->SetMinimum(1);
					else hist->SetMinimum(0);
					hist->SetMaximum(maxHeight);
					if(k==0)
						{
						hist->SetLineColor(color);
						hist->SetMarkerStyle(8);
						hData=hist;
						}
					else{
						if(k==1) {hSumMC=(TH1D*) hist->Clone();
						hSumMC->SetTitle("#Sigma MC");
						hSumMC->SetLineColor(2);
						}
						else hSumMC->Add(hist);

						hist->SetFillColor(color);
						//hist->SetLineColor(color);
						hist->SetLineColor(1);
						vechMC.push_back(hist);
						}
					}

					if(m_files.size()>=2){
					//hDataOverMC->Divide(hData,hSumMC);
					hDataOverMC = (TH1D*)hData->Clone();
					hDataOverMC->Divide(hSumMC);
					double maxratio = hDataOverMC->GetBinContent(hDataOverMC->GetMaximumBin());
					hDataOverMC->SetMaximum(maxratio);
					}


					TPad* pads[2];
					if(m_showDatatoMCRatio==1){
					 pads[0] = new TPad("upperPad", "upperPad",
			      	 .005, .30, .99, .90);
					pads[0]->SetTopMargin(0.);
					pads[0]->SetBottomMargin(0.);
  			 		pads[1] = new TPad("lowerPad", "lowerPad",
			      	 .005, .005, .99, .30);
					pads[1]->SetTopMargin(0.);
					pads[0]->Draw();
					pads[1]->Draw();
					}
					else{
					pads[0] = new TPad("upperPad", "upperPad",
			      	 .005, .1, .99, .90);
					pads[0]->SetTopMargin(0.);
					pads[0]->Draw();
					}

					pads[0]->cd();

					if(m_showData==1) hData->Draw("goff E");
					else{
						hData->SetLineColor(0);
						hData->SetMarkerColor(0);
						hData->Draw("HIST");
					}

					if(m_files.size()>=2){
					if(m_isStacked==1){
						vector<TH1D*> vechMCSorted = vechMC;
						std::sort(vechMCSorted.begin(),vechMCSorted.end(),HistHasSmallerIntg);
						for(int k=0;k<vechMCSorted.size();k++){
							hs->Add(vechMCSorted[k]);
							}
						if(m_showData==1) hs->Draw("goff HIST SAME");
						else hs->Draw("goff HIST");
						hs->GetXaxis()->SetTitle(hSumMC->GetXaxis()->GetTitle());
						}

					else{
						vector<TH1D*> vechMCSorted = vechMC;
						std::sort(vechMCSorted.begin(),vechMCSorted.end(),HistHasLargerIntg);
						for(int k=0;k<vechMCSorted.size();k++){
							if(k==0) vechMCSorted[k]->Draw("goff HIST");
							else vechMCSorted[k]->Draw("goff HIST SAME");
							}
					}

					hSumMC->Draw("goff HIST SAME");
					}

					if(m_showData==1) hData->Draw("goff E SAME");

					gPad->SetLogy(m_isLogScale);
					if(m_showDatatoMCRatio==1){
					pads[1]->cd();
					if(hDataOverMC!=NULL) hDataOverMC->Draw("E");
					}

					c->cd();
					//TLegend* leg = new TLegend(0.1,0.90,0.95,0.99);
					TLegend* leg = new TLegend(0.105,0.90,0.945,0.99);
					leg->SetNColumns(3);
					leg->SetFillColor(0);
					if(m_showData==1) leg->AddEntry(hData,hData->GetTitle());
					leg->AddEntry(hSumMC,hSumMC->GetTitle());
					for(int k=0;k<vechMC.size();k++) leg->AddEntry(vechMC[k],vechMC[k]->GetTitle());
					leg->Draw();

					c->Write();
					c->Print(Form("%s",outps.c_str()));

					if(m_showDatatoMCRatio==1){for(int k=0;k<2;k++) {delete pads[k];}	}
					else{for(int k=0;k<1;k++) {delete pads[k];}}
					c->Clear();

					delete leg;
					delete hs;
					delete hDataOverMC;
      	 }

				if(obj->InheritsFrom("TH2D")) {
					char hname[512];
					sprintf(hname,"%s/%s",m_histodir.c_str(),obj->GetName());

					c->Divide(4,4);
					gStyle->SetOptTitle(1);
					for(int k=0;k<m_files.size();k++){
						TH2D* hist = (TH2D*) m_files[k]->Get(hname);
						if(hist==NULL) continue;
						hist->SetTitle(m_descriptions[k].c_str());
						c->cd(k+1);
						hist->Draw("goff colz");
						gPad->SetLogz(m_isLogScale);
						}
					c->Print(Form("%s",outps.c_str()));
					gStyle->SetOptTitle(0);
					c->Clear();
					}
				}
				else if(m_hasData==0){
				if (obj->InheritsFrom("TH1D")) {
					char hname[512];
					sprintf(hname,"%s/%s",m_histodir.c_str(),obj->GetName());

					vector<double> vecMinMax;
					double min,max;
					double maxHeight=0.;
					TH1D* hSumMC;
					vector<TH1D*> vechMC;
					THStack *hs = new THStack("hs","");

					for(int k=0;k<m_files.size();k++)
					{
						TH1D* hist = (TH1D*) m_files[k]->Get(hname);
						if(hist==NULL) continue;
						Color_t color=atof(m_colors[k].c_str());
						if(k==0) {
						vecMinMax=FindFilledMinMax(hist);
						min=vecMinMax[0];
						max=vecMinMax[1];
						maxHeight=(hist->GetMaximum())*1.;
						}

						double intg = hist->Integral();
						char title[256];
						sprintf(title,"%s (%.2e)", m_descriptions[k].c_str(), intg);
						hist->SetTitle(title);
						cout << min << " " << max << endl;
						if(m_zoomToFilledXRange==1) {hist->GetXaxis()->SetRangeUser(min,max);
							//cout << min<< " " << max<<endl;sleep(2);
						}
						if(m_isLogScale==1) hist->SetMinimum(1);
						else hist->SetMinimum(0);
						hist->SetMaximum(maxHeight);
						if(k==0) {hSumMC=(TH1D*) hist->Clone();
							hSumMC->SetTitle("#Sigma MC");
							hSumMC->SetLineColor(2);
						}
						else hSumMC->Add(hist);

						hist->SetFillColor(color);
						hist->SetLineColor(1);
						vechMC.push_back(hist);

					}

					TPad* pads[2];
					if(m_showDatatoMCRatio==1){
					 pads[0] = new TPad("upperPad", "upperPad",
			      	 .005, .30, .99, .90);
					pads[0]->SetTopMargin(0.);
					pads[0]->SetBottomMargin(0.);
  			 		pads[1] = new TPad("lowerPad", "lowerPad",
			      	 .005, .005, .99, .30);
					pads[1]->SetTopMargin(0.);
					pads[0]->Draw();
					pads[1]->Draw();
					}
					else{
					pads[0] = new TPad("upperPad", "upperPad",
			      	 .005, .1, .99, .90);
					pads[0]->SetTopMargin(0.);
					pads[0]->Draw();
					}

					pads[0]->cd();

					if(m_files.size()>=1){
					if(m_isStacked==1){
						vector<TH1D*> vechMCSorted = vechMC;
						std::sort(vechMCSorted.begin(),vechMCSorted.end(),HistHasSmallerIntg);
						for(int k=0;k<vechMCSorted.size();k++){
							hs->Add(vechMCSorted[k]);
							}
						hs->Draw("goff HIST");
						hs->GetXaxis()->SetTitle(hSumMC->GetXaxis()->GetTitle());
						}
					else{
						vector<TH1D*> vechMCSorted = vechMC;
						std::sort(vechMCSorted.begin(),vechMCSorted.end(),HistHasLargerIntg);
						for(int k=0;k<vechMCSorted.size();k++){
							if(k==0) vechMCSorted[k]->Draw("goff HIST");
							else vechMCSorted[k]->Draw("goff HIST SAME");
							}
					}

					hSumMC->Draw("goff HIST SAME");
					}

					gPad->SetLogy(m_isLogScale);

					c->cd();
					//TLegend* leg = new TLegend(0.1,0.90,0.95,0.99);
					TLegend* leg = new TLegend(0.105,0.90,0.945,0.99);
					leg->SetNColumns(3);
					leg->SetFillColor(0);
					leg->AddEntry(hSumMC,hSumMC->GetTitle());
					for(int k=0;k<vechMC.size();k++) leg->AddEntry(vechMC[k],vechMC[k]->GetTitle());
					leg->Draw();

					c->Write();
					c->Print(Form("%s",outps.c_str()));

					for(int k=0;k<1;k++) {delete pads[k];}
					c->Clear();

					delete leg;
					delete hs;
					}
				}
			}
c->Print(Form("%s]",outps.c_str()));
outfile->Close();
}
//Convert ps file into pdf with ps2pdf (can be properly view by evince)
//On Himster environment, pdf file created by ROOT is corrupted when opened with evince
//To view the pdf : evince filename.pdf
char systcmd[256];
sprintf(systcmd,"ps2pdf %s %s",outps.c_str(), outpdf.c_str());
system(systcmd);
sprintf(systcmd,"rm %s",outps.c_str());
system(systcmd);

return;
}

void CompDataMCScaledPlotter::runAll(string inputcard, string option, string outname){

string fullOutname;
char fullOption[256];
//Get the first file
ParseInputFile(inputcard);
vector<string> strsplit = Tokenize(option,' ');
string dirname;
for(int k=0;k<strsplit.size();k++){
	vector<string> strArgVal = Tokenize(strsplit[k],'=');
	string arg=strArgVal[0];
	string val=strArgVal[1];
	if(arg=="Histo" ) {dirname= val;}
}

string firstFileName(m_files[0]->GetName());
if(option.find("Histo=Untagged")!=string::npos)
	{
	int nStages=getNumberOfSubDir(firstFileName,"Tagged", "Stage");
	cout << nStages << endl;
	for(int k=0;k<nStages;k++)
		{
		fullOutname=outname+"/Untagged-"+SSTR(k);
		sprintf(fullOption,"Histo=Untagged/Stage%d",k);
		run("DrawDataMCScaled.in",fullOption,fullOutname);
		}
	}
else if(option.find("Histo=Tagged")!=string::npos)
	{
	int nStages=getNumberOfSubDir(firstFileName,"Tagged", "Stage");
	cout << nStages << endl;

	for(int k=0;k<nStages;k++)
	//for(int k=4;k<=4;k++)
		{

		if(option.find("isLogScale=0")!=string::npos){
			 fullOutname=outname+"/Tagged-"+SSTR(k);
			sprintf(fullOption,"Histo=Tagged/Stage%d isLogScale=0",k);
			run(inputcard,fullOption,fullOutname);

			fullOutname=outname+"/eTagged-"+SSTR(k);
			sprintf(fullOption,"Histo=eTagged/Stage%d isLogScale=0",k);
			run(inputcard,fullOption,fullOutname);

			fullOutname=outname+"/pTagged-"+SSTR(k);
			sprintf(fullOption,"Histo=pTagged/Stage%d isLogScale=0",k);
			run(inputcard,fullOption,fullOutname);
		}
		else{
			
			fullOutname=outname+"/Tagged-"+SSTR(k);
			sprintf(fullOption,"Histo=Tagged/Stage%d isLogScale=1",k);
			run(inputcard,fullOption,fullOutname);

			fullOutname=outname+"/eTagged-"+SSTR(k);
			sprintf(fullOption,"Histo=eTagged/Stage%d isLogScale=1",k);
			run(inputcard,fullOption,fullOutname);

			fullOutname=outname+"/pTagged-"+SSTR(k);
			sprintf(fullOption,"Histo=pTagged/Stage%d isLogScale=1",k);
			run(inputcard,fullOption,fullOutname);
			
			/*
			fullOutname=outname+"/Tagged-"+SSTR(k);
			sprintf(fullOption,"Histo=Tagged/Stage%d isLogScale=1 hasData=0",k);
			run(inputcard,fullOption,fullOutname);

			fullOutname=outname+"/eTagged-"+SSTR(k);
			sprintf(fullOption,"Histo=eTagged/Stage%d isLogScale=1 hasData=0",k);
			run(inputcard,fullOption,fullOutname);

			fullOutname=outname+"/pTagged-"+SSTR(k);
			sprintf(fullOption,"Histo=pTagged/Stage%d isLogScale=1 hasData=0",k);
			run(inputcard,fullOption,fullOutname);
			*/
		}
		}
	}
else{
	int nStages=getNumberOfSubDir(firstFileName,dirname, "Stage");
	for(int k=0;k<nStages;k++)
		{
			fullOutname=outname+"/"+dirname+"-"+SSTR(k);
			sprintf(fullOption,"Histo=%s/Stage%d hasData=0 isLogScale=1 zoomToFilledXRange=1",dirname.c_str(),k);
			run(inputcard,fullOption,fullOutname);
		}
}
return;
}
