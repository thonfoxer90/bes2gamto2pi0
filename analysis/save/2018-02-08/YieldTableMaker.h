/*
 * YieldTableMaker.h
 *
 *  Created on: Jan 18, 2018
 *      Author: bgarillo
 */

#ifndef YIELDTABLEMAKER_H_
#define YIELDTABLEMAKER_H_

#include <TFile.h>
#include <TDirectory.h>
#include <TTree.h>
#include <TChain.h>
#include <TCanvas.h>
#include <TPad.h>
#include <TH1.h>
#include <TH2.h>
#include <THStack.h>
#include <TLine.h>
#include <stdio.h>
#include <string.h>
#include <iostream> //std::cout
#include <sstream>
#include <fstream> //std::ifstream,ofstream
#include <vector>
#include <algorithm> //std::sort
#include <TKey.h>
#include <TList.h>
#include <TStyle.h>
#include <TROOT.h>
#include <TLegend.h>
#include <TLegendEntry.h>

#include "MyGlobal.h"

using namespace std;

class YieldTableMaker {
public:
	YieldTableMaker();
	YieldTableMaker(string inputcard, string option, string outpath);
	virtual ~YieldTableMaker();
	int getNumberOfSubDir(string inputfile,string dir, string keyword);
	void run();
private:
	string m_inputcard;
	string m_option;
	string m_outpath;
	vector <string> m_files;
	vector <string> m_tags;
	vector < vector <double > > m_yields;
};

#endif /* YIELDTABLEMAKER_H_ */
