/*
 * PartialWaveFitter.h
 *
 *  Created on: Feb 5, 2018
 *      Author: bgarillo
 */

#ifndef PARTIALWAVEFITTER_H_
#define PARTIALWAVEFITTER_H_

#include <iostream>             // std::cout, std::endl
#include <fstream>              // std::ifstream
#include <sstream>
#include <cstdlib>
#include <map>
#include <time.h>

#include <TROOT.h>
#include <TSystem.h>
#include <TObject.h>
#include <TRint.h>
#include <TString.h>
#include <TTree.h>
#include <TChain.h>
#include <TFile.h>
#include <TKey.h>
#include <TList.h>
#include <TStyle.h>
#include <TSelector.h>
#include <TF1.h>
#include <TH2.h>
#include <TH1.h>
#include <THnSparse.h>
#include <TGraph2D.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TMultiGraph.h>
#include <TVirtualIndex.h>
#include <TPad.h>
#include <TCanvas.h>
#include <TGaxis.h>
#include <TPave.h>
#include <TPaveText.h>
#include <TLatex.h>
#include <TVector3.h>
#include <TLorentzVector.h>


#include "RooGlobalFunc.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooConstVar.h"
#include "RooAddPdf.h"
#include "RooGaussian.h"
#include "RooCBShape.h"
#include "RooArgusBG.h"
#include "RooPlot.h"

#include "MyGlobal.h"

using namespace std;
//using namespace RooFit ;

class PartialWaveFitter {
public:
	PartialWaveFitter();
	PartialWaveFitter(string inputfile,string option, string outpath);
	virtual ~PartialWaveFitter();
	Double_t  funcMag2Y00(Double_t *x, Double_t *par);
	Double_t  funcMag2Y02(Double_t *x, Double_t *par);
	Double_t  funcMag2Y12(Double_t *x, Double_t *par);
	Double_t  funcMag2Y22(Double_t *x, Double_t *par);
	Double_t fitFunction(Double_t *x, Double_t *par);
	void run();
	void drawFits();
	void drawPartialWaves();
private:
	string m_inputfile;
	string m_option;
	string m_outpath;

	int m_fitMode;
	TFile* m_infile;
	THnSparseD* m_hnYield;

	TFile* m_outROOT;

	string m_outpdf;

	map<vector<int>,vector<double> > m_mapFitPars;
	map<vector<int>,vector<double> > m_mapFitParsErr;

	map<vector<string>, TGraphErrors* > m_mapParsGraphs;

};

#endif /* PARTIALWAVEFITTER_H_ */
