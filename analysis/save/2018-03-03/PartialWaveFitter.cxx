/*
 * PartialWaveFitter.cxx
 *
 *  Created on: Feb 5, 2018
 *      Author: bgarillo
 */

#include "PartialWaveFitter.h"

PartialWaveFitter::PartialWaveFitter(): m_fitMode(0) {
	// TODO Auto-generated constructor stub

}

PartialWaveFitter::PartialWaveFitter(string inputfile,string option, string outpath):
		m_inputfile(inputfile),
		m_option(option),
		m_outpath(outpath){
	// TODO Auto-generated constructor stub
		gROOT->ProcessLine(".x ~/.rootlogon.C");
		m_infile = TFile::Open(m_inputfile.c_str());
		m_outROOT = new TFile((m_outpath+".root").c_str(),"RECREATE");
		m_hnYield = (THnSparseD*)m_infile->Get("hnDWYieldgg");
}

PartialWaveFitter::~PartialWaveFitter() {
	// TODO Auto-generated destructor stub
	delete m_infile;
	delete m_outROOT;
}

Double_t PartialWaveFitter::funcMag2Y00(Double_t *x, Double_t *par){
	Double_t f= (4*TMath::Pi())*1/(4*TMath::Pi())*par[0];
	return f;
}

Double_t PartialWaveFitter::funcMag2Y02(Double_t *x, Double_t *par){
	Double_t xx=x[0];
	Double_t f= (4*TMath::Pi())*(5/(16*TMath::Pi()))*pow((3*pow(xx,2)-1),2)*par[0];
	return f;
}

Double_t PartialWaveFitter::funcMag2Y12(Double_t *x, Double_t *par){
	Double_t xx=x[0];
	Double_t f= (4*TMath::Pi())*(15/(8*TMath::Pi()))*(1-pow(xx,2))*pow(xx,2)*par[0];
	return f;
}

Double_t PartialWaveFitter::funcMag2Y22(Double_t *x, Double_t *par){
	Double_t xx=x[0];
	Double_t f= (4*TMath::Pi())*(15/(32*TMath::Pi()))*(1-pow(xx,2))*par[0];
	return f;
}

Double_t PartialWaveFitter::fitFunction(Double_t *x, Double_t *par){
	Double_t xx=x[0]; //cos theta
	Double_t mag2Y00=1/(4*TMath::Pi());
	Double_t mag2Y02=(5/(16*TMath::Pi()))*pow((3*pow(xx,2)-1),2);
	Double_t mag2Y12=(15/(8*TMath::Pi()))*(1-pow(xx,2))*pow(xx,2);
	Double_t mag2Y22=(15/(32*TMath::Pi()))*(1-pow(xx,2));
	Double_t f=(4*TMath::Pi())*(par[0]*mag2Y00+par[1]*mag2Y02+par[2]*mag2Y12+par[3]*mag2Y22);
	return f;
}


void PartialWaveFitter::run(){
	int nDim = m_hnYield->GetNdimensions();
	vector<int> nBins;nBins.resize(nDim);
	vector<string> axisName;axisName.resize(nDim);
	map<string,int> mapNBins;
	for(int k=0;k<nDim;k++){
		nBins[k]= m_hnYield->GetAxis(k)->GetNbins();
		axisName[k] = string(m_hnYield->GetAxis(k)->GetName());
		mapNBins[axisName[k]] = nBins[k];
		cout << nBins[k] << " " << axisName[k] << endl;
	}
	//sleep(3);
	int nPars = 4;
	//RooRealVar x("x","x",0.,1.2) ;

	//RooRealVar cbmean("cbmean", "cbmean" , 0.6, 0.5, 1.) ;
	//RooRealVar cbsigma("cbsigma", "cbsigma" , 0.02, 0., 0.5) ;
	TIter nextkey(m_infile->GetListOfKeys());
	TKey *key;
	while ((key = (TKey*)nextkey())) {
		TObject *obj = (TObject*)key->ReadObj();
		cout << "Reading: " << obj->GetName() << " " << obj->ClassName()<< endl;
		if((TString(obj->GetName())).Contains("dsigmaggdAbsCThetaSt")){
			TH1D* histo = (TH1D*)obj;
			string hname(histo->GetName());
			string fitname="fit"+hname;
			//Retrieve bin coordinate from name
			vector<int> binCoords =MyGlobal::getBinCoord(hname);
			//TF1* fit = new TF1(fitname.c_str(),fitFunction,0,1,4);
			TF1* fit = new TF1(fitname.c_str(),this,&PartialWaveFitter::fitFunction,0,1,nPars,"PartialWaveFitter","fitFunction");
			for(int k=0;k<nPars;k++){
				string parname;
				if(k==0) parname="mag2Y00";
				if(k==1) parname="mag2Y02";	
				if(k==2) parname="mag2Y12";
				if(k==3) parname="mag2Y22";
				fit->SetParName(k,parname.c_str());
				fit->SetParLimits(k,0.,100.);
			}
			histo->Fit(fitname.c_str());
			//Extract fit parameters and errors
			//int nPars = fit->GetNpar();
			vector<double> vecPars;
			vecPars.resize(nPars);
			vector<double> vecParErrs;
			vecParErrs.resize(nPars);
			vector<TF1*> fitComponents;
			fitComponents.resize(nPars);
			for(int k=0;k<nPars;k++){
				vecPars[k]=fit->GetParameter(k);
				vecParErrs[k]=fit->GetParError(k);
				//cout << vecPars[k] << " " << vecParErrs[k]<< endl;
			}
			//cout << endl;
			m_mapFitPars[binCoords]=vecPars;
			m_mapFitParsErr[binCoords]=vecParErrs;
			
			fitname="fitY00"+hname;
			fitComponents[0] = new TF1(fitname.c_str(),this,&PartialWaveFitter::funcMag2Y00,0,1,1,"PartialWaveFitter","funcMag2Y00");
			fitComponents[0]->SetParameter(0,fit->GetParameter(0));
			fitComponents[0]->SetLineColor(kBlue);
			
			fitname="fitY02"+hname;
			fitComponents[1] = new TF1(fitname.c_str(),this,&PartialWaveFitter::funcMag2Y02,0,1,1,"PartialWaveFitter","funcMag2Y02");
			fitComponents[1]->SetParameter(0,fit->GetParameter(1));
			fitComponents[1]->SetLineColor(kMagenta);
			
			fitname="fitY12"+hname;
			fitComponents[2] = new TF1(fitname.c_str(),this,&PartialWaveFitter::funcMag2Y12,0,1,1,"PartialWaveFitter","funcMag2Y12");
			fitComponents[2]->SetParameter(0,fit->GetParameter(2));
			fitComponents[2]->SetLineColor(kMagenta+1);
			
			fitname="fitY22"+hname;
			fitComponents[3] = new TF1(fitname.c_str(),this,&PartialWaveFitter::funcMag2Y22,0,1,1,"PartialWaveFitter","funcMag2Y22");
			fitComponents[3]->SetParameter(0,fit->GetParameter(3));
			fitComponents[3]->SetLineColor(kMagenta+2);
			
			for(int k=0;k<fitComponents.size();k++) fitComponents[k]->Write();
			
			m_outROOT->cd();
			histo->Write();
			fit->Write();
			delete histo;
			delete fit;
			for(int k=0;k<fitComponents.size();k++) delete fitComponents[k];
		}
	}
	/*
	//Retrieve the number of kinematic bins
	vector<int> nBins;nBins.resize(2);
	nBins[0]=0; nBins[1]=0;
	for (map<vector<int>,vector<double> >::iterator it=m_mapFitPars.begin(); it!=m_mapFitPars.end(); ++it){
		vector<int> key = it->first;
		for(int k=0;k<key.size();k++){
			//cout << key[k] << " ";
			if(key[k]>nBins[k])nBins[k]=key[k];
		}
		//cout << endl;
	}
	cout << nBins[0] << " " << nBins[1] << endl;sleep(2);
	*/
	for(int i=0;i<nPars;i++){
		string parname;
		Color_t color;
		if(i==0) {parname="mag2Y00";color=kBlue;}
		if(i==1) {parname="mag2Y02";color=kMagenta;}	
		if(i==2) {parname="mag2Y12";color=kMagenta+1;}
		if(i==3) {parname="mag2Y22";color=kMagenta+2;}
		for(int j=0;j<m_hnYield->GetAxis(0)->GetNbins();j++){
			vector<double> vecX;
			vector<double> vecErrX;
			vector<double> vecY;
			vector<double> vecErrY;
			for(int k=0;k<m_hnYield->GetAxis(1)->GetNbins();k++){
				vector<int> binCoords;binCoords.resize(2);
				binCoords[0]=j;binCoords[1]=k;
				if(m_mapFitPars.count(binCoords)!=1) continue;
				double par = (m_mapFitPars[binCoords])[i];
				double parErr = (m_mapFitParsErr[binCoords])[i];
				//cout << j << " " << k << " " << par << " " << parErr << endl;
				vecX.push_back(m_hnYield->GetAxis(1)->GetBinCenter(k+1));
				vecErrX.push_back(0);
				vecY.push_back(par);
				vecErrY.push_back(0);
			}
			
			char grNameCstr[256];
			sprintf(grNameCstr,"gr%s_%s_%d",parname.c_str(),m_hnYield->GetAxis(0)->GetName(),j);
			TGraphErrors *gr = new TGraphErrors(vecX.size(),&vecX[0],&vecY[0],&vecErrX[0],&vecErrY[0]);
			gr->SetName(grNameCstr);
			char title[256];
			sprintf(title,"%.2f #leq Q^{2} (GeV^{2}) < %.2f",m_hnYield->GetAxis(0)->GetBinLowEdge(j+1),m_hnYield->GetAxis(0)->GetBinUpEdge(j+1));
			gr->SetTitle(title);
			gr->GetXaxis()->SetTitle("W [GeV]");
			gr->SetMarkerStyle(8);
			gr->SetLineColor(color);
			gr->Write();
			delete gr;
			
		}
		
	}
	drawFits();
	drawPartialWaves();
}

void PartialWaveFitter::drawFits(){
	string outpdf = m_outpath+"_fitCTheta.pdf";
	TCanvas *c = new TCanvas();
	c->Print((Form("%s[",outpdf.c_str())));
	for(int i=0;i<m_hnYield->GetAxis(0)->GetNbins();i++){
		for(int j=0;j<m_hnYield->GetAxis(1)->GetNbins();j++){
		 char name[256];
		 sprintf(name,"dsigmaggdAbsCThetaStAll_Q2M2Pi_%d-%d",i,j);
		 if((TH1D*)m_outROOT->Get(name)==NULL) continue;
		 TH1D* histo = (TH1D*)m_outROOT->Get(name);
		 histo->Draw();
		 
		 sprintf(name,"fitY00dsigmaggdAbsCThetaStAll_Q2M2Pi_%d-%d",i,j);
		 TF1* grY00 = (TF1*)m_outROOT->Get(name);
		 grY00->Draw("same");
		 
		 sprintf(name,"fitY02dsigmaggdAbsCThetaStAll_Q2M2Pi_%d-%d",i,j);
		 TF1* grY02 = (TF1*)m_outROOT->Get(name);
		 grY02->Draw("same");
		 
		 sprintf(name,"fitY12dsigmaggdAbsCThetaStAll_Q2M2Pi_%d-%d",i,j);
		 TF1* grY12 = (TF1*)m_outROOT->Get(name);
		 grY12->Draw("same");
		 
		 sprintf(name,"fitY22dsigmaggdAbsCThetaStAll_Q2M2Pi_%d-%d",i,j);
		 TF1* grY22 = (TF1*)m_outROOT->Get(name);
		 grY22->Draw("same");
		 
		 c->Print((Form("%s",outpdf.c_str())));
		}
	}
	c->Print((Form("%s]",outpdf.c_str())));
}
void PartialWaveFitter::drawPartialWaves(){	
	string outpdf = m_outpath+".pdf";
	TCanvas *c = new TCanvas();
	c->Print((Form("%s[",outpdf.c_str())));
	for(int i=0;i<m_hnYield->GetAxis(0)->GetNbins();i++){
		char name[256];
		sprintf(name,"grmag2Y00_Q2_%d",i);
		TGraphErrors* grY00 = (TGraphErrors*)m_outROOT->Get(name);
		sprintf(name,"grmag2Y02_Q2_%d",i);
		TGraphErrors* grY02 = (TGraphErrors*)m_outROOT->Get(name);
		sprintf(name,"grmag2Y12_Q2_%d",i);
		TGraphErrors* grY12 = (TGraphErrors*)m_outROOT->Get(name);
		sprintf(name,"grmag2Y22_Q2_%d",i);
		TGraphErrors* grY22 = (TGraphErrors*)m_outROOT->Get(name);
		
		TMultiGraph *mg = new TMultiGraph();
		mg->Add(grY00,"C");
		mg->Add(grY02,"C");
		mg->Add(grY12,"C");
		mg->Add(grY22,"C");
		mg->SetTitle(grY00->GetTitle());
		//mg->GetXaxis()->SetTitle(grY00->GetXaxis()->GetTitle());
		//mg->GetXaxis()->SetTitle("W [GeV]");
		mg->Draw("A");
		mg->GetXaxis()->SetTitle(grY00->GetXaxis()->GetTitle());
		
		c->Print((Form("%s",outpdf.c_str())));
		
		delete mg;
	}
	c->Print((Form("%s]",outpdf.c_str())));
	/*
	TIter nextkey(m_outROOT->GetListOfKeys());
	TKey *key;
	while ((key = (TKey*)nextkey())) {
		TObject *obj = (TObject*)key->ReadObj();
		if((TString(obj->GetName())).Contains("grmag2Y00")){
			TGraphErrors* gr = (TGraphErrors*) obj;
			obj->Draw("AP");
			
			c->Print((Form("%s",outpdf.c_str())));
		}
	}
	c->Print((Form("%s]",outpdf.c_str())));
	*/
	
}
