if test "${CMTROOT}" = ""; then
  CMTROOT=/cluster/bes3/CMT/v1r20p20090520; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh
tempfile=`${CMTROOT}/mgr/cmt -quiet build temporary_name`
if test ! $? = 0 ; then tempfile=/tmp/cmt.$$; fi
${CMTROOT}/mgr/cmt cleanup -sh -pack=Bes2Gamto2Pi0 -version=Bes2Gamto2Pi0-00-00-01 -path=/home/bgarillo/boss-6.6.4/Analysis $* >${tempfile}; . ${tempfile}
/bin/rm -f ${tempfile}

