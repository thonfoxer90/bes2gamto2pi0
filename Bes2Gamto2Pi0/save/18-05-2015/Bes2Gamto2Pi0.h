#include "EventModel/EventHeader.h"
#include "EventModel/Event.h"
#include "EvtRecEvent/EvtRecEvent.h"
#include "EvtRecEvent/EvtRecTrack.h"
#include "EvtRecEvent/EvtRecVeeVertex.h"
#include "EvtRecEvent/EvtRecPi0.h"
#include "EventNavigator/EventNavigator.h"

#include <string>
#include <TMath.h>
#include <TLorentzVector.h>
#include <TVector3.h>
//#include "PartPropSvc/PartPropSvc.h"
//#include "BesDChain/CDPi0List.h"

#include "GaudiKernel/Bootstrap.h"
#include "GaudiKernel/IJobOptionsSvc.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/SmartDataPtr.h"
#include "GaudiKernel/Algorithm.h"
#include "GaudiKernel/NTuple.h"

#include "McTruth/McParticle.h"
#include "ParticleID/ParticleID.h"

#include "VertexFit/VertexFit.h"
#include "VertexFit/SecondVertexFit.h"
#include "VertexFit/VertexDbSvc.h"
#include "VertexFit/IVertexDbSvc.h"
#include "VertexFit/Helix.h"
#include "VertexFit/KalmanKinematicFit.h"

#include "TRandom.h"
#include "CLHEP/Matrix/SymMatrix.h"
#include "CLHEP/Matrix/Matrix.h" 
#include "CLHEP/Matrix/Vector.h"
#include "CLHEP/Vector/LorentzVector.h"
#include "CLHEP/Vector/ThreeVector.h"
#include "VertexFit/WTrackParameter.h"
#include "MdcRecEvent/RecMdcTrack.h"
#include "MdcRecEvent/RecMdcKalTrack.h"

#include "Pi0recHelper.h"
#include "ParticleContainer.h"

# define GAMMAS 10

class Bes2Gamto2Pi0 : public Algorithm {

 public:

  Bes2Gamto2Pi0(const std::string& name, ISvcLocator*pSvcLocator);

  StatusCode initialize();
  StatusCode execute();
  StatusCode finalize();

  // 5pi
  NTuple::Tuple*            m_N5Pi;
  NTuple::Item<double>      m_d5NRunNumber;
  //NTuple::Item<double>      m_d5UEvtNumber;
  NTuple::Item<double>      m_d5NCMSEnergy;
  NTuple::Item<long>        m_i5NAnzGoodP;
  NTuple::Item<long>        m_i5NAnzGoodM;
  NTuple::Item<long>        m_i5NAnzGoodGamma;
  NTuple::Item<long>        m_i5NAnzPi0Tracks;
  NTuple::Item<long>        m_i5NAnz2Combs;
  NTuple::Item<long>        m_i5NAnz3Combs;
  NTuple::Item<double>      m_d5Nchisq4piT;
  NTuple::Item<double>      m_d5Nmhad4piT;
  NTuple::Item<double>      m_d5Nchisq4piU;
  NTuple::Item<double>      m_d5Nmhad4piU;
  NTuple::Item<double>      m_d5NPiNonRadChi2;
  NTuple::Item<double>      m_d5NPiIsrUChi2;
  NTuple::Item<double>      m_d5NPiIsrTChi2;
  NTuple::Item<double>      m_d5NPi01Chi2;
  NTuple::Item<double>      m_d5NPi02Chi2;
  NTuple::Item<double>      m_d5NPi03Chi2;
  NTuple::Item<long>        m_i5NBgrVeto;
  NTuple::Item<long>        m_i5NmcTruePi;
  NTuple::Item<long>        m_i5NmcTruePi0;
  NTuple::Item<long>        m_i5NmcTrueISR;
  NTuple::Item<long>        m_i5NmcTrueGammaFromJpsi;
  NTuple::Item<long>        m_i5NmcTrueOtherGamma;
  NTuple::Item<long>        m_i5NmcTrueOther;
  NTuple::Item<double>      m_d5NDistZp;
  NTuple::Item<double>      m_d5NDistRp;
  NTuple::Item<double>      m_d5NDistZm;
  NTuple::Item<double>      m_d5NDistRm;
  NTuple::Item<double>      m_d5NVtxChi2;
  NTuple::Item<double>      m_d5NVtxPosX;
  NTuple::Item<double>      m_d5NVtxPosY;
  NTuple::Item<double>      m_d5NVtxPosZ;
  // KsKpi veto
  NTuple::Item<double>      m_d5NProb1Pi;
  NTuple::Item<double>      m_d5NProb1K;
  NTuple::Item<double>      m_d5NProb1P;
  NTuple::Item<double>      m_d5NProb2Pi;
  NTuple::Item<double>      m_d5NProb2K;
  NTuple::Item<double>      m_d5NProb2P;
  // detector
  cParticleContainer        m_5NPi01;
  cParticleContainer        m_5NPi02;
  cParticleContainer        m_5NPi03;
  cParticleContainer        m_5NPi1;
  cParticleContainer        m_5NPi2;
  cParticleContainer        m_5NGammas[GAMMAS+6];
  NTuple::Item<double>      m_d5NClosestTrack[GAMMAS+6];
  // fit
  cParticleContainer        m_5NFitPi01;
  cParticleContainer        m_5NFitPi02;
  cParticleContainer        m_5NFitPi03;
  cParticleContainer        m_5NFitPi1;
  cParticleContainer        m_5NFitPi2;
  cParticleContainer        m_5NFitGammas[6];

  // 4pi
  NTuple::Tuple*            m_N4Pi;
  NTuple::Item<double>      m_d4NRunNumber;
  //NTuple::Item<double>      m_d5UEvtNumber;
  NTuple::Item<double>      m_d4NCMSEnergy;
  NTuple::Item<long>        m_i4NAnzGoodP;
  NTuple::Item<long>        m_i4NAnzGoodM;
  NTuple::Item<long>        m_i4NAnzGoodGamma;
  NTuple::Item<long>        m_i4NAnzPi0Tracks;
  NTuple::Item<long>        m_i4NAnz2Combs;
  NTuple::Item<long>        m_i4NAnz3Combs;
  NTuple::Item<double>      m_d4NPiNonRadChi2;
  NTuple::Item<double>      m_d4NPiIsrUChi2;
  NTuple::Item<double>      m_d4NPiIsrTChi2;
  NTuple::Item<double>      m_d4NPi01Chi2;
  NTuple::Item<double>      m_d4NPi02Chi2;
  NTuple::Item<long>        m_i4NBgrVeto;
  NTuple::Item<long>        m_i4NmcTruePi;
  NTuple::Item<long>        m_i4NmcTruePi0;
  NTuple::Item<long>        m_i4NmcTrueISR;
  NTuple::Item<long>        m_i4NmcTrueGammaFromJpsi;
  NTuple::Item<long>        m_i4NmcTrueOtherGamma;
  NTuple::Item<long>        m_i4NmcTrueOther;
  NTuple::Item<double>      m_d4NDistZp;
  NTuple::Item<double>      m_d4NDistRp;
  NTuple::Item<double>      m_d4NDistZm;
  NTuple::Item<double>      m_d4NDistRm;
  NTuple::Item<double>      m_d4NVtxChi2;
  NTuple::Item<double>      m_d4NVtxPosX;
  NTuple::Item<double>      m_d4NVtxPosY;
  NTuple::Item<double>      m_d4NVtxPosZ;
  // KsKpi veto
  NTuple::Item<double>      m_d4NProb1Pi;
  NTuple::Item<double>      m_d4NProb1K;
  NTuple::Item<double>      m_d4NProb1P;
  NTuple::Item<double>      m_d4NProb2Pi;
  NTuple::Item<double>      m_d4NProb2K;
  NTuple::Item<double>      m_d4NProb2P;
  // detector
  cParticleContainer        m_4NPi01;
  cParticleContainer        m_4NPi02;
  cParticleContainer        m_4NPi1;
  cParticleContainer        m_4NPi2;
  cParticleContainer        m_4NGammas[GAMMAS+5];
  NTuple::Item<double>      m_d4NClosestTrack[GAMMAS+5];
  // fit
  cParticleContainer        m_4NFitPi01;
  cParticleContainer        m_4NFitPi02;
  cParticleContainer        m_4NFitPi1;
  cParticleContainer        m_4NFitPi2;
  cParticleContainer        m_4NFitGammas[4];


  // untagged 5pi
  NTuple::Tuple*            m_N5PiUntagged;
  NTuple::Item<double>      m_d5URunNumber;
  NTuple::Item<double>      m_d5UEvtNumber;
  NTuple::Item<double>      m_d5UCMSEnergy;
  NTuple::Item<long>        m_i5UAnzGoodP;
  NTuple::Item<long>        m_i5UAnzGoodM;
  NTuple::Item<long>        m_i5UAnzGoodGamma;
  NTuple::Item<long>        m_i5UAnzPi0Tracks;
  NTuple::Item<long>        m_i5UAnz2Combs;
  NTuple::Item<long>        m_i5UAnz3Combs;
  NTuple::Item<double>      m_d5UTotalChi2;
  NTuple::Item<double>      m_d5UPi01Chi2;
  NTuple::Item<double>      m_d5UPi02Chi2;
  NTuple::Item<double>      m_d5UPi03Chi2;
  NTuple::Item<double>      m_d5UsqrSHadSysFit;
  NTuple::Item<long>        m_i5UBgrVeto;
  NTuple::Item<long>        m_i5UmcTruePi;
  NTuple::Item<long>        m_i5UmcTruePi0;
  NTuple::Item<long>        m_i5UmcTrueISR;
  NTuple::Item<long>        m_i5UmcTrueGammaFromJpsi;
  NTuple::Item<long>        m_i5UmcTrueOtherGamma;
  NTuple::Item<long>        m_i5UmcTrueOther;
  NTuple::Item<double>      m_d5UmcMHad;
  /*NTuple::Item<double>      m_d5UPi1EoP;
  NTuple::Item<double>      m_d5UPi2EoP;
  NTuple::Item<double>      m_d5UPi1MucDepth;
  NTuple::Item<double>      m_d5UPi2MucDepth;*/
  NTuple::Item<double>      m_d5UDistZp;
  NTuple::Item<double>      m_d5UDistRp;
  NTuple::Item<double>      m_d5UDistZm;
  NTuple::Item<double>      m_d5UDistRm;
  NTuple::Item<double>      m_dU5PiNonRadChi2;
  NTuple::Item<double>      m_d5UVtxChi2;
  NTuple::Item<double>      m_d5UVtxPosX;
  NTuple::Item<double>      m_d5UVtxPosY;
  NTuple::Item<double>      m_d5UVtxPosZ;
  NTuple::Item<double>      m_d5UIPposX;
  NTuple::Item<double>      m_d5UIPposY;
  NTuple::Item<double>      m_d5UIPposZ;
  // KsKpi veto
  NTuple::Item<double>      m_d5UProb1Pi;
  NTuple::Item<double>      m_d5UProb1K;
  NTuple::Item<double>      m_d5UProb1P;
  NTuple::Item<double>      m_d5UProb2Pi;
  NTuple::Item<double>      m_d5UProb2K;
  NTuple::Item<double>      m_d5UProb2P;
  // detector
  cParticleContainer        m_5UPi01;
  cParticleContainer        m_5UPi02;
  cParticleContainer        m_5UPi03;
  cParticleContainer        m_5UPi1;
  cParticleContainer        m_5UPi2;
  cParticleContainer        m_5UGammas[GAMMAS+6];
  NTuple::Item<double>      m_d5UClosestTrack[GAMMAS+6];
  // fit
  cParticleContainer        m_5UFitPi01;
  cParticleContainer        m_5UFitPi02;
  cParticleContainer        m_5UFitPi03;
  cParticleContainer        m_5UFitPi1;
  cParticleContainer        m_5UFitPi2;
  cParticleContainer        m_5UFitGammas[7];
  cParticleContainer        m_5UTrueISR;

  // tagged 5pi
  NTuple::Tuple*            m_N5PiTagged;
  NTuple::Item<double>      m_d5RunNumber;
  NTuple::Item<double>      m_d5EvtNumber;
  NTuple::Item<double>      m_d5TCMSEnergy;
  NTuple::Item<long>        m_i5AnzGoodP;
  NTuple::Item<long>        m_i5AnzGoodM;
  NTuple::Item<long>        m_i5AnzGoodGamma;
  NTuple::Item<long>        m_i5AnzPi0Tracks;
  NTuple::Item<long>        m_i5Anz2Combs;
  NTuple::Item<long>        m_i5Anz3Combs;
  NTuple::Item<double>      m_d5TotalChi2;
  NTuple::Item<double>      m_d5Pi01Chi2;
  NTuple::Item<double>      m_d5Pi02Chi2;
  NTuple::Item<double>      m_d5Pi03Chi2;
  NTuple::Item<double>      m_d5sqrSHadSysFit;
  NTuple::Item<long>        m_i5BgrVeto;
  NTuple::Item<long>        m_i5mcTruePi;
  NTuple::Item<long>        m_i5mcTruePi0;
  NTuple::Item<long>        m_i5mcTrueISR;
  NTuple::Item<long>        m_i5mcTrueGammaFromJpsi;
  NTuple::Item<long>        m_i5mcTrueOtherGamma;
  NTuple::Item<long>        m_i5mcTrueOther;
  NTuple::Item<double>      m_d5mcMHad;
  /*NTuple::Item<double>      m_d5Pi1EoP;
  NTuple::Item<double>      m_d5Pi2EoP;
  NTuple::Item<double>      m_d5Pi1MucDepth;
  NTuple::Item<double>      m_d5Pi2MucDepth;*/
  NTuple::Item<double>      m_d5DistZp;
  NTuple::Item<double>      m_d5DistRp;
  NTuple::Item<double>      m_d5DistZm;
  NTuple::Item<double>      m_d5DistRm;
  NTuple::Item<double>      m_d5PiNonRadChi2;
  NTuple::Item<double>      m_d5VtxChi2;
  NTuple::Item<double>      m_d5VtxPosX;
  NTuple::Item<double>      m_d5VtxPosY;
  NTuple::Item<double>      m_d5VtxPosZ;
  NTuple::Item<double>      m_d5IPposX;
  NTuple::Item<double>      m_d5IPposY;
  NTuple::Item<double>      m_d5IPposZ;
  // KsKpi veto
  NTuple::Item<double>      m_d5Prob1Pi;
  NTuple::Item<double>      m_d5Prob1K;
  NTuple::Item<double>      m_d5Prob1P;
  NTuple::Item<double>      m_d5Prob2Pi;
  NTuple::Item<double>      m_d5Prob2K;
  NTuple::Item<double>      m_d5Prob2P;
  // detector
  cParticleContainer        m_5Pi01;
  cParticleContainer        m_5Pi02;
  cParticleContainer        m_5Pi03;
  cParticleContainer        m_5Pi1;
  cParticleContainer        m_5Pi2;
  cParticleContainer        m_5Gammas[GAMMAS+7];
  NTuple::Item<double>      m_d5ClosestTrack[GAMMAS+7];
  // fit
  cParticleContainer        m_5FitPi01;
  cParticleContainer        m_5FitPi02;
  cParticleContainer        m_5FitPi03;
  cParticleContainer        m_5FitPi1;
  cParticleContainer        m_5FitPi2;
  cParticleContainer        m_5FitGammas[7];
  cParticleContainer        m_5TrueISR;


  // untagged event data
  NTuple::Tuple*            m_NUntagged;
  NTuple::Item<double>      m_dURunNumber;
  NTuple::Item<double>      m_dUEvtNumber;
  NTuple::Item<double>      m_dUCMSEnergy;
  NTuple::Item<long>        m_iUAnzGoodP;
  NTuple::Item<long>        m_iUAnzGoodM;
  NTuple::Item<long>        m_iUAnzGoodGamma;
  NTuple::Item<long>        m_iUAnzPi0Tracks;
  NTuple::Item<long>        m_iUAnz2Combs;
  NTuple::Item<long>        m_iUAnz3Combs;
  NTuple::Item<double>      m_dUTotalChi2;
  //NTuple::Item<double>      m_dUEventChi2;
  NTuple::Item<double>      m_dUPi01Chi2;
  NTuple::Item<double>      m_dUPi02Chi2;
  NTuple::Item<double>      m_dUMHadTagged;
  NTuple::Item<double>      m_dUsqrSHadSys;
  NTuple::Item<double>      m_dUsqrSHadSysFit;
  /*NTuple::Item<double>      m_dU2CFitChi2;
  NTuple::Item<double>      m_dU2CFitMhad;
  NTuple::Item<double>      m_dU2CFitMgg;
  NTuple::Item<double>      m_dU2CFitThISR;*/
  NTuple::Item<long>        m_iUBgrVeto;
  NTuple::Item<long>        m_iUmcTruePi;
  NTuple::Item<long>        m_iUmcTruePi0;
  NTuple::Item<long>        m_iUmcTrueISR;
  NTuple::Item<long>        m_iUmcTrueFSR;
  NTuple::Item<long>        m_iUmcTrueGammaFromJpsi;
  NTuple::Item<long>        m_iUmcTrueOtherGamma;
  NTuple::Item<long>        m_iUmcTrueOther;
  NTuple::Item<double>      m_dUmcMHad;
  NTuple::Item<double>      m_dUmcMHad5pi;
  NTuple::Item<double>      m_dUPi1EoP;
  NTuple::Item<double>      m_dUPi2EoP;
  NTuple::Item<double>      m_dUPi1MucDepth;
  NTuple::Item<double>      m_dUPi2MucDepth;
  NTuple::Item<double>      m_dUDistZp;
  NTuple::Item<double>      m_dUDistRp;
  NTuple::Item<double>      m_dUDistZm;
  NTuple::Item<double>      m_dUDistRm;
  //NTuple::Item<long>        m_iUmcOmega;
  NTuple::Item<double>      m_dU4PiNonRadChi2;
  // Vtx fit
  NTuple::Item<double>      m_dUVtxChi2;
  NTuple::Item<double>      m_dUVtxPosX;
  NTuple::Item<double>      m_dUVtxPosY;
  NTuple::Item<double>      m_dUVtxPosZ;
  NTuple::Item<double>      m_dUIPposX;
  NTuple::Item<double>      m_dUIPposY;
  NTuple::Item<double>      m_dUIPposZ;
  // KsKpi veto
  NTuple::Item<double>      m_dUProb1Pi;
  NTuple::Item<double>      m_dUProb1K;
  NTuple::Item<double>      m_dUProb1P;
  NTuple::Item<double>      m_dUProb2Pi;
  NTuple::Item<double>      m_dUProb2K;
  NTuple::Item<double>      m_dUProb2P;
  // detector
  cParticleContainer        m_UPi01;
  cParticleContainer        m_UPi02;
  cParticleContainer        m_UPi1;
  cParticleContainer        m_UPi2;
  cParticleContainer        m_UGammas[GAMMAS+5];
  cParticleContainer        m_TaggedISR;
  NTuple::Item<double>      m_dUClosestTrack[GAMMAS+5];
  // fit
  cParticleContainer        m_UFitPi01;
  cParticleContainer        m_UFitPi02;
  cParticleContainer        m_UFitPi1;
  cParticleContainer        m_UFitPi2;
  cParticleContainer        m_UFitGammas[5];
  cParticleContainer        m_UTrueISR;
  // 5pi veto
  //NTuple::Item<double>      m_dU5PiNonRadChi2;
  NTuple::Item<double>      m_dU5PiChi2;
  NTuple::Item<double>      m_dU5PiMhad;
  cParticleContainer        m_U5PiISR;


  // tagged event data
  NTuple::Tuple*            m_NTagged;
  NTuple::Item<double>      m_dRunNumber;
  NTuple::Item<double>      m_dEvtNumber;
  NTuple::Item<double>      m_dTCMSEnergy;
  NTuple::Item<long>        m_iAnzGoodP;
  NTuple::Item<long>        m_iAnzGoodM;
  NTuple::Item<long>        m_iAnzGoodGamma;
  NTuple::Item<long>        m_iAnzPi0Tracks;
  NTuple::Item<long>        m_iAnz2Combs;
  NTuple::Item<long>        m_iAnz3Combs;
  NTuple::Item<double>      m_dTotalChi2;
  NTuple::Item<double>      m_dEventChi2;
  /*NTuple::Item<double>      m_d5CFitChi2;
  NTuple::Item<double>      m_d5CFitMhad;
  NTuple::Item<double>      m_d5CFitMgg;*/
  NTuple::Item<double>      m_dPi01Chi2;
  NTuple::Item<double>      m_dPi02Chi2;
  NTuple::Item<double>      m_dsqrSHadSys;
  NTuple::Item<double>      m_dsqrSHadSysFit;
  NTuple::Item<long>        m_iBgrVeto;
  NTuple::Item<long>        m_imcTruePi;
  NTuple::Item<long>        m_imcTruePi0;
  NTuple::Item<long>        m_imcTrueISR;
  NTuple::Item<long>        m_imcTrueFSR;
  NTuple::Item<long>        m_imcTrueGammaFromJpsi;
  NTuple::Item<long>        m_imcTrueOtherGamma;
  NTuple::Item<long>        m_imcTrueOther;
  NTuple::Item<double>      m_dmcMHad;
  NTuple::Item<double>      m_dmcMHad5pi;
  NTuple::Item<double>      m_dPi1EoP;
  NTuple::Item<double>      m_dPi2EoP;
  NTuple::Item<double>      m_dPi1MucDepth;
  NTuple::Item<double>      m_dPi2MucDepth;
  NTuple::Item<double>      m_dDistZp;
  NTuple::Item<double>      m_dDistRp;
  NTuple::Item<double>      m_dDistZm;
  NTuple::Item<double>      m_dDistRm;
  NTuple::Item<double>      m_d4PiNonRadChi2;
  // vtx fit
  NTuple::Item<double>      m_dVtxChi2;
  NTuple::Item<double>      m_dVtxPosX;
  NTuple::Item<double>      m_dVtxPosY;
  NTuple::Item<double>      m_dVtxPosZ;
  NTuple::Item<double>      m_dIPposX;
  NTuple::Item<double>      m_dIPposY;
  NTuple::Item<double>      m_dIPposZ;
  // KsKpi veto
  NTuple::Item<double>      m_dProb1Pi;
  NTuple::Item<double>      m_dProb1K;
  NTuple::Item<double>      m_dProb1P;
  NTuple::Item<double>      m_dProb2Pi;
  NTuple::Item<double>      m_dProb2K;
  NTuple::Item<double>      m_dProb2P;

  // detector
  cParticleContainer        m_Pi01;
  cParticleContainer        m_Pi02;
  cParticleContainer        m_Pi1;
  cParticleContainer        m_Pi2;
  cParticleContainer        m_Gammas[GAMMAS+5];
  NTuple::Item<double>      m_dClosestTrack[GAMMAS+5];
  // fit
  cParticleContainer        m_FitPi01;
  cParticleContainer        m_FitPi02;
  cParticleContainer        m_FitPi1;
  cParticleContainer        m_FitPi2;
  cParticleContainer        m_FitGammas[5];
  cParticleContainer        m_TrueISR;
  // 5pi veto
  //NTuple::Item<double>      m_d5PiNonRadChi2;
  NTuple::Item<double>      m_d5PiChi2;
  NTuple::Item<double>      m_d5PiMhad;
  cParticleContainer        m_5PiISR;

  // particle lists
  EvtRecTrack*             m_pERTPiMinus;
  EvtRecTrack*             m_pERTPiPlus;
  EvtRecTrack*             m_pISRphoton;
  vector<EvtRecTrack*>     m_aISRphotons;
  cPi0TracksList           m_aPi0List;

  // attributes
  int                 m_iAll;
  int                 m_iPreSkip;
  int                 m_iPhotonSkip;  
  int                 m_iPionSkip;
  int                 m_iPi0Skip;
  int                 m_iUFitSkip;
  int                 m_iAcceptedU;

  double              m_dCMSEnergy;
  double              m_dMassPi0;
  double              m_dMassPiCharged;
  double              m_dISRcutLow;
  double              m_dEoPcut;
  double              m_dPi0GcutHigh;
  double              m_dMaxPi0Combs;
  double              m_dMaxGoodPh;
  double              m_dCylRad;
  double              m_dCylHeight;
  double              m_dPi0MassCutLow;
  double              m_dPi0MassCutHigh;
  double              m_dPhEnergyCutBarrel;
  double              m_dPhEnergyCutEndCap;
  double              m_dCosThetaBarrel;
  double              m_dMaxTime;
  double              m_dCosThetaMax;
  double              m_dCosThetaISR;
  double              m_dVtxR;
  HepLorentzVector    m_lvBoost;
  HepLorentzVector    m_lvPi01;
  HepLorentzVector    m_lvPi01G1;
  HepLorentzVector    m_lvPi01G2;
  HepLorentzVector    m_lvPi02;
  HepLorentzVector    m_lvPi02G1;
  HepLorentzVector    m_lvPi02G2;
  HepLorentzVector    m_lvPi03;
  HepLorentzVector    m_lvPi03G1;
  HepLorentzVector    m_lvPi03G2;
  HepLorentzVector    m_lvPi1;
  HepLorentzVector    m_lvPi2;
  HepLorentzVector    m_lvISR;
  HepLorentzVector    m_lvFitPi1;
  HepLorentzVector    m_lvFitPi2;
  HepLorentzVector    m_lvFitPi01;
  HepLorentzVector    m_lvFitPi02;
  HepLorentzVector    m_lvFitPi03;
  HepLorentzVector    m_lvFitPi01G1;
  HepLorentzVector    m_lvFitPi01G2;
  HepLorentzVector    m_lvFitPi02G1;
  HepLorentzVector    m_lvFitPi02G2;
  HepLorentzVector    m_lvFitPi03G1;
  HepLorentzVector    m_lvFitPi03G2;
  HepLorentzVector    m_lvFitISR;
  HepLorentzVector    m_lvmcPi1;
  HepLorentzVector    m_lvmcPi2;
  HepLorentzVector    m_lvmcPi01;
  HepLorentzVector    m_lvmcPi02;
  HepLorentzVector    m_lvmcPi03;
  /*HepLorentzVector    m_lvmcPi01G1;
  HepLorentzVector    m_lvmcPi01G2;
  HepLorentzVector    m_lvmcPi02G1;
  HepLorentzVector    m_lvmcPi02G2;*/
  HepLorentzVector    m_lvmcISR1;
  HepLorentzVector    m_lvmcISR2;
  bool                m_bMCrecorded;
  int                 m_iMCpi;
  int                 m_iMCpi0;
  int                 m_iMCisr;
  int                 m_iMCfsr;
  int                 m_iMCGammaFromJpsi;
  int                 m_iMCOtherGamma;
  int                 m_iMCOther;
  KalmanKinematicFit* m_KalKinFit;
  VertexFit*          m_Vtxfit;
  IVertexDbSvc*	      m_vtxSvc;			
  ParticleID*         m_PID;
  HepPoint3D          m_hp3IP;
  EvtRecTrackIterator m_itBegin;
  EvtRecTrackIterator m_itEndCharged;
  EvtRecTrackIterator m_itEndNeutral;
  double              m_dmHadSysMCtruth;
  double              m_d5mHadSysMCtruth;
  vector<EvtRecTrack*> m_aPhIndexList;
  double               m_dChisqMaxValue;

  // operations
  void    corset(HepSymMatrix &V, HepMatrix &C, int n);
  void    corgen(HepMatrix &C, HepVector &x, int n);
  void    calibration(RecMdcKalTrack *trk , HepVector &wtrk_zHel, int n);
  double  beam_energy(int runNo);
  void    AddToPionLists(EvtRecTrack* pTrack, double dEoP);
  bool    IsFromInteractionPoint(EvtRecTrack* pCur);
  bool    FillParticleList ();
  bool    FindCharged ();
  void    ResetNTuples ();
  TString ParticleName(int iID);
  double  ClosestTrack(EvtRecTrack* pCurNeutral);
  void    FitVertexPos ();
  double  IPdist (EvtRecTrack* pCur);
  void    AddAllToKinFit(cPi0Tracks* ptPi01,cPi0Tracks* ptPi02,cPi0Tracks* ptPi03,double iISR,EvtRecTrack* pISR, bool b5C=false);
  void    PrepareToWrite (cPi0Tracks* ptPi01,cPi0Tracks* ptPi02,cPi0Tracks* ptPi03,EvtRecTrack* pISR);
  void    PrepareToWriteNonrad (cPi0Tracks* ptPi01,cPi0Tracks* ptPi02,cPi0Tracks* ptPi03);
  bool    Find2pi02picTagged ();
  bool    Find2pi02picUntagged ();
  void    Try4PiFit();
  void    Try5PiFit();
  void    RecordOtherPhotons(int iMode);
  void    KsKpiVeto(int iMode);
  void    PerformPID(EvtRecTrack* pTrack);
  // monte carlo operations
  void    WriteMonteCarloTrurh ();
  void    PrintMCDecayChain (int iMode);

  //void RecordAngle();
 
};


