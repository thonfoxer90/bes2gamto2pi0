#include "EventModel/EventHeader.h"
#include "EventModel/Event.h"
#include "EvtRecEvent/EvtRecEvent.h"
#include "EvtRecEvent/EvtRecTrack.h"
#include "EvtRecEvent/EvtRecVeeVertex.h"
#include "EvtRecEvent/EvtRecPi0.h"
#include "EventNavigator/EventNavigator.h"

#include <string>
#include <TMath.h>
#include <TLorentzVector.h>
#include <TVector3.h>
//#include "PartPropSvc/PartPropSvc.h"
//#include "BesDChain/CDPi0List.h"
#include <map>

#include "GaudiKernel/Bootstrap.h"
#include "GaudiKernel/IJobOptionsSvc.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/SmartDataPtr.h"
#include "GaudiKernel/Algorithm.h"
#include "GaudiKernel/NTuple.h"

#include "McTruth/McParticle.h"
#include "ParticleID/ParticleID.h"

#include "VertexFit/VertexFit.h"
#include "VertexFit/SecondVertexFit.h"
#include "VertexFit/VertexDbSvc.h"
#include "VertexFit/IVertexDbSvc.h"
#include "VertexFit/Helix.h"
#include "VertexFit/KalmanKinematicFit.h"

#include "TRandom.h"
#include "CLHEP/Matrix/SymMatrix.h"
#include "CLHEP/Matrix/Matrix.h" 
#include "CLHEP/Matrix/Vector.h"
#include "CLHEP/Vector/LorentzVector.h"
#include "CLHEP/Vector/ThreeVector.h"
#include "VertexFit/WTrackParameter.h"
#include "MdcRecEvent/RecMdcTrack.h"
#include "MdcRecEvent/RecMdcKalTrack.h"

#include "Pi0recHelper.h"
#include "ParticleContainer.h"

#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"

# define GAMMAS 10

class Bes2Gamto2Pi0 : public Algorithm {

 public:

  Bes2Gamto2Pi0(const std::string& name, ISvcLocator*pSvcLocator);

  StatusCode initialize();
  StatusCode execute();
  StatusCode finalize();
  double beam_energy(int runNo);
	//bool Find2pi0Candidates();
	//bool Findelpos2pi0Untagged();
  double GetSumPt(std::vector<HepLorentzVector> vectlv);
	double GetPt(HepLorentzVector lv);
	bool IsPhoton(EvtRecTrack* pTrack);
	double RejectEMCtrack(EvtRecTrack* pCur);
	void DoUntagged();
	
 private:
 
 	//------ Histograms ------//
	IHistogram1D *m_hU2CombswValidFit;
	IHistogram1D *m_h2GammasInvM;
	IHistogram1D *m_hU2CombswSharedGammas;
	IHistogram2D *m_h2G1vs2G2InvM;
	
	map <int,IHistogram1D*> m_maphUNumEvt;
	map <int,IHistogram1D*> m_maphUSumEemc;
	map <int,IHistogram1D*> m_maphUValid2Combs;
	map <int,IHistogram1D*> m_maphUSumPt;
	map <int,IHistogram1D*> m_maphUFitSumPt;
	map <int,IHistogram1D*> m_maphUKinFitChi2;
	map <int,IHistogram1D*> m_maphUPi01InvM;
	map <int,IHistogram1D*> m_maphUPi02InvM;
	map <int,IHistogram1D*> m_maphUFitPi01InvM;
	map <int,IHistogram1D*> m_maphUFitPi02InvM;
	map <int,IHistogram1D*> m_maphU2Pi0InvM;
	map <int,IHistogram1D*> m_maphUFit2Pi0InvM;
		
	map <int,IHistogram2D*> m_maphUNumGamvsEnGam;
	map <int,IHistogram2D*> m_maphU2GamvsEn2Gam;
	map <int,IHistogram2D*> m_maphUFit2Pi0InvMvs2Pi0InvM;
 	//------ NTuples ------//
 	//All MC-truth
	NTuple::Tuple*	m_NAmcTruth;
	NTuple::Item<int> m_iAmcNpart;
	NTuple::Item<double> m_dAmcUSumPt2Pi0;
	NTuple::Item<double> m_dAmc2pi0InvM;
	NTuple::Array<int> m_iAmcPrimary;
	NTuple::Array<int> m_iAmcDecayGenerator;
	NTuple::Array<int> m_iAmcMother;
	NTuple::Array<double> m_adAmcID;
	NTuple::Array<double> m_adAmcE;
	NTuple::Array<double> m_adAmcP;
	NTuple::Array<double> m_adAmcTheta;
	NTuple::Array<double> m_adAmcPhi;
	
	
  //2pi0 untagged
  NTuple::Tuple*	m_NUntagged;
  NTuple::Item<double>	m_dURunNumber; //Run number
	
  NTuple::Item<double>      m_dUEvtNumber; //Event number
  NTuple::Item<double>      m_dUCMSEnergy; //Energy in CMS system
  NTuple::Item<long>        m_iUAnzGoodP; //Number of good positive track
  NTuple::Item<long>        m_iUAnzGoodM; //Number of good negative track
  NTuple::Item<long>        m_iUAnzGoodGamma; //Number of good photons
  NTuple::Item<long>        m_iUAnzPi0Tracks; //Number of Pi0 candidates tracks
  NTuple::Item<long>        m_iUAnz2Combs; //Number of good 2 photons combinations for pi0
  NTuple::Item<long>        m_iUAnz3Combs; //Number of good 3 photons combinations
  NTuple::Item<long>        m_iUBgrVeto;
  NTuple::Item<long>        m_iUmcTruePi;
  NTuple::Item<long>        m_iUmcTruePi0;
  NTuple::Item<long>        m_iUmcTrueFSR;
  NTuple::Item<long>        m_iUmcTrueGammaFromJpsi;
  NTuple::Item<long>        m_iUmcTrueOtherGamma;
  NTuple::Item<long>        m_iUmcTrueOther;
	NTuple::Item<long>				m_iUGood; //Good untagged 2pi0 event
  NTuple::Item<double>      m_dUDistZp;
  NTuple::Item<double>      m_dUDistRp;
  NTuple::Item<double>      m_dUDistZm;
  NTuple::Item<double>      m_dUDistRm;
  //NTuple::Item<long>        m_iUmcOmega;
	NTuple::Item<double>			m_dUpi01InvM;
	NTuple::Item<double>			m_dUpi02InvM;
	NTuple::Item<double>			m_dU2pi0InvM;
	NTuple::Item<double>			m_dUSumPtCM;
	NTuple::Item<double>			m_dUPtCMPi01;
	NTuple::Item<double>			m_dUPtCMPi02;
	NTuple::Item<double>			m_dUPCMbeamelecpositron;
	NTuple::Item<double>			m_dUPbeamelecpositron;
	NTuple::Item<double>			m_dUPi01CMPx;
	NTuple::Item<double>			m_dUPi01CMPy;
	NTuple::Item<double>			m_dUPi01CMPz;
	NTuple::Item<double>			m_dUPi01CMPt;
	NTuple::Item<double>			m_dUPi02CMPx;
	NTuple::Item<double>			m_dUPi02CMPy;
	NTuple::Item<double>			m_dUPi02CMPz;
	NTuple::Item<double>			m_dUPi02CMPt;
	NTuple::Item<double>			m_dUSumEemc;
	NTuple::Item<double>			m_cosThetaPi01CM2Gam;
	//Kinematic fit
	NTuple::Item<double>      m_dUTotalChi2; //Value of chi2 in mass constrained fit
  NTuple::Item<double>      m_dUPi01Chi2; //Value of Pi01 chi2 in mass constrained fit
  NTuple::Item<double>      m_dUPi02Chi2; //Value of Pi02 chi2 in mass constrained fit
	NTuple::Item<double>			m_dUFitpi01InvM;
	NTuple::Item<double>			m_dUFitpi02InvM;
	NTuple::Item<double>			m_dUFit2pi0InvM;
	NTuple::Item<double>			m_dUFitSumPtCM;
  // Vtx fit
  NTuple::Item<double>      m_dUVtxChi2;
  NTuple::Item<double>      m_dUVtxPosX;
  NTuple::Item<double>      m_dUVtxPosY;
  NTuple::Item<double>      m_dUVtxPosZ;
  NTuple::Item<double>      m_dUIPposX;
  NTuple::Item<double>      m_dUIPposY;
  NTuple::Item<double>      m_dUIPposZ;
  // KsKpi veto
  NTuple::Item<double>      m_dUProb1Pi;
  NTuple::Item<double>      m_dUProb1K;
  NTuple::Item<double>      m_dUProb1P;
  NTuple::Item<double>      m_dUProb2Pi;
  NTuple::Item<double>      m_dUProb2K;
  NTuple::Item<double>      m_dUProb2P;
  // detector
  cParticleContainer        m_UPi01;
  cParticleContainer        m_UPi02;
	cParticleContainer        m_UPi01CM;
  cParticleContainer        m_UPi02CM;
  cParticleContainer        m_UGammas[GAMMAS+5];
  //NTuple::Item<double>      m_dUClosestTrack[GAMMAS+5];
  // fit
  cParticleContainer        m_UFitPi01;
  cParticleContainer        m_UFitPi02;
  cParticleContainer        m_UFitGammas[5];
	
	
  //2pi0 electron-tagged
  NTuple::Tuple*	m_NtT2pi0;
  NTuple::Item<double> m_dT2pi0RunNumber;
	
  // particle lists
  cPi0TracksList           m_aPi0List;
  
   // attributes
  int                 m_iAll; //Total number of events
  int                 m_iPreSkip; //Number of skipped events at  track preselection
  int                 m_iPhotonSkip;  //Number of skipped events at number of good photons requirement
  int                 m_iPi0Skip; //Number of skipped events at number of 2 pi0 candidates requirement
  int                 m_iUFitSkip; //Number of events with failed kinematic fit
	int									m_iPreKept; //Number of kept events at  track preselection
	int									m_iPhotonKept; //Number of kept events at number of good photons requirement
	int									m_iPi0kept; //Number of kept events at number of 2 pi0 candidates requirement
	int									m_iUFitKept; //Number of events with succesful kinematic fit
  int                 m_iAcceptedU; //Number of untagged e+e-2pi0 events
	bool								m_bIsUntagged2pi0;
  double              m_dCMSEnergy;
	double 							m_dCrossingAngle;
	double 							m_dMasselec;
  double              m_dMassPi0;
  double              m_dMassPiCharged;
  double              m_dEoPcut;
  double              m_dPi0GcutHigh;
  double              m_dMaxPi0Combs;
  double              m_dMaxGoodPh;
  double              m_dCylRad;
  double              m_dCylHeight;
  double              m_dPi0MassCutLow;
  double              m_dPi0MassCutHigh;
  double              m_dPhEnergyCutBarrel;
  double              m_dPhEnergyCutEndCap;
  double              m_dCosThetaBarrel;
  double              m_dMaxTime;
  double              m_dCosThetaMax;
  double              m_dVtxR;
	double 							m_dUSumPtMax;
	double 							m_dUSumEemcMin;
	double 							m_dUSumEemcMax;
	HepLorentzVector 		m_lvBeamElectron;
	HepLorentzVector 		m_lvBeamPositron;
  HepLorentzVector    m_lvBoost; //Sum of beam e+ and beam e- 4 vectors (approximate)
  HepLorentzVector    m_lvPi01;
  HepLorentzVector    m_lvPi01G1;
  HepLorentzVector    m_lvPi01G2;
  HepLorentzVector    m_lvPi02;
  HepLorentzVector    m_lvPi02G1;
  HepLorentzVector    m_lvPi02G2;
  HepLorentzVector    m_lvPi03;
  HepLorentzVector    m_lvPi03G1;
  HepLorentzVector    m_lvPi03G2;
  HepLorentzVector    m_lvFitPi01;
  HepLorentzVector    m_lvFitPi02;
  HepLorentzVector    m_lvFitPi03;
  HepLorentzVector    m_lvFitPi01G1;
  HepLorentzVector    m_lvFitPi01G2;
  HepLorentzVector    m_lvFitPi02G1;
  HepLorentzVector    m_lvFitPi02G2;
  HepLorentzVector    m_lvFitPi03G1;
  HepLorentzVector    m_lvFitPi03G2;
	HepLorentzVector 		m_lvmcel; //electron MC-truth 4-vector
	HepLorentzVector 		m_lvmcpositron; //positron MC-truth 4-vectorr
  HepLorentzVector    m_lvmcPi01; //Pi01 MC-truth 4-vector
  HepLorentzVector    m_lvmcPi02; //Pi02 MC-truth 4-vector
  HepLorentzVector    m_lvmcPi03; //Pi03 MC-truth 4-vector
  
	HepLorentzVector 		m_lvBeamElectronCM;
	HepLorentzVector 		m_lvBeamPositronCM;
	HepLorentzVector 		m_lvPi01CM;
	HepLorentzVector 		m_lvPi02CM;
	HepLorentzVector 		m_lvFitPi01CM;
	HepLorentzVector 		m_lvFitPi02CM;
	HepLorentzVector 		m_lvPi012GamCM;
	HepLorentzVector 		m_lvPi022GamCM;
	HepLorentzVector 		m_lvFitPi012GamCM;
	HepLorentzVector 		m_lvFitPi022GamCM;
	
	bool								m_bKalFit;
  bool                m_bMCrecorded;
	int									m_iMCel; //Number of electron in MC truth
	int 								m_iMCelprimary;
	int									m_iMCpositron; //Number of positron in MC truth
	int 								m_iMCpositronprimary;
  int                 m_iMCpi0; //Number of pi0 in MC truth
  int                 m_iMCfsr; //Number of Final State Radiation in MC truth
  int                 m_iMCGammaFromJpsi; // J/psi->gamma+anything
  int                 m_iMCOtherGamma; // all other except from pi0 decay
  int                 m_iMCOther; //Number of other MC truth particles
  double              m_dmTotFinalMCtruth;
	
  KalmanKinematicFit* m_KalKinFit;
  VertexFit*          m_Vtxfit;
  IVertexDbSvc*	      m_vtxSvc;			
  ParticleID*         m_PID;
	HepPoint3D          m_hp3IP;
  
  EvtRecTrackIterator m_itBegin;
  EvtRecTrackIterator m_itEndCharged;
  EvtRecTrackIterator m_itEndNeutral;
	
	vector<EvtRecTrack*> m_aPhIndexList;
	double               m_dChisqMaxValue;
};


