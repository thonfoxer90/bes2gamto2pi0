#include "EventModel/EventHeader.h"
#include "EventModel/Event.h"
#include "EvtRecEvent/EvtRecTrack.h"
#include "VertexFit/WTrackParameter.h"


class	GamTrackList {
public:
//operations
GamTrackList();
void ClearLists();
void Init(double barrelEmin,double endcapEmin,double cosTbarrel,double time,double Ebeam);
void SetIterators (EvtRecTrackIterator itBegin,EvtRecTrackIterator itEndCh,EvtRecTrackIterator itEndN);
double RejectEMCtrack (EvtRecTrack* pCur);
bool IsPhoton(EvtRecTrack* pTrack);
void Add(EvtRecTrack* pT);
HepLorentzVector Get4VecFromEMC (EvtRecTrack* pCur);

//attributes
vector<EvtRecTrack*> m_aGamTable;
EvtRecTrackIterator m_itBegin;
  EvtRecTrackIterator m_itEndCharged;
  EvtRecTrackIterator m_itEndNeutral;
double              m_dPhEnergyCutBarrel;
  double              m_dPhEnergyCutEndCap;
  double              m_dCosThetaBarrel;
  double              m_dMaxTime;
  double              m_dBeamEnergy;
  double              m_dDeltaAngleCut;
	
};

class cPi0Tracks {

 public:

  // operations
  cPi0Tracks();
  cPi0Tracks(EvtRecTrack* pT1,EvtRecTrack* pT2,double dM);//,int iT1,int iT2);
  void Set(EvtRecTrack* pT1,EvtRecTrack* pT2,double dM);//,int iT1,int iT2);
	
	cPi0Tracks(EvtRecTrack* pT1,EvtRecTrack* pT2,double dM,int iT1,int iT2);
	void Set(EvtRecTrack* pT1,EvtRecTrack* pT2,double dM,int iT1,int iT2);
	
	void SetiT1(int iT1=-1){m_iT1=iT1;};
	int GetiT1() const {return m_iT1;};
	
	void SetiT2(int iT2=-1){m_iT2=iT2;};
	int GetiT2() const {return m_iT2;};
	
	void SetMass(double Mass=0.){m_dMass=Mass;};
	double GetMass() const {return m_dMass;};
	
	void SetEAsym(double EAsym=0.){m_dEAsym=EAsym;
	//cout << m_dEAsym <<endl;
	};
	double GetEAsym() const {return m_dEAsym;};
	
	void SetCosTheta2GHelicityG1(double CosTheta2GHelicityG1=0.){m_dCosTheta2GHelicityG1=CosTheta2GHelicityG1;};
	double GetCosTheta2GHelicityG1() const {return m_dCosTheta2GHelicityG1;};
	
	void Set2GOpeningAngle(double TwoGOpeningAngle=0.){m_d2GOpeningAngle=TwoGOpeningAngle;};
	double Get2GOpeningAngle() const {return m_d2GOpeningAngle;};
	
	void Set2GDeltaTheta(double TwoGDeltaTheta=0.){m_d2GDeltaTheta=TwoGDeltaTheta;};
	double Get2GDeltaTheta() const {return m_d2GDeltaTheta;};
	
	void Set2GDeltaPhi(double TwoGDeltaPhi=0.){m_d2GDeltaPhi=TwoGDeltaPhi;};
	double Get2GDeltaPhi() const {return m_d2GDeltaPhi;};
	
  // attributes
  EvtRecTrack* m_pTrack1;
  EvtRecTrack* m_pTrack2;
  double       m_dMass;
  int          m_iT1;
  int          m_iT2;

	double m_dEAsym;
	double m_dCosTheta2GHelicityG1;
	double m_d2GOpeningAngle;
	double m_d2GDeltaTheta;
	double m_d2GDeltaPhi;
};

class ValidCombs2 {
 
public:

  // operations
  ValidCombs2();
  void Set(cPi0Tracks* pT1,cPi0Tracks* pT2);

  // attributes
  cPi0Tracks* m_pTrack1;
  cPi0Tracks* m_pTrack2;

};

class ValidCombs3 {

 public:

  // operations
  ValidCombs3();
  void Set(cPi0Tracks* pT1,cPi0Tracks* pT2,cPi0Tracks* pT3);

  // attributes
  cPi0Tracks* m_pTrack1;
  cPi0Tracks* m_pTrack2;
  cPi0Tracks* m_pTrack3;

};

class cPi0TracksList {

 public:

  // operations
  cPi0TracksList();
  void Init(double mlow,double mhigh,double barrelEmin,double endcapEmin,double cosTbarrel,double time,double Ebeam);
  void SetIterators (EvtRecTrackIterator itBegin,EvtRecTrackIterator itEndCh,EvtRecTrackIterator itEndN);
  void Add(cPi0Tracks track);
  void Add(EvtRecTrack* pT1,EvtRecTrack* pT2,double dM);
	void Add(EvtRecTrack* pT1,EvtRecTrack* pT2,double dM, int iT1,int iT2);
  void ClearLists();
  void FillList();
  void CreatedValid2Combs();
  void CreatedValid3Combs();
  bool IsPhoton(EvtRecTrack* pTrack);
  double RejectEMCtrack (EvtRecTrack* pCur);
  double GetM(EvtRecTrack* pCur1,double dMass1,EvtRecTrack* pCur2,double dMass2);
  HepLorentzVector Get4VecFromEMC (EvtRecTrack* pCur,double dMass);
  bool   IsUsedForPi0 (EvtRecTrack* pCur,cPi0Tracks* ptPi01,cPi0Tracks* ptPi02,cPi0Tracks* ptPi03);
  bool   IsInPi0List (EvtRecTrack* pCur);
	vector<cPi0Tracks> GetPi0Tracks(); 
	//m_aPi0TracksTable member not properly set when called with this method
	//To access m_aPi0TracksTable, use cPi0TracksList.m_aPi0TracksTable
	//rather than vector<cPi0Tracks>*  aPi0TracksTable = &(cPi0TracksList.GetPi0Tracks())

  // attributes
  vector<cPi0Tracks>       m_aPi0TracksTable;
  vector<ValidCombs2>      m_aValidCombs2;
  vector<ValidCombs3>      m_aValidCombs3;

  double              m_dPi0MassCutLow;
  double              m_dPi0MassCutHigh;
  double              m_dPhEnergyCutBarrel;
  double              m_dPhEnergyCutEndCap;
  double              m_dCosThetaBarrel;
  double              m_dMaxTime;
  double              m_dBeamEnergy;
  double              m_dDeltaAngleCut;
  EvtRecTrackIterator m_itBegin;
  EvtRecTrackIterator m_itEndCharged;
  EvtRecTrackIterator m_itEndNeutral;

};

