#include "EventModel/EventHeader.h"
#include "EventModel/Event.h"
#include "EvtRecEvent/EvtRecTrack.h"
#include "VertexFit/WTrackParameter.h"

class ChargedTracksList {
public:
ChargedTracksList();
void ClearLists();
void SetIterators (EvtRecTrackIterator itBegin,EvtRecTrackIterator itEndCh,EvtRecTrackIterator itEndN);
void Add(EvtRecTrack* pT);
vector<EvtRecTrack*>& GetERTList();

private:
vector<EvtRecTrack*> m_vecERT;
EvtRecTrackIterator m_itBegin;
EvtRecTrackIterator m_itEndCharged;
EvtRecTrackIterator m_itEndNeutral;
	
};

