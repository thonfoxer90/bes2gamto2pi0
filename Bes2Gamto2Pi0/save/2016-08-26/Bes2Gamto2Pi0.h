#include "EventModel/EventHeader.h"
#include "EventModel/Event.h"
#include "EvtRecEvent/EvtRecEvent.h"
#include "EvtRecEvent/EvtRecTrack.h"
#include "EvtRecEvent/EvtRecVeeVertex.h"
#include "EvtRecEvent/EvtRecPi0.h"
#include "EventNavigator/EventNavigator.h"

#include <string>
#include <TMath.h>
#include <TLorentzVector.h>
#include <TVector3.h>
//#include "PartPropSvc/PartPropSvc.h"
//#include "BesDChain/CDPi0List.h"
#include <map>

#include "GaudiKernel/Bootstrap.h"
#include "GaudiKernel/IJobOptionsSvc.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/SmartDataPtr.h"
#include "GaudiKernel/Algorithm.h"
#include "GaudiKernel/NTuple.h"


#include "McTruth/McParticle.h"
#include "ParticleID/ParticleID.h"

#include "VertexFit/VertexFit.h"
#include "VertexFit/SecondVertexFit.h"
#include "VertexFit/VertexDbSvc.h"
#include "VertexFit/IVertexDbSvc.h"
#include "VertexFit/Helix.h"
#include "VertexFit/KalmanKinematicFit.h"

#include "TRandom.h"
#include "CLHEP/Matrix/SymMatrix.h"
#include "CLHEP/Matrix/Matrix.h" 
#include "CLHEP/Matrix/Vector.h"
#include "CLHEP/Vector/LorentzVector.h"
#include "CLHEP/Vector/ThreeVector.h"
#include "VertexFit/WTrackParameter.h"
#include "MdcRecEvent/RecMdcTrack.h"
#include "MdcRecEvent/RecMdcKalTrack.h"

#include "Pi0recHelper.h"
#include "ParticleContainer.h"

#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"

# define GAMMAS 10

class Bes2Gamto2Pi0 : public Algorithm {

 public:

  Bes2Gamto2Pi0(const std::string& name, ISvcLocator*pSvcLocator);

  StatusCode initialize();
  StatusCode execute();
  StatusCode finalize();
  double beam_energy(int runNo);
	//bool Find2pi0Candidates();
	//bool Findelpos2pi0Untagged();
	HepLorentzVector GetBoostedLV(HepLorentzVector lvIniFrame, Hep3Vector vectboost);
  double GetSumPt(std::vector<HepLorentzVector> vectlv);
	double GetPt(HepLorentzVector lv);
	double GetEAsym2Gam(HepLorentzVector lvG1,HepLorentzVector lvG2);
	bool IsPhoton(EvtRecTrack* pTrack);
	double RejectEMCtrack(EvtRecTrack* pCur);
	void DoMCTruth();
	void DoUntagged();
	void DoeTagged();
	double IPdist (EvtRecTrack* pCur);
	
 private:
 
 	
 	//------ All MC-truth ------//
	NTuple::Tuple*	m_NAmcTruth;
	NTuple::Item<int> m_iAmcNpart;
	NTuple::Item<double> m_dAmcUSumPt2Pi0;
	NTuple::Item<double> m_dAmc2pi0InvM;
	NTuple::Array<int> m_iAmcPrimary;
	NTuple::Array<int> m_iAmcDecayGenerator;
	NTuple::Array<int> m_iAmcMother;
	NTuple::Array<double> m_adAmcID;
	NTuple::Array<double> m_adAmcE;
	NTuple::Array<double> m_adAmcP;
	NTuple::Array<double> m_adAmcTheta;
	NTuple::Array<double> m_adAmcPhi;
	
	
  //------ 2pi0 untagged ------//
	//Attributes
	int                 m_iUPreSkip; //Number of skipped events at  track preselection
  int                 m_iUPhotonSkip;  //Number of skipped events at number of good photons requirement
  int                 m_iUPi0Skip; //Number of skipped events at number of 2 pi0 candidates requirement
  int                 m_iUFitSkip; //Number of events with failed kinematic fit
	int									m_iUPreKept; //Number of kept events at  track preselection
	int									m_iUPhotonKept; //Number of kept events at number of good photons requirement
	int									m_iUPi0kept; //Number of kept events at number of 2 pi0 candidates requirement
	int									m_iUFitKept; //Number of events with succesful kinematic fit
  int                 m_iAcceptedU; //Number of untagged e+e-2pi0 events
	//bool								m_bIsUntagged2pi0;
	
	double 							m_dUSumPtMax;
	double 							m_dUSumEemcMin;
	double 							m_dUSumEemcMax;
	
	//Histograms
	IHistogram1D *m_hU2CombswValidFit;
	IHistogram1D *m_hU2CombswSharedGammas;
	IHistogram2D *m_h2G1vs2G2InvM;
	
	map <int,IHistogram1D*> m_maphUNumEvt;
	map <int,IHistogram1D*> m_maphUSumEemc;
		
	map <int,IHistogram1D*> m_maphU2GammasInvM;
	map <int,IHistogram2D*> m_maphU2G1vs2G2InvM;
	
	map <int,IHistogram1D*> m_maphUValid2Combs;	
	map <int,IHistogram1D*> m_maphUKinFitChi2;
	map <int,IHistogram1D*> m_maphUPi01InvM;
	map <int,IHistogram1D*> m_maphUPi02InvM;
	map <int,IHistogram1D*> m_maphUFitPi01InvM;
	map <int,IHistogram1D*> m_maphUFitPi02InvM;
	map <int,IHistogram1D*> m_maphU2Pi0InvM;
	map <int,IHistogram1D*> m_maphUFit2Pi0InvM;		
	map <int,IHistogram2D*> m_maphUNumGamvsEnGam;
	map <int,IHistogram2D*> m_maphU2GamvsEn2Gam;
	map <int,IHistogram2D*> m_maphUFit2Pi0InvMvs2Pi0InvM;
	map <int,IHistogram1D*> m_maphUFitEAsymPi01;
	map <int,IHistogram1D*> m_maphUFitEAsymPi02;
	map <int,IHistogram1D*> m_maphUSumPt;
	map <int,IHistogram1D*> m_maphUFitSumPt;
	map <int,IHistogram1D*> m_maphUFitcosThetaPi01CM2Gam;
	
	//Ntuple
  NTuple::Tuple*	m_NUntagged;
  NTuple::Item<double>	m_dURunNumber; //Run number
	
  NTuple::Item<double>      m_dUEvtNumber; //Event number
  NTuple::Item<double>      m_dUCMSEnergy; //Energy in CMS system
  NTuple::Item<long>        m_iUAnzGoodP; //Number of good positive track
  NTuple::Item<long>        m_iUAnzGoodM; //Number of good negative track
  NTuple::Item<long>        m_iUAnzGoodGamma; //Number of good photons
  NTuple::Item<long>        m_iUAnzPi0Tracks; //Number of Pi0 candidates tracks
  NTuple::Item<long>        m_iUAnz2Combs; //Number of good 2 photons combinations for pi0
  NTuple::Item<long>        m_iUAnz3Combs; //Number of good 3 photons combinations
  NTuple::Item<long>        m_iUmcTruePi0;
  NTuple::Item<long>        m_iUmcTrueFSR;
  NTuple::Item<long>        m_iUmcTrueGammaFromJpsi;
  NTuple::Item<long>        m_iUmcTrueOtherGamma;
  NTuple::Item<long>        m_iUmcTrueOther;
	NTuple::Item<long>				m_iUGood; //Good untagged 2pi0 event
	NTuple::Item<double>			m_dUPi01InvM;
	NTuple::Item<double>			m_dUPi02InvM;
	NTuple::Item<double>			m_dU2pi0InvM;
	NTuple::Item<double>			m_dUEAsymPi01;
	NTuple::Item<double>			m_dUEAsymPi02;
	NTuple::Item<double>			m_dUSumPtCM;
	NTuple::Item<double>			m_dUPtCMPi01;
	NTuple::Item<double>			m_dUPtCMPi02;
	NTuple::Item<double>			m_dUPCMbeamelecpositron;
	NTuple::Item<double>			m_dUPbeamelecpositron;
	NTuple::Item<double>			m_dUPi01CMPx;
	NTuple::Item<double>			m_dUPi01CMPy;
	NTuple::Item<double>			m_dUPi01CMPz;
	NTuple::Item<double>			m_dUPi01CMPt;
	NTuple::Item<double>			m_dUPi02CMPx;
	NTuple::Item<double>			m_dUPi02CMPy;
	NTuple::Item<double>			m_dUPi02CMPz;
	NTuple::Item<double>			m_dUPi02CMPt;
	NTuple::Item<double>			m_dUSumEemc;	
	//Kinematic fit
	NTuple::Item<double>      m_dUTotalChi2; //Value of chi2 in mass constrained fit
  NTuple::Item<double>      m_dUPi01Chi2; //Value of Pi01 chi2 in mass constrained fit
  NTuple::Item<double>      m_dUPi02Chi2; //Value of Pi02 chi2 in mass constrained fit
	NTuple::Item<double>			m_dUFitPi01InvM;
	NTuple::Item<double>			m_dUFitPi02InvM;
	NTuple::Item<double>			m_dUFit2pi0InvM;
	NTuple::Item<double>			m_dUFitEAsymPi01;
	NTuple::Item<double>			m_dUFitEAsymPi02;
	NTuple::Item<double>			m_dUFitSumPtCM;
	NTuple::Item<double>			m_dUFitcosThetaPi01CM2Gam;
  // Vtx fit
  NTuple::Item<double>      m_dUVtxChi2;
  NTuple::Item<double>      m_dUVtxPosX;
  NTuple::Item<double>      m_dUVtxPosY;
  NTuple::Item<double>      m_dUVtxPosZ;
  NTuple::Item<double>      m_dUIPposX;
  NTuple::Item<double>      m_dUIPposY;
  NTuple::Item<double>      m_dUIPposZ;
  // KsKpi veto
  NTuple::Item<double>      m_dUProb1Pi;
  NTuple::Item<double>      m_dUProb1K;
  NTuple::Item<double>      m_dUProb1P;
  NTuple::Item<double>      m_dUProb2Pi;
  NTuple::Item<double>      m_dUProb2K;
  NTuple::Item<double>      m_dUProb2P;
  // detector
  cParticleContainer        m_UPi01;
  cParticleContainer        m_UPi02;
	cParticleContainer        m_UPi01CM;
  cParticleContainer        m_UPi02CM;
  cParticleContainer        m_UGammas[GAMMAS+5];
  //NTuple::Item<double>      m_dUClosestTrack[GAMMAS+5];
  // fit
  cParticleContainer        m_UFitPi01;
  cParticleContainer        m_UFitPi02;
  cParticleContainer        m_UFitGammas[5];
	
	// particle lists
  cPi0TracksList           m_aUPi0List;
	
	NTuple::Item<long>				m_iIsU2pi0;
	
  //------ 2pi0 single tagged ------//
	//Attributes
	int									m_ieTValidMDCKept; //Number of valid charged tracks
	int									m_ieTValidMDCIPKept; //Number of valid charged tracks from interaction point
	int									m_ieTGoodNegKept; //Number of kept good negative tracks
	int									m_ieT1Lepton; //Number of events with only one lepton
	int									m_ieTPhotonKept; //Number of kept events at number of good photons requirement
	int									m_ieTPi0kept; //Number of kept events at number of 2 pi0 candidates requirement
	int									m_ieTFitKept; //Number of events with succesful kinematic fit
  int                 m_iAcceptedeT; //Number of untagged e+e-2pi0 events
	
	
	//Histogram
	
	IHistogram1D* m_heTDist; //ok
	IHistogram1D* m_heTCylRad; //ok
	IHistogram1D* m_heTCylHeight; //ok
	IHistogram1D* m_heTPChargedTracks; //ok
	IHistogram2D* m_heTEtotOvrPvsP; //ok
	IHistogram1D *m_heTValid2Combs; //ok
	IHistogram1D *m_heT2CombswValidFit; //ok
	IHistogram1D *m_heT2GammasInvM; //ok
	IHistogram1D *m_heT2CombswSharedGammas; //ok
	IHistogram2D *m_heT2G1vs2G2InvM; //ok
	
		
	map <int,IHistogram1D*> m_mapheTNumEvt; //0,1,2,3,4,5
	map <int,IHistogram1D*> m_mapheTDist; 
	map <int,IHistogram1D*> m_mapheTCylRad;
	map <int,IHistogram1D*> m_mapheTCylHeight;
	map <int,IHistogram1D*> m_mapheTPChargedTracks;
	map <int,IHistogram2D*> m_mapheTEtotOvrPvsP;
	map <int,IHistogram1D*> m_mapheTSumEemc; //5
	map <int,IHistogram2D*> m_mapheTNumGamvsEnGam;
	map <int,IHistogram2D*> m_mapheT2GamvsEn2Gam;
	map <int,IHistogram1D*> m_mapheT2GammasInvM;
	
	map <int,IHistogram1D*> m_mapheT2GCThetaMiss;
	map <int,IHistogram1D*> m_mapheT2GCThetaHelicity;
	map <int,IHistogram1D*> m_mapheT2GOpeningAngle;
	map <int,IHistogram1D*> m_mapheT2GDeltaTheta;
	map <int,IHistogram1D*> m_mapheT2GDeltaPhi;
	
	map <int,IHistogram1D*> m_mapheTValid2Combs; //5
	map <int,IHistogram1D*> m_mapheTValid2CombswValidFit;	
	map <int,IHistogram1D*> m_mapheT2CombswSharedGammas;
	map <int,IHistogram2D*> m_mapheT2G1vs2G2InvM; //5	
	map <int,IHistogram1D*> m_mapheTKinFitChi2;
	map <int,IHistogram1D*> m_mapheTPi01InvM; //5
	map <int,IHistogram1D*> m_mapheTPi02InvM; //5
	map <int,IHistogram1D*> m_mapheTFitPi01InvM; //5
	map <int,IHistogram1D*> m_mapheTFitPi02InvM; //5
	map <int,IHistogram1D*> m_mapheT2Pi0InvM; //5
	map <int,IHistogram1D*> m_mapheTFit2Pi0InvM;	
	map <int,IHistogram2D*> m_mapheTFit2Pi0InvMvs2Pi0InvM;
	map <int,IHistogram1D*> m_mapheTFitEAsymPi01;
	map <int,IHistogram1D*> m_mapheTFitEAsymPi02;
	map <int,IHistogram1D*> m_mapheTSumPt;
	map <int,IHistogram1D*> m_mapheTFitSumPt;
	map <int,IHistogram1D*> m_mapheTFitcosThetaPi01CM2Gam;
	
	//Ntuples	
	NTuple::Tuple*	m_NeTElec;
	
	NTuple::Tuple*	m_NeTG;
	
	NTuple::Tuple*	m_NeT2G;
	NTuple::Item<double>	m_deT2GRunNumber; //Run number
	NTuple::Item<double>      m_deT2GEvtNumber; //Event Number
  NTuple::Item<double>      m_deT2GCMSEnergy; //CMS Energy
	NTuple::Item<int>      		m_ieT2GN2Gs;
	NTuple::Array<double>  		m_deT2GEG1;
	NTuple::Array<double>  		m_deT2GEG2;
	NTuple::Array<double>  		m_deT2GEAsymmetry;
	NTuple::Array<double>  		m_deT2GMass2G;
	NTuple::Array<double>  		m_deT2GCThetaMiss;
	NTuple::Array<double>  		m_deT2GCThetaHelicity;
	NTuple::Array<double>  		m_deT2GOpeningAngle;
	NTuple::Array<double>  		m_deT2GDeltaTheta;
	NTuple::Array<double>  		m_deT2GDeltaPhi;
	
	
	NTuple::Tuple*	m_NeT2x2Gs;
		
  NTuple::Tuple*	m_NeTagged; //NTuples for tagged e+e-2pi0 candidates
  NTuple::Item<double>	m_deTRunNumber; //Run number
	NTuple::Item<double>      m_deTEvtNumber; //Event Number
  NTuple::Item<double>      m_deTCMSEnergy; //CMS Energy
	NTuple::Item<double>      m_deTSumEemc; //CMS Energy
  NTuple::Item<long>        m_ieTAnzGoodP; //Number of good positive track
  NTuple::Item<long>        m_ieTAnzGoodM; //Number of good negative track
  NTuple::Item<long>        m_ieTAnzGoodGamma; //Number of good photons
  NTuple::Item<long>        m_ieTAnzPi0Tracks; //Number of Pi0 candidates tracks
  NTuple::Item<long>        m_ieTAnz2Combs; //Number of good 2 photons combinations
  NTuple::Item<long>        m_ieTAnz3Combs; //Number of good 3 photons combinations
  NTuple::Item<long>        m_ieTBgrVeto;
  NTuple::Item<long>        m_ieTmcTruePi;
  NTuple::Item<long>        m_ieTmcTruePi0;
  NTuple::Item<long>        m_ieTmcTrueISR;
	NTuple::Item<long>        m_ieTmcTrueFSR;
  NTuple::Item<long>        m_ieTmcTrueGammaFromJpsi;
  NTuple::Item<long>        m_ieTmcTrueOtherGamma;
  NTuple::Item<long>        m_ieTmcTrueOther;
  NTuple::Item<double>      m_deTmcMHad;
  NTuple::Item<double>      m_deTDistZp;
  NTuple::Item<double>      m_deTDistRp;
  NTuple::Item<double>      m_deTDistZm;
  NTuple::Item<double>      m_deTDistRm;
  NTuple::Item<double>      m_deTPiNonRadChi2;
  NTuple::Item<double>      m_deTVtxChi2;
  NTuple::Item<double>      m_deTVtxPosX;
  NTuple::Item<double>      m_deTVtxPosY;
  NTuple::Item<double>      m_deTVtxPosZ;
  NTuple::Item<double>      m_deTIPposX;
  NTuple::Item<double>      m_deTIPposY;
  NTuple::Item<double>      m_deTIPposZ;
	NTuple::Item<double>			m_deTPi01InvM;
	NTuple::Item<double>			m_deTPi02InvM;
	NTuple::Item<double>			m_deT2pi0InvM;
	NTuple::Item<double>			m_deTQ2;
	NTuple::Item<double>			m_deTEAsymPi01;
	NTuple::Item<double>			m_deTEAsymPi02;
	NTuple::Item<double>			m_deTSumPtCM;	
	NTuple::Item<double>			m_deTcosThetaPi01CM2Gam;
	NTuple::Item<double>			m_deTPhiPi01CM2Gam;
	NTuple::Item<double>			m_deTCThetaHelicity;
	NTuple::Item<double>			m_deTRGam;
	//Kinematic fit
	NTuple::Item<double>      m_deTTotalChi2; //Value of chi2 in mass constrained fit
  NTuple::Item<double>      m_deTPi01Chi2; //Value of Pi01 chi2 in mass constrained fit
  NTuple::Item<double>      m_deTPi02Chi2; //Value of Pi02 chi2 in mass constrained fit
	NTuple::Item<double>			m_deTFitPi01InvM;
	NTuple::Item<double>			m_deTFitPi02InvM;
	NTuple::Item<double>			m_deTFit2pi0InvM;
	NTuple::Item<double>			m_deTFitQ2;
	NTuple::Item<double>			m_deTFitEAsymPi01;
	NTuple::Item<double>			m_deTFitEAsymPi02;
	NTuple::Item<double>			m_deTFitSumPtCM;
	NTuple::Item<double>			m_deTFitcosThetaPi01CM2Gam;
	NTuple::Item<double>			m_deTFitPhiPi01CM2Gam;
	NTuple::Item<double>			m_deTFitCThetaHelicity;
	NTuple::Item<double>			m_deTFitRGam;
  // detector
	cParticleContainer        m_eTLepton;
  cParticleContainer        m_eTPi01;
  cParticleContainer        m_eTPi02;
  cParticleContainer        m_eTGammas[GAMMAS+7];
  NTuple::Item<double>      m_deTClosestTrack[GAMMAS+7];
  // fit
	cParticleContainer        m_eTFitLepton;
  cParticleContainer        m_eTFitPi01;
  cParticleContainer        m_eTFitPi02;
  cParticleContainer        m_eTFitGammas[7];
	
	NTuple::Item<long>				m_iIseT2pi0;
	
  // particle lists
  EvtRecTrack*             m_pERTLepton;
	cPi0TracksList           m_aeTPi0List;
  	
   //------ attributes ------//
	 bool								m_bDoUntagged;
	 bool								m_bDoeTagged;
	int									m_iNCharged;
	int									m_iNNeutral;
  int                 m_iAll; //Total number of events
  
  double              m_dCMSEnergy;
	double              m_dRunNumber;
	double 							m_dEvtNumber;
	double 							m_dCrossingAngle;
	double 							m_dMasselec;
  double              m_dMassPi0;
  double              m_dMassPiCharged;
  double              m_dEoPcut;
  double              m_dPi0GcutHigh;
  double              m_dMaxPi0Combs;
  double              m_dMaxGoodPh;
  double              m_dCylRad;
  double              m_dCylHeight;
  double              m_dPi0MassCutLow;
  double              m_dPi0MassCutHigh;
  double              m_dPhEnergyCutBarrel;
  double              m_dPhEnergyCutEndCap;
  double              m_dCosThetaBarrel;
	double							m_dCosThetaMissLepton;
	double							m_dCosTheta2GHelicityG1;
	double							m_d2GOpeningAngle;
	double							m_d2GDeltaTheta;
	double							m_d2GDeltaPhi;
  double              m_dMaxTime;
  double              m_dCosThetaMax;
  double              m_dVtxR;
	double							m_dPi01InvM;
	double							m_dFitPi01InvM;
	double							m_dPi02InvM;
	double							m_dFitPi02InvM;
	double							m_d2pi0InvM;
	double							m_dFit2pi0InvM;
	double							m_dQ2;
	double							m_dFitQ2;
	double							m_dEAsymPi01;
	double							m_dFitEAsymPi01;
	double							m_dEAsymPi02;
	double							m_dFitEAsymPi02;
	double							m_dSumPtCM;
	double							m_dFitSumPtCM;	
	double							m_dcosThetaPi01CM2Gam;
	double							m_dFitcosThetaPi01CM2Gam;
	double							m_dRGam;
	double							m_dFitRGam;
	
	HepLorentzVector 		m_lvBeamElectron;
	HepLorentzVector 		m_lvBeamPositron;
  HepLorentzVector    m_lvBoost; //Sum of beam e+ and beam e- 4 vectors (approximate)
	HepLorentzVector		m_lvLepton;
	HepLorentzVector		m_lvGammaStar;
  HepLorentzVector    m_lvPi01;
  HepLorentzVector    m_lvPi01G1;
  HepLorentzVector    m_lvPi01G2;
  HepLorentzVector    m_lvPi02;
  HepLorentzVector    m_lvPi02G1;
  HepLorentzVector    m_lvPi02G2;
  HepLorentzVector    m_lvPi03;
  HepLorentzVector    m_lvPi03G1;
  HepLorentzVector    m_lvPi03G2;
	HepLorentzVector		m_lvFitLepton;
	HepLorentzVector		m_lvFitGammaStar;
  HepLorentzVector    m_lvFitPi01;
  HepLorentzVector    m_lvFitPi02;
  HepLorentzVector    m_lvFitPi03;
  HepLorentzVector    m_lvFitPi01G1;
  HepLorentzVector    m_lvFitPi01G2;
  HepLorentzVector    m_lvFitPi02G1;
  HepLorentzVector    m_lvFitPi02G2;
  HepLorentzVector    m_lvFitPi03G1;
  HepLorentzVector    m_lvFitPi03G2;
	HepLorentzVector 		m_lvmcel; //electron MC-truth 4-vector
	HepLorentzVector 		m_lvmcpositron; //positron MC-truth 4-vectorr
  HepLorentzVector    m_lvmcPi01; //Pi01 MC-truth 4-vector
  HepLorentzVector    m_lvmcPi02; //Pi02 MC-truth 4-vector
  HepLorentzVector    m_lvmcPi03; //Pi03 MC-truth 4-vector
  
	Hep3Vector 					m_vectboostCM;
	
	HepLorentzVector 		m_lvBeamElectronCM;
	HepLorentzVector 		m_lvBeamPositronCM;
	HepLorentzVector 		m_lvLeptonCM;
	HepLorentzVector 		m_lvPi01CM;
	HepLorentzVector 		m_lvPi02CM;
	HepLorentzVector 		m_lvFitLeptonCM;
	HepLorentzVector 		m_lvFitPi01CM;
	HepLorentzVector 		m_lvFitPi02CM;
	
	Hep3Vector					m_boost2pi0;
	Hep3Vector					m_boostFit2pi0;
	
	HepLorentzVector 		m_lvLeptonCM2Gam;
	HepLorentzVector 		m_lvPi01CM2Gam;
	HepLorentzVector 		m_lvPi02CM2Gam;
	HepLorentzVector 		m_lvFitLeptonCM2Gam;
	HepLorentzVector 		m_lvFitPi01CM2Gam;
	HepLorentzVector 		m_lvFitPi02CM2Gam;
	
	bool								m_bKalFit;
  bool                m_bMCrecorded;
	int									m_iMCel; //Number of electron in MC truth
	int 								m_iMCelprimary;
	int									m_iMCpositron; //Number of positron in MC truth
	int 								m_iMCpositronprimary;
  int                 m_iMCpi0; //Number of pi0 in MC truth
  int                 m_iMCfsr; //Number of Final State Radiation in MC truth
  int                 m_iMCGammaFromJpsi; // J/psi->gamma+anything
  int                 m_iMCOtherGamma; // all other except from pi0 decay
  int                 m_iMCOther; //Number of other MC truth particles
  double              m_dmTotFinalMCtruth;
	
  KalmanKinematicFit* m_KalKinFit;
  VertexFit*          m_Vtxfit;
  IVertexDbSvc*	      m_vtxSvc;			
  ParticleID*         m_PID;
	HepPoint3D          m_hp3IP;
  
  EvtRecTrackIterator m_itBegin;
  EvtRecTrackIterator m_itEndCharged;
  EvtRecTrackIterator m_itEndNeutral;
	
	vector<EvtRecTrack*> m_aPhIndexList;
	double               m_dChisqMaxValue;
};


